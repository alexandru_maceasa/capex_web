<%@page import="com.profluo.ec.carrefour.portlet.StatusFacturi"%>
<%@page import="com.profluo.ecm.model.vo.StatusFacturiBD"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme"%>

<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui"%>
<%@ taglib uri="http://alloy.liferay.com/tld/aui" prefix="aui"%>
<%@ taglib uri="http://liferay.com/tld/util" prefix="liferay-util"%>

<%@page import="java.util.HashMap"%>
<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>


<liferay-theme:defineObjects />
<portlet:defineObjects />

<portlet:resourceURL id="ajaxURL" var="ajaxURL"/>

<portlet:resourceURL id="exportAction" var="exportURL">
	<portlet:param name="action" value="exportExcel"/>
</portlet:resourceURL>

<%
	int count 		= 5;
	int start 		= 0;
	int total 		= 0;
	String factNo 	= "";
	String invNo	= "";
	String storeId 	= "";
	List<HashMap<String,Object>> allInvoices = StatusFacturiBD.getAllInvoicesFromOptimal(start, count, "", "", "");
	allInvoices.addAll(StatusFacturiBD.getAllVouchersFromOptimal(start, count, "", "", ""));
	allInvoices.addAll(StatusFacturiBD.getAllWorkingEquipmentFromOptimal(start, count, "", "", ""));
	total = StatusFacturiBD.getAllInvoicesFromOptimalCount("", "", "");
	total += StatusFacturiBD.getAllVouchersFromOptimalCount("", "", "");
	total += StatusFacturiBD.getAllWorkingEquipmentFromOptimalCount("", "", "");
%>

<aui:layout>
	<aui:form id="exportForm" name="exportForm" action="<%=exportURL.toString()%>">
		<aui:row rowWidth="50">
			<aui:column columnWidth="15">Export facturi de la data: </aui:column>
			<aui:column columnWidth="10">
				<div id="from-date-div" style="height:30px !important;">
					<aui:input type="text" id="from_date" name="from_date" style="width:100px;" value="" placeholder="aaaa-ll-zz" label=""></aui:input>
				</div>
			</aui:column>
			<aui:column columnWidth="10">Pana la data de:</aui:column>
			<aui:column columnWidth="10">
				<div id="from-date-div" style="height:30px !important;">
					<aui:input type="text" id="to_date" name="to_date" style="width:100px;" value="" placeholder="aaaa-ll-zz" label=""></aui:input>
				</div>
			</aui:column>
			<aui:column style="display:none">
				<aui:input type="text" name="export_type" id="export_type"/>
			</aui:column>
		</aui:row>
		<aui:row>
			<aui:column columnWidth="25">
				<a class="btn btn-primary" onclick="exportFacturi('<portlet:namespace />')"> Export Excel </a>
<%-- 				<a class="btn btn-primary" onclick="exportVouchere('<portlet:namespace />')"> Export Bonuri de cesiune </a> --%>
			</aui:column>
		</aui:row>
	</aui:form>
	<br/>
	<aui:row>
		<aui:column first="true" columnWidth="100">
			<div id="invoices_status"></div>
		</aui:column>
	</aui:row>
</aui:layout>

<%-- HTML for PAGINATION --%>
<%@ include file="/html/includes/pagination.jsp" %>

<script type="text/javascript">

var start 		= "<%=start%>";
var count 		= "<%=count%>";
var total 		= "<%=total%>";
var factNo 		= "<%=factNo%>";
var invNo 		= "<%=invNo%>";
var storeId		= "<%=storeId%>";
var fromDate 	='';
var toDate		='';

	function exportFacturi(portletId){
		if ( !fromDate || fromDate == ''){
			alert("Va rugam sa selectati data de inceput.");
			return;
		}
		if ( !toDate || toDate == ''){
			alert("Va rugam sa selectati data de sfarsit.");
			return;
		}
		$('#'+portletId+'export_type').val("exportFacturi");
		$('#'+portletId+'exportForm').submit();
	}
/*
	function exportVouchere(portletId) {
		if ( !fromDate || fromDate == ''){
			alert("Va rugam sa selectati data de inceput.");
			return;
		}
		if ( !toDate || toDate == ''){
			alert("Va rugam sa selectati data de sfarsit.");
			return;
		}
		$('#'+portletId+'export_type').val("exportVouchere");
		$('#'+portletId+'exportForm').submit();
	}
*/
YUI().use('aui-datepicker', 'aui-datatable', 'aui-datatype', 'datatable-sort',
	function(Y) {
		var remoteData = [
		                  <% for(int i = 0; i< allInvoices.size(); i++) { %>
		                  	{
		      					nrInventar: <%=allInvoices.get(i).get("nrInventar")%>,
		      					dataPIF: '<%=allInvoices.get(i).get("dataPIF")%>', 
		      					denumire : '<%=allInvoices.get(i).get("denumire")%>',
		      					nrFactura: '<%=allInvoices.get(i).get("nrFactura")%>',
		      					dataFactura:'<%=allInvoices.get(i).get("dataFactura")%>',
		      					codSocietate: '<%=allInvoices.get(i).get("codSocietate")%>',
		      					codMagazin:'<%=allInvoices.get(i).get("codMagazin")%>',
		      					raion:'<%=allInvoices.get(i).get("raion")%>'
	                		}
		                  <% if (i != (allInvoices.size() - 1)) { out.print(","); } %>
		                  <% } %>
		                  ];
		var nestedCols = [
							 {key: 'nrInventar', label: '<input class="field" id="nr_inventar" name="nr_inventar" placeholder="Numar inventar" style="width:150px !important;margin:0"/>'}, 
							 {key: 'dataPIF', label:'Data PIF'},
							 {key: 'denumire', label:'Denumire'},
							 {key: 'nrFactura', label: '<input class="field" id="invoice_no" name="inventory_no" placeholder="Numar factura" style="width:100px !important;margin:0"/>'},
							 {key:'dataFactura',label:'Data factura'},
							 {key:'codSocietate', label :'Cod societate'},
							 {key:'codMagazin', label:'<input class="field" id="cod_magazin" name ="cod_magazin" placeholder="Cod magazin" style="width:100px !important;margin:0"/>'},
							 {key:'raion', label:'Raion'}
		                  ];
		
		var dataTable = new Y.DataTable({
		    columns: nestedCols,
		    data: remoteData,	
		    editEvent: 'click',
		    recordType: ['nrFactura' ,'dataFactura', 'magazin_name', 'furnizor']
		}).render('#invoices_status');
	
		// PAGINATION
  		// set status of navigation buttons on page load
  		setNavigationButtonsState('<portlet:namespace />');
  		$('.navigation').click(function(e) {
  			var elementT 	= e.target;
  			var elementId 	= elementT.id;
  			var action 		= "";
  			
  			// determine which button was pressed and set the value of start accordingly
  			if (elementId.indexOf("first") > -1)	{ action = "first"; start = 0; }
  			if (elementId.indexOf("prev") > -1) 	{ action = "prev"; if (start - count <= 0) {start = 0} else { start -= count; } }
  			if (elementId.indexOf("next") > -1) 	{ action = "next"; start = parseInt(start) + parseInt(count); }
  			if (elementId.indexOf("last") > -1) 	{ action = "last"; start = parseInt(parseInt(total) / parseInt(count)) * count; if (start == total) { start -= count; } }
  			
  			makeAjaxCall(action, "");
  		});
  		
  		
  		//filtru dupa numarul de factura
  		$('#invoice_no').keyup(function (e) {
		    if ($("#invoice_no").is(":focus") && (e.keyCode == 13)) {
				var action 		= "filter";
				factNo = e.target.value;
				storeCode = $('#cod_magazin').val();
				invNo = $('#nr_inventar').val();
				start = 0;
				makeAjaxCall(action);
		    }
  		});
  		
  		$('#nr_inventar').keyup(function (e) {
		    if ($("#nr_inventar").is(":focus") && (e.keyCode == 13)) {
				var action 		= "filter";
				invNo = e.target.value;
				factNo = $('#invoice_no').val();
				storeCode = $('#cod_magazin').val();
				start = 0;
				makeAjaxCall(action);				
		    }
		});
  		
  		$('#cod_magazin').keyup(function (e) {
		    if ($("#cod_magazin").is(":focus") && (e.keyCode == 13)) {
				var action 		= "filter";
				storeId = e.target.value;
				factNo = $('#invoice_no').val();
				invNo = $('#nr_inventar').val();
				start = 0;
				makeAjaxCall(action);				
		    }
		});
  		
  		function makeAjaxCall(action) {
  			$.ajax({
  				type: "POST",
  				url: "<%=ajaxURL%>",
  				data: 	"<portlet:namespace />action=" + action + 
  						"&<portlet:namespace />start=" + start + 
  						"&<portlet:namespace />count=" + count +
  						"&<portlet:namespace />total=" + total + 
  						"&<portlet:namespace />factNo=" + factNo +
  						"&<portlet:namespace />invNo=" + invNo +
  						"&<portlet:namespace />storeId=" + storeId,
  				success: function(msg) {
  					console.log(fromDate + ' -> ' + toDate);
  					// get table data
  					if (msg != ""){
  						var jsonEntries = jQuery.parseJSON(msg);
  						total = jsonEntries.total;
  						dataTable.set('data', eval(jsonEntries.values));
  					} else {
  						dataTable.set('data', []);
  					}
  					
  					// set status of navigation buttons on page load
  					setNavigationButtonsState('<portlet:namespace />');
  				}
  			});
  		}
  		
  		var fromDatepicker = new Y.DatePicker({
			trigger : '#<portlet:namespace />from_date',
			mask : '%Y-%m-%d',
			popover : {
				zIndex : 1
			},
			popover: {
				zIndex : 1
			},
			calendar : {
				dateFormat : '%Y-%m-%d'
			},
			after: {
	            selectionChange: function(event) {
	            	fromDate = $('#<portlet:namespace />from_date').val();
	            }
	        }
		});
  		
  		var toDatepicker = new Y.DatePicker({
			trigger : '#<portlet:namespace />to_date',
			mask : '%Y-%m-%d',
			popover : {
				zIndex : 1
			},
			popover: {
				zIndex : 1
			},
			calendar : {
				dateFormat : '%Y-%m-%d'
			},
			after: {
	            selectionChange: function(event) {
	            	toDate = $('#<portlet:namespace />to_date').val();
	            }
	        }
		});	
});
</script>
