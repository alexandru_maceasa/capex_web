
<%@page import="com.profluo.ecm.model.db.SupplierNotifications"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme"%>

<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui"%>
<%@ taglib uri="http://alloy.liferay.com/tld/aui" prefix="aui"%>
<%@ taglib uri="http://liferay.com/tld/util" prefix="liferay-util"%>

<%@page import="java.util.HashMap"%>
<%@page import="java.util.List"%>

<liferay-theme:defineObjects />
<portlet:defineObjects />

<portlet:resourceURL id="ajaxURL" var="ajaxURL"/>

<aui:layout>
	<aui:column columnWidth="50">
		<aui:column columnWidth="25"><label for="<portlet:namespace />keyWord" style="padding-top: 5px">Cuvant cheie : </label></aui:column>
		<aui:column columnWidth="35"><aui:input id="keyWord" type ="text" name="keyWord" label="" style="padding-top:5px;"/></aui:column>
		<aui:column columnWidth="30"><a class="btn btn-primary" href="#" id="cauta"> Cauta </a></aui:column>
	</aui:column>
	<aui:column first="true" columnWidth="100">
		<div id="notificari_furnizori"></div>
	</aui:column>
</aui:layout>

<%
	String supplierName = "";
	String notificationType = "";
	String keyWord = "";
	// retrieve all blocked suppliers
	List<HashMap<String,Object>> allSuppliers = SupplierNotifications.getAllNotifiedSuppliers(supplierName, notificationType, keyWord);
	String [] notificationsTypes = new String[2];
	notificationsTypes[0] = "Eroare";
	notificationsTypes[1] = "Litigiu";
%>

<script type="text/javascript">
YUI().use( 'aui-datatable', 'aui-datatype', 'datatable-sort',
		function(Y) {
			var remoteData = [
			                  <% if(allSuppliers != null){
			                	  for(int i = 0; i < allSuppliers.size(); i++){
			                	%>
			                		{
			                			id: 				<%=allSuppliers.get(i).get("id")%>,
			                			supplierName: 		'<%=allSuppliers.get(i).get("supplierName")%>',
			                			dataNotificare: 	'<%=allSuppliers.get(i).get("dataNotificare")%>',
			                			docNumber:			'<%=allSuppliers.get(i).get("docNumber")%>',
			                			dataDoc:			'<%=allSuppliers.get(i).get("dataDoc")%>',
			                			tipNotificare:		'<%=allSuppliers.get(i).get("tipNotificare")%>',
			                			detaliiNotif:		'<%=allSuppliers.get(i).get("detaliiNotif")%>'
			                		}
			                	<% if (i != (allSuppliers.size() - 1)) { out.print(","); } %>
			                	<% } }%>
			                  ];
			
			var nestedCols = [
				                 { key: 'id', className: 'hiddencol'},
				                 { key: 'supplierName', label : '<input style="width:100px;margin-bottom:0px" class="field" type="text" id="filtru_furnizor" placeholder="Nume Furnizor"></input>'},
				                 { key: 'dataNotificare', label : 'Data notificare'},
				                 { key: 'docNumber', label : 'Doc nr.'},
				                 { key: 'dataDoc', label : 'Data doc'},
				                 { key: 'tipNotificare', label : '<select id="type-filter" name="type-filter" label="" style="margin:0">'
				                 								+ '<option selected="selected" value="0">Tip Notificare</option>' +
				                 								<% for (int i = 0; i < notificationsTypes.length; i++) { %>
				                 								<% out.println("'<option value=\"" + (i+1) +"\">" + notificationsTypes[i] +"</option>' + "); %>
				                 								<% } %>
				                 								+ '</select>'},
				                 { key: 'detaliiNotif', label : 'Detalii notificare'},
				                 { key: 'actiuni', label : 'Actiuni', allowHTML:true,
				                	 formatter:  '<button id="retrimite{id}" class="butoane">Renotificare</button>'
				                 }
							]
			
			var dataTable = new Y.DataTable({
			    columns: nestedCols,
			    data: remoteData,	
			    editEvent: 'click',
			    recordType: ['id' ,'supplierName', 'dataNotificare', 'docNumber', 'dataDoc', 'tipNotificare', 'detaliiNotif', 'actiuni']
			}).render('#notificari_furnizori');
			
			dataTable.delegate('click', function (e) {
				 e.preventDefault();
		  		  var elem = e.target.get('id');
		  			retrimiteNotificare(elem.replace("retrimite",""));  
			}, '.butoane', dataTable);
			
			//Supplier filter
			$('#filtru_furnizor').change(function(e){
				var action 		 = "filter";
				var supplierName = e.target.value;
				var notificationType;
				var typeId = $('#type-filter').val();
				if(typeId == 1){
					notificationType = '13';
				} else if(typeId ==2 ){
					notificationType = '14';
				} else {
					notificationType = '';
				}
				$.ajax({
					type: "POST",
					url: "<%=ajaxURL%>",
					data: 	"<portlet:namespace />action=" + action + 
							"&<portlet:namespace />supplierName=" + supplierName +
							"&<portlet:namespace />notificationType=" + notificationType + 
							"&<portlet:namespace />keyWord=" + $('#<portlet:namespace/>keyWord').val(),
					success: function(msg) {
						// get table data
						if (msg != ""){
							var jsonEntries = jQuery.parseJSON(msg);
							dataTable.set('data', eval(jsonEntries.values));
						} else {
							dataTable.set('data', []);
						}
					}
				});
			});
			
			//notification type filter
			$('#type-filter').change(function(e){
				var action 		 = "filter";
				var typeId = e.target.value;
				var notificationType;
				var supplierName = $('#filtru_furnizor').val();
				if(!supplierName) {
					supplierName = '';
				}
				if(typeId == 1){
					notificationType = '13';
				} else if(typeId ==2 ){
					notificationType = '14';
				} else {
					notificationType = '';
				}
				$.ajax({
					type: "POST",
					url: "<%=ajaxURL%>",
					data: 	"<portlet:namespace />action=" + action + 
							"&<portlet:namespace />supplierName=" + supplierName +
							"&<portlet:namespace />notificationType=" + notificationType +
							"&<portlet:namespace />keyWord=" + $('#<portlet:namespace/>keyWord').val(),
					success: function(msg) {
						// get table data
						if (msg != ""){
							var jsonEntries = jQuery.parseJSON(msg);
							dataTable.set('data', eval(jsonEntries.values));
						} else {
							dataTable.set('data', []);
						}
					}
				});
			});
			
			$('#cauta').click(function(e){
				var action = "filter";
				var supplierName = $('#<portlet:namespace/>filtru_furnizor').val();
				if(!supplierName){
					supplierName = '';
				}
				var keyWord = $('#<portlet:namespace/>keyWord').val();
				if(!keyWord) {
					keyWord = '';
				}
				$.ajax({
					type: "POST",
					url: "<%=ajaxURL%>",
					data: 	"<portlet:namespace />action=" + action + 
							"&<portlet:namespace />supplierName=" + supplierName +
							"&<portlet:namespace />notificationType=" + '' +
							"&<portlet:namespace />keyWord=" + keyWord,
					success: function(msg) {
						// get table data
						if (msg != ""){
							var jsonEntries = jQuery.parseJSON(msg);
							dataTable.set('data', eval(jsonEntries.values));
						} else {
							dataTable.set('data', []);
						}
					}
				});
			});
			
		});
		
		function retrimiteNotificare(idNotificare){
			$.ajax({
				type: "POST",
				url: "<%=ajaxURL%>",
				data: 	"<portlet:namespace />action=" + "resendNotification" + 
						"&<portlet:namespace />notificationId=" + idNotificare,
				success: function(msg) {
					// get table data
					if (msg.length > 10){
						alert("Notificare a fost retrimisa cu succes.")
					} else {
						alert("Notificare nu a putut fi retrimisa. Va rugam reincercati.")
					}
				}
			});
		}
		
</script>