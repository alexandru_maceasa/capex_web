<%@page import="java.util.HashMap"%>
<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.profluo.ecm.model.db.InvoiceHeader"%>
<%@page import="com.profluo.ecm.model.vo.DefCompaniesVo"%>

<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme"%>

<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui"%>
<%@ taglib uri="http://alloy.liferay.com/tld/aui" prefix="aui"%>
<%@ taglib uri="http://liferay.com/tld/util" prefix="liferay-util"%>
 
<liferay-theme:defineObjects />
<portlet:defineObjects />
<portlet:resourceURL id="ajaxURL" var="ajaxURL"/>
<portlet:renderURL var="viewInvoiceURL" >
	<portlet:param name="mvcPath" value="/html/includes/visualizations/vizualizare_factura.jsp"></portlet:param>
</portlet:renderURL>

<%
	int start 			= 0 ;
	int count 			= 15;
	int total 			= 0;
	String nrFactura 	= "";
	String supplierName = "";
	int companyId 		= 0;
	
	List<HashMap<String,Object>> allWrongInvoices = InvoiceHeader.getAllWrongInvoices(start, count, nrFactura, supplierName, companyId);
	total = 0;
	List<HashMap<String,Object>> allCompanies = DefCompaniesVo.getInstance();
%>

<script type="text/javascript">
	var start 			= <%=start%>;
	var count	 		= <%=count%>;
	var total 			= <%=total%>;
	var nrFactura 		= '<%=nrFactura%>';
	var supplierName 	= '<%=supplierName%>';
	var companyId 		= <%=companyId%>;
</script>

<aui:layout>
	<aui:column first="true" columnWidth="100" last="true">
		<div id="listing-facturi"></div>
	</aui:column>
</aui:layout>

<%-- HTML for PAGINATION --%>
<%@ include file="/html/includes/pagination.jsp" %>

<script type="text/javascript">
YUI({lang: 'ro'}).use('aui-datepicker', 'aui-datatable', 'aui-datatype', 'datatable-sort', function(Y) {
	var remoteData = [
	                  <% for(int i = 0; i < allWrongInvoices.size(); i++) { %>
	                  {
	            			id: 					<%=allWrongInvoices.get(i).get("id")%>,
	            			inv_number: 			'<%=allWrongInvoices.get(i).get("inv_number")%>',
	            			created: 				'<%=allWrongInvoices.get(i).get("created")%>',
	            			supplier_code: 			'<%=allWrongInvoices.get(i).get("supplier_code")%>',
	            			supplier_name: 			'<%=allWrongInvoices.get(i).get("supplier_name")%>',
	            			inv_date: 				'<%=allWrongInvoices.get(i).get("inv_date")%>',
	            			pay_date: 				'<%=allWrongInvoices.get(i).get("pay_date")%>', 
	            			total_with_vat_curr: 	<%=allWrongInvoices.get(i).get("total_with_vat_curr")%>,
	            			currency: 				'<%=allWrongInvoices.get(i).get("currency")%>',
	            			company_name: 			'<%=allWrongInvoices.get(i).get("company_name")%>'
	            		}
	                  <% if (i != (allWrongInvoices.size() - 1)) { out.print(","); }
	                  } %>
	                  ];
	
	function formatCurrency(cell) {
	    format = {
	    	thousandsSeparator: ",",
	        decimalSeparator: ".",
	        decimalPlaces: noOfDecimalsToDisplay
	    };
	    return Y.DataType.Number.format(Number(cell.value), format);
	}
	
	var nestedCols = [
	        		    { key: 'id', className: 'hiddencol'},
	        			{ key: 'inv_number', 
	        		    	label: '<input class="field" style="width:60px !important;margin:0" type="text" value="" id="filter-nr" name="filter-nr" placeholder="Nr."></input>' },
	        			{ key: 'created', label: 'Data import'},
	        			{ key: 'supplier_code', label: 'Cod furnizor' },
	        			{ key: 'supplier_name', 
	        				label: '<input class="field" type="text" value="" id="filter-supplier_name" name="filter-supplier_code" placeholder="Furnizor" style="margin:0"/>'
	        			},
	        			{ key: 'inv_date', label:  '<div id="filter-dataf"></div>'},
	        			{ key: 'pay_date', label:  'Data Scadenta'},
	        			{ key: 'total_with_vat_curr', label: 'Val cu TVA', formatter: formatCurrency, className: 'currencyCol'},
	        			{ key: 'currency', label: 'Moneda' },
	        			{ key: 'company_name', label: '<select id="filter-societate" name="filter-societate" label="" style="width:100px;margin:0">' +
	        				'<option selected="selected" value="0">Societate</option>' +
	        				<% for (int i = 0; i < allCompanies.size(); i++) { %>
	        				<% if (allCompanies.get(i).get("status").equals("A")) {%>
	        				<% out.println("'<option value=\"" + allCompanies.get(i).get("id") +"\">" + allCompanies.get(i).get("name") +"</option>' + "); %>
	        				<% } } %>
	        				'</select>'},
	        			{ key: 'actiuni', label: 'Actiuni', allowHTML: true, formatter: '<a href="#" id="row{id}" class="editrow">Vizualizare</a>'}
	        		];
	
	var dataTable = new Y.DataTable({
	    columns: nestedCols,
	    data: remoteData,
  		scrollable: "x",
        width: "100%",
		    recordType: ['id', 'inv_number', 'created', 'supplier_code', 'supplier_name', 'inv_date', 'pay_date', 
		                 'total_with_vat_curr', 'currency', 'company_name'],
		    editEvent: 'click'
		}).render('#listing-facturi');
	
	// PAGINATION
	// set status of navigation buttons on page load
	setNavigationButtonsState('<portlet:namespace />');
	$('.navigation').click(function(e) {
		var elementT 	= e.target;
		var elementId 	= elementT.id;
		var action 		= "";
		
		// determine which button was pressed and set the value of start accordingly
		if (elementId.indexOf("first") > -1)	{ action = "first"; start = 0; }
		if (elementId.indexOf("prev") > -1) 	{ action = "prev"; if (start - count <= 0) {start = 0} else { start -= count; } }
		if (elementId.indexOf("next") > -1) 	{ action = "next"; start = parseInt(start) + parseInt(count); }
		if (elementId.indexOf("last") > -1) 	{ action = "last"; start = parseInt(parseInt(total) / parseInt(count)) * count; if (start == total) { start -= count; } }
		
		makeAjaxCall(action);
	});
	
	
	$('#filter-nr').keyup(function (e) {
	    if ($("#filter-nr").is(":focus") && (e.keyCode == 13)) {
			var action 		= "filter";
			nrFactura = e.target.value;
			supplierName = $('#filter-supplier_name').val();
			companyId = $('#filter-societate').val();
			start = 0;
			total = 0;
			makeAjaxCall(action);				
	    }
	});
	
	$('#filter-supplier_name').keyup(function (e) {
	    if ($("#filter-supplier_name").is(":focus") && (e.keyCode == 13)) {
			var action 		= "filter";
			supplierName = e.target.value;
			nrFactura = $('#filter-nr').val();
			companyId = $('#filter-societate').val();
			start = 0;
			total = 0;
			makeAjaxCall(action);				
	    }
	});
	
	$('#filter-societate').change(function (e) {
		var action 		= "filter";
		companyId = e.target.value;
		nrFactura = $('#filter-nr').val();
		supplierName = $('#filter-supplier_name').val();
		start = 0;
		total = 0;
		makeAjaxCall(action);
	});
	
	
	function makeAjaxCall(action) {
		$.ajax({
			type: "POST",
			url: "<%=ajaxURL%>",
			data: 	"<portlet:namespace />action=" + action + 
					"&<portlet:namespace />start=" + start + 
					"&<portlet:namespace />count=" + count +
					"&<portlet:namespace />total=" + total +
					"&<portlet:namespace />invoiceNo=" + nrFactura +
					"&<portlet:namespace />supplierName=" + supplierName +
					"&<portlet:namespace />companyId=" + companyId,
			success: function(msg) {
				if (msg != ""){
					var jsonEntries = jQuery.parseJSON(msg);
					total = jsonEntries.total;
					dataTable.set('data', eval(jsonEntries.values));
				} else {
					dataTable.set('data', []);
				}
				setNavigationButtonsState('<portlet:namespace />');
			}
		});
	}
	
	dataTable.delegate('click', function (e) {
	    e.preventDefault();
	    var elem = e.target.get('id');
	    var invoiceId = elem.replace('row',"");
	    $('#<portlet:namespace/>invoiceId').val(invoiceId);
	    $('#<portlet:namespace/>viewInvoice').submit();
	},'.editrow', dataTable);
	
});
</script>

<aui:form method="post" action="<%=viewInvoiceURL.toString() %>" name="viewInvoice" id="viewInvoice" style="margin:0;padding:0">
	<aui:input type="hidden" id="invoiceId" name="invoiceId"/>
</aui:form>