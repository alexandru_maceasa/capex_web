<%@page import="com.profluo.ecm.model.db.DefSupplier"%>
<%@page import="com.profluo.ecm.model.db.DefStore"%>
<%@page import="com.profluo.ec.carrefour.htmlutils.DropDownUtils"%>
<%@page import="com.profluo.ecm.model.db.ListaInit"%>
<%@page import="com.profluo.ecm.model.db.DefNewProj"%>
<%@page import="com.profluo.ecm.model.db.InvoiceHeader"%>
<%@page import="com.profluo.ecm.controller.UserTeamIdUtils"%>
<%@page import="com.profluo.ecm.model.db.DefCompanies"%>
<%@page import="com.profluo.ecm.model.db.InventoryHeader"%>
<%@taglib uri="http://java.sun.com/portlet_2_0" 	prefix="portlet"%>
<%@taglib uri="http://liferay.com/tld/theme" 		prefix="liferay-theme"%>
<%@taglib uri="http://liferay.com/tld/ui" 			prefix="liferay-ui"%>
<%@taglib uri="http://alloy.liferay.com/tld/aui" 	prefix="aui"%>
<%@taglib uri="http://liferay.com/tld/util" 		prefix="liferay-util"%>

<%@page import="java.util.HashMap"%>
<%@page import="java.util.List"%>

<liferay-theme:defineObjects />
<portlet:defineObjects />

<%
	List<HashMap<String,Object>> allCompanies = DefCompanies.getAll();
	List<HashMap<String,Object>> allSuppliers = DefSupplier.getAll();
	List<HashMap<String,Object>> allStores 	  = DefStore.getFullList();
	int start = 0; 
	int count = 10;
	List<HashMap<String,Object>> allInvLines = InvoiceHeader.getInvoicesLinesLitigii(start, count, "", "", "", 0, 0, 0, 0);
	int total = InvoiceHeader.getInvoicesLinesLitigiiCount("", "", "", 0, 0, 0, 0);
%>
<portlet:resourceURL id="ajaxURL" var="ajaxURL"/>

<portlet:resourceURL id="exportRaport" var="exportRaportURL">
	<portlet:param name="action" value="exportRaport"/>
</portlet:resourceURL>

<aui:layout>
	<aui:column columnWidth="15" first="true">
		<label for="<portlet:namespace />societate" style="padding-top: 5px">Societatea</label>
	</aui:column>
	<aui:column>
		<aui:select id="societate" name="societate" label="" style="width:200px;">
			<aui:option selected="selected" value="0"> ----------- </aui:option>
			<% for (int i = 0 ; i < allCompanies.size(); i++) { %>
				<aui:option value="<%=i+1%>"> <%=allCompanies.get(i).get("name").toString()%> </aui:option>
			<% } %>
		</aui:select>
	</aui:column>
</aui:layout>
<aui:layout>
	<aui:column columnWidth="15" first="true">
		<label for="<portlet:namespace />store" style="padding-top: 5px;">Magazin</label>
	</aui:column>
	<aui:column>
		<aui:select id="store" name="store" label="" style="width:200px;">
			<aui:option selected="selected" value="0"> Magazin </aui:option>
			<% for (int i = 0 ; i < allStores.size(); i++) { %>
				<aui:option value="<%=allStores.get(i).get(\"id\").toString()%>"> <%=allStores.get(i).get("store_id").toString()%> - <%=allStores.get(i).get("store_name").toString()%> </aui:option>
			<% } %>
		</aui:select>
	</aui:column>
</aui:layout>

<script type="text/javascript">
YUI().use('autocomplete-list', "datasource-io", 'aui-base', 'aui-io-request', 
		  'aui-form-validator', 'autocomplete-filters', 'autocomplete-highlighters', 
		  'overlay',
function (A) {
	var contactSearchDS = new A.DataSource.IO({source: '<%=ajaxURL.toString()%>'});
	var supplierData = "";
	var supplierCreated = false;
	
	var contactSearchFormatter = function (query, results) {
		return A.Array.map(results, function (result) {
			return '<strong>'+result.raw.name+'</strong><br/><b>' + result.raw.supplier_code + '</b> - CUI: ' + result.raw.cui;
		});
	};

	var testData;
	var contactSearchInput = new A.AutoCompleteList({
		allowBrowserAutocomplete: 'false',
		resultHighlighter: 'phraseMatch',
		activateFirstItem: 'true',
		resultTextLocator: 'name',
		inputNode: '#<portlet:namespace/>supplier_list',
		render: 'true',
		width: '300px',
        resultFormatter: contactSearchFormatter,
		source: function(){
			
			$('#<portlet:namespace/>id_supplier_raport').val("");
			supplierData = "";
			var inputValue = A.one("#<portlet:namespace />supplier_list").get('value');
			var myAjaxRequest = A.io.request('<%=ajaxURL.toString()%>',{
					dataType: 'json',
					method:'POST',
					data:{
						 <portlet:namespace/>action: 'findSupplier',
						 <portlet:namespace/>keywords: $('#<portlet:namespace />supplier_list').val(),
						 <portlet:namespace/>id_company: $('#<portlet:namespace />societate').val()
						},
					autoLoad: false,
					sync: true,
					on: {
					success:function(){
						var data = this.get('responseData');
						testData = data;
					}}
			});
			myAjaxRequest.start();
			return testData;
		},	
			on: {
			select: function(event) {
				// after user selects an option from the list
				var result = event.result.raw;
				// global variable to be used also in the create new supplier pop-up
				supplierData = result;
				A.one('#<portlet:namespace/>id_supplier_raport').val(result.id);
			}
		}
	});	
});
</script>
<aui:layout>
	<aui:column columnWidth="15" first="true">
		<label for="<portlet:namespace />supplier_list" style="padding-top: 5px;">Furnizor</label>
	</aui:column>
	<aui:column>
		<aui:input id="supplier_list" name="supplier_list" disabled="true" label="" style="width:200px;"></aui:input>
		<aui:input id="id_supplier_raport" name="id_supplier_raport" value="" type="hidden" label=""/>
	</aui:column>
</aui:layout>
<aui:layout>
	<aui:column columnWidth="15" first="true">
		<label style="padding-top:5px" for="start_date">Data contabila inceput: </label>
	</aui:column>
	<aui:column columnWidth="10"  style="text-align:right">
		<aui:input label="" name="start_date_an" id="start_date_an" type="text" value="" style="width:90%" placeholder="An contabil">
		</aui:input>
	</aui:column>
	<aui:column columnWidth="15"  style="text-align:right">
		<aui:select id="start_date_luna" name="start_date_luna" label="" style="width:100%" required="true">
				<aui:option selected="selected" value="0">Luna contabil</aui:option>
				<aui:option value="1">Ianuarie</aui:option>
				<aui:option value="2">Februarie</aui:option>
				<aui:option value="3">Martie</aui:option>
				<aui:option value="4">Aprilie</aui:option>
				<aui:option value="5">Mai</aui:option>
				<aui:option value="6">Iunie</aui:option>
				<aui:option value="7">Iulie</aui:option>
				<aui:option value="8">August</aui:option>
				<aui:option value="9">Septembrie</aui:option>
				<aui:option value="10">Octombrie</aui:option>
				<aui:option value="11">Noiembrie</aui:option>
				<aui:option value="12">Decembrie</aui:option>
			</aui:select>
	</aui:column>
</aui:layout>
<aui:layout>
	<aui:column columnWidth="15" first="true">
		<label style="padding-top:5px" for="end_date">Data contabila sfarsit: </label>
	</aui:column>
	<aui:column columnWidth="10"  style="text-align:right">
		<aui:input label="" name="end_date_an" id="end_date_an" type="text" value="" style="width:90%" placeholder="An contabil">
		</aui:input>
	</aui:column>
	<aui:column columnWidth="15"  style="text-align:right">
		<aui:select id="end_date_luna" name="end_date_luna" label="" style="width:100%" required="true">
				<aui:option selected="selected" value="0">Luna contabil</aui:option>
				<aui:option value="1">Ianuarie</aui:option>
				<aui:option value="2">Februarie</aui:option>
				<aui:option value="3">Martie</aui:option>
				<aui:option value="4">Aprilie</aui:option>
				<aui:option value="5">Mai</aui:option>
				<aui:option value="6">Iunie</aui:option>
				<aui:option value="7">Iulie</aui:option>
				<aui:option value="8">August</aui:option>
				<aui:option value="9">Septembrie</aui:option>
				<aui:option value="10">Octombrie</aui:option>
				<aui:option value="11">Noiembrie</aui:option>
				<aui:option value="12">Decembrie</aui:option>
			</aui:select>
	</aui:column>
</aui:layout>
<aui:layout>
	<aui:column first="true">
		<aui:column>
			<a class="btn btn-primary" onclick="filterListing('<portlet:namespace />')"> Aplica Filtre </a>
		</aui:column>
		<aui:column>
			<a class="btn btn-primary" onclick="exportRaport('<portlet:namespace />')"> Export Raport </a>
		</aui:column>
	</aui:column>
</aui:layout>
<br/>
<aui:layout>
	<aui:column first="true" columnWidth="100" last="true">
		<div id="invoicesWithLit"></div>
	</aui:column>
</aui:layout>
<script type="text/javascript">
var start = <%=start%>
var count = <%=count%>
var total = <%=total%>
var supplier = 0;
var societate = -1;
var magazin = 0;
var dataTableIV;
var from_year = 0;
var to_year = 0;
var from_month = 0;
var to_month = 0;
</script>

<%-- HTML for PAGINATION --%>
<%@ include file="/html/includes/pagination.jsp" %>

<script type="text/javascript">

$('#<portlet:namespace/>societate').change(function(e){
	if($('#<portlet:namespace/>societate').val() != '' && $('#<portlet:namespace/>societate').val() != 0 ) {
		$('#<portlet:namespace/>supplier_list').prop("disabled", false);
	} else {
		$('#<portlet:namespace/>supplier_list').prop("disabled", true);
	}
});

$('#<portlet:namespace/>supplier_list').click(function(e) {
	var selectedCo = $('#<portlet:namespace/>societate').val();
	if (selectedCo == '0'){
		alert("Selectati societatea!");
		return;
	}
});

$(document).ready(function(e){
	$('#<portlet:namespace/>store').chosen();
});


function filterListing(portletId){
	societate = $('#<portlet:namespace/>societate').val();
	magazin = $('#<portlet:namespace/>store').val();
	supplier = $('#<portlet:namespace/>id_supplier_raport').val();
	from_year = $('#<portlet:namespace/>start_date_an').val();
	to_year = $('#<portlet:namespace/>end_date_an').val();
	from_month = $('#<portlet:namespace/>start_date_luna').val();
	to_month = $('#<portlet:namespace/>end_date_luna').val();
	if(from_year == '') {
		from_year = 0;
	}
	if(to_year == '') {
		to_year = 0;
	}
	if(from_month == '') {
		from_month = 0;
	}
	if(to_month == '') {
		to_month = 0;
	}
	var action = "filter";
	makeAjaxCall(action);
}

function exportRaport(portletId){
	societate = $('#<portlet:namespace/>societate').val();
	magazin = $('#<portlet:namespace/>store').val();
	supplier = $('#<portlet:namespace/>id_supplier_raport').val();
	from_year = $('#<portlet:namespace/>start_date_an').val();
	to_year = $('#<portlet:namespace/>end_date_an').val();
	from_month = $('#<portlet:namespace/>start_date_luna').val();
	to_month = $('#<portlet:namespace/>end_date_luna').val();
	if(from_year == '') {
		from_year = 0;
	}
	if(to_year == '') {
		to_year = 0;
	}
	if(from_month == '') {
		from_month = 0;
	}
	if(to_month == '') {
		to_month = 0;
	}
	$('#<portlet:namespace/>company_id').val(societate);
	$('#<portlet:namespace/>id_magazin').val(magazin);
	$('#<portlet:namespace/>id_furnizor').val(supplier);
	$('#<portlet:namespace/>from_year').val(from_year);
	$('#<portlet:namespace/>to_year').val(to_year);
	$('#<portlet:namespace/>from_month').val(from_month);
	$('#<portlet:namespace/>to_month').val(to_month);
	$('#<portlet:namespace/>exportFormRaport').submit();
	
}

YUI({ lang: 'ro' }).use('aui-datatable','aui-datatype','datatable-sort', 'aui-datepicker', 'aui-base', 'liferay-portlet-url', 'aui-node',
		function(Y){
		var remoteData = [
	      			<% for (int i = 0; i < allInvLines.size(); i++) { %>
	      				{
	      					societate: 			'<%=allInvLines.get(i).get("societate")%>',
	      					data_import: 		'<%=allInvLines.get(i).get("data_import")%>',
	      					nr: 				'<%=allInvLines.get(i).get("nr")%>',
	      					inv_date: 			'<%=allInvLines.get(i).get("inv_date")%>',
	      					codFurnizor: 		'<%=allInvLines.get(i).get("codFurnizor")%>',
	      					furnizor: 			'<%=allInvLines.get(i).get("furnizor")%>',
	      					magazin: 			'<%=allInvLines.get(i).get("magazin")%>',
	      					cod_prod: 			'<%=allInvLines.get(i).get("cod_prod")%>',
	      					description: 		'<%=allInvLines.get(i).get("description")%>',
	      					quantity: 			'<%=allInvLines.get(i).get("quantity")%>',
	      					unit_price_ron: 	'<%=allInvLines.get(i).get("unit_price_ron")%>',
	      					val_no_vat_ron: 	'<%=allInvLines.get(i).get("val_no_vat_ron")%>',
	      					unit_price_curr: 	'<%=allInvLines.get(i).get("unit_price_curr")%>',
	      					val_no_vat_curr: 	'<%=allInvLines.get(i).get("val_no_vat_curr")%>',
	      					detalii_litigii: 	'<%=allInvLines.get(i).get("detalii_litigii")%>'
	      				}
	      				<% if (i != (allInvLines.size() - 1)) { out.print(","); } %>
	      			<% } %>
	      		];
		var nestedColumns = [		
							{key: 'societate', 			label:'Societate'},
							{key: 'data_import', 		label:'Data import'},
							{key: 'nr', 				label:'Factura nr.'},
							{key: 'inv_date', 			label:'Data factura'},
							{key: 'codFurnizor', 		label:'Cod furnizor'},
		         			{key: 'furnizor', 			label:'Furnizor'},
							{key: 'magazin', 			label:'Denumire magazin'},
							{key: 'cod_prod', 			label:'Cod produs'},
							{key: 'description', 		label:'Denumire produs'},
							{key: 'quantity', 			label:'Cantitate'},
							{key: 'unit_price_ron', 	label: 'Pret unitar factura RON'},
		         			{key: 'val_no_vat_ron', 	label:'Val fara TVA factura RON'},
		         			{key: 'unit_price_curr', 	label: 'Pret unitar factura EUR'},
		         			{key: 'val_no_vat_curr', 	label:'Val fara TVA factura EUR'},
		         			{key: 'detalii_litigii', 	label:'Detalii litigii'}
		         		];
		dataTableIV = new Y.DataTable({
			columns:nestedColumns,
			data:remoteData,
			scrollable: "x",
	        width: "100%",
			recordType:['societate', 'data_import', 'nr', 'inv_date', 'codFurnizor', 'furnizor', 'magazin', 'cod_prod', 'description', 'quantity', 'unit_price_ron',
			            'val_no_vat_ron', 'unit_price_curr', 'val_no_vat_curr', 'detalii_litigii'],
			editEvent:'click'
		}).render('#invoicesWithLit');
		
		// PAGINATION
		// set status of navigation buttons on page load
		setNavigationButtonsState('<portlet:namespace />');
		$('.navigation').click(function(e) {
			var elementT 	= e.target;
			var elementId 	= elementT.id;
			var action 		= "";
			
			// determine which button was pressed and set the value of start accordingly
			if (elementId.indexOf("first")> -1)	{ action = "first"; start = 0; }
			if (elementId.indexOf("prev") > -1) 	{ action = "prev"; if (start - count <= 0) {start = 0} else { start -= count; } }
			if (elementId.indexOf("next") > -1) 	{ action = "next"; start = parseInt(start) + parseInt(count); }
			if (elementId.indexOf("last") > -1) 	{ action = "last"; start = parseInt(parseInt(total) / parseInt(count)) * count; if (start == total) { start -= count; } }
			
			makeAjaxCall(action);
		});
		
});

function makeAjaxCall(action) {
	$.ajax({
		type: "POST",
		url: "<%=ajaxURL%>",
		data: 	"<portlet:namespace />action=" + action + 
				"&<portlet:namespace />start=" + start + 
				"&<portlet:namespace />count=" + count +
				"&<portlet:namespace />total=" + total +
				"&<portlet:namespace />societate=" + societate +
				"&<portlet:namespace />supplier=" + supplier +
				"&<portlet:namespace />magazin=" + magazin +
				"&<portlet:namespace />from_year=" + from_year +
				"&<portlet:namespace />to_year=" + to_year +
				"&<portlet:namespace />from_month=" + from_month +
				"&<portlet:namespace />to_month=" + to_month,
		success: function(msg) {
			// get table data
			if (msg != ""){
				var jsonEntries = jQuery.parseJSON(msg);
				total = jsonEntries.total;
				dataTableIV.set('data', eval(jsonEntries.values));
			} else {
				dataTableIV.set('data', []);
			}
			// set status of navigation buttons on page load
			setNavigationButtonsState('<portlet:namespace />');
		}
	});
}
</script>
<aui:form id="exportFormRaport" name="exportFormRaport" style="dysplay:none"  action="<%=exportRaportURL.toString()%>">
	<aui:input type="hidden" id="company_id" name="company_id"/>
	<aui:input type="hidden" id="id_magazin" name="id_magazin"/>
	<aui:input type="hidden" id="id_furnizor" name="id_furnizor"/>
	<aui:input type="hidden" id="from_year" name="from_year"/>
	<aui:input type="hidden" id="to_year" name="to_year"/>
	<aui:input type="hidden" id="from_month" name="from_month"/>
	<aui:input type="hidden" id="to_month" name="to_month"/>
</aui:form>