<%@page import="com.profluo.ecm.model.db.VoucherHeader"%>
<%@taglib uri="http://java.sun.com/portlet_2_0" 	prefix="portlet"%>
<%@taglib uri="http://liferay.com/tld/theme" 		prefix="liferay-theme"%>
<%@taglib uri="http://liferay.com/tld/ui" 			prefix="liferay-ui"%>
<%@taglib uri="http://alloy.liferay.com/tld/aui" 	prefix="aui"%>
<%@taglib uri="http://liferay.com/tld/util" 		prefix="liferay-util"%>

<%@page import="java.util.HashMap"%>
<%@page import="java.util.List"%>

<liferay-theme:defineObjects />
<portlet:defineObjects />

<%
	int start = 0; 
	int count = 15;
	int total = 0;
	String inventoryNo = "";
	String voucherNo = "";
	String storeCode = "";
	List<HashMap<String,Object>> allRegisteredVouchers = VoucherHeader.getAllRegisteredVouchers(start, count, inventoryNo, voucherNo, storeCode);
	total = VoucherHeader.getAllRegisteredVouchersCount(inventoryNo, voucherNo, storeCode);
%>

<portlet:resourceURL id="ajaxURL" var="ajaxURL"/>

<portlet:resourceURL id="exportAction" var="exportURL">
	<portlet:param name="action" value="exportExcel"/>
</portlet:resourceURL>

<aui:form id="exportForm" name="exportForm" action="<%=exportURL.toString()%>">
<aui:layout>
	<aui:column columnWidth="10" first="true">
		<label style="padding-top:5px" for="start_date">Data contabila inceput: </label>
	</aui:column>
	<aui:column columnWidth="10"  style="text-align:right">
		<aui:input label="" name="start_date_an" id="start_date_an" type="text" value="" style="width:90%" placeholder="An contabil">
			 <aui:validator name="required" errorMessage="Campul este obligatoriu." />
		</aui:input>
	</aui:column>
	<aui:column columnWidth="10"  style="text-align:right">
		<aui:select id="start_date_luna" name="start_date_luna" label="" style="width:95%" required="true">
				<aui:option selected="selected" value="0">Luna contabil</aui:option>
				<aui:option value="1">Ianuarie</aui:option>
				<aui:option value="2">Februarie</aui:option>
				<aui:option value="3">Martie</aui:option>
				<aui:option value="4">Aprilie</aui:option>
				<aui:option value="5">Mai</aui:option>
				<aui:option value="6">Iunie</aui:option>
				<aui:option value="7">Iulie</aui:option>
				<aui:option value="8">August</aui:option>
				<aui:option value="9">Septembrie</aui:option>
				<aui:option value="10">Octombrie</aui:option>
				<aui:option value="11">Noiembrie</aui:option>
				<aui:option value="12">Decembrie</aui:option>
			</aui:select>
	</aui:column>
</aui:layout>
<aui:layout>
	<aui:column columnWidth="10" first="true">
		<label style="padding-top:5px" for="end_date">Data contabila sfarsit: </label>
	</aui:column>
	<aui:column columnWidth="10"  style="text-align:right">
		<aui:input label="" name="end_date_an" id="end_date_an" type="text" value="" style="width:90%" placeholder="An contabil">
			 <aui:validator name="required" errorMessage="Campul este obligatoriu." />
		</aui:input>
	</aui:column>
	<aui:column columnWidth="10"  style="text-align:right">
		<aui:select id="end_date_luna" name="end_date_luna" label="" style="width:95%" required="true">
				<aui:option selected="selected" value="0">Luna contabil</aui:option>
				<aui:option value="1">Ianuarie</aui:option>
				<aui:option value="2">Februarie</aui:option>
				<aui:option value="3">Martie</aui:option>
				<aui:option value="4">Aprilie</aui:option>
				<aui:option value="5">Mai</aui:option>
				<aui:option value="6">Iunie</aui:option>
				<aui:option value="7">Iulie</aui:option>
				<aui:option value="8">August</aui:option>
				<aui:option value="9">Septembrie</aui:option>
				<aui:option value="10">Octombrie</aui:option>
				<aui:option value="11">Noiembrie</aui:option>
				<aui:option value="12">Decembrie</aui:option>
			</aui:select>
	</aui:column>
</aui:layout>
<aui:layout>
	<aui:column columnWidth="50" first="true" style="text-align:left">
		<a class="btn btn-primary" onclick="exportFacturi('<portlet:namespace />')"> Export Excel </a>
	</aui:column>
</aui:layout>
</aui:form>
<aui:layout>
	<aui:column first="true" columnWidth="100" last="true">
		<div id="reg_vouchers"></div>
	</aui:column>
</aui:layout>

<script type="text/javascript">
var start = <%=start%>
var count = <%=count%>
var total = <%=total%>
var inventoryNo = '<%=inventoryNo%>'
var voucherNo = '<%=voucherNo%>'
var storeCode = '<%=storeCode%>'


function exportFacturi(portletId){
	
	start_date_an = $('#<portlet:namespace />start_date_an').val();
	start_date_luna = $('#<portlet:namespace />start_date_luna').val();
	end_date_an = $('#<portlet:namespace />end_date_an').val();
	end_date_luna = $('#<portlet:namespace />end_date_luna').val();
	
	if ( (2015 > start_date_an) ||(start_date_an > 2020) || (2015 > end_date_an) ||(end_date_an > 2020) ){
		alert("Va rugam sa introduceti un an valid");
		return;
	}
	if ( (1 > start_date_luna) ||(start_date_luna > 12) || (1 > end_date_luna) ||(end_date_luna > 12) ){
		alert("Va rugam sa selectati luna");
		return;
	}
$('#'+portletId+'exportForm').submit();
}
</script>
<%-- HTML for PAGINATION --%>
<%@ include file="/html/includes/pagination.jsp" %>

<script type="text/javascript">

YUI({ lang: 'ro' }).use('aui-datatable','aui-datatype','datatable-sort', 'aui-datepicker', 'aui-base', 'liferay-portlet-url', 'aui-node',
	function(Y){
		var remoteData = [
						<% for (int i = 0; i < allRegisteredVouchers.size(); i++) { %>
							{
	      					nrInventar: <%=allRegisteredVouchers.get(i).get("nrInventar")%>,
	      					dataPIF: '<%=allRegisteredVouchers.get(i).get("dataPIF")%>', 
	      					denumire : '<%=allRegisteredVouchers.get(i).get("denumire")%>',
	      					nrVoucher: '<%=allRegisteredVouchers.get(i).get("nrVoucher")%>',
	      					valoare: '<%=allRegisteredVouchers.get(i).get("valoare")%>',
	      					dataVoucher:'<%=allRegisteredVouchers.get(i).get("dataVoucher")%>',
	      					codSocietate: '<%=allRegisteredVouchers.get(i).get("codSocietate")%>',
	      					codMagazin:'<%=allRegisteredVouchers.get(i).get("codMagazin")%>'
	      					}
							<% if (i != (allRegisteredVouchers.size() - 1)) { out.print(","); } %>
						<% } %>
		                  ];
		var nestedColumns = [		
			         			{key: 'nrInventar', label: '<input id="nr_inventar" name="nr_inventar" placeholder="Numar inventar" style="width:100%"', allowHTML:true,resizable:true}, 
			         			{key: 'dataPIF', label:'Data PIF'},
			         	        {key: 'denumire', label:'Denumire'},
			         	        {key: 'nrVoucher', label: '<input id="voucher_no" name="voucher_no" placeholder="Numar bun cesiune" style="width:100%"/>', allowHTML:true,resizable:true},
			         	        {key: 'valoare', label:'Val fara TVA'},
			         	        {key:'dataVoucher',label:'Data bon cesiune'},
			         	        {key:'codSocietate', label :'Cod societate'},
			         	        {key:'codMagazin', label:'<input id="cod_magazin" name ="cod_magazin" placeholder="Cod magazin" style="width:80%"/>', allowHTML:true,resizable:true}
			         		];
		var dataTable = new Y.DataTable({
			columns:nestedColumns,
			data:remoteData,
			recordType:['nrInventar', 'dataPIF', 'denumire', 'nrVoucher', 'dataVoucher', 'codSocietate', 'codMagazin', 'valoare'],
			editEvent:'click'
		}).render('#reg_vouchers');
		
		// PAGINATION
		// set status of navigation buttons on page load
		setNavigationButtonsState('<portlet:namespace />');
		$('.navigation').click(function(e) {
			var elementT 	= e.target;
			var elementId 	= elementT.id;
			var action 		= "";
			
			// determine which button was pressed and set the value of start accordingly
			if (elementId.indexOf("first") > -1)	{ action = "first"; start = 0; }
			if (elementId.indexOf("prev") > -1) 	{ action = "prev"; if (start - count <= 0) {start = 0} else { start -= count; } }
			if (elementId.indexOf("next") > -1) 	{ action = "next"; start = parseInt(start) + parseInt(count); }
			if (elementId.indexOf("last") > -1) 	{ action = "last"; start = parseInt(parseInt(total) / parseInt(count)) * count; if (start == total) { start -= count; } }
			
			makeAjaxCall(action);
		});
		function makeAjaxCall(action) {
			//console.log(inventoryNo + ' -> ' + voucherNo + ' -> ' + storeCode);
			$.ajax({
				type: "POST",
				url: "<%=ajaxURL%>",
				data: 	"<portlet:namespace />action=" + action + 
						"&<portlet:namespace />start=" + start + 
						"&<portlet:namespace />count=" + count +
						"&<portlet:namespace />total=" + total +
						"&<portlet:namespace />inventoryNo=" + inventoryNo +
						"&<portlet:namespace />voucherNo=" + voucherNo +
						"&<portlet:namespace />codMagazin=" + storeCode,
				success: function(msg) {
					// get table data
					if (msg != ""){
						var jsonEntries = jQuery.parseJSON(msg);
						total = jsonEntries.total;
						dataTable.set('data', eval(jsonEntries.values));
					} else {
						dataTable.set('data', []);
					}
					
					// set status of navigation buttons on page load
					setNavigationButtonsState('<portlet:namespace />');
				}
			});
		}
		
		$('#nr_inventar').keyup(function (e) {
		    if ($("#nr_inventar").is(":focus") && (e.keyCode == 13)) {
				var action 		= "filter";
				inventoryNo = e.target.value;
				voucherNo = $('#voucher_no').val();
				storeCode = $('#cod_magazin').val();
				start = 0;
				total = 0;
				makeAjaxCall(action);				
		    }
		});
		
		$('#voucher_no').keyup(function (e) {
		    if ($("#voucher_no").is(":focus") && (e.keyCode == 13)) {
				var action 		= "filter";
				voucherNo = e.target.value;
				inventoryNo = $('#nr_inventar').val();
				storeCode = $('#cod_magazin').val();
				start = 0;
				total = 0;
				makeAjaxCall(action);				
		    }
		});
		
		$('#cod_magazin').keyup(function (e) {
		    if ($("#cod_magazin").is(":focus") && (e.keyCode == 13)) {
				var action 		= "filter";
				storeCode = e.target.value;
				inventoryNo = $('#nr_inventar').val();
				voucherNo = $('#voucher_no').val();
				start = 0;
				total = 0;
				makeAjaxCall(action);				
		    }
		});
});
</script>