<%@page import="com.profluo.ecm.model.vo.DefCurrencyVo"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme" %>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>
<%@ taglib uri="http://alloy.liferay.com/tld/aui" prefix="aui" %>
<%@ taglib uri="http://liferay.com/tld/util" prefix="liferay-util" %>

<%@ page import="java.util.HashMap"%>
<%@ page import="java.util.List"%>

<liferay-theme:defineObjects />
<portlet:defineObjects />
	
<%
	String id = renderRequest.getParameter("id");
	List<HashMap<String,Object>> allCurrencies = DefCurrencyVo.getInstance();
%>

<%-- Links used in the view --%>
<%-- Submit URL, mapped to the saveExchRate method in the class DefExchRateController.java --%>
<portlet:actionURL name="saveExchRate" var="submitURL" />

<portlet:renderURL var="backURL">
	<portlet:param name="mvcPath" value="/html/defexchratescontroller/view.jsp"></portlet:param>
</portlet:renderURL>

<aui:form action="<%= submitURL %>" method="post">

<aui:fieldset>
	<% if (id == null) { %>
	<legend> Adaugare rata de schimb</legend>
	<% } else { %>
	<legend> Editare rata de schimb</legend>
	<% } %>
	
	<liferay-ui:success key="update_ok" message="update_ok" />
	<liferay-ui:error key="insert_nok" message="update_nok" />
	<liferay-ui:error key="insert_nok" message="insert_nok" />
	<liferay-ui:error key="params_nok" message="params_nok" />
	<liferay-ui:error key="entry_exists" message="entry_exists" />
	
	<% if (id != null) { %>
	<aui:layout>
		<aui:column columnWidth="25" first="true">
			<label style="padding-top:5px" for="<portlet:namespace/>id">ID: </label>
		</aui:column>
		<aui:column columnWidth="25" style="text-align:right">
			<aui:input label="" name="id" id="id" type="text" value="" style="width:90%" readonly="readonly" />
		</aui:column>
	</aui:layout>
	<% } %>
	
	
	<aui:layout>
		<aui:column columnWidth="25" first="true">
				<label style="padding-top:5px" for="<portlet:namespace/>ref" >Moneda: </label>
		</aui:column>
		<aui:column columnWidth="25" style="text-align:right">
			<aui:select id="ref" name="ref" label="" required="true" style="width:95%">
				<% for (int i = 0; i < allCurrencies.size(); i++) { %>
					<% if (allCurrencies.get(i).get("status").equals("A")) {%>
						<% if (!allCurrencies.get(i).get("ref").toString().equals("RON")) { %>
							<aui:option
								value="<%=allCurrencies.get(i).get(\"ref\")%>"><%=allCurrencies.get(i).get("ref")%></aui:option>
						<% } %>
					<% } %>
				<% } %>				
			</aui:select>
		</aui:column>
	</aui:layout>

	<aui:layout>
		<aui:column columnWidth="25" first="true">
			<label style="padding-top:5px" for="<portlet:namespace/>value">Valoare: </label>
		</aui:column>
		<aui:column columnWidth="25" style="text-align:right">
			<aui:input label="" name="value" id="value" type="text" value="" style="width:90%">
				 <aui:validator name="required" errorMessage="Campul este obligatoriu." />
			</aui:input>
		</aui:column>
	</aui:layout>
	
	<aui:layout>
		<aui:column columnWidth="25" first="true">
			<label style="padding-top:5px" for="<portlet:namespace/>date_of_rate">Data curs: </label>
		</aui:column>
		<aui:column columnWidth="25" style="text-align:right">
			<aui:input id="date_of_rate" name="date_of_rate" type="text" value="" label="" style="width:90%" required="true">
			</aui:input>
		</aui:column>
	</aui:layout>
	
	<aui:layout>
		<aui:column columnWidth="25" first="true">
			<label style="padding-top:5px" for="<portlet:namespace/>type">Tip: </label>
		</aui:column>
		<aui:column columnWidth="25" style="text-align:right">
			<aui:select id="type" name="type" type="text" value="" label="" style="width:95%" required="true">
				<aui:option id="tip" name="tip" value="1">Zilnic</aui:option>
				<aui:option id="tip" name="tip" value="2">Lunar</aui:option>
				<aui:option id="tip" name="tip" value="3">Mediu</aui:option>
				<aui:option id="tip" name="tip" value="4">Media grupului</aui:option>
				<aui:option id="tip" name="tip" value="5">Lunar la vama</aui:option>
			</aui:select>
		</aui:column>
	</aui:layout>
		
	<% if (id != null) { %>		
	
	<aui:layout>
		<aui:column columnWidth="25" first="true">
			<label style="padding-top:5px" for="<portlet:namespace/>created">Adaugat la data: </label>
		</aui:column>
		<aui:column columnWidth="25" style="text-align:right">
			<aui:input label="" name="created" id="created" type="text" value="" style="width:90%"  readonly="readonly"/>
		</aui:column>
	</aui:layout>
	<% } %>
	
	<aui:layout>
		<aui:column last="true">
			<a class="btn btn-primary-red" href="<%=backURL %>">Inapoi</a>
			<aui:button value="Salveaza" cssClass="btn btn-primary" style="margin:0px" type="submit"/>
		</aui:column>
	</aui:layout>
</aui:fieldset>
</aui:form>
<script type="text/javascript">
YUI().use('aui-datepicker', function(Y) {
	new Y.DatePicker({
		trigger : '#<portlet:namespace />date_of_rate',
		mask : '%Y-%m-%d',
		popover : {
			zIndex : 1
		},
		calendar : {
			dateFormat : '%Y-%m-%d'
		}
	});
});
</script>