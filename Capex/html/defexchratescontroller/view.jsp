<%@page import="com.profluo.ecm.model.vo.DefCurrencyVo"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.List"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme" %>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>
<%@ taglib uri="http://alloy.liferay.com/tld/aui" prefix="aui" %>
<%@ taglib uri="http://liferay.com/tld/util" prefix="liferay-util" %>

<liferay-theme:defineObjects />
<portlet:defineObjects />

<portlet:resourceURL id="ajaxURL" var="ajaxURL"/>
<script type="text/javascript">
var start 		= 0;
var count 		= 15;
var total 		= 0;
//filters
var data_rata 	= "";
var moneda 		= "";
</script>
<%
	//locale = renderRequest.getLocale();
	//ResourceBundle resmain = portletConfig.getResourceBundle(locale);
	// retrieve exchange rates from the database
	//List<HashMap<String,Object>> allExchRates = DefExchRates.getAllFiltered(start, count, date, currency);
	// retrieve all currencies
	List<HashMap<String,Object>> allCurrencies = DefCurrencyVo.getInstance();
%>

<liferay-ui:success key="insert_ok" message="insert_ok" />
<liferay-ui:success key="update_ok" message="update_ok" />

<portlet:renderURL var="addEntryURL" >
	<portlet:param name="mvcPath" value="/html/defexchratescontroller/add_exch_rate.jsp"></portlet:param>
</portlet:renderURL>

<aui:layout>
	<aui:column columnWidth="100" last="true" style="text-align:right">
		<a class="btn btn-primary" href="<%=addEntryURL.toString()%>">Adauga rata de schimb</a>
	</aui:column>
</aui:layout>
 <br/>
<aui:layout>
	<aui:column first="true" columnWidth="100" last="true">
		<div id="def-rates-groupe"></div>
	</aui:column>
</aui:layout>

<%-- HTML for PAGINATION --%>
<%@ include file="/html/includes/pagination.jsp" %>

<%--  datepicker for table header --%>
<div id="filtru_datafactura_div" style="display:none">
	<aui:input type="text" id="datafactura_header" name="datafactura_header" placeholder="Data curs" value=""  label="" style="width:100px; margin:0">
	</aui:input>
</div>

<script type="text/javascript">
YUI({ lang: 'ro' }).use( 'aui-datepicker', 'aui-datatable', 'aui-datatype', 'datatable-sort', 'datasource-get', 'datasource-jsonschema', 'datatable-datasource', 
		'datatable-paginator', 'datatype-number',
	function(Y) {
		function edit(sel_id) {
		    var ml  = dataTable.data, msg = '', template = '';
		    ml.each(function (item, i) {
		        var data = item.getAttrs(['id', 'ref', 'value', 'date_of_rate', 'type']);
		        if (data.id == sel_id) {
					$('#<portlet:namespace />id').val(data.id);
					$('#<portlet:namespace />ref').val(data.ref);
					$('#<portlet:namespace />value').val(data.value);
				    $('#<portlet:namespace />date_of_rate').val(data.date_of_rate);
				    $('#<portlet:namespace />type').val(data.type);					
					$('#<portlet:namespace />created').val(data.created);						
					$('#<portlet:namespace />editRate').submit();
		        }
		    });
		}
	
		// data array
		var remoteDataExchRates = [

		];
		
		var nestedColsExchRates = [
			{ key: 'id', label: 'ID', className: 'hiddencol' },
			{ key: 'ref', label: '<select id="filter-moneda" name="filter-moneda" label="" style="width:100px;margin:0">' +
				'<option selected="selected" value="">Moneda</option>' +
				<% for (int i = 0; i < allCurrencies.size(); i++) { %>
				<% if (allCurrencies.get(i).get("status").equals("A")) {%>
				<% out.println("'<option value=\"" + allCurrencies.get(i).get("ref") +"\">" + allCurrencies.get(i).get("ref") +"</option>' + "); %>
				<% }  %>
				<% }  %>
				'</select>'},
			{ key: 'value', label: 'Valoare'},
			{ key: 'date_of_rate', label:'<div id="filter-date" name="filter-date"></div>'},
			{ key: 'type', label: 'Tip', formatter: function (o) {
			    if (o.value == 1) return "Zilnic";
			    else if (o.value == 2) return "Lunar";
			    else if (o.value == 3) return "Mediu lunar";
			    else if (o.value == 4) return "Media grupului";
			    else if (o.value == 5) return "Lunar la vama";
				}
			},
			{ key: 'actiuni', label: 'Actiuni', allowHTML: true, 
				formatter: '<a href="#" id="rowrate{id}" class="editrowrate">Editare</a>' },
		];

		// TABLE INIT
		var dataTable = new Y.DataTable({
		    columns: nestedColsExchRates,
		    data: remoteDataExchRates,
		    recordType: ['id', 'ref', 'value', 'date_of_rate', 'type' ],
            autoSync: true
		}).render('#def-rates-groupe');
		
		//dataTable.get('boundingBox').unselectable();
		
		// Also define a listener on the single TH checkbox to
		// toggle all of the checkboxes
		dataTable.delegate('click', function (e) {
		    // undefined to trigger the emptyCellValue
		    var targetRate = e.target.get('id');
			edit(targetRate.replace('rowrate',''));
		}, '.editrowrate', dataTable);
		
		// PAGINATION
		// set status of navigation buttons on page load
		setNavigationButtonsState('<portlet:namespace />');
		$('.navigation').click(function(e) {
			var elementT 	= e.target;
			var elementId 	= elementT.id;
			var action 		= "";
			
			// determine which button was pressed and set the value of start accordingly
			if (elementId.indexOf("first") > -1)	{ action = "first"; start = 0; }
			if (elementId.indexOf("prev") > -1) 	{ action = "prev"; if (start - count <= 0) {start = 0} else { start -= count; } }
			if (elementId.indexOf("next") > -1) 	{ action = "next"; start = parseInt(start) + parseInt(count); }
			if (elementId.indexOf("last") > -1) 	{ action = "last"; start = parseInt(parseInt(total) / parseInt(count)) * count; }
			
			makeAjaxCall(action);
		});

		//get the initial value of the date input and use it in 
		//order not to make too many ajas request because of an 
		//existing bug in the alloy ui framework
		var oldDateValue = $('#<portlet:namespace />datafactura_header').val();
		
		// filter currency based on change
		$('#filter-moneda').change(function(e) {
			var elementT 	= e.target;
			var elementId 	= elementT.id;
			var action 		= "filter";
			// get selected value
			moneda = elementT.value;
			data_rata = oldDateValue;
			// reset start page 
			start = 0;
			// reset count
			total = 0;
			// make ajax call
			makeAjaxCall(action);
		});
		
		// called whenever a change in date selection was performed
		function dateChanged(input) {
			console.log('date changed: ' + input.val());
			oldDateValue = input.val();
			// get selected value
			data_rata = oldDateValue;
			moneda = $('#filter-moneda').val();
			// reset start page 
			start = 0;
			// reset count
			total = 0;
			action = "filter";
			// make ajax call
			makeAjaxCall(action);
		}
		
		var datepicker2 = new Y.DatePicker({
			trigger : '#<portlet:namespace />datafactura_header',
			mask : '%Y-%m-%d',
			calendar : {
				dateFormat : '%Y-%m-%d'
			},
			popover: {
				zIndex : 1,
		          toolbars: {
		            header: [[
		              {
		                icon:'icon-trash',
		                label: 'Reseteaza filtru',
		                on: {
		                  click: function() {
		                	  datepicker2.clearSelection();
		                  }
		                }
		              }]]
		          }
			},
			after: {
	            selectionChange: function(event) {
	            	if (oldDateValue != $('#<portlet:namespace />datafactura_header').val()) {
	            		oldDateValue = $('#<portlet:namespace />datafactura_header').val();
	            		dateChanged($('#<portlet:namespace />datafactura_header'));
	            	}
	            	$('#filtru_datafactura_div').html('');
	            }
	        }
		});		
		
		// general method for ajax calls.
		function makeAjaxCall(action) {
			$.ajax({
				type: 	"POST",
				url: 	"<%=ajaxURL%>",
				data: 	"<portlet:namespace />action=" + action + 
						"&<portlet:namespace />start=" + start +
						"&<portlet:namespace />count=" + count +
						"&<portlet:namespace />total=" + total +						
						"&<portlet:namespace />data_rata=" + data_rata +
						"&<portlet:namespace />moneda=" + moneda,
				success: function(msg) {
					// get table data
					if (msg != ""){
						var jsonEntries = jQuery.parseJSON(msg);
						total = jsonEntries.total;
						dataTable.set('data', eval(jsonEntries.values));
					} else {
						dataTable.set('data', []);
					}
					
					// set status of navigation buttons on page load
					setNavigationButtonsState('<portlet:namespace />');
				}
			});
		}
		// PAGINATION
		
		// get All entries after page load
		$(document).ready(function (){
			makeAjaxCall("first");
		});
	}
);
</script>
<aui:form method="post" action="<%=addEntryURL.toString() %>" name="editRate" id="editRate" style="margin:0;padding:0">
	<aui:input type="hidden" id="id" name="id"/>
	<aui:input type="hidden" id="ref" name="ref"/>
	<aui:input type="hidden" id="value" name="value"/> 
	<aui:input type="hidden" id="date_of_rate" name="date_of_rate"/> 
	<aui:input type="hidden" i="type" name="type"/>
	<aui:input type="hidden" id="created" name="created"/> 
	<aui:input type="hidden" id="modified" name="modified"/> 
</aui:form>
