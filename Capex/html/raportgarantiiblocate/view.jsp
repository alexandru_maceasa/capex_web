<%@page import="com.profluo.ecm.controller.UserTeamIdUtils"%>
<%@page import="com.profluo.ecm.model.db.RaportSituatieInregistrari"%>
<%@taglib uri="http://java.sun.com/portlet_2_0" 	prefix="portlet"%>
<%@taglib uri="http://liferay.com/tld/theme" 		prefix="liferay-theme"%>
<%@taglib uri="http://liferay.com/tld/ui" 			prefix="liferay-ui"%>
<%@taglib uri="http://alloy.liferay.com/tld/aui" 	prefix="aui"%>
<%@taglib uri="http://liferay.com/tld/util" 		prefix="liferay-util"%>

<%@page import="java.util.HashMap"%>
<%@page import="java.util.List"%>

<liferay-theme:defineObjects />
<portlet:defineObjects />

<%
	int start 			= 0; 
	int count 			= 15;
	int total 			= 0;
	String invoiceNo 	= "";
	String supplier 	= "";
	String magazin		= "";
	String co_number	= "";
	
	List<HashMap<String,Object>> allLines = RaportSituatieInregistrari.getAllWarrantyLines(start, count, "", "" , "", "", user);
 	allLines.addAll(RaportSituatieInregistrari.getAllWarrantyLinesTotal(start, count, "", "" , "", "", user));
	total = RaportSituatieInregistrari.getAllWarrantyLinesCount("", "" , "", "", user);
	
%>

<portlet:resourceURL id="ajaxURL" var="ajaxURL"/>

<portlet:resourceURL id="exportAction" var="exportURL">
	<portlet:param name="action" value="exportExcel"/>
</portlet:resourceURL>

<aui:layout>
	<aui:column columnWidth="15">
		<a class="btn btn-primary" href="<%=exportURL.toString()%>"> Export Excel </a>
	</aui:column>
	<% if(UserTeamIdUtils.belongsToTeam(user.getUserId(), UserTeamIdUtils.ACCOUNTING)) { %>
		<aui:column>
			<label for="<portlet:namespace />total_garantii" style="padding-top: 5px">Total garantii</label>
		</aui:column>
		<aui:column>
			<aui:input type="text" label="" name="total_garantii" id = "total_garantii" disabled="true" value="<%=RaportSituatieInregistrari.getAllWarrantyLinesTotalNoFIlter()%>"/>
		</aui:column>
	<% } %>
</aui:layout>
<aui:layout>
	<aui:column first="true" columnWidth="100" last="true">
		<div id="warranty_lines"></div>
	</aui:column>
</aui:layout>
<% if(!UserTeamIdUtils.belongsToTeam(user.getUserId(), UserTeamIdUtils.ACCOUNTING)) { %>
	<aui:layout>
		<aui:column last="true">
			<aui:button cssClass="btn btn-primary unlockWarranty" name="unlock_warranty" id="unlock_warranty" value="Deblocheaza garantiile"></aui:button>
		</aui:column>
	</aui:layout>
<% } %>
<%-- HTML for PAGINATION --%>
<%@ include file="/html/includes/pagination.jsp" %>

<script type="text/javascript">
var dataTable;
var start 		= <%=start%>
var count 		= <%=count%>
var total 		= <%=total%>
var invoiceNo 	= '<%=invoiceNo%>'
var supplier 	= '<%=supplier%>'
var magazin 	= '<%=magazin%>'
var co_number 	= '<%=co_number%>'

YUI({ lang: 'ro' }).use('aui-datatable','aui-datatype','datatable-sort', 'aui-datepicker', 'aui-base', 'liferay-portlet-url', 'aui-node',
		function(Y){
			var remoteData = [
						<% for (int i = 0; i < allLines.size(); i++) { %>
						{
							line_id: <%=allLines.get(i).get("line_id")%>,
							inv_number: '<%=allLines.get(i).get("inv_number")%>',
							inv_date: '<%=allLines.get(i).get("inv_date")%>', 
							supplier_name : '<%=allLines.get(i).get("supplier_name")%>',
							product_name: '<%=allLines.get(i).get("product_name")%>',
							garantie: '<%=allLines.get(i).get("garantie")%>',
							contract: '<%=allLines.get(i).get("contract")%>',
							magazin: '<%=allLines.get(i).get("magazin")%>'
							}
						<% if (i != (allLines.size() - 1)) { out.print(","); } %>
						<% } %>
			                  ];
			

			function formatCurrency(cell) {
			    format = {
			    	thousandsSeparator: ",",
			        decimalSeparator: ".",
			        decimalPlaces: noOfDecimalsToDisplay
			    };
			    return Y.DataType.Number.format(Number(cell.value), format);
			}
			var nestedColumns = [
								{key: 'line_id', className: 'hiddencol'},
								{key: 'inv_number', label: '<input id="nr_factura" type="text" class="field" placeholder="Numar Factura" style="width:100px;margin-bottom:0px"></input>'}, 
								{key: 'inv_date', label:'Data Factura'},
								{key: 'supplier_name', label:'<input id="filter_furnizor" type="text" class="field" placeholder="Furnizor" style="width:100%;margin-bottom:0px"></input>'},
								{key: 'magazin', label:'<input id="filter_magazin" type="text" class="field" placeholder="Magazin" style="width:100%;margin-bottom:0px"></input>'},
								{key: 'product_name', label: 'Produs'},
								{key: 'contract', label:'<input id="filter_contract" type="text" class="field" placeholder="Nr. contract" style="width:80px;margin-bottom:0px"></input>'},
								{key: 'garantie', formatter: formatCurrency, label:'Garantie',className: 'currencyCol'},
								<% if(!UserTeamIdUtils.belongsToTeam(user.getUserId(), UserTeamIdUtils.ACCOUNTING)) { %>
								,{ key: 'select', className:'align_center',
									label:'<input type="checkbox" name="header_checkbox" class="select-all align_center" />', allowHTML:  true,
									formatter: function (o) {
							            if (o.value == true) {
							            	return '<input type="checkbox" checked/>';
							            } else {
							            	return '<input type="checkbox" />';
							            }
							        },
					                emptyCellValue: '<input type="checkbox" />'}
								<% } %>
			                     ];
			
			dataTable = new Y.DataTable({
				columns:nestedColumns,
				data:remoteData,
				recordType:['inv_number', 'inv_date', 'supplier_name', 'product_name', 'garantie', 'magazin', 'select'],
				editEvent:'click'
			}).render('#warranty_lines');
			
			setNavigationButtonsState('<portlet:namespace />');
			$('.navigation').click(function(e) {
				var elementT 	= e.target;
				var elementId 	= elementT.id;
				var action 		= "";
				
				// determine which button was pressed and set the value of start accordingly
				if (elementId.indexOf("first") > -1)	{ action = "first"; start = 0; }
				if (elementId.indexOf("prev") > -1) 	{ action = "prev"; if (start - count <= 0) {start = 0} else { start -= count; } }
				if (elementId.indexOf("next") > -1) 	{ action = "next"; start = parseInt(start) + parseInt(count); }
				if (elementId.indexOf("last") > -1) 	{ action = "last"; start = parseInt(parseInt(total) / parseInt(count)) * count; if (start == total) { start -= count; } }
				
				makeAjaxCall(action);
			});
			
			//select all
			  dataTable.delegate('click', function (e) {
			        var checked = e.target.get('checked') || undefined;
			        this.data.invoke('set', 'select', checked, { silent: true });
			        this.syncUI();
			        if (checked) {
			        	dataTable.get('contentBox').one('.select-all').set('checked', true);
			        }
			    }, '.select-all', dataTable);
			
			  dataTable.delegate("click", function(e){
			        var checked = e.target.get('checked') || undefined;
			        this.getRecord(e.target).set('select', checked, { silent: true });
			        this.get('contentBox').one('.select-all').set('checked', false);
			    }, ".table-col-select input", dataTable);
			
			function makeAjaxCall(action) {
				$.ajax({
					type: "POST",
					url: "<%=ajaxURL%>",
					data: 	"<portlet:namespace />action=" + action + 
							"&<portlet:namespace />start=" + start + 
							"&<portlet:namespace />count=" + count +
							"&<portlet:namespace />total=" + total +
							"&<portlet:namespace />invoiceNo=" + invoiceNo +
							"&<portlet:namespace />supplier=" + supplier +
							"&<portlet:namespace />magazin=" + magazin +
							"&<portlet:namespace />co_number=" + co_number,
					success: function(msg) {
						// get table data
						if (msg != ""){
							var jsonEntries = jQuery.parseJSON(msg);
							total = jsonEntries.total;
							dataTable.set('data', eval(jsonEntries.values));
						} else {
							dataTable.set('data', []);
						}
						// set status of navigation buttons on page load
						setNavigationButtonsState('<portlet:namespace />');
					}
				});
			}
			
			$('#nr_factura').keyup(function (e) {
			    if ($("#nr_factura").is(":focus") && (e.keyCode == 13)) {
					var action 		= "filter";
					invoiceNo = e.target.value;
					supplier = $('#filter_furnizor').val();
					magazin = $('#filter_magazin').val();
					co_number = $('#filter_contract').val();
					start = 0;
					total = 0;
					makeAjaxCall(action);				
			    }
			});
			
			$('#filter_contract').keyup(function (e) {
			    if ($("#filter_contract").is(":focus") && (e.keyCode == 13)) {
					var action 		= "filter";
					co_number = e.target.value;
					supplier = $('#filter_furnizor').val();
					magazin = $('#filter_magazin').val();
					invoiceNo = $('#nr_factura').val();
					start = 0;
					total = 0;
					makeAjaxCall(action);				
			    }
			});
			
			$('#filter_furnizor').keyup(function (e) {
			    if ($("#filter_furnizor").is(":focus") && (e.keyCode == 13)) {
					var action 		= "filter";
					supplier = e.target.value;
					invoiceNo = $('#nr_factura').val();
					magazin = $('#filter_magazin').val();
					co_number = $('#filter_contract').val();
					start = 0;
					total = 0;
					makeAjaxCall(action);				
			    }
			});
			
			$('#filter_magazin').keyup(function (e) {
			    if ($("#filter_magazin").is(":focus") && (e.keyCode == 13)) {
					var action 		= "filter";
					magazin = e.target.value;
					supplier = $('#filter_furnizor').val();
					invoiceNo = $('#nr_factura').val();
					co_number = $('#filter_contract').val();
					start = 0;
					total = 0;
					makeAjaxCall(action);				
			    }
			});
			
			$('.unlockWarranty').click(function(e){
				deblocheazaGarantii('<portlet:namespace/>');
			});
			
			function deblocheazaGarantii(portletId){
				var ids = [];
				var ml  = dataTable.data, msg = '', template = '';
				ml.each(function(item,i){
					 var data = item.getAttrs(['line_id', 'select']);
					 if (data.select == true) {
						 ids.push(data.line_id);
					 }
				});
				if(ids.length > 0) {
					makeAjaxCallToSetWarrantyFlag(ids);
				} else {
					alert("Nu ati selectat nici o linie.")
				}
			}
			
			function makeAjaxCallToSetWarrantyFlag(ids) {
				$.ajax({
					type: "POST",
					url: "<%=ajaxURL%>",
					data: 	"<portlet:namespace />action=setWarrantyFlag" + 
							"&<portlet:namespace />ids_to_update=" + ids,
					success: function(msg) {
						// get table data
						if (msg != ""){
							alert("Solicitare inregistrata cu success!");
							location.reload();
						} else {
							alert("A aparut o eroare. Va rugam reincercati!");
						}
					}
				});
			}
});


</script>
