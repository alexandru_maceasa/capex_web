<%@page import="com.profluo.ec.carrefour.htmlutils.TabelUtils"%>
<%@page import="com.profluo.ecm.model.db.Raports"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme"%>

<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui"%>
<%@ taglib uri="http://alloy.liferay.com/tld/aui" prefix="aui"%>
<%@ taglib uri="http://liferay.com/tld/util" prefix="liferay-util"%>

<%@page import="java.util.HashMap"%>
<%@page import="java.util.List"%>


<liferay-theme:defineObjects />
<portlet:defineObjects />
<style>
.tip_capex{
	background-color: rgb(31, 78, 150);
	color : white;
}
.companie{
	background-color: #E3E3E3;
}
.total, th {
	background-color: rgb(32, 55, 100);
	color : white;
}
td {
	border: 1px solid black;
}

</style>

<portlet:resourceURL id="ajaxURL" var="ajaxURL"/>

<portlet:resourceURL id="exportAction" var="exportURL">
	<portlet:param name="action" value="exportExcel"/>
</portlet:resourceURL>

<portlet:actionURL name="updateInfo" var="submitURL" />

<%
	List<HashMap<String,Object>> list = Raports.getRaport(0, 500000, 1);
	list.addAll(Raports.getTableFooter(true, 1));
	List<HashMap<String,Object>> listTotal = Raports.getSecondRaport();
	listTotal.addAll(Raports.getTableFooter(false, 1));
%>


<aui:layout>
	<a class="btn btn-primary" onclick="exportRaport('<portlet:namespace />')"> Export Excel </a>
	<a class="btn btn-primary" onclick="actualizeaza()"> Actualizeaza informatii </a> 
	<aui:form id="exportForm" name="exportForm" action="<%=exportURL.toString()%>"> </aui:form>
	<aui:form id="actualizareForm" name="actualizareForm" action="<%=submitURL%>"> </aui:form>
</aui:layout>

<aui:layout>
	<div style="overflow-x:auto!important; white-space: nowrap;">
				<table class="inlineTable">
					<tr>
						<th>Cod</th><th>Denumire</th><th>Buget an</th><th>Actual Ian</th><th>Actual Feb</th><th>Actual Mar</th><th>Actual Apr</th>
						<th>Actual Mai</th><th>Actual Iun</th><th>Actual Iul</th><th>Actual Aug</th><th>Actual Sept</th><th>Actual Oct</th>
						<th>Actual Noi</th><th>Actual Dec</th><th>Total</th><th>Actual vs Bugetat</th>
					</tr>
				<% for (int i = 0; i < list.size(); i++) {%>
				<% if (list.get(i).get("code").equals("1") || list.get(i).get("code").equals("2") || list.get(i).get("code").equals("3"))  { %>
					<tr class='tip_capex'>
						<%=TabelUtils.getTableTdS(list, i) %>
					</tr>
				 <% } else if (list.get(i).get("code").toString().startsWith("D")) { %>
					<tr class='companie'>
						<%=TabelUtils.getTableTdS(list, i) %>
					</tr>
				<% } else if (list.get(i).get("code").toString().equals("Total")) { %>
					<tr class='total'>
						<%=TabelUtils.getTableTdS(list, i) %>
					</tr>
				<% } else { %>
					<tr>
						<%=TabelUtils.getTableTdS(list, i) %>
					</tr>
				<% } } %>
				
				</table>
				<table class="inlineTableEmptycol"><tr><td></td></tr></table>
				<table class="inlineTable">
					<tr>
						<th>Cod</th><th>Denumire</th><th>Buget an</th><th>Actual Ian</th><th>Actual Feb</th><th>Actual Mar</th><th>Actual Apr</th>
						<th>Actual Mai</th><th>Actual Iun</th><th>Actual Iul</th><th>Actual Aug</th><th>Actual Sept</th><th>Actual Oct</th>
						<th>Actual Noi</th><th>Actual Dec</th><th>Total</th><th>Actual vs Bugetat</th>
					</tr>
				<% for (int i = 0; i < list.size(); i++) {%>
				<% if (list.get(i).get("code").equals("1") || list.get(i).get("code").equals("2") || list.get(i).get("code").equals("3"))  { %>
					<tr class='tip_capex'>
						<%=TabelUtils.getTableTdS(list, i) %>
					</tr>
				 <% } else if (list.get(i).get("code").toString().startsWith("D")) { %>
					<tr class='companie'>
						<%=TabelUtils.getTableTdS(list, i) %>
					</tr>
				<% } else if (list.get(i).get("code").toString().equals("Total")) { %>
					<tr class='total'>
						<%=TabelUtils.getTableTdS(list, i) %>
					</tr>
				<% } else { %>
					<tr>
						<%=TabelUtils.getTableTdS(list, i) %>
					</tr>
				<% } } %>
				</table>
	</div>
</aui:layout>

<br/><br/>

<aui:layout>
	<div style="overflow-x:auto!important; white-space: nowrap;">
				<table class="inlineTable">
					<tr>
						<th>Denumire</th><th>\"   \"</th><th>Buget an</th><th>Actual Ian</th><th>Actual Feb</th><th>Actual Mar</th><th>Actual Apr</th>
						<th>Actual Mai</th><th>Actual Iun</th><th>Actual Iul</th><th>Actual Aug</th><th>Actual Sept</th><th>Actual Oct</th>
						<th>Actual Noi</th><th>Actual Dec</th><th>Total</th><th>Diferenta</th>
					</tr>
				<% for (int i = 0; i < listTotal.size(); i++) {%>
					<tr>
						<%=TabelUtils.getTotalTableTdS(listTotal, i) %>
					</tr>
				<% }  %>
				</table>
				<table class="inlineTable">
					<tr>
						<th>Denumire</th><th>    </th><th>Buget an</th><th>Actual Ian</th><th>Actual Feb</th><th>Actual Mar</th><th>Actual Apr</th>
						<th>Actual Mai</th><th>Actual Iun</th><th>Actual Iul</th><th>Actual Aug</th><th>Actual Sept</th><th>Actual Oct</th>
						<th>Actual Noi</th><th>Actual Dec</th><th>Total</th><th>Diferenta</th>
					</tr>
				<% for (int i = 0; i < listTotal.size(); i++) {%>
					<tr>
						<%=TabelUtils.getTotalTableTdS(listTotal, i) %>
					</tr>
				<% } %>
				</table>
	</div>
</aui:layout>


<script type="text/javascript">
function actualizeaza(){
	$('#<portlet:namespace/>actualizareForm').submit();
}
function exportRaport(portletId){
	$('#'+portletId+'exportForm').submit();
}
</script>
