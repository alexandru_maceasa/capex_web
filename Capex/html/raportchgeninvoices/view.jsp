<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme" %>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>
<%@ taglib uri="http://alloy.liferay.com/tld/aui" prefix="aui" %>
<%@ taglib uri="http://liferay.com/tld/util" prefix="liferay-util" %>


<liferay-theme:defineObjects />
<portlet:defineObjects />

<portlet:resourceURL id="exportAction" var="exportURL">
	<portlet:param name="action" value="exportExcel"/>
</portlet:resourceURL>

<aui:layout>
	<aui:form id="exportForm" name="exportForm" action="<%=exportURL.toString()%>">
		<aui:row rowWidth="50">
			<aui:column columnWidth="15">Export raport de la data: </aui:column>
			<aui:column columnWidth="10">
				<div id="from-date-div" style="height:30px !important;">
					<aui:input type="text" id="from_date" name="from_date" style="width:100px;" value="" placeholder="aaaa-ll-zz" label=""></aui:input>
				</div>
			</aui:column>
			<aui:column columnWidth="10">Pana la data:</aui:column>
			<aui:column columnWidth="10">
				<div id="from-date-div" style="height:30px !important;">
					<aui:input type="text" id="to_date" name="to_date" style="width:100px;" value="" placeholder="aaaa-ll-zz" label=""></aui:input>
				</div>
			</aui:column>
		</aui:row>
		<aui:row>
			<aui:column columnWidth="25">
				<a class="btn btn-primary" onclick="exportRaport('<portlet:namespace />')"> Export Excel </a>
			</aui:column>
		</aui:row>
	</aui:form>
</aui:layout>

<script type="text/javascript">
function exportRaport(portletId){
	if ( !$('#' + portletId + 'from_date').val()){
		alert("Va rugam sa selectati data de inceput.");
		return;
	}
	if ( !$('#' + portletId + 'to_date').val()){
		alert("Va rugam sa selectati data de sfarsit.");
		return;
	}
	$('#'+portletId+'exportForm').submit();
}
YUI().use('aui-datepicker', 'aui-datatable', 'aui-datatype', 'datatable-sort',
		function(Y) {
			var fromDatepicker = new Y.DatePicker({
				trigger : '#<portlet:namespace />from_date',
				mask : '%Y-%m-%d',
				popover : {
					zIndex : 1
				},
				popover: {
					zIndex : 1
				},
				calendar : {
					dateFormat : '%Y-%m-%d'
				},
				after: {
			        selectionChange: function(event) {
			        	fromDate = $('#<portlet:namespace />from_date').val();
			        }
			    }
			});
				
				var toDatepicker = new Y.DatePicker({
				trigger : '#<portlet:namespace />to_date',
				mask : '%Y-%m-%d',
				popover : {
					zIndex : 1
				},
				popover: {
					zIndex : 1
				},
				calendar : {
					dateFormat : '%Y-%m-%d'
				},
				after: {
			        selectionChange: function(event) {
			        	toDate = $('#<portlet:namespace />to_date').val();
			        }
			    }
			});	
});
</script>
	


