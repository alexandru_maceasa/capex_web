<%@page import="com.liferay.portal.service.UserServiceUtil"%>
<%@page import="com.liferay.portal.model.User"%>
<%@page import="com.profluo.ecm.model.db.DefSupplier"%>
<%@page import="com.profluo.ecm.model.db.DefStore"%>
<%@page import="com.profluo.ec.carrefour.htmlutils.DropDownUtils"%>
<%@page import="com.profluo.ecm.model.db.ListaInit"%>
<%@page import="com.profluo.ecm.model.db.DefNewProj"%>
<%@page import="com.profluo.ecm.model.db.InvoiceHeader"%>
<%@page import="com.profluo.ecm.controller.UserTeamIdUtils"%>
<%@page import="com.profluo.ecm.model.db.DefCompanies"%>
<%@page import="com.profluo.ecm.model.db.InventoryHeader"%>
<%@taglib uri="http://java.sun.com/portlet_2_0" 	prefix="portlet"%>
<%@taglib uri="http://liferay.com/tld/theme" 		prefix="liferay-theme"%>
<%@taglib uri="http://liferay.com/tld/ui" 			prefix="liferay-ui"%>
<%@taglib uri="http://alloy.liferay.com/tld/aui" 	prefix="aui"%>
<%@taglib uri="http://liferay.com/tld/util" 		prefix="liferay-util"%>

<%@page import="java.util.HashMap"%>
<%@page import="java.util.List"%>

<liferay-theme:defineObjects />
<portlet:defineObjects />

<%
	List<HashMap<String,Object>> allCompanies = DefCompanies.getAll();
	int start = 0; 
	int count = 10;
	List<HashMap<String,Object>> allSuppliers = DefSupplier.getAllFilteredForRaport(start, count, "0");
	int total = DefSupplier.getAllFilteredForRaportCount("0");
%>
<portlet:resourceURL id="ajaxURL" var="ajaxURL"/>

<portlet:resourceURL id="exportRaport" var="exportRaportURL">
	<portlet:param name="action" value="exportRaport"/>
</portlet:resourceURL>

<aui:layout>
	<aui:column columnWidth="15" first="true">
		<label for="<portlet:namespace />societate" style="padding-top: 5px">Societatea</label>
	</aui:column>
	<aui:column>
		<aui:select id="societate" name="societate" label="" style="width:200px;">
			<aui:option selected="selected" value="0"> ----------- </aui:option>
			<% for (int i = 0 ; i < allCompanies.size(); i++) { %>
				<aui:option value="<%=i+1%>"> <%=allCompanies.get(i).get("name").toString()%> </aui:option>
			<% } %>
		</aui:select>
	</aui:column>
</aui:layout>
<aui:layout>
	<aui:column first="true">
		<aui:column>
			<a class="btn btn-primary" onclick="filterListing('<portlet:namespace />')"> Aplica Filtre </a>
		</aui:column>
		<aui:column>
			<a class="btn btn-primary" onclick="exportRaport('<portlet:namespace />')"> Export Raport </a>
		</aui:column>
	</aui:column>
</aui:layout>
<br/>
<aui:layout>
	<aui:column first="true" columnWidth="100" last="true">
		<div id="createdSupplier"></div>
	</aui:column>
</aui:layout>
<script type="text/javascript">
var start = <%=start%>
var count = <%=count%>
var total = <%=total%>
var societate = -1;
var dataTableIV;
</script>

<%-- HTML for PAGINATION --%>
<%@ include file="/html/includes/pagination.jsp" %>

<script type="text/javascript">


$(document).ready(function(e){
	$('#<portlet:namespace/>store').chosen();
});


function filterListing(portletId){
	societate = $('#<portlet:namespace/>societate').val();
	var action = "filter";
	makeAjaxCall(action);
}

function exportRaport(portletId){
	societate = $('#<portlet:namespace/>societate').val();
	$('#<portlet:namespace/>company_id').val(societate);
	$('#<portlet:namespace/>exportFormRaport').submit();
	
}

YUI({ lang: 'ro' }).use('aui-datatable','aui-datatype','datatable-sort', 'aui-datepicker', 'aui-base', 'liferay-portlet-url', 'aui-node',
		function(Y){
		var remoteData = [
	      			<% for (int i = 0; i < allSuppliers.size(); i++) { 
// 	      				User creator = null;
	      				String store = "";
	      				try {
// 	      					creator = UserServiceUtil.getUserById(Long.parseLong(allSuppliers.get(i).get("user").toString()));
// 	      					store = DefStore.getIdByStoreId(creator.getAddresses().get(0).getStreet2().toString());
	      				} catch (Exception e) {
	      					e.printStackTrace();
	      				}
	      			%>
	      				{
	      					creator_name: 		'<%=allSuppliers.get(i).get("creator_name")%>',
	      					creator_store: 		'<%=allSuppliers.get(i).get("magazin")%>',
	      					societate: 			'<%=allSuppliers.get(i).get("societate")%>',
	      					supplier_code: 		'<%=allSuppliers.get(i).get("supplier_code")%>',
	      					supplier_name: 		'<%=allSuppliers.get(i).get("supplier_name")%>',
	      					supplier_cui: 		'<%=allSuppliers.get(i).get("supplier_cui")%>',
	      					reg_com: 			'<%=allSuppliers.get(i).get("reg_com")%>',
	      					address: 			'<%=allSuppliers.get(i).get("address")%>',
	      					phone: 				'<%=allSuppliers.get(i).get("phone")%>',
	      					email: 				'<%=allSuppliers.get(i).get("email")%>',
	      					scadenta: 			'<%=allSuppliers.get(i).get("scadenta")%>',
	      					bank_acc: 			'<%=allSuppliers.get(i).get("bank_acc")%>',
	      					bank: 				'<%=allSuppliers.get(i).get("bank")%>',
	      					swift: 				'<%=allSuppliers.get(i).get("swift")%>',
	      					payment_term: 		'<%=allSuppliers.get(i).get("payment_term")%>',
	      					supplier_status: 	'<%=allSuppliers.get(i).get("supplier_status")%>',
	      					supplier_type : 	'<%=allSuppliers.get(i).get("supplier_type")%>',
	      					supplier_request_date:'<%=allSuppliers.get(i).get("supplier_request_date")%>',
	      					status:				'<%=allSuppliers.get(i).get("status")%>',
	      					kontan_creation_date:'<%=allSuppliers.get(i).get("kontan_creation_date")%>'
	      					
	      				}
	      				<% if (i != (allSuppliers.size() - 1)) { out.print(","); } %>
	      			<% } %>
	      		];
		var nestedColumns = [		
							{key: 'creator_name', 		label:'Nume solicitant'},
							{key: 'creator_store', 		label:'Magazin'},
							{key: 'societate', 			label:'Societate'},
							{key: 'supplier_code', 		label:'Cod furnizor'},
							{key: 'supplier_name', 		label:'Nume furnizor'},
		         			{key: 'supplier_cui', 		label:'CUI'},
							{key: 'reg_com', 			label:'Numar Reg. Comertului'},
							{key: 'address', 			label:'Adresa'},
							{key: 'phone', 				label:'Telefon'},
							{key: 'email', 			label:'Adresa email'},
							{key: 'scadenta', 	label: 'Scadenta'},
		         			{key: 'bank_acc', 	label:'Cont bancar furnizor'},
		         			{key: 'bank', 	label: 'Banca furnizor'},
		         			{key: 'swift', 	label: 'Cod swift'},
		         			{key: 'payment_term', 	label: 'Termen plata(zile)'},
		         			{key: 'supplier_status', 	label:'Furnizor Activ/Inactiv'},
		         			{key: 'supplier_type', 	label:'Tip furnizor'},
		         			{key: 'supplier_request_date', 	label:'Data solicitare'},
		         			{key: 'status', 	label:'Status'},
		         			{key: 'kontan_creation_date', 	label:'Data creare Kontan'}
		         		];
		dataTableIV = new Y.DataTable({
			columns:nestedColumns,
			data:remoteData,
			scrollable: "x",
	        width: "100%",
			recordType:['creator_name', 'creator_store', 'societate', 'supplier_code', 'supplier_name', 'supplier_cui',
			            'reg_com', 'address', 'phone', 'email', 'scadenta', 'bank_acc', 'bank', 'swift','payment_term',
			            'supplier_status', 'supplier_type', 'supplier_request_date', 'status', 'kontan_creation_date'],
			editEvent:'click'
		}).render('#createdSupplier');
		
		// PAGINATION
		// set status of navigation buttons on page load
		setNavigationButtonsState('<portlet:namespace />');
		$('.navigation').click(function(e) {
			var elementT 	= e.target;
			var elementId 	= elementT.id;
			var action 		= "";
			
			// determine which button was pressed and set the value of start accordingly
			if (elementId.indexOf("first")> -1)	{ action = "first"; start = 0; }
			if (elementId.indexOf("prev") > -1) 	{ action = "prev"; if (start - count <= 0) {start = 0} else { start -= count; } }
			if (elementId.indexOf("next") > -1) 	{ action = "next"; start = parseInt(start) + parseInt(count); }
			if (elementId.indexOf("last") > -1) 	{ action = "last"; start = parseInt(parseInt(total) / parseInt(count)) * count; if (start == total) { start -= count; } }
			
			makeAjaxCall(action);
		});
		
});

function makeAjaxCall(action) {
	$.ajax({
		type: "POST",
		url: "<%=ajaxURL%>",
		data: 	"<portlet:namespace />action=" + action + 
				"&<portlet:namespace />start=" + start + 
				"&<portlet:namespace />count=" + count +
				"&<portlet:namespace />total=" + total +
				"&<portlet:namespace />societate=" + societate,
		success: function(msg) {
			// get table data
			if (msg != ""){
				var jsonEntries = jQuery.parseJSON(msg);
				total = jsonEntries.total;
				dataTableIV.set('data', eval(jsonEntries.values));
			} else {
				dataTableIV.set('data', []);
			}
			// set status of navigation buttons on page load
			setNavigationButtonsState('<portlet:namespace />');
		}
	});
}
</script>
<aui:form id="exportFormRaport" name="exportFormRaport" style="dysplay:none"  action="<%=exportRaportURL.toString()%>">
	<aui:input type="hidden" id="company_id" name="company_id"/>
</aui:form>