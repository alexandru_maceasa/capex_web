<%@page import="com.profluo.ecm.model.db.WorkEquipmentHeader"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.List"%>
<%@page import="com.profluo.ecm.model.vo.DefCurrencyVo"%>
<%@page import="com.profluo.ecm.model.vo.DefCompaniesVo"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme"%>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui"%>
<%@ taglib uri="http://alloy.liferay.com/tld/aui" prefix="aui"%>
<%@ taglib uri="http://liferay.com/tld/util" prefix="liferay-util"%>

<liferay-theme:defineObjects />
<portlet:defineObjects />
<portlet:resourceURL id="ajaxURL" var="ajaxURL"/>
<portlet:renderURL var="addEntryURL" >
	<portlet:param name="mvcPath" value="/html/workequipment/registerWorkEquipment.jsp"></portlet:param>
</portlet:renderURL>

<% // PAGINATION Start - env setup 
int start 			= 0; 
int count 			= 15;
int total 			= 0;
String invoiceNo	= "";
String categorie 	= "";
String supplierCode = "";
String supplierName = "";

	List<HashMap<String,Object>> allCompanies = DefCompaniesVo.getInstance();
	List<HashMap<String,Object>> allCurrencies = DefCurrencyVo.getInstance();
	
	List<HashMap<String,Object>> allWorkingClothes = WorkEquipmentHeader.getAllFilteredFromOptimal(start, count, "", "", "", "", "");
	total = WorkEquipmentHeader.getAllFilteredFromOptimalCount("", "", "", "", "");
	
%>


<script type="text/javascript">
var start = "<%=start%>";
var count = "<%=count%>";
var total = "<%=total%>";
//filters
var supplierCode 	= "";
var supplierName 	= "";
var company_id 		= 0;
var moneda 			= "";
var invoiceNo 		= 0;
</script>

<aui:layout>
	<aui:column first="true" columnWidth="100" last="true">
		<div id="listing-facturi"></div>
	</aui:column>
</aui:layout>

<%-- HTML for PAGINATION --%>
<%@ include file="/html/includes/pagination.jsp" %>

<script type="text/javascript">
YUI({lang: 'ro'}).use('aui-datepicker', 'aui-datatable', 'aui-datatype', 'datatable-sort', function(Y) {
	var remoteData = [
	            		<%
	            		if (allWorkingClothes != null) {
	            		for (int i = 0; i < allWorkingClothes.size(); i++) {	
	            		%>
	            		{
	            			id: 				<%=allWorkingClothes.get(i).get("id")%>,
	            			inv_number: 		'<%=allWorkingClothes.get(i).get("inv_number")%>',
	            			reg_date: 			'<%=allWorkingClothes.get(i).get("reg_date")%>',
	            			id_supplier: 		'<%=allWorkingClothes.get(i).get("id_supplier")%>',
	            			supplier_name: 		'<%=allWorkingClothes.get(i).get("supplier_name")%>',
	            			inv_date: 			'<%=allWorkingClothes.get(i).get("inv_date")%>',
	            			due_date: 			'<%=allWorkingClothes.get(i).get("due_date")%>', 
	            			total_with_vat_curr: <%=allWorkingClothes.get(i).get("total_with_vat_curr")%>,
	            			currency: 			'<%=allWorkingClothes.get(i).get("currency")%>',
	            			company_name: 		'<%=allWorkingClothes.get(i).get("company_name")%>'
	            		}
	            		<% if (i != (allWorkingClothes.size() - 1)) { out.print(","); } %>
	            		<% } } %>
	            		];
	function formatCurrency(cell) {
	    format = {
	    	thousandsSeparator: ",",
	        decimalSeparator: ".",
	        decimalPlaces: noOfDecimalsToDisplay
	    };
	    return Y.DataType.Number.format(Number(cell.value), format);
	}
	
	
		var nestedCols = [
	        		    { key: 'id', className: 'hiddencol'},
	        			{ key: 'inv_number', 
	        		    	label: '<input class="field" style="width:60px !important;margin:0" type="text" value="" id="filter-nr" name="filter-nr" placeholder="Nr."></input>' },
	        		    { key: 'reg_date', label: 'Data Inregistrare'},
	        		    { key: 'id_supplier',
	          				label: '<input class="field" style="width:70px !important;margin:0" type="text" value="" id="filter-supplier_code" name="filter-supplier_code" placeholder="Cod furnizor" style="margin:0"/>'
	          			},
	          			{ key: 'supplier_name', 
	          				label: '<input class="field" type="text" value="" id="filter-supplier_name" name="filter-supplier_code" placeholder="Furnizor" style="margin:0"/>'
	          			},
	          			{ key: 'inv_date', label:  'Data factura'},
	          			{ key: 'due_date', label:  'Data Scadenta'},
	          			{ key: 'total_with_vat_curr', label: 'Val cu TVA', formatter: formatCurrency, className: 'currencyCol'},
	          			{ key: 'currency', label: '<select id="filter-moneda" name="filter-moneda" label="" style="width:100px;margin:0">' +
	          				'<option selected="selected" value="">Moneda</option>' +
	          				<% for (int i = 0; i < allCurrencies.size(); i++) { %>
	          				<% if (allCurrencies.get(i).get("status").equals("A")) {%>
	          				<% out.println("'<option value=\"" + allCurrencies.get(i).get("ref") +"\">" + allCurrencies.get(i).get("ref") +"</option>' + "); %>
	          				<% } } %>
	          				'</select>'},
	          			{ key: 'company_name', label: '<select id="filter-societate" name="filter-societate" label="" style="width:100px;margin:0">' +
	          				'<option selected="selected" value="0">Societate</option>' +
	          				<% for (int i = 0; i < allCompanies.size(); i++) { %>
	          				<% if (allCompanies.get(i).get("status").equals("A")) {%>
	          				<% out.println("'<option value=\"" + allCompanies.get(i).get("id") +"\">" + allCompanies.get(i).get("name") +"</option>' + "); %>
	          				<% } } %>
	          				'</select>'},
	          	  		{ key: 'actiuni', label: 'Actiuni', allowHTML: true, 
	          	  				formatter: '<a href="#" id="row{id}" class="editrow">Vizualizare</a>'
	          	  		}
	        		    ];
		
		// TABLE INIT
  		var dataTable = new Y.DataTable({
  		    columns: nestedCols,
  		    data: remoteData,
	  		scrollable: "x",
	        width: "100%",
  		    recordType: ['id', 'inv_number', 'created', 'supplier_code', 
  		                 'supplier_name', 'inv_date', 'pay_date', 'total_no_vat_curr', 
  		                 'total_with_vat_curr', 'currency', 'company_name'],
  		    editEvent: 'click'
  		}).render('#listing-facturi');
		
  		dataTable.delegate('click', function (e) {
  		    // undefined to trigger the emptyCellValue
  		    e.preventDefault();
  		    var elem = e.target.get('id');
  		    var invoiceId = elem.replace('row',"");
  		    $('#<portlet:namespace/>id').val(invoiceId);
  		  	$('#<portlet:namespace/>viewInvoice').submit();
  		}, '.editrow', dataTable);
		
  		// PAGINATION
  		// set status of navigation buttons on page load
  		setNavigationButtonsState('<portlet:namespace />');
  		$('.navigation').click(function(e) {
  			var elementT 	= e.target;
  			var elementId 	= elementT.id;
  			var action 		= "";
  			
  			// determine which button was pressed and set the value of start accordingly
  			if (elementId.indexOf("first") > -1)	{ action = "first"; start = 0; }
  			if (elementId.indexOf("prev") > -1) 	{ action = "prev"; if (start - count <= 0) {start = 0} else { start -= count; } }
  			if (elementId.indexOf("next") > -1) 	{ action = "next"; start = parseInt(start) + parseInt(count); }
  			if (elementId.indexOf("last") > -1) 	{ action = "last"; start = parseInt(parseInt(total) / parseInt(count)) * count; if (start == total) { start -= count; } }
  			
  			makeAjaxCall(action);
  		});
  		
  		$('#filter-nr').keyup(function (e) {
		    if ($("#filter-nr").is(":focus") && (e.keyCode == 13)) {
				var action 		= "filter";
				invoiceNo = e.target.value;
				supplierCode = $('#filter-supplier_code').val();
				supplierName = $('#filter-supplier_name').val();
				moneda = $('#filter-moneda').val();
				company_id = $('#filter-societate').val(); 
				start = 0;
				total = 0;
				makeAjaxCall(action);				
		    }
		});
  		
  		$('#filter-supplier_code').keyup(function (e) {
		    if ($("#filter-supplier_code").is(":focus") && (e.keyCode == 13)) {
				var action 		= "filter";
				supplierCode = e.target.value;
				invoiceNo = $('#filter-nr').val();
				supplierName = $('#filter-supplier_name').val();
				moneda = $('#filter-moneda').val();
				company_id = $('#filter-societate').val(); 
				start = 0;
				total = 0;
				makeAjaxCall(action);				
		    }
		});
  		
  		$('#filter-supplier_name').keyup(function (e) {
		    if ($("#filter-supplier_name").is(":focus") && (e.keyCode == 13)) {
				var action 		= "filter";
				supplierName = e.target.value;
				invoiceNo = $('#filter-nr').val();
				supplierCode = $('#filter-supplier_code').val();
				moneda = $('#filter-moneda').val();
				company_id = $('#filter-societate').val(); 
				start = 0;
				total = 0;
				makeAjaxCall(action);				
		    }
		});
  		
  		$('#filter-moneda').change(function(e){
  			var action 		= "filter";
			moneda = e.target.value;
			invoiceNo = $('#filter-nr').val();
			supplierCode = $('#filter-supplier_code').val();
			supplierName = $('#filter-supplier_name').val();
			company_id = $('#filter-societate').val(); 
			start = 0;
			total = 0;
			makeAjaxCall(action);	
  		});
  		
  		$('#filter-societate').change(function(e){
  			var action 		= "filter";
			company_id = e.target.value;
			invoiceNo = $('#filter-nr').val();
			supplierCode = $('#filter-supplier_code').val();
			supplierName = $('#filter-supplier_name').val();
			moneda = $('#filter-moneda').val(); 
			start = 0;
			total = 0;
			makeAjaxCall(action);	
  		});
  		
  		function makeAjaxCall(action) {
  			$.ajax({
  				type: "POST",
  				url: "<%=ajaxURL%>",
  				data: 	"<portlet:namespace />action=" + action + 
  						"&<portlet:namespace />start=" + start + 
  						"&<portlet:namespace />count=" + count +
  						"&<portlet:namespace />total=" + total +
  						"&<portlet:namespace />moneda=" + moneda +
  						"&<portlet:namespace />invoiceNo=" + invoiceNo +
  						"&<portlet:namespace />company_id=" + company_id +
  						"&<portlet:namespace />supplier_code=" + supplierCode +
  						"&<portlet:namespace />supplier_name=" + supplierName,
  				success: function(msg) {
  					// get table data
  					if (msg != ""){
  						var jsonEntries = jQuery.parseJSON(msg);
  						total = jsonEntries.total;
  						dataTable.set('data', eval(jsonEntries.values));
  					} else {
  						dataTable.set('data', []);
  					}
  					// set status of navigation buttons on page load
  					setNavigationButtonsState('<portlet:namespace />');
  				}
  			});
  		}
});

</script>
<aui:form method="post" action="<%=addEntryURL.toString() %>" name="viewInvoice" id="viewInvoice">
	<aui:input type="hidden" id="id" name="id"/> 
</aui:form>