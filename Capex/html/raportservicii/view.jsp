<%@page import="com.profluo.ecm.model.db.InvoiceHistory"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.List"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme"%>

<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui"%>
<%@ taglib uri="http://alloy.liferay.com/tld/aui" prefix="aui"%>
<%@ taglib uri="http://liferay.com/tld/util" prefix="liferay-util"%>

<liferay-theme:defineObjects />
<portlet:defineObjects />

<portlet:resourceURL id="exportAction" var="exportURL">
	<portlet:param name="action" value="exportExcel"/>
</portlet:resourceURL>

<aui:layout>
	<aui:form id="exportForm" name="exportForm" action="<%=exportURL.toString()%>">
		<aui:row rowWidth="50">
			<aui:column columnWidth="15">Export raport de la data factura: </aui:column>
			<aui:column columnWidth="10">
				<div id="from-date-div" style="height:30px !important;">
					<aui:input type="text" id="from_date" name="from_date" style="width:100px;" value="" placeholder="aaaa-ll-zz" label=""></aui:input>
				</div>
			</aui:column>
			<aui:column columnWidth="10">Pana la data factura:</aui:column>
			<aui:column columnWidth="10">
				<div id="from-date-div" style="height:30px !important;">
					<aui:input type="text" id="to_date" name="to_date" style="width:100px;" value="" placeholder="aaaa-ll-zz" label=""></aui:input>
				</div>
			</aui:column>
		</aui:row>
		<aui:row>
			<aui:column columnWidth="25">
				<a class="btn btn-primary" onclick="exportRaport('<portlet:namespace />')"> Export Excel </a>
			</aui:column>
		</aui:row>
	</aui:form>
</aui:layout>

<aui:layout>
	<aui:column first="true" columnWidth="100">
		<div id="raport_servicii"></div>
	</aui:column>
</aui:layout>


<%
	List<HashMap<String,Object>> list = InvoiceHistory.getAllServicesLines("2016-01-01", "2016-12-30");
%>

<script type="text/javascript">
function exportRaport(portletId){
	if ( !$('#' + portletId + 'from_date').val()){
		alert("Va rugam sa selectati data de inceput.");
		return;
	}
	if ( !$('#' + portletId + 'to_date').val()){
		alert("Va rugam sa selectati data de sfarsit.");
		return;
	}
	$('#'+portletId+'exportForm').submit();
}
YUI().use('aui-datepicker', 'aui-datatable', 'aui-datatype', 'datatable-sort',
		function(Y) {
			var fromDatepicker = new Y.DatePicker({
				trigger : '#<portlet:namespace />from_date',
				mask : '%Y-%m-%d',
				popover : {
					zIndex : 1
				},
				popover: {
					zIndex : 1
				},
				calendar : {
					dateFormat : '%Y-%m-%d'
				},
				after: {
			        selectionChange: function(event) {
			        	fromDate = $('#<portlet:namespace />from_date').val();
			        }
			    }
			});
				
				var toDatepicker = new Y.DatePicker({
				trigger : '#<portlet:namespace />to_date',
				mask : '%Y-%m-%d',
				popover : {
					zIndex : 1
				},
				popover: {
					zIndex : 1
				},
				calendar : {
					dateFormat : '%Y-%m-%d'
				},
				after: {
			        selectionChange: function(event) {
			        	toDate = $('#<portlet:namespace />to_date').val();
			        }
			    }
			});	
				
				var remoteData = [
				                  <% for(int i = 0; i< list.size(); i++) { %>
				                  	{
				                  		nrFactura:			'<%=list.get(i).get("nrFactura")%>',
				                  		dataFactura: 		'<%=list.get(i).get("dataFactura")%>',
				                  		furnizor: 		'<%=list.get(i).get("furnizor")%>',
				                  		magazin: 		'<%=list.get(i).get("magazin")%>',
				                  		produs: 	'<%=list.get(i).get("produs")%>',
				                  		oldValue:			'<%=list.get(i).get("oldValue")%>',
				                  		newValue:		'<%=list.get(i).get("newValue")%>',
				                  		cantitate:			'<%=list.get(i).get("cantitate")%>'
			                		}
				                  <% if (i != (list.size() - 1)) { out.print(","); } %>
				                  <% } %>
				                  ];
				var nestedCols = [
				                  { key : 'nrFactura', label : 'Nr. factura'},
				                  { key : 'dataFactura', label : 'Data factura'},
				                  { key : 'furnizor', label : 'Furnizor'},
				                  { key : 'magazin', label : 'Magazin'},
				                  { key : 'produs', label : 'Produs'},
				                  { key : 'oldValue', label : 'Valoare veche'},
				                  { key : 'newValue', label : 'Valoare noua'},
				                  { key : 'cantitate', label : 'Cantitatea'}
				                  ];
				
				var dataTable = new Y.DataTable({
				    columns: nestedCols,
				    data: remoteData,	
				    editEvent: 'click',
				    recordType: ['nrFactura' ,'dataFactura', 'furnizor', 'produs', 'oldValue', 'newValue', 'cantitate']
				}).render('#raport_servicii');
});
</script>