<%@page import="com.profluo.ecm.flows.CashExpenseStages"%>
<%@page import="com.profluo.ecm.model.vo.DefCurrencyVo"%>
<%@page import="com.profluo.ecm.model.vo.DefCompaniesVo"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.List"%>
<%@page import="com.profluo.ecm.model.db.CashExpense"%>
<%@page import="com.profluo.ecm.model.db.DefCompanies"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme"%>

<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui"%>
<%@ taglib uri="http://alloy.liferay.com/tld/aui" prefix="aui"%>
<%@ taglib uri="http://liferay.com/tld/util" prefix="liferay-util"%>
 
<liferay-theme:defineObjects />
<portlet:defineObjects />
<portlet:resourceURL id="ajaxURL" var="ajaxURL"/>

<portlet:renderURL var="addEntryURL" >
	<portlet:param name="mvcPath" value="/html/deconturi/inregistrare_decont.jsp"></portlet:param>
</portlet:renderURL>

<% // PAGINATION Start - env setup 
int start 			= 0; 
int count 			= 15;
int total 			= 0;
String docNo 		= "";
String cuiFurnizor 	= "";
String numeFurnizor = "";
String moneda 		= "";
String compania 	= "";

	// retrieve all invoices headers from DB
	List<HashMap<String,Object>> allCashExp = null;
	// retrieve all companies from DB
	List<HashMap<String,Object>> allCompanies = DefCompaniesVo.getInstance();
	// retrieve all currencies from DB
	List<HashMap<String,Object>> allCurrencies = DefCurrencyVo.getInstance();
	
	allCashExp = CashExpense.getAllFiltered(start, count, docNo, cuiFurnizor, numeFurnizor, moneda, compania);
	total = CashExpense.getAllFilteredCount(docNo, cuiFurnizor, numeFurnizor, moneda, compania);

%>

<script type="text/javascript">
var start 		= "<%=start%>";
var count 		= "<%=count%>";
var total 		= "<%=total%>";
var docNo 		= "<%=docNo%>";
var cuiFurnizor = "<%=cuiFurnizor%>";
var numeFurnizor = "<%=numeFurnizor%>";
var company_id 	= "<%=compania%>";
var moneda 		= "<%=moneda%>";
</script>

<liferay-ui:success message="delete_ok" key="delete_ok"/>
<liferay-ui:success message="CashExpAdded" 	key="CashExpAdded"/>
<liferay-ui:success message="CashExpUpdated" 	key="CashExpUpdated"/>
<liferay-ui:success message="update_ok" key="update_ok"/>
<liferay-ui:error message="update_nok" key="update_nok"/>

<%-- Links --%>

<aui:layout>
	<aui:column first="true" columnWidth="100" last="true">
		<div id="listing-deconturi"></div>
	</aui:column>
</aui:layout>

<%-- HTML for PAGINATION --%>
<%@ include file="/html/includes/pagination.jsp" %>

<script type="text/javascript">
	YUI({lang: 'ro'}).use('aui-datepicker', 'aui-datatable', 'aui-datatype', 'datatable-sort', function(Y) {
		
		var remoteData = [
				  		<% if (allCashExp != null) {
				  				for (int i = 0; i < allCashExp.size(); i++) {	
				  		%>
				  		{
				  			id: 					'<%=allCashExp.get(i).get("id")%>',
				  			doc_nr: 				'<%=allCashExp.get(i).get("doc_nr")%>',
				  			dataImport: 			'<%=allCashExp.get(i).get("dataImport")%>',
				  			supplier_cui: 			'<%=allCashExp.get(i).get("supplier_cui")%>',
				  			supplier_name: 			'<%=allCashExp.get(i).get("supplier_name")%>',
				  			inv_date: 				'<%=allCashExp.get(i).get("inv_date")%>',
				  			total_with_vat_curr: 	'<%=allCashExp.get(i).get("total_with_vat_curr")%>',
				  			currency: 				'<%=allCashExp.get(i).get("currency")%>',
				  			company_name: 			'<%=allCashExp.get(i).get("company_name")%>'
				  		}
				  		<% if (i != (allCashExp.size() - 1)) { out.print(","); } %>
				  		<% } } %>
				  		];
  		
  		// COLUMN INFO and ATTRIBUTES
  		var nestedCols = [
  		    { key: 'id', className: 'hiddencol'},
  			{ key: 'doc_nr', 
  		    	label: '<input class="field" style="width:60px !important;margin:0" type="text" value="" id="filter-nr" name="filter-nr" placeholder="Nr."></input>' },
  			{ key: 'dataImport', label: 'Data import'},
  			{ key: 'supplier_cui',
  				label: '<input class="field" style="width:70px !important;margin:0" type="text" value="" id="filter-supplier_code" name="filter-supplier_code" placeholder="CUI furnizor" style="margin:0"/>'
  			},
  			{ key: 'supplier_name', 
  				label: '<input class="field" type="text" value="" id="filter-supplier_name" name="filter-supplier_code" placeholder="Nume furnizor" style="margin:0"/>'
  			},
  			{ key: 'inv_date', label:  'Data document'},
  			{ key: 'total_with_vat_curr', label: 'Val cu TVA', formatter: formatCurrency, className: 'currencyCol'},
  			{ key: 'currency', label: '<select id="filter-moneda" name="filter-moneda" label="" style="width:100px;margin:0">' +
  				'<option selected="selected" value="">Moneda</option>' +
  				<% for (int i = 0; i < allCurrencies.size(); i++) { %>
  				<% if (allCurrencies.get(i).get("status").equals("A")) {%>
  				<% out.println("'<option value=\"" + allCurrencies.get(i).get("ref") +"\">" + allCurrencies.get(i).get("ref") +"</option>' + "); %>
  				<% } } %>
  				'</select>'},
  			{ key: 'company_name', label: '<select id="filter-societate" name="filter-societate" label="" style="width:100px;margin:0">' +
  				'<option selected="selected" value="0">Societate</option>' +
  				<% for (int i = 0; i < allCompanies.size(); i++) { %>
  				<% if (allCompanies.get(i).get("status").equals("A")) {%>
  				<% out.println("'<option value=\"" + allCompanies.get(i).get("id") +"\">" + allCompanies.get(i).get("name") +"</option>' + "); %>
  				<% } } %>
  				'</select>'},
  			{ key: 'actiuni', label: 'Actiuni', allowHTML: true, 
  				// gets the id of the row and creates an unique link for every row
  					formatter: '<a href="#" id="row{id}" class="editrow">Inregistrare</a>'
  				
  			}
  		];

  		// TABLE INIT
  		var dataTable = new Y.DataTable({
  		    columns: nestedCols,
  		    data: remoteData,
  		    recordType: ['id', 'inv_number', 'doc_rec_data', 'supplier_code', 'supplier_name', 'inv_date',  
  		                 'total_with_vat_curr', 'currency', 'company_name'],
  		    editEvent: 'click'
  		}).render('#listing-deconturi');
  		
  		// PAGINATION  set status of navigation buttons on page load
  		setNavigationButtonsState('<portlet:namespace />');
  		$('.navigation').click(function(e) {
  			var elementT 	= e.target;
  			var elementId 	= elementT.id;
  			var action 		= "";
  			
  			// determine which button was pressed and set the value of start accordingly
  			if (elementId.indexOf("first") > -1)	{ action = "first"; start = 0; }
  			if (elementId.indexOf("prev") > -1) 	{ action = "prev"; if (start - count <= 0) {start = 0} else { start -= count; } }
  			if (elementId.indexOf("next") > -1) 	{ action = "next"; start = parseInt(start) + parseInt(count); }
  			if (elementId.indexOf("last") > -1) 	{ action = "last"; start = parseInt(parseInt(total) / parseInt(count)) * count; if (start == total) { start -= count; } }
  			
  			makeAjaxCall(action);
  		});
  		
  		function formatCurrency(cell) {
		    format = {
		    	thousandsSeparator: ",",
		        decimalSeparator: ".",
		        decimalPlaces: noOfDecimalsToDisplay
		    };
		    return Y.DataType.Number.format(Number(cell.value), format);
		}
  		
  		//filtrare dupa docNo
  		$('#filter-nr').keyup(function (e) {
  		    if ($("#filter-nr").is(":focus") && (e.keyCode == 13)) {
  				var elementT 	= e.target;
  				var elementId 	= elementT.id;
  				var action 		= "filter";
  				// get selected value
  				docNo = elementT.value;
  				cuiFurnizor = $('#filter-supplier_code').val();
  				numeFurnizor = $('#filter-supplier_name').val();
  				moneda = $('#filter-moneda').val();
  				compania = $('#filter-societate').val();
  				// reset start page 
  				start = 0;
  				// reset count
  				total = 0;
  				// make ajax call
  				makeAjaxCall(action);
  		    }
  		});
  		
  		//filtrare dupa cui furnizor
  		$('#filter-supplier_code').keyup(function (e) {
  		    if ($("#filter-supplier_code").is(":focus") && (e.keyCode == 13)) {
  				var elementT 	= e.target;
  				var elementId 	= elementT.id;
  				var action 		= "filter";
  				// get selected value
  				cuiFurnizor = elementT.value;
  				docNo = $('#filter-nr').val();
  				numeFurnizor = $('#filter-supplier_name').val();
  				moneda = $('#filter-moneda').val();
  				compania = $('#filter-societate').val();
  				// reset start page 
  				start = 0;
  				// reset count
  				total = 0;
  				// make ajax call
  				makeAjaxCall(action);
  		    }
  		});
  		
  		//filtrare dupa nume furnizor
  		$('#filter-supplier_name').keyup(function (e) {
  		    if ($("#filter-supplier_name").is(":focus") && (e.keyCode == 13)) {
  				var elementT 	= e.target;
  				var elementId 	= elementT.id;
  				var action 		= "filter";
  				// get selected value
  				numeFurnizor = elementT.value;
  				cuiFurnizor = $('#filter-supplier_code').val();
  				docNo = $('#filter-nr').val();
  				moneda = $('#filter-moneda').val();
  				compania = $('#filter-societate').val();
  				// reset start page 
  				start = 0;
  				// reset count
  				total = 0;
  				// make ajax call
  				makeAjaxCall(action);
  		    }
  		});
  		
  		//filtrare dupa moneda
  		$('#filter-moneda').change(function (e) {
  				var elementT 	= e.target;
  				var elementId 	= elementT.id;
  				var action 		= "filter";
  				// get selected value
  				numeFurnizor = $('#filter-supplier_name').val();
  				cuiFurnizor = $('#filter-supplier_code').val();
  				docNo = $('#filter-nr').val();
  				moneda = elementT.value;
  				compania = $('#filter-societate').val();
  				// reset start page 
  				start = 0;
  				// reset count
  				total = 0;
  				// make ajax call
  				makeAjaxCall(action);
  		});
  	
  		//filtrare dupa companie
  		$('#filter-societate').change(function (e) {
  				var elementT 	= e.target;
  				var elementId 	= elementT.id;
  				var action 		= "filter";
  				// get selected value
  				numeFurnizor = $('#filter-supplier_name').val();
  				cuiFurnizor = $('#filter-supplier_code').val();
  				docNo = $('#filter-nr').val();
  				moneda = $('#filter-moneda').val();
  				compania = elementT.value;
  				// reset start page 
  				start = 0;
  				// reset count
  				total = 0;
  				// make ajax call
  				makeAjaxCall(action);
  		});
  		
  		//la click pe inregistrare
  		dataTable.delegate('click', function (e) {
  		    e.preventDefault();
  		    var elem = e.target.get('id');
  		    var docId;
  		    if(elem.indexOf("rowapp") >= 0) {
  		    	docId = elem.replace('rowapp','');
  		    } else {
  		    	docId = elem.replace('row',"");
  		    }
  		  	$('#<portlet:namespace />id').val(docId);
  			$('#<portlet:namespace />registerCashExpense').submit();
  		}, '.editrow', dataTable);
  		
  		function makeAjaxCall(action) {
  			$.ajax({
  				type: "POST",
  				url: "<%=ajaxURL%>",
  				data: 	"<portlet:namespace />action=" + action + 
  						"&<portlet:namespace />start=" + start + 
  						"&<portlet:namespace />count=" + count +
  						"&<portlet:namespace />total=" + total +
  						"&<portlet:namespace />docNo=" + docNo +
  						"&<portlet:namespace />cuiFurnizor=" + cuiFurnizor + 
  						"&<portlet:namespace />numeFurnizor=" + numeFurnizor + 
  						"&<portlet:namespace />moneda=" + moneda + 
  						"&<portlet:namespace />compania=" + compania,
  				success: function(msg) {
  					// get table data
  					if (msg != ""){
  						var jsonEntries = jQuery.parseJSON(msg);
  						total = jsonEntries.total;
  						console.log(jsonEntries.values);
  						dataTable.set('data', eval(jsonEntries.values));
  					} else {
  						dataTable.set('data', []);
  					}
  					
  					// set status of navigation buttons on page load
  					setNavigationButtonsState('<portlet:namespace />');
  				}
  			});
  		}
	});
</script>
<aui:form method="post" action="<%=addEntryURL.toString() %>" name="registerCashExpense" id="registerCashExpense" style="margin:0;padding:0">
	<aui:input type="hidden" id="id" name="id"/>
</aui:form>
