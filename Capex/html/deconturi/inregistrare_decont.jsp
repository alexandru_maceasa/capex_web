<%@page import="com.profluo.ecm.model.db.DefVAT"%>
<%@page import="com.profluo.ecm.flows.CashExpenseStages"%>
<%@page import="com.profluo.ecm.model.db.DatabaseConnectionManager"%>
<%@page import="com.profluo.ecm.model.db.DefSupplier"%>
<%@page import="com.profluo.ecm.model.db.DefExchRates"%>
<%@page import="com.profluo.ecm.controller.ControllerUtils"%>
<%@page import="com.profluo.ecm.model.vo.DefNewProjVo"%>
<%@page import="com.profluo.ecm.model.vo.ListaInitVo"%>
<%@page import="com.profluo.ecm.model.db.CashExpense"%>
<%@page import="com.profluo.ecm.model.vo.DefAttDocsVo"%>
<%@page import="com.profluo.ecm.model.vo.DefVATVo"%>
<%@page import="com.profluo.ecm.model.vo.DefRelComerVo"%>
<%@page import="com.profluo.ecm.model.vo.DefTipFactVo"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.List"%>
<%@page import="com.profluo.ecm.model.vo.DefCurrencyVo"%>
<%@page import="com.profluo.ecm.model.vo.DefCompaniesVo"%>
<%@page import="com.profluo.ecm.model.vo.DefUMVo"%>
<%@page import="com.profluo.ecm.model.vo.DefIASVo"%>

<%@page import="com.profluo.ecm.model.vo.DefIFRSVo"%>
<%@page import="com.profluo.ecm.model.vo.DefLotVo"%>
<%@page import="com.profluo.ecm.model.vo.DefProductVo"%>
<%@page import="com.profluo.ecm.model.vo.DefDpis"%>


<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme"%>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui"%>
<%@ taglib uri="http://alloy.liferay.com/tld/aui" prefix="aui"%>
<%@ taglib uri="http://liferay.com/tld/util" prefix="liferay-util"%>

<%@ page import="java.util.ResourceBundle"%>
<%@ page import="java.util.Locale"%>

<liferay-theme:defineObjects />
<portlet:defineObjects />

<%
		List<HashMap<String,Object>> allCompanies 		= DefCompaniesVo.getInstance();
		List<HashMap<String,Object>> allCurrencies 		= DefCurrencyVo.getInstance();
		List<HashMap<String,Object>> allRelTypes 		= DefRelComerVo.getInstance();
		List<HashMap<String,Object>> allVat 			= DefVATVo.getInstance();
		List<HashMap<String,Object>> allDocs 			= DefAttDocsVo.getInstance();
		List<HashMap<String,Object>> supplierInfo 		= null;
		List<HashMap<String,Object>> productInfo 		= null;
		List<HashMap<String,Object>> oneCashExpenseHeader = null;
		List<HashMap<String,Object>> cashExpenseLines 	= null;
		List<HashMap<String,Object>> cashExpStoreLines	= null;
		
		String status 		= "";
		String vat 			= "";
		String currency 	= "";
		String tip_capex 	= "0";
		String initiativa 	= "0";
		String prj 			= "0";
		String cui 			= "";
		String storeId 		= "";
		String id = "";
		try {
			id = renderRequest.getParameter("id").toString();
			System.out.println("ID = " + id);
		} catch (Exception e) { System.out.println("Eroare la preluarea Id-ului!!!");}
		oneCashExpenseHeader = CashExpense.getHeaderById(id, "cashexp_header");
		vat = oneCashExpenseHeader.get(0).get("vat_code").toString();
		supplierInfo = DefSupplier.getSupplierNameByIdAndCompany(oneCashExpenseHeader.get(0).get("id_supplier").toString(),oneCashExpenseHeader.get(0).get("id_company").toString());
		cashExpenseLines = CashExpense.getCashExpLines(id);
		cashExpStoreLines = CashExpense.getCashExpStoreLines(id);
		if(cashExpStoreLines.size() > 0) {
			storeId += cashExpStoreLines.get(0).get("id_store").toString();
		}
// 		for(int i = 0 ; i < cashExpStoreLines.size(); i++ ){
// 			if(i == cashExpStoreLines.size() - 1) {
// 				storeId += cashExpStoreLines.get(i).get("id_store").toString();
// 			} else {
// 				storeId += cashExpStoreLines.get(i).get("id_store").toString() + ",";
// 			}
// 		}
		if(storeId.equals("")) {
			storeId = "326";
		}
	%>
<%-- Default portletURL --%>
<portlet:renderURL var="portletURL"></portlet:renderURL>

<portlet:resourceURL id="ajaxURL" var="ajaxURL" />
<portlet:actionURL name="addCashExpense" var="submitURL" />
<portlet:renderURL var="backURL">
	<portlet:param name="mvcPath" value="/html/deconturi/view.jsp"></portlet:param>
</portlet:renderURL>

<a href="<%=portletURL%>" class="btn btn-primary-red fr"
	style="margin: 0px">Inapoi</a>


<liferay-ui:error message="insert_nok" key="insert_nok" />
<liferay-ui:error message="update_nok" key="update_nok" />

<%-- Creare formular inregistrare decont --%>
<aui:form action="<%=submitURL%>" method="post" id="ADD-cash-exp" name="ADD-cash-exp">
	<aui:fieldset>
		<legend><strong>Antet document fiscal</strong></legend>
		<aui:layout>
			<aui:column columnWidth="10" first="true">
				<label for="<portlet:namespace />cash_exp_no" style="padding-top: 5px">Numar document</label>
			</aui:column>
			<aui:column columnWidth="20">
				<aui:input type="text" id="cash_exp_no" name="cash_exp_no" value="<%=oneCashExpenseHeader.get(0).get(\"cash_exp_no\") %>" label="">
					<aui:validator name="required"  errorMessage="field-required"></aui:validator>
					<aui:validator name="alphanum" errorMessage="alpha-num"></aui:validator>
				</aui:input>
			</aui:column>
			<aui:column columnWidth="10">
				<label for="<portlet:namespace/>cash_exp_date" style="padding-top: 5px">Data document</label>
			</aui:column>
			<aui:column columnWidth="20">
				<aui:input type="date" id="cash_exp_date" name="cash_exp_date" value="<%=oneCashExpenseHeader.get(0).get(\"cash_exp_date\") %>" 
				placeholder="aaaa-ll-zz" label="">
				<aui:validator name="required"  errorMessage="field-required"></aui:validator>
				</aui:input>
			</aui:column>
			<script type="text/javascript">
				var oldCashExpDate = "";
				YUI({lang:'ro'}).use('aui-datepicker', function(Y) {
					oldCashExpDate = $('#<portlet:namespace />cash_exp_date').val();
					new Y.DatePicker({
						trigger : '#<portlet:namespace />cash_exp_date',
						mask : '%Y-%m-%d',
						popover : {
							zIndex : 1
						},
						calendar : {
							dateFormat : '%Y-%m-%d'
						},
						after: {
				            selectionChange: function(event) {
				            	if (oldCashExpDate != $('#<portlet:namespace />inv_date2').val()) {
				            		oldCashExpDate = $('#<portlet:namespace />inv_date2').val();
				            	}
				            }
				        }
					});
				});
			</script>
			<aui:column columnWidth="10">
				<label for="<portlet:namespace />company"
					style="padding-top: 5px">Societate</label>
			</aui:column>
			<aui:column columnWidth="20" id="societate">
				<aui:select id="company" name="company" label="" required="true">
					<% for (int i = 0; i < allCompanies.size(); i++) { %>
						<% if (allCompanies.get(i).get("status").equals("A")) { %>
							<aui:option value="<%=allCompanies.get(i).get(\"id\")%>"><%=allCompanies.get(i).get("name")%></aui:option>
						<% } %>
					<% } %>
				</aui:select>
			</aui:column>
			<script type="text/javascript">
				$(document).ready(function(e){
					$('#<portlet:namespace/>company').val(<%=oneCashExpenseHeader.get(0).get("id_company")%>);
				});
			</script>
		</aui:layout>
		<aui:layout>
			<aui:column columnWidth="10" first="true">
				<label for="<portlet:namespace />tip-rel-comerciala"
					style="padding-top: 5px">Tip relatie comerciala</label>
			</aui:column>
			<aui:column columnWidth="20">
				<aui:select id="tip-rel-comerciala" name="tip-rel-comerciala" label="" required="true">
					<% for (int i = 0; i < allRelTypes.size(); i++) { %>
						<% if (allRelTypes.get(i).get("status").equals("A")) {%>
							<% if ( (oneCashExpenseHeader.get(0).get("tip_rel_com") != null) && 
									allRelTypes.get(i).get("id").equals(oneCashExpenseHeader.get(0).get("tip_rel_com").toString())) {%>
								<aui:option selected = "selected" value="<%=allRelTypes.get(i).get(\"id\")%>"><%=allRelTypes.get(i).get("name")%></aui:option>
							<% } else { %>
								<aui:option value="<%=allRelTypes.get(i).get(\"id\")%>"><%=allRelTypes.get(i).get("name")%></aui:option>
							<% } %>
						<% } %>
					<% } %>
				</aui:select>
			</aui:column>
			<aui:column columnWidth="10" id = "label_contract_display">
				<label for="<portlet:namespace />nr-contract" style="padding-top: 5px">Numar contract</label>
			</aui:column>
			<aui:column columnWidth="20" id="numar_contract_display">
				<aui:input id="nr-contract" name="nr-contract" label="" value = "<%=oneCashExpenseHeader.get(0).get(\"co_number\") %>">
				</aui:input>
			</aui:column>
			<aui:column columnWidth="10" id="label_comanda_display">
				<label for="<portlet:namespace />nr-comanda" style="padding-top: 5px">Numar comanda</label>
			</aui:column>
			<aui:column columnWidth="20" id="numar_comanda_display">
				<aui:input id="nr-comanda" name="nr-comanda" label="" value = "<%=oneCashExpenseHeader.get(0).get(\"order_no\") %>">
				</aui:input>
			</aui:column>
		</aui:layout>
	</aui:fieldset>
	<aui:fieldset>
		<legend><strong>Informatii document</strong></legend>
		<aui:layout>
			<aui:column columnWidth="10" first="true">
				<label for="<portlet:namespace />datascadenta" style="padding-top: 5px">Data scadenta</label>
			</aui:column>
			<aui:column columnWidth="20">
				<aui:input type="text" id="datascadenta" name="datascadenta" value="<%=oneCashExpenseHeader.get(0).get(\"due_date\") %>" placeholder="aaaa-ll-zz" label="">
					<aui:validator name="required"  errorMessage="field-required"></aui:validator>
				</aui:input>
			</aui:column>
			<script type="text/javascript">
				YUI({lang: ''}).use('aui-datepicker', function(Y) {
					new Y.DatePicker({
						trigger : '#<portlet:namespace />datascadenta',
						mask : '%Y-%m-%d',
						popover : {
							zIndex : 1
						},
						calendar : {
							dateFormat : '%Y-%m-%d'
						}
					});
				});
			</script>
			<aui:column columnWidth="10" first="true">
				<label for="<portlet:namespace />currency" style="padding-top: 5px">Moneda</label>
			</aui:column>
			<aui:column columnWidth="20">
				<aui:select id="currency" name="currency" label="" required="true" >
				<% for (int i = 0; i < allCurrencies.size(); i++) { %>
					<% if (allCurrencies.get(i).get("status").equals("A")) {%>
						<% if (allCurrencies.get(i).get("ref").toString().equals(oneCashExpenseHeader.get(0).get("currency").toString())) { %>
							<aui:option selected="selected"
								value="<%=allCurrencies.get(i).get(\"ref\")%>"><%=allCurrencies.get(i).get("ref")%></aui:option>
						<% }else { %> 
							<aui:option value="<%=allCurrencies.get(i).get(\"ref\")%>"><%=allCurrencies.get(i).get("ref")%></aui:option>
						<% } %>
					<% } %>
				<% } %>
			</aui:select>
			</aui:column>
			<aui:column columnWidth="10">
				<label for="<portlet:namespace />rata-schimb"
					style="padding-top: 5px">Rata de schimb</label>
			</aui:column>
			<aui:column columnWidth="20">
				<aui:input type="text" id="rata-schimb" name="rata-schimb" value="<%=oneCashExpenseHeader.get(0).get(\"exchange_rate\")%>"
					label="">
					<aui:validator name="required"  errorMessage="field-required"></aui:validator>
				</aui:input>
			</aui:column>
		</aui:layout>
	</aui:fieldset>
	<aui:fieldset>
		<aui:column columnWidth="10" first="true">
			<label for="<portlet:namespace />cota_tva" style="padding-top: 5px">Cota TVA</label>
		</aui:column>
		<aui:column columnWidth="20">
			<aui:select id="cota_tva" name="cota_tva" label="" required="true" autocomplete="off">
				<% for (int i = 0; i < allVat.size(); i++) { %>
					<% if (allVat.get(i).get("status").equals("A")) {%>
						<% if (allVat.get(i).get("ref").toString().equals(vat)) { %>
						<aui:option selected="true" value="<%=allVat.get(i).get(\"ref\")%>"><%=allVat.get(i).get("ref")%>%</aui:option>
						<% } else { %>
						<aui:option value="<%=allVat.get(i).get(\"ref\")%>"><%=allVat.get(i).get("ref")%>%</aui:option>
						<% } %>
					<% } %>
				<% } %>
			</aui:select>
		</aui:column>
		<aui:column columnWidth="10" first="true">
			<label for="<portlet:namespace />procent_garantie"
				style="padding-top: 5px">Procent garantie</label>
		</aui:column>
		<aui:column columnWidth="20">
			<aui:input type="text" id="procent_garantie" name="procent_garantie" value="<%=(oneCashExpenseHeader.get(0).get(\"warranty\") == null ? \"0\" : oneCashExpenseHeader.get(0).get(\"warranty\"))%>" 
			placeholder="x%" label="">
				<aui:validator name="required"  errorMessage="field-required"></aui:validator>
			</aui:input>
		</aui:column>
	</aui:fieldset>
	
<%--calculare valoare currency --%>
<aui:fieldset id="valoareCurrency" style="display:none">
	<legend id="currency_label"></legend>
	<aui:layout>
		<aui:column columnWidth="10">
			<label for="<portlet:namespace />val_no_vat_curr"
				style="padding-top: 5px">Valoare</label>
		</aui:column>
		<aui:column columnWidth="20">
			<aui:input cssClass="currencyCol" type="text" id="val_no_vat_curr" name="val_no_vat_curr" value="<%=oneCashExpenseHeader.get(0).get(\"val_no_vat_curr\")%>" label="">
			</aui:input>
		</aui:column>
		<aui:column columnWidth="10">
			<label for="<portlet:namespace />val_vat_curr" style="padding-top: 5px">TVA</label>
		</aui:column>
		<aui:column columnWidth="20">
			<aui:input cssClass="currencyCol" type="text" id="val_vat_curr" name="val_vat_curr" value="<%=oneCashExpenseHeader.get(0).get(\"val_vat_curr\")%>" label="" >
			</aui:input>
		</aui:column>
		<aui:column columnWidth="10">
			<label for="<portlet:namespace />val_with_vat_curr"
				style="padding-top: 5px">Valoarea cu TVA</label>
		</aui:column>
		<aui:column columnWidth="20">
			<aui:input cssClass="currencyCol" type="text" id="val_with_vat_curr" name="val_with_vat_curr" value="<%=oneCashExpenseHeader.get(0).get(\"total_with_vat_curr\")%>" label="">
			</aui:input>
		</aui:column>
	</aui:layout>
	</aui:fieldset>
	
	<%--calculare valoare RON --%>
	<aui:fieldset id="valoareRON">
		<legend>Valoare RON</legend>
		<aui:layout>
			<aui:column columnWidth="10">
				<label for="<portlet:namespace />val_no_vat_ron"
					style="padding-top: 5px">Valoare</label>
			</aui:column>
			<aui:column columnWidth="20">
				<aui:input cssClass="currencyCol" type="text" id="val_no_vat_ron" name="val_no_vat_ron" 
					value="<%=oneCashExpenseHeader.get(0).get(\"val_no_vat_ron\")%>" label="">
					<aui:validator name="required"  errorMessage="field-required"></aui:validator>
				</aui:input>
			</aui:column>
			<aui:column columnWidth="10">
				<label for="<portlet:namespace />val_vat_ron" style="padding-top: 5px">TVA</label>
			</aui:column>
			<aui:column columnWidth="20">
				<aui:input cssClass="currencyCol" type="text" id="val_vat_ron" name="val_vat_ron" value="<%=oneCashExpenseHeader.get(0).get(\"val_vat_ron\")%>" label="">
					<aui:validator name="required"  errorMessage="field-required"></aui:validator>
				</aui:input>
			</aui:column>
			<aui:column columnWidth="10">
				<label for="<portlet:namespace />val_with_vat_ron"
					style="padding-top: 5px">Valoare cu TVA</label>
			</aui:column>	
			<aui:column columnWidth="20">
				<aui:input cssClass="currencyCol" type="text" id="val_with_vat_ron" name="val_with_vat_ron" value="<%=oneCashExpenseHeader.get(0).get(\"total_with_vat_ron\")%>" label="">
					<aui:validator name="required"  errorMessage="field-required"></aui:validator>
				</aui:input>
			</aui:column>
		</aui:layout>
	</aui:fieldset>
	
<aui:fieldset>
	<legend>Furnizor</legend>
	<aui:layout>
		<aui:column columnWidth="10">
			<label for="<portlet:namespace />supplier" style="padding-top: 5px">Furnizor</label>
		</aui:column>
		<aui:column columnWidth="20" id="supplier">
			<aui:input id="supplier" readonly="true" name="supplier" label="" value = "<%=supplierInfo.get(0).get(\"name\") %>">
			</aui:input>
		</aui:column>
		<aui:column columnWidth="10">
			<label for="<portlet:namespace />cui" style="padding-top: 5px">CUI</label>
		</aui:column>
		<aui:column columnWidth="20" id="cui">
			<aui:input id="cui" name="cui" readonly="true" label="" value = "<%=supplierInfo.get(0).get(\"cui\") %>">
			</aui:input>
		</aui:column>
		<aui:column columnWidth="10">
			<label for="<portlet:namespace />associated_acc" style="padding-top: 5px">Cont asociat furnizorului</label>
		</aui:column>
		<aui:column columnWidth="20" id="associated_acc">
			<aui:input id="associated_acc" readonly="true" name="associated_acc" label="" value = "<%=supplierInfo.get(0).get(\"bank_acc\") %>">
			</aui:input>
		</aui:column>
	</aui:layout>
</aui:fieldset>

<aui:fieldset>
	<aui:layout>
		<%@include file="../includes/capex_details.jsp" %>
	</aui:layout>
</aui:fieldset>

<!-- INCLUD SURSA PENTRU ALOCAREA DE LINII -->
<%@ include file="alocare_linii.jsp" %>
	
<aui:input type="hidden" name="id_cash_exp" id="id_cash_exp" value='<%=oneCashExpenseHeader.get(0).get("id").toString()%>' />
<aui:input type="hidden" name="datatable_lines" id="datatable_lines" value="" />
<aui:input type="hidden" name="datatable_data" id="datatable_data" value="" />
<aui:button type="button" value="Validare" onclick="submitFormular()" cssClass="btn btn-primary fr" style="margin-right:10px" /><br/>
	
</aui:form>

<script type="text/javascript">
	function submitFormular(){
		submitCashExpenseForm('<portlet:namespace />');
	}
</script>
