<%@page import="com.profluo.ecm.controller.ControllerUtils"%>
<%@page import="com.profluo.ecm.model.vo.DefCatGestVo"%>
<%@page import="com.profluo.ecm.model.vo.DefAisles"%>
<%@page import="com.profluo.ecm.model.vo.DefDpis"%>
<%@ page import="com.profluo.ecm.model.vo.DefIFRSVo"%>
<%@ page import="com.profluo.ecm.model.vo.DefLotVo"%>
<%@ page import="com.profluo.ecm.model.vo.DefProductVo"%>
<%@ page import="com.profluo.ecm.model.vo.DefIASVo"%>
<%@ page import="com.profluo.ecm.model.db.DefIAS"%>
<%@ page import="com.profluo.ecm.model.vo.DefUMVo"%>
<%@ page import="com.profluo.ecm.model.vo.DefStoreVo"%>
<%@ page import="com.profluo.ecm.model.vo.DefRegEquipVo"%>
<%@ page import="com.profluo.ecm.model.vo.DefVATVo"%>
<%@ page import="com.profluo.ecm.model.vo.DefMFActVo"%>
<%@ page import="java.util.HashMap"%>
<%@ page import="java.util.List"%>

<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme"%>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui"%>
<%@ taglib uri="http://alloy.liferay.com/tld/aui" prefix="aui"%>
<%@ taglib uri="http://liferay.com/tld/util" prefix="liferay-util"%>

<%
// retrieve all mf actions
List<HashMap<String,Object>> allMF = DefMFActVo.getInstance();
// retrieve all VAT values
List<HashMap<String,Object>> allVAT = DefVATVo.getInstance();
// tipuri inregistrare echipament
List<HashMap<String,Object>> allRegEquip = DefRegEquipVo.getInstance();
// get all stores
List<HashMap<String,Object>> allStores = DefStoreVo.getInstance();
// get all units of measure VO
List<HashMap<String,Object>> allUM = DefUMVo.getInstance();
// def all ias
List<HashMap<String,Object>> allIAS = DefIASVo.getInstance();
// def all ifrs
List<HashMap<String,Object>> allIFRS = DefIFRSVo.getInstance();
// def all prods
List<HashMap<String,Object>> allProds = DefProductVo.getInstance();
// def all lots
List<HashMap<String,Object>> allLots = DefLotVo.getInstance(); 
%>

<portlet:resourceURL id="ajaxURL" var="ajaxURL"/>

<legend><strong>Articole</strong></legend>
<%-- Lines table --%>
<aui:layout>
	<aui:column first="true" columnWidth="100" last="true">
		<div id="alocare-articole"></div>
	</aui:column>
	<aui:column last = "true">
		<a href="#" class="add_all btn btn-primary">Aloca toate articolele</a>
	</aui:column>
</aui:layout>
<aui:layout>
	<aui:column>
		<a href="#" class="add_article_line" onclick="javascript:;return false;">Adauga articol +</a>
	</aui:column>
</aui:layout>
<%-- Distributed lines --%>
<legend><strong>Alocare articol pe linii</strong></legend>
<aui:layout>
	<aui:column first="true" columnWidth="100" last="true">
		<div id="alocare-linii"></div>
	</aui:column>
</aui:layout>

<aui:layout>
	<aui:column cssClass="currencyCol">
		<label for="<portlet:namespace />valoare_totala"
			style="padding-top: 5px">Valoare fara TVA</label>
	</aui:column>
	<aui:column>
		<aui:input cssClass="currencyCol" name="valoare_totala" id="valoare_totala"
			value="" label="" readonly="readonly" />
	</aui:column>
	<aui:column cssClass="currencyCol">
		<label for="<portlet:namespace />valoare_totala_alocata"
			style="padding-top: 5px">Suma fara TVA alocare linii</label>
	</aui:column>
	<aui:column>
		<aui:input cssClass="currencyCol" name="valoare_totala_alocata" id="valoare_totala_alocata"
			value="" label="" readonly="readonly" />
	</aui:column>
	<aui:column>
		<label for="<portlet:namespace />difference" style="padding-top: 5px"><liferay-ui:message key="difference"/></label>
	</aui:column>
	<aui:column>
		<aui:input cssClass="currencyCol" name="difference" id="difference" value="" label=""
			readonly="readonly">
			<aui:validator name="custom" errorMessage="incorect">
				function (val, fieldNode, ruleValue) {
					var result = false;
					if (val == 0) {
						result = true;
					}
					return result;
				}
			</aui:validator>
		</aui:input>
	</aui:column>
</aui:layout>

<div id="overlay-detalii-inv" class="yui3-overlay-loading" style="left:-350px;position:absolute;width: 280px;z-index: 1">
    <div class="yui3-widget-hd" style="font-size: 12px">
    	Detalii inventar pentru articolul: <span id="cod_art_header" style="display:none">&nbsp;</span> <span id="den_art_header">&nbsp;</span>
    </div>
    <div class="yui3-widget-bd">
		<aui:layout>
			<aui:column first="true" columnWidth="100" last="true" style="position:relative">
				<div id="detalii-inventar"></div>
			</aui:column>
		</aui:layout>
		<aui:layout>
			<aui:column columnWidth="50" first="true">
				<aui:input value="Inchide" name="fcancel" id="finvcancel" type="button" label="" cssClass="btn btn-primary-red fl"/>
			</aui:column>
			<aui:column columnWidth="50" last="true">
				<aui:input id="salveaza-inv" name="salveaza-inv" value="Salveaza" type="button" label="" cssClass="btn btn-primary fr"/>
			</aui:column>
		</aui:layout>
    </div>
</div>
<%-- overlay solicita vizualizare DPI --%>
<div id="vizualizareDPI" class="yui3-overlay-loading"
	style="right: 100px; z-index: 1; position: absolute; width: 800px; display: none">
	<div class="yui3-widget-hd">Solicitare vizualizare DPI</div>
	<div class="yui3-widget-bd">
		<aui:layout>
			<aui:fieldset>
				<aui:column columnWidth="20">
					<label for="<portlet:namespace />nr_dpi" style="padding-top: 5px">Nr DPI: </label>
				</aui:column>
				<aui:column columnWidth="30">
					<aui:input id="nr_dpi" name="nr_dpi" label="" readonly="true"></aui:input>
				</aui:column>
				<aui:column columnWidth="20">
					<label for="<portlet:namespace />societate"
						style="padding-top: 5px">Societate:</label>
				</aui:column>
				<aui:column columnWidth="30">
					<aui:input id="societate" name="societate" label="" readonly="true"></aui:input>
				</aui:column>
			</aui:fieldset>

			<aui:fieldset>
				<aui:column columnWidth="20">
					<label for="<portlet:namespace />data_dpi" style="padding-top: 5px">Data DPI: </label>
				</aui:column>
				<aui:column columnWidth="30">
					<aui:input id="data_dpi" name="data_dpi" label="" readonly="true"></aui:input>
				</aui:column>
				<aui:column columnWidth="20">
					<label for="<portlet:namespace />categ_capex_dpi"
						style="padding-top: 5px">Categorie Capex:</label>
				</aui:column>
				<aui:column columnWidth="30">
					<aui:input id="categ_capex_dpi" name="categ_capex_dpi" label=""
						readonly="true"></aui:input>
				</aui:column>
			</aui:fieldset>

			<aui:fieldset>
				<aui:column columnWidth="20">
					<label for="<portlet:namespace />solicitant"
						style="padding-top: 5px">Solicitant: </label>
				</aui:column>
				<aui:column columnWidth="30">
					<aui:input id="solicitant" name="solicitant" label=""
						readonly="true"></aui:input>
				</aui:column>
			</aui:fieldset>

			<aui:fieldset>
				<aui:column columnWidth="20">
					<label for="<portlet:namespace />aprobator"
						style="padding-top: 5px">Aprobator: </label>
				</aui:column>
				<aui:column columnWidth="80">
					<aui:input id="aprobator" name="aprobator" label="" readonly="true"
						style="width: 97.6%"></aui:input>
				</aui:column>
			</aui:fieldset>
			<aui:layout>
				<aui:column columnWidth="100" last="true" first="true">
					<div id="tabelDPI"></div>
				</aui:column>
			</aui:layout>
			<aui:layout>
				<aui:column columnWidth="50" last="true" style="text-align:right">
					<aui:input value="Inchide" name="fcancel" type="button" label="" cssClass="btn btn-primary-red fr"
						onclick="$('#vizualizareDPI').hide()"></aui:input>
					<div class="clear"></div>
				</aui:column>
			</aui:layout>
		</aui:layout>
	</div>
</div>


<script type="text/javascript">
// global variable for tables in order to be able use them in other scripts
var dataTable, dataTableSplit;
// global variables used to save inventory numbers on input type hidden
var idLine, denArt, codArt, tipInv;
var storeId = "<%=storeId%>";
var selectedCompany = '';
var dataTableInv = null;
var counter_cash_exp_lines = <%=cashExpenseLines.size()%>;

YUI({lang: 'ro'}).use( 'aui-datatable', 'aui-datatype', 'datatable-sort', 'overlay', 'datatable-scroll', 'aui-base', 'liferay-portlet-url', 'aui-node','datatable-mutable',
		function(Y) {
			/** GENERIC CURRENCY FORMATTER **/
			function formatCurrency(cell) {
			    format = {
			    	thousandsSeparator: ",",
			        decimalSeparator: ".",
			        decimalPlaces: noOfDecimalsToDisplay
			    };
			    return Y.DataType.Number.format(Number(cell.value), format);
			}
			/** GENERIC SAVE ON ENTER **/
			function saveOnEnter(e) {
				if (e.domEvent.charCode == 13) {
					if (!e.target.validator.hasErrors()) {
						e.target.fire('save', {
			                newVal: e.target.getValue(),
			                prevVal: e.target.get('value')
			            });
					}
				}
			}
			/** GENERIC SAVE ON CHANGE ON SELECT **/
			function saveOnChange(e) {
				e.target.fire('save', {
		            newVal: e.target.getValue(),
		            prevVal: e.target.get('value')
		        });
			}
			// editor attributes, actions and formats based on datatypes
			var attrNumberEditor = {inputFormatter: Y.DataType.Number.evaluate, validator: { rules: { value: { number: true } } }, on : {keyup: saveOnEnter}, showToolbar: false };
			var attrStringEditor = {on :{keyup: saveOnEnter}, showToolbar: false};
			
			$("#<portlet:namespace/>finvcancel").click(function () { saveInventoryOverlayCashExp('<portlet:namespace />'); });
			$("#<portlet:namespace/>salveaza-inv").click(function () { saveInventoryOverlayCashExp('<portlet:namespace />'); });
			
			var allDPIs = <%=DefDpis.getJsonStringDpis(true)%>;

			var raionDepartament = <%=DefAisles.getJsonStringAisles(storeId)%>;
			var UMList = [<% for (int i = 0; i < allUM.size(); i++) { %>
							<% if (allUM.get(i).get("status").equals("A")) { %>
								<% if (allUM.size() - 1 == i) { %>
									'<%=allUM.get(i).get("ref")%>'
								<% } else { %>
									'<%=allUM.get(i).get("ref")%>',
								<% } %>
							<% } %>
						<% } %>];
			var iasList = <%=DefIASVo.getJsonStringIAS()%>;
			var ifrsList = <%=DefIFRSVo.getJsonStringIFRS()%>;
			var allProds = <%=DefProductVo.getJsonStringProduct()%>;
			var allLots = <%=DefLotVo.getJsonStringLots()%>;
			var clasif = {1:'IT Hardware', 2:'IT Software', 0:'Active.'};
			var tvalist = {<% for (int i = 0; i < allVAT.size(); i++) { %>
								<% if (allVAT.get(i).get("status").equals("A")) { %>
									<% if (allVAT.size() - 1 == i) { %>
										'<%=allVAT.get(i).get("ref")%>':'<%=allVAT.get(i).get("ref")%>%'
									<% } else { %>
										'<%=allVAT.get(i).get("ref")%>':'<%=allVAT.get(i).get("ref")%>%',
									<% } %>
								<% } %>
							<% } %>};
			var tipinventar = {1: "individual", 2: "de grup"};
			var tipinregistrare = <%=DefRegEquipVo.getJsonStringRegEquip()%>;
			var actiunimf=<%=DefMFActVo.getJsonStringMfAct()%>;
			var cat_gestiune=<%=DefCatGestVo.getJsonStringCatGest()%>;
			
			//initialize store variables based on companies
			<%for (int i = 0; i < allCompanies.size(); i++) { %>
				<%if (allCompanies.get(i).get("status").toString().equals("A")) { %>
				var store_<%=allCompanies.get(i).get("id")%> = <%=DefStoreVo.getJsonStringStoresByCompany(allCompanies.get(i).get("id").toString())%>
				<% } %>
			<% } %>
			
			
			/***********************************/
			/**	   Definitions Datatables	  **/
			/***********************************/
			
			/******************* DPI Datatable ************/
			var datahDPI = [];
			
			var colsDPI = [
					{ key: 'id_prod', 				label:'Cod produs'},
					{ key: 'description', 			label:'Denumire produs'},
					{ key: 'unit_price_curr', 		label:'Pret Unitar'},
					{ key: 'quantity', 				label:'Cantitate'},
					{ key: 'total_with_vat_curr', 	label:'Valoare totala'},
					{ key: 'magazin', 				label:'Magazin'},
					{ key: 'clasificare_it', 		label:'Clasificare IT'}
			];
			
			dataTableDPI = new Y.DataTable({
				columns: colsDPI,
				data:datahDPI
			}).render('#tabelDPI');	
			
			/************************* CashExpense Lines Datatable *********************/
			// data array - main table
			var remoteData = <%= ControllerUtils.generateJSONcashExpLines(cashExpenseLines) %>
			
			// COLUMN INFO and ATTRIBUTES
			var nestedCols = [
			    { key: 'id_cash_exp', 	className: 'hiddencol'},
			    { key: 'cash_exp_line_id', 	className: 'hiddencol'},
				{ key: 'delete_line', label: ' ', allowHTML: true, 
					// gets the id of the row and creates an unique link for every row
					formatter: function(o) {
						if(o.data.cash_exp_line_id != '') {
							return '<a href="#" id="row' + o.data.cash_exp_line_id + '" class="deleteLineCashExp"><img src="${themeDisplay.getPathThemeImages()}/delete.png" width="20"/></a>';
						} else {
							return '<a href="#" id="row' + o.value + '" class="deleteLineCashExp"><img src="${themeDisplay.getPathThemeImages()}/delete.png" width="20"/></a>';
						}
						
					}
				},
				{ key: 'line', 			label: 'Nr.' },
				{ key: 'counter', 	className: 'hiddencol'},
				{ key: 'product_code', 		label: 'Cod Prod', 		
					editor: new Y.DropDownCellEditor({options: allProds, id: 'allprods', elementName: 'allprods',
						after: {
				            focus: function(event) {
					            $("select[name=allprods]").chosen();
				            }
						}
					}),
					emptyCellValue: '',
					formatter: function(o) {
						var obj = JSON.parse(JSON.stringify(allProds));
						
						if (typeof o.value != 'undefined') {
							return obj[o.value].toString();
						} else {
							return '';
						}
					}
				},
				{ key: 'description',		label: 'Den. articol.', editor: new Y.TextCellEditor(attrStringEditor)},
				{ key: 'um', 				label: 'UM', 			editor: new Y.DropDownCellEditor({options: UMList, on : {change: saveOnChange}, showToolbar: false})},
				{ key: 'quantity', 			label: 'Cant',	 		editor: new Y.TextCellEditor(attrNumberEditor )},
				{ key: 'unit_price_curr', 	label: 'Pret unitar', 	editor: new Y.TextCellEditor(attrNumberEditor), formatter: formatCurrency},
				{ key: 'price_no_vat_curr', 	label: 'Val fara TVA', 	formatter: formatCurrency, className: 'currencyCol' /*editor: new Y.TextCellEditor(attrNumberEditor)*/},
				{ key: 'price_vat_curr', className: 'hiddencol'},
				{ key: 'vat_code', 			label: 'Cota TVA', 		editor: new Y.DropDownCellEditor({options: tvalist, on : {change: saveOnChange}, showToolbar: false}),
					//formatter:	function(o) { return o.value + '%'; }
					formatter: function(o) {
						var obj = JSON.parse(JSON.stringify(tvalist));
						if (typeof o.value != 'undefined') {
							return obj[o.value].toString();
						} else {
							return '';
						}
					}
				},
				{ key: 'price_with_vat_curr', 	label: 'Val cu TVA', formatter: formatCurrency, className: 'currencyCol' 	/*editor: new Y.TextCellEditor(attrNumberEditor), formatter: formatCurrency*/},			
				{ key: 'id_ias', 	label: 'Grupa IAS', 	
					editor: new Y.DropDownCellEditor(
	   						{options: iasList, id: 'allias', elementName: 'allias',
								after: {
				            focus: function(event) {
					            $("select[name=allias]").chosen();
				           	 		}
								}
	   						}),
					emptyCellValue: '',
					formatter: function(o) {
						var obj = JSON.parse(JSON.stringify(iasList));
						
						if (typeof o.value != 'undefined') {
							return obj[o.value].toString();
						} else {
							return '';
						}
					}
				},
				{ key: 'id_ifrs', 	label: 'Grupa IFRS', 	
					editor: new Y.DropDownCellEditor(
	   						{options: ifrsList, id: 'allifrs', elementName: 'allifrs',
								after: {
				            focus: function(event) {
					            $("select[name=allifrs]").chosen();
				           	 		}
								}
	   						}),
					emptyCellValue: '',
					formatter: function(o) {
						var obj = JSON.parse(JSON.stringify(ifrsList));
						
						if (typeof o.value != 'undefined') {
							return obj[o.value].toString();
						} else {
							return '';
						}
					}
				},
				{ key: 'dt_ias', 		label: 'Dt. RAS', editor: new Y.TextCellEditor({on : {keyup: saveOnEnter}, showToolbar: false})},
				{ key: 'dt_ifrs', 		label: 'Dt. IFRS', editor: new Y.TextCellEditor({on : {keyup: saveOnEnter}, showToolbar: false})},
				{ key: 'lot', 			label: 'Lot', 			editor: new Y.DropDownCellEditor({options: allLots, on : {change: saveOnChange}, showToolbar: false}),
					emptyCellValue: '',
					formatter: function(o) {
						var obj = JSON.parse(JSON.stringify(allLots));
						
						if (typeof o.value != 'undefined') {
							return obj[o.value].toString();
						} else {
							return '';
						}
					}
				},
				{ key: 'clasif', 		label: 'Clasif.', 		editor: new Y.DropDownCellEditor({options: clasif, on : {change: saveOnChange}, showToolbar: false}), 
					emptyCellValue: '',
					formatter: function(o) {
						var obj = JSON.parse(JSON.stringify(clasif));
						return obj[o.value].toString();
					}
				},
				{ key: 'detalii', 		label: 'Actiuni', allowHTML: true, 
					// gets the id of the row and creates an unique link for every row
					formatter: function(o) {
						return '<a id="row' + o.value + '" class="allocline">Aloc. art. pe linii</a>';
					}, 
					emptyCellValue: '<a id="row{value}" class="allocline">Aloc. art. pe linii</a>', 
				},
				{ key: 'cont_imob_in_fct', 	className: 'hiddencol'},
				{ key: 'cont_imob_in_curs', 	className: 'hiddencol'},
				{ key: 'cont_imob_avans', 	className: 'hiddencol'},
				{ key: 'is_new_line', 	className: 'hiddencol'}
			];

			// TABLE INIT
			dataTable = new Y.DataTable({
			    columns: nestedCols,
			    data: remoteData,
			    editEvent: 'click',
			    recordType: ['id_cash_exp', 'cash_exp_line_id', 'delete_line', 'line', 'counter', 'product_code', 'description', 'um', 'quantity', 'unit_price_curr', 
			                 'price_no_vat_curr', 'price_vat_curr', 'vat_code', 'price_with_vat_curr', 'id_ias', 'id_ifrs', 'dt_ias', 'dt_ifrs', 'lot', 'clasif', 
			                 'detalii', 'cont_imob_in_fct', 'cont_imob_in_curs', 'cont_imob_avans', 'is_new_line']
			}).render('#alocare-articole');
			
			/************************* CashExpense StoreLines Datatable *********************/
			// bottom table values
			var remoteDataSplit = <%= ControllerUtils.generateJSONcashExpStoreLines(cashExpStoreLines, oneCashExpenseHeader.get(0).get("id_company").toString()) %>
			
			// bottom table header
			var nestedColsSplit = [
				{ key: 'id_cash_exp', className: 'hiddencol'},
				{ key: 'id_cash_exp_line', className: 'hiddencol'},
				{ key: 'id_store_line', className: 'hiddencol'},
				{ key: 'id_store', className: 'hiddencol'},
				{ key: 'line', className: 'hiddencol'},
				{ key: 'delete_store_line', label: ' ', allowHTML: true, 
					// gets the id of the row and creates an unique link for every row
					formatter: function(o) {
						if(o.data.id_store_line != '') {
							return '<a href="#" id="row' + o.data.id_store_line + '" class="deleteStoreLineCashExp"><img src="${themeDisplay.getPathThemeImages()}/delete.png" width="20"/></a>';
						} else {
							return '<a href="#" id="row' + o.value + '" class="deleteStoreLineCashExp"><img src="${themeDisplay.getPathThemeImages()}/delete.png" width="20"/></a>';
						}
					}
				},
			   	{ key: 'description', label: 'Den. articol.'},
				{ key: 'quantity', label:  'Cant'},
				{ key: 'price_no_vat_curr', label: 'Val fara TVA', formatter: formatCurrency},
				<% for (int i = 0; i < allCompanies.size(); i++) { %>
				<% if (allCompanies.get(i).get("status").toString().equals("A")) { %> 
			   	{ key: 'id_store_<%=allCompanies.get(i).get("id")%>', className :'hiddencol stores_<%=allCompanies.get(i).get("id")%>',
					label: 'Magazin',editor: new Y.DropDownCellEditor({options: store_<%=allCompanies.get(i).get("id")%>, id: 'store_<%=allCompanies.get(i).get("id")%>', elementName: 'store_<%=allCompanies.get(i).get("id")%>',
					after: {
			            focus: function(event) {
				            $("select[name=store_<%=allCompanies.get(i).get("id")%>]").chosen();
			            }
					}
				}),
				emptyCellValue: '',
				formatter: function(o) {
					var obj = JSON.parse(JSON.stringify(store_<%=allCompanies.get(i).get("id")%>));
					
					if (typeof o.value != 'undefined') {
						if(o.value != null) {
							return obj[o.value].toString();
						}else return '';
					} else {
						return '';
					}
				}
				},
				<% } %>
				<% } %>
				{ key: 'id_aisle', label: 'Raion/Dep.',editor: new Y.DropDownCellEditor({options: raionDepartament, id: 'raionDepartament', elementName: 'raionDepartament',
					after: {
			            focus: function(event) {
 				            $("select[name=raionDepartament]").chosen();
			            }
					}
					}),
					emptyCellValue: '',
					formatter: function(o) {
						var obj = JSON.parse(JSON.stringify(raionDepartament));
						
						if (typeof o.value != 'undefined') {
							if(o.value != null) {
								raionOrDept = o.value;
								console.log("aici : " + o.value);
								return obj[o.value].toString();
							} else return '';
						} else {
							return '';
						}
					}
				},
				/*{ key: 'price_vat_ron', className: 'hiddencol'},*/
				{ key: 'associated_acc', label: 'Cont', editor: new Y.TextCellEditor(attrNumberEditor)}, 
				{ key: 'id_tip_inreg', label: 'Tip inregistrare', 
					editor: new Y.DropDownCellEditor({options: tipinregistrare, on : {change: saveOnChange}, showToolbar: false}),
					emptyCellValue: '1',
					formatter: function(o) {
						var obj = JSON.parse(JSON.stringify(tipinregistrare));
						return obj[o.value].toString();
					}
				},
				{ key: 'id_act_mf', label: 'Actiune MF', 
					editor: new Y.DropDownCellEditor({options: actiunimf, on : {change: saveOnChange}, showToolbar: false}),
					emptyCellValue: '1',
					formatter: function(o) {
						var obj = JSON.parse(JSON.stringify(actiunimf));
						return obj[o.value].toString();
					}
				},
				{ key: 'gestiune', label : 'Gestiune', editor: new Y.DropDownCellEditor({options: cat_gestiune, on : {change: saveOnChange}, showToolbar: false}),
	   				emptyCellValue: '',
	   				formatter: function(o) {
	   					var obj = JSON.parse(JSON.stringify(cat_gestiune));
	   					
	   					if (typeof o.value != 'undefined') {
	   						return obj[o.value].toString();
	   					} else {
	   						return '0';
	   					}
	   				}
	   			},
				{ key: 'id_dpi', label: 'Nr. DPI', editor: new Y.DropDownCellEditor({options: allDPIs, id: 'alldpis', elementName: 'alldpis',
	   				after: {
	   		            focus: function(event) {
	   			            $("select[name=alldpis]").chosen();
	   		            }
	   				}
	   			}), formatter: function(o) {
		   				var obj = JSON.parse(JSON.stringify(allDPIs));
		   				if (typeof o.value != 'undefined') {
		   					return obj[o.value].toString();
		   				} else {
		   					return '0';
		   				}
	   				}
	   			},
	   			{ key: 'vizualizare_dpi', label: 'Link DPI', 
	   					allowHTML: true, 
	   					formatter: function(o) { 
	   						return '<a href="#" class="dpilink" id="dpi_' + o.data.line + '_' + o.value + '">DPI</a>';
	   					},
	   					emptyCellValue: '<a href="#" class="dpilink" id="dpi_{line}_{value}">DPI</a>'},
				{ key: 'id_tip_inventar', label: 'Tip nr. inventar', 
					editor: new Y.DropDownCellEditor({options: tipinventar, on : {change: saveOnChange}, showToolbar: false}),
					emptyCellValue: '2',  // de grup - default
					formatter: function(o) {
						var obj = JSON.parse(JSON.stringify(tipinventar));
						return obj[o.value].toString();
					}
				},
				{ key: 'detalii', label: 'Nr. inventar', allowHTML: true, 
					// gets the id of the row and creates an unique link for every row
					// formatter: '<a href="#" id="line-{nr}" class="proddet">detalii</a>', 
					formatter: function(o) {
						return '<a href="#" id="line-' + o.value + '" class="proddet">detalii</a>';
					}, 
					emptyCellValue: '<a href="#" id="line-{line}" class="proddet">detalii</a>', 
				},
				{ key: 'cont_imob_in_fct', 	className: 'hiddencol'},
				{ key: 'cont_imob_in_curs', className: 'hiddencol'},
				{ key: 'cont_imob_avans', 	className: 'hiddencol'},
				{ key: 'inventory', 		className: 'hiddencol'},
			];
			
			// TABLE INIT - bottom
			dataTableSplit = new Y.DataTable({
			    columns: nestedColsSplit,
			    data: remoteDataSplit,
			    editEvent: 'click',
			    recordType: ['id_cash_exp', 'id_cash_exp_line','id_store_line', 'line', 'delete_store_line','description','id_store_1','id_store_2','id_store_3','id_store_4','id_store_5',
			                 'id_store_6','id_store_7','id_store_8', 'id_store_9', 'id_store', 'quantity','price_no_vat_curr','id_aisle','associated_acc','id_tip_inreg','id_act_mf','gestiune','id_dpi','vizualizare_dpi',
			                 'id_tip_inventar','detalii','cont_imob_in_fct','cont_imob_in_curs','cont_imob_avans','inventory']
			}).render('#alocare-linii');
			
			
			/************************* Inventory Pop-up *********************/
			var remoteDataInv = []; 
     		
			var nestedColsInv = [
			      			{ key: 'nr_inv', 	label: 'Nr. inv.', editor: new Y.TextCellEditor({on : {keyup: saveOnEnter}, showToolbar: false})},
			      			{ key: 'data_pif', 	label: 'Data PIF', editor: new Y.DateCellEditor({dateFormat: '%Y-%m-%d'})},
			      			{ key: 'receptie', 	label: 'Receptie', editor: new Y.TextCellEditor(attrStringEditor)}
			      			];
	  		
	  		// TABLE INIT
	  		dataTableInv = new Y.DataTable({
	  		    columns: nestedColsInv,
	  		    data: remoteDataInv,
	  		    editEvent: 'click'
	  		}).render('#detalii-inventar');
			
			/**********************************************/
			/*			END datatables definitions		  */
			/**********************************************/
			
			// Changes of info on the line table
			dataTable.after('record:change', function (e) {
				var obj = e.target.changed;
				var existingData = e.target._state.data;
				//there was a change on product code => add new peoduct
				if (typeof obj.product_code != 'undefined') {
					setProductValues(obj.product_code, false);
				}
				//there was a change on ias => get ias info
				if (typeof obj.id_ias != 'undefined') {
					setIasDtValuesCashExp(obj.id_ias, '<portlet:namespace />', '<%=ajaxURL%>');
				}
				//there was a change on ifrs => get ifrs info
				if (typeof obj.id_ifrs != 'undefined') {
					setIfrsDtValuesCashExp(obj.id_ifrs, '<portlet:namespace />', '<%=ajaxURL%>');
				}
				//there was a change on vat
				if( typeof obj.vat_code != 'undefined'){
					updateVatWithTvaCashExp(obj.vat_code, existingData.cash_exp_line_id.value);
				}
				// there was a change on quantity
				if (typeof obj.quantity != 'undefined' || typeof obj.unit_price_curr != 'undefined') {
					// if both are not empty make changes to all related fields otherwise do nothing
					if (typeof existingData.quantity.value != '' && typeof existingData.unit_price_curr.value != '') {
						setLineValuesCashExp(existingData.quantity.value, existingData.unit_price_curr.value, '<portlet:namespace/>');
					}
				}
			});
			
			// listener event to add line for top table
			$('.add_article_line').click(function (e) {
				var elementT 	= e.target;
				var elementId 	= elementT.id;
				if($('#<portlet:namespace/>cota_tva').val() == "" || $().val('#<portlet:namespace/>cota_tva') == null ){
					alert("Va rugam completati cota TVA.")
					return;
				} else {
					createNewLine();
				}
			});
			
			/* add new line in main table */
	   		function createNewLine() {
				remoteData = dataTable.data.toArray();
				counter_cash_exp_lines++;
				remoteData.push({
					id_cash_exp :'<%=id%>',
					cash_exp_line_id: counter_cash_exp_lines,
					delete_line: counter_cash_exp_lines,
					line: counter_cash_exp_lines,
					counter : counter_cash_exp_lines,
					product_code: '0',
					description: '',
					um: 'Buc.',
					quantity: '1',
					unit_price_curr : '',
					price_no_vat_curr : '',
					price_vat_curr : '',
					vat_code:  $('#<portlet:namespace/>cota_tva').val() == "" ? "24.00" : $('#<portlet:namespace/>cota_tva').val(),
					price_with_vat_curr : '',
					id_ias: '0',
					id_ifrs: '0',
					dt_ias: '',
					dt_ifrs: '',
					lot: '',
					detalii: '',
					clasif: 0,
					detalii: counter_cash_exp_lines,
					cont_imob_in_fct : 0,
					cont_imob_in_curs : 0,
					cont_imob_avans : 0,
					is_new_line : 1
				});
				dataTable.set('recordset', remoteData);
	   		}
			
	   		/* set all product related values on a line */
	   		function setProductValues(prodId, isNew) {
	   			//console.log("Set prod values");
	        	$.ajax({
					type: "POST",
					url: '<%=ajaxURL%>',
					data: 	"<portlet:namespace />action=getProdInfo" +
							"&<portlet:namespace />prodId=" + prodId,
					success: function(msgjson) {
						// get table data
						if (msgjson != ""){
				   			var rows = [];
				   			var rowsSplit = [];
						    var ml = dataTable.data, msg = '', template = '';
						    var mlS = dataTableSplit.data, msg = '', template = '';
						    ml.each(function (item, i) {
						        var data = item.getAttrs(['id_cash_exp', 'cash_exp_line_id', 'delete_line', 'line', 'counter', 'product_code', 'description', 'um', 'quantity', 'unit_price_curr', 
						     			                 'price_no_vat_curr', 'price_vat_curr', 'vat_code', 'price_with_vat_curr', 'id_ias', 'id_ifrs', 'dt_ias', 'dt_ifrs', 'lot', 'clasif', 
						    			                 'detalii', 'cont_imob_in_fct', 'cont_imob_in_curs', 'cont_imob_avans', 'is_new_line']);
							        var jsonEntries = jQuery.parseJSON(msgjson);
									var product = jsonEntries.product;
						        if (data.product_code == prodId) {
						        	data.dt_ifrs = product[0].ifrs_dt;
						        	data.dt_ias = product[0].ias_dt;
						        	data.id_ias = product[0].ias_id;
						        	data.id_ifrs = product[0].ifrs_id;
						        	data.clasif = product[0].flag_it;
						        	data.lot = product[0].lot_id;
						        	data.cont_imob_in_fct = product[0].cont_imob_in_fct;
						        	data.cont_imob_in_curs = product[0].cont_imob_in_curs;
						        	data.cont_imob_avans = product[0].cont_imob_avans;
						        }
						        if(isNew) {
									mlS.each(function (item, i) {
										//for each store line
										var dataS = item.getAttrs(['id_cash_exp', 'id_cash_exp_line','id_store_line','delete_store_line', 'line', 'description','id_store_1','id_store_2','id_store_3','id_store_4','id_store_5',
													                 'id_store_6','id_store_7','id_store_8', 'id_store_9','quantity','price_no_vat_curr','id_aisle','associated_acc','id_tip_inreg','id_act_mf','gestiune','id_dpi','vizualizare_dpi',
													                 'id_tip_inventar','detalii','cont_imob_in_fct','cont_imob_in_curs','cont_imob_avans','inventory']);
										if(dataS.line == data.cash_exp_line_id) {
											dataS.cont_imob_in_fct = product[0].cont_imob_in_fct;
											dataS.cont_imob_in_curs = product[0].cont_imob_in_curs;
											dataS.cont_imob_avans = product[0].cont_imob_avans;
										}
										rowsSplit.push(dataS);
										dataTableSplit.set('recordset', rowsSplit);
									});
						        }
					        	rows.push(data);
							    dataTable.set('recordset', rows);
						    });
						    setAssociatedAccountCashExp('<portlet:namespace/>');
						} else {
							alert('Produsul ales nu exista in baza de date!');
						}
					},
					error: function(msg) {
						alert('A aparut o problema in preluarea datelor de pe server. Va rugam incercati din nou!');
					}
				});
	   		}
	   		
	   		//click on aloca toate articolele
			$('.add_all').click(function (e) {
				e.preventDefault();
				allocateThemAllCashExp('<portlet:namespace/>');
			});
			
			// click on alocare linie
			dataTable.delegate('click', function (e) {
				//console.log('aloc lines');
	   		    var target = e.target.get('id');
	   		    createNewStoreLineCashExp(target.replace('row',''), '<portlet:namespace/>');
	   		    //calculateCashExpValue('<portlet:namespace/>', target.replace('row',''));
	   		}, '.allocline', dataTable);
			
			
			dataTableSplit.after('record:change', function (e) {
				var obj = e.target.changed;
				var existingData = e.target._state.data;
				//change associated account based on reg type and mf act
				if (typeof obj.id_act_mf != 'undefined' || typeof obj.id_tip_inreg != 'undefined') {
					setAssociatedAccountCashExp('<portlet:namespace/>');
				}
				if (typeof obj.id_dpi != 'undefined') {
					setDPILinkCashExp(obj.id_dpi, '<portlet:namespace/>');
				}
				if(typeof obj.id_store_1 != 'undefined') {
					storeId = obj.id_store_1;
				}
				if(typeof obj.id_store_2 != 'undefined') {
					storeId = obj.id_store_2;
				}
				if(typeof obj.id_store_3 != 'undefined') {
					storeId = obj.id_store_3;
				}
				if(typeof obj.id_store_4 != 'undefined') {
					storeId = obj.id_store_4;
				}
				if(typeof obj.id_store_5 != 'undefined') {
					storeId = obj.id_store_5;
				}
				if(typeof obj.id_store_6 != 'undefined') {
					storeId = obj.id_store_6;
				}
				if(typeof obj.id_store_7 != 'undefined') {
					storeId = obj.id_store_7;
				}
				if(typeof obj.id_store_8 != 'undefined') {
					storeId = obj.id_store_8;
				}
				if(typeof obj.id_store_9 != 'undefined') {
					storeId = obj.id_store_9;
				}
				
			});
			
			var invTypes = [];
			var invIndex = 0;
			// click for inventory number link
			dataTableSplit.delegate("click", function(e){
		        e.preventDefault();
				var line = 0;
		        var quantity = 0;
		        var iasId = 0;
		        var loadedInventory = "";
		        var majorare = "";
		        var store = "";
		        var hasStore = true;
		        var selectedCompany = $('#<portlet:namespace/>company').val();
		        idLine = e.target.getAttribute("id").replace('line-', '');
		        // loop through table
		        var ml = dataTableSplit.data, msg = '', template = '';
			    ml.each(function (item, i) {
			    	var dataS = item.getAttrs(['id_cash_exp', 'id_cash_exp_line','id_store_line','delete_store_line', 'line', 'description','id_store_1','id_store_2','id_store_3','id_store_4','id_store_5',
			    				                 'id_store_6','id_store_7','id_store_8', 'id_store_9','quantity','price_no_vat_curr','id_aisle','associated_acc','id_tip_inreg','id_act_mf','gestiune','id_dpi','vizualizare_dpi',
			    				                 'id_tip_inventar','detalii','cont_imob_in_fct','cont_imob_in_curs','cont_imob_avans','inventory']);
			    	if (dataS.line == idLine) {
			    		var dataS_store = 0;
			    		if (selectedCompany == 1) {
			    			dataS_store = dataS.id_store_1;
			    		} else if (selectedCompany == 2) {
			    			dataS_store = dataS.id_store_2;
			    		} else if (selectedCompany == 3) {
			    			dataS_store = dataS.id_store_3;
			    		} else if (selectedCompany == 4) {
			    			dataS_store = dataS.id_store_4;
			    		} else if (selectedCompany == 5) {
			    			dataS_store = dataS.id_store_5;
			    		} else if (selectedCompany == 6) {
			    			dataS_store = dataS.id_store_6;
			    		}else if (selectedCompany == 7) {
			    			dataS_store = dataS.id_store_7;
			    		}else if (selectedCompany == 8) {
			    			dataS_store = dataS.id_store_8;
			    		}else if (selectedCompany == 9) {
			    			dataS_store = dataS.id_store_9;
			    		}
			    		if (dataS_store != 0) {
				    	tipInv = dataS.id_tip_inventar;
				    	denArt = dataS.description;
				    	codArt = dataS.line; // just an attribute to know the selected element
				    	quantity = dataS.quantity;
				    	if(quantity < 0) { 
			        		quantity = quantity*(-1);
			        	}
				    	line = dataS.line;
				    	loadedInventory = dataS.inventory;
				    	majorare = dataS.id_act_mf;
			    		} else {
			    			alert("Atentie! Pe aceasta linie magazinul nu a fost alocat!");
			    			hasStore = false;
			    		}
			    	}
			    });
		        
			    if(!hasStore) {
			    	return;
			    }
			    
			    console.log(idLine + " -> " + tipInv + " -> " + denArt + " -> " + codArt + " -> " + loadedInventory);
			    
		        // prepare table, insert into remoteDataInv, add to dataTableInv
		        var wasChanged = false;
		        if (invTypes[invIndex-1] != tipInv) {
		        	invTypes[invIndex++] = tipInv;
		        	wasChanged = true;
		        }
		        // IF the inventory was already loaded on this line do not get a new sequence from the DB
		        if (loadedInventory != "" && !wasChanged) {
					// incarcare nr de inventar
			        var jsonEntries = jQuery.parseJSON(loadedInventory);
					// show pop-up and load record in pop-up
					dataTableInv.set('data', eval(jsonEntries));
					showInventoryOverlay(e, Y);
					$('.stores_' + $('#<portlet:namespace />company').val()).removeClass('hiddencol');
		        } else {
		        	console.log("majorarea este : " + majorare);
		        	if(majorare != 2 ){
		        	// GET inventory SEQUENCE form DB, get IAS group for AJAX call
				    var ml1 = dataTable.data, msg = '', template = '';
				    
				    ml1.each(function (item, i) {
				        var data = item.getAttrs(['cash_exp_line_id', 'id_ias']);
				        if (data.cash_exp_line_id == line) {
				        	iasId = data.id_ias;
				        }
				    });
				    
				    if (loadedInventory != "" && tipInv == 2 && wasChanged){
				    	var invComponents = loadedInventory.split(',');
				    	loadedInventory = invComponents[0] + ',' + invComponents[1] + ',' + invComponents[2] + ']';
				    	// incarcare nr de inventar
				        var jsonEntries = jQuery.parseJSON(loadedInventory);
						// show pop-up and load record in pop-up
						dataTableInv.set('data', eval(jsonEntries));
						
						showInventoryOverlay(e, Y);
						//updateInventoryOverlay('<portlet:namespace />');
						$('.stores_' + $('#<portlet:namespace />company').val()).removeClass('hiddencol');
				    } else {
				        // perform AJAX call in order to get inventory number(s)
						$.ajax({
						type: "POST",
						url: '<%=ajaxURL%>',
						data: 	"<portlet:namespace />action=getInventoryNo" +
								"&<portlet:namespace />storeId=" + storeId +
								"&<portlet:namespace />iasId=" + iasId +
								"&<portlet:namespace />count=" + quantity +
								"&<portlet:namespace />tipInv=" + tipInv,
						success: function(msgjson) {
							// get table data
							if (msgjson != "") {
								// incarcare nr de inventar
						        var jsonEntries = jQuery.parseJSON(msgjson);
								// show pop-up and load record in pop-up
								dataTableInv.set('data', eval(jsonEntries));
								showInventoryOverlay(e, Y);
								//updateInventoryOverlay('<portlet:namespace />');
								$('.stores_' + $('#<portlet:namespace />company').val()).removeClass('hiddencol');
							} else {
								//alert('');
							}
						},
						error: function(msg) {
							alert('A aparut o problema in preluarea datelor de pe server. Va rugam incercati din nou!');
						}
						});
			        }
					 //daca e majorare
			        } else {
			        	emptyPopUpInventoryVouchers(dataTableInv);
		    			setEntriesDataTableInvVouchers(quantity, tipInv, dataTableInv);
		    			showInventoryOverlay(e, Y);
		    			//updateInventoryOverlay('<portlet:namespace />');
		    			$('.stores_' + $('#<portlet:namespace />company').val()).removeClass('hiddencol');
			        }
		        }
		    }, ".proddet", dataTableSplit);
			
			/********  Delete lines functions  *********/
			//DELETE LINE FROM DATABASE AND INTERFACE
			dataTable.delegate('click', function (e) {
			e.preventDefault();
			var elem = e.currentTarget.get('id');
  			var elementId	= elem.replace('row', '');
  			var indexToRemove = -1;
  			var lineNumber = -1;
  			var ml = dataTable.data, msg = '', template = '';
 		    ml.each(function (item, i) {
 		    	var data = item.getAttrs(['cash_exp_line_id']);
 		    	//find the index of the line that is to be deleted
 		    	if (elementId == data.cash_exp_line_id){
 		    		//save the index of the line
 		    		indexToRemove = i;
 		    		//save the invoice line number
 		    		lineNumber = data.cash_exp_line_id;
 		    		return;
 		    	}
 		    });
 		    //remove the line
 		    if (confirm ("Atentie! La stergerea unei linii de factura se vor sterge si toate alocarile pe magazine asociate acesteia." +
 		    		" Sunteti sigur/a ca doriti sa stergeti aceasta linie?")){
				makeAjaxCallToDeleteLine(elementId, '<%=ajaxURL%>', '<portlet:namespace/>');
 		    	dataTable.removeRow(indexToRemove);
 		    	//delete all associated store lines
 		    	if(lineNumber != -1 ) {
 	 		    	deleteAssociatedStoreLinesCashExp(lineNumber);
 		   	 	}
 		    	setLineAndStoreLineValues('<portlet:namespace/>');
 		    }
		}, '.deleteLineCashExp', dataTable);
			
			//DELETE STORE LINE FROM DATABASE AND INTERFACE
			dataTableSplit.delegate('click', function (e) {
				e.preventDefault();
				//get target
				var elem = e.currentTarget.get('id');
				//get the id of the target
	  			var elementId	= elem.replace('row', '');
				//index of the row that is deleted
	  			var indexToRemove = -1;
				//iterate the data to find the index of the row that is deleted
	  			var ml = dataTableSplit.data, msg = '', template = '';
	 		    ml.each(function (item, i) {
	 		    	//console.log(item);
	 		    	var dataS = item.getAttrs(['delete_store_line']);
	 		    	if (dataS.delete_store_line == elementId){
	 		    		indexToRemove = i;
	 		    		return;
	 		    	}
	 		    });
	 		    //remove row from locat array
	 		    if (confirm("Sunteti sigur/a ca doriti sa stergeti aceasta linie?")){
	  				dataTableSplit.removeRow(indexToRemove);
	  				makeAjaxCallToDeleteStoreLine(elementId, '<%=ajaxURL%>', '<portlet:namespace/>');
	  				setLineAndStoreLineValues('<portlet:namespace/>');
	 		    }
			}, '.deleteStoreLineCashExp', dataTableSplit);
			
		//delete all the store lines associated with the invoice line that has the number = @param lineNumber
		function deleteAssociatedStoreLinesCashExp(lineNumber){
  			var mls = dataTableSplit.data, msg = '', template = '';
 		 	var storeLines_id = -1;
 		   	mls.each(function (item, i) {
 		   		var dataS = item.getAttrs(['line']);
 		   		if (dataS.line == lineNumber) {
 		   			storeLines_id = i;
 					return;
 		   		}
 		   	});
 		   	//remove the store line
 			dataTableSplit.removeRow(storeLines_id); 
		}	
	   		
		// click on dpi link
   		dataTableSplit.delegate('click', function (e) {
   			e.preventDefault();
   		    var target = e.target.get('id');
   		    var ids = target.replace('dpi_','').split("_");
   		    var lineid = ids[0];
   		    var dpiId = ids[1];
   		 	showDPIPopupCashExp(e.target, dpiId,'<portlet:namespace/>', '<%=ajaxURL%>', Y);
		}, '.dpilink', dataTableSplit);
		
	   		$(document).ready(function(){
	   			
	   			var tipCapex = <%=oneCashExpenseHeader.get(0).get("tip_capex")%>
	   			if(tipCapex == 0){
	   				$('#<portlet:namespace/>capex_diverse').attr('checked', true);
	   			} else if (tipCapex == 1){
	   				$('#<portlet:namespace/>init_cent').attr('checked', true);
	   				var idInitiativa = <%=oneCashExpenseHeader.get(0).get("id_initiative")%>
	   				$('#<portlet:namespace/>lista_initiative').val(idInitiativa);
	   				$('#<portlet:namespace/>lista_initiative').trigger("chosen:updated");
	   			} else {
	   				$('#<portlet:namespace/>proiect_nou').attr('checked', true);
	   				var idNewPrj = <%=oneCashExpenseHeader.get(0).get("id_new_prj")%>
	   				$('#<portlet:namespace/>project').val(idNewPrj);
	   				$('#<portlet:namespace/>project').trigger("chosen:updated");
	   			}
	   			
		   		$('#<portlet:namespace/>difference').autoNumeric('init', {aSep: ',', mDec: 4, vMin : -999999999, vMax : 999999999 });
		  	    $('#<portlet:namespace/>val_ron').autoNumeric('init', {aSep: ',', mDec: 4, vMin : -999999999, vMax : 999999999 });
		  	    $('#<portlet:namespace/>valoare_totala').autoNumeric('init', {aSep: ',', mDec: 4, vMin : -999999999, vMax : 999999999 });
		  	  	$('#<portlet:namespace/>valoare_totala_alocata').autoNumeric('init', {aSep: ',', mDec: 4, vMin : -999999999, vMax : 999999999 });
		  	 	$('#<portlet:namespace/>value_curre').autoNumeric('init', {aSep: ',', mDec: 4, vMin : -999999999, vMax : 999999999 });
		  		$('#<portlet:namespace/>value_vat').autoNumeric('init', {aSep: ',', mDec: 4, vMin : -999999999, vMax : 999999999 });
		  		$('#<portlet:namespace/>value_with_vat').autoNumeric('init', {aSep: ',', mDec: 4, vMin : -999999999, vMax : 999999999 });
		  		$('#<portlet:namespace/>ron_vat').autoNumeric('init', {aSep: ',', mDec: 4, vMin : -999999999, vMax : 999999999 });
		  		$('#<portlet:namespace/>ron_with_vat').autoNumeric('init', {aSep: ',', mDec: 4, vMin : -999999999, vMax : 999999999 });
		  		<% if (cashExpenseLines.size() > 0 ){ %>
			  		//display store column
			  		$('.stores_' + $('#<portlet:namespace/>company').val()).removeClass('hiddencol');
			  		setLineAndStoreLineValues('<portlet:namespace/>');
		  		<% } %>
		  		//set product codes for existing lines
		  		<% if (cashExpenseLines.size() > 0 ){ 
		  			for(int i = 0 ; i < cashExpenseLines.size(); i++ ){ %>
	  					setProductValues(<%=cashExpenseLines.get(i).get("product_code")%>, false);
		  		<% }  } %>
	   		});
});

</script>