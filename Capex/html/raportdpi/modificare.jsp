<%@page import="com.profluo.ecm.model.db.DPIHeader"%>
<%@page import="com.profluo.ecm.model.db.InvoiceHeader"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.List"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme" %>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>
<%@ taglib uri="http://alloy.liferay.com/tld/aui" prefix="aui" %>
<%@ taglib uri="http://liferay.com/tld/util" prefix="liferay-util" %>

<liferay-theme:defineObjects />
<portlet:defineObjects />

<%	
	String id = renderRequest.getParameter("id");
	List<HashMap<String,Object>> allDpiInvoices 	= DPIHeader.getAllDpiInvoices(id);
%>

<portlet:actionURL name="saveDpi" var="submitURL" />

<portlet:renderURL var="backURL">
	<portlet:param name="mvcPath" value="/html/raportdpi/view.jsp"></portlet:param>
</portlet:renderURL>

<portlet:resourceURL id="ajaxURL" var="ajaxURL"/>


<aui:form action="<%=submitURL %>" id="submitDpi" name="submitDpi" method="post">
	<aui:fieldset>
		<legend>Editare DPI</legend>
		<aui:layout>
			<aui:column columnWidth="15" first="true">
				<label style="padding-top:5px" for="<portlet:namespace/>id_dpi">Numar DPI </label>
			</aui:column>
			<aui:column columnWidth="15" style="text-align:right">
				<aui:input label="" name="id_dpi" id="id_dpi" type="text" value="" style="width:90%" readonly="readonly" />
				<aui:input label="" name="id" id="id" type="text" style="display:none" />
			</aui:column>
		</aui:layout>
		<aui:layout>
			<aui:column columnWidth="15">
				<label style="padding-top:5px" for="<portlet:namespace/>valoare">Valoare DPI </label>
			</aui:column>
			<aui:column columnWidth="15" style="text-align:right">
				<aui:input label="" name="valoare" id="valoare" type="text" value="" style="width:90%" readonly="readonly" />
			</aui:column>
		</aui:layout>
		<aui:layout>
			<aui:column columnWidth="15">
				<label style="padding-top:5px" for="<portlet:namespace/>status">Status </label>
			</aui:column>
			<aui:column columnWidth="15">
				<aui:select label="" name="status" id="status">
					<aui:option value="1">Activ</aui:option>
					<aui:option value="2">Inactiv</aui:option>
				</aui:select>
			</aui:column>
		</aui:layout>
		<aui:layout>
			<aui:column columnWidth="15">
				<label style="padding-top:5px" for="<portlet:namespace/>diferenta">Diferenta</label>
			</aui:column>
			<aui:column columnWidth="15" style="text-align:right">
				<aui:input label="" name="diferenta" id="diferenta" type="text" value="" style="width:90%" readonly="readonly" />
			</aui:column>
		</aui:layout>
		<legend>Adaugare factura</legend>
		<aui:layout>
			<aui:row>
				<aui:column first="true" columnWidth="75">
					<div id="facturi"></div>
				</aui:column>
			</aui:row>
		</aui:layout>
		<aui:layout>
			<aui:column>
<%-- 				<aui:button class="addInvoice" value="Adauga factura"></aui:button> --%>
					<a href="#" class="addInvoice btn"
						onclick="javascript:;return false;">Adauga factura </a>
			</aui:column>
		</aui:layout>
		<aui:layout>
			<aui:column last="true">
				<aui:input label="" name="liniiFacturi" id="liniiFacturi" type="text" style="display:none"/>
				<a class="btn btn-primary-red" href="<%= backURL %>">Inapoi</a>
				<aui:button value="Salveaza" id="salveazaDpi" cssClass="btn btn-primary" style="margin:0px"/>
			</aui:column>
		</aui:layout>
		
		<aui:layout>
			<aui:input name="iduriFacturi" label="" id="iduriFacturi" style="display:none"></aui:input>
		</aui:layout>
	</aui:fieldset>
</aui:form>

<aui:layout>
	<aui:fieldset><jsp:include page="/html/includes/popups/alocare_facturi_dpi.jsp" /></aui:fieldset>
</aui:layout>

<script type="text/javascript">
var dataTable;
var counter = <%=allDpiInvoices.size()%>;

YUI().use('aui-datatable', 'aui-datatype', 'datatable-sort', 'datatable-mutable',
	function(Y) {
	/** GENERIC SAVE ON ENTER **/
	function saveOnEnter(e) {
		if (e.domEvent.charCode == 13) {
			if (!e.target.validator.hasErrors()) {
				e.target.fire('save', {
	                newVal: e.target.getValue(),
	                prevVal: e.target.get('value')
	            });
			}
		}
	}
	function formatCurrency(cell) {
	    format = {
	    	thousandsSeparator: ",",
	        decimalSeparator: ".",
	        decimalPlaces: noOfDecimalsToDisplay
	    };
	    return Y.DataType.Number.format(Number(cell.value), format);
	}
	
	var allInvoices = <%=InvoiceHeader.getJsonStringInvoiceHeader()%>
	var remoteData = [
	                  <% for(int i = 0 ; i < allDpiInvoices.size(); i++ ) { %>
	                	  {   'id'				: <%=allDpiInvoices.get(i).get("id")%>,
	                		  'idFactura' 		: <%=allDpiInvoices.get(i).get("idFactura")%> != '' ? <%=allDpiInvoices.get(i).get("idFactura")%> : 0,
	                		  'nrFactura' 		: '<%=allDpiInvoices.get(i).get("nrFactura")%>',
	                		  'valoareFactura' 	: '<%=allDpiInvoices.get(i).get("valoareFactura")%>',
	                		  'totalFactura' 	: '<%=allDpiInvoices.get(i).get("totalFactura")%>',
	                		  'counter'			: <%=i%>
                		  <% if(i < allDpiInvoices.size() - 1) { %>
                		  		},
                		  <%} else { %>
                		  		}
                		  <% } %>
	                 <% } %>
	                  ];
	var nestedCols = [
						 {key: 'idFactura', label:'Factura din sistem', 
// 							editor: new Y.DropDownCellEditor({options: allInvoices, id: 'allInvoices', elementName: 'allInvoices', disabled: "true",
// 			    			after: { focus: function(event) {  }  } }),
			    			formatter: function(o) {
			    				var obj = JSON.parse(JSON.stringify(allInvoices));
			    				if (typeof o.value != 'undefined') {
			    					if(o.value != null) {
			    						return obj[o.value].toString();
			    					} else {
			    						return '';
			    					}
			    				} else {
			    					return '';
			    				}
			    			}
			    		},
						 {key: 'nrFactura', label:'Factura noua', 
			    				editor: new Y.TextCellEditor({on : {keyup: saveOnEnter}, showToolbar: false})},
						 {key: 'valoareFactura', label:'Valoare factura noua', 
							 editor: new Y.TextCellEditor({on : {keyup: saveOnEnter}, showToolbar: false}),
							 formatter: formatCurrency
						 },
						 { key: 'counter', label: 'Actiuni', allowHTML: true, 
								formatter: function(o) {
									if(o.value >= 0) {
										return '<a href="#" id="' + o.value + '" class="deleteRow">Stergere</a>';
									} else {
										return ' ';
									}
								},
								emptyCellValue: '<a href="#" id="{counter}" class="deleteRow">Stergere</a>'},
						{ key: 'totalFactura',  className: 'hiddencol'}
					]
	dataTable = new Y.DataTable({
	    columns: nestedCols,
	    data: remoteData,	
	    editEvent: 'click',
	    recordType: ['counter', 'idFactura', 'nrFactura', 'valoareFactura', 'totalFactura']
	}).render('#facturi');
	
	$('.addInvoice').click( function (e) {
		  // prepare overlay
// 	    var currentNode = this;
		var overlay = new Y.Overlay({
		    srcNode:"#overlay-alocare-facturi",
		    width:"800px",
		    align: {
		        node: e.target,
		        points:["bl", "tl"]
		    }
		});
		
		overlay.render();

		 $('#overlay-alocare-facturi').show();
		
// 		var remoteData = dataTable.data.toArray();
// 		remoteData.push({
// 			counter			: counter++,
// 			idFactura 		: 0,
// 			nrFactura 		: '',
// 			valoareFactura	: 0
// 		});
// 		dataTable.set('recordset', remoteData);
	});
	
	$('#<portlet:namespace/>add_invoices').click(function (e) {
		var remoteData = dataTable.data.toArray();
		remoteData.push({
			counter			: counter++,
			idFactura 		: $('#<portlet:namespace/>sistemFact').val(),
			nrFactura 		: $('#<portlet:namespace/>factNoua').val(),
			valoareFactura	: $('#<portlet:namespace/>valFactNoua').val()
		});
		dataTable.set('recordset', remoteData);
		getInvoiceTotal($('#<portlet:namespace/>sistemFact').val());
		$('#overlay-alocare-facturi').hide();
		$('#<portlet:namespace/>sistemFact').val(0);
// 		$('#<portlet:namespace/>sistemFact_chosen').val(0);
		$('#<portlet:namespace/>sistemFact').trigger("chosen:updated");
		$('#<portlet:namespace/>factNoua').val('');
		$('#<portlet:namespace/>valFactNoua').val(0);
	});
	
	$('#salveazaDpi').click( function (e) {
		var ml = dataTable.data;
		var json = ml.toJSON();
		var stringData = JSON.stringify(json);
		$('#<portlet:namespace/>liniiFacturi').val(stringData);
		$('#<portlet:namespace/>submitDpi').append('<input type="submit" name="submit-button" id="submit-button"></input>');
		$('#submit-button').click();
		setTimeout(function() { $('#submit-button').remove(); }, 200);
	});
	
	 dataTable.delegate('click', function (e) {
		 e.preventDefault();
		 var elem = e.currentTarget.get('id');
		 var indexToRemove = -1;
		 var lineId;
		 var ml = dataTable.data, msg = '', template = '';
		    ml.each(function (item, i) {
		    	var data = item.getAttrs(['counter', 'id']);
		    	if(data.counter == elem) {
		    		indexToRemove = i;
		    		lineId = data.id;
		    		return;
		    	}
		    });
		  if(indexToRemove != -1) {
			  if(lineId && lineId != 0) {
				  if(confirm("Confirmati stergerea liniei?")) {
					  $.ajax({
							type: "POST",
							url: '<%=ajaxURL%>',
							data: 	"<portlet:namespace />action=deleteLine" +
									"&<portlet:namespace />lineId=" + lineId,
							success: function(msgjson) {
								dataTable.removeRow(indexToRemove);
								recalculateDifference();
							},
							error: function(msg) {
								alert('A aparut o problema in stergerea liniei de pe server. Va rugam incercati din nou!');
							}
						});
				  }
			  } else {
				  	dataTable.removeRow(indexToRemove);
					recalculateDifference();
			  }
			  
		  }
	    }, '.deleteRow', dataTable);
	 
	 dataTable.after('record:change', function (e) {
		 var obj = e.target.changed;
		 //daca s-a adaugat factura din sistem
		 if (typeof obj.idFactura != 'undefined') {
			 getInvoiceTotal(obj.idFactura);
		 }
		 //daca s-a adaugat valoare noua
		 if(typeof obj.valoareFactura != 'undefined') {
			 recalculateDifference();
		 }
	 });
	
	 function getInvoiceTotal(idFactura) {
		 $.ajax({
				type: "POST",
				url: '<%=ajaxURL%>',
				data: 	"<portlet:namespace/>action=getInvoiceTotal&<portlet:namespace/>invoiceId=" + idFactura,
				success: function(msg) {
					if (msg != ""){
						var rows = [];
						var json = jQuery.parseJSON(msg);
						var obj = JSON.parse(JSON.stringify(json));
						var ml = dataTable.data, msg = '', template = '';
						ml.each(function (item, i) {
							var data = item.getAttrs(['counter', 'idFactura', 'nrFactura', 'valoareFactura', 'totalFactura']);
							if (data.idFactura == idFactura) {
								data.totalFactura = obj.valoareFactura;
							}
							rows.push(data);
						});
						dataTable.set('recordset', rows);
						recalculateDifference();
					} else {
						alert('A aparut o eroarea la prealuarea valorii facturii');
					}
				}
				
		 });
	 }
	 
	 function recalculateDifference() {
		 debugger;
		 var total = 0;
		 var ml = dataTable.data, msg = '', template = '';
		 ml.each(function (item, i) {
			var data = item.getAttrs(['counter', 'idFactura', 'nrFactura', 'valoareFactura', 'totalFactura']);
			if(data.valoareFactura && data.valoareFactura != "null" && data.valoareFactura != "undefined") {
				total = parseFloat(parseFloat(total) + parseFloat(data.valoareFactura)).toFixed(4);
			} 
			if(data.totalFactura && data.totalFactura != "null" && data.totalFactura != "undefined")  {
				total = parseFloat(parseFloat(total) + parseFloat(data.totalFactura)).toFixed(4);
			}
		 });
		 if(total) {
			 $('#<portlet:namespace/>diferenta').val(parseFloat($('#<portlet:namespace/>valoare').val() -  parseFloat(total)).toFixed(4));
		 } else {
			 $('#<portlet:namespace/>diferenta').val($('#<portlet:namespace/>valoare').val()); 
		 }
	 }
	 
	 $(document).ready(function() {
		 recalculateDifference();
		 $('#<portlet:namespace/>sistemFact').chosen();
	 });
	 
});
</script>