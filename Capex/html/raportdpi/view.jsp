<%@page import="com.profluo.ecm.model.db.InvoiceHeader"%>
<%@page import="com.profluo.ecm.controller.ControllerUtils"%>
<%@page import="com.profluo.ecm.model.db.DPIHeader"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme"%>

<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui"%>
<%@ taglib uri="http://alloy.liferay.com/tld/aui" prefix="aui"%>
<%@ taglib uri="http://liferay.com/tld/util" prefix="liferay-util"%>

<%@page import="java.util.HashMap"%>
<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>


<liferay-theme:defineObjects />
<portlet:defineObjects />

<portlet:resourceURL id="ajaxURL" var="ajaxURL"/>

<portlet:resourceURL id="exportAction" var="exportURL">
	<portlet:param name="action" value="exportExcel"/>
</portlet:resourceURL>


<portlet:renderURL var="editEntryURL" >
	<portlet:param name="mvcPath" value="/html/raportdpi/modificare.jsp"></portlet:param>
</portlet:renderURL>

<%
	int count 		= 15;
	int start 		= 0;
	int total 		= 0;
	String idDpi 	= "";
	List<HashMap<String,Object>> allDpis = DPIHeader.getAllActiveDpis(start, count, idDpi, "");
	total = DPIHeader.getAllActiveDpisCount(idDpi);
%>

<aui:layout>
	<aui:column>
		<a class="btn btn-primary" href="<%=exportURL.toString()%>"> Export Excel </a>
	</aui:column>
</aui:layout>

<aui:layout>
	<aui:row>
		<aui:column first="true" columnWidth="100">
			<div id="raport_dpi"></div>
		</aui:column>
	</aui:row>
</aui:layout>

<%-- HTML for PAGINATION --%>
<%@ include file="/html/includes/pagination.jsp" %>

<script type="text/javascript">
var colName = '';
var start 		= "<%=start%>";
var count 		= "<%=count%>";
var total 		= "<%=total%>";
var idDpi 		= "<%=idDpi%>";
var dataTable;

YUI().use('aui-datepicker', 'aui-datatable', 'aui-datatype', 'datatable-sort',
	function(Y) {
	function formatCurrency(cell) {
	    format = {
	    	thousandsSeparator: ",",
	        decimalSeparator: ".",
	        decimalPlaces: 2
	    };
	    return Y.DataType.Number.format(Number(cell.value), format);
	}
	
	var statusuri = {1:'Activ', 2:'Inactiv'};
<%-- 	var allInvoices = <%=InvoiceHeader.getJsonStringInvoiceHeader()%> --%>
		var remoteData = <%=ControllerUtils.getJSONDPI(allDpis)%>
		var nestedCols = [
							 {key: 'societate', label:'Societate'},
							 {key: 'magazin', label:'Magazin', className : 'sort_magazin'},
							 {key: 'id_dpi', label: '<input class="field" id="id_dpi" name="id_dpi" placeholder="Id DPI" style="width:75px !important;margin:0"/>'}, 
							 {key: 'data_dpi', label:'Data DPI', className : 'data_dpi_sort'},
							 {key: 'valoare_dpi', label: 'Valoare', formatter: formatCurrency,  className: 'currencyCol sort_valoare' },
							 {key: 'suma_facturi', label:'Valoare fara TVA facturi', formatter: formatCurrency, className: 'currencyCol sort_facturi'},
							 {key: 'diferenta', label:'Diferenta', formatter: formatCurrency, className: 'currencyCol'},
							 {key: 'facturi', label:'Facturi asociate'},
							 {key: 'facturi_adaugate', label:'Facturi adiacente'},
							 {key: 'furnizori', label:'Furnizori'},
							 {key: 'initiator', label:'Initiator'},
// 							 {key: 'add_invoice', label:'Adauga factura',
// 								 editor: new Y.DropDownCellEditor({options: allInvoices, id: 'allInvoices', elementName: 'allInvoices', 
// 					    				after: { focus: function(event) {  $("select[name=allInvoices]").chosen();  }  } })
// 							},
// 							 {key: 'status', label: 'Status', allowHTML: true, 
// 								 editor: new Y.DropDownCellEditor({options: statusuri, id: 'statusuri', elementName: 'statusuri', 
// 					    				after: { focus: function(event) {  $("select[name=statusuri]").chosen();  }  } }),
// 								 formatter: function(o) { 
// 									if(o.value == 'Activ' || o.value == 1 || o.value == 'A') {return '<font color="green">Activ<font>';} 
// 									else { return '<font color="red">Inactiv</font>'; } } },
							 {key  :'status', label: 'Status'},
							 {key: 'actions', label: 'Actiuni',  allowHTML: true, 
									 formatter: function(o) {
											return '<a id="row-' + o.value + '" class="editeaza_dpi">Editeaza DPI</a>'; 
									}
							}
		                  ];
		
		dataTable = new Y.DataTable({
		    columns: nestedCols,
		    data: remoteData,	
		    editEvent: 'click',
		    recordType: ['societate', 'magazin', 'id_dpi', 'data_dpi', 'valoare_dpi', 'suma_facturi', 'diferenta', 'facturi', 
		                 'furnizori', 'add_invoice', 'facturi_adaugate','initiator', 'status', 'actions']
		}).render('#raport_dpi');
	
		// PAGINATION
  		// set status of navigation buttons on page load
  		setNavigationButtonsState('<portlet:namespace />');
  		$('.navigation').click(function(e) {
  			var elementT 	= e.target;
  			var elementId 	= elementT.id;
  			var action 		= "";
  			
  			// determine which button was pressed and set the value of start accordingly
  			if (elementId.indexOf("first") > -1)	{ action = "first"; start = 0; }
  			if (elementId.indexOf("prev") > -1) 	{ action = "prev"; if (start - count <= 0) {start = 0} else { start -= count; } }
  			if (elementId.indexOf("next") > -1) 	{ action = "next"; start = parseInt(start) + parseInt(count); }
  			if (elementId.indexOf("last") > -1) 	{ action = "last"; start = parseInt(parseInt(total) / parseInt(count)) * count; if (start == total) { start -= count; } }
  			
  			makeAjaxCall(action, "");
  		});
  		
  		$('#id_dpi').keyup(function (e) {
		    if ($("#id_dpi").is(":focus") && (e.keyCode == 13)) {
				var action 		= "filter";
				idDpi = e.target.value;
				makeAjaxCall(action);				
		    }
		});
  		
  		function makeAjaxCall(action) {
  			$.ajax({
  				type: "POST",
  				url: "<%=ajaxURL%>",
  				data: 	"<portlet:namespace />action=" + action + 
  						"&<portlet:namespace />start=" + start + 
  						"&<portlet:namespace />count=" + count +
  						"&<portlet:namespace />total=" + total + 
  						"&<portlet:namespace />idDpi=" + idDpi +
  						"&<portlet:namespace />col_name=" + colName,
  				success: function(msg) {
  					// get table data
  					if (msg != ""){
  						var jsonEntries = jQuery.parseJSON(msg);
  						total = jsonEntries.total;
  						dataTable.set('data', eval(jsonEntries.values));
  					} else {
  						dataTable.set('data', []);
  					}
  					// set status of navigation buttons on page load
  					setNavigationButtonsState('<portlet:namespace />');
  				}
  			});
  		}
  		/*
  		dataTable.delegate('click', function (e) {
  			var action = "filter";
  			colName = "data_dpi";
  			makeAjaxCall(action);
  		}, '.data_dpi_sort', dataTable);
  		
  		dataTable.delegate('click', function (e) {
  			var action = "filter";
  			colName = "ds.name";
  			makeAjaxCall(action);
  		}, '.sort_magazin', dataTable);
  		
  		dataTable.delegate('click', function (e) {
  			var action = "filter";
  			colName = "valoare_dpi";
  			makeAjaxCall(action);
  		}, '.sort_valoare', dataTable);
  		
  		dataTable.delegate('click', function (e) {
  			var action = "filter";
  			colName = "suma_facturi";
  			makeAjaxCall(action);
  		}, '.sort_facturi', dataTable);
  		*/
  		dataTable.delegate('click', function (e) {
  			e.preventDefault();
  			var targetDpi = e.target.get('id');
  			var dpiId = targetDpi.split("-")[1];
  			var ml  = dataTable.data, msg = '', template = '';
  			
  			 ml.each(function (item, i) {
  				var data = item.getAttrs(['societate', 'magazin', 'id_dpi', 'data_dpi', 'valoare_dpi', 'suma_facturi', 'diferenta', 'facturi', 
					  		                 'furnizori', 'add_invoice', 'facturi_adaugate','initiator', 'status', 'actions']);
  				 if (data.id_dpi == dpiId) {
  					$('#<portlet:namespace />id').val(data.id_dpi);
  					$('#<portlet:namespace />status').val(data.status);
  					$('#<portlet:namespace />id_dpi').val(data.id_dpi + ' - ' + data.data_dpi);
  					$('#<portlet:namespace />valoare').val(data.valoare_dpi);
  					$('#<portlet:namespace />editDpi').submit();
  				 }
  			 });
  			
  		}, '.editeaza_dpi', dataTable);
  		
  		dataTable.after('record:change', function (e) { 
  			var rows = [];
			var obj = e.target.changed;
			var existingData = e.target._state.data;
			if(typeof obj.add_invoice != 'undefined'){
				var inv_number = obj.add_invoice;
				var ml = dataTable.data, msg = '', template = '';
				 ml.each(function (item, i) {
					 var data = item.getAttrs(['societate', 'magazin', 'id_dpi', 'data_dpi', 'valoare_dpi', 'suma_facturi', 'diferenta', 'facturi', 
					  		                 'furnizori', 'add_invoice', 'facturi_adaugate','initiator' ,'status', 'actions']);
					 if(existingData.id_dpi.value == data.id_dpi) {
						 if( data.facturi_adaugate ) {
							 data.facturi_adaugate = data.facturi_adaugate + "," + data.add_invoice;
						 } else {
							 data.facturi_adaugate = data.add_invoice;
						 }
					 }
					rows.push(data);
		   		  });
		   		 dataTable.set('recordset', rows);
			}
  		});
});
</script>


<aui:form method="post" action="<%=editEntryURL.toString() %>" name="editDpi" id="editDpi" style="margin:0;padding:0">
	<aui:input type="hidden" id="id" name="id"/>
	<aui:input type="hidden" id="status" name="status"/>
	<aui:input type="hidden" id="id_dpi" name="id_dpi"/>
	<aui:input type="hidden" id="valoare" name="valoare"/>
</aui:form>