<%@page import="com.profluo.ecm.model.db.Raports"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme"%>

<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui"%>
<%@ taglib uri="http://alloy.liferay.com/tld/aui" prefix="aui"%>
<%@ taglib uri="http://liferay.com/tld/util" prefix="liferay-util"%>

<%@page import="java.util.HashMap"%>
<%@page import="java.util.List"%>


<liferay-theme:defineObjects />
<portlet:defineObjects />

<portlet:resourceURL id="ajaxURL" var="ajaxURL"/>

<portlet:resourceURL id="exportAction" var="exportURL">
	<portlet:param name="action" value="exportExcel"/>
</portlet:resourceURL>

<portlet:actionURL name="updateInfo" var="submitURL" />

<%
	int start = 0 ;
	int count = 14;
	int total = 0;
	List<HashMap<String,Object>> list = Raports.getRaport(start, count, 1);
	list.addAll(Raports.getTableFooter(true, 1));
	total = Raports.getRaportTotal();
	List<HashMap<String,Object>> listTotal = Raports.getSecondRaport();
	listTotal.addAll(Raports.getTableFooter(false, 1));
%>

<aui:layout>
	<a class="btn btn-primary" onclick="exportRaport('<portlet:namespace />')"> Export Excel </a>
	<a class="btn btn-primary" onclick="actualizeaza()"> Actualizeaza informatii </a> 
	<aui:form id="exportForm" name="exportForm" action="<%=exportURL.toString()%>"> </aui:form>
	<aui:form id="actualizareForm" name="actualizareForm" action="<%=submitURL%>"> </aui:form>
</aui:layout>

<aui:layout>
	<aui:column first="true" columnWidth="100">
		<div id="raport"></div>
	</aui:column>
</aui:layout>

<%-- HTML for PAGINATION --%>
<%@ include file="/html/includes/pagination.jsp" %>

<aui:layout>
	<aui:column first="true" columnWidth="100">
		<div id="raport_total"></div>
	</aui:column>
</aui:layout>

<script type="text/javascript">
var start = <%=start%>;
var count = <%=count%>;
var total = <%=total%>;
var dataTable;
var dataTableTotal;
function actualizeaza(){
	$('#<portlet:namespace/>actualizareForm').submit();
}
function exportRaport(portletId){
	$('#'+portletId+'exportForm').submit();
}

YUI().use('aui-datepicker', 'aui-datatable', 'aui-datatype', 'datatable-sort',
	function(Y) {
	/** GENERIC CURRENCY FORMATTER **/
	function formatCurrency(cell) {
	    format = {
	    	thousandsSeparator: ",",
	        decimalSeparator: ".",
	        decimalPlaces: 2
	    };
	    return Y.DataType.Number.format(Number(cell.value), format);
	}
	
	var remoteData = [
	                  <% for (int i = 0; i < list.size(); i++) { %>
	                  {
	                	  code : '<%=list.get(i).get("code")%>',
	                	  denumire : '<%=list.get(i).get("denumire")%>', 
	                	  buget_an : <%=list.get(i).get("buget_an")%>,
	                	  actual_ian : <%=list.get(i).get("actual_ian")%>,
	                	  actual_feb : <%=list.get(i).get("actual_feb")%>,
	                	  actual_mar : <%=list.get(i).get("actual_mar")%>,
	                	  actual_apr : <%=list.get(i).get("actual_apr")%>,
	                	  actual_mai : <%=list.get(i).get("actual_mai")%>,
	                	  actual_iun: <%=list.get(i).get("actual_iun")%>,
	                	  actual_iul : <%=list.get(i).get("actual_iul")%>,
	                	  actual_aug : <%=list.get(i).get("actual_aug")%>,
	                	  actual_sept : <%=list.get(i).get("actual_sept")%>,
	                	  actual_oct : <%=list.get(i).get("actual_oct")%>,
	                	  actual_noi : <%=list.get(i).get("actual_noi")%>,
	                	  actual_dec : <%=list.get(i).get("actual_dec")%>,
	                	  total : <%=list.get(i).get("total")%>,
	                	  diferenta : <%=list.get(i).get("diferenta")%>
	                  }
	                  <% if (i != (list.size() - 1)) { out.print(","); } %>
	           		  <% } %>
	                  ];
	var nestedCols = [
						 {key: 'code', label: 'Cod'}, 
						 {key: 'denumire', label:'Denumire'},
						 {key: 'buget_an', label:'Buget an', formatter: formatCurrency, className: 'currencyCol'},
						 {key: 'actual_ian', label: 'Actual Ian', formatter: formatCurrency, className: 'currencyCol'},
						 {key: 'actual_feb', label: 'Actual Feb',formatter: formatCurrency, className: 'currencyCol'},
						 {key: 'actual_mar', label: 'Actual Mar',formatter: formatCurrency, className: 'currencyCol'},
						 {key: 'actual_apr', label: 'Actual Apr',formatter: formatCurrency, className: 'currencyCol'},
						 {key: 'actual_mai', label: 'Actual Mai',formatter: formatCurrency, className: 'currencyCol'},
						 {key: 'actual_iun', label: 'Actual Iun',formatter: formatCurrency, className: 'currencyCol'},
						 {key: 'actual_iul', label: 'Actual Iul',formatter: formatCurrency, className: 'currencyCol'},
						 {key: 'actual_aug', label: 'Actual Aug',formatter: formatCurrency, className: 'currencyCol'},
						 {key: 'actual_sept', label: 'Actual Sept',formatter: formatCurrency, className: 'currencyCol'},
						 {key: 'actual_oct', label: 'Actual Oct',formatter: formatCurrency, className: 'currencyCol'},
						 {key: 'actual_noi', label: 'Actual Noi',formatter: formatCurrency, className: 'currencyCol'},
						 {key: 'actual_dec', label: 'Actual Dec',formatter: formatCurrency, className: 'currencyCol'},
						 {key: 'total', label: 'Total',formatter: formatCurrency, className: 'currencyCol'},
						 {key: 'diferenta', label: 'Actual vs Buget',formatter: formatCurrency, className: 'currencyCol'}
	                  ];
	dataTable = new Y.DataTable({
	    columns: nestedCols,
	    data: remoteData,	
	    editEvent: 'click',
	    recordType: ['code' ,'denumire', 'buget_an', 'actual_ian', 'actual_feb', 'actual_mar', 'actual_apr', 'actual_mai', 'actual_iun', 'actual_iul'
	                 , 'actual_aug', 'actual_sept', 'actual_oct', 'actual_noi', 'actual_dec', 'total', 'diferenta']
	}).render('#raport');
	
	var remoteDataTotal = [
	                  <% for (int i = 0; i < listTotal.size(); i++) { %>
	                  {
	                	  denumire : '<%=listTotal.get(i).get("denumire")%>', 
	                	  spatiu : '',
	                	  buget_an : <%=listTotal.get(i).get("buget_an")%>,
	                	  actual_ian : <%=listTotal.get(i).get("actual_ian")%>,
	                	  actual_feb : <%=listTotal.get(i).get("actual_feb")%>,
	                	  actual_mar : <%=listTotal.get(i).get("actual_mar")%>,
	                	  actual_apr : <%=listTotal.get(i).get("actual_apr")%>,
	                	  actual_mai : <%=listTotal.get(i).get("actual_mai")%>,
	                	  actual_iun: <%=listTotal.get(i).get("actual_iun")%>,
	                	  actual_iul : <%=listTotal.get(i).get("actual_iul")%>,
	                	  actual_aug : <%=listTotal.get(i).get("actual_aug")%>,
	                	  actual_sept : <%=listTotal.get(i).get("actual_sept")%>,
	                	  actual_oct : <%=listTotal.get(i).get("actual_oct")%>,
	                	  actual_noi : <%=listTotal.get(i).get("actual_noi")%>,
	                	  actual_dec : <%=listTotal.get(i).get("actual_dec")%>,
	                	  total : <%=listTotal.get(i).get("total")%>,
	                	  diferenta : <%=listTotal.get(i).get("diferenta")%>
	                  }
	                  <% if (i != (listTotal.size() - 1)) { out.print(","); } %>
	           		  <% } %>
	                  ];
	var nestedColsTotal = [
						 {key: 'denumire', label:'Denumire'},
						 {key: 'spatiu', label:' '},
						 {key: 'buget_an', label:'Buget an', formatter: formatCurrency, className: 'currencyCol'},
						 {key: 'actual_ian', label: 'Actual Ian', formatter: formatCurrency, className: 'currencyCol'},
						 {key: 'actual_feb', label: 'Actual Feb',formatter: formatCurrency, className: 'currencyCol'},
						 {key: 'actual_mar', label: 'Actual Mar',formatter: formatCurrency, className: 'currencyCol'},
						 {key: 'actual_apr', label: 'Actual Apr',formatter: formatCurrency, className: 'currencyCol'},
						 {key: 'actual_mai', label: 'Actual Mai',formatter: formatCurrency, className: 'currencyCol'},
						 {key: 'actual_iun', label: 'Actual Iun',formatter: formatCurrency, className: 'currencyCol'},
						 {key: 'actual_iul', label: 'Actual Iul',formatter: formatCurrency, className: 'currencyCol'},
						 {key: 'actual_aug', label: 'Actual Aug',formatter: formatCurrency, className: 'currencyCol'},
						 {key: 'actual_sept', label: 'Actual Sept',formatter: formatCurrency, className: 'currencyCol'},
						 {key: 'actual_oct', label: 'Actual Oct',formatter: formatCurrency, className: 'currencyCol'},
						 {key: 'actual_noi', label: 'Actual Noi',formatter: formatCurrency, className: 'currencyCol'},
						 {key: 'actual_dec', label: 'Actual Dec',formatter: formatCurrency, className: 'currencyCol'},
						 {key: 'total', label: 'Total',formatter: formatCurrency, className: 'currencyCol'},
						 {key: 'diferenta', label: 'Actual vs Buget',formatter: formatCurrency, className: 'currencyCol'}
	                  ];
	dataTableTotal = new Y.DataTable({
	    columns: nestedColsTotal,
	    data: remoteDataTotal,	
	    editEvent: 'click',
	    recordType: ['denumire', 'spatiu', 'buget_an', 'actual_ian', 'actual_feb', 'actual_mar', 'actual_apr', 'actual_mai', 'actual_iun', 'actual_iul'
	                 , 'actual_aug', 'actual_sept', 'actual_oct', 'actual_noi', 'actual_dec', 'total', 'diferenta']
	}).render('#raport_total');
	
	// PAGINATION
	// set status of navigation buttons on page load
	setNavigationButtonsState('<portlet:namespace />');
	$('.navigation').click(function(e) {
		var elementT 	= e.target;
		var elementId 	= elementT.id;
		var action 		= "";
		
		// determine which button was pressed and set the value of start accordingly
		if (elementId.indexOf("first")> -1)	{ action = "first"; start = 0; }
		if (elementId.indexOf("prev") > -1) 	{ action = "prev"; if (start - count <= 0) {start = 0} else { start -= count; } }
		if (elementId.indexOf("next") > -1) 	{ action = "next"; start = parseInt(start) + parseInt(count); }
		if (elementId.indexOf("last") > -1) 	{ action = "last"; start = parseInt(parseInt(total) / parseInt(count)) * count; if (start == total) { start -= count; } }
		
		makeAjaxCall(action);
	});
	
	function makeAjaxCall(action) {
		$.ajax({
			type: "POST",
			url: "<%=ajaxURL%>",
			data: 	"<portlet:namespace />action=" + action + 
					"&<portlet:namespace />start=" + start + 
					"&<portlet:namespace />count=" + count +
					"&<portlet:namespace />total=" + total,
			success: function(msg) {
				// get table data
				if (msg != ""){
					var jsonEntries = jQuery.parseJSON(msg);
					total = jsonEntries.total;
					dataTable.set('data', eval(jsonEntries.values));
				} else {
					dataTable.set('data', []);
				}
				// set status of navigation buttons on page load
				setNavigationButtonsState('<portlet:namespace />');
			}
		});
	}
	
});
</script>
