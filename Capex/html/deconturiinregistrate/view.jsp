<%@page import="com.profluo.ecm.model.db.CashExpense"%>
<%@taglib uri="http://java.sun.com/portlet_2_0" 	prefix="portlet"%>
<%@taglib uri="http://liferay.com/tld/theme" 		prefix="liferay-theme"%>
<%@taglib uri="http://liferay.com/tld/ui" 			prefix="liferay-ui"%>
<%@taglib uri="http://alloy.liferay.com/tld/aui" 	prefix="aui"%>
<%@taglib uri="http://liferay.com/tld/util" 		prefix="liferay-util"%>

<%@page import="java.util.HashMap"%>
<%@page import="java.util.List"%>

<liferay-theme:defineObjects />
<portlet:defineObjects />

<portlet:resourceURL id="ajaxURL" var="ajaxURL"/>

<portlet:renderURL var="viewURL" >
	<portlet:param name="mvcPath" value="/html/includes/visualizations/vizualizare_decont.jsp"></portlet:param>
</portlet:renderURL>

<%
	int start = 0; 
	int count = 15;
	int total = 0;
	
	List<HashMap<String,Object>> allcahsExp = CashExpense.getAllRegistered(start, count, "", "");
	total = CashExpense.getAllRegisteredCount("", "");
%>

<aui:layout>
	<aui:column first="true" columnWidth="100" last="true">
		<div id="listing-deconturi-inregistrate"></div>
	</aui:column>
</aui:layout>

<%-- HTML for PAGINATION --%>
<%@ include file="/html/includes/pagination.jsp" %>


<script type="text/javascript">
var start = "<%=start%>";
var count = "<%=count%>";
var total = "<%=total%>"
var number = '';
var furnizor = '';
YUI({lang: 'ro'}).use('aui-datepicker', 'aui-datatable', 'aui-datatype', 'datatable-sort',
		function(Y) {
	
	var remoteData = [
	            		<%
	            		if (allcahsExp != null) {
	            		for (int i = 0; i < allcahsExp.size(); i++) {	
	            		%>
	            		{
	            			cash_id: 			<%=allcahsExp.get(i).get("cash_id")%>,
	            			cash_exp_no: 	'<%=allcahsExp.get(i).get("cash_exp_no")%>',
	            			data_import: 	'<%=allcahsExp.get(i).get("data_import")%>',
	            			supplier_code: 	'<%=allcahsExp.get(i).get("supplier_code")%>',
	            			cash_exp_date: 		'<%=allcahsExp.get(i).get("cash_exp_date")%>',
	            			furnizor: 	'<%=allcahsExp.get(i).get("furnizor")%>',
	            			societate: 	'<%=allcahsExp.get(i).get("societate")%>',
	            			valoare: 		'<%=allcahsExp.get(i).get("valoare")%>',
	            			currency: 		'<%=allcahsExp.get(i).get("currency")%>',
	            			actiuni: <%=allcahsExp.get(i).get("cash_id")%>
	            		}
	            		<% if (i != (allcahsExp.size() - 1)) { out.print(","); } %>
	            		<% } } %>
	            		];
	
		var nestedCols = [
	        		    { key: 'cash_id', className: 'hiddencol'},
	        		    { key: 'cash_exp_no', label: '<input class="field" style="width:100px !important;margin:0" type="text" value="" id="filter-nr" name="filter-nr" placeholder="Nr. decont"></input>'},
	        		    { key: 'data_import', label: 'Data import'},
	        		    { key: 'supplier_code', label: 'Cod furnizor'},
	        		    { key: 'furnizor', label: '<input class="field" style="width:150px !important;margin:0" type="text" value="" id="filter-furnizor" name="filter-furnizor" placeholder="Furnizor"></input>'},
	        		    { key: 'cash_exp_date', label: 'Data document'},
	        		    { key: 'valoare', label: 'Valoare cu TVA'},
	        		    { key: 'currency', label: 'Moneda'},
	        		    { key: 'societate', label: 'Societate'},
	        		    { key: 'actiuni', label: 'Actiuni', allowHTML: true, 
	        		    	formatter: '<a href="#" id="row{cash_id}" class="editrow">Vizualizare</a>'}
	        		    ];
		
		// TABLE INIT
  		var dataTable = new Y.DataTable({
  		    columns: nestedCols,
  		    data: remoteData,
	  		scrollable: "x",
	        width: "100%",
  		    recordType: ['cash_id', 'cash_exp_no', 'cash_exp_date', 'furnizor', 
  		                 'societate', 'valoare', 'currency'],
  		    editEvent: 'click'
  		}).render('#listing-deconturi-inregistrate');
		
  	// PAGINATION
  		// set status of navigation buttons on page load
  		setNavigationButtonsState('<portlet:namespace />');
  		$('.navigation').click(function(e) {
  			var elementT 	= e.target;
  			var elementId 	= elementT.id;
  			var action 		= "";
  			
  			// determine which button was pressed and set the value of start accordingly
  			if (elementId.indexOf("first") > -1)	{ action = "first"; start = 0; }
  			if (elementId.indexOf("prev") > -1) 	{ action = "prev"; if (start - count <= 0) {start = 0} else { start -= count; } }
  			if (elementId.indexOf("next") > -1) 	{ action = "next"; start = parseInt(start) + parseInt(count); }
  			if (elementId.indexOf("last") > -1) 	{ action = "last"; start = parseInt(parseInt(total) / parseInt(count)) * count; if (start == total) { start -= count; } }
  			
  			//makeAjaxCall(action);
  		});
  		
  		function makeAjaxCall(action) {
  			$.ajax({
  				type: "POST",
  				url: "<%=ajaxURL%>",
  				data: 	"<portlet:namespace />action=" + action + 
  						"&<portlet:namespace />start=" + start + 
  						"&<portlet:namespace />count=" + count +
  						"&<portlet:namespace />total=" + total +
  						"&<portlet:namespace />number=" + number +
  						"&<portlet:namespace />supplier=" + supplier,
  				success: function(msg) {
  					// get table data
  					if (msg != ""){
  						var jsonEntries = jQuery.parseJSON(msg);
  						total = jsonEntries.total;
  						dataTable.set('data', eval(jsonEntries.values));
  					} else {
  						dataTable.set('data', []);
  					}
  					
  					// set status of navigation buttons on page load
  					setNavigationButtonsState('<portlet:namespace />');
  				}
  			});
  		}
  		
  		// filtrare dupa nr factura
  		$('#filter-nr').keyup(function (e) {
  		    if ($("#filter-nr").is(":focus") && (e.keyCode == 13)) {
  				var elementT 	= e.target;
  				var elementId 	= elementT.id;
  				var action 		= "filter";
  				number = elementT.value;
  				supplier = $('#filter-furnizor').val();
  				start = 0;
  				total = 0;
  				makeAjaxCall(action);
  		    }
  		});
  		
  		$('#filter-furnizor').keyup(function (e) {
  		    if ($("#filter-furnizor").is(":focus") && (e.keyCode == 13)) {
  				var elementT 	= e.target;
  				var elementId 	= elementT.id;
  				var action 		= "filter";
  				supplier = elementT.value;
  				number = $('#filter-nr').val();
  				start = 0;
  				total = 0;
  				makeAjaxCall(action);
  		    }
  		});
  		
  		dataTable.delegate('click', function (e) {
  		    // undefined to trigger the emptyCellValue
  		    e.preventDefault();
  		    var elem = e.target.get('id');
  		   	var cashId = elem.replace("row", "");
  		   	$('#<portlet:namespace/>id').val(cashId);
  			$('#<portlet:namespace/>viewDoc').submit();
  		}, '.editrow', dataTable);
		
	});

</script>

<aui:form method="post" action="<%=viewURL.toString() %>" name="viewDoc" id="viewDoc">
	<aui:input type="hidden" id="id" name="id"/>
</aui:form>
