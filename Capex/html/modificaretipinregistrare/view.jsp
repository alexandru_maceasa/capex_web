<%@page import="com.profluo.ecm.model.vo.DefCompaniesVo"%>
<%@page import="com.profluo.ecm.model.db.ModificareTipInregistrareDB"%>
<%@page import="com.profluo.ecm.model.db.DefStore"%>
<%@page import="com.profluo.ec.carrefour.htmlutils.DropDownUtils"%>
<%@page import="com.profluo.ecm.model.db.ListaInit"%>
<%@page import="com.profluo.ecm.model.db.DefNewProj"%>
<%@page import="com.profluo.ecm.model.db.InvoiceHeader"%>
<%@page import="com.profluo.ecm.controller.UserTeamIdUtils"%>
<%@page import="com.profluo.ecm.model.db.DefCompanies"%>
<%@page import="com.profluo.ecm.model.db.InventoryHeader"%>
<%@taglib uri="http://java.sun.com/portlet_2_0" 	prefix="portlet"%>
<%@taglib uri="http://liferay.com/tld/theme" 		prefix="liferay-theme"%>
<%@taglib uri="http://liferay.com/tld/ui" 			prefix="liferay-ui"%>
<%@taglib uri="http://alloy.liferay.com/tld/aui" 	prefix="aui"%>
<%@taglib uri="http://liferay.com/tld/util" 		prefix="liferay-util"%>

<%@page import="java.util.HashMap"%>
<%@page import="java.util.List"%>

<liferay-theme:defineObjects />
<portlet:defineObjects />

<%

	// retrieve all companies from DB
	List<HashMap<String,Object>> allCompanies = DefCompaniesVo.getInstance();
	int start = 0; 
	int count = 10;
	List<HashMap<String,Object>> allInvLines = ModificareTipInregistrareDB.getAllInfoWithLimit(start, count, "", "", "", "");
	int total = ModificareTipInregistrareDB.getInventoryTotal("", "", "", "");
%>
<portlet:resourceURL id="ajaxURL" var="ajaxURL"/>

<portlet:actionURL name="modifyTipInreg" var="modifyTipInregURL" />

<portlet:resourceURL id="exportAction" var="exportURL">
	<portlet:param name="action" value="exportExcel"/>
</portlet:resourceURL>

<liferay-ui:success message="update_ok" key="update_ok"/>
<liferay-ui:error message="update_nok" key="update_nok"/>
<liferay-ui:error message="Modificarea nu a fost efectuata cu succes deoarece exista inregistrari fara document setat. Va rugam setati documentul!" key="missing_doc"/>


<%if (user.getUserId() == UserTeamIdUtils.USER_TEST) {%>
	<aui:layout>
		<aui:form id="exportForm" name="exportForm" action="<%=exportURL.toString()%>">
		<aui:column>
			<label for="<portlet:namespace />company" style="padding-top: 5px">Societatea</label>
		</aui:column>
		<aui:column>
			<aui:select id="company" name="company" label="">
				<aui:option selected="selected" value="0"/>
				<% for (int i = 0 ; i < allCompanies.size(); i++) { %>
					<aui:option value="<%=i+1%>"> <%=allCompanies.get(i).get("name").toString()%> </aui:option>
				<% } %>
			</aui:select>
		</aui:column>
		<aui:column>
			<a class="btn btn-primary" onclick="exportDocumente('<portlet:namespace />')"> Export Documente </a>
		</aui:column>
	</aui:form>
	</aui:layout>
<% } %>
<aui:layout>
	<aui:column columnWidth="4" first="true">
		<label for="<portlet:namespace />documentToSet" style="padding-top: 5px">&nbsp&nbspDocument</label>
	</aui:column>
	<aui:column columnWidth="14">
		<aui:input type="text" id="documentToSet" name="documentToSet" value="" label="" />
	</aui:column>
<%-- 	<aui:form method="post" action="<%=modifyTipInregURL.toString() %>" name="modify1" id="modify" style="margin:0;padding:0"> --%>
		<aui:layout>
			<aui:column first="true">
				<a class="btn btn-primary" onclick="setDocument()"> Aplica document </a>
			</aui:column>
		</aui:layout>
<%-- 	</aui:form> --%>
</aui:layout>
<aui:layout>
	<aui:column>
		<aui:form method="post" action="<%=modifyTipInregURL.toString() %>" name="modify" id="modify" style="margin:0;padding:0">
			<aui:layout>
				<aui:column>
					<aui:column>
						<a class="btn btn-primary" onclick="modifyRegType()"> Modificare tip inregistrare </a>
					</aui:column>
				</aui:column>
			</aui:layout>
			<aui:input type="hidden" id="filterCompany" name="filterCompany" value="" />
			<aui:input type="hidden" id="filterSupplierName" name="filterSupplierName" value="" />
			<aui:input type="hidden" id="filterSupplierCode" name="filterSupplierCode" value="" />
			<aui:input type="hidden" id="filterDocumentType" name="filterDocumentType" value="" />
		</aui:form>
	</aui:column>
</aui:layout>
<br/>
<aui:layout>
	<aui:column first="true" columnWidth="100" last="true">
		<div id="inventar"></div>
	</aui:column>
</aui:layout>



<script type="text/javascript">
var start = <%=start%>
var count = <%=count%>
var total = <%=total%>
var company_id = "";
var supplierName = "";
var supplier_code = "";
var documentType = "";
</script>

<%-- HTML for PAGINATION --%>
<%@ include file="/html/includes/pagination.jsp" %>

<script type="text/javascript">

function exportDocumente(portletId) {
	var company = $('#' + portletId + 'company').val();
	if ( !company || company == 0){
		alert("Va rugam sa selectati societatea.");
		return;
	}
	$('#'+portletId+'exportForm').submit();
}

YUI({ lang: 'ro' }).use('aui-datatable','aui-datatype','datatable-sort', 'aui-datepicker', 'aui-base', 'liferay-portlet-url', 'aui-node',
		function(Y){
		var remoteData = [
	      			<% for (int i = 0; i < allInvLines.size(); i++) { %>
	      				{
	      					societate: 		'<%=allInvLines.get(i).get("societate")%>',
	      					codMagazin:		'<%=allInvLines.get(i).get("codMagazin")%>',
	      					magazin:		'<%=allInvLines.get(i).get("magazin")%>',
	      					codLot:			'<%=allInvLines.get(i).get("codLot")%>',
	      					lot:			'<%=allInvLines.get(i).get("lot")%>',
	      					codProdus:		'<%=allInvLines.get(i).get("codProdus")%>',
	      					denumireProdus:	'<%=allInvLines.get(i).get("denumireProdus")%>',
	      					tipInreg:		'<%=allInvLines.get(i).get("tipInreg")%>',
	      					gestiune:		'<%=allInvLines.get(i).get("gestiune")%>',
	      					nrInventar: 	'<%=allInvLines.get(i).get("nrInventar")%>',
	      					valoareRON: 	'<%=allInvLines.get(i).get("valoareRON")%>',
	      					valoareCURR: 	'<%=allInvLines.get(i).get("valoareCURR")%>',
	      					grupaIFRS: 		'<%=allInvLines.get(i).get("grupaIFRS")%>',
	      					durataIFRS: 	'<%=allInvLines.get(i).get("durataIFRS")%>',
	      					grupaIAS: 		'<%=allInvLines.get(i).get("grupaIAS")%>',
	      					durataIAS: 		'<%=allInvLines.get(i).get("durataIAS")%>',
	      					codFurnizor: 	'<%=allInvLines.get(i).get("codFurnizor")%>',
	      					furnizor: 		'<%=allInvLines.get(i).get("furnizor")%>',
	      					luna: 			'<%=allInvLines.get(i).get("luna")%>',
	      					dataPIF: 		'<%=allInvLines.get(i).get("dataPIF")%>',
	      					actMF: 			'<%=allInvLines.get(i).get("actMF")%>',
	      					detinator: 		'<%=allInvLines.get(i).get("detinator")%>',
	      					dpi: 			'<%=allInvLines.get(i).get("dpi")%>',
	      					nrContract: 	'<%=allInvLines.get(i).get("nrContract")%>',
	      					receptie: 		'<%=allInvLines.get(i).get("receptie")%>',
	      					cont: 			'<%=allInvLines.get(i).get("cont")%>',
	      					docNo: 			'<%=allInvLines.get(i).get("docNo")%>',
	      					dataDoc:		'<%=allInvLines.get(i).get("dataDoc")%>',
	      					docType:		'<%=allInvLines.get(i).get("docType")%>',
	      					document:     	'<%=allInvLines.get(i).get("document")%>',
	      					tipInregFinal: 	'<%=allInvLines.get(i).get("tipInregFinal")%>'
	      				}
	      				<% if (i != (allInvLines.size() - 1)) { out.print(","); } %>
	      			<% } %>
	      		];
		var nestedColumns = [		
							{key: 'societate', label: '<select id="filter-societate" name="filter-societate" label="" style="width:100px;margin:0">' +
							'<option selected="selected" value="">Societate</option>' +
							<% for (int i = 0; i < allCompanies.size(); i++) { %>
							<% if (allCompanies.get(i).get("status").equals("A")) {%>
							<% out.println("'<option value=\"" + allCompanies.get(i).get("id") +"\">" + allCompanies.get(i).get("name") +"</option>' + "); %>
							<% } } %>
							'</select>'},
							{key: 'codMagazin', 	label:'Cod magazin'},
							{key: 'magazin', 	label:'Denumire magazin'},
							{key: 'codLot', 	label:'Cod lot'},
							{key: 'lot', 		label:'Lot'},
							{key: 'codProdus', 	label:'Cod produs'},
							{key: 'denumireProdus', label:'Denumire produs'},
							{key: 'tipInreg', 	label:'Tip obiect'},
							{key: 'tipInregFinal', 	label:'Tip obiect final'},
							{key: 'gestiune', 	label:'Clasificare'},
		         			{key: 'nrInventar', label: 'Numar inventar'},
		         			{key: 'valoareRON', label:'Valoare operatie - RON'},
		         			{key: 'valoareCURR', label:'Valoare operatie - EURO'},
		         			{key: 'grupaIFRS', 	label:'Grupa IFRS'},
		         			{key: 'durataIFRS', label:'Durata IFRS'},
		         			{key: 'grupaIAS', 	label:'Grupa IAS'},
		         			{key: 'durataIAS', label:'Durata IAS'},
		         			{key: 'codFurnizor', label: '<input class="field" style="width:70px !important;margin:0" type="text" value="" id="filter-supplier_code" name="filter-supplier_code" placeholder="Cod furnizor" style="margin:0"/>'},
		         			{key: 'furnizor', 	label: '<input class="field" type="text" value="" id="filter-supplier_name" name="filter-supplier_name" placeholder="Furnizor" style="margin:0"/>'},
		         			{key: 'luna', 		label:'Luna'},
		         			{key: 'docNo', 		label: 'Nr document'},
		         			{key: 'dataDoc',		label:'Data document'},
		         			{key: 'dataPIF', 	label:'Data PIF'},
		         			{key: 'actMF', 	label:'Tip operatie'},
		         			{key: 'detinator', 	label:'Active/ Property'},
		         			{key: 'dpi', 	label:'DPI'},
		         			{key: 'nrContract', 	label:'Nr. Contract'},
		         			{key: 'receptie', 	label:'Receptie'},
		         			{key: 'cont', 	label:'Cont contabil'},
		         			{key: 'document', label :'Document'},
		         	       	{key: 'docType', label: '<select id="filter-document-type" name="filter-document-type" label="" style="width:100px;margin:0">' +
							'<option selected="selected" value="">Toate</option>' +
							'<option value="0">Factura</option>' +
							'<option value="1">Bon cesiune</option>' +
							'<option value="2">Haine de lucru</option>' +
							'<option value="3">Deconturi</option>' +
							'</select>',  
							formatter: function (o) { 
						  		if (o.value == "0") { 
									return 'Factura'; 
								} else if (o.value == "1") { 
									return 'Bon cesiune'; 
								} else if (o.value == "2") { 
									return 'Haine de lucru'; 
								} else { 
									return 'Deconturi'; 
								}
							}
						}
		         		];
		dataTableIV = new Y.DataTable({
			columns:nestedColumns,
			data:remoteData,
			scrollable: "x",
	        width: "100%",
			recordType:['nrInventar', 'dataPIF', 'denumire', 'docNo', 'dataDoc', 'codSocietate', 'codMagazin', 'raion', 'docType'],
			editEvent:'click'
		}).render('#inventar');
		
		// PAGINATION
		// set status of navigation buttons on page load
		setNavigationButtonsState('<portlet:namespace />');
		$('.navigation').click(function(e) {
			var elementT 	= e.target;
			var elementId 	= elementT.id;
			var action 		= "";
			
			// determine which button was pressed and set the value of start accordingly
			if (elementId.indexOf("first")> -1)	{ action = "first"; start = 0; }
			if (elementId.indexOf("prev") > -1) 	{ action = "prev"; if (start - count <= 0) {start = 0} else { start -= count; } }
			if (elementId.indexOf("next") > -1) 	{ action = "next"; start = parseInt(start) + parseInt(count); }
			if (elementId.indexOf("last") > -1) 	{ action = "last"; start = parseInt(parseInt(total) / parseInt(count)) * count; if (start == total) { start -= count; } }
			
			makeAjaxCall(action);
		});
		
		//filtrare societate
		$('#filter-societate').change(function(e) {
			var elementT 	= e.target;
			var elementId 	= elementT.id;
			var action 		= "filter";
			// get selected value
			company_id = elementT.value;
			supplier_code = $('#filter-supplier_code').val();
			supplierName = $('#filter-supplier_name').val();
			documentType = $('#filter-document-type').val();
			// reset start page 
			start = 0;
			// reset count
			total = 0;
			// make ajax call
			makeAjaxCall(action);
		});
		
		$('#filter-document-type').change(function(e) {
			var elementT 	= e.target;
			var elementId 	= elementT.id;
			var action 		= "filter";
			// get selected value
			documentType = elementT.value;
			supplier_code = $('#filter-supplier_code').val();
			supplierName = $('#filter-supplier_name').val();
			company_id = $('#filter-societate').val();
			// reset start page 
			start = 0;
			// reset count
			total = 0;
			// make ajax call
			makeAjaxCall(action);
		});
		
		// filtrare dupa cod furnizor
			$('#filter-supplier_code').keyup(function (e) {
			    if ($("#filter-supplier_code").is(":focus") && (e.keyCode == 13)) {
					var elementT 	= e.target;
					var elementId 	= elementT.id;
					var action 		= "filter";
					// get selected value
					supplier_code = elementT.value;
		  			supplierName = $('#filter-supplier_name').val();
		  			company_id = $('#filter-societate').val();
		  			documentType = $('#filter-document-type').val();
					// reset start page 
					start = 0;
					// reset count
					total = 0;
					// make ajax call
					makeAjaxCall(action);
			    }
			});
			
			// filtrare dupa nume furnizor
			$('#filter-supplier_name').change(function (e) {
					var elementT 	= e.target;
					var elementId 	= elementT.id;
					var action 		= "filter";
					// get selected value
					supplierName = elementT.value;
		  			supplier_code = $('#filter-supplier_code').val();
		  			company_id = $('#filter-societate').val();
		  			documentType = $('#filter-document-type').val();
					// reset start page 
					start = 0;
					// reset count
					total = 0;
					// make ajax call
					makeAjaxCall(action);
			    
			});
		
});


	

function setDocument(){
	var doc = $('#<portlet:namespace />documentToSet').val();
	if (doc == ""){
		alert("Completati campul document!");
	} else {
		makeAjaxCallSetDoc("setDocument", doc);
	}
}
		
function modifyRegType(){
	if (confirm("Sunteti sigur/a ca doriti sa modificati tipul de inregistrare?")){
		$('#<portlet:namespace />filterCompany').val(company_id);
		$('#<portlet:namespace />filterSupplierName').val(supplierName);
		$('#<portlet:namespace />filterSupplierCode').val(supplier_code);
		$('#<portlet:namespace />filterDocumentType').val(documentType);
		$('#<portlet:namespace />modify').submit();
	}
}

function makeAjaxCall(action) {
	$.ajax({
		type: "POST",
		url: "<%=ajaxURL%>",
		data: 	"<portlet:namespace />action=" + action + 
				"&<portlet:namespace />start=" + start + 
				"&<portlet:namespace />count=" + count +
				"&<portlet:namespace />total=" + total +
				"&<portlet:namespace />company_id=" + company_id +
				"&<portlet:namespace />supplier_code=" + supplier_code +
				"&<portlet:namespace />documentType=" + documentType +
				"&<portlet:namespace />supplier_name=" + supplierName,
		success: function(msg) {
			// get table data
			if (msg != ""){
				var jsonEntries = jQuery.parseJSON(msg);
				total = jsonEntries.total;
				dataTableIV.set('data', eval(jsonEntries.values));
			} else {
				dataTableIV.set('data', []);
			}
			// set status of navigation buttons on page load
			setNavigationButtonsState('<portlet:namespace />');
		}
	});
}

function makeAjaxCallSetDoc(action, docName) {
	$.ajax({
		type: "POST",
		url: "<%=ajaxURL%>",
		data: 	"<portlet:namespace />action=" + action +
				"&<portlet:namespace />company_id=" + company_id +
				"&<portlet:namespace />supplier_code=" + supplier_code +
				"&<portlet:namespace />documentType=" + documentType +
				"&<portlet:namespace />supplier_name=" + supplierName +
				"&<portlet:namespace />doc=" + docName, 
		success: function(msg) {
			// get table data
			if (msg != ""){
				var jsonEntries = jQuery.parseJSON(msg);
				total = jsonEntries.total;
				dataTableIV.set('data', eval(jsonEntries.values));
			} else {
				dataTableIV.set('data', []);
			}
			// set status of navigation buttons on page load
			setNavigationButtonsState('<portlet:namespace />');
		}
	});
}

</script>

