<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme"%>

<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui"%>
<%@ taglib uri="http://alloy.liferay.com/tld/aui" prefix="aui"%>
<%@ taglib uri="http://liferay.com/tld/util" prefix="liferay-util"%>
<%@page import="com.profluo.ecm.model.vo.DefCompaniesVo"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.List"%>
<%@ page import="java.util.ResourceBundle"%>
<%@ page import="java.util.Locale"%>

<liferay-theme:defineObjects />
<portlet:defineObjects />

<%
	locale = renderRequest.getLocale();
	ResourceBundle resmain = portletConfig.getResourceBundle(locale);
	
%>
<%--redirect to page add_request.jsp --%>
<portlet:renderURL var="addAproveURL" >
	<portlet:param name="mvcPath" value="/html/aproveadvanceapproval/aprove.jsp"></portlet:param>
</portlet:renderURL>

<portlet:actionURL name="updateStage" var="updateStageURL" />
<liferay-ui:success key="update_ok" message="update_ok" />

<aui:form action="<%=updateStageURL%>" name="toUpdate" id="toUpdate" method="post">
	<aui:input type="hidden" id="sourcePage" name="sourcePage"/>
	<aui:input type="hidden" id="idToUpdate" name="idToUpdate"/>
	<aui:input type="hidden" id="refuzSauAproba" name="refuzSauAproba" value="" />
</aui:form>	
	
<aui:layout>
	<aui:column columnWidth="50" last="true" style="text-align:right">
	<%-- 
		<aui:select id="societate" name="societate" label="">
			<aui:option selected="selected" value="0">Societate</aui:option>
			<% for (int i = 0; i < allCompanies.size(); i++) { %>
				<% if (allCompanies.get(i).get("status").equals("A")) {%>
					<aui:option value="<%=allCompanies.get(i).get(\"name\")%>"><%=allCompanies.get(i).get("name")%></aui:option>
				<% } %>
			<% } %>
		</aui:select>
	--%>
	</aui:column>
</aui:layout>
<aui:layout>
	<aui:layout  style="margin-top:10px">
		<%@ include file="tabel_din_view/listing.jsp" %>
	</aui:layout>
	<legend></legend>
	<aui:layout>
		<aui:column last="true">
			<aui:button name="refuz" id="refuz" cssClass="btn btn-primary-red validateGroup" 
					value="<%=resmain.getString(\"refuz_la_plata\")%>"></aui:button>
			<aui:button name="bun_plata" id="bun_plata" cssClass="btn btn-primary validateGroup" 
					value="<%=resmain.getString(\"bun_de_plata\")%>"></aui:button>
		</aui:column>
	</aui:layout>
</aui:layout>

<aui:form method="post" action="<%=addAproveURL.toString() %>" name="toRedirect" id="toRedirect" style="margin:0;padding:0">
	<aui:input type="hidden" id="id" name="id"/>
	<aui:input type="hidden" id="categorie" name="categorie"/>
	<aui:input type="hidden" id="nr" name="nr"/>
	<aui:input type="hidden" id="datascadenta" name="datascadenta"/>
	<aui:input type="hidden" id="explicatii_plata" name="explicatii_plata"/>
</aui:form>