<%@page import="com.profluo.ecm.model.db.AdvanceRequest"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme"%>
<%@page import="com.liferay.portal.util.PortalUtil"%>

<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui"%>
<%@ taglib uri="http://alloy.liferay.com/tld/aui" prefix="aui"%>
<%@ taglib uri="http://liferay.com/tld/util" prefix="liferay-util"%>

<%@ page import="java.util.ResourceBundle"%>
<%@ page import="java.util.Locale"%>

<liferay-theme:defineObjects />
<portlet:defineObjects />

<portlet:renderURL var="backAproveURL" >
	<portlet:param name="mvcPath" value="/html/aproveadvanceapproval/view.jsp"></portlet:param>
</portlet:renderURL>
<portlet:resourceURL id="ajaxURL" var="ajaxURL"/>

<%
	locale = renderRequest.getLocale();
	ResourceBundle resmain = portletConfig.getResourceBundle(locale);
	
	String idSolicitarePlata 	= "";
	String status				= "ro";
	String whereAmI				= "aprove";
	List<HashMap<String,Object>> oneRow = null;
	String tip_capex = "0";
	String initiativa = "0";
	String prj = "0";
	try {
		idSolicitarePlata 		= renderRequest.getParameter("id").toString();
	} catch (Exception e) { }
	// solicitare existenta
	if(idSolicitarePlata.equals("") || idSolicitarePlata == null) {
		String url = PortalUtil.getCurrentCompleteURL(request);
		String [] params = url.split("&");
		String [] lastParameter = params[params.length-1].split("=");
		if (lastParameter[0].equals("id")){
			idSolicitarePlata = lastParameter[1];
		}
	}
	
	System.out.println("idSolicitarePlata acolo : " + idSolicitarePlata);
	
	oneRow = AdvanceRequest.getOne(idSolicitarePlata);
	tip_capex = oneRow.get(0).get("tip_capex").toString();
	initiativa = oneRow.get(0).get("id_initiative").toString();
	prj = oneRow.get(0).get("id_new_prj").toString();
	
	System.out.println("tip_capex : " + tip_capex);
	System.out.println("initiativa : " + initiativa);
	System.out.println("prj : " + prj);
%>

<portlet:actionURL name="updateStage" var="updateSingleStageURL" />

<aui:form action="<%=updateSingleStageURL%>"  method="post" name="update-stage-aprove">
	<liferay-ui:error key="update_nok" message="update_nok" />
	<%--input-ul hidden este pentru a putea prelua id-ul in controller --%>
	<aui:input type="hidden" name="idUpd" id="idUpd" value="<%= idSolicitarePlata %>" />
	<aui:input type="hidden" name="refuzlaplata" id="refuzlaplata" value="" />
</aui:form>

<liferay-ui:error key="error" message="A aparut o eroare in procesarea cererii. Va rugam sa incercati retrimitere formularului." />

<aui:layout  style="margin-top:10px">
	<%@include file="../includes/fact_info.jsp" %>
</aui:layout>
<legend></legend>
<%--linia de butoane--%>
<aui:fieldset>
	<aui:layout>
		<aui:column first="true">
			<aui:button id="back" name="back" cssClass="btn btn-primary-red" onClick="<%=backAproveURL.toString() %>" value="<%=resmain.getString(\"back\")%>"></aui:button>
		</aui:column>
		<aui:column last="true">
			<aui:button id="refuz_plata" name="refuz_plata" cssClass="btn btn-primary-red" value="<%=resmain.getString(\"refuz_la_plata\")%>"></aui:button>
			<aui:button id="bun_de_plata" name="bun_de_plata" cssClass="btn btn-primary" value="<%=resmain.getString(\"bun_de_plata\")%>"></aui:button>
		</aui:column>
	</aui:layout>
</aui:fieldset>
