<%@page import="com.profluo.ecm.model.db.DefStore"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.profluo.ecm.model.db.UsersInformations"%>
<%@page import="com.profluo.ecm.flows.AdvanceRequestStages"%>
<%@page import="com.profluo.ecm.controller.UserTeamIdUtils"%>
<%@page import="com.profluo.ecm.model.db.AdvanceRequest"%>
<%@page import="com.liferay.portal.service.UserLocalServiceUtil"%>
<%@page import="com.liferay.portal.model.User"%>
<%@page import="com.profluo.ecm.model.vo.DefCompaniesVo"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.List"%>

<%@page import="com.liferay.portal.kernel.log.Log" %>
<%@page import="com.liferay.portal.kernel.log.LogFactoryUtil" %>

<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme"%>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui"%>
<%@ taglib uri="http://alloy.liferay.com/tld/aui" prefix="aui"%>
<%@ taglib uri="http://liferay.com/tld/util" prefix="liferay-util"%>

<liferay-theme:defineObjects />
<portlet:defineObjects />

<portlet:resourceURL id="ajaxURL" var="ajaxURL"/>

<%

	Log log = LogFactoryUtil.getLog("PROFLUO-" + this.getClass().getName());
	List<HashMap<String,Object>> allCompanies = DefCompaniesVo.getInstance();
 // PAGINATION Start - env setup 
	int start = 0; 
	int count = 15;
	int userID = (int)user.getUserId();
	int total = 0;
	List<HashMap<String,Object>> allAdvReq = new ArrayList<HashMap<String, Object>>();
	allAdvReq = AdvanceRequest.getAdvanceRequestApprovers(start, count, 0, "", "", 0, "", (int)user.getUserId());
	total = AdvanceRequest.getAdvanceRequestApproversCount(start, count, 0,  "", "",  0, "", (int)user.getUserId());
	log.info("AllAdvancerequests : " + allAdvReq);
	User approver = null;
%>

<script type="text/javascript">
var start = "<%=start%>";
var count = "<%=count%>";
var total = "<%=total%>";
//filters
var companyId 	= 0;
var voucherNo 	= "";
var storeId 	= 0;
var voucherDate = "";
</script>

<aui:layout>
	<aui:column first="true" columnWidth="100" last="true">
		<div id="aprove_adv_approval"></div>
	</aui:column>
</aui:layout>

<%-- HTML for PAGINATION --%>
<%@ include file="/html/includes/pagination.jsp" %>

<%-- datepicker for table header --%>
<div id="filtru_data_factura" style="display:none">
<aui:input type="text" id="inv_date" name="inv_date" value="" placeholder="Data solicitare" label="" style="width:80px;margin-bottom:0px">
</aui:input>
</div>

<script type="text/javascript">
var dataTable;
YUI().use('aui-datepicker', 'aui-modal', 'aui-datatable', 'aui-datatype', 'datatable-sort',
	function(Y) {
		// called whenever a change in date selection was performed
		var oldDateValue = $('#<portlet:namespace />inv_date').val();
		function dateChanged(input) {
			console.log('date changed: ' + input.val());
			oldDateValue = input.val();
			// get selected value
			voucherDate = oldDateValue;
			// reset start page 
			start = 0;
			// reset count
			total = 0;
			action = "filter";
			// make ajax call
			makeAjaxCall(action);
		}
		
		var datepicker2 = new Y.DatePicker({
			trigger : '#<portlet:namespace />inv_date',
			mask : '%Y-%m-%d',
			calendar : {
				dateFormat : '%Y-%m-%d'
			},
			popover: {
				zIndex : 1,
		          toolbars: {
		            header: [[
		              {
		                icon:'icon-trash',
		                label: 'Reseteaza filtru',
		                on: {
		                  click: function() {
		                	  datepicker2.clearSelection();
		                  }
		                }
		              }]]
		          }
			},
			after: {
	            selectionChange: function(event) {
	            	if (oldDateValue != $('#<portlet:namespace />inv_date').val()) {
	            		oldDateValue = $('#<portlet:namespace />inv_date').val();
	            		dateChanged($('#<portlet:namespace />inv_date'));
	            	}
	            	$('#filtru_data_factura').html('');
	            }
	        }
		});
	
		// data array
		var remoteData = [
		<% for (int i = 0; i < allAdvReq.size(); i++) {
			int userA = 0;
			
			log.info("A ajung aici");
			
			if(!allAdvReq.get(i).get("approver").toString().equals("") && !allAdvReq.get(i).get("approver").toString().equals("0")){
				log.info("a intrat in if");
				userA = Integer.parseInt(allAdvReq.get(i).get("approver").toString()); 
	 			approver = UserLocalServiceUtil.getUserById(userA);
	 			log.info("userA : " + userA);
	 			log.info(approver);
			}
			int userId = Integer.parseInt(allAdvReq.get(i).get("id_user").toString()); 
 			User solicitant = UserLocalServiceUtil.getUserById(userId);
		%>
		{
				id: <%=allAdvReq.get(i).get("id") %>,
				solicitant: '<%= solicitant.getFullName()%>',
				functie:'<%=solicitant.getJobTitle()%>',
				<% if (solicitant.getAddresses().size() > 0) { %>
				store:'<%=solicitant.getAddresses().get(0).getStreet1()%>',
				<% } else { %>
				store:'<%=allAdvReq.get(i).get("store")%>',
				<% } %>
				company:'<%=allAdvReq.get(i).get("company")%>',
				categorie:'<%=allAdvReq.get(i).get("categorie")%>',
				furnizor:'<%=allAdvReq.get(i).get("furnizor")%>',
				id:<%=allAdvReq.get(i).get("id")%>,
				inv_date:'<%=allAdvReq.get(i).get("inv_date")%>',
				datascadenta:'<%=allAdvReq.get(i).get("datascadenta")%>',
				avans:<%=allAdvReq.get(i).get("avans")%>,
				currency:'<%=allAdvReq.get(i).get("currency")%>',
				explicatii_plata:'<%=allAdvReq.get(i).get("explicatii_plata")%>',
				<% if (userA != 0) { %>
				istoric:'<%=approver.getFullName()%>',
				<% } else { %>
				istoric:'',
				<% } %>
				vizualizeaza:'<a href="#" class="edit"><img src="${themeDisplay.getPathThemeImages()}/edit.png" width="24" style="margin-left: 10px"/></a>',
		}
		<% if (i != (allAdvReq.size() - 1)) { out.print(","); } %>
		<% } %>
		];

		// COLUMN INFO and ATTRIBUTES
		var nestedCols = [
		    { key: 'solicitant' , label : 'Solicitant'},
		    { key: 'functie' , label : 'Functie'},
		    { key: 'store', allowHTML: true, label: 'Magazin'
		    	<%--
				label: '<select id="filter-store" name="filter-store" label="" style="width:100px;margin:0">' +
				'<option selected="selected" value="">Magazin</option>' +
				<% for (int i = 0; i < allStores.size(); i++) { %>
				<% if (allStores.get(i).get("status").equals("A")) {%>
				<% out.println("'<option value=\"" + allStores.get(i).get("id") +"\" class=\"company-options-" + allStores.get(i).get("company_id") +"\">" 
					+ allStores.get(i).get("store_name") +"</option>' + "); %>
				<% } } %>
				'</select>'
				--%>
			},
			{ key: 'company', label: '<select id="filter-company" name="filter-company" label="" style="width:100px;margin:0">' +
				'<option selected="selected" value="0">Societate</option>' +
				<% for (int i = 0; i < allCompanies.size(); i++) { %>
				<% if (allCompanies.get(i).get("status").equals("A")) {%>
				<% out.println("'<option value=\"" + allCompanies.get(i).get("id") +"\">" + allCompanies.get(i).get("name") +"</option>' + "); %>
				<% } } %>
				'</select>'},
		    
			{ key: 'categorie', label : 'Categorie'},
			{ key: 'furnizor', label : 'Furnizor'},
			{ key: 'id', allowHTML: true,
				label: '<input name="numar_filtrare" id="numar_filtrare" placeholder="Nr." style="width:40px;margin-bottom:0px"></input>'},	
			{ key: 'inv_date' , label : 'Data solicitare', allowHTML: true, label: '<div id="header_inv_date"></div>'},
			{ key: 'datascadenta' , label : 'Data scadenta'},
			{ key: 'avans' , label : 'Avans'},
			{ key: 'currency' , label : 'Moneda'},
			{ key: 'explicatii_plata' , label : 'Explicatii plata'},
			{ key: 'vizualizeaza', label: 'Vizualizeaza', allowHTML: true, 
				// gets the id of the row and creates an unique link for every row
				formatter: '<a href="#" id="row{id}" class="getId">Document {id}</a>' },
			{ key: 'istoric' , label : 'Istoric aprobari'},
			{ key: 'select', className:'align_center',
				label:'<input type="checkbox" class="select-all align_center" />', allowHTML:  true,
				formatter: function (o) {
		            if (o.value == true) {
		            	return '<input type="checkbox" checked/>';
		            } else {
		            	return '<input type="checkbox" />';
		            }
		        },
                emptyCellValue: '<input type="checkbox" />'}
		];
		
		function redirect(sel_id) {
		    var ml  = dataTable.data, msg = '', template = '';
	
		    ml.each(function (item, i) {
		        var data = item.getAttrs(['id','solicitant', 'functie', 'mag_dep', 'company', 
		                                  'categorie', 'furnizor', 'inv_date', 'datascadenta', 
		                                  'avans', 'currency', 'explicatii_plata', 'istoric']);
		        if (data.id == sel_id) {
					$('#<portlet:namespace />id').val(data.id);
					$('#<portlet:namespace />categorie').val(data.categorie);
					$('#<portlet:namespace />nr').val(data.id);
					$('#<portlet:namespace />datascadenta').val(data.datascadenta);
					$('#<portlet:namespace />explicatii_plata').val(data.explicatii_plata);

					$('#<portlet:namespace />toRedirect').submit();
		        }
		    });
		}

		// TABLE INIT
		dataTable = new Y.DataTable({
		    columns: nestedCols,
		    data: remoteData,	
		    editEvent: 'click',
		    recordType: ['id', 'solicitant', 'functie', 'mag_dep', 'company', 'categorie', 
		                 'furnizor', 'inv_date', 'datascadenta', 'avans', 'currency',
		                 'explicatii_plata', 'vizualizeaza', 'istoric', 'select']
		}).render('#aprove_adv_approval');
		
		//dataTable.get('boundingBox').unselectable();
	
	    // Also define a listener on the single TH "checkbox" to
	    //   toggle all of the checkboxes
	    dataTable.delegate('click', function (e) {
	        // undefined to trigger the emptyCellValue
	        var checked = e.target.get('checked') || undefined;
	        // Set the selected attribute in all records in the ModelList silently
	        // to avoid each update triggering a table update
	        this.data.invoke('set', 'select', checked, { silent: true });
	        // Update the table now that all records have been updated
	        this.syncUI();
	        // workaraound in order to keep the header checkbox state after table reload
	        if (checked) {
	        	dataTable.get('contentBox').one('.select-all').set('checked', true);
	        }
	        $('#header_inv_date').html($('#filtru_data_factura').html());
	    }, '.select-all', dataTable);

	    // Define a listener on the DT first column for each record's "checkbox",
	    //   to set the value of `select` to the checkbox setting
	    dataTable.delegate("click", function(e){
	        // undefined to trigger the emptyCellValue
	        var checked = e.target.get('checked') || undefined;
	        // Don't pass `{silent:true}` if there are other objects in your app
	        // that need to be notified of the checkbox change.
	        this.getRecord(e.target).set('select', checked, { silent: true });
	        // Uncheck the header checkbox
	        this.get('contentBox').one('.select-all').set('checked', false);
	    }, ".table-col-select input", dataTable);
	    
	    dataTable.delegate('click', function (e) {
	    	 // undefined to trigger the emptyCellValue
		    var target = e.target.get('id');
			redirect(target.replace('row',''));
			//window.location.href=redirect_add;
		}, '.getId', dataTable);
	    
		// filter currency based on change
		$('#filter-company').change(function(e) {
			var elementT 	= e.target;
			var elementId 	= elementT.id;
			var action 		= "filter";
			// get selected value
			companyId = elementT.value;
			// reset start page 
			voucherNo 	= $('#numar_filtrare').val();
			storeId 	= 0;
			voucherDate = oldDateValue;
			start = 0;
			// reset count
			total = 0;
			// make ajax call
			makeAjaxCall(action);
		});
		
		// filtare dupa voucher no
		$('#numar_filtrare').keyup(function (e) {
		    if ($("#numar_filtrare").is(":focus") && (e.keyCode == 13)) {
				var elementT 	= e.target;
				var elementId 	= elementT.id;
				var action 		= "filter";
				
				// get selected value
				voucherNo = elementT.value;
				voucherDate = oldDateValue;
				storeId 	= 0;
				companyId = $('#filter-company').val();
				// reset start page 
				start = 0;
				// reset count
				total = 0;
				// make ajax call
				makeAjaxCall(action);				
		    }
		});

	    //for multiselection approval in view.jsp
	    Y.on("click", function(e){
	        e.preventDefault();
	        var action;
	        if ( e.target.get("id").toString().indexOf("refuz") >= 0) {
	        	action = "refuz";
	        } else {
	        	action = "aproba";
	        }
	        
	        var ml = dataTable.data, msg = '', template = '';
	    	var IDs = "";
	    	//save all id's checked in table
		    ml.each(function (item, i) {
		    	var data = item.getAttrs(['id', 'select']);
		    	
		    	if (data.select == true) {
		    		IDs += data.id + ",";
		    	}
		    });
	    	
	    	if (IDs != "") {
			    //remove the last ","
			   	IDs = IDs.substring(0, IDs.length - 1);
			    $('#<portlet:namespace />idToUpdate').val(IDs);
			    $('#<portlet:namespace />refuzSauAproba').val(action);
			    $('#<portlet:namespace />sourcePage').val("viewAprove");
			    $('#<portlet:namespace />toUpdate').submit();
	    	} else {
	    		alert("Pentru a efectua aceasta actiune trebuie sa selectati una sau mai multe intrari din lista.");
	    		//new Y.Modal({bodyContent: 'Pentru a efectua aceasta actiune trebuie sa selectati una sau mai multe intrari din lista.',
	    		//	centered: true, headerContent: '&nbsp;', modal: true, render: 'body', width: 450 }).render();
	    		return false;
	    	}
	    }, ".validateGroup");
	    
	    $(document).ready(function(){ 
	    	$('#filter-company').change(function(e) {
				var selectedCo = $('#filter-company').val();
				if (selectedCo == '') {
					for (var i = 0; i < 5; i++) {
							$('.company-options-' + i).show();
					}
				} else {
					$('#filter-store').val('');
					for (var i = 0; i < 5; i++) {
						if (i == selectedCo) {
							$('.company-options-' + i).show();
						} else {
							$('.company-options-' + i).hide();
						}
					}
				}
			});
	    });
	    
		// PAGINATION
		// set status of navigation buttons on page load
		setNavigationButtonsState('<portlet:namespace />');
		$('.navigation').click(function(e) {
			var elementT 	= e.target;
			var elementId 	= elementT.id;
			var action 		= "";
			
			// determine which button was pressed and set the value of start accordingly
			if (elementId.indexOf("first") > -1)	{ action = "first"; start = 0; }
			if (elementId.indexOf("prev") > -1) 	{ action = "prev"; if (start - count <= 0) {start = 0} else { start -= count; } }
			if (elementId.indexOf("next") > -1) 	{ action = "next"; start = parseInt(start) + parseInt(count); }
			if (elementId.indexOf("last") > -1) 	{ action = "last"; start = parseInt(parseInt(total) / parseInt(count)) * count; if (start == total) { start -= count; } }
			
			makeAjaxCall(action);
		});
		
		function makeAjaxCall(action) {
			$.ajax({
				type: "POST",
				url: "<%=ajaxURL%>",
				data: 	"<portlet:namespace />action=" + action + 
						"&<portlet:namespace />start=" + start + 
						"&<portlet:namespace />count=" + count +
						"&<portlet:namespace />total=" + total +
						"&<portlet:namespace />voucher_no=" + voucherNo +
						"&<portlet:namespace />voucher_date=" + voucherDate +
						"&<portlet:namespace />store_id=" + storeId +
						"&<portlet:namespace />company_id=" + companyId,
				success: function(msg) {
					// get table data
					if (msg != ""){
						var jsonEntries = jQuery.parseJSON(msg);
						total = jsonEntries.total;
						dataTable.set('data', eval(jsonEntries.values));
					} else {
						dataTable.set('data', []);
					}
					
					// set status of navigation buttons on page load
					setNavigationButtonsState('<portlet:namespace />');
				}
			});
		}
		// PAGINATION
	}
);

</script>

	