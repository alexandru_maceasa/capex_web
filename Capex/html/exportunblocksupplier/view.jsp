<%@taglib uri="http://java.sun.com/portlet_2_0" 	prefix="portlet"%>
<%@taglib uri="http://liferay.com/tld/theme" 		prefix="liferay-theme"%>
<%@taglib uri="http://liferay.com/tld/ui" 			prefix="liferay-ui"%>
<%@taglib uri="http://alloy.liferay.com/tld/aui" 	prefix="aui"%>
<%@taglib uri="http://liferay.com/tld/util" 		prefix="liferay-util"%>

<liferay-theme:defineObjects />
<portlet:defineObjects />

<portlet:resourceURL id="ajaxURL" var="ajaxURL"/>
<portlet:actionURL name="exportExcel" var="exportURL" />

<aui:layout>
	<aui:form id="exportUnblSuppliersKontan" name="exportUnblSuppliersKontan" action="<%=exportURL.toString()%>">
		<aui:column>
			<a class="btn btn-primary" onclick="exportUnblSupppKontan('<portlet:namespace />')"> Export Documente </a>
		</aui:column>
	</aui:form>
</aui:layout>

<script type="text/javascript">
function exportUnblSupppKontan(portletId){
	$('#'+portletId+'exportUnblSuppliersKontan').submit();
}
</script>