<%@page import="com.profluo.ecm.flows.AdvanceRequestStages"%>
<%@page import="com.profluo.ecm.controller.UserTeamIdUtils"%>
<%@page import="com.profluo.ecm.model.db.AdvanceRequest"%>
<%@page import="com.profluo.ecm.model.vo.DefCurrencyVo"%>
<%@page import="com.profluo.ecm.model.vo.DefCompaniesVo"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.List"%>
<%@ page import="java.util.ResourceBundle"%>
<%@ page import="java.util.Locale"%>

<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme"%>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui"%>
<%@ taglib uri="http://alloy.liferay.com/tld/aui" prefix="aui"%>
<%@ taglib uri="http://liferay.com/tld/util" prefix="liferay-util"%>

<liferay-theme:defineObjects />
<portlet:defineObjects />

<portlet:resourceURL id="ajaxURL" var="ajaxURL"/>

<%
	// retrieve all currencies from DB
	List<HashMap<String,Object>> allCurrencies = DefCurrencyVo.getInstance();
	//retrieve all advance requests
	int stage = AdvanceRequestStages.NONE;
	int id_store_aisle = AdvanceRequestStages.NONE;
	if (UserTeamIdUtils.belongsToTeam(user.getUserId(), UserTeamIdUtils.ACCOUNTING )) {
		stage = AdvanceRequestStages.VALIDATION_ACCOUNTING;
	}
	
	int start = 0; 
	int count = 15;
	
	int total = AdvanceRequest.getAdvanceRequestAccountingCount(start, count, 0, 0, "", "", 0, "");
	// List<HashMap<String,Object>> allAdvReq = AdvanceRequest.getAdvanceRequestByStage(whatStage);
	List<HashMap<String,Object>> allAdvReq = AdvanceRequest.getAdvanceRequestAccounting(start, count, 0, 0, "", "", 0, "");
%>

<script type="text/javascript">
var start = "<%=start%>";
var count = "<%=count%>";
var total = "<%=total%>";
var stage = "<%=stage%>";
//filters
var companyId 	= 0;
var voucherNo 	= "";
var storeId 	= 0;
var voucherDate = "";
var supplierCode = 0;
var supplierName = "";
</script>

<aui:column first="true" columnWidth="100" last="true">
	<div id="validate_adv_approval"></div>
</aui:column>

<%-- HTML for PAGINATION --%>
<%@ include file="/html/includes/pagination.jsp" %>

<script type="text/javascript">
var dataTable;
YUI().use( 'aui-datepicker', 'aui-datatable', 'aui-datatype', 'datatable-sort',
	function(Y) {
		// data array
		var remoteData = [
		<% for (int i = 0; i < allAdvReq.size(); i++) { %>
		{
				id:'<%=allAdvReq.get(i).get("id")%>',
				supplier_code:'<%=allAdvReq.get(i).get("supplier_code")%>',
				furnizor:'<%=allAdvReq.get(i).get("furnizor")%>',
				categorie:'<%=allAdvReq.get(i).get("categorie")%>',
				explicatii_plata:'<%=allAdvReq.get(i).get("explicatii_plata")%>',
				inv_date: '<%=allAdvReq.get(i).get("inv_date")%>',
				datascadenta:'<%=allAdvReq.get(i).get("datascadenta")%>',
				val_cu_tva:'<%=allAdvReq.get(i).get("val_cu_tva")%>',
				currency:'<%=allAdvReq.get(i).get("currency")%>',
				company:'<%=allAdvReq.get(i).get("company")%>',
				
		}
		<% if (i != (allAdvReq.size() - 1)) { out.print(","); } %>
		<% } %>
		];

		function formatCurrency(cell) {
		    format = {
		    	thousandsSeparator: ",",
		        decimalSeparator: ".",
		        decimalPlaces: noOfDecimalsToDisplay
		    };
		    return Y.DataType.Number.format(Number(cell.value), format);
		}
		
		// COLUMN INFO and ATTRIBUTES
		var nestedCols = [
		    { key: 'id', allowHTML: true,
				label: '<input class="field" name="nr_filtrare" id="nr_filtrare" placeholder="Doc Nr." style="width:80px;margin-bottom:0px"></input>'},	
			{ key: 'supplier_code', allowHTML: true,
				label: '<input class="field" name="code_filtrare" id="code_filtrare" placeholder="Cod Furnizor" style="width:100px;margin-bottom:0px"></input>'},		
			{ key: 'furnizor', allowHTML: true,
				label: '<input class="field" name="furnizor_filtrare" id="furnizor_filtrare" placeholder="Nume Furnizor" style="width:100px;margin-bottom:0px"></input>'},	
			{ key: 'inv_date' , label : 'Data solicitare' /*, allowHTML: true, label: '<div id="header_inv_date"></div>'*/},
			{ key: 'datascadenta' , label : 'Data scadenta'/*, allowHTML: true, label: '<div id="header_datascadenta"></div>'*/},
			{ key: 'val_cu_tva', label: 'Val cu TVA', formatter: formatCurrency, className: 'currencyCol'},
			{ key: 'currency', label: 'Moneda'},
			{ key: 'company', label: '<select id="filter-company" name="filter-company" label="" style="width:100px;margin:0">' +
				'<option selected="selected" value="0">Societate</option>' +
				<% for (int i = 0; i < allCompanies.size(); i++) { %>
					<% if (allCompanies.get(i).get("status").equals("A")) {%>
					<% out.println("'<option value=\"" + allCompanies.get(i).get("id") +"\">" + allCompanies.get(i).get("name") +"</option>' + "); %>
				<% } } %>
				'</select>'},
			{ key: 'explicatii_plata',className:'hidden'},
			{ key: 'categorie',className:'hidden'},
			{ key: 'actiuni', label: 'Actiuni', allowHTML: true, 
				// gets the id of the row and creates an unique link for every row
				formatter: '<a href="#" id="row{id}" class="goTo">Verificare cerere avans</a>' }	      
		];
		
		function redirect(sel_id) {
		    var ml  = dataTable.data, msg = '', template = '';
	
		    ml.each(function (item, i) {
		        var data = item.getAttrs(['id', 'explicatii_plata', 'categorie', 'cui', 'furnizor', 'inv_date', 
		                                  'datascadenta', 'val_cu_tva', 'currency', 'company']);
		        if (data.id == sel_id) {
					$('#<portlet:namespace />id').val(data.id);
					$('#<portlet:namespace />nr').val(data.id);
					$('#<portlet:namespace />datascadenta').val(data.datascadenta);
					$('#<portlet:namespace />currency').val(data.currency);
					$('#<portlet:namespace />explicatii_plata').val(data.explicatii_plata);
					$('#<portlet:namespace />categorie').val(data.categorie);
					$('#<portlet:namespace />toRedirect').submit();
		        }
		    });
		}
		// TABLE INIT
		dataTable = new Y.DataTable({
		    columns: nestedCols,
		    data: remoteData,	
		    editEvent: 'click',
		    recordType: ['id' , 'nr', 'cui', 'furnizor', 'inv_date', 'datascadenta', 'val_cu_tva', 'currency', 'company']
		}).render('#validate_adv_approval');
		
		//dataTable.get('boundingBox').unselectable();

	    dataTable.delegate('click', function (e) {
		    	// undefined to trigger the emptyCellValue
			    var target = e.target.get('id');
				redirect(target.replace('row',''));
				//window.location.href=redirect_add;
			}, '.goTo', dataTable);
	    
		// filter currency based on change
		$('#filter-company').change(function(e) {
			var elementT 	= e.target;
			var elementId 	= elementT.id;
			var action 		= "filter";
			// get selected value
			companyId = elementT.value;
  			supplierName = $('#furnizor_filtrare').val();
  			voucherNo = $('#nr_filtrare').val();
			supplierCode = $('#code_filtrare').val();
			if (supplierCode == "") {
				supplierCode = 0;
			}
			// reset start page 
			start = 0;
			// reset count
			total = 0;
			// make ajax call
			makeAjaxCall(action);
		});
		
		// filtare dupa supplier_code
		$('#code_filtrare').keyup(function (e) {
		    if ($("#code_filtrare").is(":focus") && (e.keyCode == 13)) {
				var elementT 	= e.target;
				var elementId 	= elementT.id;
				var action 		= "filter";
				
				// get selected value
				if (elementT.value != "") {
					supplierCode = elementT.value;
				} else {
					supplierCode = 0;
				}

	  			supplierName = $('#furnizor_filtrare').val();
	  			voucherNo = $('#nr_filtrare').val();
	  			companyId = $('#filter-company').val();
				// reset start page 
				start = 0;
				// reset count
				total = 0;
				// make ajax call
				makeAjaxCall(action);				
		    }
		});
		
		// filtare dupa supplier_name
		$('#furnizor_filtrare').keyup(function (e) {
		    if ($("#furnizor_filtrare").is(":focus") && (e.keyCode == 13)) {
				var elementT 	= e.target;
				var elementId 	= elementT.id;
				var action 		= "filter";
				
				// get selected value
				supplierName = elementT.value;
	  			voucherNo = $('#nr_filtrare').val();
	  			companyId = $('#filter-company').val();
				supplierCode = $('#code_filtrare').val();
				if (supplierCode == "") {
					supplierCode = 0;
				}
				// reset start page 
				start = 0;
				// reset count
				total = 0;
				// make ajax call
				makeAjaxCall(action);				
		    }
		});
		
		$('#nr_filtrare').keyup(function (e) {
		    if ($("#nr_filtrare").is(":focus") && (e.keyCode == 13)) {
				var elementT 	= e.target;
				var elementId 	= elementT.id;
				var action 		= "filter";
				
				// get selected value
				voucherNo = elementT.value;

	  			supplierName = $('#furnizor_filtrare').val();
	  			companyId = $('#filter-company').val();
				supplierCode = $('#code_filtrare').val();
				if (supplierCode == "") {
					supplierCode = 0;
				}
				// reset start page 
				start = 0;
				// reset count
				total = 0;
				// make ajax call
				makeAjaxCall(action);				
		    }
		});
	    
		// PAGINATION
		// set status of navigation buttons on page load
		setNavigationButtonsState('<portlet:namespace />');
		$('.navigation').click(function(e) {
			var elementT 	= e.target;
			var elementId 	= elementT.id;
			var action 		= "";
			
			// determine which button was pressed and set the value of start accordingly
			if (elementId.indexOf("first") > -1)	{ action = "first"; start = 0; }
			if (elementId.indexOf("prev") > -1) 	{ action = "prev"; if (start - count <= 0) {start = 0} else { start -= count; } }
			if (elementId.indexOf("next") > -1) 	{ action = "next"; start = parseInt(start) + parseInt(count); }
			if (elementId.indexOf("last") > -1) 	{ action = "last"; start = parseInt(parseInt(total) / parseInt(count)) * count; if (start == total) { start -= count; } }
			
			makeAjaxCall(action);
		});
		
		function makeAjaxCall(action) {
			$.ajax({
				type: "POST",
				url: "<%=ajaxURL%>",
				data: 	"<portlet:namespace />action=" + action + 
						"&<portlet:namespace />start=" + start + 
						"&<portlet:namespace />count=" + count +
						"&<portlet:namespace />total=" + total +
						"&<portlet:namespace />stage=" + stage +
						"&<portlet:namespace />voucher_no=" + voucherNo +
						"&<portlet:namespace />supplier_code=" + supplierCode +
						"&<portlet:namespace />supplier_name=" + supplierName +
						"&<portlet:namespace />store_id=" + storeId +
						"&<portlet:namespace />company_id=" + companyId,
				success: function(msg) {
					// get table data
					if (msg != ""){
						var jsonEntries = jQuery.parseJSON(msg);
						total = jsonEntries.total;
						dataTable.set('data', eval(jsonEntries.values));
					} else {
						dataTable.set('data', []);
					}
					
					// set status of navigation buttons on page load
					setNavigationButtonsState('<portlet:namespace />');
				}
			});
		}
		// PAGINATION    
	});
</script>

<aui:form method="post" action="<%=validateURL.toString() %>" name="toRedirect" id="toRedirect" style="margin:0;padding:0">
	<aui:input type="hidden" id="id" name="id"/>
	<aui:input type="hidden" id="nr" name="nr"/>
	<aui:input type="hidden" id="datascadenta" name="datascadenta"/>
	<aui:input type="hidden" id="currency" name="currency"/>
	<aui:input type="hidden" id="explicatii_plata" name="explicatii_plata"/>
	<aui:input type="hidden" id="categorie" name="categorie"/>
</aui:form>