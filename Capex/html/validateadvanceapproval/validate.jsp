<%@ page import="com.profluo.ecm.model.db.AdvanceRequest"%>
<%@ page import="java.util.ResourceBundle"%>
<%@ page import="java.util.Locale"%>
<%@page import="com.liferay.portal.util.PortalUtil"%>

<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme"%>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui"%>
<%@ taglib uri="http://alloy.liferay.com/tld/aui" prefix="aui"%>
<%@ taglib uri="http://liferay.com/tld/util" prefix="liferay-util"%>

<liferay-theme:defineObjects />
<portlet:defineObjects />

<portlet:renderURL var="backAproveURL" >
	<portlet:param name="mvcPath" value="/html/validateadvanceapproval/view.jsp"></portlet:param>
</portlet:renderURL>
<portlet:resourceURL id="ajaxURL" var="ajaxURL"/>

<%
	locale = renderRequest.getLocale();
	ResourceBundle resmain = portletConfig.getResourceBundle(locale);
	
	String idSolicitarePlata 	= "";
	String status 				= "ro";
	String whereAmI 			= "validate";
	String tip_capex = "0";
	String initiativa = "0";
	String prj = "0";
	try {
		idSolicitarePlata = renderRequest.getParameter("id").toString();
	} catch (Exception e) { }
	
	List<HashMap<String,Object>> oneRow = null;
	
	if(idSolicitarePlata.equals("") || idSolicitarePlata == null) {
		String url = PortalUtil.getCurrentCompleteURL(request);
		String [] params = url.split("&");
		String [] lastParameter = params[params.length-1].split("=");
		if (lastParameter[0].equals("id")){
			idSolicitarePlata = lastParameter[1];
		}
	}
	
	if(idSolicitarePlata.equals("")) {
		status="";
	}
	
	System.out.println("idSolicitarePlata aici : " + idSolicitarePlata + " status = " + status);
	
	oneRow = AdvanceRequest.getOne(idSolicitarePlata);
	tip_capex = oneRow.get(0).get("tip_capex").toString();
	initiativa = oneRow.get(0).get("id_initiative").toString();
	prj = oneRow.get(0).get("id_new_prj").toString();
%>
<%-- Cand se doreste vizualizarea unei cereri pentru validare --%>
<portlet:actionURL name="updateStage" var="updateStageValidateURL" />

<aui:form action="<%=updateStageValidateURL%>"  method="post" name="update-stage-validate">
	<liferay-ui:error key="update_nok" message="update_nok" />
	<%--input-ul hidden este pentru a putea prelua id-ul in controller --%>
	<aui:input type="hidden" name="idToUpdate" id="idToUpdate" value="<%= idSolicitarePlata %>" />

	<aui:layout  style="margin-top:10px">
		<%@include file="../includes/fact_info.jsp" %>
	</aui:layout>
	<%--linia de butoane--%>
	<aui:fieldset>
		<aui:layout>
			<aui:column first="true">
				<aui:button name="inapoi" cssClass="btn btn-primary-red" onClick ="<%=backAproveURL.toString()%>" value="<%=resmain.getString(\"back\")%>"/>
			</aui:column>
			<aui:column last="true">
				<aui:button type="submit" id="validare_document" name="validare_document" cssClass="btn btn-primary" value="<%=resmain.getString(\"validare_document\")%>"></aui:button>
			</aui:column>
		</aui:layout>
	</aui:fieldset>
</aui:form>