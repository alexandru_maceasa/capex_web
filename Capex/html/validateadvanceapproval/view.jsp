<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme"%>

<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui"%>
<%@ taglib uri="http://alloy.liferay.com/tld/aui" prefix="aui"%>
<%@ taglib uri="http://liferay.com/tld/util" prefix="liferay-util"%>

<%@ page import="java.util.ResourceBundle"%>
<%@ page import="java.util.Locale"%>

<liferay-theme:defineObjects />
<portlet:defineObjects />

<portlet:renderURL var="validateURL" >
	<portlet:param name="mvcPath" value="/html/validateadvanceapproval/validate.jsp"></portlet:param>
</portlet:renderURL>

<%
	locale = renderRequest.getLocale();
	ResourceBundle resmain = portletConfig.getResourceBundle(locale);
	// retrieve all companies from DB
	List<HashMap<String,Object>> allCompanies = DefCompaniesVo.getInstance();
%>

<aui:layout>
	<aui:column columnWidth="50" last="true" style="text-align:right">
		<%-- //TODO: add it before going live ... functionality also
		<aui:select id="societate" name="societate" label="">
			<aui:option selected="selected" value="">Societate</aui:option>
			<% for (int i = 0; i < allCompanies.size(); i++) { %>
				<% if (allCompanies.get(i).get("status").equals("A")) {%>
					<aui:option value="<%=allCompanies.get(i).get(\"id\")%>"><%=allCompanies.get(i).get("name")%></aui:option>
				<% } %>
			<% } %>
		</aui:select>
		--%>
	</aui:column>
</aui:layout>
<aui:layout>
	<liferay-ui:success key="update_ok" message="update_ok" />
	<aui:layout  style="margin-top:10px">
		<%@ include file="tabel_listing/listing.jsp" %>
	</aui:layout>
</aui:layout>