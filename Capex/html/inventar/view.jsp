<%@page import="com.profluo.ecm.model.db.DefStore"%>
<%@page import="com.profluo.ec.carrefour.htmlutils.DropDownUtils"%>
<%@page import="com.profluo.ecm.model.db.ListaInit"%>
<%@page import="com.profluo.ecm.model.db.DefNewProj"%>
<%@page import="com.profluo.ecm.model.db.InvoiceHeader"%>
<%@page import="com.profluo.ecm.controller.UserTeamIdUtils"%>
<%@page import="com.profluo.ecm.model.db.DefCompanies"%>
<%@page import="com.profluo.ecm.model.db.InventoryHeader"%>
<%@taglib uri="http://java.sun.com/portlet_2_0" 	prefix="portlet"%>
<%@taglib uri="http://liferay.com/tld/theme" 		prefix="liferay-theme"%>
<%@taglib uri="http://liferay.com/tld/ui" 			prefix="liferay-ui"%>
<%@taglib uri="http://alloy.liferay.com/tld/aui" 	prefix="aui"%>
<%@taglib uri="http://liferay.com/tld/util" 		prefix="liferay-util"%>

<%@page import="java.util.HashMap"%>
<%@page import="java.util.List"%>

<liferay-theme:defineObjects />
<portlet:defineObjects />

<%
	List<HashMap<String,Object>> allCompanies = DefCompanies.getAll();
	List<HashMap<String,Object>> allNewProjects = DefNewProj.getAll();
	List<HashMap<String,Object>> allInitiatives = ListaInit.getAll();
	List<HashMap<String,Object>> allStores = DefStore.getFullList();
%>

<portlet:resourceURL id="ajaxURL" var="ajaxURL"/>

<portlet:resourceURL id="exportAction" var="exportURL">
	<portlet:param name="action" value="exportExcel"/>
</portlet:resourceURL>

<portlet:resourceURL id="exportRaport" var="exportRaportURL">
	<portlet:param name="action" value="exportRaport"/>
</portlet:resourceURL>


<%if (user.getUserId() == UserTeamIdUtils.USER_TEST) {%>
<aui:layout>
	<aui:form id="exportForm" name="exportForm" action="<%=exportURL.toString()%>">
		<aui:column>
			<label for="<portlet:namespace />company" style="padding-top: 5px">Societatea</label>
		</aui:column>
		<aui:column>
			<aui:select id="company" name="company" label="">
				<aui:option selected="selected" value="0"/>
				<% for (int i = 0 ; i < allCompanies.size(); i++) { %>
					<aui:option value="<%=i+1%>"> <%=allCompanies.get(i).get("name").toString()%> </aui:option>
				<% } %>
			</aui:select>
		</aui:column>
		<aui:column style="display:none">
			<aui:input type="text" name="export_type" id="export_type"/>
		</aui:column>
		<aui:column>
			<a class="btn btn-primary" onclick="exportDocumente('<portlet:namespace />')"> Export Documente </a>
		</aui:column>
	</aui:form>
</aui:layout>
<% } %>

<aui:layout>
	<aui:form id="exportCeva" name="exportCeva" action="">
	<aui:column columnWidth="100">
		<aui:column columnWidth="7">
			<label for="<portlet:namespace />societate" style="padding-top: 5px">Societatea</label>
		</aui:column>
		<aui:column>
			<aui:select id="societate" name="societate" label="" style="width:200px;">
				<aui:option selected="selected" value="0"> ----------- </aui:option>
				<% for (int i = 0 ; i < allCompanies.size(); i++) { %>
					<aui:option value="<%=i+1%>"> <%=allCompanies.get(i).get("name").toString()%> </aui:option>
				<% } %>
			</aui:select>
		</aui:column>
		<aui:column  columnWidth="7">
			<label for="<portlet:namespace />initiative" style="padding-top: 5px;">Initiativa</label>
		</aui:column>
		<aui:column>
			<aui:select id="initiative" name="initiative" label="" style="width:150px;">
				<aui:option selected="selected" value="-1"> ----------- </aui:option>
					<aui:option value="0">Capex Diverse</aui:option>
					<aui:option value="1">Initiative</aui:option>
					<aui:option value="2">Proiecte noi</aui:option>
			</aui:select>
		</aui:column>
		<aui:column columnWidth="7">
			<label id="initiative_label" for="<portlet:namespace />lista_initiative" style="padding-top: 5px; display:none">Initiative</label>
		</aui:column>
		<aui:column>
			<aui:select name="lista_initiative" label="" id="lista_initiative" style="display:none; width:150px;">
				<aui:option selected="selected" value=""><liferay-ui:message key="initiative"/></aui:option>
				<% for (int i = 0;i < allInitiatives.size(); i++) { %>
					<% if (allInitiatives.get(i).get("status").equals("A")){ %>
						<aui:option value="<%=allInitiatives.get(i).get(\"id\") %>">
						<%=allInitiatives.get(i).get("code") %> - <%=allInitiatives.get(i).get("name") %>(<%=allInitiatives.get(i).get("year")%>)
						</aui:option>
					<% } %>
				<% } %>
			</aui:select>
		</aui:column>
		<aui:column columnWidth="7">
			<label id="projects_label" for="<portlet:namespace />project" style="padding-top: 5px; display:none">Proiecte noi</label>
		</aui:column>
		<aui:column>
			<aui:select name="project" label="" id="project" style="display:none; width:150px;">
			<aui:option selected="selected" value="-1">Proiecte noi</aui:option>
				<% for (int i = 0;i < allNewProjects.size(); i++) { %>
					<% if (allNewProjects.get(i).get("status").equals("A")){ %>
						<aui:option value="<%=allNewProjects.get(i).get(\"id\") %>">
						<%=allNewProjects.get(i).get("name") %>
						</aui:option>
					<% } %>
				<% } %>
			</aui:select>
		</aui:column>
		<aui:column columnWidth="7">
			<label for="<portlet:namespace />store" style="padding-top: 5px;">Magazin</label>
		</aui:column>
		<aui:column>
			<aui:select id="store" name="store" label="" style="width:150px;">
				<aui:option selected="selected" value="0"> Magazin </aui:option>
				<% for (int i = 0 ; i < allStores.size(); i++) { %>
					<aui:option value="<%=allStores.get(i).get(\"id\").toString()%>"> <%=allStores.get(i).get("store_id").toString()%> - <%=allStores.get(i).get("store_name").toString()%> </aui:option>
				<% } %>
			</aui:select>
		</aui:column>
	</aui:column>
	<aui:layout>
	<aui:column columnWidth="15" first="true">
		<label style="padding-top:5px" for="start_date">Data contabila inceput: </label>
	</aui:column>
	<aui:column columnWidth="10"  style="text-align:right">
		<aui:input label="" name="start_date_an" id="start_date_an" type="text" value="" style="width:90%" placeholder="An contabil">
		</aui:input>
	</aui:column>
	<aui:column columnWidth="15"  style="text-align:right">
		<aui:select id="start_date_luna" name="start_date_luna" label="" style="width:100%" required="true">
				<aui:option selected="selected" value="0">Luna contabil</aui:option>
				<aui:option value="1">Ianuarie</aui:option>
				<aui:option value="2">Februarie</aui:option>
				<aui:option value="3">Martie</aui:option>
				<aui:option value="4">Aprilie</aui:option>
				<aui:option value="5">Mai</aui:option>
				<aui:option value="6">Iunie</aui:option>
				<aui:option value="7">Iulie</aui:option>
				<aui:option value="8">August</aui:option>
				<aui:option value="9">Septembrie</aui:option>
				<aui:option value="10">Octombrie</aui:option>
				<aui:option value="11">Noiembrie</aui:option>
				<aui:option value="12">Decembrie</aui:option>
			</aui:select>
	</aui:column>
</aui:layout>
<aui:layout>
	<aui:column columnWidth="15" first="true">
		<label style="padding-top:5px" for="end_date">Data contabila sfarsit: </label>
	</aui:column>
	<aui:column columnWidth="10"  style="text-align:right">
		<aui:input label="" name="end_date_an" id="end_date_an" type="text" value="" style="width:90%" placeholder="An contabil">
		</aui:input>
	</aui:column>
	<aui:column columnWidth="15"  style="text-align:right">
		<aui:select id="end_date_luna" name="end_date_luna" label="" style="width:100%" required="true">
				<aui:option selected="selected" value="0">Luna contabil</aui:option>
				<aui:option value="1">Ianuarie</aui:option>
				<aui:option value="2">Februarie</aui:option>
				<aui:option value="3">Martie</aui:option>
				<aui:option value="4">Aprilie</aui:option>
				<aui:option value="5">Mai</aui:option>
				<aui:option value="6">Iunie</aui:option>
				<aui:option value="7">Iulie</aui:option>
				<aui:option value="8">August</aui:option>
				<aui:option value="9">Septembrie</aui:option>
				<aui:option value="10">Octombrie</aui:option>
				<aui:option value="11">Noiembrie</aui:option>
				<aui:option value="12">Decembrie</aui:option>
			</aui:select>
	</aui:column>
</aui:layout>
	<aui:column columnWidth="100">
		<aui:column>
			<a class="btn btn-primary" onclick="filterListing('<portlet:namespace />')"> Aplica Filtre </a>
		</aui:column>
		<aui:column>
			<a class="btn btn-primary" onclick="exportRaport('<portlet:namespace />')"> Export Raport </a>
		</aui:column>
	</aui:column>
	</aui:form>
</aui:layout>
<br/>
<aui:layout>
	<aui:column first="true" columnWidth="100" last="true">
		<div id="inventar"></div>
	</aui:column>
</aui:layout>

<%
	int start = 0; 
	int count = 10;
	List<HashMap<String,Object>> allInvLines = InventoryHeader.getAllInfoWithLimit(start, count, "", "", "", "", 0, 0, 0 ,0, -1);
	int total = InventoryHeader.getInventoryTotal("", "", "", "");
%>

<script type="text/javascript">
var start = <%=start%>
var count = <%=count%>
var total = <%=total%>
var societate = -1;
var tipInitiativa = -1;
var initiativa = '';
var proiectNou = '';
var magazin = 0;
var dataTableIV;
var from_year = 0;
var to_year = 0;
var from_month = 0;
var to_month = 0;
</script>

<%-- HTML for PAGINATION --%>
<%@ include file="/html/includes/pagination.jsp" %>

<script type="text/javascript">

$(document).ready(function(e){
	$('#<portlet:namespace/>store').chosen();
});

$('#<portlet:namespace/>initiative').change(function(e){
	var initiativa = $('#<portlet:namespace/>initiative').val();
	if(initiativa == 1 ){
		$('#projects_label').hide();
		$('#<portlet:namespace/>project').hide();
		$('#<portlet:namespace/>project_chosen').hide();
		$('#initiative_label').show();
		$('#<portlet:namespace/>lista_initiative').show();
		$('#<portlet:namespace/>lista_initiative').chosen();
	} else if (initiativa == 2 ) {
		$('#initiative_label').hide();
		$('#<portlet:namespace/>lista_initiative_chosen').hide();
		$('#<portlet:namespace/>lista_initiative').hide();
		$('#projects_label').show();
		$('#<portlet:namespace/>project').show();
		$('#<portlet:namespace/>project').chosen();
	} else {
		$('#initiative_label').hide();
		$('#<portlet:namespace/>lista_initiative_chosen').hide();
		$('#<portlet:namespace/>lista_initiative').hide();
		$('#projects_label').hide();
		$('#<portlet:namespace/>project_chosen').hide();
		$('#<portlet:namespace/>project').hide();
	}
});

function filterListing(portletId){
	societate = $('#<portlet:namespace/>societate').val();
	tipInitiativa = $('#<portlet:namespace/>initiative').val();
	initiativa = '';
	proiectNou = '';
	if(tipInitiativa == 1) {
		initiativa = $('#<portlet:namespace/>lista_initiative').val();
	} else if(tipInitiativa == 2) {
		proiectNou = $('#<portlet:namespace/>project').val();
	}
	
	magazin = $('#<portlet:namespace/>store').val();
	
	from_year = $('#<portlet:namespace/>start_date_an').val();
	to_year = $('#<portlet:namespace/>end_date_an').val();
	from_month = $('#<portlet:namespace/>start_date_luna').val();
	to_month = $('#<portlet:namespace/>end_date_luna').val();
	
	if(from_year == '') {
		from_year = 0;
	}
	if(to_year == '') {
		to_year = 0;
	}
	if(from_month == '') {
		from_month = 0;
	}
	if(to_month == '') {
		to_month = 0;
	}
	
	var action = "filter";
	makeAjaxCall(action);
}

function exportRaport(portletId){
	societate = $('#<portlet:namespace/>societate').val();
	tipInitiativa = $('#<portlet:namespace/>initiative').val();
	initiativa = '';
	proiectNou = '';
	if(tipInitiativa == 1) {
		initiativa = $('#<portlet:namespace/>lista_initiative').val();
	} else if(tipInitiativa == 2) {
		proiectNou = $('#<portlet:namespace/>project').val();
	}
	
	magazin = $('#<portlet:namespace/>store').val();
	
	from_year = $('#<portlet:namespace/>start_date_an').val();
	to_year = $('#<portlet:namespace/>end_date_an').val();
	from_month = $('#<portlet:namespace/>start_date_luna').val();
	to_month = $('#<portlet:namespace/>end_date_luna').val();
	
	if(from_year == '') {
		from_year = 0;
	}
	if(to_year == '') {
		to_year = 0;
	}
	if(from_month == '') {
		from_month = 0;
	}
	if(to_month == '') {
		to_month = 0;
	}

	$('#<portlet:namespace/>company_id').val(societate);
	$('#<portlet:namespace/>tip_capex').val(tipInitiativa);
	$('#<portlet:namespace/>initiativa').val(initiativa);
	$('#<portlet:namespace/>proiectNou').val(proiectNou);
	$('#<portlet:namespace/>id_magazin').val(magazin);
	$('#<portlet:namespace/>from_year').val(from_year);
	$('#<portlet:namespace/>to_year').val(to_year);
	$('#<portlet:namespace/>from_month').val(from_month);
	$('#<portlet:namespace/>to_month').val(to_month);
	$('#<portlet:namespace/>exportFormRaport').submit();
	
}

function exportDocumente(portletId){
	var company = $('#' + portletId + 'company').val();
	if ( !company || company == 0){
		alert("Va rugam sa selectati societatea.");
		return;
	}
	$('#'+portletId+'export_type').val("exportDocumente");
	$('#'+portletId+'exportForm').submit();
}

YUI({ lang: 'ro' }).use('aui-datatable','aui-datatype','datatable-sort', 'aui-datepicker', 'aui-base', 'liferay-portlet-url', 'aui-node',
		function(Y){
		var remoteData = [
	      			<% for (int i = 0; i < allInvLines.size(); i++) { %>
	      				{
	      					societate: 		'<%=allInvLines.get(i).get("societate")%>',
	      					codInitiativa: 	'<%=allInvLines.get(i).get("codInitiativa")%>',
	      					initiativa: 	'<%=allInvLines.get(i).get("initiativa")%>',
	      					codMagazin:		'<%=allInvLines.get(i).get("codMagazin")%>',
	      					magazin:		'<%=allInvLines.get(i).get("magazin")%>',
	      					codLot:			'<%=allInvLines.get(i).get("codLot")%>',
	      					lot:			'<%=allInvLines.get(i).get("lot")%>',
	      					codProdus:		'<%=allInvLines.get(i).get("codProdus")%>',
	      					denumireProdus:	'<%=allInvLines.get(i).get("denumireProdus")%>',
	      					denumireSubprodus: '',
	      					tipInreg:		'<%=allInvLines.get(i).get("tipInreg")%>',
	      					tip_final:		'<%=allInvLines.get(i).get("tip_final")%>',
	      					gestiune:		'<%=allInvLines.get(i).get("gestiune")%>',
	      					nrInventar: 	'<%=allInvLines.get(i).get("nrInventar")%>',
	      					valoareRON: 	'<%=allInvLines.get(i).get("valoareRON")%>',
	      					valoareCURR: 	'<%=allInvLines.get(i).get("valoareCURR")%>',
	      					grupaIFRS: 		'<%=allInvLines.get(i).get("grupaIFRS")%>',
	      					durataIFRS: 	'<%=allInvLines.get(i).get("durataIFRS")%>',
	      					grupaIAS: 		'<%=allInvLines.get(i).get("grupaIAS")%>',
	      					durataIAS: 		'<%=allInvLines.get(i).get("durataIAS")%>',
	      					codFurnizor: 	'<%=allInvLines.get(i).get("codFurnizor")%>',
	      					furnizor: 		'<%=allInvLines.get(i).get("furnizor")%>',
	      					luna: 			'<%=allInvLines.get(i).get("luna")%>',
	      					dataPIF: 		'<%=allInvLines.get(i).get("dataPIF")%>',
	      					actMF: 			'<%=allInvLines.get(i).get("actMF")%>',
	      					detinator: 		'<%=allInvLines.get(i).get("detinator")%>',
	      					an_contabil:	'<%=allInvLines.get(i).get("an_contabil")%>',
	      					luna_contabila:	'<%=allInvLines.get(i).get("luna_contabila")%>',
	      					denumire:		'<%=allInvLines.get(i).get("denumire")%>',
	      					dpi: 			'<%=allInvLines.get(i).get("dpi")%>',
	      					nrContract: 	'<%=allInvLines.get(i).get("nrContract")%>',
	      					receptie: 		'<%=allInvLines.get(i).get("receptie")%>',
	      					cont: 			'<%=allInvLines.get(i).get("cont")%>',
	      					docNo: 			'<%=allInvLines.get(i).get("docNo")%>',
	      					dataDoc:		'<%=allInvLines.get(i).get("dataDoc")%>',
	      					keuro:			'<%=allInvLines.get(i).get("keuro")%>',
	      					docType:		'<%=allInvLines.get(i).get("docType")%>'
	      				}
	      				<% if (i != (allInvLines.size() - 1)) { out.print(","); } %>
	      			<% } %>
	      		];
		var nestedColumns = [		
							{key: 'societate', 	label:'Societate'},
							{key: 'codInitiativa', 	label:'Cod Initiativa'},
							{key: 'initiativa', 	label:'Initiativa'},
							{key:'codMagazin', 	label:'Cod magazin'},
							{key: 'magazin', 	label:'Denumire magazin'},
							{key: 'codLot', 	label:'Cod lot'},
							{key: 'lot', 		label:'Lot'},
							{key: 'codProdus', 	label:'Cod produs'},
							{key: 'denumireProdus', label:'Denumire produs'},
							{key: 'denumireSubprodus', label:'Denumire subprodus/articol'},
							{key: 'tipInreg', 	label:'Tip obiect'},
							{key: 'tip_final', 	label:'Tip obiect final'},
							{key: 'gestiune', 	label:'Clasificare'},
		         			{key: 'nrInventar', label: 'Numar inventar'},
		         			{key: 'valoareRON', label:'Valoare operatie - RON'},
		         			{key: 'valoareCURR', label:'Valoare operatie - EURO'},
		         			{key: 'grupaIFRS', 	label:'Grupa IFRS'},
		         			{key: 'durataIFRS', label:'Durata IFRS'},
		         			{key: 'grupaIAS', 	label:'Grupa IAS'},
		         			{key: 'durataIAS', label:'Durata IAS'},
		         			{key: 'codFurnizor', label:'Cod furnizor'},
		         			{key: 'furnizor', 	label:'Furnizor'},
		         			{key: 'luna', 		label:'Luna'},
		         			{key: 'docNo', 		label: 'Nr document'},
		         			{key:'dataDoc',		label:'Data document'},
		         			{key: 'dataPIF', 	label:'Data PIF'},
		         			{key: 'actMF', 	label:'Tip operatie'},
		         			{key: 'detinator', 	label:'Active/ Property'},
 		         			{key: 'an_contabil',		label:'An'},
 		         			{key: 'luna_contabila', 	label:'Luna'},
 		         			{key: 'denumire', 	label:'Denumire'},
		         			{key: 'dpi', 	label:'DPI'},
		         			{key: 'nrContract', 	label:'Nr. Contract'},
		         			{key: 'receptie', 	label:'Receptie'},
		         			{key: 'cont', 	label:'Cont contabil'},
		         			{key: 'keuro', label:'KEURO'},
		         	       	{key:'docType', 	label:'Categorie'}
		         		];
		dataTableIV = new Y.DataTable({
			columns:nestedColumns,
			data:remoteData,
			scrollable: "x",
	        width: "100%",
			recordType:['nrInventar', 'dataPIF', 'denumire', 'docNo', 'dataDoc', 'codSocietate', 'codMagazin', 'raion', 'docType'],
			editEvent:'click'
		}).render('#inventar');
		
		// PAGINATION
		// set status of navigation buttons on page load
		setNavigationButtonsState('<portlet:namespace />');
		$('.navigation').click(function(e) {
			var elementT 	= e.target;
			var elementId 	= elementT.id;
			var action 		= "";
			
			// determine which button was pressed and set the value of start accordingly
			if (elementId.indexOf("first")> -1)	{ action = "first"; start = 0; }
			if (elementId.indexOf("prev") > -1) 	{ action = "prev"; if (start - count <= 0) {start = 0} else { start -= count; } }
			if (elementId.indexOf("next") > -1) 	{ action = "next"; start = parseInt(start) + parseInt(count); }
			if (elementId.indexOf("last") > -1) 	{ action = "last"; start = parseInt(parseInt(total) / parseInt(count)) * count; if (start == total) { start -= count; } }
			
			makeAjaxCall(action);
		});
		
});
function makeAjaxCall(action) {
	$.ajax({
		type: "POST",
		url: "<%=ajaxURL%>",
		data: 	"<portlet:namespace />action=" + action + 
				"&<portlet:namespace />start=" + start + 
				"&<portlet:namespace />count=" + count +
				"&<portlet:namespace />total=" + total +
				"&<portlet:namespace />societate=" + societate +
				"&<portlet:namespace />tip_capex=" + tipInitiativa +
				"&<portlet:namespace />initiativa=" + initiativa +
				"&<portlet:namespace />proiectNou=" + proiectNou +
				"&<portlet:namespace />magazin=" + magazin +
				"&<portlet:namespace />from_year=" + from_year +
				"&<portlet:namespace />to_year=" + to_year +
				"&<portlet:namespace />from_month=" + from_month +
				"&<portlet:namespace />to_month=" + to_month,
		success: function(msg) {
			// get table data
			if (msg != ""){
				var jsonEntries = jQuery.parseJSON(msg);
				total = jsonEntries.total;
				dataTableIV.set('data', eval(jsonEntries.values));
			} else {
				dataTableIV.set('data', []);
			}
			// set status of navigation buttons on page load
			setNavigationButtonsState('<portlet:namespace />');
		}
	});
}
</script>
<aui:form id="exportFormRaport" name="exportFormRaport" style="dysplay:none"  action="<%=exportRaportURL.toString()%>">
	<aui:input type="hidden" id="company_id" name="company_id"/>
	<aui:input type="hidden" id="tip_capex" name="tip_capex"/>
	<aui:input type="hidden" id="initiativa" name="initiativa"/>
	<aui:input type="hidden" id="proiectNou" name="proiectNou"/>
	<aui:input type="hidden" id="id_magazin" name="id_magazin"/>
	<aui:input type="hidden" id="from_year" name="from_year"/>
	<aui:input type="hidden" id="to_year" name="to_year"/>
	<aui:input type="hidden" id="from_month" name="from_month"/>
	<aui:input type="hidden" id="to_month" name="to_month"/>
</aui:form>

