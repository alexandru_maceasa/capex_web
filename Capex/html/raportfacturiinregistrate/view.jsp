<%@page import="com.profluo.ecm.model.db.InvoiceHeader"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.List"%>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui"%>
<%@ taglib uri="http://alloy.liferay.com/tld/aui" prefix="aui"%>
<%@ taglib uri="http://liferay.com/tld/util" prefix="liferay-util"%>

 
<liferay-theme:defineObjects />
<portlet:defineObjects />

<portlet:resourceURL id="ajaxURL" var="ajaxURL"/>


<portlet:resourceURL id="exportAction" var="exportURL">
	<portlet:param name="action" value="exportExcel"/>
</portlet:resourceURL>

<portlet:resourceURL id="exportAction" var="exportUsersURL">
	<portlet:param name="action" value="exportUsersExcel"/>
</portlet:resourceURL>

<%
		int count = 15;
		int start = 0;
		int total = 0;
		
		List<HashMap<String,Object>> facturiInregistrate = InvoiceHeader.getFacturiInregistrate(start, count, "", "", "", "", "", "", "","", "", "", "", "");
		total = InvoiceHeader.getFacturiInregistrateCount( "", "", "","","", "", "","", "", "", "", "");
%>


	

<aui:layout>
		
<aui:column columnWidth="10" first="true">
		<label style="padding-top:5px" for="start_date">Data contabila inceput: </label>
	</aui:column>
	<aui:column columnWidth="10"  style="text-align:right">
		<aui:input label="" name="start_date_an" id="start_date_an" type="text" value="" style="width:90%" placeholder="An contabil">
			 <aui:validator name="required" errorMessage="Campul este obligatoriu." />
		</aui:input>
	</aui:column>
	<aui:column columnWidth="10"  style="text-align:right">
		<aui:select id="start_date_luna" name="start_date_luna" label="" style="width:95%" required="true">
				<aui:option selected="selected" value="0">Luna contabil</aui:option>
				<aui:option value="1">Ianuarie</aui:option>
				<aui:option value="2">Februarie</aui:option>
				<aui:option value="3">Martie</aui:option>
				<aui:option value="4">Aprilie</aui:option>
				<aui:option value="5">Mai</aui:option>
				<aui:option value="6">Iunie</aui:option>
				<aui:option value="7">Iulie</aui:option>
				<aui:option value="8">August</aui:option>
				<aui:option value="9">Septembrie</aui:option>
				<aui:option value="10">Octombrie</aui:option>
				<aui:option value="11">Noiembrie</aui:option>
				<aui:option value="12">Decembrie</aui:option>
			</aui:select>
	</aui:column>
	</aui:layout>
	
	<aui:layout>
	<aui:column columnWidth="10" first="true">
		<label style="padding-top:5px" for="end_date">Data contabila sfarsit: </label>
	</aui:column>
	<aui:column columnWidth="10"  style="text-align:right">
		<aui:input label="" name="end_date_an" id="end_date_an" type="text" value="" style="width:90%" placeholder="An contabil">
			 <aui:validator name="required" errorMessage="Campul este obligatoriu." />
		</aui:input>
	</aui:column>
	<aui:column columnWidth="10"  style="text-align:right">
		<aui:select id="end_date_luna" name="end_date_luna" label="" style="width:95%" required="true">
				<aui:option selected="selected" value="0">Luna contabil</aui:option>
				<aui:option value="1">Ianuarie</aui:option>
				<aui:option value="2">Februarie</aui:option>
				<aui:option value="3">Martie</aui:option>
				<aui:option value="4">Aprilie</aui:option>
				<aui:option value="5">Mai</aui:option>
				<aui:option value="6">Iunie</aui:option>
				<aui:option value="7">Iulie</aui:option>
				<aui:option value="8">August</aui:option>
				<aui:option value="9">Septembrie</aui:option>
				<aui:option value="10">Octombrie</aui:option>
				<aui:option value="11">Noiembrie</aui:option>
				<aui:option value="12">Decembrie</aui:option>
			</aui:select>
	</aui:column>
	</aui:layout>
	
	<aui:layout>

		<aui:column>
			<a class="btn btn-primary" id="exportExcelBtn" style="margin-bottom: 20% !important"> Export Excel </a>
		</aui:column>
</aui:layout>
	
	<aui:layout>	
	<aui:form method="post" action="<%=exportURL.toString() %>" name="exportExcel" id="exportExcel">
		<aui:column first="true" columnWidth="100" last="true">
			<div id="raport_facturi_inregistrate"></div>
				<aui:input type="hidden" id="factNo" name="factNo"/>
				<aui:input type="hidden" id="societate" name="societate"/>
				<aui:input type="hidden" id="furnizor" name="furnizor"/>
				<aui:input type="hidden" id="dataFactura" name="dataFactura"/>
				<aui:input type="hidden" id="Magazin" name="Magazin"/>
				<aui:input type="hidden" id="valoareCuTVA" name="valoareCuTVA"/>
				<aui:input type="hidden" id="codInitiativa" name="codInitiativa"/>
				<aui:input type="hidden" id="Initiativa" name="Initiativa"/>
				<aui:input type="hidden" id="start_date_an_param" name="start_date_an_param"/>
				<aui:input type="hidden" id="start_date_luna_param" name="start_date_luna_param"/>
				<aui:input type="hidden" id="end_date_an_param" name="end_date_an_param"/>
				<aui:input type="hidden" id="end_date_luna_param" name="end_date_luna_param"/>
		</aui:column>
		
	</aui:form>
</aui:layout>
	

<%-- HTML for PAGINATION --%>
<%@ include file="/html/includes/pagination.jsp" %>

<script type="text/javascript">;

var start = "<%=start%>";
var count = "<%=count%>";
var total = "<%=total%>";
var factNo = "";
var supplier = "";
var societate = "";
var Magazin = "";
var dataFactura = "";
var codInitiativa = "";
var Initiativa = "";
var valoareCuTVA = "";
var dataTable;

var anInceput = 0;
var anSfarsit = 0;

var lunaInceput = 0;
var lunaSfarsit = 0;

YUI({lang: 'ro'}).use( 'aui-datatable', 'aui-datatype', 'datatable-sort', 'aui-datepicker',
		function(Y) {
	debugger;
	var remoteData = [
	       		   <% for (int i = 0; i < facturiInregistrate.size(); i++) { %>
	       					{
	       						name: '<%=facturiInregistrate.get(i).get("name")%>',
	       						Magazin: '<%=facturiInregistrate.get(i).get("Magazin")%>',
	       						reg_date: '<%=facturiInregistrate.get(i).get("reg_date")%>',
	       						Furnizor: '<%=facturiInregistrate.get(i).get("Furnizor")%>',
	       						inv_number: '<%=facturiInregistrate.get(i).get("inv_number")%>',
	       						inv_date: '<%=facturiInregistrate.get(i).get("inv_date")%>',
	       						lunaContabila: '<%=facturiInregistrate.get(i).get("lunaContabila")%>',
	       						anContabil: '<%=facturiInregistrate.get(i).get("anContabil")%>',
	       						Numar_Inventar: '<%=facturiInregistrate.get(i).get("Numar_Inventar")%>',
	       						Denumire_Articol: '<%=facturiInregistrate.get(i).get("Denumire_Articol")%>',
	       						total_no_vat_ron: '<%=facturiInregistrate.get(i).get("total_no_vat_ron")%>',
	       						total_with_vat_ron: '<%=facturiInregistrate.get(i).get("total_with_vat_ron")%>',
	       						warranty: '<%=facturiInregistrate.get(i).get("warranty")%>',
	       						Proiect_Nou: '<%=facturiInregistrate.get(i).get("Proiect_Nou")%>',
	       						Id_IAS: '<%=facturiInregistrate.get(i).get("Id_IAS")%>',
	       						Cod_Ifrs: '<%=facturiInregistrate.get(i).get("Cod_Ifrs")%>',
	       						Durata_Amortizare_IAS: '<%=facturiInregistrate.get(i).get("Durata_Amortizare_IAS")%>',
	       						Durata_Amortizare_IFRS: '<%=facturiInregistrate.get(i).get("Durata_Amortizare_IFRS")%>',
	       						Tip_Inregistrare: '<%=facturiInregistrate.get(i).get("Tip_Inregistrare")%>',
	       						Actiune_MF: '<%=facturiInregistrate.get(i).get("Actiune_MF")%>',
	       						Gestiune: '<%=facturiInregistrate.get(i).get("Gestiune")%>',
	       						Cont_Imob_Generale: '<%=facturiInregistrate.get(i).get("Cont_Imob_Generale")%>',   							       						
	       						Cod_Initiativa: '<%=facturiInregistrate.get(i).get("Cod_Initiativa")%>',
	       						Initiativa: '<%=facturiInregistrate.get(i).get("Initiativa")%>'
	       					}
	       				<% if (i != facturiInregistrate.size() - 1) {  out.print(",");} %>
	       			<% } %>
	       		];
			
			// COLUMN INFO and ATTRIBUTES
			var nestedCols = [
			    { key: 'id', className: 'hiddencol'},
			    { key: 'name', label: '<input type="text" id="filtru_societate" placeholder= "Societate" style="Width:60px;margin:10;"/>'},
			    { key: 'Magazin', label: '<input type="text" id="filtru_Magazin" placeholder= "Magazin" style="Width:80px;margin:10;"/>'},			    
			    { key: 'reg_date', label: 'Data Inregistrare <style="Width:60px;"/>'},
			    { key: 'Furnizor', label: '<input type="text" id="filtru_furnizor" placeholder= "Furnizor" style="width:80%;margin:0;"/>'},
			    { key: 'inv_number', label: '<input type="text" id="filtru_Numar_Factura" placeholder= "Nr. Factura" style="width:70px;margin:0;"/>'},
			    { key: 'inv_date', label: '<input type="text" id="filtru_dataFactura" placeholder= "Data Factura" style="width:90px;margin:0;"/>'},
			    { key: 'lunaContabila', label: 'Luna Contabila'},
			    { key: 'anContabil', label: 'An Contabil'},
			    { key: 'Numar_Inventar', label: 'Numar Inventar'},
			    { key: 'Denumire_Articol', label: 'Denumire Articol'},
			    { key: 'total_no_vat_ron', label: 'Valoare fara TVA'},
			    { key: 'total_with_vat_ron', label: '<input type="text" id="filtru_valoareCuTVA" placeholder="Valoare cu TVA" style="width:90px;margin:0;"/>'},
			    { key: 'warranty', label: 'Garantie'},
			    { key: 'Id_IAS', label: 'Id IAS'},
			    { key: 'Cod_Ifrs', label: 'Cod Ifrs'},
			    { key: 'Durata_Amortizare_IAS', label: 'Durata Amortizare IAS'},
			    { key: 'Durata_Amortizare_IFRS', label: 'Durata Amortizare IFRS'},
			    { key: 'Tip_Inregistrare', label: 'Tip inregistrare'},
			    { key: 'Actiune_MF', label: 'Actiune MF'},
			    { key: 'Gestiune', label: 'Gestiune'},
			    { key: 'Cont_Imob_Generale', label: 'Cont Imob. Generale'}, 
			    { key: 'Cod_Initiativa', label: '<input type="text" id="filtru_codInitiativa" placeholder= "Cod Initiativa" style="width:80px;margin:0;"/>'},
			    { key: 'Initiativa', label: '<input type="text" id="filtru_Initiativa" placeholder="Initiativa" style="width:90px;margin:0;"/>'}
			];
			

			
			// TABLE INIT
			dataTable = new Y.DataTable({
			    columns: nestedCols,
			    data: remoteData,	
			    scrollable: 'x',
			    width: '100%',		    
			    recordType: ['id','name','id_store','reg_date','id_supplier','inv_number','inv_date','denumire_articol','total_no_vat_ron',
			                'total_with_vat_ron','warranty','id_ias', 'id_ifrs','durata_ias','durata_ifrs','tip_inregistrare','actiune','gestiune',
			                'cont','cont_analitic','tip_obiect','stare','cod_initiativa','initiativa']
			}).render('#raport_facturi_inregistrate');
			
		
	// PAGINATION
		// set status of navigation buttons on page load
		setNavigationButtonsState('<portlet:namespace />');
		$('.navigation').click(function(e) {
			var elementT 	= e.target;
			var elementId 	= elementT.id;
			var action 		= "";
			debugger;
			// determine which button was pressed and set the value of start accordingly
			if (elementId.indexOf("first") > -1)	{ action = "first"; start = 0; }
			if (elementId.indexOf("prev") > -1) 	{ action = "prev"; if (start - count <= 0) {start = 0} else { start -= count; } }
			if (elementId.indexOf("next") > -1) 	{ action = "next"; start = parseInt(start) + parseInt(count); }
			if (elementId.indexOf("last") > -1) 	{ action = "last"; start = parseInt(parseInt(total) / parseInt(count)) * count; if (start == total) { start -= count; } }
			
			makeAjaxCall(action, factNo, supplier, societate, dataFactura, codInitiativa, Initiativa, valoareCuTVA, Magazin);
		});
		
		//filtru dupa numarul de factura
  		$('#filtru_Numar_Factura').change(function(e){
			var action 		= "filter";
			factNo = $('#filtru_Numar_Factura').val();
			supplier = $('#filtru_furnizor').val(); 
			societate = $('#filtru_societate').val();
			dataFactura = $('#filtru_dataFactura').val();
			codInitiativa = $('#filtru_codInitiativa').val();
			Initiativa = $('#filtru_Initiativa').val();
			valoareCuTVA = $('#filtru_valoareCuTVA').val();
			Magazin = $('#filtru_Magazin').val();
			start = 0;
			makeAjaxCall(action, factNo, supplier, societate, dataFactura, codInitiativa, Initiativa, valoareCuTVA, Magazin);
  		});
  		
  		//filtru dupa numarul de furnizor
  		$('#filtru_furnizor').change(function(e){
			var action 		= "filter";
			factNo = $('#filtru_Numar_Factura').val();
			supplier = $('#filtru_furnizor').val(); 
			societate = $('#filtru_societate').val();
			dataFactura = $('#filtru_dataFactura').val();
			codInitiativa = $('#filtru_codInitiativa').val();
			Initiativa = $('#filtru_Initiativa').val();
			valoareCuTVA = $('#filtru_valoareCuTVA').val();
			Magazin = $('#filtru_Magazin').val();
			start = 0;
			makeAjaxCall(action, factNo, supplier, societate, dataFactura, codInitiativa, Initiativa, valoareCuTVA, Magazin);
			
  		});
  		
  		//filtru dupa societate
  		$('#filtru_societate').change(function(e){
			var action 		= "filter";
			factNo = $('#filtru_Numar_Factura').val();
			supplier = $('#filtru_furnizor').val(); 
			societate = $('#filtru_societate').val();
			dataFactura = $('#filtru_dataFactura').val();
			codInitiativa = $('#filtru_codInitiativa').val();
			Initiativa = $('#filtru_Initiativa').val();
			valoareCuTVA = $('#filtru_valoareCuTVA').val();
			Magazin = $('#filtru_Magazin').val();
			start = 0;
			makeAjaxCall(action, factNo, supplier, societate, dataFactura, codInitiativa, Initiativa, valoareCuTVA, Magazin);
			
  		});
  		
  		//filtru dupa dataFactura
  		$('#filtru_dataFactura').change(function(e){
			var action 		= "filter";
			factNo = $('#filtru_Numar_Factura').val();
			supplier = $('#filtru_furnizor').val(); 
			societate = $('#filtru_societate').val();
			dataFactura = $('#filtru_dataFactura').val();
			codInitiativa = $('#filtru_codInitiativa').val();
			Initiativa = $('#filtru_Initiativa').val();
			valoareCuTVA = $('#filtru_valoareCuTVA').val();
			Magazin = $('#filtru_Magazin').val();
			start = 0;
			makeAjaxCall(action, factNo, supplier, societate, dataFactura, codInitiativa, Initiativa, valoareCuTVA, Magazin);
			
  		});
  		
  		//filtru dupa codInitiativa
  		$('#filtru_codInitiativa').change(function(e){
			var action 		= "filter";
			factNo = $('#filtru_Numar_Factura').val();
			supplier = $('#filtru_furnizor').val(); 
			societate = $('#filtru_societate').val();
			dataFactura = $('#filtru_dataFactura').val();
			codInitiativa = $('#filtru_codInitiativa').val();
			Initiativa = $('#filtru_Initiativa').val();
			valoareCuTVA = $('#filtru_valoareCuTVA').val();
			Magazin = $('#filtru_Magazin').val();
			start = 0;
			makeAjaxCall(action, factNo, supplier, societate, dataFactura, codInitiativa, Initiativa, valoareCuTVA, Magazin);
			
  		});
  		
  	//filtru dupa Initiativa
  		$('#filtru_Initiativa').change(function(e){
			var action 		= "filter";
			factNo = $('#filtru_Numar_Factura').val();
			supplier = $('#filtru_furnizor').val(); 
			societate = $('#filtru_societate').val();
			dataFactura = $('#filtru_dataFactura').val();
			codInitiativa = $('#filtru_codInitiativa').val();
			Initiativa = $('#filtru_Initiativa').val();
			valoareCuTVA = $('#filtru_valoareCuTVA').val();
			Magazin = $('#filtru_Magazin').val();
			start = 0;
			makeAjaxCall(action, factNo, supplier, societate, dataFactura, codInitiativa, Initiativa, valoareCuTVA, Magazin);
  		});
			
		  	//filtru dupa valoareCuTVA
	  	$('#filtru_valoareCuTVA').change(function(e){
			var action 		= "filter";
			factNo = $('#filtru_Numar_Factura').val();
			supplier = $('#filtru_furnizor').val(); 
			societate = $('#filtru_societate').val();
			dataFactura = $('#filtru_dataFactura').val();
			codInitiativa = $('#filtru_codInitiativa').val();
			Initiativa = $('#filtru_Initiativa').val();
			valoareCuTVA = $('#filtru_valoareCuTVA').val();
			Magazin = $('#filtru_Magazin').val();
			start = 0;
			makeAjaxCall(action, factNo, supplier, societate, dataFactura, codInitiativa, Initiativa, valoareCuTVA, Magazin);
			
  		});
		  	
	  	//filtru dupa Magazin
	  	$('#filtru_Magazin').change(function(e){
			var action 		= "filter";
			factNo = $('#filtru_Numar_Factura').val();
			supplier = $('#filtru_furnizor').val(); 
			societate = $('#filtru_societate').val();
			dataFactura = $('#filtru_dataFactura').val();
			codInitiativa = $('#filtru_codInitiativa').val();
			Initiativa = $('#filtru_Initiativa').val();
			valoareCuTVA = $('#filtru_valoareCuTVA').val();
			Magazin = $('#filtru_Magazin').val();
			start = 0;
			makeAjaxCall(action, factNo, supplier, societate, dataFactura, codInitiativa, Initiativa, valoareCuTVA, Magazin);
			
  		});
		  	 		
  		// export excel
  		$('#exportExcelBtn').click(function(e) {
  			
  			anInceput = $('#<portlet:namespace />start_date_an').val();
  			anSfarsit = $('#<portlet:namespace />end_date_an').val();
  			
  			lunaInceput = $('#<portlet:namespace />start_date_luna').val();
  			lunaSfarsit = $('#<portlet:namespace />end_date_luna').val();
  			
  			if ( (2015 > anInceput) ||(anInceput > 2021) || (2015 > anSfarsit) ||(anSfarsit > 2021) ){
				alert("Va rugam sa introduceti un an valid!");
				return;
			}
  			
  			if(anSfarsit < anInceput) {
  				alert("Va rugam sa introduceti o perioada valida!");
				return;
  			}
  			
  			if(anInceput == anSfarsit) {
  				if(lunaInceput > lunaSfarsit) {
  					alert("Va rugam sa introduceti o perioada valida!");
  					return;
  				} else {
  					if(lunaSfarsit - lunaInceput > 3) {
  						alert("Perioada introdusa nu poate depasi 3 luni!");
  	  					return;
  					}
  				}
  			} else {
  				if(lunaSfarsit > 2) {
  					alert("Perioada introdusa nu poate depasi 3 luni!");
	  					return;
  				}
  			}
  			
  			$('#<portlet:namespace />factNo').val( $('#filtru_Numar_Factura').val());
  			$('#<portlet:namespace />societate').val( $('#filtru_societate').val());
  			$('#<portlet:namespace />furnizor').val( $('#filtru_furnizor').val());
  			$('#<portlet:namespace />dataFactura').val( $('#filtru_dataFactura').val());
  			$('#<portlet:namespace />codInitiativa').val( $('#filtru_codInitiativa').val());
  			$('#<portlet:namespace />Initiativa').val( $('#filtru_Initiativa').val());
  			$('#<portlet:namespace />valoareCuTVA').val( $('#filtru_valoareCuTVA').val());
  			$('#<portlet:namespace />Magazin').val( $('#filtru_Magazin').val());
  			$('#<portlet:namespace />start_date_an_param').val(anInceput);
  			$('#<portlet:namespace />start_date_luna_param').val(lunaInceput);
  			$('#<portlet:namespace />end_date_an_param').val(anSfarsit);
  			$('#<portlet:namespace />end_date_luna_param').val(lunaSfarsit);
  			$('#<portlet:namespace />exportExcel').submit();
  		});
  		
		
 		function makeAjaxCall(action, factNo, supplier, societate, dataFactura, codInitiativa, Initiativa, valoareCuTVA, Magazin) {
  			$.ajax({
  				type: "POST",
  				url: "<%=ajaxURL%>",
  				data: 	"<portlet:namespace />action=" + action + 
  						"&<portlet:namespace />start=" + start + 
  						"&<portlet:namespace />count=" + count +
  						"&<portlet:namespace />total=" + total + 
  						"&<portlet:namespace />factNo=" + factNo + 
  						"&<portlet:namespace />supplier=" + supplier +
  						"&<portlet:namespace />societate=" + societate +
  						"&<portlet:namespace />dataFactura=" + dataFactura +
  						"&<portlet:namespace />codInitiativa=" + codInitiativa +
  						"&<portlet:namespace />Initiativa=" + Initiativa +
  						"&<portlet:namespace />Magazin=" + Magazin +
  						"&<portlet:namespace />start_date_an=" +  $('#<portlet:namespace />start_date_an').val() +
  						"&<portlet:namespace />start_date_luna=" +  $('#<portlet:namespace />start_date_luna').val() +
  						"&<portlet:namespace />end_date_an=" +  $('#<portlet:namespace />end_date_an').val() +
  						"&<portlet:namespace />end_date_luna=" +  $('#<portlet:namespace />end_date_luna').val() +
  						"&<portlet:namespace />valoareCuTVA=" + valoareCuTVA,
  				success: function(msg) {
  					// get table data
  					if (msg != ""){
  						var jsonEntries = jQuery.parseJSON(msg);
  						total = jsonEntries.total;
  						debugger;
  						dataTable.set('data', eval(jsonEntries.values));
  					} else {
  						dataTable.set('data', []);
  					}
  					// set status of navigation buttons on page load
  					setNavigationButtonsState('<portlet:namespace />');
  				}
  			});
  		}
	}
);
</script>