<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme"%>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui"%>
<%@ taglib uri="http://alloy.liferay.com/tld/aui" prefix="aui"%>
<%@ taglib uri="http://liferay.com/tld/util" prefix="liferay-util"%>
<%@ page import="java.util.ResourceBundle"%>
<%@ page import="java.util.Locale"%>
<liferay-theme:defineObjects />
<portlet:defineObjects />
<aui:fieldset>
	<legend><strong>Valori factura</strong></legend>
	<aui:layout>
		<aui:column>
			<label for="<portlet:namespace />val-netaplata-ron" style="padding-top: 5px">Valoare neta de plata</label>
		</aui:column>
		<%if (id.equals("")) { %>
		<aui:column columnWidth="40">
			<aui:column>
				<aui:input type="text" id="val-netaplata-ron" name="val-netaplata-ron" value="" label="" suffix="RON" 
						inlineField="true" style="width:100px" placeholder="0.00">
					<aui:validator name="required"  errorMessage="field-required"></aui:validator>	
				</aui:input>
			</aui:column>
			<aui:column id = "net_plata_currency" style="display:none">
				<aui:input type="text" id="val-netaplata-eur" name="val-netaplata-eur" value="" label="" 
					suffix="Valuta" inlineField="true" style="width:100px" placeholder="0.00">
				</aui:input>
			</aui:column>
		</aui:column>
		<% } else { %>
		<aui:column columnWidth="40">
			<aui:column>
				<aui:input type="text" id="val-netaplata-ron" name="val-netaplata-ron" 
					value="<%=oneInvoiceHeader.get(0).get(\"payment_value_ron\") %>" label="" suffix="RON" inlineField="true" style="width:100px" placeholder="0.00">
					<aui:validator name="required" errorMessage="field-required"></aui:validator>	
				</aui:input>
			</aui:column>
			<% if(currency.equals("RON")) { %>
			<aui:column id="net_plata_currency" style="display:none">
				<aui:input type="text" id="val-netaplata-eur" name="val-netaplata-eur" 
					value="<%=oneInvoiceHeader.get(0).get(\"payment_value_curr\") %>" label="" suffix="Valuta" style="width:100px" placeholder="0.00">
				</aui:input>
			</aui:column>
			<% } else { %>
			<aui:column id="net_plata_currency">
				<aui:input type="text" id="val-netaplata-eur" name="val-netaplata-eur" 
					value="<%=oneInvoiceHeader.get(0).get(\"payment_value_curr\") %>" label="" suffix="Valuta" style="width:100px" placeholder="0.00">
					<aui:validator name="required" errorMessage="field-required"></aui:validator>
				</aui:input>
			</aui:column>
			<% } %>
		</aui:column>
		<% } %>
	</aui:layout>
</aui:fieldset>
<script type="text/javascript">
	var oldValue;
	$(document).ready(function() {
		$('#<portlet:namespace />val-netaplata-eur').autoNumeric('init', {aSep: ',', mDec: 4, vMin : -999999999, vMax : 999999999 });
		$('#<portlet:namespace />val-netaplata-ron').autoNumeric('init', {aSep: ',', mDec: 4, vMin : -999999999, vMax : 999999999 });
	});
	$('#<portlet:namespace />val-netaplata-ron').click(function(e) {
		oldValue = $('#<portlet:namespace />val-netaplata-ron').val();
	});
	$('#<portlet:namespace />val-netaplata-ron').change(function(e) {
		alert("Acest camp nu este editabil! ");
		$('#<portlet:namespace />val-netaplata-ron').val(oldValue);
	});
</script>