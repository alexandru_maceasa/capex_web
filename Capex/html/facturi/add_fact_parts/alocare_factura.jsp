<%@page import="com.profluo.ec.carrefour.definitions.UMController"%>
<%@page import="com.profluo.ecm.model.db.GeneralCodes"%>
<%@page import="com.profluo.ecm.controller.ControllerUtils"%>
<%@page import="com.profluo.ecm.model.db.DatabaseConnectionManager"%>
<%@page import="com.profluo.ecm.model.vo.DefDpis"%>
<%@page import="com.profluo.ecm.model.vo.DefAisles"%>
<%@page import="com.profluo.ecm.model.vo.DefCatGestVo"%>
<%@page import="com.profluo.ecm.model.vo.DefIFRSVo"%>
<%@page import="com.profluo.ecm.flows.InvoiceStages"%>
<%@page import="com.profluo.ecm.model.vo.DefProductVo"%>
<%@page import="com.profluo.ecm.model.vo.DefLotVo"%>
<%@page import="com.profluo.ecm.model.vo.DefIASVo"%>
<%@page import="com.profluo.ecm.model.vo.DefUMVo"%>
<%@page import="com.profluo.ecm.model.db.InvoiceHeader"%>
<%@page import="com.profluo.ecm.model.db.GenericDBStatements"%>
<%@page import="com.profluo.ecm.model.vo.DefStoreVo"%>
<%@page import="com.profluo.ecm.model.vo.DefRegEquipVo"%>
<%@page import="com.profluo.ecm.model.vo.DefVATVo"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.List"%>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui"%>
<%@ taglib uri="http://alloy.liferay.com/tld/aui" prefix="aui"%>
<%@ taglib uri="http://liferay.com/tld/util" prefix="liferay-util"%>
<%@page import="com.profluo.ecm.model.vo.DefMFActVo"%>
<%@ page import="java.util.ResourceBundle"%>
<%@ page import="java.util.Locale"%>
<liferay-theme:defineObjects />
<portlet:defineObjects />
<%
List<HashMap<String,Object>> allVAT = DefVATVo.getInstance();
List<HashMap<String,Object>> allUM = DefUMVo.getInstance();
String servicii = GeneralCodes.getServices();
%>
<legend>
	<strong>Linii factura</strong>
</legend>
<aui:layout>
	<aui:column first="true" columnWidth="100" last="true">
		<div id="alocare-facturi"></div>
	</aui:column>
</aui:layout>
<aui:layout>
	<aui:column first = "true">
	<a href="#" class="delete_lines btn">Sterge liniile - </a>
		<a href="#" class="add_article_line_invoice btn"
			onclick="javascript:;return false;">Adauga articol + </a>
	</aui:column>
	<a href="#" class="add_serv_line_invoice btn"
			onclick="javascript:;return false;">Adauga servicii + </a>
	<a href="#" class="add_article_line_invoice_prorata btn"
			onclick="javascript:;return false;">Adauga articol prorata + </a>
	<aui:column last = "true">
		<a href="#" class="allocate_all btn btn-primary">Aloca toate articolele</a>
	</aui:column>
</aui:layout>
<legend>
	<strong>Alocare articole pe linii</strong>
</legend>
<aui:layout>
	<aui:column columnWidth="100" last="true">
		<div id="alocare-magazine"></div>
	</aui:column>
</aui:layout>
<aui:layout>
	<aui:column columnWidth="10">
		<label for="<portlet:namespace />val_no_vat_art"
			style="padding-top: 5px"><liferay-ui:message key="val_no_vat_art"/></label>
	</aui:column>
	<aui:column>
	<% if (oneInvoiceHeader != null && oneInvoiceHeader.size() > 0)  { %>
		<aui:input cssClass="currencyCol" value="<%=oneInvoiceHeader.get(0).get(\"total_no_vat_curr\") %>" name="val_no_vat_art" id="val_no_vat_art" label=""
			readonly="readonly" />
	<% } else { %>
		<aui:input cssClass="currencyCol" name="val_no_vat_art" id="val_no_vat_art" value="" label=""
			readonly="readonly" />
	<% } %>
	</aui:column>
	<aui:column columnWidth="15">
		<label for="<portlet:namespace />val_no_vat_aloc_linii"
			style="padding-top: 5px"><liferay-ui:message key="val_no_vat_aloc_linii"/></label>
	</aui:column>
	<aui:column>
		<aui:input cssClass="currencyCol" name="val_no_vat_aloc_linii" id="val_no_vat_aloc_linii"
			value="" label="" readonly="readonly" />
	</aui:column>
	<aui:column>
		<label for="<portlet:namespace />difference" style="padding-top: 5px"><liferay-ui:message key="difference"/></label>
	</aui:column>
	<aui:column>
		<aui:input cssClass="currencyCol" name="difference" id="difference" value="" label=""
			readonly="readonly">
			<aui:validator name="custom" errorMessage="incorect">
				function (val, fieldNode, ruleValue) {
					var result = false;
					if (val == 0) {
						result = true;
					}
					return result;
				}
			</aui:validator>
		</aui:input>
	</aui:column>
</aui:layout>
<aui:layout>
	<aui:column columnWidth="10">
		<label for="<portlet:namespace />tvaPeLinii" style="padding-top: 5px">TVA alocat pe linii</label>
	</aui:column>
		<aui:column>
			<aui:input cssClass="currencyCol" name="tvaPeLinii" id="tvaPeLinii" value="" label="" readonly="readonly"></aui:input>
		</aui:column>
	<aui:column columnWidth="15">
		<label for="<portlet:namespace />diferentaTva" style="padding-top: 5px">Diferenta</label>
	</aui:column>
	<aui:column>
		<aui:input cssClass="currencyCol" name="diferentaTva" id="diferentaTva" value="" label="" readonly="readonly"></aui:input>
	</aui:column>
</aui:layout>
<div id="overlay-detalii-inv" class="yui3-overlay-loading"
	style="left: -350px; position: absolute; width: 280px">
	<div class="yui3-widget-hd">
		Detalii inventar pentru articolul: [<span id="cod_art_header">&nbsp;</span>]<span
			id="den_art_header">&nbsp;</span>
	</div>
	<div class="yui3-widget-bd">
		<aui:layout>
			<aui:column first="true" columnWidth="100" last="true">
				<div id="detalii-inventar"></div>
			</aui:column>
		</aui:layout>
		<aui:layout>
			<aui:column columnWidth="50" first="true">
				<aui:input value="Inchide" id="finvcancel" name="finvcancel" type="button" label=""
					cssClass="btn btn-primary-red fl"></aui:input>
			</aui:column>
			<aui:column columnWidth="50" last="true">
				<aui:input id="salveaza-inv" name="salveaza-inv" value="Salveaza"
					type="button" label="" cssClass="btn btn-primary fr"></aui:input>
			</aui:column>
		</aui:layout>
	</div>
</div>
<div id="overlay-repartizare-servicii" class="yui3-overlay-loading"
	style="left: -350px; position: absolute; width: 280px">
	<div class="yui3-widget-hd">
		Repartizare servicii
	</div>
	<div class="yui3-widget-bd">
		<aui:layout>
			<aui:column first="true" columnWidth="100" last="true">
				<div id="repartizare_servicii"></div>
			</aui:column>
		</aui:layout>
		<aui:layout>
			<aui:column columnWidth="50" first="true">
				<aui:input value="Inchide" id="inchide" name="inchide" type="button" label=""
					cssClass="btn btn-primary-red fl"></aui:input>
			</aui:column>
			<aui:column columnWidth="50" last="true">
				<aui:input id="salveaza-repartizare" name="salveaza-repartizare" value="Salveaza"
					type="button" label="" cssClass="btn btn-primary fr"></aui:input>
			</aui:column>
		</aui:layout>
	</div>
</div>
<div id="vizualizareDPI" class="yui3-overlay-loading"
	style="right: 100px; z-index: 1; position: absolute; width: 800px; display: none">
	<div class="yui3-widget-hd">Solicitare vizualizare DPI</div>
	<div class="yui3-widget-bd">
		<aui:layout>
			<aui:fieldset>
				<aui:column columnWidth="20">
					<label for="<portlet:namespace />nr_dpi" style="padding-top: 5px">Nr
						DPI: </label>
				</aui:column>
				<aui:column columnWidth="30">
					<aui:input id="nr_dpi" name="nr_dpi" label="" readonly="true"></aui:input>
				</aui:column>
				<aui:column columnWidth="20">
					<label for="<portlet:namespace />societate"
						style="padding-top: 5px">Societate:</label>
				</aui:column>
				<aui:column columnWidth="30">
					<aui:input id="societate" name="societate" label="" readonly="true"></aui:input>
				</aui:column>
			</aui:fieldset>
			<aui:fieldset>
				<aui:column columnWidth="20">
					<label for="<portlet:namespace />data_dpi" style="padding-top: 5px">Data
						DPI: </label>
				</aui:column>
				<aui:column columnWidth="30">
					<aui:input id="data_dpi" name="data_dpi" label="" readonly="true"></aui:input>
				</aui:column>
				<aui:column columnWidth="20">
					<label for="<portlet:namespace />categ_capex_dpi"
						style="padding-top: 5px">Categorie Capex:</label>
				</aui:column>
				<aui:column columnWidth="30">
					<aui:input id="categ_capex_dpi" name="categ_capex_dpi" label=""
						readonly="true"></aui:input>
				</aui:column>
			</aui:fieldset>
			<aui:fieldset>
				<aui:column columnWidth="20">
					<label for="<portlet:namespace />solicitant"
						style="padding-top: 5px">Solicitant: </label>
				</aui:column>
				<aui:column columnWidth="30">
					<aui:input id="solicitant" name="solicitant" label=""
						readonly="true"></aui:input>
				</aui:column>
			</aui:fieldset>
			<aui:fieldset>
				<aui:column columnWidth="20">
					<label for="<portlet:namespace />aprobator"
						style="padding-top: 5px">Aprobator: </label>
				</aui:column>
				<aui:column columnWidth="80">
					<aui:input id="aprobator" name="aprobator" label="" readonly="true"
						style="width: 97.6%"></aui:input>
				</aui:column>
			</aui:fieldset>
			<aui:layout>
				<aui:column columnWidth="100" last="true" first="true">
					<div id="tabelDPI"></div>
				</aui:column>
			</aui:layout>
			<aui:layout>
				<aui:column columnWidth="50" last="true" style="text-align:right">
					<aui:input value="Inchide" name="fcancel" type="button" label=""
						cssClass="btn btn-primary-red fr"
						onclick="$('#vizualizareDPI').hide()"></aui:input>
					<div class="clear"></div>
				</aui:column>
			</aui:layout>
		</aui:layout>
	</div>
</div>
<aui:input type="hidden" name="idStoreLine" id="idStoreLine" value=""/>
<script type="text/javascript">
var counter = 0;
var deletedCounter = 1;
var servicii = '<%=servicii%>';
var storeLineCounter = 0;
var remoteDataSplit = [];
var databaseLinesPosInLocalArray = [];
var dataTable;
var dataTableDPI;
var dataTableSplit;
var dataTableInv;
var dataTableDivision;
var selectedCompany = '';
var storeid = <%=storeId%>;
var companyHasChanged = false;
var idLine, denArt, codArt, tipInv;
YUI({lang: 'ro'}).use( 'aui-datatable', 'aui-datatype', 'datatable-sort', 'overlay', 'aui-form-validator', 'datatable-mutable',
	function(Y) {
	function formatCurrency(cell) {
	    format = {
	    	thousandsSeparator: ",",
	        decimalSeparator: ".",
	        decimalPlaces: noOfDecimalsToDisplay
	    };
	    return Y.DataType.Number.format(Number(cell.value), format);
	}
	function saveOnEnter(e) {
		if (e.domEvent.charCode == 13) {
			if (!e.target.validator.hasErrors()) {
				e.target.fire('save', {
	                newVal: e.target.getValue(),
	                prevVal: e.target.get('value')
	            });
			}
		}
	}
	function saveOnChange(e) {
		
		e.target.fire('save', {
            newVal: e.target.getValue(),
            prevVal: e.target.get('value')
        });
	}
	var attrNumberEditor = {inputFormatter: Y.DataType.Number.evaluate, validator: { rules: { value: { number: true } } }, on : {keyup: saveOnEnter}, showToolbar: false };
	var attrStringEditor = {on :{keyup: saveOnEnter}, showToolbar: false};	
	var firstStore = "";
		var raionDepartament = <%=DefAisles.getJsonStringAisles("326")%>;
		var allProds = <%=DefProductVo.getJsonStringProduct()%>;
		var allDPIs;
		<% if(whatPage.equals("facturi-nealocate")) { %>
			allDPIs = <%=DefDpis.getJsonStringDpis(true)%>;
		<% } else { %>
			allDPIs = <%=DefDpis.getJsonStringDpis(false)%>;
		<% } %>
		var raionOrDept = 0;
		var UMList = <%=ControllerUtils.getUmList(allUM)%>;
		var iasList = <%=DefIASVo.getJsonStringIAS()%>;
		var ifrsList = <%=DefIFRSVo.getJsonStringIFRS()%>;
		var allLots = <%=DefLotVo.getJsonStringLots()%>;
		var clasif = {1:'IT Hardware', 2:'IT Software', 0:'Active.'};
		var tvalist = <%=ControllerUtils.getTvaList(allVAT)%>;
		<%for (int i = 0; i < allCompanies.size(); i++) { %>
			<%if (allCompanies.get(i).get("status").toString().equals("A")) { %>
			var store_<%=allCompanies.get(i).get("id")%> = <%=DefStoreVo.getJsonStringStoresByCompany(allCompanies.get(i).get("id").toString())%>
			<% } %>
		<% } %>
		var tipinventar =  {0: '', 1: "individual", 2: "de grup"}; 
		var tipinregistrare = <%=DefRegEquipVo.getJsonStringRegEquip()%>;
		var actiunimf=<%=DefMFActVo.getJsonStringMfAct()%>;
		var cat_gestiune=<%=DefCatGestVo.getJsonStringCatGest()%>;
//=============================================================================================================================================================
//																	POP-UP INFO DPI LINES
//=============================================================================================================================================================			
	var datahDPI = [];
	var colsDPI = <%=ControllerUtils.getJSONDpiFormater()%>;
		dataTableDPI = new Y.DataTable({
		    columns: colsDPI,
		    data:datahDPI
		}).render('#tabelDPI');	
//=============================================================================================================================================================
//																		NR DE INVENTAR POP-UP
//=============================================================================================================================================================			
		var remoteDataInv = []; 		
		var nestedColsInv = <%=ControllerUtils.getJSONInventoryFormater()%>;	
  		// TABLE INIT
  		dataTableInv = new Y.DataTable({
  		    columns: nestedColsInv,
  		    data: remoteDataInv,
  		  	recordType: ['nr_inv', 'data_pif', 'receptie', 'id_store'],
  		    editEvent: 'click',
  		}).render('#detalii-inventar');
  		/********** Tabel repartizare servicii **********/
  		var remoteDataDivision = []; 		
		var nestedColsDivision = <%=ControllerUtils.getJSONServicesFormater()%>;
   		dataTableDivision = new Y.DataTable({
  		    columns: nestedColsDivision,
  		    data: remoteDataDivision,
  		  	recordType: ['product_code'],
  		    editEvent: 'click',
  		}).render('#repartizare_servicii');
  		 dataTableDivision.delegate('click', function (e) {
 	        var checked = e.target.get('checked') || undefined;
 	        this.data.invoke('set', 'select', checked, { silent: true });
 	        this.syncUI();
 	        if (checked) {
 	        	dataTableDivision.get('contentBox').one('.select-all').set('checked', true);
 	        }
 	    }, '.select-all', dataTableDivision);
  		dataTableDivision.delegate("click", function(e){
 	        var checked = e.target.get('checked') || undefined;
 	        this.getRecord(e.target).set('select', checked, { silent: true });
 	        this.get('contentBox').one('.select-all').set('checked', false);
 	    }, ".table-col-select input", dataTableDivision);
//	INVOICE LINE TABLE
		var remoteData = <%= ControllerUtils.generateJSONInvoiceLine(id, invoiceLinesByInvoiceNr)%>;
		//COLUMNS DEFINITION
		var nestedCols = <%=ControllerUtils.getJSONInvoiceLinesFormater()%>;
   		//INVOICE LINE TABLE INIT
   		dataTable = new Y.DataTable({
   		    columns: nestedCols,
   		    data: remoteData,
   		    editEvent: 'click',
   		    recordType: ['invoice_line_id', 'delete_line', 'counter', 'line', 'inv_number', 'unit_price_ron', 'price_no_vat_ron', 'price_vat_ron', 'price_vat_curr', 'currency',  'article_code', 'product_code', 
 		                 'description', 'um', 'quantity', 'unit_price_curr', 'price_no_vat_curr', 'vat', 'val_cu_tva', 'warranty',
  		                'id_ias', 'id_ifrs', 'dt_ras', 'dt_ifrs', 'lot', 'clasif', 'lot_warranty', 'cont_imob_in_fct', 
  		                'cont_imob_in_curs', 'cont_imob_avans', 'detalii']
   		}).render('#alocare-facturi');

//	INVOICE STORE LINE TABLE
	remoteDataSplit = <%=ControllerUtils.generateJSONInvoiceStoreLine(id, invoiceLinesByInvoiceNr, invoiceStoreLinesByInvoiceNr, companyId, InvoiceStages.PAGE_CONTABILITATE_ASTEAPTA_VALIDARE)%>;
		var nestedColsSplit = <%=ControllerUtils.getJSONInvoiceStoreLinesFormater(allCompanies)%>;  		
   		dataTableSplit = new Y.DataTable({
   		    columns: nestedColsSplit,
   		    data: remoteDataSplit,
   		    editEvent: 'click',
   		    recordType: ['id_store_line', 'id_store', 'delete_line', 'index', 'article_code', 'description', 'quantity', 'price_no_vat_curr', 'id_store_1',
   		                 'id_store_2', 'id_store_3', 'id_store_4', 'id_store_5', 'id_store_6','id_store_7','id_store_8', 'id_store_9', 'id_department', 
                            'associated_acc', 'id_tip_inreg', 'id_act_mf', 'id_dpi', 'id_tip_inventar', 'unit_price', 'product_code', 'nr',
                            'currency', 'inv_number', 'line_id', 'price_no_vat_ron', 'vizualizare_dpi', 'cont_imob_in_curs', 'cont_imob_in_fct', 
                            'cont_imob_avans', 'id_cat_gestiune', 'detalii', 'inventory']
   		}).render('#alocare-magazine');
//=============================================================================================================================================================
//																FUNCTII PENTRU ALOCARE LINII IN TABEL
//=============================================================================================================================================================			
			$('.add_article_line_invoice').click(function (e) {
			if ($('#<portlet:namespace/>cota_tva').val() == ""){
				alert("Va rugam completati cota TVA !");
				$('#<portlet:namespace/>cota_tva').focus();
				return;
			}
			if ($('#<portlet:namespace/>val_no_vat_ron').val() == ""){
				alert("Va rugam completati valoarea facturii !");
				$('#<portlet:namespace/>val_no_vat_ron').focus();
				return;
			}
			if ($('#<portlet:namespace/>supplier_type_invoice').val() == ""){
				alert("Va rugam completati tipul de furnizor !");
				$('#<portlet:namespace/>supplier_type_invoice').focus();
				return;
			}
			if ($('#<portlet:namespace/>intern_invoice_option').val() == "" && $('#<portlet:namespace/>extern_invoice_option').val() == ""){
				alert("Va rugam completati optiunea de furnizor !");
				return;
			}
			var type = "article";
			var nextLineNumber = 1;
			<% if (!id.equals("")){ %>
				nextLineNumber = <%= InvoiceHeader.getNextLineNumber(id)%>
			<% } %>
			createNewLineTopTable(type, nextLineNumber, '<portlet:namespace />');
		});
		$('.add_serv_line_invoice').click(function (e) {
			if ($('#<portlet:namespace/>cota_tva').val() == ""){
				alert("Va rugam completati cota TVA !");
				$('#<portlet:namespace/>cota_tva').focus();
				return;
			}
			if ($('#<portlet:namespace/>val_no_vat_ron').val() == ""){
				alert("Va rugam completati valoarea facturii !");
				$('#<portlet:namespace/>val_no_vat_ron').focus();
				return;
			}
			if ($('#<portlet:namespace/>supplier_type_invoice').val() == ""){
				alert("Va rugam completati tipul de furnizor !");
				$('#<portlet:namespace/>supplier_type_invoice').focus();
				return;
			}
			if ($('#<portlet:namespace/>intern_invoice_option').val() == "" && $('#<portlet:namespace/>extern_invoice_option').val() == ""){
				alert("Va rugam completati optiunea de furnizor !");
				return;
			}
			var type = "servici";
			var nextLineNumber = 1;
			<% if (!id.equals("")){ %>
				nextLineNumber = <%= InvoiceHeader.getNextLineNumber(id)%>
			<% } %>
			createNewLineTopTable(type, nextLineNumber, '<portlet:namespace />');
		});
		$('.add_article_line_invoice_prorata').click(function (e) {
			if ($('#<portlet:namespace/>cota_tva').val() == ""){
				alert("Va rugam completati cota TVA !");
				$('#<portlet:namespace/>cota_tva').focus();
				return;
			}
			if ($('#<portlet:namespace/>val_no_vat_ron').val() == ""){
				alert("Va rugam completati valoarea facturii !");
				$('#<portlet:namespace/>val_no_vat_ron').focus();
				return;
			}
			if ($('#<portlet:namespace/>supplier_type_invoice').val() == ""){
				alert("Va rugam completati tipul de furnizor !");
				$('#<portlet:namespace/>supplier_type_invoice').focus();
				return;
			}
			if ($('#<portlet:namespace/>intern_invoice_option').val() == "" && $('#<portlet:namespace/>extern_invoice_option').val() == ""){
				alert("Va rugam completati optiunea de furnizor !");
				return;
			}
			var type = "prorata";
			var nextLineNumber = 0;
			<% if (!id.equals("")){ %>
				nextLineNumber = <%= InvoiceHeader.getNextLineNumber(id)%>
			<% } %>
			createNewLineTopTable(type, nextLineNumber, '<portlet:namespace />');
		});
		$('.allocate_all').click(function (e) {
			e.preventDefault();
			if (companyHasChanged){
				storeid = 0;
			}
			allocateAllArticles('<portlet:namespace/>', storeid, servicii);
			$('#<portlet:namespace />val_no_vat_art').autoNumeric('update', {aSep: ',', mDec: 4, vMin : -999999999, vMax : 999999999 });
			$('#<portlet:namespace />val_no_vat_aloc_linii').autoNumeric('update', {aSep: ',', mDec: 4, vMin : -999999999, vMax : 999999999 });
			$('#<portlet:namespace />difference').autoNumeric('update', {aSep: ',', mDec: 4, vMin : -999999999, vMax : 999999999 });
		});
//=============================================================================================================================================================
//																				TABLE.DELEGATE
//=============================================================================================================================================================			
dataTable.delegate('click', function (e) {
	        var checked = e.target.get('checked') || undefined;
	        this.data.invoke('set', 'select', checked, { silent: true });
	        this.syncUI();
	        if (checked) {
	        	dataTable.get('contentBox').one('.select-all').set('checked', true);
	        }
	    }, '.select-all', dataTable);
	    
	    dataTable.delegate("click", function(e){
		        var checked = e.target.get('checked') || undefined;
		        this.getRecord(e.target).set('select', checked, { silent: true });
		        this.get('contentBox').one('.select-all').set('checked', false);
		    }, ".table-col-select input", dataTable);
	
   		dataTableSplit.after('record:change', function (e) {
			var obj = e.target.changed;
			var existingData = e.target._state.data;
			if (typeof obj.id_act_mf != 'undefined' || typeof obj.id_tip_inreg != 'undefined') {
				if(obj.id_act_mf == 1){
					wasClicked[existingData.delete_line.value] = false;
				}
				setAssociatedAccount();
			}
			var companie = $('#<portlet:namespace />company').val();
			var store = 0;
			if ( companie == 1 ){
				store = obj.id_store_1;
			} else if ( companie == 2) {
				store = obj.id_store_2;
			} else if ( companie == 3 ) {
				store = obj.id_store_3;
			}else if ( companie == 4 ) {
				store = obj.id_store_4;
			}else if ( companie == 5 ) {
				store = obj.id_store_5;
			}else if ( companie == 6 ) {
				store = obj.id_store_6;
			}else if ( companie == 7 ) {
				store = obj.id_store_7;
			}else if ( companie == 8 ) {
				store = obj.id_store_8;
			}else if ( companie == 9 ) {
				store = obj.id_store_9;
			}
			if(typeof obj.id_store_1 != 'undefined') {
				removeOldInventory(obj, '<portlet:namespace />');
			}
			if(typeof obj.id_store_2 != 'undefined') {
				removeOldInventory(obj, '<portlet:namespace />');
			}
			if(typeof obj.id_store_3 != 'undefined') {
				removeOldInventory(obj, '<portlet:namespace />');
			}
			if(typeof obj.id_store_4 != 'undefined') {
				removeOldInventory(obj, '<portlet:namespace />');
			}
			if(typeof obj.id_store_5 != 'undefined') {
				removeOldInventory(obj, '<portlet:namespace />');
			}
			if(typeof obj.id_store_6 != 'undefined') {
				removeOldInventory(obj, '<portlet:namespace />');
			}
			if(typeof obj.id_store_7 != 'undefined') {
				removeOldInventory(obj, '<portlet:namespace />');
			}
			if(typeof obj.id_store_8 != 'undefined') {
				removeOldInventory(obj, '<portlet:namespace />');
			}
			if(typeof obj.id_store_9 != 'undefined') {
				removeOldInventory(obj, '<portlet:namespace />');
			}
			
			if (typeof obj.quantity != 'undefined') {
				setValNoVatStoreLine('<portlet:namespace />');
			}
			
			if (typeof obj.id_dpi != 'undefined') {
				setDPILink(obj.id_dpi);
			}
   		});	
   		// click on dpi link
   		dataTableSplit.delegate('click', function (e) {
   			e.preventDefault();
   		    var target = e.target.get('id');
   		    var ids = target.replace('dpi_','').split("_");
   		    var lineid = ids[0];
   		    var dpiId = ids[1];
   		 	showDPIPopupInvoice(e.target, dpiId,'<portlet:namespace/>', '<%=ajaxURL%>', Y);
		}, '.dpilink', dataTableSplit);
   		// Changes of info on the main table
		dataTable.after('record:change', function (e) { 
			var obj = e.target.changed;
			var existingData = e.target._state.data;
			if(typeof obj.vat != 'undefined'){
				setValueWithVatLine('<portlet:namespace/>');
			}
			if(typeof obj.price_no_vat_curr != 'undefined'){
				setValueWithVatLine('<portlet:namespace/>');
				//recalculate payment value
				setPaymentValues('<portlet:namespace />');
			}
			// there was a change on the cod prod
			if (typeof obj.product_code != 'undefined') {
				setProductValuesInvoice(existingData.line.value, obj.product_code, '<portlet:namespace />', '<%=ajaxURL%>');
			}
			//there was a change on ias
			if (typeof obj.id_ias != 'undefined') {
				setIasDtValuesInvoice(obj.id_ias, '<portlet:namespace />', '<%=ajaxURL%>');
			}
			//there was a change on ifrs
			if (typeof obj.id_ifrs != 'undefined') {
				setIfrsDtValuesInvoice(obj.id_ifrs, '<portlet:namespace />', '<%=ajaxURL%>');
			}
			//there was a change on ifrs
			if (typeof obj.warranty != 'undefined') {
				recalculatePaymentValue(obj.warranty, existingData, '<portlet:namespace />');
				e.target._state.data.warranty.initValue = obj.warranty;
			}
			// there was a change on quantity
			if (typeof obj.quantity != 'undefined'  || typeof obj.unit_price_curr != 'undefined') {
				if ( existingData.quantity.value != '' &&  existingData.unit_price_curr.value != '' ) {
					//console.log("setLineValues");
					setLineValues(existingData.quantity.value, existingData.unit_price_curr.value, '<portlet:namespace />');
				}
			}
			$('#<portlet:namespace />val_no_vat_art').autoNumeric('update', {aSep: ',', mDec: 4, vMin : -999999999, vMax : 999999999 });
			$('#<portlet:namespace />val_no_vat_aloc_linii').autoNumeric('update', {aSep: ',', mDec: 4, vMin : -999999999, vMax : 999999999 });
			$('#<portlet:namespace />difference').autoNumeric('update', {aSep: ',', mDec: 4, vMin : -999999999, vMax : 999999999 });
		});
   		// click on alocare linie
   		//CREATE NEW STORE LINE ONLY IF THE CURRENT PAGE IS PAGE_CONTABILITATE_FACTURI_NEALOCATE 
   		dataTable.delegate('click', function (e) {
   		    var target = e.target.get('id');
   		    console.log(target);
   		    var flagInvoiceID = 0;
   		    <%if(id != "" ){%>
   		    	<%if (whatPage == InvoiceStages.PAGE_CONTABILITATE_ASTEAPTA_VALIDARE || whatPage == InvoiceStages.PAGE_CONTABILITATE_ASTEAPTA_LITIGII) {%>
   		    	flagInvoiceID = 1;
   		    <%} }%>
   		    if(flagInvoiceID == 0){
   		    	createNewStoreLine(target.replace('row',''), '<portlet:namespace />');
   		    }else{
   		    	alert("Nu este permisa alocarea de articole pe linii!");
   		    }
   			$('#<portlet:namespace />val_no_vat_art').autoNumeric('update', {aSep: ',', mDec: 4, vMin : -999999999, vMax : 999999999 });
			$('#<portlet:namespace />val_no_vat_aloc_linii').autoNumeric('update', {aSep: ',', mDec: 4, vMin : -999999999, vMax : 999999999 });
			$('#<portlet:namespace />difference').autoNumeric('update', {aSep: ',', mDec: 4, vMin : -999999999, vMax : 999999999 });
		}, '.allocline', dataTable);
   		dataTable.delegate('click', function(e){
   			showDivisionOverlay(e, Y, servicii);
   		}, '.repartizare', dataTable);
   		var wasClicked = [];
   		var wasClickedMajorare = [];
   		wasClicked[0] = false;
   		wasClickedMajorare[0] = false;
   		var invTypes = [];
		var invIndex = 0;
   		// click for inventory number link
		dataTableSplit.delegate("click", function(e){
	        e.preventDefault();
	        // id of line, name of article and type of inventory
	     	var line = 0;
	        var iasId = 0;
	        var store = "";
	        var loadedInventory = "";
	        var quantity = 0;
	        var flag = false;
	        var idStoreLine = 0;
	        idLine = e.target.getAttribute("id").replace('line-', '');
	        $('#<portlet:namespace />idStoreLine').val(idLine);
	        var majorare = "";
	        // loop through table
	        var ml = dataTableSplit.data, msg = '', template = '';
		    ml.each(function (item, i) {
		    	var dataS = item.getAttrs(['id_store_line', 'id_store', 'delete_line', 'index', 'article_code', 'description', 'quantity', 'price_no_vat_curr','id_store_1',
		    	     		                 'id_store_2', 'id_store_3', 'id_store_4', 'id_store_5', 'id_store_6','id_store_7','id_store_8', 'id_store_9', 'id_department', 
		    	                             'associated_acc', 'id_tip_inreg', 'id_act_mf', 'id_dpi', 'id_tip_inventar', 'unit_price', 'product_code', 'nr',
		    	                             'currency', 'inv_number', 'line_id', 'price_no_vat_ron', 'vizualizare_dpi', 'cont_imob_in_curs', 'cont_imob_in_fct', 
		    	                             'cont_imob_avans', 'id_cat_gestiune', 'detalii', 'inventory']);
		    	if (dataS.delete_line == idLine) {
		    		var dataS_store = 0;
		    		if (selectedCompany == 1) {
		    			dataS_store = dataS.id_store_1;
		    		} else if (selectedCompany == 2) {
		    			dataS_store = dataS.id_store_2;
		    		} else if (selectedCompany == 3) {
		    			dataS_store = dataS.id_store_3;
		    		} else if (selectedCompany == 4) {
		    			dataS_store = dataS.id_store_4;
		    		} else if (selectedCompany == 5) {
		    			dataS_store = dataS.id_store_5;
		    		} else if (selectedCompany == 6) {
		    			dataS_store = dataS.id_store_6;
		    		} else if (selectedCompany == 7) {
		    			dataS_store = dataS.id_store_7;
		    		} else if (selectedCompany == 8) {
		    			dataS_store = dataS.id_store_8;
		    		} else if (selectedCompany == 9) {
		    			dataS_store = dataS.id_store_9;
		    		} 
		    		if (dataS_store != "0" ){
				    	tipInv = dataS.id_tip_inventar;
				    	denArt = dataS.description;
				    	codArt = dataS.delete_line;
				    	quantity = dataS.quantity;
				    	line = dataS.nr;
				    	loadedInventory = dataS.inventory;
				    	store = dataS_store;
				    	majorare = dataS.id_act_mf;
				    	idStoreLine = dataS.id_store_line;
		    		} else {
		    			alert("Atentie! Pe aceasta linie magazinul nu a fost alocat!");
		    			flag = true;
		    			return;
		    		}
		    	}
		    });
		    //true id the store was not set
	        if (flag){
	        	return;
	        }
	        var wasChanged = false;
	        if (invTypes[invIndex-1] != tipInv) {
	        	invTypes[invIndex++] = tipInv;
	        	wasChanged = true;
	        }
		 	// prepare table, insert into remoteDataInv, add to dataTableInv   
	        // IF the inventory was already loaded on this line do not get a new sequence from the DB
	        if (loadedInventory != "" && wasClicked[idLine] && !wasChanged) {
	        	console.log("intra pe if");
				// incarcare nr de inventar
		        var jsonEntries = jQuery.parseJSON(loadedInventory);
		        // dataTableInv.set('data', eval(jsonEntries.values));
				// show pop-up and load record in pop-up
				dataTableInv.set('data', eval(jsonEntries));
				showInventoryOverlay(e, Y);
				updateInventoryOverlay('<portlet:namespace />');
	        } else {
	        	console.log("intra pe else");
	        	//tip_act_mf == majorare
	        	if (majorare == 2) {
	        		if(loadedInventory != "") {
	        			var jsonEntries = jQuery.parseJSON(loadedInventory);
	     		        // dataTableInv.set('data', eval(jsonEntries.values));
	     				// show pop-up and load record in pop-up
	     				dataTableInv.set('data', eval(jsonEntries));
	     				showInventoryOverlay(e, Y);
	        		} else {
	        			<% if (invoiceStoreLinesByInvoiceNr != null) { %>
		        			makeAjaxToGetInventoryDetails(idStoreLine, wasClicked, idLine, e, Y);
		        			hasInvNo = true;
	        			<% } else { %>
		        			console.log("intra unde trebuie");
			        		emptyPopUpInventory();
			        		if(quantity < 0) {
			        			quantity = quantity * (-1);
			        		}
		        			setEntriesDataTableInv(quantity, tipInv, store);
		        			showInventoryOverlay(e, Y);
		        			updateInventoryOverlay('<portlet:namespace />');
	        			<% } %>
	        		}
	        	} else {
				    var ml1 = dataTable.data, msg = '', template = '';
				    
				    ml1.each(function (item, i) {
				        var data = item.getAttrs(['line', 'id_ias']);
		
				        if (data.line == line) {
				        	iasId = data.id_ias;
				        }
				    });
				    if (loadedInventory != "" && tipInv == 2 && wasChanged) {
				    	var invComponents = loadedInventory.split(',');
				    	loadedInventory = invComponents[0] + ',' + invComponents[1] + ',' + invComponents[2] + ']';
				    	loadedInventory = loadedInventory.replace("]]","]");
				    	loadedInventory = loadedInventory.replace("inventory_no","nr_inv");
				    	loadedInventory = loadedInventory.replace("pif_date","data_pif");
				    	loadedInventory = loadedInventory.replace("reception","receptie");
				        var jsonEntries = jQuery.parseJSON(loadedInventory);
						dataTableInv.set('data', eval(jsonEntries));
						showInventoryOverlay(e, Y);
						updateInventoryOverlay('<portlet:namespace />');
				    } else {
				    	var hasInvNo = false;
				    	<% if (invoiceStoreLinesByInvoiceNr != null && invoiceStoreLinesByInvoiceNr.size() > 0) { %>
				    			makeAjaxToGetInventoryDetails(idStoreLine, wasClicked, idLine, e, Y);
				    			hasInvNo = true;
						<% } %>
				    	if ( !hasInvNo ) {
				    		var idNewProj = '';
				    		if ($('#<portlet:namespace />proiect_nou').is(':checked')) {
				    			var proiectNou = $("select[name=<portlet:namespace />project]").find(":selected").text();
				    			idNewProj = proiectNou.split("-")[0];
				    		}
					    	$.ajax({
								type: "POST",
								url: '<%=ajaxURL%>',
								data: 	"<portlet:namespace />action=getInventoryNo" +
										"&<portlet:namespace />store=" + store +
										"&<portlet:namespace />iasId=" + iasId +
										"&<portlet:namespace />count=" + quantity +
										"&<portlet:namespace />tipInv=" + tipInv +
										"&<portlet:namespace />id_new_prj=" + idNewProj,
								success: function(msgjson) {
									if (msgjson != "" && msgjson.length > 2 ) {
											wasClicked[idLine] = true;
									        var jsonEntries = jQuery.parseJSON(msgjson);
											dataTableInv.set('data', eval(jsonEntries));
											showInventoryOverlay(e, Y);
											updateInventoryOverlay('<portlet:namespace />');
										} else {
											alert("Nu exista regula pentru generarea numarului de inventar.");
											return;
										}
								},
								error: function(msg) {
									alert('A aparut o problema in preluarea datelor de pe server. Va rugam incercati din nou!');
								}
							});
					    }
		        	}
	        	}
	        }
	    }, ".proddet", dataTableSplit);
   		
   		dataTableInv.after('record:change', function (e) {
   			var obj = e.target.changed;
			var existingData = e.target._state.data;
			console.log("Merge");
			if(typeof obj.nr_inv != 'undefined'){
				console.log("Click = "+codArt);
				wasClicked[codArt] = true;
			}
   		});
   		function makeAjaxToGetInventoryDetails(idStoreLine, wasClicked, idLine, e, Y){
   			$.ajax({
				type: "POST",
				url: '<%=ajaxURL%>',
				data: 	"<portlet:namespace />action=getInventoryDetails" +
						"&<portlet:namespace />idStoreLine=" + idStoreLine,
				success: function(msgjson) {
					if (msgjson != "" && msgjson.length > 2 ) {
							wasClicked[idLine] = true;
					        var jsonEntries = jQuery.parseJSON(msgjson);
							dataTableInv.set('data', eval(jsonEntries));
							showInventoryOverlay(e, Y);
							updateInventoryOverlay('<portlet:namespace />');
						} else {
							alert("Nu exista numar de inventar pe aceasta linie");
							return;
						}
				},
				error: function(msg) {
					alert('A aparut o problema in preluarea datelor de pe server. Va rugam incercati din nou!');
				}
			});
   		}
//=============================================================================================================================================================
//																	DOCUMENT READY FUNCTIONS
//=============================================================================================================================================================			
  	$('.delete_lines').click(function(e){
		e.preventDefault();
		var idsDataBase = [];
		var idsLocalArray = [];
		var lineIds = "";
		var hasChecked = false;
		var ml  = dataTable.data, msg = '', template = '';
		 ml.each(function (item, i) {
			 var data = item.getAttrs(['delete_line', 'select', 'invoice_line_id']);
			 if (data.select == true) {
				 hasChecked = true;
				 if(data.invoice_line_id != ''){
					 idsDataBase.push(data.invoice_line_id);
				 } else {
					 idsLocalArray.push(data.delete_line);
				 }
			 }
		 });
		 if(!hasChecked) {
			 alert("Nu ati selectat nici o linie.");
		 } else {
			 if (confirm("Sunteti sigur/a ca doriti sa stergeti liniile selectate?")){
				 //delete lines from interface that are not in database
				 if (idsLocalArray.length > 0){
					 for (var i = idsLocalArray.length - 1; i >= 0; i--) {
						 deleteLineFromInterfaceById(idsLocalArray[i], '<portlet:namespace />');
					 }
				 }
				 if (idsDataBase.length > 0){
					//delete lines from database
					makeAjaxCallDeleteLineInvoice(idsDataBase, "deleteLine", '<portlet:namespace/>', '<%=ajaxURL%>', false);
				 }		 
			 }
		 }
	});
	//la modificarea garantiei
	$('#<portlet:namespace />procent_garantie').change(function() {
		//daca e factura din EDI
		if($('#<portlet:namespace />val_no_vat_ron').val()) {
			var wasChanged = false;
			var rows = [];
			var ml = dataTable.data, msg = '', template = '';
			ml.each(function (item, i) {
				var data = item.getAttrs(['invoice_line_id', 'delete_line', 'counter', 'line', 'inv_number', 'unit_price_ron', 'price_no_vat_ron', 'price_vat_ron', 'price_vat_curr', 'currency',  'article_code', 'product_code', 
				  		                 'description', 'um', 'quantity', 'unit_price_curr', 'price_no_vat_curr', 'vat', 'val_cu_tva', 'warranty',
				   		                'id_ias', 'id_ifrs', 'dt_ras', 'dt_ifrs', 'lot', 'clasif', 'lot_warranty', 'cont_imob_in_fct', 
				   		                'cont_imob_in_curs', 'cont_imob_avans', 'detalii']);
				console.log("Garantie pe lot : " + data.lot_warranty);
				//daca produsul are garantie
				if(data.lot_warranty == "Y") {
					data.warranty = parseFloat($('#<portlet:namespace />procent_garantie').val() / 100 * data.price_no_vat_curr).toFixed(4);
					wasChanged = true;
				}
				rows.push(data);
			});
			if(wasChanged) {
				dataTable.set('recordset', rows);
			}
		}
	});
	$("#<portlet:namespace/>finvcancel").click(function () { saveInventoryOverlay('<portlet:namespace />','close', '<%=ajaxURL%>'); });
	$("#<portlet:namespace/>salveaza-inv").click(function () { saveInventoryOverlay('<portlet:namespace />','save', '<%=ajaxURL%>'); });
	$("#<portlet:namespace/>inchide").click(function () { $('#overlay-repartizare-servicii').hide(); });
	$("#<portlet:namespace/>salveaza-repartizare").click(function () { saveDivision('<portlet:namespace/>', servicii); });
	$('#<portlet:namespace />val_vat_ron').change(function(){
		$('#<portlet:namespace />val_vat_ron').autoNumeric('update', {aSep: ',', mDec: 4, vMin : -999999999, vMax : 999999999 });
	});
	$('#<portlet:namespace />val_with_vat_ron').change(function(){
		$('#<portlet:namespace />val_with_vat_ron').autoNumeric('update', {aSep: ',', mDec: 4, vMin : -999999999, vMax : 999999999 });
	});
	// calculate total values once page loads
		$(document).ready(function() { 
			deletedCounter = 1;
			$('#<portlet:namespace />val_no_vat_ron').autoNumeric('init', {aSep: ',', mDec: 4, vMin : -999999999, vMax : 999999999 });
			$('#<portlet:namespace />val_vat_ron').autoNumeric('init', {aSep: ',', mDec: 4, vMin : -999999999, vMax : 999999999 });
			$('#<portlet:namespace />val_with_vat_ron').autoNumeric('init', {aSep: ',', mDec: 4, vMin : -999999999, vMax : 999999999 });
			$('#<portlet:namespace />val_vat_curr').autoNumeric('init', {aSep: ',', mDec: 4, vMin : -999999999, vMax : 999999999 });
			$('#<portlet:namespace />val_with_vat_curr').autoNumeric('init', {aSep: ',', mDec: 4, vMin : -999999999, vMax : 999999999 });
			$('#<portlet:namespace />val_no_vat_art').autoNumeric('init', {aSep: ',', mDec: 4, vMin : -999999999, vMax : 999999999 });
			$('#<portlet:namespace />val_no_vat_aloc_linii').autoNumeric('init', {aSep: ',', mDec: 4, vMin : -999999999, vMax : 999999999 });
			$('#<portlet:namespace />difference').autoNumeric('init', {aSep: ',', mDec: 4, vMin : -999999999, vMax : 999999999 });
			<% if (oneInvoiceHeader != null && oneInvoiceHeader.size() < 1) { %>
				setTotalNoVat('<portlet:namespace />');
			<% } %>
			setTotalNoVatLines('<portlet:namespace />');
			<% if (!whatPage.equals(InvoiceStages.PAGE_CONTABILITATE_ASTEAPTA_VALIDARE)) { %>
				setPaymentValues('<portlet:namespace />');
			<% } %>
			//get store list if the company is already set
			var company = $('#<portlet:namespace />company');
			if (company.val() != ""){
				//save the current company
				selectedCompany = company.val(); 
				//show the store header
				$('.stores_' + company.val()).show();
				//getStoresAssociatedWithCompany("getStoresAssociatedWithCompany", company.val());
			}
			//get exchange rate on document ready
			var currency_select = $('#<portlet:namespace />currency');
			var date 			= $('#<portlet:namespace />inv_date2');
			// autocomplete the exchange rate when the currency is not RON
			currency_select.change(function(e) {
					var elementT 	= e.target;
					var elementId 	= elementT.id;
					var action 		= "getExchangeRate";
					// make ajax call
					if (currency_select.val() != "RON") {
						//if currency is not RON
						if (date.val() != ""){
							//if the invoice date is selected
							makeAjaxCallGetExchangeRate(action, date.val(), currency_select.val(), '<portlet:namespace />', '<%=ajaxURL%>');
						} else {
							//reverse the change to RON
							currency_select.val("RON");
							alert("Va rugam selectati data facturii!");
						}
					}
				});
			//adauga_factura.jsp : make ajax call to get all all invoices that have type = 1 and belong to the selected supplier
				var invoice_type = $('#<portlet:namespace />tip-factura');
				var supplier = $('#<portlet:namespace />vendor');
				var company = $('#<portlet:namespace />company');
				if(invoice_type && supplier && company) {
					invoice_type.on('change',function(e){
						//if invoice type is "de corectie"
						if (invoice_type.val() == 2) {
							makeAjaxCallGetBaseInvoices("getBaseInvoices", supplier.val(), company.val(), '<%=ajaxURL%>');
						}
					});		
					//if the supplier is changed and invoice type is "de corectie"
					supplier.on('change', function(e){
						if (supplier.val() != ""){
							//if invoice type is "de corectie"
							if (invoice_type.val() == 2) {
								makeAjaxCallGetBaseInvoices('<portlet:namespace />', "getBaseInvoices", supplier.val(), company.val(), '<%=ajaxURL%>');
							}
						} else {
							//reset the invoice type
							invoice_type.val("");
							$('#<portlet:namespace />nr-fact-baza2').hide();
							$('#<portlet:namespace />nr-fact-baza3').hide();
						}
					});
					company.on('change', function(e){
						if (company.val() != ""){
							//hide the previous store header
							$('.stores_' + selectedCompany).hide();
							//hide the store column 
							$('.stores_' + selectedCompany).addClass('hiddencol');
							//show the new store header
							$('.stores_' + company.val()).show();
							//save the new company
							selectedCompany = company.val();
							//set the flag to true
							companyHasChanged = true;
						}
					});
				}
			});
//=============================================================================================================================================================
//																				DELETE
//=============================================================================================================================================================			
		//DELETE STORE LINE FROM THE INTERFACE; THE STORE LINE IS NOT IN THE DATABASE
		dataTableSplit.delegate('click', function (e) {
			e.preventDefault();
			if (confirm("Sunteti sigur/a ca doriti sa stergeti aceasta linie?")){
				deleteStoreLineFromInterface(e, '<portlet:namespace />');
			}
		}, '.deleteStoreLineFromArray', dataTableSplit);
		//DELETE LINE FROM THE INTERFACE; THE LINE IS NOT IN THE DATABASE
		dataTable.delegate('click', function (e) {
			e.preventDefault();
			if (confirm("Sunteti sigur/a ca doriti sa stergeti aceasta linie?")){
				deleteLineFromInterface(e, '<portlet:namespace />');
			}
		}, '.deleteLineFromArray', dataTable);
		<% if (whatPage.equals(InvoiceStages.PAGE_FACTURI_NEALOCATE) ||
				whatPage.equals(InvoiceStages.PAGE_CONTABILITATE_ASTEAPTA_VALIDARE)) { %>
		//DELETE LINE FROM DATABASE
		dataTable.delegate('click', function (e) {
			e.preventDefault();
			//get target
			var elem = e.currentTarget.get('id');
			//get the id of the target
			var elementId	= elem.replace('row', '');
			if (confirm("Sunteti sigur/a ca doriti sa stergeti aceasta linie?")){
				makeAjaxCallDeleteLineInvoice(elementId, "deleteLine", '<portlet:namespace/>', '<%=ajaxURL%>', true);
			}
		}, '.deleteLineFromDatabase', dataTable);
		<% } else { %> 
			//ON OTHET PAGES THE DELETE OPTION IS NOT ALLOWED	
			dataTable.delegate('click', function (e) {
				alert("Stergerea nu este permisa!");
				e.preventDefault();
			}, '.deleteLineFromDatabase', dataTable);
		<% } %>
		dataTableSplit.delegate('click', function (e) {
			alert("Stergerea nu este permisa!");
			e.preventDefault();
		}, '.deleteStoreLineFromDatabase', dataTableSplit);
//=============================================================================================================================================================
//																			SUBMIT 
//=============================================================================================================================================================			
		// bind the submit button on form
		Y.on('click', function(e){
			e.preventDefault();
			submitInvoiceForm(e, '<portlet:namespace/>', true);
		},'.validate_inv');
	}
);
</script>