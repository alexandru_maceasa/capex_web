<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme"%>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui"%>
<%@ taglib uri="http://alloy.liferay.com/tld/aui" prefix="aui"%>
<%@ taglib uri="http://liferay.com/tld/util" prefix="liferay-util"%>
<liferay-theme:defineObjects />
<portlet:defineObjects />
<legend><strong>Alte avansuri in sold acordate furnizorului</strong></legend>
<aui:column first="true" columnWidth="100" last="true">
<div id="avansuri-furnizor"></div>
</aui:column>
<script type="text/javascript">
function showLineDetails(id) {
	return false;
}
YUI().use( 'aui-datatable', 'aui-datatype', 'datatable-sort',
	function(Y) {
		// data array
		var remoteData = [
			{
				id: 1, 
				data_avans: '2015-06-01', 
				valoare_valuta: 523.00, 
				moneda: 'RON',
				valoare_ron: 523.00,
				explicatii_avans: 'NA',
				comentariu_contabilitate: '...',
				avansuri_propuse_de_compensat: true
			}
		];

		var clasif = ['IT Hardware', 'IT Software', 'Active.'];
		
		// COLUMN INFO and ATTRIBUTES
		var nestedCols = [
			{ key: 'data_avans', label: 'Data Avans' },
			{ key: 'valoare_valuta', label: 'Valoare Valuta'},
			{ key: 'moneda', label: 'Moneda'},
			{ key: 'valoare_ron', label: 'Valoare RON' },
			{ key: 'explicatii_avans', label:  'Explicatii Avans'},
			{ key: 'avansuri_propuse_de_compensat', label:  'Avansuri Propuse de Compensat',
				formatter: '<input type="checkbox" checked/>',
	            emptyCellValue: '<input type="checkbox"/>'
			}		      
		];

		// TABLE INIT
		var dataTable = new Y.DataTable({
		    columns: nestedCols,
		    data: remoteData,
		    editEvent: 'click'
		}).render('#avansuri-furnizor');
		
		//dataTable.get('boundingBox').unselectable();
	}
);
</script>