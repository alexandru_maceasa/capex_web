<%@page import="com.sun.crypto.provider.DESCipher"%>
<%@page import="com.profluo.ecm.model.db.Administrare"%>
<%@page import="com.profluo.ecm.model.db.DefCategory"%>
<%@page import="com.profluo.ecm.flows.InvoiceStages"%>
<%@page import="com.profluo.ecm.model.vo.DefCurrencyVo"%>
<%@page import="com.profluo.ecm.model.vo.DefCompaniesVo"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.profluo.ecm.model.db.InvoiceHeader"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme"%>

<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui"%>
<%@ taglib uri="http://alloy.liferay.com/tld/aui" prefix="aui"%>
<%@ taglib uri="http://liferay.com/tld/util" prefix="liferay-util"%>
 
<liferay-theme:defineObjects />
<portlet:defineObjects />
<portlet:resourceURL id="ajaxURL" var="ajaxURL"/>

<% // PAGINATION Start - env setup 
int start 			= 0; 
int count 			= 15;
int total 			= 0;
String type 		= "";
int stage 			= 0;
String categorie 	= "";
String datafactura 	= "";
String datainreg 	= "";
String supplierCode = "";
String supplierName = "";
String magazinName = "";
int company_id		=  0;
String number 		= "";
String moneda		= "";
String descriere	= "";
%>

<%
	//locale = renderRequest.getLocale();
	//ResourceBundle resmain = portletConfig.getResourceBundle(locale);
	// retrieve all invoices headers from DB
	List<HashMap<String,Object>> allInvoices = new ArrayList<HashMap<String,Object>>();
	// retrieve all companies from DB
	List<HashMap<String,Object>> allCompanies = DefCompaniesVo.getInstance();
	// retrieve all currencies from DB
	List<HashMap<String,Object>> allCurrencies = DefCurrencyVo.getInstance();
	
	List<HashMap<String,Object>> allLotCat = DefCategory.getAll();
	
	int pageStatus 		= 0;
	String pageLayout 	= "";
	String pageType   		= "";
	String storeCode 	= "";
	
	String email 	= "";
	
	String invoiceId = null;
	String invoiceCid = null;
	
	try {
		invoiceId = request.getAttribute("invoice_id").toString();
		invoiceCid = request.getAttribute("invoice_cid").toString();
	} catch (Exception e) {}
	
	System.out.println("invoiceID --------->>>>> " + invoiceId);

	if(user.getAddresses().size() != 0 && user.getAddresses().get(0).getStreet2() != null){
		storeCode = user.getAddresses().get(0).getStreet2();
	}
	System.out.println(user.getUserGroups());
	boolean activeExtended = false;
	if(user.getUserGroups() != null) {
		for(int i = 0 ; i < user.getUserGroups().size(); i++) {
			if(user.getUserGroups().get(i).getName().equals("Active Extended")) {
				activeExtended = true;
			}
		}
	}
	
	if(user.getEmailAddress() != null){
		email = user.getEmailAddress();
	}
	try {
		pageStatus 				= Integer.parseInt(renderRequest.getAttribute("page-status").toString());
	} catch (Exception e) { }
	try {
		pageLayout 				= renderRequest.getAttribute("page-layout").toString();
	} catch (Exception e) { }
	try {
		pageType				= renderRequest.getAttribute("type").toString();
	} catch (Exception e) { }

	//restore filters 
	try {
		categorie = renderRequest.getParameter("categorie_filter").toString();
	} catch (Exception e) { categorie="";}
	try{
		number = renderRequest.getParameter("number_filter").toString();
	} catch (Exception e) { number = "0";}
	try{
		company_id = Integer.parseInt(renderRequest.getParameter("company_id_filter").toString());
	} catch (Exception e) { company_id = 0;}
	try{
		moneda = renderRequest.getParameter("moneda_filter").toString();
	} catch (Exception e) { moneda = "";}
	try{
		datafactura = renderRequest.getParameter("datafactura_filter").toString();
	} catch (Exception e) { datafactura="";}
	try{
		datainreg = renderRequest.getParameter("datainreg_filter").toString();
	} catch (Exception e) { datainreg = "";}
	try{
		supplierCode = renderRequest.getParameter("supplier_code_filter").toString();
	} catch (Exception e) {supplierCode = "";}
	try{
		supplierName = renderRequest.getParameter("supplier_name_filter").toString();
	} catch (Exception e) {supplierName = "";}

	String [] allTypes = pageType.split(",");
	
	HashMap<String,String> allStages = new HashMap<String,String>();

	if(pageLayout.equals(InvoiceStages.PAGE_FACTURI_NEALOCATE)){
		allInvoices = InvoiceHeader.getAllFilteredForAccounting(start, count, user.getUserId(), company_id, moneda, number, storeCode, datafactura, datainreg, supplierCode, supplierName, magazinName, email, categorie, "1", descriere);
		total = InvoiceHeader.getAllFilteredCountForAccounting(user.getUserId(), company_id, moneda, number, storeCode, datafactura, datainreg, supplierCode, supplierName, magazinName, email, categorie, "1", descriere);
	} else if(pageLayout.equals(InvoiceStages.KONTAN_PAGE)) {
		allInvoices = InvoiceHeader.getAllFiltered(start, count, 0, "", "0", 10, pageStatus, storeCode, "", "", "", "", email, categorie, user);
		total = InvoiceHeader.getAllFilteredCount(start, count, 0, "", "0", 10, pageStatus, storeCode, "", "", "", "", email, categorie, user);
	} else if (pageLayout.equals(InvoiceStages.PAGE_CONTABILITATE_ASTEAPTA_VALIDARE)){
		allInvoices = InvoiceHeader.getAllFilteredForAccounting(start, count, user.getUserId(), company_id, moneda, number, storeCode, datafactura, datainreg, supplierCode, supplierName, magazinName, email, categorie, "3", descriere);
		total = InvoiceHeader.getAllFilteredCountForAccounting(user.getUserId(), company_id, moneda, number, storeCode, datafactura, datainreg, supplierCode, supplierName, magazinName, email, categorie, "3", descriere);
	} else {
		allInvoices = InvoiceHeader.getAllFiltered(start, count, user.getUserId(), company_id, moneda, number, storeCode, datafactura, datainreg, supplierCode, supplierName, magazinName, email, categorie, descriere);
		total = InvoiceHeader.getAllFilteredCount(user.getUserId(), company_id, moneda, number, storeCode, datafactura, datainreg, supplierCode, supplierName, magazinName, email, categorie, descriere);
	}

	System.out.println("Total -> " + total);
	System.out.println("AllStages -> " + allStages);
	type = pageType;
	stage = pageStatus;
	
	boolean isAuthorized = Administrare.isAuthorizedToEdit(user.getEmailAddress());
%>

<script type="text/javascript">
var start = "<%=start%>";
var count = "<%=count%>";
var total = "<%=total%>";
var stage = "<%=stage%>";
var type = "<%=type%>";
var categorie = "<%=categorie%>";
var storeName = "<%=storeCode%>";
var datainreg = "<%=datainreg%>";
var datafactura = "<%=datafactura%>";
var supplierCode = "<%=supplierCode%>";
var supplierName = "<%=supplierName%>";
var magazinName = "<%=magazinName%>";
var isAuthorized = <%=isAuthorized%>;
//filters
var company_id = "<%=company_id%>";
var moneda = "<%=moneda%>";
var number = "<%=number%>";
var pageLayout = '<%=pageLayout%>';
var descriere = '<%=descriere%>';
</script>

<liferay-ui:success message="delete_ok" key="delete_ok"/>
<liferay-ui:success message="invoiceAdded" 	key="invoiceAdded"/>
<liferay-ui:success message="invoiceUpdated" 	key="invoiceUpdated"/>
<liferay-ui:success message="update_ok" key="update_ok"/>
<liferay-ui:error message="A aparut o problema la trimiterea facturi in Cheltuieli Generale. Va rugam reincercati mai tarziu!" key="chgen_error"/>
<% if(invoiceId != null) { %>
<div class="div_invoice_id">
	<p> ID-ul intern al facturii salvate este : <%=invoiceId%> </p>
</div>
<% } %>
<% if(invoiceCid != null) { %>
<div class="div_invoice_cid">
	<p> CID-ul generat al facturii salvate este : <%=invoiceCid%> </p>
</div>
<% } %>
<%-- Links --%>
<portlet:renderURL var="addEntryURL" >
	<portlet:param name="mvcPath" value="/html/facturi/adauga_factura.jsp"></portlet:param>
</portlet:renderURL>

<portlet:renderURL var="viewEntryURL" >
	<portlet:param name="mvcPath" value="/html/includes/visualizations/vizualizare_factura.jsp"></portlet:param>
</portlet:renderURL>

<portlet:renderURL var="approversURL" >
	<portlet:param name="mvcPath" value="/html/definitions/aprobarefactura/view.jsp"></portlet:param>
</portlet:renderURL>

<% if (pageLayout.contains("facturi-nealocate") || pageLayout.contains("asteapta-validare")) { %>
<aui:layout>
	<aui:column>
	<div id="numberOfInvoices">
		<% if (pageLayout.contains("facturi-nealocate")) { %>
			Numarul de facturi nealocate este :  <%= total %>
		<% } else { %>
			Numarul de facturi ce asteapta validare este :  <%= total %>
		<% } %>
	</div>
	</aui:column>
</aui:layout>
<% } %>
<aui:layout>
	<aui:column columnWidth="65" first = "true">
		<%if (pageLayout.equals(InvoiceStages.PAGE_FACTURI_NEALOCATE) && !activeExtended)  { %>
		<aui:button cssClass="btn btn-primary" onClick="<%=addEntryURL.toString()%>" value="Adauga factura"></aui:button>
		<br/><br/>
		<% } %>
	</aui:column>
		<aui:column columnWidth="25">
			<aui:input id="descriere" name="descriere" label="" placeholder="cautare in descriere linii factura"></aui:input>
		</aui:column>
		<aui:column columnWidth="10" style="margin-top:-5px">
			<aui:button cssClass="btn btn-primary" id="butonCautare" value="Cautare"></aui:button>
		</aui:column>
	<div id="filtru_dataimport_div" style="display:none">
		<aui:input type="text" id="data_fact" name="data_fact" value="" placeholder="aaaa-ll-zz" label=""></aui:input>
	</div>
</aui:layout>

<aui:layout>
	<aui:column first="true" columnWidth="100" last="true">
		<div id="listing-facturi"></div>
	</aui:column>
</aui:layout>

<%-- HTML for PAGINATION --%>
<%@ include file="/html/includes/pagination.jsp" %>

<%--  datepicker for table header --%>
<div id="filtru_datafactura_div" style="display:none">
	<aui:input type="text" id="datafactura_header" name="datafactura_header" placeholder="Data factura" value=""  label="" style="width:100px;margin:0">
	</aui:input>
</div>
<%--  second datepicker for table header --%>
<div id="filtru_datainregfactura_div" style="display:none">
	<aui:input type="text" id="datainreg_header" name="datainreg_header" placeholder="Data import" value="" label="" style="width:110px;margin:0">
	</aui:input>
</div>

<script type="text/javascript">

	var oldDateValue2 = '';
	var oldDateValue = '';
	YUI({lang: 'ro'}).use('aui-datepicker', 'aui-datatable', 'aui-datatype', 'datatable-sort', function(Y) {
		// called whenever a change in date selection was performed
		var oldDateValue2 = $('#<portlet:namespace />datafactura_header').val();
		// called whenever a change in date selection was performed
		var oldDateValue = $('#<portlet:namespace />datainreg_header').val();
		
		var remoteData = [
  		<%
  		if (allInvoices != null) {
  		for (int i = 0; i < allInvoices.size(); i++) {	
  		%>
  		{
  			id: 			<%=allInvoices.get(i).get("id")%>,
  			inv_number: 	'<%=allInvoices.get(i).get("inv_number")%>',
  			created: 		'<%=allInvoices.get(i).get("created")%>',
  			supplier_code: 	'<%=allInvoices.get(i).get("supplier_code")%>',
  			supplier_name: 	'<%=allInvoices.get(i).get("supplier_name")%>',
  			inv_date: 		'<%=allInvoices.get(i).get("inv_date")%>',
  			pay_date: 		'<%=allInvoices.get(i).get("pay_date")%>', 
  			total_with_vat_curr: <%=allInvoices.get(i).get("total_with_vat_curr")%>,
  			currency: 		'<%=allInvoices.get(i).get("currency")%>',
  			company_name: 	'<%=allInvoices.get(i).get("company_name")%>',
  			store_name:		'<%=allInvoices.get(i).get("store_name")%>',
  			lot_category: 	'<%=allInvoices.get(i).get("lot_category")%>',
  			in_process: 	'<%=allInvoices.get(i).get("in_process")%>',
  			active_user: 	'<%=allInvoices.get(i).get("active_user")%>'
  		}
  		<% if (i != (allInvoices.size() - 1)) { out.print(","); } %>
  		<% } } %>
  		];
		
		function formatCurrency(cell) {
		    format = {
		    	thousandsSeparator: ",",
		        decimalSeparator: ".",
		        decimalPlaces: noOfDecimalsToDisplay
		    };
		    return Y.DataType.Number.format(Number(cell.value), format);
		}
  		
  		// COLUMN INFO and ATTRIBUTES
  		var nestedCols = [
  		    { key: 'id', className: 'hiddencol'},
  		  	{ key: 'in_process', className: 'hiddencol'},
  		  	{ key: 'active_user', className: 'hiddencol'},
  			{ key: 'inv_number', 
  		    	label: '<input class="field" style="width:60px !important;margin:0" type="text" value="" id="filter-nr" name="filter-nr" placeholder="Nr."></input>' },
  			{ key: 'created', label: '<div id="filter-data-inreg"></div>'},
  			{ key: 'supplier_code',
  				label: '<input class="field" style="width:70px !important;margin:0" type="text" value="" id="filter-supplier_code" name="filter-supplier_code" placeholder="Cod furnizor" style="margin:0"/>'
  			},
  			{ key: 'supplier_name', 
  				label: '<input class="field" type="text" value="" id="filter-supplier_name" name="filter-supplier_code" placeholder="Furnizor" style="margin:0"/>'
  			},
  			{ key: 'inv_date', label:  '<div id="filter-dataf"></div>'},
  			{ key: 'pay_date', label:  'Data Scadenta'},
  			{ key: 'total_with_vat_curr', label: 'Val cu TVA', formatter: formatCurrency, className: 'currencyCol'},
  			{ key: 'currency', label: '<select id="filter-moneda" name="filter-moneda" label="" style="width:100px;margin:0">' +
  				'<option selected="selected" value="">Moneda</option>' +
  				<% for (int i = 0; i < allCurrencies.size(); i++) { %>
  				<% if (allCurrencies.get(i).get("status").equals("A")) {%>
  				<% out.println("'<option value=\"" + allCurrencies.get(i).get("ref") +"\">" + allCurrencies.get(i).get("ref") +"</option>' + "); %>
  				<% } } %>
  				'</select>'},
  			{ key: 'company_name', label: '<select id="filter-societate" name="filter-societate" label="" style="width:100px;margin:0">' +
  				'<option selected="selected" value="0">Societate</option>' +
  				<% for (int i = 0; i < allCompanies.size(); i++) { %>
  				<% if (allCompanies.get(i).get("status").equals("A")) {%>
  				<% out.println("'<option value=\"" + allCompanies.get(i).get("id") +"\">" + allCompanies.get(i).get("name") +"</option>' + "); %>
  				<% } } %>
  				'</select>'},
  			<% if(pageLayout.equals("asteapta-aprobare")){ %>
  				{ key : 'store_name', label: '<input class="field" type="text" value="" id="filter-magazin_name" name="filter-magazin_name" placeholder="Magazin" style="margin:0"/>', className: 'store_names' },
  			<% } %>
  				<% if (!pageLayout.equals(InvoiceStages.PAGE_FACTURI_NEALOCATE)) {%>
  				{ key: 'lot_category', label: '<select id="filtru_categorie_lot" name="filtru_categorie_lot" label="" style="width:100px;margin:0">' +
  	  				'<option selected="selected" value="">Categorie</option>' +
  	  				<% for (int i = 0; i < allLotCat.size(); i++) { %>
  	  				<% out.println("'<option value=\"" + allLotCat.get(i).get("name") +"\">" + allLotCat.get(i).get("name") +"</option>' + "); %>
  	  				<% } %>
  	  				'</select>'},
  	  			<% } %>
  			{ key: 'actiuni', label: 'Actiuni', allowHTML: true, 
  				// gets the id of the row and creates an unique link for every row
  				<% if (pageLayout.equals(InvoiceStages.PAGE_FACTURI_NEALOCATE) && !activeExtended) {  // facturi neinregistrate - contabilitate %>
  					formatter: '<a href="#" id="rowconta{id}-{in_process}-{active_user}" class="editrow">Inregistrare</a>'
  				<% } else if ( pageLayout.equals(InvoiceStages.PAGE_CONTABILITATE_ASTEAPTA_LITIGII)) { 
  					// contabilitate %>
  					formatter: '<a href="#" id="row{id}" class="editrow">Vizualizare</a>' 
  				<% } else if (pageLayout.equals(InvoiceStages.PAGE_APROBATORI_LITIGII)) { 
  					// aprobatori %>	
  					formatter: '<a href="#" id="rowapp{id}" class="editrow">Validare</a>' 

  				<% } else if ( pageLayout.equals(InvoiceStages.PAGE_APROBATORI_ASTEAPTA_VALIDARE)) { 
  					// aprobatori %>	
  					formatter: '<a href="#" id="rowapp{id}" class="editrow">Validare</a>'
  				<% } else if (pageLayout.equals(InvoiceStages.PAGE_CONTABILITATE_ASTEAPTA_VALIDARE)) { 
  					// contabilitate 
				 if (isAuthorized) { %>
  						formatter: '<a href="#" id="rowacc{id}" class="editrow">Actualizare informatii</a>' 
  					<% } else { %> 
						formatter: '<a href="#" id="rowacc{id}" class="editrow">Vizualizare</a>' 
					<% } %>
  				<% } else if (pageLayout.equals(InvoiceStages.KONTAN_PAGE)) { 
  					// facturi kontan - contabilitate %>	
  					formatter: '<a href="#" id="row{id}" class="editrow">Vizualizare</a>' 
  				<% } %>
  			}
  		];

  		// TABLE INIT
  		var dataTable = new Y.DataTable({
  		    columns: nestedCols,
  		    data: remoteData,
	  		scrollable: "x",
	        width: "100%",
  		    recordType: ['id', 'in_process', 'active_user', 'inv_number', 'created', 'supplier_code', 
  		                 'supplier_name', 'inv_date', 'pay_date', 'total_no_vat_curr', 
  		                 'total_with_vat_curr', 'currency', 'company_name'],
  		    editEvent: 'click'
  		}).render('#listing-facturi');
  		
  		//dataTable.get('boundingBox').unselectable();
  		
  		function edit(sel_id, action) {
  		    var ml  = dataTable.data, msg = '', template = '';
  	
  		    ml.each(function (item, i) {
  		        var data = item.getAttrs(['id', 'in_process', 'active_user', 'inv_number', 'created', 'supplier_code', 
  		                                 'supplier_name', 'inv_date', 'pay_date', 'total_no_vat_curr', 
  		         		                 'total_with_vat_curr', 'currency', 'company_name']);
  		        if (data.id == sel_id) {
  					$('#<portlet:namespace />id').val(data.id);
  					$('#<portlet:namespace />inv_number').val(data.inv_number);
  					$('#<portlet:namespace />created').val(data.created);
  					$('#<portlet:namespace />supplier_code').val(data.supplier_code);
  					$('#<portlet:namespace />supplier_name').val(data.supplier_name);
  					$('#<portlet:namespace />inv_date').val(data.inv_date);
  					$('#<portlet:namespace />datascadenta').val(data.pay_date);
  					$('#<portlet:namespace />total_no_vat_curr').val(data.total_no_vat_curr);
  					$('#<portlet:namespace />total_with_vat_curr').val(data.total_with_vat_curr);
  					$('#<portlet:namespace />param_currency').val(data.currency);
  					
  					if(action == 1){
  						makeAjaxToSetInProcessFlag('setFlag', '<%=ajaxURL.toString()%>', '<portlet:namespace/>', data.id, 1);
  					}else if(action == 2) {
  						
  						var portletNamespace = '<portlet:namespace/>';
  						
  						$.ajax({
  							type: "POST",
  							url: '<%=ajaxURL.toString()%>',
  							data: 	portletNamespace + "action=checkAccess" + 
  									"&" + portletNamespace + "invoiceId=" + data.id +
  									"&" + portletNamespace + "userId=" + <%=user.getUserId()%>,
  							success: function(msg) {
  								if(msg.indexOf('true') >= 0) {
  									$('#<portlet:namespace />categorie_filter').val( $('#filtru_categorie_lot').val());
  			  	  					$('#<portlet:namespace />supplier_code_filter').val($('#filter-supplier_code').val());
  			  	  					$('#<portlet:namespace />supplier_name_filter').val($('#filter-supplier_name').val());
  			  	  					$('#<portlet:namespace />number_filter').val($('#filter-nr').val());
  			  	  					$('#<portlet:namespace />company_id_filter').val($('#filter-societate').val());
  			  	  					$('#<portlet:namespace />moneda_filter').val($('#filter-moneda').val());
  			  	  					$('#<portlet:namespace />datafactura_filter').val(oldDateValue2);
  			  	  					$('#<portlet:namespace />datainreg_filter').val(oldDateValue);
  			  						$('#<portlet:namespace />registerInvoice').attr("action", "<%=approversURL.toString() %>");
  			  						$('#<portlet:namespace />registerInvoice').attr("target", "_blank");
  			  						$('#<portlet:namespace />registerInvoice').submit();
  								} else {
  									alert("Factura a fost deja aprobata. Pentru actualizarea listingului de facturi trebuie sa faceti refresh paginii!");
  								}
  							}
  						});
  						
  						
  					} else if (action == 3 ) {
  						if(isAuthorized) {
  							$('#<portlet:namespace />registerInvoice').attr("action", "<%=addEntryURL.toString() %>");
  						} else {
  							$('#<portlet:namespace />registerInvoice').attr("action", "<%=viewEntryURL.toString() %>");
  						}
  						$('#<portlet:namespace />registerInvoice').submit();
  					}
  		        }
  		    });
  		}
  		
  		dataTable.delegate('click', function (e) {
  			debugger;
  		    // undefined to trigger the emptyCellValue
  		    e.preventDefault();
  		    var elem = e.target.get('id');
  		    if(elem.indexOf("rowapp") >= 0) {
  				edit(elem.replace('rowapp',''), 2);
  		    } else if (elem.indexOf("rowacc") >= 0) {
  		    	edit(elem.replace('rowacc',""), 3);
  		    } else {
  		    	var detalii = elem.replace('rowconta',"");
  		    	var parts = detalii.split("-");
  		    	if(parts[1] == 1 && parts[2] != '<%=user.getUserId()%>' ) {
  		    		alert("Factura se afla in procesare la un alt utilizator.");
  		    	} else {
  		    		edit(parts[0], 1);
  		    	}
  		    }
  		}, '.editrow', dataTable);
  		
  		
  		// PAGINATION
  		// set status of navigation buttons on page load
  		setNavigationButtonsState('<portlet:namespace />');
  		$('.navigation').click(function(e) {
  			var elementT 	= e.target;
  			var elementId 	= elementT.id;
  			var action 		= "";
  			
  			// determine which button was pressed and set the value of start accordingly
  			if (elementId.indexOf("first") > -1)	{ action = "first"; start = 0; }
  			if (elementId.indexOf("prev") > -1) 	{ action = "prev"; if (start - count <= 0) {start = 0} else { start -= count; } }
  			if (elementId.indexOf("next") > -1) 	{ action = "next"; start = parseInt(start) + parseInt(count); }
  			if (elementId.indexOf("last") > -1) 	{ action = "last"; start = parseInt(parseInt(total) / parseInt(count)) * count; if (start == total) { start -= count; } }
  			
  			makeAjaxCall(action);
  		});
  		
  		//filtrare pe categorie
  		$('#filtru_categorie_lot').change(function(e) {
  			var elementT 	= e.target;
  			var elementId 	= elementT.id;
  			var action 		= "filter";
  			categorie = elementT.value;
  			descriere = $('#<portlet:namespace/>descriere').val();
  			company = $('#filter-societate').val();
  			moneda = $('#filter-moneda').val();
  			supplier_code = $('#filter-supplier_code').val();
  			supplierName = $('#filter-supplier_name').val();
  			magazinName = $('#filter-magazin_name').val();
  			number = $('#filter-nr').val();
			datafactura = oldDateValue2;
			datainreg = oldDateValue;
  			// reset start page 
  			start = 0;
  			// reset count
  			total = 0;
  			// make ajax call
  			makeAjaxCall(action);
  			
  		});
  		
  		// filtrare societate
  		$('#filter-societate').change(function(e) {
  			var elementT 	= e.target;
  			var elementId 	= elementT.id;
  			var action 		= "filter";
  			// get selected value
  			company_id = elementT.value;
  			descriere = $('#<portlet:namespace/>descriere').val();
  			categorie = $('#filtru_categorie_lot').val();
  			moneda = $('#filter-moneda').val();
  			supplier_code = $('#filter-supplier_code').val();
  			supplierName = $('#filter-supplier_name').val();
  			magazinName = $('#filter-magazin_name').val();
  			number = $('#filter-nr').val();
			datafactura = oldDateValue2;
			datainreg = oldDateValue;
  			// reset start page 
  			start = 0;
  			// reset count
  			total = 0;
  			// make ajax call
  			makeAjaxCall(action);
  		});
  		
  		// filtrare moneda
  		$('#filter-moneda').change(function(e) {
  			var elementT 	= e.target;
  			var elementId 	= elementT.id;
  			var action 		= "filter";
  			// get selected value
  			moneda = elementT.value;
  			descriere = $('#<portlet:namespace/>descriere').val();
  			categorie = $('#filtru_categorie_lot').val();
  			supplier_code = $('#filter-supplier_code').val();
  			supplierName = $('#filter-supplier_name').val();
  			magazinName = $('#filter-magazin_name').val();
  			number = $('#filter-nr').val();
  			company_id = $('#filter-societate').val();
			datafactura = oldDateValue2;
			datainreg = oldDateValue;
  			// reset start page 
  			start = 0;
  			// reset count
  			total = 0;
  			// make ajax call
  			makeAjaxCall(action);
  		});

  		// filtrare dupa cod furnizor
  		$('#filter-supplier_code').keyup(function (e) {
  		    if ($("#filter-supplier_code").is(":focus") && (e.keyCode == 13)) {
  				var elementT 	= e.target;
  				var elementId 	= elementT.id;
  				var action 		= "filter";
  				// get selected value
  				supplierCode = elementT.value;
  				descriere = $('#<portlet:namespace/>descriere').val();
  				categorie = $('#filtru_categorie_lot').val();
  				moneda = $('#filter-moneda').val();
  	  			supplierName = $('#filter-supplier_name').val();
  	  			magazinName = $('#filter-magazin_name').val();
  	  			number = $('#filter-nr').val();
  	  			company_id = $('#filter-societate').val();
  				datafactura = oldDateValue2;
  				datainreg = oldDateValue;
  				if (number == "") {
  					number = 0;
  				}
  				// reset start page 
  				start = 0;
  				// reset count
  				total = 0;
  				// make ajax call
  				makeAjaxCall(action);
  		    }
  		});
  		
  		// filtrare dupa nume furnizor
  		$('#filter-supplier_name').change(function (e) {
  				var elementT 	= e.target;
  				var elementId 	= elementT.id;
  				var action 		= "filter";
  				// get selected value
  				supplierName = elementT.value;
  				descriere = $('#<portlet:namespace/>descriere').val();
  				categorie = $('#filtru_categorie_lot').val();
  				moneda = $('#filter-moneda').val();
  	  			supplier_code = $('#filter-supplier_code').val();
  	  			number = $('#filter-nr').val();
  	  			company_id = $('#filter-societate').val();
  	  			magazinName = $('#filter-magazin_name').val();
  				datafactura = oldDateValue2;
  				datainreg = oldDateValue;
  				if (number == "") {
  					number = 0;
  				}
  				// reset start page 
  				start = 0;
  				// reset count
  				total = 0;
  				// make ajax call
  				makeAjaxCall(action);
  		    
  		});
  		
  	// filtrare dupa magazin
  		$('#filter-magazin_name').keyup(function (e) {
  		    if ($("#filter-magazin_name").is(":focus") && (e.keyCode == 13)) {
  				var elementT 	= e.target;
  				var elementId 	= elementT.id;
  				var action 		= "filter";
  				// get selected value
  				magazinName = elementT.value;
  				descriere = $('#<portlet:namespace/>descriere').val();
  				categorie = $('#filtru_categorie_lot').val();
  				moneda = $('#filter-moneda').val();
  	  			supplierName = $('#filter-supplier_name').val();
  	  			supplierCode = $('#filter-supplier_code').val();
  	  			magazinName = $('#filter-magazin_name').val();
  	  			number = $('#filter-nr').val();
  	  			company_id = $('#filter-societate').val();
  				datafactura = oldDateValue2;
  				datainreg = oldDateValue;
  				if (number == "") {
  					number = 0;
  				}
  				// reset start page 
  				start = 0;
  				// reset count
  				total = 0;
  				// make ajax call
  				makeAjaxCall(action);
  		    }
  		});
  		
  		// filtrare dupa nr factura
  		$('#filter-nr').keyup(function (e) {
  		    if ($("#filter-nr").is(":focus") && (e.keyCode == 13)) {
  				var elementT 	= e.target;
  				var elementId 	= elementT.id;
  				var action 		= "filter";
  				// get selected value
  				number = elementT.value;
  				descriere = $('#<portlet:namespace/>descriere').val();
  				categorie = $('#filtru_categorie_lot').val();
  				moneda = $('#filter-moneda').val();
  	  			supplier_code = $('#filter-supplier_code').val();
  	  			supplierName = $('#filter-supplier_name').val();
  	  			company_id = $('#filter-societate').val();
  	  			magazinName = $('#filter-magazin_name').val();
  				datafactura = oldDateValue2;
  				datainreg = oldDateValue;
  				if (number == "") {
  					number = 0;
  				}
  				// reset start page 
  				start = 0;
  				// reset count
  				total = 0;
  				// make ajax call
  				console.log("number = "+number);
  				makeAjaxCall(action);
  		    }
  		});
  		
  		// filtrare dupa descriere
  		$('#butonCautare').click(function() {
 				var action = "filter";
 				descriere = $('#<portlet:namespace/>descriere').val();
 				categorie = $('#filtru_categorie_lot').val();
 				moneda = $('#filter-moneda').val();
 	  			supplier_code = $('#filter-supplier_code').val();
 	  			supplierName = $('#filter-supplier_name').val();
 	  			magazinName = $('#filter-magazin_name').val();
 	  			company_id = $('#filter-societate').val();
 				datafactura = oldDateValue2;
 				datainreg = oldDateValue;
 				if (number == "") {
 					number = 0;
 				}
 				// reset start page 
 				start = 0;
 				// reset count
 				total = 0;
 				// make ajax call
 				makeAjaxCall(action);
  		});
  		
  		function makeAjaxCall(action) {
  			$.ajax({
  				type: "POST",
  				url: "<%=ajaxURL%>",
  				data: 	"<portlet:namespace />action=" + action + 
  						"&<portlet:namespace />start=" + start + 
  						"&<portlet:namespace />count=" + count +
  						"&<portlet:namespace />total=" + total +
  						"&<portlet:namespace />moneda=" + moneda +
  						"&<portlet:namespace />number=" + number +
  						"&<portlet:namespace />company_id=" + company_id +
  						"&<portlet:namespace />datafactura=" + datafactura +
  						"&<portlet:namespace />datainreg=" + datainreg +
  						"&<portlet:namespace />type=" + type +
  						"&<portlet:namespace />stage=" + stage +
  						"&<portlet:namespace />descriere=" + descriere +
  						"&<portlet:namespace />supplier_code=" + supplierCode +
  						"&<portlet:namespace />supplier_name=" + supplierName +
  						"&<portlet:namespace />magazin_name=" + magazinName +
  						"&<portlet:namespace />storeName=" + storeName +
  						"&<portlet:namespace />categorie=" + categorie +
  						"&<portlet:namespace />pageLayout=" + pageLayout,
  				success: function(msg) {
  					// get table data
  					if (msg != ""){
  						var jsonEntries = jQuery.parseJSON(msg);
  						total = jsonEntries.total;
  						$('#numberOfInvoices').html("");
  						$('#numberOfInvoices').html("Numarul de facturi nealocate este : " + total);
  						dataTable.set('data', eval(jsonEntries.values));
  					} else {
  						dataTable.set('data', []);
  					}
  					
  					// set status of navigation buttons on page load
  					setNavigationButtonsState('<portlet:namespace />');
  				}
  			});
  		}
		
		function dateChangedFact(input) {
			oldDateValue2 = input.val();
			// get selected value
			if ($('#<portlet:namespace />datainreg_header').val() == ""){
				datainreg = "";
			} else {
				datainreg = $('#<portlet:namespace />datainreg_header').val();
			}
			datafactura = oldDateValue2;
			moneda = $('#filter-moneda').val();
			categorie = $('#filtru_categorie_lot').val();
  			supplier_code = $('#filter-supplier_code').val();
  			supplierName = $('#filter-supplier_name').val();
  			magazinName = $('#filter-magazin_name').val();
  			number = $('#filter-nr').val();
  			company_id = $('#filter-societate').val();
			// reset start page 
			start = 0;
			// reset count
			total = 0;
			action = "filter";
			// make ajax call
			makeAjaxCall(action);
		}
		
		var datepicker2 = new Y.DatePicker({
			trigger : '#<portlet:namespace />datafactura_header',
			mask : '%Y-%m-%d',
			popover : {
				zIndex : 1
			},
			popover: {
				zIndex : 1,
		          toolbars: {
		            header: [[
		              {
		                icon:'icon-trash',
		                label: 'Reseteaza filtru',
		                on: {
		                  click: function() {
		                	  datepicker2.clearSelection();
		                  }
		                }
		              }]]
		          }
			},
			after: {
	            selectionChange: function(event) {
	            	if (oldDateValue2 != $('#<portlet:namespace />datafactura_header').val()) {
	            		oldDateValue2 = $('#<portlet:namespace />datafactura_header').val();
	            		dateChangedFact($('#<portlet:namespace />datafactura_header'));
	            	}
	            	//$('#data_filter').html('');
	            }
	        },
			calendar : {
				dateFormat : '%Y-%m-%d'
			}
		});
		

		function dateChangedInreg(input) {
			
			oldDateValue = input.val();
			// get selected value
			if ($('#<portlet:namespace />datafactura_header').val() == ""){
				datafactura = "";
			} else {
				datafactura = $('#<portlet:namespace />datafactura_header').val();
			}
			datainreg = oldDateValue;
			categorie = $('#filtru_categorie_lot').val();
			moneda = $('#filter-moneda').val();
  			supplier_code = $('#filter-supplier_code').val();
  			supplierName = $('#filter-supplier_name').val();
  			magazinName = $('#filter-magazin_name').val();
  			number = $('#filter-nr').val();
  			company_id = $('#filter-societate').val();
			// reset start page 
			start = 0;
			// reset count
			total = 0;
			action = "filter";
			// make ajax call
			makeAjaxCall(action);
		}
		
		var datepicker = new Y.DatePicker({
			trigger : '#<portlet:namespace />datainreg_header',
			mask : '%Y-%m-%d',
			popover : {
				zIndex : 1
			},
			popover: {
				zIndex : 1,
		          toolbars: {
		            header: [[
		              {
		                icon:'icon-trash',
		                label: 'Reseteaza filtru',
		                on: {
		                  click: function() {
		                	  datepicker.clearSelection();
		                  }
		                }
		              }]]
		          }
			},
			calendar : {
				dateFormat : '%Y-%m-%d'
			},
			after: {
	            selectionChange: function(event) {
	            	if (oldDateValue != $('#<portlet:namespace />datainreg_header').val()) {
	            		oldDateValue = $('#<portlet:namespace />datainreg_header').val();
	            		dateChangedInreg($('#<portlet:namespace />datainreg_header'));
	            	}
	            	//$('#data_filter').html('');
	            }
	        }
		});
	});
	
	
	$(document).ready(function() {
		isAuthorized = <%=isAuthorized%>;
	});
</script>

<aui:form method="post" action="<%=addEntryURL.toString() %>" name="registerInvoice" id="registerInvoice" style="margin:0;padding:0">
	<aui:input type="hidden" id="id" name="id"/>
	<aui:input type="hidden" id="inv_number" name="inv_number"/>
	<aui:input type="hidden" id="created" name="created"/>
	<aui:input type="hidden" id="supplier_code" name="supplier_code"/>
	<aui:input type="hidden" id="supplier_name" name="supplier_name"/>
	<aui:input type="hidden" id="magazin_name" name="magazin_name"/>
	<aui:input type="hidden" id="inv_date" name="inv_date"/>
	<aui:input type="hidden" id="datascadenta" name="datascadenta"/>
	<aui:input type="hidden" id="total_no_vat_curr" name="total_no_vat_curr"/>
	<aui:input type="hidden" id="total_with_vat_curr" name="total_with_vat_curr"/>
	<aui:input type="hidden" id="param_currency" name="currency"/>
	<aui:input type="hidden" id="storeId" name="storeId" value="<%=storeCode %>"/> 	
	<aui:input type="hidden" id="listingType" name="listingType" value = "<%=type %>"/> 
	<aui:input type="hidden" id="invoiceMultipleStages" name="invoiceMultipleStages" value = "<%=allStages %>"/> 
	<aui:input type="hidden" id="pages" name="pages" value="<%=pageLayout %>" />
	<aui:input type="hidden" id="categorie_filter" name="categorie_filter"/>
	<aui:input type="hidden" id="number_filter" name="number_filter"/>
	<aui:input type="hidden" id="company_id_filter" name="company_id_filter"/>
	<aui:input type="hidden" id="moneda_filter" name="moneda_filter"/>
	<aui:input type="hidden" id="datafactura_filter" name="datafactura_filter"/>
	<aui:input type="hidden" id="datainreg_filter" name="datainreg_filter"/>
	<aui:input type="hidden" id="supplier_code_filter" name="supplier_code_filter"/>
	<aui:input type="hidden" id="supplier_name_filter" name="supplier_name_filter"/>
</aui:form>