<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="com.profluo.ecm.model.db.DefCatGest"%>
<%@page import="com.profluo.ecm.model.db.DefProduct"%>
<%@page import="com.profluo.ecm.model.db.DefStore"%>
<%@page import="com.profluo.ecm.model.db.DPIHeader"%>
<%@page import="com.profluo.ecm.flows.SupplierStages"%>
<%@page import="com.profluo.ecm.model.db.DatabaseConnectionManager"%>
<%@page import="com.profluo.ecm.model.db.DefSupplier"%>
<%@page import="com.profluo.ecm.model.db.DefExchRates"%>
<%@page import="com.profluo.ecm.controller.ControllerUtils"%>
<%@page import="com.profluo.ecm.model.vo.ListaInitVo"%>
<%@page import="com.profluo.ecm.model.db.InvoiceHeader"%>
<%@page import="com.profluo.ecm.model.vo.DefAttDocsVo"%>
<%@page import="com.profluo.ecm.model.vo.DefVATVo"%>
<%@page import="com.profluo.ecm.model.vo.DefRelComerVo"%>
<%@page import="com.profluo.ecm.model.vo.DefTipFactVo"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.List"%>
<%@page import="com.profluo.ecm.model.vo.DefCurrencyVo"%>
<%@page import="com.profluo.ecm.model.vo.DefCompaniesVo"%>
<%@page import="com.liferay.portal.util.PortalUtil"%>
<%@page import="com.profluo.ecm.flows.Links"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme"%>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui"%>
<%@ taglib uri="http://alloy.liferay.com/tld/aui" prefix="aui"%>
<%@ taglib uri="http://liferay.com/tld/util" prefix="liferay-util"%>
<%@ page import="java.util.ResourceBundle"%>
<%@ page import="java.util.Locale"%>
<liferay-theme:defineObjects />
<portlet:defineObjects />
<%
	locale 					= renderRequest.getLocale();
	ResourceBundle resmain 	= portletConfig.getResourceBundle(locale);
	String nrFactura 	= "";
	String id			= "";
	String whatPage     = "";
	boolean isFromLipsaDoc = false;
	String status 		= "";
	try { id = renderRequest.getParameter("id").toString();
	} catch (Exception e){
		try { id 						= renderRequest.getParameter("invoiceId").toString();
			  isFromLipsaDoc = true;
			  status = "";
		} catch(Exception e1){}
	}
	try{ whatPage = renderRequest.getAttribute("page-layout").toString();
	} catch (Exception e){
		try { whatPage = renderRequest.getParameter("pagina").toString();
		} catch(Exception e1){
			try { whatPage = renderRequest.getParameter("source_page").toString();
			} catch (Exception e2) {}
		}
	}
 	if ((id.equals("") || id == null ) ) {
 		String url = PortalUtil.getCurrentCompleteURL(request);
 		String [] params = url.split("&");
 		String [] lastParameter = params[params.length-1].split("=");
 		if (lastParameter[0].equals("id")){ id = lastParameter[1]; }
 	}
 	System.out.println("ID -> " + id);
	List<HashMap<String,Object>> allCompanies = DefCompaniesVo.getInstance();
	List<HashMap<String,Object>> allCurrencies = DefCurrencyVo.getInstance();
	List<HashMap<String,Object>> allInvoiceTypes = DefTipFactVo.getInstance();
	List<HashMap<String,Object>> allRelTypes = DefRelComerVo.getInstance();
	List<HashMap<String,Object>> allVat = DefVATVo.getInstance();
	List<HashMap<String,Object>> allDocs = DefAttDocsVo.getInstance();
	List<HashMap<String,Object>> oneInvoiceHeader 				= null;
	List<HashMap<String,Object>> supplierInfo 					= null;
	List<HashMap<String,Object>> invoiceLinesByInvoiceNr 		= null;
	List<HashMap<String,Object>> invoiceStoreLinesByInvoiceNr 	= null;
	String vat = "";
	String stage = "";
	String currency = "";
	String tip_capex = "0";
	String initiativa = "0";
	String prj = "0";
	String getInvoiceOption = "-1";
	int storeId = 0;
	String companyId = "";
	String companyListingStoreLines = "";
	if (!id.equals("")) {
		oneInvoiceHeader 		= InvoiceHeader.getHeaderById(id, "invoice_header");
		companyId 				= oneInvoiceHeader.get(0).get("id_company").toString();
		companyListingStoreLines = oneInvoiceHeader.get(0).get("id_company").toString();
		supplierInfo 			= DefSupplier.getSupplierInfo(oneInvoiceHeader.get(0).get("id_supplier").toString());
		invoiceLinesByInvoiceNr = InvoiceHeader.getLinesById(id, "invoice_line");
		if (whatPage.equals(InvoiceStages.PAGE_CONTABILITATE_ASTEAPTA_LITIGII) || 
				whatPage.equals(InvoiceStages.PAGE_CONTABILITATE_ASTEAPTA_VALIDARE) || 
				whatPage.equals(InvoiceStages.PAGE_FACTURI_BLOCATE_CONTABILITATE ) ||
					whatPage.equals(InvoiceStages.KONTAN_PAGE) ||
					whatPage.equals(InvoiceStages.PAGE_CONTABILITATE_LISPA_DOC) ||
					whatPage.equals("exported_docs")) {
			invoiceStoreLinesByInvoiceNr = InvoiceHeader.getStoreLinesById(id, "invoice_store_line");
		}
		currency = oneInvoiceHeader.get(0).get("currency").toString();
		float valCuTva = Float.parseFloat(oneInvoiceHeader.get(0).get("total_with_vat_ron").toString());
		float valFaraTva = Float.parseFloat(oneInvoiceHeader.get(0).get("total_no_vat_ron").toString());
		stage = oneInvoiceHeader.get(0).get("stage").toString();
		if(stage.equals("1")) { vat = String.format("%.2f", (float)(Math.round(((valCuTva - valFaraTva) / valFaraTva) * 100)));
		} else { vat = oneInvoiceHeader.get(0).get("vat_code").toString(); }
		if (oneInvoiceHeader.get(0).get("GLN") != null) { storeId = DefStoreVo.getStoreIdbyGLN(oneInvoiceHeader.get(0).get("GLN").toString(),oneInvoiceHeader.get(0).get("id_company")); }
		try{ tip_capex = oneInvoiceHeader.get(0).get("tip_capex").toString(); } catch (Exception e){}
		try{ initiativa = oneInvoiceHeader.get(0).get("id_initiative").toString(); } catch (Exception e){}
		try{ prj = oneInvoiceHeader.get(0).get("id_new_prj").toString(); } catch (Exception e){}
		if (oneInvoiceHeader.get(0).get("invoice_supplier_option") != null){ getInvoiceOption = oneInvoiceHeader.get(0).get("invoice_supplier_option").toString();}
	}
%>
<portlet:actionURL name="deleteInvoice" var="deleteInvoiceURL" />
<portlet:actionURL name="addInvoice" var="submitURL" />
<portlet:renderURL var="portletURL"></portlet:renderURL>
<portlet:resourceURL id="ajaxURL" var="ajaxURL"/>
<portlet:renderURL var="backURL"><portlet:param name="mvcPath" value="/html/facturi/view.jsp"></portlet:param></portlet:renderURL>
<portlet:actionURL var="deleteURL" name="deleteFact"/>
<portlet:actionURL name="deleteInvoice" var="deleteInvoiceURL" />
<a href="<%=portletURL%>" class="btn btn-primary-red fr" id="go_inapoi" style="margin:0px">Inapoi</a>
<% if (!id.equals("")) {%>
	<% if(!isFromLipsaDoc && !whatPage.equals(InvoiceStages.KONTAN_PAGE) && !whatPage.equals(InvoiceStages.PAGE_CONTABILITATE_ASTEAPTA_VALIDARE)) {%>
		<aui:form id="deleteForm" style="display:none" action="<%=deleteURL%>"> <aui:input type="hidden" name="fact_id_delete" value="<%= id %>"/> <aui:input type="submit" name="sterge" id="sterge"/>
		</aui:form>
		<a class="btn btn-primary-red fr" style="margin:0px" onclick="alertDelete('<portlet:namespace />')">Sterge Factura</a>
	<% } %>
		<!-- data >= 23 atunci alfresco nou, altfel alfresco vechi -->
		<% DateFormat dateFormat = new  SimpleDateFormat("yyyy-MM-dd");
       	   Date invCreateDate = dateFormat.parse(oneInvoiceHeader.get(0).get("created").toString().substring(0, 10));
       	   Date dataReferinta = dateFormat.parse("2018-11-23");
       %>
		<% if(invCreateDate.before(dataReferinta) && oneInvoiceHeader.get(0).get("image") != null) { %>
		<a href="<%=ControllerUtils.alfrescoURLOld + oneInvoiceHeader.get(0).get("image") %>" <%--+ ControllerUtils.alfrescoDirectLoginOld  --%>
			class="btn btn-primary fr" style="margin-right:10px" target="_blank">Vizualizare Factura</a>
		<% } else if (invCreateDate.after(dataReferinta) && oneInvoiceHeader.get(0).get("cid") != null) { %>
			<a onclick="downloadPdfFunction('<%=oneInvoiceHeader.get(0).get("cid") %>')" <%--+ ControllerUtils.alfrescoDirectLoginOld  --%>
			class="btn btn-primary fr" style="margin-right:10px" target="_blank">Vizualizare Factura</a>
		<% } %>
	<% if(!isFromLipsaDoc && !whatPage.equals(InvoiceStages.KONTAN_PAGE) && !whatPage.equals(InvoiceStages.PAGE_CONTABILITATE_ASTEAPTA_VALIDARE)) { %>
		<aui:form id="sendChGen" style="display:none" action="<%=deleteInvoiceURL%>"> <aui:input type="hidden" name="fact_id" value="<%= id %>"/> <aui:input type="submit" name="update" id="update"/>
		</aui:form>
		<a class="btn btn-primary fr" style="margin:0px" onclick="sendToChGen('<portlet:namespace />')">Trimite factura in Ch. Gen.</a>
	<% } %>
<% } %>
<div class="clear"></div>
<liferay-ui:error message="insert_nok" key="insert_nok"/>
<liferay-ui:error message="update_nok" key="update_nok"/>
<aui:form action="<%=submitURL%>" method="post" id="ADD-invoice" name="ADD-invoice">
	<aui:input name="insertORupdate" id="insertORupdate" type="hidden" value="<%=id %>"/>
	<aui:input name="page" type="hidden" value="<%=whatPage%>"/>
	<aui:input name="currentStage" type="hidden" value="<%=stage%>"/>
	<aui:fieldset> <legend><strong>Factura</strong></legend>
		<aui:layout>
			<aui:column columnWidth="10" first="true">
				<label for="<portlet:namespace />inv_number" style="padding-top: 5px"><%=resmain.getString("nr-factura")%></label>
			</aui:column>
			<% if (id.equals("")) {%>
			<aui:column columnWidth="20">
				<aui:input type="text" id="inv_number" name="inv_number" value="" label="">
				<aui:validator name="required"  errorMessage="field-required"></aui:validator>
				</aui:input>
			</aui:column>
			<% } else { %>
			<aui:column columnWidth="20">
				<aui:input type="text" id="inv_number" name="inv_number" value="<%=oneInvoiceHeader.get(0).get(\"inv_number\") %>" label="">
				<aui:validator name="required"  errorMessage="field-required"></aui:validator>
				</aui:input>
			</aui:column>
			<% } %>
			<aui:column columnWidth="10">
				<label for="<portlet:namespace/>inv_date2" style="padding-top: 5px"><%=resmain.getString("datafactura")%></label>
			</aui:column>
			<% if (id.equals("")) {%>
			<aui:column columnWidth="20">
				<aui:input type="date" id="inv_date2" name="inv_date2" value="" placeholder="aaaa-ll-zz" label="">
				<aui:validator name="required"  errorMessage="field-required"></aui:validator>
				</aui:input>
			</aui:column>
			<% } else { %>
			<aui:column columnWidth="20">
				<aui:input type="date" id="inv_date2" name="inv_date2" value="<%=oneInvoiceHeader.get(0).get(\"inv_date\") %>" 
				placeholder="aaaa-ll-zz" label="">
				<aui:validator name="required"  errorMessage="field-required"></aui:validator>
				</aui:input>
			</aui:column>
			<% } %>
			<script type="text/javascript">
				var oldInvoiceDateValue = "";
				YUI({lang:'ro'}).use('aui-datepicker', function(Y) {
					oldInvoiceDateValue = $('#<portlet:namespace />inv_date2').val();
					new Y.DatePicker({ trigger : '#<portlet:namespace />inv_date2', mask : '%Y-%m-%d', popover : { zIndex : 1 }, calendar : { dateFormat : '%Y-%m-%d' },
						after: { selectionChange: function(event) {
				            	if (oldInvoiceDateValue != $('#<portlet:namespace />inv_date2').val()) {
				            		oldInvoiceDateValue = $('#<portlet:namespace />inv_date2').val();
				            		invoiceDateChanged($('#<portlet:namespace />inv_date2').val(), '<portlet:namespace />');
				            		setDueDate('<portlet:namespace/>');
				            	}
				            }
				        }
					});
				});
			</script>
			<aui:column columnWidth="10">
				<label for="<portlet:namespace />company"
					style="padding-top: 5px"><%=resmain.getString("societate")%></label>
			</aui:column>
			<% if (id.equals("")) {%>
			<aui:column columnWidth="20" id="societate2">
				<aui:select id="company" name="company" label="" required="true">
					<aui:option selected="selected" value=""><%=resmain.getString("societate")%></aui:option>
					<% for (int i = 0; i < allCompanies.size(); i++) { %>
						<% if (allCompanies.get(i).get("status").equals("A")) {%>
							<aui:option value="<%=allCompanies.get(i).get(\"id\")%>"><%=allCompanies.get(i).get("name")%></aui:option>
						<% } %>
					<% } %>
				</aui:select>
			</aui:column>
			<% } else { %>
			<aui:column columnWidth="20" id="societate2">
				<aui:select id="company" name="company" label="" required="true">
					<% for (int i = 0; i < allCompanies.size(); i++) { %>
						<% if (allCompanies.get(i).get("status").equals("A")) { %>
							<aui:option value="<%=allCompanies.get(i).get(\"id\")%>"><%=allCompanies.get(i).get("name")%></aui:option>
						<% } %>
					<% } %>
				</aui:select>
			</aui:column>
			<% } %>
		</aui:layout>
		<%if (!id.equals("")) { %>
		<% for (int i = 0; i < allCompanies.size(); i++) { %>
			<% if (allCompanies.get(i).get("status").equals("A")) { %>
				<% if (allCompanies.get(i).get("id").toString().equals(oneInvoiceHeader.get(0).get("id_company").toString())) { %>
				<script type="text/javascript">
					$(document).ready(function(){  
						$('#<portlet:namespace />company').val('<%=allCompanies.get(i).get("id")%>');
					});
				</script>
				<% } %>
			<% } %>
		<% } } %>
	</aui:fieldset>
	<aui:fieldset>
		<aui:layout>
			<aui:column columnWidth="10" first="true">
				<label for="<portlet:namespace />tip-rel-comerciala"
					style="padding-top: 5px"><%=resmain.getString("tip-rel-comerciala")%></label>
			</aui:column>
			<% if(id.equals("")) {%>
			<aui:column columnWidth="20">
				<aui:select id="tip-rel-comerciala" name="tip-rel-comerciala" label="" required="true">
					<aui:option selected="selected" value=""><%=resmain.getString("selecteaza-tipul")%></aui:option>
					<% for (int i = 0; i < allRelTypes.size(); i++) { %>
						<% if (allRelTypes.get(i).get("status").equals("A")) {%>
							<aui:option value="<%=allRelTypes.get(i).get(\"id\")%>"><%=allRelTypes.get(i).get("name")%></aui:option>
						<% } %>
					<% } %>
				</aui:select>
			</aui:column>
			<% } else { %>
			<aui:column columnWidth="20">
				<aui:select id="tip-rel-comerciala" name="tip-rel-comerciala" label="" required="true">
					<% for (int i = 0; i < allRelTypes.size(); i++) { %>
						<% if (allRelTypes.get(i).get("status").equals("A")) {%>
							<% if ( (oneInvoiceHeader.get(0).get("tip_rel_com") != null) && 
									allRelTypes.get(i).get("id").equals(oneInvoiceHeader.get(0).get("tip_rel_com").toString())) {%>
								<aui:option selected = "selected" value="<%=allRelTypes.get(i).get(\"id\")%>"><%=allRelTypes.get(i).get("name")%></aui:option>
							<% } else { %>
								<aui:option value="<%=allRelTypes.get(i).get(\"id\")%>"><%=allRelTypes.get(i).get("name")%></aui:option>
							<% } %>
						<% } %>
					<% } %>
				</aui:select>
			</aui:column>
			<% } %>
			<aui:column columnWidth="10" id = "label_contract_display">
				<label for="<portlet:namespace />nr-contract" style="padding-top: 5px"><%=resmain.getString("nr-contract")%></label>
			</aui:column>
			<% if(id.equals("")) {%>
			<aui:column columnWidth="20" id="numar_contract_display">
				<aui:input id="nr-contract" name="nr-contract" label="" value="0">
				<aui:validator name="required"  errorMessage="field-required"></aui:validator>
				</aui:input>
			</aui:column>
			<% } else { %>
				<aui:column columnWidth="20" id="numar_contract_display">
				<aui:input id="nr-contract" name="nr-contract" label="" value = "<%=oneInvoiceHeader.get(0).get(\"co_number\") %>">
				</aui:input>
			</aui:column>
			<% } %>
			<aui:column columnWidth="10" id="label_comanda_display">
				<label for="<portlet:namespace />nr-comanda" style="padding-top: 5px"><%=resmain.getString("nr-comanda")%></label>
			</aui:column>
			<% if(id.equals("")) {%>
			<aui:column columnWidth="20" id="numar_comanda_display">
				<aui:input id="nr-comanda" name="nr-comanda" label="" value="0">
				<aui:validator name="required"  errorMessage="field-required"></aui:validator>
				</aui:input>
			</aui:column>
			<% } else { %>
				<aui:column columnWidth="20" id="numar_comanda_display">
				<aui:input id="nr-comanda" name="nr-comanda" label="" value = "<%=oneInvoiceHeader.get(0).get(\"order_no\") %>">
				</aui:input>
			</aui:column>
			<% } %>
		</aui:layout>
	</aui:fieldset>
	<aui:fieldset>
		<aui:layout>
			<aui:column columnWidth="10" first="true">
				<label for="<portlet:namespace />tip-factura"
					style="padding-top: 5px"><%=resmain.getString("tip-factura")%></label>
			</aui:column>
			<% if(id.equals("")) {%>
			<aui:column columnWidth="20">
				<aui:select id="tip-factura" name="tip-factura" label="" required="true">
					<aui:option selected="selected" value=""><%=resmain.getString("tip-factura")%></aui:option>
					<%=DropDownUtils.generateSimpleDropdown(allInvoiceTypes, "id", "name", "0", resmain.getString("tip-factura")) %>
				</aui:select>
			</aui:column>
			<% } else { %>
				<aui:column columnWidth="20">
				<aui:select id="tip-factura" name="tip-factura" label="" required="true">
					<% for (int i = 0; i < allInvoiceTypes.size(); i++) { %>
						<% if (allInvoiceTypes.get(i).get("status").equals("A")) {%>
							<% if( (oneInvoiceHeader.get(0).get("tip_fact") != null) &&
									allInvoiceTypes.get(i).get("id").equals(oneInvoiceHeader.get(0).get("tip_fact").toString())) { %>
								<aui:option selected = "selected" value="<%=allInvoiceTypes.get(i).get(\"id\")%>"><%=allInvoiceTypes.get(i).get("name")%></aui:option>
							<% } else { %>
								<aui:option value="<%=allInvoiceTypes.get(i).get(\"id\")%>"><%=allInvoiceTypes.get(i).get("name")%></aui:option>						
							<% } %>
						<% } %>
					<% } %>
				</aui:select>
			</aui:column>
			<% } %>
			<aui:column columnWidth="10" id="nr-fact-baza2" style="display:none">
				<label for="<portlet:namespace />nr-fact-baza"
					style="padding-top: 5px"><%=resmain.getString("nr-fact-baza")%></label>
			</aui:column>
			<aui:column columnWidth="20" id="nr-fact-baza3" style="display:none">
				<aui:select id="nr-fact-baza" name="nr-fact-baza" label="">
				</aui:select>
			</aui:column>
			<aui:column columnWidth="10">
				<label for="<portlet:namespace />created"
					style="padding-top: 5px"><%=resmain.getString("datainregistrare")%></label>
			</aui:column>
			<% if(id.equals("")) { %>
			<aui:column columnWidth="20">
				<aui:input type="text" id="created" name="created" value="" placeholder="aaaa-ll-zz" label="" readonly="true">
				<aui:validator name="required"  errorMessage="field-required"></aui:validator>
				</aui:input>
			</aui:column>
			<% } else { %>
				<aui:column columnWidth="20">
				<aui:input type="text" id="created" name="created" value="<%=oneInvoiceHeader.get(0).get(\"created\") %>" 
				placeholder="aaaa-ll-zz" label="" readonly="true">
				<aui:validator name="required"  errorMessage="field-required"></aui:validator>
				</aui:input>
			</aui:column>
			<% } %>
		</aui:layout>
	</aui:fieldset>
	<%@ include file="/html/includes/supplier.jsp" %>
	<% if (!id.equals("")) { %>
	<script type="text/javascript"> 
		$(document).ready(function(){
			$('#<portlet:namespace />id_supplier').val('<%=oneInvoiceHeader.get(0).get("id_supplier").toString()%>');
			$('#<portlet:namespace />new_furnizor_id').val('<%=oneInvoiceHeader.get(0).get("id_supplier").toString()%>');
			$('#<portlet:namespace />vendor').val('<%=supplierInfo.get(0).get("name")%>');
			$('#<portlet:namespace />payment_term').val('<%=supplierInfo.get(0).get("payment_term")%>');
			$('#<portlet:namespace />cui').val('<%=(supplierInfo.get(0).get("cui") == null ? "" : supplierInfo.get(0).get("cui"))%>');
			$('#<portlet:namespace />cont-asociat').val('<%=(supplierInfo.get(0).get("bank_account") == null ? "" : supplierInfo.get(0).get("bank_account"))%>');
			$('#<portlet:namespace/>supplier_code').val('<%=(supplierInfo.get(0).get("supplier_code") == null ? "" : supplierInfo.get(0).get("supplier_code"))%>');
			$('#<portlet:namespace/>supplier_data').val('<%=(supplierInfo.get(0) == null ? "" : DatabaseConnectionManager.convertListToJson(supplierInfo).toJSONString())%>');
			var acc = $('#<portlet:namespace/>supplier_code').val();
			var kon_acc = <%=oneInvoiceHeader.get(0).get("kontan_acc").toString()%>;
			var invoiceOption = <%=getInvoiceOption%>;
			var supplierName = '<%=supplierInfo.get(0).get("name").toString()%>';
			setAssociatedKontanAccInvoice('<portlet:namespace/>', acc, kon_acc, supplierName, false);
			<%if (oneInvoiceHeader.get(0).get("invoice_supplier_option") != null) { %>
				var invoiceSupplierOptionValue = <%= oneInvoiceHeader.get(0).get("invoice_supplier_option") %>
				if (invoiceSupplierOptionValue == -1){
					var supplierType = oneInvoiceHeader.get(0).get("invoice_supplier_option");
					if (supplierType == "0"){ $('#<portlet:namespace/>intern_invoice_option').val("");
					} else if (supplierType == "1"){ $('#<portlet:namespace/>extern_invoice_option').val("");
					}
				} else if (invoiceSupplierOptionValue < 4){
					$('#<portlet:namespace/>supplier_type_invoice').val("1");
					$('#<portlet:namespace/>extern_invoice_option').val(invoiceSupplierOptionValue);
				} else {
					$('#<portlet:namespace/>supplier_type_invoice').val("0");
					$('#<portlet:namespace/>intern_invoice_option').val(invoiceSupplierOptionValue);
				}
				setInvoiceOption('<portlet:namespace/>');
			<% } else { %>
				setSupplierType('<portlet:namespace/>', acc);
				setInvoiceSupplierOptionValue('<portlet:namespace/>', invoiceOption);
			<% } %>
		});
	</script>
	<% } %>
	<aui:fieldset>
		<legend><strong><%=resmain.getString("info-factura")%></strong></legend>
		<aui:layout>
			<aui:column columnWidth="10" first="true">
				<label for="<portlet:namespace />datascadenta" style="padding-top: 5px"><%=resmain.getString("datascadenta")%></label>
			</aui:column>
			<% if(id.equals("")) { %>
			<aui:column columnWidth="20">
				<aui:input type="text" id="datascadenta" name="datascadenta" value="" placeholder="aaaa-ll-zz" label="">
					<aui:validator name="required"  errorMessage="field-required"></aui:validator>
				</aui:input>
			</aui:column>
			<% } else { %>
				<aui:column columnWidth="20">
				<aui:input type="text" id="datascadenta" name="datascadenta" value="<%=oneInvoiceHeader.get(0).get(\"due_date\") %>" placeholder="aaaa-ll-zz" label="">
					<aui:validator name="required"  errorMessage="field-required"></aui:validator>
				</aui:input>
			</aui:column>
			<% } %>
			<script type="text/javascript">
				YUI({lang: ''}).use('aui-datepicker', function(Y) {
					new Y.DatePicker({ trigger : '#<portlet:namespace />datascadenta', mask : '%Y-%m-%d', popover : { zIndex : 1 }, calendar : { dateFormat : '%Y-%m-%d' } });
				});
				$(document).ready(function(){ setDueDate('<portlet:namespace />'); });
			</script>
			<aui:column columnWidth="10" first="true">
				<label for="<portlet:namespace />currency" style="padding-top: 5px"><%=resmain.getString("moneda")%></label>
			</aui:column>
			<% if (id.equals("")) {%>
			<aui:column columnWidth="20">
				<aui:select id="currency" name="currency" label="" required="true" >
				<% for (int i = 0; i < allCurrencies.size(); i++) { %>
					<% if (allCurrencies.get(i).get("status").equals("A")) {%>
						<aui:option value="<%=allCurrencies.get(i).get(\"ref\")%>"><%=allCurrencies.get(i).get("ref")%></aui:option>
					<% } %>
				<% } %>
			</aui:select>
				<script type="text/javascript">
					$(document).ready(function(){  
					<% for (int i = 0; i < allCurrencies.size(); i++) { %>
						<% if (allCurrencies.get(i).get("status").equals("A")) {%>
							<% if (allCurrencies.get(i).get("ref").equals("RON")) { %>
							$('#<portlet:namespace />currency').val('<%=allCurrencies.get(i).get("ref")%>');
						<% } %>
					<% }  }%>
					});
				</script>
			</aui:column>
			<% } else { %>
				<aui:column columnWidth="20">
				<aui:select id="currency" name="currency" label="" required="true" >
				<% for (int i = 0; i < allCurrencies.size(); i++) { %>
					<% if (allCurrencies.get(i).get("status").equals("A")) {%>
						<% if (allCurrencies.get(i).get("ref").toString().equals(oneInvoiceHeader.get(0).get("currency").toString())) { %>
							<aui:option selected="selected" value="<%=allCurrencies.get(i).get(\"ref\")%>"><%=allCurrencies.get(i).get("ref")%></aui:option>
						<% }else { %> 
							<aui:option value="<%=allCurrencies.get(i).get(\"ref\")%>"><%=allCurrencies.get(i).get("ref")%></aui:option>
						<% } %>
					<% } %>
				<% } %>
			</aui:select>
			</aui:column>
			<% } %>
			<aui:column columnWidth="10">
				<label for="<portlet:namespace />rata-schimb" style="padding-top: 5px"><%=resmain.getString("rata-schimb")%></label>
			</aui:column>
			<% if (id.equals("")) { %>
			<aui:column columnWidth="20">
				<aui:input type="text" id="rata-schimb" name="rata-schimb" value="1" label="">
					<aui:validator name="required"  errorMessage="field-required"></aui:validator>
				</aui:input>
			</aui:column>
			<% } else { %>
				<aui:column columnWidth="20">
				<aui:input type="text" id="rata-schimb" name="rata-schimb" value="<%=oneInvoiceHeader.get(0).get(\"exchange_rate\")%>" label="">
					<aui:validator name="required"  errorMessage="field-required"></aui:validator>
				</aui:input>
			</aui:column>
			<% } %>
		</aui:layout>
	</aui:fieldset>
	<aui:fieldset>
		<aui:column columnWidth="10" first="true">
			<label for="<portlet:namespace />cota_tva" style="padding-top: 5px"><%=resmain.getString("cota-tva")%></label>
		</aui:column>
		<% if (id.equals("")) { %>
		<aui:column columnWidth="20">
			<aui:select id="cota_tva" name="cota_tva" label="" required="true">
				<aui:option selected="selected" value=""><%=resmain.getString("cota-tva")%></aui:option>
				<% for (int i = 0; i < allVat.size(); i++) { %>
					<% if (allVat.get(i).get("status").equals("A")) {%>
						<aui:option value="<%=allVat.get(i).get(\"ref\")%>"><%=allVat.get(i).get("ref")%>%</aui:option>
					<% } %>
				<% } %>
			</aui:select>
		</aui:column>
		<%} else { %>
		<aui:column columnWidth="20">
			<aui:select id="cota_tva" name="cota_tva" label="" required="true" autocomplete="off">
				<% for (int i = 0; i < allVat.size(); i++) { %>
					<% if (allVat.get(i).get("status").equals("A")) {%>
						<% if (allVat.get(i).get("ref").toString().equals(vat)) { %>
						<aui:option selected="true" value="<%=allVat.get(i).get(\"ref\")%>"><%=allVat.get(i).get("ref")%>%</aui:option>
						<% } else { %>
						<aui:option value="<%=allVat.get(i).get(\"ref\")%>"><%=allVat.get(i).get("ref")%>%</aui:option>
						<% } %>
					<% } %>
				<% } %>
			</aui:select>
		</aui:column>
		<% } %>
		<aui:column columnWidth="10" first="true">
			<label for="<portlet:namespace />procent_garantie"
				style="padding-top: 5px"><%=resmain.getString("procent_garantie")%></label>
		</aui:column>
		<% if (id.equals("")) { %>
		<aui:column columnWidth="20">
			<aui:input type="text" id="procent_garantie" name="procent_garantie" value="0" placeholder="x%" label="">
				<aui:validator name="required"  errorMessage="field-required"></aui:validator>
			</aui:input>
		</aui:column>
		<% } else { %>
			<aui:column columnWidth="20">
			<aui:input type="text" id="procent_garantie" name="procent_garantie" value="<%=(oneInvoiceHeader.get(0).get(\"warranty_proc\") == null ? \"0\" : oneInvoiceHeader.get(0).get(\"warranty_proc\"))%>" 
			placeholder="x%" label="">
				<aui:validator name="required"  errorMessage="field-required"></aui:validator>
			</aui:input>
		</aui:column>
		<% } %>
	</aui:fieldset>
<aui:fieldset id="valoareCurrency" style="display:none">
	<legend id="currency_label"></legend>
	<aui:layout>
		<aui:column columnWidth="10">
			<label for="<portlet:namespace />val_no_vat_curr"
				style="padding-top: 5px"><%=resmain.getString("val_fara_tva")%></label>
		</aui:column>
		<% if (id.equals("")) { %>
		<aui:column columnWidth="20">
			<aui:input cssClass="currencyCol" type="text" id="val_no_vat_curr" name="val_no_vat_curr" value="" label="">
			</aui:input>
		</aui:column>
		<% } else { %>
		<aui:column columnWidth="20">
			<aui:input cssClass="currencyCol" type="text" id="val_no_vat_curr" name="val_no_vat_curr" value="<%=oneInvoiceHeader.get(0).get(\"total_no_vat_curr\")%>" label="">
			</aui:input>
		</aui:column>
		<% } %>
		<aui:column columnWidth="10">
			<label for="<portlet:namespace />val_vat_curr" style="padding-top: 5px"><%=resmain.getString("val_tva")%></label>
		</aui:column>
		<% if (id.equals("")) { %>
		<aui:column columnWidth="20">
			<aui:input cssClass="currencyCol" type="text" id="val_vat_curr" name="val_vat_curr" value="" label="" >
			</aui:input>
		</aui:column>
		<% } else { %>
		<aui:column columnWidth="20">
			<aui:input cssClass="currencyCol" type="text" id="val_vat_curr" name="val_vat_curr" value="<%=oneInvoiceHeader.get(0).get(\"total_vat_curr\")%>" label="" >
			</aui:input>
		</aui:column>
		<% } %>
		<aui:column columnWidth="10">
			<label for="<portlet:namespace />val_with_vat_curr"
				style="padding-top: 5px"><%=resmain.getString("val_cu_tva")%></label>
		</aui:column>
		<% if (id.equals("")) { %>
		<aui:column columnWidth="20">
			<aui:input cssClass="currencyCol" type="text" id="val_with_vat_curr" name="val_with_vat_curr" value="" label="">
			</aui:input>
		</aui:column>
		<% } else { %>
		<aui:column columnWidth="20">
			<aui:input cssClass="currencyCol" type="text" id="val_with_vat_curr" name="val_with_vat_curr" value="<%=oneInvoiceHeader.get(0).get(\"total_with_vat_curr\")%>" label="">
			</aui:input>
		</aui:column>
		<% } %>	
	</aui:layout>
	</aui:fieldset>
	<aui:fieldset id="valoareRON">
		<legend><%=resmain.getString("valoare-ron")%></legend>
		<aui:layout>
			<aui:column columnWidth="10">
				<label for="<portlet:namespace />val_no_vat_ron"
					style="padding-top: 5px"><%=resmain.getString("val_fara_tva")%></label>
			</aui:column>
			<% if (id.equals("")) { %>
			<aui:column columnWidth="20">
				<aui:input cssClass="currencyCol" type="text" id="val_no_vat_ron" name="val_no_vat_ron" 
					value="" label="">
					<aui:validator name="required"  errorMessage="field-required"></aui:validator>
				</aui:input>
			</aui:column>
			<% } else { %>
			<aui:column columnWidth="20">
				<aui:input cssClass="currencyCol" type="text" id="val_no_vat_ron" name="val_no_vat_ron" 
					value="<%=oneInvoiceHeader.get(0).get(\"total_no_vat_ron\")%>" label="">
					<aui:validator name="required"  errorMessage="field-required"></aui:validator>
				</aui:input>
			</aui:column>
			<% } %>
			<aui:column columnWidth="10">
				<label for="<portlet:namespace />val_vat_ron" style="padding-top: 5px"><%=resmain.getString("val_tva")%></label>
			</aui:column>
			<% if (id.equals("")) { %>
			<aui:column columnWidth="20">
				<aui:input cssClass="currencyCol" type="text" id="val_vat_ron" name="val_vat_ron" value="" label="">
					<aui:validator name="required"  errorMessage="field-required"></aui:validator>
				</aui:input>
			</aui:column>
			<% } else { %>
			<aui:column columnWidth="20">
				<aui:input cssClass="currencyCol" type="text" id="val_vat_ron" name="val_vat_ron" value="<%=oneInvoiceHeader.get(0).get(\"total_vat_ron\")%>" label="">
					<aui:validator name="required"  errorMessage="field-required"></aui:validator>
				</aui:input>
			</aui:column>
			<% } %>
			<aui:column columnWidth="10">
				<label for="<portlet:namespace />val_with_vat_ron"
					style="padding-top: 5px"><%=resmain.getString("val_cu_tva")%></label>
			</aui:column>
			<% if (id.equals("")) { %>
			<aui:column columnWidth="20">
				<aui:input cssClass="currencyCol" type="text" id="val_with_vat_ron" name="val_with_vat_ron" value="" label="">
					<aui:validator name="required"  errorMessage="field-required"></aui:validator>
				</aui:input>
			</aui:column>	
			<% } else { %>	
			<aui:column columnWidth="20">
				<aui:input cssClass="currencyCol" type="text" id="val_with_vat_ron" name="val_with_vat_ron" value="<%=oneInvoiceHeader.get(0).get(\"total_with_vat_ron\")%>" label="">
					<aui:validator name="required"  errorMessage="field-required"></aui:validator>
				</aui:input>
			</aui:column>	
			<% } %>
		</aui:layout>
	</aui:fieldset>
    <aui:fieldset>
	    <legend><strong><%=resmain.getString("actiuni")%></strong></legend>  
		<aui:input name="blocat-la-plata" type="hidden" value="0"/> 
		<aui:layout>
  			<aui:column columnWidth="20">
				<label for="<portlet:namespace />fact-avans-asoc" style="padding-top: 5px">Avans asociat</label>
			</aui:column>
			<% if ( id.equals("") || oneInvoiceHeader.get(0).get("id_fact_avans").toString().equals("0")) { %>
			<aui:column columnWidth="10">
	             <aui:input label="Da" name="fact-avans-asoc" type="radio" value="1" inlineField="true" onchange="$('#assoc-invoice').show()"/>
	             <aui:input label="Nu" name="fact-avans-asoc" type="radio" value="0" inlineField="true" onchange="$('#assoc-invoice').hide()" checked="true"/>
			</aui:column>
			<% } else { %>
			<aui:column columnWidth="10">
	             <aui:input label="Da" name="fact-avans-asoc" type="radio" value="1" inlineField="true" onchange="$('#assoc-invoice').show()" checked="true"/>
	             <aui:input label="Nu" name="fact-avans-asoc" type="radio" value="0" inlineField="true" onchange="$('#assoc-invoice').hide()"/>
			</aui:column>
			<% } %>
	        <div id="assoc-invoice" <%if (id.equals("") || oneInvoiceHeader.get(0).get("id_fact_avans").toString().equals("0")) { %> style="display:none" <% } %>>
				<aui:column columnWidth="20">
					<label for="<portlet:namespace />nr-fact-avans-asoc" style="padding-top: 5px">Nr solicitare avans</label>
				</aui:column>
				<% if (id.equals("")) { %>
				<aui:column columnWidth="20">
					<aui:select id="nr-fact-avans-asoc" name="nr-fact-avans-asoc" label=""></aui:select>
				</aui:column>
				<% } else { %>
					<aui:column columnWidth="20">
					<aui:input type="text" id="nr-fact-avans-asoc" name="nr-fact-avans-asoc" value="<%=oneInvoiceHeader.get(0).get(\"id_fact_avans\") %>" label="">
					</aui:input>
				</aui:column>
				<% } %>
				<aui:column columnWidth="20">
					<a id ="go-to-advance-request" href="#" style="padding-top:5px;display:block">Vizualizare document</a>
				</aui:column>
			</div>
	    </aui:layout>
	    <br/>
		<aui:layout>
  			<aui:column columnWidth="20">
				<label for="<portlet:namespace />garantie-blocata" style="padding-top: 5px">Garantie blocata la plata</label>
			</aui:column>
			<% if (id.equals("") || oneInvoiceHeader.get(0).get("warranty_lock").toString().equals("N")) {%>
			<aui:column columnWidth="10">
	             <aui:input label="Da" name="garantie-blocata" type="radio" value="1" inlineField="true"/>
	             <aui:input label="Nu" name="garantie-blocata" type="radio" value="0" inlineField="true" checked="true"/>
			</aui:column>
			<% } else { %>
			<aui:column columnWidth="10">
	             <aui:input label="Da" name="garantie-blocata" type="radio" value="1" inlineField="true" checked="true"/>
	             <aui:input label="Nu" name="garantie-blocata" type="radio" value="0" inlineField="true"/>
			</aui:column>
			<% } %>
		</aui:layout> <br/>
		<aui:layout>
  			<aui:column columnWidth="20">
				<label for="<portlet:namespace />lipsa-doc" style="padding-top: 5px">Lipsa documente justificative</label>
			</aui:column>
			<% if(oneInvoiceHeader != null && oneInvoiceHeader.get(0).get("missing_docs") != null) { %>
			<% if(id.equals("") || oneInvoiceHeader.get(0).get("missing_docs").toString().equals("")) { %>
			<aui:column columnWidth="10">
	             <aui:input label="Da" id="lipsa" name="lipsa-doc" type="radio" value="1" inlineField="true" onchange="$('#lipsa-doc-true').show()"/>
	             <aui:input label="Nu" name="lipsa-doc" type="radio" value="0" inlineField="true" onchange="$('#lipsa-doc-true').hide()" checked="true"/>
			</aui:column>
			<% } else { %>
			<aui:column columnWidth="10">
	             <aui:input label="Da" id="lipsa" name="lipsa-doc" type="radio" value="1" inlineField="true" onchange="$('#lipsa-doc-true').show()" checked="true"/>
	             <aui:input label="Nu" name="lipsa-doc" type="radio" value="0" inlineField="true" onchange="$('#lipsa-doc-true').hide()" />
			</aui:column>
			<% } } else { %>
			<aui:column columnWidth="10">
	             <aui:input label="Da" id="lipsa" name="lipsa-doc" type="radio" value="1" inlineField="true" onchange="$('#lipsa-doc-true').show()" />
	             <aui:input label="Nu" name="lipsa-doc" type="radio" value="0" inlineField="true" onchange="$('#lipsa-doc-true').hide()" checked="true"/>
			</aui:column>
			<% } %>
			<div id="lipsa-doc-true" <%if (id.equals("") || 
					(oneInvoiceHeader.get(0).get("missing_docs") == null ) || 
					(oneInvoiceHeader.get(0).get("missing_docs").toString().equals(""))) { %>
						style="display:none" <% } %>>
				<aui:column columnWidth="20">
					<label for="<portlet:namespace />doc-lipsa" style="padding-top: 5px">Selectie document lipsa:</label>
				</aui:column>
				<aui:column columnWidth="20">
					<aui:select id="doc-lipsa" name="doc-lipsa" label="">
						<aui:option selected="selected" value=""><%=resmain.getString("doc_lipsa")%></aui:option>
						<% for (int i = 0; i < allDocs.size(); i++) { %>
							<% if (allDocs.get(i).get("status").equals("A")) {%>
								<aui:option value="<%=allDocs.get(i).get(\"name\")%>"><%=allDocs.get(i).get("name")%></aui:option>
							<% } %>
						<% } %>
					</aui:select>
					<div id="displayDPi">
					<% if(!id.equals("") && oneInvoiceHeader.get(0).get("missing_docs") != null) { %>
						<% for (int i = 0; i < allDocs.size(); i++) { %>
							<% if (allDocs.get(i).get("status").equals("A")) {%>
								<% if (oneInvoiceHeader.get(0).get("missing_docs").toString().contains(allDocs.get(i).get("name").toString())) { %>
								<a href="#"><%=allDocs.get(i).get("name")%></a><br/>
								<% } %>
							<% } %>
						<% } %>
					<% } %>
					</div>
				</aui:column>
				<% if(!id.equals("")) { %>
				<aui:input type="hidden" name = "missing_docs" id="missing_docs" value="<%=oneInvoiceHeader.get(0).get(\"missing_docs\")%>"></aui:input>
				<% } else { %>
				<aui:input type="hidden" name = "missing_docs" id="missing_docs" value="" />
				<% } %>
				<aui:column columnWidth="20">
					<aui:button name="addDoc" id="addDoc" cssClass="btn btn-primary" value="<%=resmain.getString(\"add_doc\")%>"></aui:button>
				</aui:column>
			</div>
		</aui:layout>
		<br/>
		<aui:layout>
	   		 <aui:column columnWidth="20">
				<label for="<portlet:namespace />marcheaza-fact-eronata" style="padding-top: 5px"><%=resmain.getString("marcheaza-fact-eronata")%></label>
			</aui:column>
			<% if (oneInvoiceHeader != null && oneInvoiceHeader.get(0).get("error_details") != null) { %>
			<% if ((!id.equals("") && oneInvoiceHeader.get(0).get("error_details").toString().equals("")) || id.equals("")) { %>
			<aui:column columnWidth="10">
				 <aui:input label="Da" name="marcheaza-fact-eronata"  type="radio" value="1" inlineField="true" onChange ="$('#eroare').show()" />
	             <aui:input label="Nu" name="marcheaza-fact-eronata"  type="radio" value="0" inlineField="true" checked="true" onChange ="$('#eroare').hide()" />
			</aui:column>
			<% } else { %>
			<aui:column columnWidth="10">
				 <aui:input label="Da" name="marcheaza-fact-eronata"  type="radio" value="1" inlineField="true" checked="true" onChange ="$('#eroare').show()" />
	             <aui:input label="Nu" name="marcheaza-fact-eronata"  type="radio" value="0" inlineField="true" onChange ="$('#eroare').hide()" />
			</aui:column>
			<% } } else { %>
			<aui:column columnWidth="10">
				 <aui:input label="Da" name="marcheaza-fact-eronata"  type="radio" value="1" inlineField="true" onChange ="$('#eroare').show()" />
	             <aui:input label="Nu" name="marcheaza-fact-eronata"  type="radio" value="0" inlineField="true" checked="true" onChange ="$('#eroare').hide()" />
			</aui:column>
			<% } %>
			<div id="eroare" <% if(id.equals("") || (oneInvoiceHeader.get(0).get("error_details") == null || oneInvoiceHeader.get(0).get("error_details").toString().equals(""))) { %>style="display:none"<%  } %>>
				<aui:column columnWidth="20">
					<label for="<portlet:namespace />detalii-eroare" style="padding-top: 5px"><%=resmain.getString("detalii-eroare")%></label>
				</aui:column>
			<% if (oneInvoiceHeader != null && oneInvoiceHeader.get(0).get("error_details") != null) { %>
				<% if ((!id.equals("") && oneInvoiceHeader.get(0).get("error_details").toString().equals("")) || id.equals("")) {%>
				<aui:column columnWidth="30">
					<aui:input type="textarea" id="detalii-eroare" name="detalii-eroare" value="" label="">
					</aui:input>
				</aui:column>
				<% } else { %>
				<aui:column columnWidth="30">
					<aui:input type="textarea" id="detalii-eroare" name="detalii-eroare" value="<%=oneInvoiceHeader.get(0).get(\"error_details\") %>" label="">
					</aui:input>
				</aui:column>
			<% } } else { %>
				<aui:column columnWidth="30">
					<aui:input type="textarea" id="detalii-eroare" name="detalii-eroare" value="" label="">
					</aui:input>
				</aui:column>
			<% } %>
			</div>
		</aui:layout>
		<aui:layout>
			<aui:column columnWidth="20">
				<label for="<portlet:namespace />filter_dpi_store" style="padding-top: 5px">Filtre DPI </label>
			</aui:column>
			<aui:column columnWidth="20">
				<aui:select name="filter_dpi_store" label="" id="filter_dpi_store">
					<%=DropDownUtils.generateDropDownWithTwoNameColumn(DefStore.getFullList(), "Magazin", "id", "store_id", "store_name") %>
				</aui:select>
			</aui:column>
			<aui:column columnWidth="20">
				<aui:select name="filter_dpi_prod" label="" id="filter_dpi_prod">
					<%=DropDownUtils.generateDropDownWithTwoNameColumn(DefProduct.getFullList(), "Produs", "code", "code", "name") %>
				</aui:select>
			</aui:column>
			<aui:column columnWidth="20">
				<aui:input name="filter_dpi_value" id="filter_dpi_value" label="" value="" placeholder="Valoare">
					<aui:validator name="number"></aui:validator>	
				</aui:input>
			</aui:column>
		</aui:layout>
		<aui:layout>
			<aui:column columnWidth="20">
				<label for="<portlet:namespace />alocare_dpi" style="padding-top: 5px">Alocare DPI pe toate liniile facturii</label>
			</aui:column>
			<aui:column columnWidth="15">
				<aui:select name="alocare_dpi" label="" id="alocare_dpi">
					<%=DropDownUtils.generateDropDownWithTwoNameColumn(DefDpis.getAllDpis(), "--------------", "id", "id", "dpi_date") %>
				</aui:select>
			</aui:column>
			<aui:column columnWidth="15">
				<a style="width:35%;" class="btn btn-primary-blue" onclick="setDpiAndCategCapex()">Aloca DPI</a>
			</aui:column>
			<aui:column columnWidth="20">
				<label for="<portlet:namespace />alocare_magazin" style="padding-top: 5px">Alocare magazin pe toate liniile facturii</label>
			</aui:column>
			<aui:column columnWidth="15">
				<aui:select name="alocare_magazin" label="" id="alocare_magazin">
					<%=DropDownUtils.generateDropDownWithTwoNameColumn(DefStore.getAllByCompanyForDropdown(companyId), "Magazin", "id", "store_id", "name") %>
				</aui:select>
			</aui:column>
			<aui:column columnWidth="15">
				<a style="width:35%;" class="btn btn-primary-blue" onclick="alocareMagazin()">Aloca magazin</a>
			</aui:column>
		</aui:layout>
		<aui:layout>
			<aui:column columnWidth="20">
				<label for="<portlet:namespace />alocare_raion" style="padding-top: 5px">Alocare raion pe toate liniile facturii</label>
			</aui:column>
			<aui:column columnWidth="15">
				<aui:select name="alocare_raion" label="" id="alocare_raion">
					<%=DropDownUtils.generateDropDownWithTwoNameColumn(DefAisles.getAllStoresAisles("326"), "Raion", "short_id", "short_id", "name") %>
				</aui:select>
			</aui:column>
			<aui:column columnWidth="15">
				<a style="width:35%;" class="btn btn-primary-blue" onclick="alocareRaion()">Aloca raion</a>
			</aui:column>
			<aui:column columnWidth="20">
				<label for="<portlet:namespace />alocare_gestiune" style="padding-top: 5px">Alocare gestiune pe toate liniile facturii</label>
			</aui:column>
			<aui:column columnWidth="15">
				<aui:select name="alocare_gestiune" label="" id="alocare_gestiune">
					<%=DropDownUtils.generateSimpleDropdown(DefCatGest.getAll(), "id", "ref", "0", "------------") %>
				</aui:select>
			</aui:column>
			<aui:column columnWidth="15">
				<a style="width:35%;" class="btn btn-primary-blue" onclick="alocareGestiune()">Aloca Gestiune</a>
			</aui:column>
		</aui:layout>
		<aui:layout>
			<aui:column columnWidth="20">
				<label for="<portlet:namespace />alocare_tipInreg" style="padding-top: 5px">Alocare tip inregistrare pe toate liniile facturii</label>
			</aui:column>
			<aui:column columnWidth="15">
				<aui:select name="alocare_tipInreg" label="" id="alocare_tipInreg">
					<%=DropDownUtils.generateSimpleDropdown(DefRegEquipVo.getInstance(), "id", "name", "0", "------------") %>
				</aui:select>
			</aui:column>
			<aui:column columnWidth="15">
				<a style="width:35%;" class="btn btn-primary-blue" onclick="alocaretipInreg()">Aloca tip inregistrare</a>
			</aui:column>
			<aui:column columnWidth="20">
				<label for="<portlet:namespace />alocare_tipInventar" style="padding-top: 5px">Alocare tip inventar pe toate liniile facturii</label>
			</aui:column>
			<aui:column columnWidth="15">
				<aui:select name="alocare_tipInventar" label="" id="alocare_tipInventar"> <aui:option value="0">------------</aui:option> <aui:option value="1">Individual</aui:option> <aui:option value="2">De grup</aui:option>
				</aui:select>
			</aui:column>
			<aui:column columnWidth="15">
				<a style="width:35%;" class="btn btn-primary-blue" onclick="alocaretipInventar()">Aloca tip inventar</a>
			</aui:column>
		</aui:layout>
	</aui:fieldset>
	<aui:layout  style="margin-top:0px">
		<aui:column last="true">
		</aui:column>
	</aui:layout>
	<aui:fieldset>
		<aui:layout>
			<%@include file="../includes/capex_details.jsp" %>
		</aui:layout>
	</aui:fieldset>
	<script type="text/javascript">
	function alocareMagazin() { alocaMagazinPeLinii($('#<portlet:namespace />alocare_magazin').val(), $('#<portlet:namespace/>company').val());
	}
	function alocareRaion() { alocaRaionPeLinii($('#<portlet:namespace />alocare_raion').val());
	}
	function alocareGestiune() { alocaGestiunePeLinii($('#<portlet:namespace />alocare_gestiune').val());
	}
	function alocaretipInreg() { alocaTipInregPeLinii($('#<portlet:namespace />alocare_tipInreg').val());
	}
	function alocaretipInventar() { alocaTipInventarPeLinii($('#<portlet:namespace />alocare_tipInventar').val());
	}
		$('#<portlet:namespace />filter_dpi_store').on('change', function(e){ ajaxFilterDpis('<portlet:namespace />', "filterDpis", '<%=ajaxURL%>'); });
		$('#<portlet:namespace />filter_dpi_prod').on('change', function(e){ ajaxFilterDpis('<portlet:namespace />', "filterDpis", '<%=ajaxURL%>'); });
		$('#<portlet:namespace />filter_dpi_value').on('change', function(e){ ajaxFilterDpis('<portlet:namespace />', "filterDpis", '<%=ajaxURL%>'); });
		function setDpiAndCategCapex(){
			var dpi = $('#<portlet:namespace />alocare_dpi').val();
			if (dpi != 0){ ajaxSetDpiAndCategCapex('<portlet:namespace />', "getDpiCategCapex", '<%=ajaxURL%>', dpi);
			} else { alert("Dpi-ul nu a fost selectat!"); }
		}
		$( window ).load(function() { calculeazaTvaPeStoreLines('<portlet:namespace/>'); });
		$(document).ready(function() {
			$('#<portlet:namespace />alocare_dpi').chosen();
			$('#<portlet:namespace />alocare_dpi').chosen();
			$('#<portlet:namespace />filter_dpi_store').chosen();
			$('#<portlet:namespace />filter_dpi_prod').chosen();
			var tip_capex = <%=tip_capex%>;
			if (tip_capex == 0){
				$('#<portlet:namespace />capex_diverse').prop('checked', true);
			} else if (tip_capex == 1){
				$('#<portlet:namespace />init_cent').prop('checked', true);
				$('#<portlet:namespace />lista_initiative').val('<%=initiativa%>');
				$('#<portlet:namespace/>lista_initiative').trigger("chosen:updated");
			} else {
				$('#<portlet:namespace />proiect_nou').prop('checked', true);
				$('#<portlet:namespace />project').val('<%=prj%>');
				$('#<portlet:namespace/>project').trigger("chosen:updated");
			}
		});
	</script>	
	<aui:layout  style="margin-top:10px"><%@ include file="add_fact_parts/alocare_factura.jsp" %></aui:layout>
	<aui:layout  style="margin-top:10px"><%@ include file="add_fact_parts/valori_factura.jsp" %></aui:layout>
	<aui:input type="hidden" name="datatable_lines" id="datatable_lines" value="" />
	<aui:input type="hidden" name="datatable_data" id="datatable_data" value="" />
	<% if(whatPage.equals(InvoiceStages.PAGE_CONTABILITATE_ASTEAPTA_VALIDARE)  || whatPage.equals(InvoiceStages.PAGE_FACTURI_NEALOCATE)  || whatPage.equals(InvoiceStages.PAGE_CONTABILITATE_LISPA_DOC)  || whatPage.equals("exported_docs")) { %>
		<a href="#" class="btn btn-primary fr validate_inv" style="margin-right:0px">Validare</a>
	<% } %>
	<div class="clear"></div>
</aui:form>
<aui:layout>
	<aui:fieldset><jsp:include page="/html/includes/popups/solicita_inregistrare_furnizor.jsp" /></aui:fieldset>
</aui:layout>
<script type="text/javascript">
function sendToChGen(namespace){
	if (confirm("Sunteti sigur/a ca doriti sa trimiteti factura curenta in Cheltuieli Generale?")){
		$('#' + namespace + 'update').click();
	}
}
$('#go-to-advance-request').click(function(e) {
	e.preventDefault();
	var link = "<%=Links.individualAdvanceRequestForAccounting%>";
	link = link.substring(link.indexOf("url")+4);
	link = link + "&id=" + $('#<portlet:namespace />nr-fact-avans-asoc').val();
	window.location.href = link;
});
$('#<portlet:namespace />vendor').change(function(e){
	var action = "getAdvanceRequests";
	var supplierName = e.target.value;
	var company = $('#<portlet:namespace />company').val();
	makeAjaxToPopulateDropdown(action, company, supplierName, '<%=ajaxURL%>', '<portlet:namespace/>');
});
$('#<portlet:namespace />company').change(function(e) {
		var action = "getAdvanceRequests";
		var company = e.target.value;
		var supplierName = $('#<portlet:namespace />vendor').val();
		$('#<portlet:namespace />new_furnizor_id').val("");
		$('#<portlet:namespace />supplier_code').val("");
		makeAjaxToPopulateDropdown(action, company, supplierName, '<%=ajaxURL%>', '<portlet:namespace/>');
});
$('#go_inapoi').click(function(e){
	makeAjaxToSetInProcessFlag('setFlag', '<%=ajaxURL.toString()%>', '<portlet:namespace/>', '<%=id%>', 0);
});
</script>