<%@page import="com.profluo.ecm.model.db.DatabaseConnectionManager"%>
<%@page import="com.profluo.ecm.model.db.DPIHeader"%>
<%@page import="com.profluo.ecm.flows.SupplierStages"%>
<%@page import="sun.text.SupplementaryCharacterData"%>
<%@page import="com.profluo.ecm.flows.AdvanceRequestStages"%>
<%@page import="com.profluo.ecm.controller.UserTeamIdUtils"%>
<%@page import="com.profluo.ecm.model.db.GenericDBStatements"%>
<%@page import="java.sql.SQLException"%>
<%@page import="com.profluo.ecm.model.db.DefSupplier"%>
<%@page import="com.profluo.ecm.model.db.InvoiceHeader"%>
<%@page import="com.profluo.ecm.model.db.DefProduct"%>
<%@page import="com.profluo.ecm.model.db.DefLot"%>
<%@page import="java.math.BigDecimal"%>
<%@page import="com.profluo.ecm.model.vo.DefVATVo"%>
<%@page import="com.profluo.ecm.model.vo.DefAdvReqTypeVo"%>
<%@page import="com.profluo.ecm.model.vo.DefRelComerVo"%>
<%@page import="com.profluo.ecm.model.vo.DefTipFactVo"%>
<%@page import="com.profluo.ecm.model.vo.DefLotVo" %>
<%@page import="com.profluo.ecm.model.vo.DefCategoryVo" %>
<%@page import="com.profluo.ecm.model.vo.DefProductVo" %>
<%@page import="com.profluo.ecm.model.vo.ListaInitVo" %>
<%@page import="com.profluo.ecm.model.vo.DefNewProjVo" %>
<%@page import="com.profluo.ecm.model.vo.DefStoreVo" %>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.List"%>
<%@page import="com.profluo.ecm.model.vo.DefCurrencyVo"%>
<%@page import="com.profluo.ecm.model.vo.DefCompaniesVo"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme"%>

<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui"%>
<%@ taglib uri="http://alloy.liferay.com/tld/aui" prefix="aui"%>
<%@ taglib uri="http://liferay.com/tld/util" prefix="liferay-util"%>
<%@ page import="java.util.ResourceBundle"%>
<%@ page import="java.util.Locale"%>

<liferay-theme:defineObjects />
<portlet:defineObjects />

<portlet:resourceURL id="ajaxSupplierURL" var="ajaxSupplierURL"/>

<%
	// retrieve all companies from DB
	List<HashMap<String,Object>> allCompanies = DefCompaniesVo.getInstance();
    // retrieve all stores from DB
    List<HashMap<String,Object>> allStores = DefStoreVo.getInstance();
	// retrieve all currencies from DB
	List<HashMap<String,Object>> allCurrencies = DefCurrencyVo.getInstance();
	// retrieve all invoice types from DB
	List<HashMap<String,Object>> allInvoiceTypes = DefTipFactVo.getInstance();
	// retrieve commercial relations from the database
	List<HashMap<String,Object>> allRelTypes = DefRelComerVo.getInstance();
	// retrieve commercial relations from the database
	List<HashMap<String,Object>> allReqTypes = DefAdvReqTypeVo.getInstance();
	//retrive all lots from DB
	List<HashMap<String,Object>> allLots = DefLot.getAll();
	//retrieve all category lots from DB
	List<HashMap<String,Object>> allCategoryLots = DefCategoryVo.getInstance();
	//retrieve all products from DB
	List<HashMap<String,Object>> allProducts = DefProductVo.getInstance();//.getFullList();
	//retrieve all vat values from DB 
	List<HashMap<String,Object>> allVat = DefVATVo.getInstance();
	
	//if the current user is a Store Director, the variable "storeId" will contain
	//the id of the store owned by the user
	String storeId = "";
	//if the current user is a Store Director, the variable "company_id" will contain
	//the id of the company that owns the store
	String company_id = "";
	String [] storeInfo = new String[2];
	String storeCode = "";
	storeInfo 	= UserTeamIdUtils.getStoreIndex(user, allStores, allCompanies);
	try {
		storeId 	= storeInfo[0];
		company_id 	= storeInfo[1];
		storeCode 	= storeInfo[2];
		//System.out.println(storeCode + " " + company_id + " " + storeId);
	} catch (Exception e) {}
%>

<portlet:renderURL var="backURL" >
	<portlet:param name="mvcPath" value="/html/requestadvanceapproval/view.jsp"></portlet:param>
</portlet:renderURL>

<%-- overlay solicita vizualizare DPI --%>
<jsp:include page="../requestadvanceapproval/tabel_view/solicita_vizualizare_dpi.jsp" />

<%--primul linie pentru Factura --%>
<aui:fieldset>
	<legend><strong>Informatii solicitare plata</strong></legend>
	<aui:layout>
		<aui:column columnWidth="10" first="true">
			<label for="<portlet:namespace />company" style="padding-top: 5px"><%=resmain.getString("societate")%></label>
		</aui:column>
		<aui:column columnWidth="20" >
		<%if (status.equals("ro")) { %> 
			<aui:select id="company" name="company" label="" disabled="true">
			<% for (int i = 0; i < allCompanies.size(); i++) { %>
				<% if (allCompanies.get(i).get("status").equals("A")) { %>
					<% if (allCompanies.get(i).get("id").toString().equals(oneRow.get(0).get("company").toString())) { %>
						<aui:option selected="selected"
							value="<%=allCompanies.get(i).get(\"id\")%>"><%=allCompanies.get(i).get("name")%></aui:option>
					<% }%> 
				<% } %>
			<% } %>
			</aui:select>
		<% } else { %>
			<aui:select id="company" name="company" label="" required="true" >
				<aui:option selected="selected" value=""><%=resmain.getString("societate")%></aui:option>
			<% for (int i = 0; i < allCompanies.size(); i++) { %>
				<% if (allCompanies.get(i).get("status").equals("A")) { %>
					<aui:option value="<%=allCompanies.get(i).get(\"id\")%>"><%=allCompanies.get(i).get("name")%></aui:option>
				<% } %>
			<% } %>
			</aui:select>
			<script type="text/javascript">
				$(document).ready(function(){  
					$('#<portlet:namespace />company').val('<%=company_id%>');
					$('#<portlet:namespace />company').trigger('change');
				});

				// leave just the stores that belong to selected company in the drowpdown on next row in the form
				$('#<portlet:namespace/>company').change(function(e) {
					var selectedCo = $('#<portlet:namespace/>company').val();
					if (selectedCo == '') {
						for (var i = 0; i < 5; i++) {
								$('.company-options-' + i).show();
						}
					} else {
						$('#<portlet:namespace/>mag_dep').val('');
						for (var i = 0; i < 5; i++) {
							if (i == selectedCo) {
								$('.company-options-' + i).show();
							} else {
								$('.company-options-' + i).hide();
							}
						}
					}
				});
			</script>
		<% } %>
		</aui:column>
		<aui:column columnWidth="10">
			<label for="<portlet:namespace/>inv_date" style="padding-top: 5px"><%=resmain.getString("datasolicitare")%>: </label>
		</aui:column>
		<aui:column columnWidth="20">
		 <%if (status.equals("ro")) { %> 	 	
			<aui:input type="date" id="inv_date" name="inv_date" value="<%=oneRow.get(0).get(\"inv_date\").toString() %>" placeholder="aaaa-ll-zz" readonly="true" label=""></aui:input>		
		<% }else { %>
			<aui:input type="date" id="inv_date" name="inv_date" value="" readonly="true" placeholder="aaaa-ll-zz" label=""></aui:input>
		<% } %>
		</aui:column>
		
		<aui:column columnWidth="10">
			<label for="<portlet:namespace />tip-rel-comerciala" style="padding-top: 5px"><%=resmain.getString("tip-rel-comerciala")%></label>
		</aui:column>
		<aui:column columnWidth="20">
		<% if (status.equals("ro")) {%>
			<aui:select id="tip-rel-comerciala" name="tip-rel-comerciala" label=""   disabled="true">
				<% for (int i = 0; i < allRelTypes.size(); i++) { %>
					<% if (allRelTypes.get(i).get("status").equals("A")) { %>
						<% if (allRelTypes.get(i).get("id").toString().equals(oneRow.get(0).get("tip_rel_comerciala").toString())) { %>
						<aui:option selected="selected" value="<%=allRelTypes.get(i).get(\"id\")%>"><%=allRelTypes.get(i).get("name")%></aui:option>
						<% } %>
					<% } %>
				<% } %>
			</aui:select>
		<% }else { %>
			<aui:select id="tip-rel-comerciala" name="tip-rel-comerciala" label="" required="true" >
				<aui:option selected="selected" value=""><%=resmain.getString("selecteaza-tipul")%></aui:option>
				<% for (int i = 0; i < allRelTypes.size(); i++) { %>
					<% if (allRelTypes.get(i).get("status").equals("A")) {%>
						<aui:option value="<%=allRelTypes.get(i).get(\"id\")%>"><%=allRelTypes.get(i).get("name")%></aui:option>
					<% } %>
				<% } %>
			</aui:select>
		<% } %>
		</aui:column>
	</aui:layout>
</aui:fieldset>
<%--a doua linie pentru factura --%>
<aui:fieldset>
	<aui:layout>
		<aui:column columnWidth="10" first="true">
			<label style="padding-top:5px" for="<portlet:namespace />mag_dep"><%=resmain.getString("mag_dep")%></label>
		</aui:column>
		<aui:column columnWidth="20">
		<% if (status.equals("ro")) {%> 
			<aui:select id="mag_dep" name="mag_dep" label=""  disabled="true">
				<%for (int i = 0; i < allStores.size(); i++) {%>
					<% if (allStores.get(i).get("status").equals("A")) { %>
						<% if (allStores.get(i).get("id").toString().equals(oneRow.get(0).get("mag_dep").toString())) { %>
							<aui:option selected="selected" value="<%=allStores.get(i).get(\"id\")%>"><%=allStores.get(i).get("store_name")%></aui:option>
						<% } %>
				<% } } %>
			</aui:select>
		<% } else { %>
			<aui:select id="mag_dep" name="mag_dep" label="" required="true" >
				<aui:option selected="selected" value=""><%=resmain.getString("store")%></aui:option>
				<% for (int i = 0; i < allStores.size(); i++) { %>
					<%if (allStores.get(i).get("status").equals("A")) { %>
						<option value="<%=allStores.get(i).get("id")%>" class="company-options-<%=allStores.get(i).get("company_id")%>">
							<%=allStores.get(i).get("store_name") %>
						</option>
					<% } %>
				<% } %>
			</aui:select>
			<script type="text/javascript">
				$(document).ready(function(){  
					$('#<portlet:namespace />mag_dep').val('<%=storeId%>');
				});
				$('#<portlet:namespace />mag_dep').change(function(){
					updateDpiListAjax('<portlet:namespace/>', '<%=ajaxURL%>', $('#<portlet:namespace />mag_dep').val());
				});
			</script>
		<% } %>
		</aui:column>
		<aui:column columnWidth="10">
			<label for="<portlet:namespace />datascadenta" style="padding-top: 5px"><%=resmain.getString("datascadenta")%></label>
		</aui:column>
		<aui:column columnWidth="20">
		<% if (status.equals("ro")) {%>
			<aui:input type="text" id="datascadenta" name="datascadenta" value="<%=oneRow.get(0).get(\"datascadenta\").toString() %>" label="" readonly="true"></aui:input>
		<% }else { %>
			<aui:input type="text" id="datascadenta" name="datascadenta" value="" placeholder="aaaa-ll-zz" label="">
				<aui:validator name="required"  errorMessage="field-required"></aui:validator>
			</aui:input>
		<% } %>
		</aui:column>
		<script type="text/javascript">
			YUI().use('aui-datepicker', function(Y) {
				new Y.DatePicker({
					trigger : '#<portlet:namespace />datascadenta',
					mask : '%Y-%m-%d',
					popover : {
						zIndex : 1
					},
					calendar : {
						dateFormat : '%Y-%m-%d'
					}
				});
			});
		</script>
		<aui:column columnWidth="10">
		<% if (status.equals("ro")) {%> 
			<label for="<portlet:namespace />nr"
				style="padding-top: 5px"><%=resmain.getString("nr_cerere_avans")%></label>
		<% } %>	
		</aui:column>
		<aui:column columnWidth="20">
		<% if (status.equals("ro")) {%> 
			<aui:input type="text" id="nr" name="nr" value="<%= idSolicitarePlata %>" label="" readonly="true"></aui:input>
		<% } %>
		</aui:column>
	</aui:layout>
</aui:fieldset>
<%-- a treia linie din sectiunea factura --%>
<aui:fieldset>
	<aui:layout>
		<aui:column columnWidth="10" first="true">
			<label for="<portlet:namespace />currency" style="padding-top: 5px"><%=resmain.getString("moneda")%></label>
		</aui:column>
		<aui:column columnWidth="20">
		<% if (status.equals("ro")) {%>
			<aui:select id="currency" name="currency" label="" readonly="true">
				<% for (int i = 0; i < allCurrencies.size(); i++) { %>
					<% if (allCurrencies.get(i).get("status").equals("A")) {%>
						<% if (allCurrencies.get(i).get("ref").toString().equals(oneRow.get(0).get("moneda").toString())) { %>
							<aui:option selected="selected" value="<%=allCurrencies.get(i).get(\"ref\")%>"><%=allCurrencies.get(i).get("ref")%></aui:option>
					<% } %>
				<% } } %>
			</aui:select>
		<% }else { %>
			<aui:select id="currency" name="currency" label="" required="true" >
				<% for (int i = 0; i < allCurrencies.size(); i++) { %>
					<% if (allCurrencies.get(i).get("status").equals("A")) {%>
						<% if (allCurrencies.get(i).get("ref").toString().equals("RON")) { %>
							<aui:option selected="selected"
								value="<%=allCurrencies.get(i).get(\"ref\")%>"><%=allCurrencies.get(i).get("ref")%></aui:option>
						<% } else { %> 
							<aui:option value="<%=allCurrencies.get(i).get(\"ref\")%>"><%=allCurrencies.get(i).get("ref")%></aui:option>
						<% } %>
					<% } %>
				<% } %>
			</aui:select>
		<% } %>
		</aui:column>
		<aui:column columnWidth="10">
			<label for="<portlet:namespace />rata-schimb"
				style="padding-top: 5px"><%=resmain.getString("rata-schimb")%></label>
		</aui:column>
		<aui:column columnWidth="20">
		<% if (status.equals("ro")) {%>
			<aui:input type="text" id="rata-schimb" name="rata-schimb" 
				value="<%=oneRow.get(0).get(\"rata_schimb\").toString() %>" readonly="true" label=""></aui:input>
		<% }else { %>
			<aui:input type="text" id="rata-schimb" name="rata-schimb" value="1" label=""> 
				<aui:validator name="required"  errorMessage="field-required"></aui:validator>
			</aui:input>
		<% } %>
		</aui:column>
		<aui:column columnWidth="10">
			<label for="<portlet:namespace />cota_tva" style="padding-top: 5px"><%=resmain.getString("cota-tva")%></label>
		</aui:column>
		<aui:column columnWidth="20">
		<% if (status.equals("ro")) {%>
			<aui:select id="cota_tva" name="cota_tva" label="" disabled="true">
				<%-- SHOULD BE OK on integer values --%>
				<% int cota = (int)(100 * (Float.valueOf(((BigDecimal)(oneRow.get(0).get("val_vat_ron"))).floatValue()))/(Float.valueOf(((BigDecimal)(oneRow.get(0).get("val_no_vat_ron"))).floatValue())));
				for (int i = 0; i < allVat.size(); i++) { %>
					<% if (allVat.get(i).get("status").equals("A")) { %>
						<% if(allVat.get(i).get("ref").toString().equals(cota+".00")) { %>
						<aui:option selected="selected" value="<%=allVat.get(i).get(\"ref\")%>"><%=allVat.get(i).get("ref")%>%</aui:option>
					<% } %>
				<% } } %>
			</aui:select>
		<% } else { %>
			<aui:select id="cota_tva" name="cota_tva" label="" required="true">
				<aui:option selected="selected" value=""><%=resmain.getString("cota-tva")%></aui:option>
				<% for (int i = 0; i < allVat.size(); i++) { %>
					<% if (allVat.get(i).get("status").equals("A")) {%>
						<aui:option value="<%=allVat.get(i).get(\"ref\")%>"><%=allVat.get(i).get("ref")%>%</aui:option>
					<% } %>
				<% } %>
			</aui:select>
		<% } %>
		</aui:column>
	</aui:layout>
</aui:fieldset>

<aui:fieldset>
	<aui:layout>
	<% if (whereAmI.equals("validate")) { %>
		<aui:column columnWidth="10" first="true">
			<label for="<portlet:namespace />cont_asociat_furn" style="padding-top: 5px"><%=resmain.getString("cont_asociat_furn")%>:
			</label>
		</aui:column>
		<aui:column columnWidth="20">
			<aui:input id="cont_asociat_furn" name="cont_asociat_furn" value="" label="" >
				<aui:validator name="required" errorMessage="field-required"></aui:validator>
			</aui:input>
		</aui:column>
		<% } %>
	</aui:layout>
</aui:fieldset>


<%--calculare valoare Currency --%>
<aui:fieldset id="valoareCurrency" style="display:none">
	<legend id="currency_label"><%=resmain.getString("valoare-usd")%></legend>
	<aui:layout>
		<aui:column columnWidth="10" first="true">
			<label for="<portlet:namespace />val_no_vat_curr" style="padding-top: 5px"><%=resmain.getString("val_fara_tva")%></label>
		</aui:column>
		<aui:column columnWidth="20">
		<% if (status.equals("ro") && oneRow != null && !oneRow.get(0).get("moneda").toString().equals("RON")) {%>
			<aui:input cssClass="currencyCol" type="text" id="val_no_vat_curr" name="val_no_vat_curr" readonly="true"
				value="<%=oneRow.get(0).get(\"val_no_vat_curr\").toString() %>" label=""></aui:input>
		<% }else { %>
			<aui:input cssClass="currencyCol" type="text" id="val_no_vat_curr" name="val_no_vat_curr" value="" label="">
				<%--<aui:validator name="required"  errorMessage="field-required"></aui:validator> --%>
			</aui:input>
		<% } %>
		</aui:column>
		<aui:column columnWidth="10">
			<label for="<portlet:namespace />val_vat_curr" style="padding-top: 5px"><%=resmain.getString("val_tva")%></label>
		</aui:column>
		<aui:column columnWidth="20">
			<% if (status.equals("ro") && oneRow != null && !oneRow.get(0).get("moneda").toString().equals("RON")) {%>
				<aui:input cssClass="currencyCol" type="text" id="val_vat_curr" name="val_vat_curr" value="<%=oneRow.get(0).get(\"val_vat_curr\").toString() %>" label=""   readonly="true"></aui:input>
			<% }else { %>
			<aui:input cssClass="currencyCol" type="text" id="val_vat_curr" name="val_vat_curr" value="" label="" >
				<%--	<aui:validator name="required"  errorMessage="field-required"></aui:validator> --%>
				</aui:input>
			<% } %>
		</aui:column>

		<aui:column columnWidth="10">
			<label for="<portlet:namespace />val_with_vat_curr"
				style="padding-top: 5px"><%=resmain.getString("val_cu_tva")%></label>
		</aui:column>
		<aui:column columnWidth="20">
		<% if (status.equals("ro") && oneRow != null && !oneRow.get(0).get("moneda").toString().equals("RON")) {%>
			<aui:input cssClass="currencyCol" type="text" id="val_with_vat_curr" name="val_with_vat_curr" value="<%=oneRow.get(0).get(\"val_with_vat_curr\").toString() %>" label=""   readonly="true"></aui:input>
		<% }else { %>
			<aui:input cssClass="currencyCol" type="text" id="val_with_vat_curr" name="val_with_vat_curr" value="" label="">
<%--	<aui:validator name="required"  errorMessage="field-required"></aui:validator> --%>			</aui:input>
		<% } %>
		</aui:column>		
	</aui:layout>
</aui:fieldset>

<%--calculare valoare Ron --%>
<aui:fieldset id="valoareRON">
	<legend><%=resmain.getString("valoare-ron")%></legend>
	<aui:layout>
		<aui:column columnWidth="10" first="true">
			<label for="<portlet:namespace />val_no_vat_ron"
				style="padding-top: 5px"><%=resmain.getString("val_fara_tva")%></label>
		</aui:column>
		<aui:column columnWidth="20">
		<% if (status.equals("ro")) {%>
			<aui:input cssClass="currencyCol" type="text" id="val_no_vat_ron" name="val_no_vat_ron" readonly="true" value="<%=oneRow.get(0).get(\"val_no_vat_ron\").toString() %>" label=""></aui:input>
		<% }else { %>
			<aui:input cssClass="currencyCol" type="text" id="val_no_vat_ron" name="val_no_vat_ron" value="" label="">
				<aui:validator name="required"  errorMessage="field-required"></aui:validator>
			</aui:input>
		<% } %>
		</aui:column>
		<aui:column columnWidth="10">
			<label for="<portlet:namespace />val_vat_ron" style="padding-top: 5px"><%=resmain.getString("val_tva")%></label>
		</aui:column>
		<aui:column columnWidth="20">
		<% if (status.equals("ro")) {%>
			<aui:input cssClass="currencyCol" type="text" id="val_vat_ron" name="val_vat_ron" value="<%=oneRow.get(0).get(\"val_vat_ron\").toString() %>" label=""   readonly="true"></aui:input>
		<% }else { %>
		<aui:input cssClass="currencyCol" type="text" id="val_vat_ron" name="val_vat_ron" value="" label="" >
				<aui:validator name="required"  errorMessage="field-required"></aui:validator>
			</aui:input>
		<% } %>
		</aui:column>

		<aui:column columnWidth="10">
			<label for="<portlet:namespace />val_with_vat_ron"
				style="padding-top: 5px"><%=resmain.getString("val_cu_tva")%></label>
		</aui:column>
		<aui:column columnWidth="20">
		<% if (status.equals("ro")) {%>
			<aui:input cssClass="currencyCol" type="text" id="val_with_vat_ron" name="val_with_vat_ron" value="<%=oneRow.get(0).get(\"val_with_vat_ron\").toString() %>" label=""  readonly="true"></aui:input>
		<% }else { %>
			<aui:input cssClass="currencyCol" type="text" id="val_with_vat_ron" name="val_with_vat_ron" value="" label="">
				<aui:validator name="required"  errorMessage="field-required"></aui:validator>
			</aui:input>
		<% } %>
		</aui:column>		
	</aui:layout>
</aui:fieldset>

<script type="text/javascript">
$(document).ready(function() {
	$('#<portlet:namespace />val_no_vat_ron').autoNumeric('init', {aSep: ',', mDec: 4 });
	$('#<portlet:namespace />val_vat_ron').autoNumeric('init', {aSep: ',', mDec: 4 });
	$('#<portlet:namespace />val_with_vat_ron').autoNumeric('init', {aSep: ',', mDec: 4 });
	$('#<portlet:namespace />val_no_vat_curr').autoNumeric('init', {aSep: ',', mDec: 4 });
	$('#<portlet:namespace />val_vat_curr').autoNumeric('init', {aSep: ',', mDec: 4 });
	$('#<portlet:namespace />val_with_vat_curr').autoNumeric('init', {aSep: ',', mDec: 4 });
});

$('#<portlet:namespace />val_vat_ron').change(function(){
	$('#<portlet:namespace />val_vat_ron').autoNumeric('update', {aSep: ',', mDec: 4 });
});
$('#<portlet:namespace />val_with_vat_ron').change(function(){
	$('#<portlet:namespace />val_with_vat_ron').autoNumeric('update', {aSep: ',', mDec: 4 });
});
$('#<portlet:namespace />val_no_vat_curr').change(function(){
	$('#<portlet:namespace />val_no_vat_curr').autoNumeric('update', {aSep: ',', mDec: 4 });
});
$('#<portlet:namespace />val_vat_curr').change(function(){
	$('#<portlet:namespace />val_vat_curr').autoNumeric('update', {aSep: ',', mDec: 4 });
});
$('#<portlet:namespace />val_with_vat_curr').change(function(){
	$('#<portlet:namespace />val_with_vat_curr').autoNumeric('update', {aSep: ',', mDec: 4 });
});
</script>

<%-- adaugare sectiune furnizor --%>
<%--
<aui:fieldset>
	<legend><strong><%=resmain.getString("furnizor")%></strong></legend>
	<aui:layout>
		<aui:column columnWidth="10" first="true">
			<label for="<portlet:namespace />furnizor" style="padding-top: 5px"><%=resmain.getString("furnizor")%>:
			</label>
		</aui:column>
		<aui:column columnWidth="20">
		<aui:input type="hidden" id="id_supplier" name="id_supplier" label=""></aui:input>
		<% if (status.equals("ro")) {%>
			<aui:input id="vendor2" name="furnizor" value="<%=oneRow.get(0).get(\"furnizor\").toString() %>" label=""  readonly="true"></aui:input>
		<% } else { %>
			<aui:input id="vendor2" name="furnizor" label="">
				<aui:validator name="required"  errorMessage="field-required"></aui:validator>
			</aui:input>
		<% } %>
		</aui:column>
		<aui:column columnWidth="10">
			<label for="<portlet:namespace />cui" style="padding-top: 5px"><%=resmain.getString("cui")%>:
			</label>
		</aui:column>
		<aui:column columnWidth="20">
		<% if (status.equals("ro")) {%>
			<aui:input id="cui" name="cui" label=""  value="<%=oneRow.get(0).get(\"cui\").toString() %>" readonly="true"></aui:input>
		<% } else { %>
			<aui:input id="cui" name="cui" label="">
				<aui:validator name="required"  errorMessage="field-required"></aui:validator>
			</aui:input>
		<% } %>
		</aui:column>
		<aui:column columnWidth="10">
			<label for="<portlet:namespace />cont-asociat"
				style="padding-top: 5px"><%=resmain.getString("cont-asociat")%>:
			</label>
		</aui:column>
		<aui:column columnWidth="20">
		 <% if (status.equals("ro")) {%>
			<aui:input id="cont-asociat" name="cont-asociat" label=""  value="<%=oneRow.get(0).get(\"cont_asociat\").toString() %>" readonly="true"></aui:input>
		<% } else { %>
			<aui:input id="cont-asociat" name="cont-asociat" label="">
			<aui:validator name="required"  errorMessage="field-required"></aui:validator>
			</aui:input>
		<% } %>
		</aui:column>
	</aui:layout>
	<aui:layout style="display:none" id="loccked_supplier">
		<a><font color="#FF0000">Atentie, furnizorul este blocat in Kontan</font></a>
	</aui:layout>
	<%if (!status.equals("ro")) {%>
	<aui:layout>
		<aui:column columnWidth="100" first="true">
			<a class="btn btn-primary" style="margin:0px" id="showoverlay">Solicita inregistrare furnizor</a>
			<div class="clear"></div>
		</aui:column>
	</aui:layout>
	<% } %>
	<script type="text/javascript">
		YUI().use("overlay", function(Y) {
			var overlay = new Y.Overlay({
			    srcNode:"#overlay",
			    width:"900px",
			    align: {
			        node:"#showoverlay",
			        points:["tl", "tr"]
			    }
			});
			overlay.render();
			$('#overlay').hide();
			
		    Y.on("click", function() {
		    	$('#overlay').show();
		    	return;
		    }, "#showoverlay");
		});
		
		YUI().use('autocomplete-list',"datasource-io",'aui-base','aui-io-request','autocomplete-filters','autocomplete-highlighters',
				function (A) {
				var contactSearchDS = new A.DataSource.IO({source: '<%=ajaxSupplierURL.toString()%>'});
				
				var contactSearchFormatter = function (query, results) {
					return A.Array.map(results, function (result) {
						return '<strong>'+result.raw.name+'</strong><br/><b>' + result.raw.supplier_code + '</b> - CUI: ' + result.raw.cui;
					});
				};
				
				var testData;
				var contactSearchInput = new A.AutoCompleteList({
					allowBrowserAutocomplete: 'false',
					resultHighlighter: 'phraseMatch',
					activateFirstItem: 'false',
					resultTextLocator: 'name',
					inputNode: '#<portlet:namespace/>vendor2',
					render: 'true',
					width: '300px',
			        resultFormatter: contactSearchFormatter,
					source: function(){
							var inputValue = A.one("#<portlet:namespace />vendor2").get('value');
							var myAjaxRequest = A.io.request('<%=ajaxSupplierURL.toString()%>',{
									dataType: 'json',
									method:'POST',
									data:{
										 <portlet:namespace/>action: 'findSupplier',
										 <portlet:namespace/>keywords: $('#<portlet:namespace />vendor2').val()
										},
									autoLoad:false,
									sync:false,
									on: {
									success:function(){
										var data = this.get('responseData');
										testData = data;
									}}
							});
							myAjaxRequest.start();
							return testData;
					},	
		 			on: {
						select: function(event) {
							// after user selects an option from the list
							var result = event.result.raw;
							A.one('#<portlet:namespace/>id_supplier').val(result.id);
							A.one('#<portlet:namespace/>cui').val(result.cui);
							A.one('#<portlet:namespace/>cont-asociat').val(result.bank_account);
							if (result.blocked == 0) {
								$('#loccked_supplier').hide();
							} else {
								$('#loccked_supplier').show();
							}
						}
					}
				});	
			});

	</script>
</aui:fieldset>
--%>
	<%-- adaugare sectiune furnizor --%>
	<jsp:include page="/html/includes/supplier.jsp" />
	
	<% if (!idSolicitarePlata.equals("")) { %>
	<script type="text/javascript">
		$('#<portlet:namespace />id_supplier').val('<%=oneRow.get(0).get("id_supplier").toString()%>');
		<% if (status.equals("ro")) {%>
		<% List<HashMap<String,Object>> supplierInfo = DefSupplier.getSupplierInfo(oneRow.get(0).get("id_supplier").toString());%>
		$('#<portlet:namespace />vendor').val('<%=supplierInfo.get(0).get("name")%>');
		$('#<portlet:namespace />vendor').prop('readonly', true);
		$('#<portlet:namespace />cui').val('<%=(supplierInfo.get(0).get("cui") == null ? "" : supplierInfo.get(0).get("cui"))%>');
		$('#<portlet:namespace />cui').prop('readonly', true);
		$('#<portlet:namespace />cont-asociat').val('<%=(supplierInfo.get(0).get("bank_account") == null ? "" : supplierInfo.get(0).get("bank_account"))%>');
		$('#<portlet:namespace />cont-asociat').prop('readonly', true);
		$('#<portlet:namespace/>supplier_code').val('<%=(supplierInfo.get(0).get("supplier_code") == null ? "" : supplierInfo.get(0).get("supplier_code"))%>');
		$('#<portlet:namespace/>supplier_data').val('<%=(supplierInfo.get(0) == null ? "" : 
			DatabaseConnectionManager.convertListToJson(supplierInfo).toJSONString())%>');
		//var acc = $('#<portlet:namespace/>supplier_code').val();
		//setAssociatedKontanAccInvoice('<portlet:namespace/>', acc, '<%=SupplierStages.INVOICE_SUPPLIER_WITH_2_MF%>');
		$('#request_new_supplier').hide();
		
		//supplier_code
		var supplierC = '<%=supplierInfo.get(0).get("supplier_code")%>';
		//contul asociat
		var acc = $('#<portlet:namespace />cont_asociat_furn');
		acc.removeAttr('readonly');
		if (supplierC.substring(0,1) == "7"){
			acc.val('<%=SupplierStages.ADV_REQ_SUPPLIER_WITH_7%>');
			acc.attr('readonly', true);
		} else if (supplierC.substring(0,1) == "2"){
			acc.val('<%=SupplierStages.ADV_REQ_SUPPLIER_WITH_2%>');
			acc.attr('readonly', true);
		}
		<% } %>
	<% } %>
	</script>
	
<%--fieldset DPI --%>
<%	
List<HashMap<String,Object>> dpis = null;
dpis = DPIHeader.getAllByFieldName("id_store", storeCode);
%>
<% if (!status.equals("ro")){ %>
<aui:fieldset id="dpiAsociate">
	<legend><strong><%=resmain.getString("dpi") %></strong></legend>
	
	<aui:column columnWidth="10"  first="true">
		<label for="<portlet:namespace />select_dpi" style="padding-top:5px"><%=resmain.getString("dpiuri") %>: </label>
	</aui:column>
	<aui:column columnWidth="20">
		<aui:select name="select_dpi" label="" id="select_dpi" style="width:91%">
			<aui:option selected="selected" value="">DPI</aui:option>
			<% if (dpis != null) { %>
				<% for (int i = 0; i < dpis.size(); i++) { %>
					<aui:option value="<%=dpis.get(i).get(\"id\")%>"><%=dpis.get(i).get("id")%> (<%=dpis.get(i).get("dpi_date")%>)</aui:option>
				<% } %>
			<% } %>
		</aui:select>
	</aui:column>
	<script type="text/javascript">
		$("select[name=<portlet:namespace/>select_dpi]").chosen().trigger("chosen:updated");
	</script>
	<aui:column columnWidth="10" style="padding-top:8px">
		<a href="#" id="vizualizare" style="display:none"><%=resmain.getString("show_hide_dpi")%></a>
	</aui:column>
	<aui:column>
		<aui:button name="addDPI" id="addDPI" cssClass="btn btn-primary" value="<%=resmain.getString(\"add_dpi\") %>"></aui:button>
	</aui:column>
</aui:fieldset>
<% } %>
<div id="displayDPi"></div>

<%--tip solicitare --%>
<aui:fieldset>
	<aui:layout>
		<legend><strong><%=resmain.getString("tip_solicitare") %></strong></legend>
		<aui:column columnWidth="10" first="true">
			<label for="<portlet:namespace />tip_solicitare"
				style="padding-top: 5px"><%=resmain.getString("tip_solicitare")%></label>
		</aui:column>
		<aui:column columnWidth="20" id="societate2">
		<% if (status.equals("ro")) {%>
			<aui:select id="tip_solicitare" name="tip_solicitare" label="" disabled="true" >
				<% for (int i = 0; i < allReqTypes.size(); i++) { %>
					<% if (allReqTypes.get(i).get("status").equals("A")) {%>
						<% if (allReqTypes.get(i).get("id").toString().equals(oneRow.get(0).get("tip_solicitare").toString())) { %>
						<aui:option selected="selected" value="<%=allReqTypes.get(i).get(\"id\")%>"><%=allReqTypes.get(i).get("name")%></aui:option>
					<% } %>
				<% } } %>
			</aui:select>
		<% } else { %>
			<aui:select id="tip_solicitare" name="tip_solicitare" label="" required="true">
				<aui:option selected="selected" value=""><%=resmain.getString("tip_solicitare")%></aui:option>
				<% for (int i = 0; i < allReqTypes.size(); i++) { %>
					<% if (allReqTypes.get(i).get("status").equals("A")) {%>
						<aui:option value="<%=allReqTypes.get(i).get(\"id\")%>"><%=allReqTypes.get(i).get("name")%></aui:option>
					<% } %>
				<% } %>
			</aui:select>
		<% } %>
		</aui:column>
		<%--only for not readonly --%>
		<aui:layout id="pentru_factura_proforma" style="display:none">
			<aui:column columnWidth="20">
				<label for="<portlet:namespace />nr_document_asociat" style="padding-top:5px"><%=resmain.getString("numar_document_asociat") %></label>
			</aui:column>
			<aui:column columnwidth="20">
				<aui:input name="nr_document_asociat" id="nr_document_asociat" type="text" label="" />
			</aui:column>
		</aui:layout>
		<aui:layout id="fact_neajunsa_scadenta" style="display:none">
			<aui:column columnWidth="20">
				<label for="<portlet:namespace />fact_neplatite" style="padding-top:5px"><%=resmain.getString("fact_neplatite") %></label>
			</aui:column>
			<aui:column columnWidth="20">
				<aui:select name="fact_neplatite" label="" id="fact_neplatite">
				</aui:select>
			</aui:column>
		</aui:layout>
	</aui:layout>
</aui:fieldset>
<%--fielsdet detalii generale--%>
<aui:fieldset>
	<aui:layout>
		<legend><strong><%=resmain.getString("detalii_generale")%></strong></legend>
		<aui:column columnWidth="10"  first="true">
			<label for="<portlet:namespace />explicatii_plata" style="padding-top: 5px"><%=resmain.getString("explicatii_plata")%></label>
		</aui:column>
		<aui:column columnWidth="30">
		<% if (status.equals("ro")) { %>
			<aui:input type="textarea" id="explicatii_plata" name="explicatii_plata" value="<%=oneRow.get(0).get(\"descriere\")%>" label="" readonly="true"></aui:input>
		<% } else { %>
			<aui:input type="textarea" id="explicatii_plata" name="explicatii_plata" value="" label=""></aui:input>
		<% } %>
		</aui:column>
		<% if(status.equals("ro")) { %>
		<%-- Need to get related DPIs from DB --%>
		<%	
			List<HashMap<String,Object>> relatedDpis = null;
			relatedDpis = GenericDBStatements.getAllByFieldName("advance_request_dpi", "id_adv_req", idSolicitarePlata);
		%>
		<aui:column>
			<% for (int i = 0; i < relatedDpis.size(); i++) { %>
			<a href="#" class="added_dpis" id="added_dpi_<%=relatedDpis.get(i).get("id_dpi")%>">DPI nr. <%=relatedDpis.get(i).get("id_dpi")%></a>
			<br/>
			<% } %>
		</aui:column>
		<% } %>
	</aui:layout>
	<% if (!status.equals("ro")) { %>
	<%--
	<aui:layout>
		<aui:column first="true">
			<aui:button name="att_doc" id="att_doc" value="<%=resmain.getString(\"att_doc\") %>" label=""></aui:button>
		</aui:column>
		<aui:column>
			<a href="#">Vizualizeaza documente atasate</a>
		</aui:column>
	</aui:layout>
	 --%>
	<% } %>
</aui:fieldset>

<aui:fieldset>
	<aui:layout  style="margin-top:10px">
		<%@include file="capex_details.jsp" %>
	</aui:layout>
</aui:fieldset>

<script type="text/javascript">
//autocomplete section "detalii capex"
	$(document).ready(function() { 
		var tip_capex = <%=tip_capex%>;
		if (tip_capex == 0){
			//capex diverse
			$('#<portlet:namespace />capex_diverse').prop('checked', true);
			//when id != "" the section "detalii capex" has only one input
			$('#<portlet:namespace />categorie').val("Capex Diverse")
		} else if (tip_capex == 1){
			//initiative
			$('#<portlet:namespace />init_cent').prop('checked', true);
			$('#<portlet:namespace />lista_initiative').val('<%=initiativa%>');
			//when id != "" the section "detalii capex" has only one input
			$('#<portlet:namespace />categorie').val("Initiativa Centralizata");
		} else {
			//new project
			$('#<portlet:namespace />proiect_nou').prop('checked', true);
			$('#<portlet:namespace />project').val('<%=prj%>');
			//when id != "" the section "detalii capex" has only one input
			$('#<portlet:namespace />categorie').val("Proiect Nou");
		}
	});
</script>

<legend></legend>
<%-- detalii capex -> a doua linie--%>
<aui:fieldset>
	<aui:layout>
		<aui:column columnWidth="10"  first="true">
			<label for="<namespace:portlet />categ_loturi" style="padding-top:5px"><%=resmain.getString("categ_loturi") %></label>
		</aui:column>
		<aui:column columnWidth="20">
		<% if (status.equals("ro")) { %>
			<aui:select id="categ_loturi" name="categ_loturi" label=""   disabled="true">
				<% for (int i=0;i<allCategoryLots.size();i++) { %>
					<% if (allCategoryLots.get(i).get("status").equals("A")){ %>
						<%if (allCategoryLots.get(i).get("id").toString().equals(oneRow.get(0).get("categ").toString())) { %>
							<aui:option selected="selected" value="<%=allCategoryLots.get(i).get(\"id\") %>"><%=allCategoryLots.get(i).get("name")%></aui:option>
						<% } %>
					<% } %>
				<% } %>
			</aui:select>
			<% } else { %>
				<aui:select id="categ_loturi" name="categ_loturi" label="" required="true">
				<aui:option selected="selected" value="">Categorie</aui:option>
				<% for (int i = 0;i < allCategoryLots.size(); i++) { %>
					<% if (allCategoryLots.get(i).get("status").equals("A")) { %>
						<aui:option value="<%=allCategoryLots.get(i).get(\"id\") %>">
							<%=allCategoryLots.get(i).get("name")%>
						</aui:option>
					<% } %>
				<% } %>
			</aui:select>
			<% } %>
		</aui:column>
		<script type="text/javascript">
			$("select[name=<portlet:namespace/>categ_loturi]").chosen();
		</script>
		<aui:column columnWidth="10">
			<label for="<namespace:portlet />lot" style="padding-top:5px"><%=resmain.getString("lot") %></label>
		</aui:column>
		<aui:column columnWidth="20">
		 <% if (status.equals("ro")) { %>
			<aui:select id="loturi" name="loturi" label=""   disabled="true">
				<% for (int i=0;i<allLots.size();i++) { %>
				<% if (allLots.get(i).get("status").equals("A")){ %>
				<% if(allLots.get(i).get("id").toString().equals(oneRow.get(0).get("lot").toString())) { %>
					<aui:option selected="selected" value="<%=allLots.get(i).get(\"id\") %>"><%=allLots.get(i).get("name")%></aui:option>
				<% } %>
				<% } } %>
			</aui:select>
			<%} else { %>
				<aui:select id="loturi" name="loturi" label="">
				<aui:option selected="selected" value=""><%=resmain.getString("lots")%></aui:option>
				<% for (int i=0;i<allLots.size();i++) { %>
				<% if (allLots.get(i).get("status").equals("A")){ %>
				<aui:option value="<%=allLots.get(i).get(\"id\") %>"><%=allLots.get(i).get("name")%></aui:option>
				<% } %>
				<% } %>
			</aui:select>
			<% } %>
		</aui:column>
		<script type="text/javascript">
			$("select[name=<portlet:namespace/>loturi]").chosen();
		</script>
		<aui:column columnWidth="10">
			<label for="<namespace:portlet />produs" style="padding-top:5px"><%=resmain.getString("produs") %></label>
		</aui:column>
		<aui:column columnWidth="20">
		<% if (status.equals("ro"))  { %>
			<aui:select id="produs" name="produs" label=""   disabled="true">
				<% for (int i = 0;i < allProducts.size(); i++) { %>
				<% if (allProducts.get(i).get("status").equals("A")){ %>
					<% if (allProducts.get(i).get("id").toString().equals(oneRow.get(0).get("product").toString())) { %>
						<aui:option value="<%=allProducts.get(i).get(\"id\") %>"><%=allProducts.get(i).get("name")%></aui:option>
					<% } %>
				<% } } %>
			</aui:select>
			<%} else { %>
				<aui:select id="produs" name="produs" label="">
				<aui:option selected="selected" value=""><%=resmain.getString("produs")%></aui:option>
				<% for (int i=0;i<allProducts.size();i++) { %>
					<% if (allProducts.get(i).get("status").equals("A")){ %>
						<aui:option value="<%=allProducts.get(i).get(\"id\") %>"><%=allProducts.get(i).get("name")%></aui:option>
					<% } %>
				<% } %>
			</aui:select>
		<% } %>
		</aui:column>
		<script type="text/javascript">
			$("select[name=<portlet:namespace/>produs]").chosen();
		</script>
	</aui:layout>
</aui:fieldset>
<%--ultima linie din pagina --%>
<aui:fieldset>
	<aui:layout>
		<aui:column columnWidth="10"  first="true">
			<label for="<namespace:portlet />sel_mfix" style="padding-top:5px"><%=resmain.getString("sel_nr_mfix") %></label>
		</aui:column>
		<aui:column columnWidth="20">
		<% if (status.equals("ro")) { %>
			<aui:input id="sel_mfix" name="sel_mfix" label="" disabled="true"></aui:input>
		<% } else { %>
			<aui:input id="sel_mfix" name="sel_mfix" label=""></aui:input>
		<% } %>
		</aui:column>
	</aui:layout>
</aui:fieldset>
<br/>
<script type="text/javascript">
	$(document).ready(function(){
		var currency_select = $('#<portlet:namespace/>currency');
		var date 			= $('#<portlet:namespace/>inv_date');
		// autocomplete the exchange rate when the currency is not RON
		currency_select.change(function(e) {
				var elementT 	= e.target;
				var elementId 	= elementT.id;
				var action 		= "getExchangeRate";
				// make ajax call
				if (currency_select.val() != "RON") {
					//if currency is not RON
					if (date.val() != ""){
						//if the invoice date is selected
						makeAjaxCallSupplierGroupValidation(action, date.val(), currency_select.val());
					} else {
						//reverse the change to RON
						currency_select.val("RON");
						alert("Va rugam selectati data solicitarii de avans!");
					}
				}
			});
	});
	//get the exchange rate  
	function makeAjaxCallSupplierGroupValidation(action, date, currency) {
		$.ajax({
			type: "POST",
			url: "<%=ajaxURL%>",
			data: 	"<portlet:namespace />action=" + action +  
			"&<portlet:namespace />date=" +  date +
			"&<portlet:namespace />currency_sel=" + currency,
			success: function(msg) {
					$('#<portlet:namespace/>rata-schimb').val("");
					// get table data
					if (msg != ""){
						var jsonEntries = jQuery.parseJSON(msg);
						//set the exchange rate
						$('#<portlet:namespace/>rata-schimb').val(jsonEntries.exchange_rate);
						//simulate a change on the exchange rate field to recalculate the advance request values 
						var A = AUI();
						A.one('#<portlet:namespace/>rata-schimb').simulate('change');
					} else {
						alert("A aparut o eroare in procesul de preluare a ratei de schimb valutar !");
					}
				}
			});
		}

</script>