<%@page import="com.profluo.ecm.model.db.ListaInit"%>
<%@page import="java.util.Calendar"%>
<%@page import="com.profluo.ec.carrefour.htmlutils.DropDownUtils"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme"%>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui"%>
<%@ taglib uri="http://alloy.liferay.com/tld/aui" prefix="aui"%>
<%@ taglib uri="http://liferay.com/tld/util" prefix="liferay-util"%>
<%@page import="com.profluo.ecm.model.db.DefNewProj"%>
<%@page import="com.profluo.ecm.model.vo.ListaInitVo"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.List"%>
<liferay-theme:defineObjects />
<portlet:defineObjects />
<%
	List<HashMap<String,Object>> allInitiatives = ListaInit.getAllByYear(Calendar.getInstance().get(Calendar.YEAR));
	List<HashMap<String,Object>> allNewProjects = DefNewProj.getAll();
%>
<legend><strong>Detalii CAPEX</strong></legend>
<aui:fieldset> 
   	<aui:layout>
   	 <% if (!status.equals("ro")) { %>
		<aui:column columnWidth="25">
			<aui:input type="radio" name="categ_capex" id="capex_diverse" label="Capex Diverse" value="0"></aui:input>
		</aui:column>
		<aui:column columnWidth="15">
		 	<aui:input type="radio" name="categ_capex" id="init_cent" value="1" label="initiative"></aui:input>
		</aui:column>
		<aui:column columnWidth="20">
			<aui:select name="lista_initiative" label="" id="lista_initiative" >
				<%=DropDownUtils.generateDropDownWithTwoNameColumn(allInitiatives, "Initiativa", "id", "code", "name") %>
				</aui:select>
		</aui:column>
		<script type="text/javascript">$(document).ready(function() { $("select[name=<portlet:namespace />lista_initiative]").chosen(); });</script>
		<aui:column columnWidth="10">
			<aui:input type="radio" name="categ_capex" value="2" id="proiect_nou" label="proiect_nou"></aui:input>
		</aui:column>
		<aui:column columnWidth="20">
			<aui:select name="project" label="" id="project">
				<aui:option selected="selected" value=""><liferay-ui:message key="proiect_nou"/></aui:option>
				<%= DropDownUtils.generateDropdownWithClassAndTwoNamesColumns(allNewProjects, "id", "store_id", "name", "company-prj-", "company_id", true) %>
			</aui:select>
		</aui:column>
		<script type="text/javascript">$(document).ready(function() { $("select[name=<portlet:namespace />project]").chosen(); });</script>
		<% } else { %>
		<aui:column columnWidth="10"  first="true">
			<label for="<portlet:namespace />categorie" style="padding-top:5px"><liferay-ui:message key="categ_capex"/></label>
		</aui:column>
		<aui:column>
			<aui:input name="categorie" id="categorie"  label="" type="text" readonly="true"></aui:input>
		</aui:column>
		<% } %>
	</aui:layout>
</aui:fieldset>
<script type="text/javascript">
$(document).ready(function(e) {
	var comp = $('#<portlet:namespace/>company').val();
	if (!comp  || comp == '') {
		for (var i = 1; i < 7; i++) {
			$('.company-prj-' + i).show();
		}
	} else {
		$('#<portlet:namespace/>project').val('');
		for (var i = 1; i < 7; i++) {
			if (i == comp) {
				$('.company-prj-' + i).show();
			} else {
				$('.company-prj-' + i).hide();
			}
		}
	}
	$('#<portlet:namespace/>project').trigger("chosen:updated");
});
$('#<portlet:namespace/>company').change(function(e) {
	var comp = $('#<portlet:namespace/>company').val();
	if (comp == '') {
		for (var i = 1; i < 7; i++) {
			$('.company-prj-' + i).show();
		}
	} else {
		$('#<portlet:namespace/>project').val('');
		for (var i = 1; i < 7; i++) {
			if (i == comp) {
				$('.company-prj-' + i).show();
			} else {
				$('.company-prj-' + i).hide();
			}
		}
	}
	$('#<portlet:namespace/>project').trigger("chosen:updated");
});
</script>