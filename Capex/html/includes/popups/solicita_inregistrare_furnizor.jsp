<%@page import="com.profluo.ec.carrefour.htmlutils.DropDownUtils"%>
<%@page import="com.profluo.ecm.model.db.GenericDBStatements"%>
<%@page import="com.profluo.ecm.model.vo.DefCompaniesVo"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.List"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme"%>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui"%>
<%@ taglib uri="http://alloy.liferay.com/tld/aui" prefix="aui"%>
<%@ taglib uri="http://liferay.com/tld/util" prefix="liferay-util"%>
<%@ page import="java.util.ResourceBundle"%>
<%@ page import="java.util.Locale"%>
<liferay-theme:defineObjects />
<portlet:defineObjects />
<%
	locale = renderRequest.getLocale();
	ResourceBundle resmainSol = portletConfig.getResourceBundle(locale);
	List<HashMap<String,Object>> allCompanies = DefCompaniesVo.getInstance();
	List<HashMap<String,Object>> termenePlata = GenericDBStatements.getAll("def_termen_plata_fz");
	boolean isAtAccountingDirector = false;
	String currentPage = "";
	try {
		currentPage = renderRequest.getAttribute("pagina").toString();
	} catch(Exception e){}
	if( currentPage.equals("validare-furnizori")){
		isAtAccountingDirector = true;
	}
%>
<div id="overlay" class="yui3-overlay-loading">
	<div class="yui3-widget-hd">Solicitare inregistrare furnizor</div>
	<div class="yui3-widget-bd">
		<aui:input id="new_furnizor_id" name="new_furnizor_id" label="" type="hidden"></aui:input>
		<aui:layout>
			<aui:fieldset>
				<aui:column columnWidth="20">
					<label for="<portlet:namespace />new_furnizor" style="padding-top: 5px"><%=resmainSol.getString("furnizor")%>:
					</label>
				</aui:column>
				<aui:column columnWidth="30">
					<aui:input id="new_furnizor" name="new_furnizor" label=""></aui:input>
				</aui:column>
				<aui:column columnWidth="20">
					<label for="<portlet:namespace />new_company" style="padding-top: 5px"><%=resmainSol.getString("societate")%>:
					</label>
				</aui:column>
				<aui:column>
					<aui:select id="new_company" name="new_company" label="">
						<aui:option selected="selected" value=""><liferay-ui:message key="societate"/></aui:option>
						<% for (int i = 0; i < allCompanies.size(); i++) { %>
							<% if (allCompanies.get(i).get("status").equals("A")) { %>
								<aui:option value="<%=allCompanies.get(i).get(\"id\")%>"><%=allCompanies.get(i).get("name")%></aui:option>
							<% } %>
						<% } %>
					</aui:select>
				</aui:column>
			</aui:fieldset>
			<aui:fieldset>
				<aui:column columnWidth="20">
					<label for="<portlet:namespace />new_cui" style="padding-top: 5px"><%=resmainSol.getString("cui")%>:
					</label>
				</aui:column>
				<aui:column columnWidth="30">
					<aui:input id="new_cui" name="new_cui" label=""></aui:input>
				</aui:column>
				<aui:column columnWidth="20">
					<label for="<portlet:namespace />new_email" style="padding-top: 5px"><%=resmainSol.getString("email")%>:</label>
				</aui:column>
				<aui:column columnWidth="30">
					<aui:input id="new_email" name="new_email" label=""></aui:input>
				</aui:column>
			</aui:fieldset>
			<aui:fieldset>
				<aui:column columnWidth="20">
					<label for="<portlet:namespace />new_ro" style="padding-top: 5px"><%=resmainSol.getString("ro")%>:
					</label>
				</aui:column>
				<aui:column columnWidth="30">
					<aui:input type="checkbox" id="new_ro" name="new_ro" label="" value="RO"></aui:input>
				</aui:column>
				<aui:column columnWidth="20">
					<label for="<portlet:namespace />new_telefon" style="padding-top: 5px"><%=resmainSol.getString("telefon")%>:</label>
				</aui:column>
				<aui:column columnWidth="30">
					<aui:input id="new_telefon" name="new_telefon" label=""></aui:input>
				</aui:column>
			</aui:fieldset>
			<aui:fieldset>
				<aui:column columnWidth="20">
					<label for="<portlet:namespace />new_J_field" style="padding-top: 5px"><%=resmainSol.getString("J_field")%>:
					</label>
				</aui:column>
				<aui:column columnWidth="30">
					<aui:input id="new_J_field" name="new_J_field" label="" value=""></aui:input>
				</aui:column>
					<aui:column columnWidth="20">
						<label for="<portlet:namespace />new_banca_beneficiar" style="padding-top: 5px">Modalitate de plata:</label>
					</aui:column>
					<aui:column columnWidth="30">
						<aui:input id="new_banca_beneficiar" name="new_banca_beneficiar" label=""></aui:input>
					</aui:column>
			</aui:fieldset>
			<aui:fieldset>
				<aui:column columnWidth="20">
					<label for="<portlet:namespace />new_banca" style="padding-top: 5px"><%=resmainSol.getString("banca_beneficiar")%>:
					</label>
				</aui:column>
				<aui:column columnWidth="30">
					<aui:input id="new_banca" name="new_banca" label=""></aui:input>
				</aui:column>
				<aui:column columnWidth="20">
					<label for="<portlet:namespace />new_termen_plata" style="padding-top: 5px"><%=resmainSol.getString("termen_plata")%>:</label>
				</aui:column>
				<aui:column columnWidth="30">
<%-- 					<aui:input id="new_termen_plata" name="new_termen_plata" label=""> --%>
<%--  					</aui:input> --%>
					<aui:select id="new_termen_plata" name="new_termen_plata" label="">
						<%=DropDownUtils.generateSimpleDropdown(termenePlata, "termenPlata", "termenPlata", "30", "Termen de plata")%>
				</aui:select>
				</aui:column>
			</aui:fieldset>
			<aui:column columnWidth="20">
					<label for="<portlet:namespace />new_cont_bancar" style="padding-top: 5px"><%=resmainSol.getString("cont_bancar")%>:
					</label>
				</aui:column>
				<aui:column columnWidth="30">
					<aui:input id="new_cont_bancar" name="new_cont_bancar" label="" value=""></aui:input>
				</aui:column>
					<aui:column columnWidth="20">
						<label for="<portlet:namespace />new_cod_swift" style="padding-top: 5px"><%=resmainSol.getString("cod_swift")%>:
						</label>
					</aui:column>
					<aui:column columnWidth="30">
						<aui:input id="new_cod_swift" name="new_cod_swift" label="" required="true"></aui:input>
					</aui:column>
		</aui:layout>
		<aui:layout>
			<aui:column columnWidth="20">
					<label for="<portlet:namespace />new_address" style="padding-top: 5px"><%=resmainSol.getString("address")%>:</label>
				</aui:column>
				<aui:column columnWidth="30">
					<aui:input id="new_address" name="new_address" label=""></aui:input>
			</aui:column>
			<aui:column columnWidth="20">
					<label for="<portlet:namespace />supplier_type_popup" style="padding-top: 5px"><%=resmainSol.getString("supplier_type")%>:</label>
				</aui:column>
				<aui:column columnWidth="30">
					<aui:select id="supplier_type_popup" name="supplier_type_popup" label="">
						<aui:option selected="selected" value="">-</aui:option>
						<aui:option value="0">Intern</aui:option>
						<aui:option value="1">Extern</aui:option>
						<aui:option value="2">Taxe</aui:option>
					</aui:select>
			</aui:column>
		</aui:layout>
		<aui:layout>
					<aui:column columnWidth="20">
					<label for="<portlet:namespace />supplier_tert_group_popup" style="padding-top: 5px">Grupa tert:</label>
				</aui:column>
				<aui:column columnWidth="30">
					<aui:select id="supplier_tert_group_popup" name="supplier_tert_group_popup" label="">
						<aui:option selected="selected" value="FR">FR</aui:option>
						<aui:option value="BG">BG</aui:option>
				</aui:select>
			</aui:column>
		</aui:layout>
		<aui:layout>
			<aui:column columnWidth="50" first="true">
				<aui:input value="Anuleaza" name="fcancel" type="button" label="" 
							cssClass="btn btn-primary-red fl" onclick="$('#overlay').hide()"></aui:input>
			</aui:column>
			<aui:column last="true">
				<aui:button name="validate_new_supplier" type="button" id="validate_new_supplier" 
					cssClass="btn btn-primary validateSupplier" value="<%=resmainSol.getString(\"validate\")%>"></aui:button>
			</aui:column>
		</aui:layout>
	</div>
</div>