<%@ taglib uri="http://alloy.liferay.com/tld/aui" prefix="aui"%>
<%@page import="com.profluo.ecm.model.db.InvoiceHeader"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.List"%>

<%
	List<HashMap<String,Object>> allSuppliersInv 	= InvoiceHeader.getAllInvoicesHeadersForDpi();
%>


<div id="overlay-alocare-facturi" class="yui3-overlay-loading">
	<div class="yui3-widget-hd">Adaugare factura</div>
	<div class="yui3-widget-bd">
		<aui:layout>
			<aui:fieldset>
				<aui:column columnWidth="20">
					<label for="<portlet:namespace />sistemFact" style="padding-top: 5px">Factura din sistem: </label>
				</aui:column>
				<aui:column columnWidth="30">
					<aui:select label="" name="sistemFact" id="sistemFact">
						<aui:option selected="selected" value="0">-----------------</aui:option>
						<% for (int i = 0; i < allSuppliersInv.size(); i++) { %>
							<aui:option value="<%=allSuppliersInv.get(i).get(\"id\")%>"><%=allSuppliersInv.get(i).get("nume")%></aui:option>
						<% } %>
					</aui:select>
				</aui:column>
			</aui:fieldset>
			<aui:fieldset>
				<aui:column columnWidth="20">
					<label for="<portlet:namespace />factNoua" style="padding-top: 5px">Factura noua: </label>
				</aui:column>
				<aui:column columnWidth="30">
					<aui:input label="" name="factNoua" id="factNoua" type="text" value="" />
				</aui:column>
				<aui:column columnWidth="20">
					<label for="<portlet:namespace />valFactNoua" style="padding-top: 5px">Valoare: </label>
				</aui:column>
				<aui:column columnWidth="30">
					<aui:input type="number" label="" name="valFactNoua" id="valFactNoua"/>
				</aui:column>
			</aui:fieldset>
		</aui:layout>
		<aui:layout>
			<aui:column columnWidth="50">
				<aui:input value="Anuleaza" name="anulare" type="button" label="" cssClass="btn btn-primary-red" onclick="$('#overlay-alocare-facturi').hide()"></aui:input>
			</aui:column>
			<aui:column columnWidth="50">
				<aui:button value="Adauga" name="add_invoices" type="button" label="" id="add_invoices" cssClass="btn btn-primary" ></aui:button>
			</aui:column>
		</aui:layout>
	</div>
</div>