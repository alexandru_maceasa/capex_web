<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme"%>
<%@page import="com.profluo.ecm.model.vo.DefCompaniesVo"%>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui"%>
<%@ taglib uri="http://alloy.liferay.com/tld/aui" prefix="aui"%>
<%@ taglib uri="http://liferay.com/tld/util" prefix="liferay-util"%>

<%@ page import="java.util.ResourceBundle"%>
<%@ page import="java.util.Locale"%>

<liferay-theme:defineObjects />
<portlet:defineObjects />

<div id="facturi_furnizor" style="display:none"></div>

<script type="text/javascript">
var dataTableInvoices;
YUI().use( 'aui-datatable', 'aui-datatype', 'datatable-sort',
	function(Y) {
		// data array
		var remoteDataInvoices = [];
		 
		var nestedColsInvoices = [
					
					{ key: 'nr', label : 'Factura nr.'},
					{ key: 'supplier_name' , label : 'Furnizor'},
					{ key: 'due_date', label : 'Data scadenta' },
					{ key: 'value_with_vat', label : 'Val cu TVA' },
					{ key: 'currency', label : 'Moneda' },
					{ key: 'company', label : 'Societate' },
							
		];
		 
		// TABLE INIT
		dataTableInvoices = new Y.DataTable({
		    columns: nestedColsInvoices,
		    data: remoteDataInvoices,	
		    editEvent: 'click',
		    recordType: ['id','supplier_name','due_date', 'value_with_vat', 'currency', 'company']
		}).render('#facturi_furnizor');
		
		//dataTableInvoices.get('boundingBox').unselectable();
}
);
</script>
