<aui:layout>
	<aui:column first="true" columnWidth="30">&nbsp;</aui:column>
	<aui:column columnWidth="10">
	<aui:input type="button" name="firstButton" id="firstButton" cssClass="navigation btn btn-primary" value="|<<" label="" disabled="true"></aui:input>
	</aui:column>
	<aui:column columnWidth="10">
	<aui:input type="button" name="prevButton" id="prevButton" cssClass="navigation btn btn-primary" value="<" label="" disabled="true"></aui:input>
	</aui:column>
	<aui:column columnWidth="10">
	<aui:input type="button" name="nextButton" id="nextButton" cssClass="navigation btn btn-primary" value=">" label=""></aui:input>
	</aui:column>
	<aui:column columnWidth="10">
	<aui:input type="button" name="lastButton" id="lastButton" cssClass="navigation btn btn-primary" value=">>|" label=""></aui:input>
	</aui:column>
	<aui:column last="true" columnWidth="30">&nbsp;</aui:column>
</aui:layout>