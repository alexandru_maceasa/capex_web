<%@page import="com.profluo.ecm.controller.ControllerUtils"%>
<%@page import="com.profluo.ecm.model.db.Administrare"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.List"%>
<%@page import="com.profluo.ecm.flows.InvoiceStages"%>
<%@page import="com.profluo.ecm.controller.UserTeamIdUtils"%>
<%@page import="com.profluo.ecm.flows.SupplierStages"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme"%>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui"%>
<%@ taglib uri="http://alloy.liferay.com/tld/aui" prefix="aui"%>
<%@ taglib uri="http://liferay.com/tld/util" prefix="liferay-util"%>
<%@ page import="java.util.ResourceBundle"%>
<%@ page import="java.util.Locale"%>
<liferay-theme:defineObjects />
<portlet:defineObjects />
<%
locale = renderRequest.getLocale();
ResourceBundle resmain1 = portletConfig.getResourceBundle(locale);
List<HashMap<String,Object>> kontanDays = Administrare.getKontanDays();
String currentPage = "";
try{ currentPage = renderRequest.getAttribute("pagina").toString();
}catch (Exception e){
	try { currentPage = renderRequest.getParameter("pagina").toString();
	} catch(Exception e1){
		try { currentPage = renderRequest.getParameter("source_page").toString();
		} catch (Exception e2){ }
	}
}
%>
<portlet:resourceURL id="ajaxSupplierURL" var="ajaxSupplierURL"/>
	<legend><strong><%=resmain1.getString("furnizor")%></strong></legend>
	<aui:layout>
		<aui:column columnWidth="10" first="true">
			<label for="<portlet:namespace />new_vendor" style="padding-top: 5px"><%=resmain1.getString("furnizor")%>:
			</label>
		</aui:column>
		<aui:column columnWidth="20">
			<aui:input type="hidden" id="id_supplier" name="id_supplier" label=""></aui:input>
			<aui:input type="hidden" id="supplier_code" name="supplier_code" label=""></aui:input>
			<aui:input type="hidden" id="supplier_data" name="supplier_data" label=""></aui:input>
			<aui:input id="vendor" name="vendor" label="" autocomplete="off" readonly = "readonly">
				<aui:validator name="required"  errorMessage="field-required"></aui:validator>
			</aui:input>
			<aui:input type="hidden" id="payment_term" name="payment_term" label=""></aui:input>
		</aui:column>
		<aui:column columnWidth="10">
			<label for="<portlet:namespace />cui" style="padding-top: 5px"><%=resmain1.getString("cui")%>:
			</label>
		</aui:column>
		<aui:column columnWidth="20">
			<aui:input id="cui" name="cui" label="">
			<aui:validator name="required"  errorMessage="field-required"></aui:validator>
			</aui:input>
		</aui:column>
		<aui:column columnWidth="10">
			<label for="<portlet:namespace />cont-asociat"
				style="padding-top: 5px"><%=resmain1.getString("cont-asociat")%>:
			</label>
		</aui:column>
		<aui:column columnWidth="20">
			<aui:input id="cont-asociat" name="cont-asociat" label="">
			</aui:input>
		</aui:column>
	</aui:layout>
	<aui:layout>
	<% if(!currentPage.equals("solicitari-avans")) { %>
		<aui:column columnWidth="10" first="true">
			<label for="<portlet:namespace />kontan_acc" style="padding-top: 5px"><%=resmain1.getString("cont_kontan")%>:</label>
		</aui:column>
		<aui:column id="acc2" name="acc2" columnWidth="20">
			<aui:input type="text" id="kontan_acc2" name="kontan_acc2" value="" label="" >
			</aui:input>
		</aui:column>
		<aui:column id="acc7" name="acc7" columnWidth="20" style="display:none">
			<aui:select type="text" id="kontan_acc7" name="kontan_acc7" value="" label="">
				<aui:option selected="selected" value="">-</aui:option>
				<aui:option value="<%=SupplierStages.INVOICE_SUPPLIER_WITH_7_MF %>">MF - <%=SupplierStages.INVOICE_SUPPLIER_WITH_7_MF %></aui:option>
				<aui:option value="<%=SupplierStages.INVOICE_SUPPLIER_WITH_7_OI %>">OB I - <%=SupplierStages.INVOICE_SUPPLIER_WITH_7_OI %></aui:option>
			</aui:select>
		</aui:column>
		<aui:column id="acc8" name="acc8" columnWidth="20" style="display:none">
			<aui:select type="text" id="kontan_acc8" name="kontan_acc8" value="" label="">
				<aui:option selected="selected" value="">-</aui:option>
				<aui:option value="<%=SupplierStages.INVOICE_SUPPLIER_WITH_8_MF %>">MF - <%=SupplierStages.INVOICE_SUPPLIER_WITH_8_MF %></aui:option>
				<aui:option value="<%=SupplierStages.INVOICE_SUPPLIER_WITH_8_OI %>">OB I - <%=SupplierStages.INVOICE_SUPPLIER_WITH_8_OI %></aui:option>
			</aui:select>
		</aui:column>
		<% } %>
		<% if (!currentPage.equals("workEquipment")) { %>
		<aui:column columnWidth="10">
			<label for="<portlet:namespace />supplier_type_invoice"
				style="padding-top: 5px"><%=resmain1.getString("supplier_type")%>:
			</label>
		</aui:column>
		<aui:column columnWidth="20">
			<aui:select type="text" id="supplier_type_invoice" name="supplier_type_invoice" value="" label="" required="true">
				<aui:option selected="selected" value="">-</aui:option>
				<aui:option value="0">Intern</aui:option>
				<aui:option value="1">Extern</aui:option>
			</aui:select>
		</aui:column>
		<% } %>
		<% if (currentPage.equals("invoice") || currentPage.equals(InvoiceStages.PAGE_CONTABILITATE_LISPA_DOC) 
				|| currentPage.equals("exported_docs")) { %>
		<aui:column columnWidth="10" id="extern_invoice_label_display" name="extern_invoice_label_display" style="display:none">
			<label for="<portlet:namespace />extern_invoice_option"
				style="padding-top: 5px"><%=resmain1.getString("extern_invoice_option")%>:
			</label>
		</aui:column>
		<aui:column columnWidth="20" id="extern_invoice_display" name="extern_invoice_display" style="display:none">
			<aui:select type="text" id="extern_invoice_option" name="extern_invoice_option" value="" label="">
				<aui:option selected="selected" value="">-</aui:option>
				<aui:option value="0">Intracomunitar bunuri</aui:option>
				<aui:option value="1">Intracomunitar servicii</aui:option>
				<aui:option value="2">Extracomunitar DEI</aui:option>
				<aui:option value="3">Extracomunitar DEI cu amanare TVA</aui:option>
				<aui:option value="6">Extracomunitar servicii</aui:option>
				<aui:option value="7">Extracomunitari de bunuri scutit</aui:option>
				<aui:option value="8">Intracomunitar bunuri regularizare</aui:option>
				<aui:option value="9">Intracomunitar servicii regularizare</aui:option>
			</aui:select>
		</aui:column>
		<aui:column columnWidth="10" id="intern_invoice_label_display" name="intern_invoice_label_display" style="display:none">
			<label for="<portlet:namespace />intern_invoice_option"
				style="padding-top: 5px"><%=resmain1.getString("intern_invoice_option")%>:
			</label>
		</aui:column>
		<aui:column columnWidth="20" id="intern_invoice_display" name="intern_invoice_display" style="display:none" >
			<aui:select type="text" id="intern_invoice_option" name="intern_invoice_option" value="" label="">
				<aui:option selected="selected" value="">-</aui:option>
				<aui:option value="4">Cu taxare inversa</aui:option>
				<aui:option value="5">Fara taxare inversa</aui:option>
			</aui:select>
		</aui:column>
		<% } %>
	</aui:layout>
	<aui:layout style="display:none" id="loccked_supplier"><a><font color="#FF0000">Atentie, furnizorul este blocat in Kontan!</font></a></aui:layout>
	<aui:layout style="display:none" id="missing_supplier"><a><font color="#FF0000">Atentie, furnizorul nu este definit in Kontan!</font></a></aui:layout>
	<aui:layout style="display:none" id="missing_payment_term"><a><font color="#FF0000">Atentie, termenul de plata nu este definit pentru acest furnizor!</font></a></aui:layout>
	<% if (!currentPage.equals("workEquipment")) { %>
	<aui:layout id="request_supplier">
		<aui:column columnWidth="100" first="true">
			<a class="btn btn-primary" style="margin:0px" id="showoverlay">Solicita inregistrare furnizor</a>
			<div class="clear"></div>
		</aui:column>
	</aui:layout>
	<% } %>
<script type="text/javascript">
YUI().use('autocomplete-list', "datasource-io", 'aui-base', 'aui-io-request', 'aui-form-validator', 'autocomplete-filters', 'autocomplete-highlighters', 'overlay',
	function (A) {
		var contactSearchDS = new A.DataSource.IO({source: '<%=ajaxSupplierURL.toString()%>'});
		var supplierData = "";
		var supplierCreated = false;
		var contactSearchFormatter = function (query, results) {
		return A.Array.map(results, function (result) {
			return '<strong>'+result.raw.name+'</strong><br/><b>' + result.raw.supplier_code + '</b> - CUI: ' + result.raw.cui;
			});
		};
		var testData;
		var contactSearchInput = new A.AutoCompleteList({
			allowBrowserAutocomplete: 'false',
			resultHighlighter: 'phraseMatch',
			activateFirstItem: 'true',
			resultTextLocator: 'name',
			inputNode: '#<portlet:namespace/>vendor',
			render: 'true',
			width: '300px',
	        resultFormatter: contactSearchFormatter,
			source: function(){
				$('#<portlet:namespace/>id_supplier').val("");
				$('#<portlet:namespace/>cui').val("");
				$('#<portlet:namespace/>cont-asociat').val("");
				$('#<portlet:namespace/>supplier_code').val("");
				$('#<portlet:namespace/>supplier_data').val("");
				$('#<portlet:namespace/>new_furnizor_id').val("");
				supplierData = "";
				$('#<portlet:namespace />validate_new_supplier').show();
				var inputValue = A.one("#<portlet:namespace />vendor").get('value');
				var myAjaxRequest = A.io.request('<%=ajaxSupplierURL.toString()%>',{
						dataType: 'json',
						method:'POST',
						data:{
							 <portlet:namespace/>action: 'findSupplier',
							 <portlet:namespace/>keywords: $('#<portlet:namespace />vendor').val(),
							 <portlet:namespace/>id_company: $('#<portlet:namespace />company').val()
							},
						autoLoad: false,
						sync: true,
						on: {
						success:function(){
							var data = this.get('responseData');
							testData = data;
						}}
				});
				myAjaxRequest.start();
				return testData;
			},	
 			on: {
				select: function(event) {
					// after user selects an option from the list
					var result = event.result.raw;
					// global variable to be used also in the create new supplier pop-up
					supplierData = result;
					A.one('#<portlet:namespace/>id_supplier').val(result.id);
					A.one('#<portlet:namespace/>cui').val(result.cui);
					A.one('#<portlet:namespace/>cont-asociat').val(result.bank_account);
					A.one('#<portlet:namespace/>supplier_code').val(result.supplier_code);
					A.one('#<portlet:namespace/>payment_term').val(result.payment_term);
					A.one('#<portlet:namespace/>supplier_data').val(result);
					$('#<portlet:namespace />new_furnizor_id').val("");
					if (result.supplier_code < 0)
						$('#<portlet:namespace/>new_furnizor_id').val(result.id);
					//setare data scadenta
					setDueDate('<portlet:namespace/>');
					//setare cont kontan asociat furnizorului
					<% if(!UserTeamIdUtils.belongsToTeam(user.getUserId(), UserTeamIdUtils.ACCOUNTING)){ %>
						setAssociatedKontanAccInvoice('<portlet:namespace/>', result.supplier_code, '<%=SupplierStages.INVOICE_SUPPLIER_WITH_2_MF%>', result.name, true);
					<% } else { %>
						setAssociatedKontanAccInvoice('<portlet:namespace/>', result.supplier_code, '<%=SupplierStages.INVOICE_SUPPLIER_WITH_2_MF%>', result.name, false);
					<% } %>
					//set supplier type
					setSupplierType('<portlet:namespace/>', result.supplier_code);
					//set invoice option based on supplier type
					<% if (currentPage.equals("invoice")) { %>
					setInvoiceOption('<portlet:namespace/>');
					<% } %>
					if (result.blocked == 1 && result.supplier_code > 0) {
						$('#loccked_supplier').show();
					} else {
						$('#loccked_supplier').hide();
					}
					if (result.supplier_code < 0) {
						$('#missing_supplier').show();
					} else {
						$('#missing_supplier').hide();
					}
					if (result.payment_term == ""){
						$('#missing_payment_term').show();
					} else {
						$('#missing_payment_term').hide();
					}
				}
			}
		});	
		// creare furnizor overlay
		var overlay = new A.Overlay({
		    srcNode:"#overlay",
		    width:"900px",
		    align: {
		        node:"#showoverlay",
		        points:["tl", "tr"]
		    }
		});
		overlay.render();
		$('#overlay').hide();
		var days = <%=ControllerUtils.getKontanDays(kontanDays)%>;
		// click on validate new supplier pop-up
		A.on("click", function() {
			// error check
			var error = validateSupplierPopupValues('<portlet:namespace/>');
			// make ajax request if there are no errors
			var supplierID = $('#<portlet:namespace/>new_furnizor_id').val();
			if (error == 0) {
				var termenPlata = parseInt($('#<portlet:namespace/>new_termen_plata').val());
					if($.inArray(termenPlata, days) >= 0) {
						$.ajax({
			  				type: "POST",
			  				url: "<%=ajaxSupplierURL%>",
			  				data: 	"<portlet:namespace />action=requestSupplierCreation" +  
			  						"&<portlet:namespace />supplierIdCreation=" + supplierID + 
			  						"&<portlet:namespace />name=" + $('#<portlet:namespace/>new_furnizor').val() +
			  						"&<portlet:namespace />cui=" + $('#<portlet:namespace/>new_cui').val() +
			  						"&<portlet:namespace />email=" + $('#<portlet:namespace/>new_email').val() +
			  						"&<portlet:namespace />phone=" + $('#<portlet:namespace/>new_telefon').val() +
			  						"&<portlet:namespace />reg_com=" + $('#<portlet:namespace/>new_J_field').val() +
			  						"&<portlet:namespace />payment_code=" + $('#<portlet:namespace/>new_banca_beneficiar').val() +
			  						"&<portlet:namespace />ro=" + $('#<portlet:namespace/>new_roCheckbox').val() +
			  						"&<portlet:namespace />bank=" + $('#<portlet:namespace/>new_banca').val() +
			  						"&<portlet:namespace />bank_acc=" + $('#<portlet:namespace/>new_cont_bancar').val() +
			  						"&<portlet:namespace />id_company=" + $('#<portlet:namespace/>new_company').val() +
			  						"&<portlet:namespace />swift=" + $('#<portlet:namespace/>new_cod_swift').val() +
			  						"&<portlet:namespace />supplier_type_db=" + $('#<portlet:namespace/>supplier_type_popup').val() +
			  						"&<portlet:namespace />payment_term=" + $('#<portlet:namespace/>new_termen_plata').val() +
			  						"&<portlet:namespace />new_address=" + $('#<portlet:namespace/>new_address').val()+
			  						"&<portlet:namespace />supplier_tert_group_popup=" + $('#<portlet:namespace/>supplier_tert_group_popup').val(),
			  				success: function(msg) {
			  					$('#<portlet:namespace />vendor').val($('#<portlet:namespace/>new_furnizor').val());
			  					$('#<portlet:namespace />cui').val($('#<portlet:namespace/>new_cui').val());
			  					$('#<portlet:namespace />cont-asociat').val($('#<portlet:namespace/>new_cont_bancar').val());
			  					$('#<portlet:namespace />payment_term').val($('#<portlet:namespace/>new_termen_plata').val());
			  					$('#<portlet:namespace />supplier_type_invoice').val($('#<portlet:namespace/>supplier_type_popup').val());
			  					$('#overlay').hide();
			  					// get resulted id
			  					if (msg != ""){
			  						var jsonEntries = jQuery.parseJSON(msg);
			  						//set the id resulted from the insertion
			  						$('#<portlet:namespace />id_supplier').val(jsonEntries.id);
			  						supplierData = jsonEntries.supplier_data[0];
			  						alert(jsonEntries.message);
			  						$('#<portlet:namespace />new_furnizor_id').val(supplierData.id);
			  						$('#<portlet:namespace/>tip-factura').prop('disabled', false);
			  						setInvoiceOption('<portlet:namespace/>');
			  					}
			  				}
			  			});
					} else {
						alert("Termenul de plata introdus nu exista in Kontan.");
						$('#<portlet:namespace/>new_termen_plata').focus();
					}
				}
		}, "#<portlet:namespace />validate_new_supplier");
		// click on "solicitare creare furnizor"
	    A.on("click", function() {
	    	console.log($('#<portlet:namespace />new_furnizor_id').val());
	    	displaySupplierOverlay('<portlet:namespace />', supplierData);
	    }, "#showoverlay");
		$('#<portlet:namespace />supplier_type_invoice').change(function(e){
			setInvoiceOption('<portlet:namespace/>');
		});
	});
</script>