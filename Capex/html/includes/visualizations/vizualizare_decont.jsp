<%@page import="com.profluo.ecm.model.db.DefLot"%>
<%@page import="com.profluo.ecm.model.db.DefIFRS"%>
<%@page import="com.profluo.ecm.model.db.DefIAS"%>
<%@page import="com.profluo.ecm.model.db.DefProduct"%>
<%@page import="com.profluo.ecm.model.db.DefCatGest"%>
<%@page import="com.profluo.ecm.model.db.DefMFAct"%>
<%@page import="com.profluo.ecm.model.db.DefRegEquip"%>
<%@page import="com.profluo.ecm.model.db.DefStore"%>
<%@page import="com.profluo.ecm.model.db.ListaInit"%>
<%@page import="com.profluo.ecm.model.db.DefNewProj"%>
<%@page import="com.profluo.ecm.model.db.DefRelComer"%>
<%@page import="com.profluo.ecm.model.db.DefCompanies"%>
<%@page import="com.profluo.ecm.model.db.DefVAT"%>
<%@page import="com.profluo.ecm.flows.CashExpenseStages"%>
<%@page import="com.profluo.ecm.model.db.DatabaseConnectionManager"%>
<%@page import="com.profluo.ecm.model.db.DefSupplier"%>
<%@page import="com.profluo.ecm.model.db.DefExchRates"%>
<%@page import="com.profluo.ecm.controller.ControllerUtils"%>
<%@page import="com.profluo.ecm.model.vo.DefNewProjVo"%>
<%@page import="com.profluo.ecm.model.vo.ListaInitVo"%>
<%@page import="com.profluo.ecm.model.db.CashExpense"%>
<%@page import="com.profluo.ecm.model.vo.DefAttDocsVo"%>
<%@page import="com.profluo.ecm.model.vo.DefVATVo"%>
<%@page import="com.profluo.ecm.model.vo.DefRelComerVo"%>
<%@page import="com.profluo.ecm.model.vo.DefTipFactVo"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.List"%>
<%@page import="com.profluo.ecm.model.vo.DefCurrencyVo"%>
<%@page import="com.profluo.ecm.model.vo.DefCompaniesVo"%>
<%@page import="com.profluo.ecm.model.vo.DefUMVo"%>
<%@page import="com.profluo.ecm.model.vo.DefIASVo"%>

<%@page import="com.profluo.ecm.model.vo.DefIFRSVo"%>
<%@page import="com.profluo.ecm.model.vo.DefLotVo"%>
<%@page import="com.profluo.ecm.model.vo.DefProductVo"%>
<%@page import="com.profluo.ecm.model.vo.DefDpis"%>


<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme"%>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui"%>
<%@ taglib uri="http://alloy.liferay.com/tld/aui" prefix="aui"%>
<%@ taglib uri="http://liferay.com/tld/util" prefix="liferay-util"%>

<%@ page import="java.util.ResourceBundle"%>
<%@ page import="java.util.Locale"%>

<liferay-theme:defineObjects />
<portlet:defineObjects />

<%
		List<HashMap<String,Object>> supplierInfo 		= null;
		List<HashMap<String,Object>> productInfo 		= null;
		List<HashMap<String,Object>> oneCashExpenseHeader = null;
		List<HashMap<String,Object>> cashExpenseLines 	= null;
		List<HashMap<String,Object>> cashExpStoreLines	= null;
		
		
		String newProjName = "";
		String idInitiative = "";
		String productCode = "0";
		String id = "";
		try {
			id = renderRequest.getParameter("id").toString();
			System.out.println("ID = " + id);
		} catch (Exception e) { System.out.println("Eroare la preluarea Id-ului!!!");}
		oneCashExpenseHeader = CashExpense.getHeaderById(id, "cashexp_header");
		String vat = oneCashExpenseHeader.get(0).get("vat_code").toString();
		supplierInfo = DefSupplier.getSupplierNameByIdAndCompany(oneCashExpenseHeader.get(0).get("id_supplier").toString(),oneCashExpenseHeader.get(0).get("id_company").toString());
		cashExpenseLines = CashExpense.getCashExpLines(id);
		cashExpStoreLines = CashExpense.getCashExpStoreLines(id);
		if (oneCashExpenseHeader.get(0).get("id_new_prj") != null){
			newProjName = DefNewProj.getNewPrjNameById(oneCashExpenseHeader.get(0).get("id_new_prj").toString());
		}
		if (oneCashExpenseHeader.get(0).get("id_initiative") != null){
			idInitiative = ListaInit.getInitiativeNameById(oneCashExpenseHeader.get(0).get("id_initiative").toString());
		}
		
		
	%>
	
<portlet:resourceURL id="ajaxURL" var="ajaxURL"/>
<%-- Default portletURL --%>
<portlet:renderURL var="portletURL"></portlet:renderURL>

<portlet:renderURL var="backURL">
	<portlet:param name="mvcPath" value="/html/deconturi/view.jsp"></portlet:param>
</portlet:renderURL>

<a href="<%=portletURL%>" class="btn btn-primary-red fr"
	style="margin: 0px">Inapoi</a>


<%-- Creare formular inregistrare decont --%>
	<aui:fieldset>
		<legend><strong>Antet document fiscal</strong></legend>
		<aui:layout>
			<aui:column columnWidth="10" first="true">
				<label for="<portlet:namespace />cash_exp_no" style="padding-top: 5px">Numar document</label>
			</aui:column>
			<aui:column columnWidth="20">
				<aui:input type="text" id="cash_exp_no" disabled="true" name="cash_exp_no" value="<%=oneCashExpenseHeader.get(0).get(\"cash_exp_no\") %>" label="">
				</aui:input>
			</aui:column>
			<aui:column columnWidth="10">
				<label for="<portlet:namespace/>cash_exp_date" style="padding-top: 5px">Data document</label>
			</aui:column>
			<aui:column columnWidth="20">
				<aui:input type="date" id="cash_exp_date" name="cash_exp_date" disabled="true" value="<%=oneCashExpenseHeader.get(0).get(\"cash_exp_date\") %>" label="">
				</aui:input>
			</aui:column>

			<aui:column columnWidth="10">
				<label for="<portlet:namespace />company"
					style="padding-top: 5px">Societate</label>
			</aui:column>
			<aui:column columnWidth="20">
				<aui:input type="text" id="societate" disabled="true" name="societate" 
				value="<%=DefCompanies.getCompanyNameById(oneCashExpenseHeader.get(0).get(\"id_company\").toString()) %>" label=""/>
			</aui:column>
		</aui:layout>
		<aui:layout>
			<aui:column columnWidth="10" first="true">
				<label for="<portlet:namespace />tip-rel-comerciala"
					style="padding-top: 5px">Tip relatie comerciala</label>
			</aui:column>
			<aui:column columnWidth="20">
				<aui:input type="text" id="tip-rel-comerciala" disabled="true" name="tip-rel-comerciala" 
				value='<%=DefRelComer.getTipRelComNameById(Integer.parseInt(oneCashExpenseHeader.get(0).get("tip_rel_com").toString())) %>' label=""/>
			</aui:column>
			<aui:column columnWidth="10" id = "label_contract_display">
				<label for="<portlet:namespace />nr-contract" style="padding-top: 5px">Numar contract</label>
			</aui:column>
			<aui:column columnWidth="20" id="numar_contract_display">
				<aui:input id="nr-contract" name="nr-contract" disabled="true" label="" value = "<%=oneCashExpenseHeader.get(0).get(\"co_number\") %>">
				</aui:input>
			</aui:column>
			<aui:column columnWidth="10" id="label_comanda_display">
				<label for="<portlet:namespace />nr-comanda" style="padding-top: 5px">Numar comanda</label>
			</aui:column>
			<aui:column columnWidth="20" id="numar_comanda_display">
				<aui:input id="nr-comanda" name="nr-comanda" disabled="true" label="" value = "<%=oneCashExpenseHeader.get(0).get(\"order_no\") %>">
				</aui:input>
			</aui:column>
		</aui:layout>
	</aui:fieldset>
	<aui:fieldset>
		<legend><strong>Informatii document</strong></legend>
		<aui:layout>
			<aui:column columnWidth="10" first="true">
				<label for="<portlet:namespace />datascadenta" style="padding-top: 5px">Data scadenta</label>
			</aui:column>
			<aui:column columnWidth="20">
				<aui:input type="text" id="datascadenta" name="datascadenta" disabled="true" value="<%=oneCashExpenseHeader.get(0).get(\"due_date\") %>" label="" />
			</aui:column>
			<aui:column columnWidth="10" first="true">
				<label for="<portlet:namespace />currency" style="padding-top: 5px">Moneda</label>
			</aui:column>
			<aui:column columnWidth="20">
				<aui:input type="text" id="moneda" name="moneda" label="" disabled="true" value="<%=oneCashExpenseHeader.get(0).get(\"currency\") %>"/>
			</aui:column>
			<aui:column columnWidth="10">
				<label for="<portlet:namespace />rata-schimb"
					style="padding-top: 5px">Rata de schimb</label>
			</aui:column>
			<aui:column columnWidth="20">
				<aui:input type="text" id="rata-schimb" name="rata-schimb" disabled="true" value="<%=oneCashExpenseHeader.get(0).get(\"exchange_rate\")%>"
					label="" />
			</aui:column>
		</aui:layout>
	</aui:fieldset>
	<aui:fieldset>
		<aui:column columnWidth="10" first="true">
			<label for="<portlet:namespace />cota_tva" style="padding-top: 5px">Cota TVA</label>
		</aui:column>
		<aui:column columnWidth="20">
				<aui:input type="text" id="tva" name="tva" label="" disabled="true" value="<%=vat %>"/>
			</aui:column>
		<aui:column columnWidth="10" first="true">
			<label for="<portlet:namespace />procent_garantie"
				style="padding-top: 5px">Procent garantie</label>
		</aui:column>
		<aui:column columnWidth="20">
			<aui:input type="text" id="procent_garantie" name="procent_garantie" value="<%=(oneCashExpenseHeader.get(0).get(\"warranty\") == null ? \"0\" : oneCashExpenseHeader.get(0).get(\"warranty\"))%>" 
			 label="" disabled="true" />
		</aui:column>
	</aui:fieldset>
	
<%--calculare valoare currency --%>
<aui:fieldset id="valoareCurrency" style="display:none">
	<legend id="currency_label"></legend>
	<aui:layout>
		<aui:column columnWidth="10">
			<label for="<portlet:namespace />val_no_vat_curr"
				style="padding-top: 5px">Valoare</label>
		</aui:column>
		<aui:column columnWidth="20">
			<aui:input cssClass="currencyCol" disabled="true" type="text" id="val_no_vat_curr" name="val_no_vat_curr" value="<%=oneCashExpenseHeader.get(0).get(\"val_no_vat_curr\")%>" label="">
			</aui:input>
		</aui:column>
		<aui:column columnWidth="10">
			<label for="<portlet:namespace />val_vat_curr" style="padding-top: 5px">TVA</label>
		</aui:column>
		<aui:column columnWidth="20">
			<aui:input cssClass="currencyCol" disabled="true" type="text" id="val_vat_curr" name="val_vat_curr" value="<%=oneCashExpenseHeader.get(0).get(\"val_vat_curr\")%>" label="" >
			</aui:input>
		</aui:column>
		<aui:column columnWidth="10">
			<label for="<portlet:namespace />val_with_vat_curr"
				style="padding-top: 5px">Valoarea cu TVA</label>
		</aui:column>
		<aui:column columnWidth="20">
			<aui:input cssClass="currencyCol" disabled="true" type="text" id="val_with_vat_curr" name="val_with_vat_curr" value="<%=oneCashExpenseHeader.get(0).get(\"total_with_vat_curr\")%>" label="">
			</aui:input>
		</aui:column>
	</aui:layout>
	</aui:fieldset>
	
	<%--calculare valoare RON --%>
	<aui:fieldset id="valoareRON">
		<legend>Valoare RON</legend>
		<aui:layout>
			<aui:column columnWidth="10">
				<label for="<portlet:namespace />val_no_vat_ron"
					style="padding-top: 5px">Valoare</label>
			</aui:column>
			<aui:column columnWidth="20">
				<aui:input cssClass="currencyCol" type="text" id="val_no_vat_ron" name="val_no_vat_ron" disabled="true"
					value="<%=oneCashExpenseHeader.get(0).get(\"val_no_vat_ron\")%>" label="" />
			</aui:column>
			<aui:column columnWidth="10">
				<label for="<portlet:namespace />val_vat_ron" style="padding-top: 5px">TVA</label>
			</aui:column>
			<aui:column columnWidth="20">
				<aui:input cssClass="currencyCol" disabled="true" type="text" id="val_vat_ron" name="val_vat_ron" 
				value="<%=oneCashExpenseHeader.get(0).get(\"val_vat_ron\")%>" label="" />
			</aui:column>
			<aui:column columnWidth="10">
				<label for="<portlet:namespace />val_with_vat_ron"
					style="padding-top: 5px">Valoare cu TVA</label>
			</aui:column>	
			<aui:column columnWidth="20">
				<aui:input cssClass="currencyCol" type="text" disabled="true" id="val_with_vat_ron" name="val_with_vat_ron" 
				value="<%=oneCashExpenseHeader.get(0).get(\"total_with_vat_ron\")%>" label="" />
			</aui:column>
		</aui:layout>
	</aui:fieldset>
	
<aui:fieldset>
	<legend>Furnizor</legend>
	<aui:layout>
		<aui:column columnWidth="10">
			<label for="<portlet:namespace />supplier" style="padding-top: 5px">Furnizor</label>
		</aui:column>
		<aui:column columnWidth="20" id="supplier">
			<aui:input id="supplier" readonly="true" disabled="true" name="supplier" label="" value = "<%=supplierInfo.get(0).get(\"name\") %>">
			</aui:input>
		</aui:column>
		<aui:column columnWidth="10">
			<label for="<portlet:namespace />cui" style="padding-top: 5px">CUI</label>
		</aui:column>
		<aui:column columnWidth="20" id="cui">
			<aui:input id="cui" name="cui" readonly="true" disabled="true" label="" value = "<%=supplierInfo.get(0).get(\"cui\") %>">
			</aui:input>
		</aui:column>
		<aui:column columnWidth="10">
			<label for="<portlet:namespace />associated_acc" style="padding-top: 5px">Cont asociat furnizorului</label>
		</aui:column>
		<aui:column columnWidth="20" id="associated_acc">
			<aui:input id="associated_acc" readonly="true" disabled="true" name="associated_acc" label="" value = "<%=supplierInfo.get(0).get(\"bank_acc\") %>">
			</aui:input>
		</aui:column>
	</aui:layout>
</aui:fieldset>

<aui:fieldset>
		<legend><strong>Detalii CAPEX</strong></legend>
		<aui:column columnWidth="25">
			<% if (oneCashExpenseHeader.get(0).get("tip_capex") != null && oneCashExpenseHeader.get(0).get("tip_capex").toString().equals("0")) { %>
				<aui:input type="radio" name="categ_capex" id="capex_diverse" label="Capex Diverse" value="0" disabled="true" checked="true" />
			<% } else { %>
				<aui:input type="radio" name="categ_capex" id="capex_diverse" label="Capex Diverse" value="0" disabled="true" />
			<% } %>
		</aui:column>
		
		<% if (oneCashExpenseHeader.get(0).get("tip_capex") != null && oneCashExpenseHeader.get(0).get("tip_capex").toString().equals("1")) { %>
		<aui:column columnWidth="10">
			<aui:input type="radio" name="initiative" id="initiative" label="Lista initiative" value="0" disabled="true" checked="true" />
		</aui:column>
		<aui:column columnWidth="15">
			<aui:input type="text" name="init" id="init" disabled="true" label="" value="<%=idInitiative %>"/>
		</aui:column>
		<% } else { %>
		<aui:column columnWidth="25">
			<aui:input type="radio" name="initiative" id="initiative" label="Lista initiative" value="0" disabled="true" />
		</aui:column>
		<% } %>
		
		<% if (oneCashExpenseHeader.get(0).get("tip_capex") != null && oneCashExpenseHeader.get(0).get("tip_capex").toString().equals("2")) { %>
		<aui:column columnWidth="10">
			<aui:input type="radio" name="new_prj" id="new_prj" label="Proiect nou" value="0" disabled="true" checked="true" />
		</aui:column>
		<aui:column columnWidth="15">
			<aui:input type="text" name="new_prj" id="new_prj" disabled="true" label="" value="<%=newProjName %>"/>
		</aui:column>
		<% } else { %>
		<aui:column columnWidth="25">
			<aui:input type="radio" name="new_prj" id="new_prj" label="Proiect nou" value="0" disabled="true" />
		</aui:column>
		<% } %>
	</aui:fieldset>

<legend><strong>Linii factura</strong></legend>
<aui:layout>
	<aui:column first="true" columnWidth="100" last="true">
		<div id="alocare-articole"></div>
	</aui:column>
</aui:layout>
<%-- Distributed lines --%>
<legend><strong>Alocare articol pe linii</strong></legend>
<aui:layout>
	<aui:column first="true" columnWidth="100" last="true">
		<div id="alocare-linii"></div>
	</aui:column>
</aui:layout>

<div id="overlay-detalii-inv" class="yui3-overlay-loading" style="left:-350px;position:absolute;width: 280px;z-index: 1">
    <div class="yui3-widget-hd" style="font-size: 12px">
    	Detalii inventar pentru articolul: <span id="cod_art_header" style="display:none">&nbsp;</span> <span id="den_art_header">&nbsp;</span>
    </div>
    <div class="yui3-widget-bd">
		<aui:layout>
			<aui:column first="true" columnWidth="100" last="true" style="position:relative">
				<div id="detalii-inventar"></div>
			</aui:column>
		</aui:layout>
		<aui:layout>
			<aui:column columnWidth="50" first="true">
				<aui:input value="Inchide" name="fcancel" id="finvcancel" type="button" label="" cssClass="btn btn-primary-red fl"/>
			</aui:column>
		</aui:layout>
    </div>
</div>
<%-- overlay solicita vizualizare DPI --%>
<div id="vizualizareDPI" class="yui3-overlay-loading"
	style="right: 100px; z-index: 1; position: absolute; width: 800px; display: none">
	<div class="yui3-widget-hd">Solicitare vizualizare DPI</div>
	<div class="yui3-widget-bd">
		<aui:layout>
			<aui:fieldset>
				<aui:column columnWidth="20">
					<label for="<portlet:namespace />nr_dpi" style="padding-top: 5px">Nr DPI: </label>
				</aui:column>
				<aui:column columnWidth="30">
					<aui:input id="nr_dpi" name="nr_dpi" label="" readonly="true"></aui:input>
				</aui:column>
				<aui:column columnWidth="20">
					<label for="<portlet:namespace />societate"
						style="padding-top: 5px">Societate:</label>
				</aui:column>
				<aui:column columnWidth="30">
					<aui:input id="societate" name="societate" label="" readonly="true"></aui:input>
				</aui:column>
			</aui:fieldset>

			<aui:fieldset>
				<aui:column columnWidth="20">
					<label for="<portlet:namespace />data_dpi" style="padding-top: 5px">Data DPI: </label>
				</aui:column>
				<aui:column columnWidth="30">
					<aui:input id="data_dpi" name="data_dpi" label="" readonly="true"></aui:input>
				</aui:column>
				<aui:column columnWidth="20">
					<label for="<portlet:namespace />categ_capex_dpi"
						style="padding-top: 5px">Categorie Capex:</label>
				</aui:column>
				<aui:column columnWidth="30">
					<aui:input id="categ_capex_dpi" name="categ_capex_dpi" label=""
						readonly="true"></aui:input>
				</aui:column>
			</aui:fieldset>

			<aui:fieldset>
				<aui:column columnWidth="20">
					<label for="<portlet:namespace />solicitant"
						style="padding-top: 5px">Solicitant: </label>
				</aui:column>
				<aui:column columnWidth="30">
					<aui:input id="solicitant" name="solicitant" label=""
						readonly="true"></aui:input>
				</aui:column>
			</aui:fieldset>

			<aui:fieldset>
				<aui:column columnWidth="20">
					<label for="<portlet:namespace />aprobator"
						style="padding-top: 5px">Aprobator: </label>
				</aui:column>
				<aui:column columnWidth="80">
					<aui:input id="aprobator" name="aprobator" label="" readonly="true"
						style="width: 97.6%"></aui:input>
				</aui:column>
			</aui:fieldset>
			<aui:layout>
				<aui:column columnWidth="100" last="true" first="true">
					<div id="tabelDPI"></div>
				</aui:column>
			</aui:layout>
			<aui:layout>
				<aui:column columnWidth="50" last="true" style="text-align:right">
					<aui:input value="Inchide" name="fcancel" type="button" label="" cssClass="btn btn-primary-red fr"
						onclick="$('#vizualizareDPI').hide()"></aui:input>
					<div class="clear"></div>
				</aui:column>
			</aui:layout>
		</aui:layout>
	</div>
</div>
<script type="text/javascript">
YUI({lang: 'ro'}).use('aui-datepicker', 'aui-datatable', 'aui-datatype', 'datatable-sort',
		function(Y) {
// global variable for tables in order to be able use them in other scripts
var dataTable;
var dataTableSplit;
var datahDPI = [];

function formatCurrency(cell) {
    format = {
    	thousandsSeparator: ",",
        decimalSeparator: ".",
        decimalPlaces: noOfDecimalsToDisplay
    };
    return Y.DataType.Number.format(Number(cell.value), format);
}
			
			var colsDPI = [
					{ key: 'id_prod', 				label:'Cod produs'},
					{ key: 'description', 			label:'Denumire produs'},
					{ key: 'unit_price_curr', 		label:'Pret Unitar'},
					{ key: 'quantity', 				label:'Cantitate'},
					{ key: 'total_with_vat_curr', 	label:'Valoare totala'},
					{ key: 'magazin', 				label:'Magazin'},
					{ key: 'clasificare_it', 		label:'Clasificare IT'}
			];
			
			dataTableDPI = new Y.DataTable({
				columns: colsDPI,
				data:datahDPI
			}).render('#tabelDPI');	
			
			/************************* CashExpense Lines Datatable *********************/
			// data array - main table
			var remoteData = <%= ControllerUtils.generateJSONcashExpLines(cashExpenseLines) %>
			var remoteData = [
			                  <% for(int i = 0; i < cashExpenseLines.size(); i++) { 
									if (cashExpenseLines.get(i).get("product_code") != null){
										productCode = DefProduct.getProductCodeWithNameById(cashExpenseLines.get(i).get("product_code").toString());
									} else {
										productCode = "0";
									}
			                  		%>
			                	  {
			                		line:               '<%=cashExpenseLines.get(i).get("line").toString()%>',
			                		product_code: 		'<%=productCode %>',
			                		description:	'<%=cashExpenseLines.get(i).get("description").toString()%>',
			                		um:					'<%=cashExpenseLines.get(i).get("um").toString()%>',
			                		quantity:			'<%=cashExpenseLines.get(i).get("quantity").toString()%>',
			                		unit_price_curr: 		'<%=cashExpenseLines.get(i).get("unit_price_curr")%>',
			                		price_no_vat_curr: 		'<%=cashExpenseLines.get(i).get("price_no_vat_curr")%>',
			                		price_vat_curr:		'<%=cashExpenseLines.get(i).get("price_vat_curr")%>',
			                		vat_code: 			'<%=(cashExpenseLines.get(i).get("vat_code") == null ? "24.00" : String.format("%.2f", Float.parseFloat(cashExpenseLines.get(i).get("vat_code").toString() )))%>',
			                		price_with_vat_curr: 		'<%=Float.parseFloat(cashExpenseLines.get(i).get("price_with_vat_curr").toString())%>',
			                		id_ias: 			'<%=DefIAS.getAnalithicGroupById(cashExpenseLines.get(i).get("id_ias").toString())%>',
			        				id_ifrs: 			'<%=DefIFRS.getAnalithicGroupById(cashExpenseLines.get(i).get("id_ifrs").toString())%>',
			        				dt_ias: 			'<%=cashExpenseLines.get(i).get("dt_ias")%>',
			        				dt_ifrs: 			'<%=cashExpenseLines.get(i).get("dt_ifrs")%>',
			        				lot: 				'<%=DefLot.getLotCodeWithNameById(cashExpenseLines.get(i).get("lot").toString())%>',
			        				clasif:             '<%=cashExpenseLines.get(i).get("clasif")%>'
			                	  }
			                	  <%if (i != (cashExpenseLines.size() - 1)) { out.print(","); }%>
			                  <% } %>
			                  ];
			// COLUMN INFO and ATTRIBUTES
			var nestedCols = [
				{ key: 'line', 			label: 'Nr.' },
				{ key: 'product_code', 		label: 'Cod Prod'},
				{ key: 'description',		label: 'Den. articol.'},
				{ key: 'um', 				label: 'UM'},
				{ key: 'quantity', 			label: 'Cant'},
				{ key: 'unit_price_curr', 	label: 'Pret unitar', formatter: formatCurrency},
				{ key: 'price_no_vat_curr', 	label: 'Val fara TVA', 	formatter: formatCurrency, className: 'currencyCol' /*editor: new Y.TextCellEditor(attrNumberEditor)*/},
				{ key: 'price_vat_curr', className: 'hiddencol'},
				{ key: 'vat_code', 			label: 'Cota TVA'},
				{ key: 'price_with_vat_curr', 	label: 'Val cu TVA', formatter: formatCurrency, className: 'currencyCol' 	/*editor: new Y.TextCellEditor(attrNumberEditor), formatter: formatCurrency*/},			
				{ key: 'id_ias', 	label: 'Grupa IAS'},
				{ key: 'id_ifrs', 	label: 'Grupa IFRS'},
				{ key: 'dt_ias', 		label: 'Dt. IAS'},
				{ key: 'dt_ifrs', 		label: 'Dt. IFRS'},
				{ key: 'lot', 			label: 'Lot'},
				{ key: 'clasif', 		label: 'Clasif.'}
			];

			// TABLE INIT
			dataTable = new Y.DataTable({
			    columns: nestedCols,
			    data: remoteData,
			    editEvent: 'click',
			    recordType: ['line', 'product_code', 'description', 'um', 'quantity', 'unit_price_curr', 
			                 'price_no_vat_curr', 'price_vat_curr', 'vat_code', 'price_with_vat_curr', 'id_ias', 'id_ifrs', 'dt_ias', 'dt_ifrs', 'lot', 'clasif']
			}).render('#alocare-articole');
			
			/************************* CashExpense StoreLines Datatable *********************/
			// bottom table values
			var remoteDataSplit = [
			                       <% for(int i = 0; i < cashExpStoreLines.size(); i++) { %>
			                       	{
			                       		line:				'<%=cashExpStoreLines.get(i).get("id_cash_exp_line")%>',
			                       		description:			'<%=cashExpStoreLines.get(i).get("description")%>',
			                       		quantity:				'<%=cashExpStoreLines.get(i).get("quantity")%>',
			                       		price_no_vat_curr: 			'<%=cashExpStoreLines.get(i).get("price_no_vat_curr")%>',
			                       		id_store: 					'<%=DefStore.getStoreNameById(cashExpStoreLines.get(i).get("id_store").toString())%>',
			                       		id_aisle: 					'<%=cashExpStoreLines.get(i).get("id_aisle")%>',
			                       		associated_acc: 				'<%=cashExpStoreLines.get(i).get("associated_acc")%>',
			                       		id_tip_inreg: 				'<%=DefRegEquip.getNameById(cashExpStoreLines.get(i).get("id_tip_inreg").toString())%>',
			                       		id_act_mf: 				'<%=DefMFAct.getNameById(cashExpStoreLines.get(i).get("id_act_mf").toString())%>',
			                       		gestiune: 		'<%=DefCatGest.getNameById(cashExpStoreLines.get(i).get("gestiune").toString())%>',
			                       		id_dpi:					'<%=cashExpStoreLines.get(i).get("id_dpi")%>',
			                       		vizualizare_dpi:				'<%=cashExpStoreLines.get(i).get("id_dpi")%>',
			                       		id_tip_inventar: 			('<%=cashExpStoreLines.get(i).get("id_tip_inventar")%>' == "2") ? "De grup" : "Individual",
			                       		detalii:				<%=cashExpStoreLines.get(i).get("id")%>,
			                       		inventory : 			'<%=cashExpStoreLines.get(i).get("inventory")%>'
			                       	}
			                        <%if (i != (cashExpStoreLines.size() - 1)) { out.print(","); }%>
			                       <% } %>
			                       ]; 
			// bottom table header
			var nestedColsSplit = [
				{ key: 'line', className: 'hiddencol'},
			   	{ key: 'description', label: 'Den. articol.'},
				{ key: 'quantity', label:  'Cant'},
				{ key: 'price_no_vat_curr', label: 'Val fara TVA', formatter: formatCurrency},
			   	{ key: 'id_store', label: 'Magazin'},
				{ key: 'id_aisle', label: 'Raion/Dep.'},
				{ key: 'associated_acc', label: 'Cont'}, 
				{ key: 'id_tip_inreg', label: 'Tip inregistrare'},
				{ key: 'id_act_mf', label: 'Actiune MF'},
				{ key: 'gestiune', label : 'Gestiune'},
				{ key: 'id_dpi', label: 'Nr. DPI'},
	   			{ key: 'vizualizare_dpi', label: 'Link DPI', 
	   					allowHTML: true, 
	   					formatter: function(o) { 
	   						return '<a href="#" class="dpilink" id="dpi_' + o.value + '">DPI</a>';
	   					},
	   					emptyCellValue: '<a href="#" class="dpilink" id="dpi_{value}">DPI</a>'},
				{ key: 'id_tip_inventar', label: 'Tip nr. inventar'},
				{ key: 'detalii', label: 'Nr. inventar', allowHTML: true, 
					// gets the id of the row and creates an unique link for every row
					// formatter: '<a href="#" id="line-{nr}" class="proddet">detalii</a>', 
					formatter: function(o) {
						return '<a href="#" id="line-' + o.value + '" class="proddet">detalii</a>';
					}, 
					emptyCellValue: '<a href="#" id="line-{line}" class="proddet">detalii</a>', 
				},
				{ key: 'inventory', 		className: 'hiddencol'},
			];
			
			// TABLE INIT - bottom
			dataTableSplit = new Y.DataTable({
			    columns: nestedColsSplit,
			    data: remoteDataSplit,
			    editEvent: 'click',
			    recordType: ['line','description','id_store', 'quantity','price_no_vat_curr','id_aisle','associated_acc','id_tip_inreg','id_act_mf','gestiune','id_dpi','vizualizare_dpi',
			                 'id_tip_inventar','detalii','inventory']
			}).render('#alocare-linii');
			
			
			/************************* Inventory Pop-up *********************/
			var remoteDataInv = []; 
     		
			var nestedColsInv = [
			      			{ key: 'nr_inv', 	label: 'Nr. inv.'},
			      			{ key: 'data_pif', 	label: 'Data PIF'},
			      			{ key: 'receptie', 	label: 'Receptie'}
			      			];
	  		
	  		// TABLE INIT
	  		dataTableInv = new Y.DataTable({
	  		    columns: nestedColsInv,
	  		    data: remoteDataInv,
	  		    editEvent: 'click'
	  		}).render('#detalii-inventar');
	  		
	  		dataTableSplit.delegate("click", function(e){
				e.preventDefault();
				var loadedInventory = "";
				var idStoreLine = e.target.getAttribute("id").replace('line-', '');
				console.log(idStoreLine);
				makeAjaxToGetInventoryDetails(idStoreLine, e, Y);
			}, ".proddet", dataTableSplit);
	  		
	  		dataTableSplit.delegate('click', function (e) {
	   			e.preventDefault();
	   		    var target = e.target.get('id');
	   		    var dpiId = target.replace('dpi_','');
	   		 	showDPIPopupInvoice(e.target, dpiId,'<portlet:namespace/>', '<%=ajaxURL%>', Y);
			}, '.dpilink', dataTableSplit);
			
			function makeAjaxToGetInventoryDetails(idCashexp, e, Y){
	   			$.ajax({
					type: "POST",
					url: '<%=ajaxURL%>',
					data: 	"<portlet:namespace />action=getInventoryDetailsCash" +
							"&<portlet:namespace />idCashexp=" + idCashexp,
					success: function(msgjson) {
						console.log(msgjson);
						if (msgjson != "" && msgjson.length > 2 ) {
						        var jsonEntries = jQuery.parseJSON(msgjson);
								dataTableInv.set('data', eval(jsonEntries));
									
								    // prepare overlay
								    var currentNode = this;
									var overlay = new Y.Overlay({
									    srcNode:"#overlay-detalii-inv",
									    width:"400px",
									    align: {
									        node: e.target,
									        points:["bl", "tl"]
									    }
									});
									
									overlay.render();

								    $('#overlay-detalii-inv').show();
							} else {
								alert("Nu exista numar de inventar pe aceasta linie");
								return;
							}
					},
					error: function(msg) {
						alert('A aparut o problema in preluarea datelor de pe server. Va rugam incercati din nou!');
					}
				});
	   		}
			
			$("#<portlet:namespace/>finvcancel").click(function () { $('#overlay-detalii-inv').hide(); });
			$("#<portlet:namespace/>salveaza-inv").click(function () { $('#overlay-detalii-inv').hide(); });
});
</script>
