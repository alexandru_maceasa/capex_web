<%@page import="com.profluo.ecm.model.db.DefStore"%>
<%@page import="com.profluo.ecm.model.db.DefTipFact"%>
<%@page import="com.profluo.ecm.model.db.DefRelComer"%>
<%@page import="com.profluo.ecm.model.db.DefCompanies"%>
<%@page import="java.util.ResourceBundle"%>
<%@page import="com.profluo.ecm.controller.ControllerUtils"%>
<%@page import="com.profluo.ecm.model.db.DefSupplier"%>
<%@page import="com.profluo.ecm.model.db.WorkEquipmentHeader"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.List"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme"%>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui"%>
<%@ taglib uri="http://alloy.liferay.com/tld/aui" prefix="aui"%>
<%@ taglib uri="http://liferay.com/tld/util" prefix="liferay-util"%>

<liferay-theme:defineObjects />
<portlet:defineObjects />

<portlet:renderURL var="portletURL"></portlet:renderURL>

<portlet:resourceURL id="ajaxURL" var="ajaxURL"/>

<portlet:renderURL var="backURL">
	<portlet:param name="mvcPath" value="/html/exporteddocs/view.jsp"></portlet:param>
</portlet:renderURL>

<%
	locale 					= renderRequest.getLocale();
	ResourceBundle resmain 	= portletConfig.getResourceBundle(locale);
	String id = "";
	try {
		id				= renderRequest.getParameter("id").toString();
	} catch (Exception e){}
	
	//id = "2";
	
	List<HashMap<String,Object>> oneWorkEquipment 					= WorkEquipmentHeader.getHeaderById(id, "working_equipment_header");
	List<HashMap<String,Object>> workEquipmentStoreLinesByWorkEqNr	= WorkEquipmentHeader.getStoreLinesById(id, "working_equipment_store_line");
	List<HashMap<String,Object>> supplierInfo 						= DefSupplier.getSupplierInfo(oneWorkEquipment.get(0).get("id_supplier").toString());
	List<HashMap<String,Object>> workEquipmentLinesByWorkEqNr 		= WorkEquipmentHeader.getLinesById(id, "working_equipment_line");
%>
<aui:layout>
	<a href="<%=portletURL%>" class="btn btn-primary-red fr" style="margin:0px">Inapoi</a>
	<% if(oneWorkEquipment.get(0).get("image") != null) { %>
		<a href="<%=ControllerUtils.alfrescoURLOld + oneWorkEquipment.get(0).get("image") %>" <%--+ ControllerUtils.alfrescoDirectLoginOld  --%>
		class="btn btn-primary fr" style="margin-right:10px" target="_blank">Vizualizare Factura</a>
	<% } %>
</aui:layout>
<aui:layout>
	<aui:fieldset>
		<legend><strong>Factura</strong></legend>
		<aui:column columnWidth="10" first="true">
			<label for="<portlet:namespace />inv_number" style="padding-top: 5px">Numar factura</label>
		</aui:column>
		<aui:column columnWidth="20">
			<aui:input type="text" id="inv_number" name="inv_number" disabled="true" value="<%=oneWorkEquipment.get(0).get(\"inv_number\") %>" label=""/>
		</aui:column>
		<aui:column columnWidth="10">
			<label for="<portlet:namespace/>inv_date2" style="padding-top: 5px">Data factura</label>
		</aui:column>
		<aui:column columnWidth="20">
			<aui:input type="date" id="inv_date2" name="inv_date2" disabled="true" value="<%=oneWorkEquipment.get(0).get(\"inv_date\") %>" 
			placeholder="aaaa-ll-zz" label=""/>
		</aui:column>
		<aui:column columnWidth="10">
			<label for="<portlet:namespace />company"
				style="padding-top: 5px"><%=resmain.getString("societate")%></label>
		</aui:column>
		<aui:column columnWidth="20">
			<aui:input type="text" id="company" name="company" disabled="true" value="<%=DefCompanies.getCompanyNameById(oneWorkEquipment.get(0).get(\"id_company\").toString()) %>" label=""/>
		</aui:column>
	</aui:fieldset>
	<aui:fieldset>
		<aui:layout>
			<aui:column columnWidth="10" first="true">
				<label for="<portlet:namespace />tip-rel-comerciala"
					style="padding-top: 5px"><%=resmain.getString("tip-rel-comerciala")%></label>
			</aui:column>
			<aui:column columnWidth="20">
				<aui:input type="text" id="tip-rel-comerciala" disabled="true" name="tip-rel-comerciala" value="<%=DefRelComer.getTipRelComNameById(Integer.parseInt(oneWorkEquipment.get(0).get(\"tip_rel_com\").toString())) %>" label=""/>
			</aui:column>
			<aui:column columnWidth="10" id = "label_contract_display">
				<label for="<portlet:namespace />nr-contract" style="padding-top: 5px"><%=resmain.getString("nr-contract")%></label>
			</aui:column>
			<aui:column columnWidth="20" id="numar_contract_display">
				<aui:input id="nr-contract" name="nr-contract" label="" disabled="true" value = "<%=oneWorkEquipment.get(0).get(\"co_number\") %>">
				</aui:input>
			</aui:column>
			<aui:column columnWidth="10" id="label_comanda_display">
				<label for="<portlet:namespace />nr-comanda" style="padding-top: 5px"><%=resmain.getString("nr-comanda")%></label>
			</aui:column>
			<aui:column columnWidth="20" id="numar_comanda_display">
				<aui:input id="nr-comanda" name="nr-comanda" label="" disabled="true" value = "<%=oneWorkEquipment.get(0).get(\"order_no\") %>">
				</aui:input>
			</aui:column>
		</aui:layout>
	</aui:fieldset>
	<aui:fieldset>
		<aui:layout>
			<aui:column columnWidth="10" first="true">
				<label for="<portlet:namespace />tip-factura"
					style="padding-top: 5px"><%=resmain.getString("tip-factura")%></label>
			</aui:column>
			<aui:column columnWidth="20">
				<aui:input id="tip-factura" name="tip-factura" label="" disabled="true" value = "<%=DefTipFact.getNameById(Integer.parseInt(oneWorkEquipment.get(0).get(\"tip_fact\").toString())) %>"/>
			</aui:column>
			<aui:column columnWidth="10">
				<label for="<portlet:namespace />created"
					style="padding-top: 5px"><%=resmain.getString("datainregistrare")%></label>
			</aui:column>
			<aui:column columnWidth="20">
				<aui:input type="text" id="created" name="created" disabled="true" value="<%=oneWorkEquipment.get(0).get(\"created\") %>" 
				placeholder="aaaa-ll-zz" label="" readonly="true">
				</aui:input>
			</aui:column>
		</aui:layout>
	</aui:fieldset>
	<aui:fieldset>
		<legend><strong>Furnizor</strong></legend>
		<aui:layout>
			<aui:column columnWidth="10" first="true">
				<label for="<portlet:namespace />furnizor" style="padding-top: 5px">Furnizor</label>
			</aui:column>
			<aui:column columnWidth="20">
				<aui:input type="text" id="furnizor" name="furnizor" label="" disabled="true" value="<%=supplierInfo.get(0).get(\"name\") %>"/>
			</aui:column>
			<aui:column columnWidth="10" first="true">
				<label for="<portlet:namespace />cui" style="padding-top: 5px">CUI</label>
			</aui:column>
			<aui:column columnWidth="20">
				<aui:input type="text" id="cui" name="cui" label="" disabled="true" value="<%=supplierInfo.get(0).get(\"cui\") %>"/>
			</aui:column>
			<aui:column columnWidth="10" first="true">
				<label for="<portlet:namespace />account" style="padding-top: 5px">Cont bancar</label>
			</aui:column>
			<aui:column columnWidth="20">
				<aui:input type="text" id="account" name="account" label="" disabled="true" value="<%=supplierInfo.get(0).get(\"bank_account\") %>"/>
			</aui:column>
		</aui:layout>
		<aui:layout>
			<aui:column columnWidth="10" first="true">
				<label for="<portlet:namespace />cont_kontan" style="padding-top: 5px">Cont Kontan</label>
			</aui:column>
			<aui:column columnWidth="20">
				<aui:input type="text" id="cont_kontan" name="cont_kontan" label="" disabled="true" value="<%=oneWorkEquipment.get(0).get(\"kontan_acc\") %>"/>
			</aui:column>
			<aui:column columnWidth="10" first="true">
				<label for="<portlet:namespace />tip_furnizor" style="padding-top: 5px">Tip furnizor</label>
			</aui:column>
			<aui:column columnWidth="20">
				<aui:input type="text" id="tip_furnizor" name="tip_furnizor" label="" disabled="true" value="<%=supplierInfo.get(0).get(\"tip_furnizor\") %>"/>
			</aui:column>
		</aui:layout>
	</aui:fieldset>
	<aui:fieldset>
		<legend><strong><%=resmain.getString("info-factura")%></strong></legend>
		<aui:layout>
			<aui:column columnWidth="10" first="true">
				<label for="<portlet:namespace />datascadenta" style="padding-top: 5px"><%=resmain.getString("datascadenta")%></label>
			</aui:column>
			<aui:column columnWidth="20">
				<aui:input type="text" id="datascadenta" name="datascadenta" disabled="true" value="<%=oneWorkEquipment.get(0).get(\"due_date\") %>" placeholder="aaaa-ll-zz" label="">
				</aui:input>
			</aui:column>
			<aui:column columnWidth="10" first="true">
				<label for="<portlet:namespace />currency" style="padding-top: 5px"><%=resmain.getString("moneda")%></label>
			</aui:column>
			<aui:column columnWidth="20">
				<aui:input type="text" id="moneda" name="moneda" label="" disabled="true" value="<%=oneWorkEquipment.get(0).get(\"currency\") %>"/>
			</aui:column>
			<aui:column columnWidth="10" first="true">
				<label for="<portlet:namespace />exchange_rate" style="padding-top: 5px">Rata de schimb</label>
			</aui:column>
			<aui:column columnWidth="20">
				<aui:input type="text" id="exchange_rate" name="exchange_rate" label="" disabled="true" value="<%=oneWorkEquipment.get(0).get(\"exchange_rate\") %>"/>
			</aui:column>
		</aui:layout>
	</aui:fieldset>
	<aui:fieldset>
		<aui:column columnWidth="10" first="true">
			<label for="<portlet:namespace />tva" style="padding-top: 5px">Cota TVA</label>
		</aui:column>
		<aui:column columnWidth="20">
			<aui:input type="text" id="tva" name="tva" label="" disabled="true" value="<%=workEquipmentLinesByWorkEqNr.get(0).get(\"vat\") %>"/>
		</aui:column>
	</aui:fieldset>
	<aui:fieldset>
		<legend><strong>Valoare RON</strong></legend>
		<aui:layout>
			<aui:column columnWidth="10" first="true">
				<label for="<portlet:namespace />val_no_vat_ron" style="padding-top: 5px">Valoare fara TVA</label>
			</aui:column>
			<aui:column columnWidth="20">
				<aui:input type="text" id="val_no_vat_ron" name="val_no_vat_ron" label="" disabled="true" value="<%=oneWorkEquipment.get(0).get(\"total_no_vat_ron\") %>"/>
			</aui:column>
			<aui:column columnWidth="10" first="true">
				<label for="<portlet:namespace />val_vat_ron" style="padding-top: 5px">Valoare TVA</label>
			</aui:column>
			<aui:column columnWidth="20">
				<aui:input type="text" id="val_vat_ron" name="val_no_vat_ron" label="" disabled="true" value="<%=oneWorkEquipment.get(0).get(\"total_vat_ron\") %>"/>
			</aui:column>
			<aui:column columnWidth="10" first="true">
				<label for="<portlet:namespace />val_with_vat_ron" style="padding-top: 5px">Valoare cu TVA</label>
			</aui:column>
			<aui:column columnWidth="20">
				<aui:input type="text" id="val_with_vat_ron" name="val_with_vat_ron" label="" disabled="true" value="<%=oneWorkEquipment.get(0).get(\"total_with_vat_ron\") %>"/>
			</aui:column>
		</aui:layout>
	</aui:fieldset>
		<legend><strong>Linii factura</strong></legend>
		<aui:layout>
			<aui:column first="true" columnWidth="100" last="true">
				<div id="alocare-magazine"></div>
			</aui:column>
		</aui:layout>
</aui:layout>
<div id="vizualizareDPI" class="yui3-overlay-loading"
	style="right: 100px; z-index: 1; position: absolute; width: 800px; display: none">
	<div class="yui3-widget-hd">Solicitare vizualizare DPI</div>
	<div class="yui3-widget-bd">
		<aui:layout>
			<aui:fieldset>
				<aui:column columnWidth="20">
					<label for="<portlet:namespace />nr_dpi" style="padding-top: 5px">Nr
						DPI: </label>
				</aui:column>
				<aui:column columnWidth="30">
					<aui:input id="nr_dpi" name="nr_dpi" label="" readonly="true"></aui:input>
				</aui:column>
				<aui:column columnWidth="20">
					<label for="<portlet:namespace />societate"
						style="padding-top: 5px">Societate:</label>
				</aui:column>
				<aui:column columnWidth="30">
					<aui:input id="societate" name="societate" label="" readonly="true"></aui:input>
				</aui:column>
			</aui:fieldset>

			<aui:fieldset>
				<aui:column columnWidth="20">
					<label for="<portlet:namespace />data_dpi" style="padding-top: 5px">Data
						DPI: </label>
				</aui:column>
				<aui:column columnWidth="30">
					<aui:input id="data_dpi" name="data_dpi" label="" readonly="true"></aui:input>
				</aui:column>
				<aui:column columnWidth="20">
					<label for="<portlet:namespace />categ_capex_dpi"
						style="padding-top: 5px">Categorie Capex:</label>
				</aui:column>
				<aui:column columnWidth="30">
					<aui:input id="categ_capex_dpi" name="categ_capex_dpi" label=""
						readonly="true"></aui:input>
				</aui:column>
			</aui:fieldset>

			<aui:fieldset>
				<aui:column columnWidth="20">
					<label for="<portlet:namespace />solicitant"
						style="padding-top: 5px">Solicitant: </label>
				</aui:column>
				<aui:column columnWidth="30">
					<aui:input id="solicitant" name="solicitant" label=""
						readonly="true"></aui:input>
				</aui:column>
			</aui:fieldset>

			<aui:fieldset>
				<aui:column columnWidth="20">
					<label for="<portlet:namespace />aprobator"
						style="padding-top: 5px">Aprobator: </label>
				</aui:column>
				<aui:column columnWidth="80">
					<aui:input id="aprobator" name="aprobator" label="" readonly="true"
						style="width: 97.6%"></aui:input>
				</aui:column>
			</aui:fieldset>
			<aui:layout>
				<aui:column columnWidth="100" last="true" first="true">
					<div id="tabelDPI"></div>
				</aui:column>
			</aui:layout>
			<aui:layout>
				<aui:column columnWidth="50" last="true" style="text-align:right">
					<aui:input value="Inchide" name="fcancel" type="button" label=""
						cssClass="btn btn-primary-red fr"
						onclick="$('#vizualizareDPI').hide()"></aui:input>
					<div class="clear"></div>
				</aui:column>
			</aui:layout>
		</aui:layout>
	</div>
</div>
<script type="text/javascript">
YUI({lang: 'ro'}).use( 'aui-datatable', 'aui-datatype', 'datatable-sort', 'overlay', 'aui-form-validator', 'datatable-mutable',
		function(Y) {
			var remoteDataSplit = [
                       <% int count = 1; 
                       int noOfCommas = workEquipmentLinesByWorkEqNr.size() - 1; %>   
   					<%for (int i = 0; i < workEquipmentLinesByWorkEqNr.size(); i++ ) { %>
           			{
           				invoice_line_id	: <%=workEquipmentLinesByWorkEqNr.get(i).get("id")%>,
           				delete_line 	: '<%=i+1%>',
        				inv_number		: '<%=workEquipmentLinesByWorkEqNr.get(i).get("inv_number")%>',
        				<%if(workEquipmentLinesByWorkEqNr.get(i).get("currency").toString().equals("RON")){%>
        				unit_price_ron 	: <%=workEquipmentLinesByWorkEqNr.get(i).get("unit_price_curr")%>,
        				price_no_vat_ron: '<%=workEquipmentLinesByWorkEqNr.get(i).get("price_no_vat_curr")%>',
        				price_vat_ron 	: '<%=workEquipmentLinesByWorkEqNr.get(i).get("price_vat_curr")%>',
        				<%} else {%>
        				unit_price_ron 	: <%=workEquipmentLinesByWorkEqNr.get(i).get("unit_price_ron")%>,
        				price_no_vat_ron: '<%=workEquipmentLinesByWorkEqNr.get(i).get("price_no_vat_ron")%>',
        				price_vat_ron 	: '<%=workEquipmentLinesByWorkEqNr.get(i).get("price_vat_ron")%>',
        				<%}%>
        				price_vat_curr 	: '<%=workEquipmentLinesByWorkEqNr.get(i).get("price_vat_curr")%>',
        				currency:'<%=workEquipmentLinesByWorkEqNr.get(i).get("currency")%>',
        				article_code	: '<%=(workEquipmentLinesByWorkEqNr.get(i).get("article_code") == null ? "0" : workEquipmentLinesByWorkEqNr.get(i).get("article_code"))%>', 
        				product_code	: '<%=(workEquipmentLinesByWorkEqNr.get(i).get("product_code")  == null ? "" : workEquipmentLinesByWorkEqNr.get(i).get("product_code"))%>', 
        				description		: '<%=(workEquipmentLinesByWorkEqNr.get(i).get("description") == null ? "" : workEquipmentLinesByWorkEqNr.get(i).get("description"))%>',
        				um: '<%=workEquipmentLinesByWorkEqNr.get(i).get("um")%>',
        				quantity		: '<%=(workEquipmentLinesByWorkEqNr.get(i).get("quantity")  == null ?  "" : workEquipmentLinesByWorkEqNr.get(i).get("quantity"))%>',
        				unit_price_curr	: '<%=workEquipmentLinesByWorkEqNr.get(i).get("unit_price_curr")%>',
        				price_no_vat_curr: '<%=workEquipmentLinesByWorkEqNr.get(i).get("price_no_vat_curr")%>',
        				vat: '<%=(workEquipmentLinesByWorkEqNr.get(i).get("vat") == null ? "24.00" : String.format("%.2f", Float.parseFloat(workEquipmentLinesByWorkEqNr.get(i).get("vat").toString() )))%>',
        				val_cu_tva		: <%=Float.parseFloat(workEquipmentLinesByWorkEqNr.get(i).get("price_no_vat_curr").toString()) + Float.parseFloat(workEquipmentLinesByWorkEqNr.get(i).get("price_vat_curr").toString())%>,
        				warranty		: '<%=(workEquipmentLinesByWorkEqNr.get(i).get("warranty")  == null ? "0" : workEquipmentLinesByWorkEqNr.get(i).get("warranty"))%>',
        				id_ias			: '<%=workEquipmentLinesByWorkEqNr.get(i).get("id_ias")%>',
        				id_ifrs			: '<%=workEquipmentLinesByWorkEqNr.get(i).get("id_ifrs")%>',
        				dt_ras			: '<%=workEquipmentLinesByWorkEqNr.get(i).get("dt_ras")%>',
        				dt_ifrs			: '<%=workEquipmentLinesByWorkEqNr.get(i).get("dt_ifrs")%>',
        				lot				: '<%=workEquipmentLinesByWorkEqNr.get(i).get("lot")%>',
        				clasif			: '<%=workEquipmentLinesByWorkEqNr.get(i).get("clasif")%>',
        				cont_imob_in_fct:'<%=workEquipmentLinesByWorkEqNr.get(i).get("cont_imob_in_fct")%>',
             		    cont_imob_in_curs:'<%=workEquipmentLinesByWorkEqNr.get(i).get("cont_imob_in_curs")%>',
             		    cont_imob_avans:'<%=workEquipmentLinesByWorkEqNr.get(i).get("cont_imob_avans")%>',
        				detalii			: <%=workEquipmentLinesByWorkEqNr.get(i).get("line")%>,
           				id_store_line : '<%=workEquipmentStoreLinesByWorkEqNr.get(i).get("id")%>',
           				store_name : '<%=DefStore.getStoreNameById(workEquipmentStoreLinesByWorkEqNr.get(i).get("id_store").toString())%>',
           				id_store : '<%=workEquipmentStoreLinesByWorkEqNr.get(i).get("id_store")%>',
           				unit_price: '<%=workEquipmentLinesByWorkEqNr.get(i).get("unit_price_curr")%>',
           				id :'<%=workEquipmentStoreLinesByWorkEqNr.get(i).get("id")%>',
           				id_department : '<%=workEquipmentStoreLinesByWorkEqNr.get(i).get("id_department")%>',
           				associated_acc:'<%=(workEquipmentLinesByWorkEqNr.get(i).get("cont_imob_in_curs") == null ? 0 : workEquipmentLinesByWorkEqNr.get(i).get("cont_imob_in_curs"))%>',
           				<% if (workEquipmentStoreLinesByWorkEqNr.get(i).get("reception_date").toString().compareTo(oneWorkEquipment.get(0).get("inv_date").toString()) > 0){ %>
           					id_tip_inreg: '<%=(workEquipmentStoreLinesByWorkEqNr.get(i).get("id_tip_inreg") == null ? 1 : workEquipmentStoreLinesByWorkEqNr.get(i).get("id_tip_inreg"))%>',
       					<% } else { %>
           					id_tip_inreg: '<%=(workEquipmentStoreLinesByWorkEqNr.get(i).get("id_tip_inreg") == null ? 2 : workEquipmentStoreLinesByWorkEqNr.get(i).get("id_tip_inreg"))%>',
           				<% } %>
           				id_act_mf 			: '<%=(workEquipmentStoreLinesByWorkEqNr.get(i).get("id_act_mf") == null ? 1 : workEquipmentStoreLinesByWorkEqNr.get(i).get("id_act_mf"))%>',
           				id_cat_gestiune		: '<%=workEquipmentStoreLinesByWorkEqNr.get(i).get("id_cat_gestiune")%>',
           				id_dpi 				:'<%=(workEquipmentStoreLinesByWorkEqNr.get(i).get("id_dpi") == null ? 0 : workEquipmentStoreLinesByWorkEqNr.get(i).get("id_dpi"))%>',
           				vizualizare_dpi		: '<%=(workEquipmentStoreLinesByWorkEqNr.get(i).get("id_dpi") == null ? 0 : workEquipmentStoreLinesByWorkEqNr.get(i).get("id_dpi"))%>',
						id_tip_inventar		: '<%=(workEquipmentStoreLinesByWorkEqNr.get(i).get("id_tip_inventar") == null ? 2 : workEquipmentStoreLinesByWorkEqNr.get(i).get("id_tip_inventar"))%>',
           				initiative 			: '<%=workEquipmentStoreLinesByWorkEqNr.get(i).get("initiative")%>',
           				new_prj 			: '<%=workEquipmentStoreLinesByWorkEqNr.get(i).get("new_prj")%>',
           				pif_date			: '<%=workEquipmentStoreLinesByWorkEqNr.get(i).get("pif_date")%>',
           				reception			: '<%=workEquipmentStoreLinesByWorkEqNr.get(i).get("reception")%>',
           				inventory_no		: '<%=workEquipmentStoreLinesByWorkEqNr.get(i).get("inventory_no")%>'
           			}
           			<%if (noOfCommas > 0) { out.print(","); noOfCommas--;count++;}%>
   	        	<%}%>
                       
                       
                       ];
//INVOICE STORE LINE COLUMN DEFINITION

var nestedColsSplit = [
	{ key: 'invoice_line_id', 	className: 'hiddencol'},  
	{ key: 'id_store_line', 	className: 'hiddencol'},    
	{ key: 'delete_line', className: 'hiddencol'},
	{ key: 'inv_number', 		className: 'hiddencol'},
	{ key: 'unit_price_ron', 	className: 'hiddencol'},
	{ key: 'price_no_vat_ron', 	className: 'hiddencol'},
	{ key: 'price_vat_ron', 	className: 'hiddencol'}, 
	{ key: 'price_vat_curr', 	className: 'hiddencol'},
    { key: 'currency', 			className: 'hiddencol'}, 
	{ key: 'article_code', 		className: 'hiddencol'},
	{ key: 'product_code', 		label: 'Cod Prod', },
	{ key: 'description', 		label: 'Den. articol'},
	{ key: 'um', 				label: 'UM'},
	{ key: 'quantity', 			label:  'Cant'},
	{ key: 'unit_price_curr', 	label: 'Pret unitar'},		
	{ key: 'price_no_vat_curr', label: 'Val fara TVA'},		
	{ key: 'vat', 				label: 'Cota TVA' },
	{ key: 'val_cu_tva', 		label: 'Val cu TVA'},
	{ key: 'id_ias', 			label: 'Grupa IAS' },
	{ key: 'id_ifrs', 	label: 'Grupa IFRS' },
	{ key: 'dt_ras', 		label: 'Dt. IAS'},
	{ key: 'dt_ifrs', 		label: 'Dt. IFRS'},
	{ key: 'lot', 			label: 'Lot' },
	{ key: 'clasif', 		label: 'Clasif.' },
    { key: 'cont_imob_in_fct', className: 'hiddencol'},
    { key: 'cont_imob_in_curs', className: 'hiddencol'},
    { key: 'cont_imob_avans', className: 'hiddencol'},
	    { key: 'unit_price', className: 'hiddencol'},
	{ key: 'store_name', label: 'Magazin', className :'stores' },
	{ key: 'id_store', className: 'hiddencol' },
		{ key: 'id_department', label: 'Raion/Dep.' },
	{ key: 'associated_acc', label: 'Cont' },
	{ key: 'id_tip_inreg', label: 'Tip inregistrare' },
		{ key: 'id_act_mf', label: 'Actiune MF' },
		{ key: 'id_cat_gestiune', label : 'Gestiune' },
		{ key: 'id_dpi', label: 'Nr. DPI' },
		{ key: 'vizualizare_dpi', label: 'Link DPI', 
				allowHTML: true, 
				formatter: function(o) { return '<a href="#" class="dpilink" id="dpi_' + o.data.nr + '_' + o.value + '">DPI</a>'; },
				emptyCellValue: '<a href="#" class="dpilink" id="dpi_{nr}_{value}">DPI</a>'},
		{ key: 'id_tip_inventar', label: 'Tip nr. inventar' },
	{ key: 'initiative', 	label: 'Initiative' },
		{ key: 'new_prj', 	label: 'Proiecte Noi' },
		{ key: 'inventory_no', 		label: 'Nr. Inventar'},
		{ key: 'pif_date', 			label: 'Data PIF'},
		{ key: 'reception', 		label: 'Receptie'}
		
];
		
	// INVOICE STORE LINE TABLE INIT
	dataTableSplit = new Y.DataTable({
	    columns: nestedColsSplit,
	    data: remoteDataSplit,
	 	scrollable: "x",
    	width: "100%",
	    editEvent: 'click',
	    recordType: ['invoice_line_id', 'delete_line', 'inv_number', 'unit_price_ron', 'price_no_vat_ron', 'price_vat_ron', 'price_vat_curr', 'currency', 'article_code', 
		              'product_code', 'description', 'um', 'quantity', 'unit_price_curr', 'price_no_vat_curr', 'vat', 'val_cu_tva', 'id_ias', 'id_ifrs', 
   		           'dt_ras', 'dt_ifrs', 'lot', 'clasif', 'cont_imob_in_fct', 'cont_imob_in_curs', 'cont_imob_avans', 
   		           'id_store', 'store_name', 'unit_price',  'id_department', 'associated_acc', 'id_tip_inreg', 'id_act_mf', 'id_cat_gestiune', 'id_dpi', 'vizualizare_dpi', 
   		           'id_tip_inventar', 'initiative', 'new_prj', 'detalii', 'inventory_no', 'pif_date', 'reception']
	}).render('#alocare-magazine');
	
	var datahDPI = [];
	
	var colsDPI = [
		{ key: 'id_prod', 				label:'Cod produs'},
		{ key: 'description', 			label:'Denumire produs'},
		{ key: 'unit_price_curr', 		label:'Pret Unitar'},
		{ key: 'quantity', 				label:'Cantitate'},
		{ key: 'total_with_vat_curr', 	label:'Valoare totala'},
		{ key: 'magazin', 				label:'Magazin'},
		{ key: 'clasificare_it', 		label:'Clasificare IT'}
	];
	
	dataTableDPI = new Y.DataTable({
	    columns: colsDPI,
	    data:datahDPI
	}).render('#tabelDPI');	
	
	// click on dpi link
	dataTableSplit.delegate('click', function (e) {
		e.preventDefault();
	    var target = e.target.get('id');
	    var ids = target.replace('dpi_','').split("_");
	    var lineid = ids[0];
	    var dpiId = ids[1];
	 	showDPIPopupInvoice(e.target, dpiId,'<portlet:namespace/>', '<%=ajaxURL%>', Y);
	}, '.dpilink', dataTableSplit);
});
</script>