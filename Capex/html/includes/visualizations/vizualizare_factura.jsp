<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<%@page import="com.profluo.ecm.flows.InvoiceStages"%>
<%@page import="java.util.Date"%>
<%@page import="com.profluo.ecm.model.db.DefCatGest"%>
<%@page import="com.profluo.ecm.model.db.DefMFAct"%>
<%@page import="com.profluo.ecm.model.db.DefRegEquip"%>
<%@page import="com.profluo.ecm.model.db.DefStore"%>
<%@page import="com.profluo.ecm.model.db.DefLot"%>
<%@page import="com.profluo.ecm.model.db.DefIFRS"%>
<%@page import="com.profluo.ecm.model.db.DefIAS"%>
<%@page import="com.profluo.ecm.model.db.DefProduct"%>
<%@page import="com.profluo.ecm.model.db.DefNewProj"%>
<%@page import="com.profluo.ecm.model.db.ListaInit"%>
<%@page import="com.profluo.ecm.model.db.DefTipFact"%>
<%@page import="com.profluo.ecm.model.db.DefRelComer"%>
<%@page import="com.profluo.ecm.model.db.DefCompanies"%>
<%@page import="com.profluo.ecm.flows.SupplierStages"%>
<%@page import="com.profluo.ecm.model.db.DatabaseConnectionManager"%>
<%@page import="com.profluo.ecm.model.db.DefSupplier"%>
<%@page import="com.profluo.ecm.model.db.DefExchRates"%>
<%@page import="com.profluo.ecm.controller.ControllerUtils"%>
<%@page import="com.profluo.ecm.model.vo.DefNewProjVo"%>
<%@page import="com.profluo.ecm.model.vo.ListaInitVo"%>
<%@page import="com.profluo.ecm.model.db.InvoiceHeader"%>
<%@page import="com.profluo.ecm.model.vo.DefAttDocsVo"%>
<%@page import="com.profluo.ecm.model.vo.DefVATVo"%>
<%@page import="com.profluo.ecm.model.vo.DefRelComerVo"%>
<%@page import="com.profluo.ecm.model.vo.DefTipFactVo"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.List"%>
<%@page import="com.profluo.ecm.model.vo.DefCurrencyVo"%>
<%@page import="com.profluo.ecm.model.vo.DefCompaniesVo"%>
<%@page import="com.liferay.portal.util.PortalUtil"%>

<%@page import="com.profluo.ecm.flows.Links"%>

<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme"%>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui"%>
<%@ taglib uri="http://alloy.liferay.com/tld/aui" prefix="aui"%>
<%@ taglib uri="http://liferay.com/tld/util" prefix="liferay-util"%>

<%@ page import="java.util.ResourceBundle"%>
<%@ page import="java.util.Locale"%>

<liferay-theme:defineObjects />
<portlet:defineObjects />

<%
	locale 					= renderRequest.getLocale();
	ResourceBundle resmain 	= portletConfig.getResourceBundle(locale);
	String id			= "";
	Double sumaPeLinii 	= 0.00;
	
	//filters
	String invoiceNo 	= "";
	String regDate		= "";
	String supplierName = "";
	String invoiceDate	= "";
	String currency		= "";
	int companyId		= 0;
	int docType			= 0;
	String sourcePage 	= "";
	try {
		invoiceNo					= renderRequest.getParameter("number_filter").toString();
	} catch (Exception e){ }
	try {
		regDate						= renderRequest.getParameter("datainreg_filter").toString();
	} catch (Exception e){ }
	try {
		supplierName				= renderRequest.getParameter("supplier_name_filter").toString();
	} catch (Exception e){ }
	try {
		invoiceDate					= renderRequest.getParameter("datafactura_filter").toString();
	} catch (Exception e){ }
	try {
		currency					= renderRequest.getParameter("moneda_filter").toString();
	} catch (Exception e){ }
	try {
		companyId					= Integer.parseInt(renderRequest.getParameter("company_id_filter").toString());
	} catch (Exception e){ }
	try {
		docType						= Integer.parseInt(renderRequest.getParameter("filter_doc_type").toString());
	} catch (Exception e){ }
	try {
		sourcePage					= renderRequest.getParameter("source_page").toString();
	} catch (Exception e){ }
	//End filters
	
	try {
		id						= renderRequest.getParameter("id").toString();
	} catch (Exception e){
		id						= renderRequest.getParameter("invoiceId").toString();
	}
	
	System.out.println(" ID : " + id);
	
	List<HashMap<String,Object>> oneInvoiceHeader 				= InvoiceHeader.getHeaderById(id, "invoice_header");
	List<HashMap<String,Object>> supplierInfo 					= DefSupplier.getSupplierInfo(oneInvoiceHeader.get(0).get("id_supplier").toString());
	List<HashMap<String,Object>> invoiceLinesByInvoiceNr 		= InvoiceHeader.getLinesById(id, "invoice_line");
	List<HashMap<String,Object>> invoiceStoreLinesByInvoiceNr 	= InvoiceHeader.getStoreLinesByIdWithoutInvNo(id, "invoice_store_line");
	String tipRelCom = "";
	String tipFact = "";
	String vat = "";
	String newProjName = "";
	String productCode = "0";
	if(oneInvoiceHeader.get(0).get("tip_rel_com") != null){
		tipRelCom = DefRelComer.getTipRelComNameById(Integer.parseInt(oneInvoiceHeader.get(0).get("tip_rel_com").toString()));
	}
	if(oneInvoiceHeader.get(0).get("tip_fact") != null){
		tipFact = DefTipFact.getNameById(Integer.parseInt(oneInvoiceHeader.get(0).get("tip_fact").toString()));
	}
	if (oneInvoiceHeader.size() != 0 ){
		vat = oneInvoiceHeader.get(0).get("vat_code").toString();
	}
	if (oneInvoiceHeader.get(0).get("id_new_prj") != null){
		newProjName = DefNewProj.getNewPrjNameById(oneInvoiceHeader.get(0).get("id_new_prj").toString());
	}
	 
%>

<portlet:renderURL var="backToMissingDocsURL">
	<portlet:param name="mvcPath" value="/html/inv_missing_doc/view.jsp"></portlet:param>
</portlet:renderURL>

<portlet:renderURL var="backToExportedDocsURL">
	<portlet:param name="mvcPath" value="/html/exporteddocs/view.jsp"></portlet:param>
</portlet:renderURL>

<portlet:renderURL var="portletURL"></portlet:renderURL>
<portlet:resourceURL id="ajaxURL" var="ajaxURL"/>

<a href="#" class="btn btn-primary-red fr" id="inapoi" style="margin:0px">Inapoi</a>
<!-- data >= 23 atunci alfresco nou, altfel alfresco vechi -->
	<% DateFormat dateFormat = new  SimpleDateFormat("yyyy-MM-dd");
      	   Date invCreateDate = dateFormat.parse(oneInvoiceHeader.get(0).get("created").toString().substring(0, 10));
      	   Date dataReferinta = dateFormat.parse("2018-11-23");
      %>
	<% if(invCreateDate.before(dataReferinta) && oneInvoiceHeader.get(0).get("image") != null) { %>
	<a href="<%=ControllerUtils.alfrescoURLOld + oneInvoiceHeader.get(0).get("image") %>" <%--+ ControllerUtils.alfrescoDirectLoginOld  --%>
		class="btn btn-primary fr" style="margin-right:10px" target="_blank">Vizualizare Factura</a>
	<% } else if (invCreateDate.after(dataReferinta) && oneInvoiceHeader.get(0).get("cid") != null) { %>
		<a onclick="downloadPdfFunction('<%=oneInvoiceHeader.get(0).get("cid") %>')" <%--+ ControllerUtils.alfrescoDirectLoginOld  --%>
		class="btn btn-primary fr" style="margin-right:10px" target="_blank">Vizualizare Factura</a>
	<% } %>
<div class="clear"></div>

<aui:form method="post" id="unId" name="unName">
	<aui:fieldset>
		<legend><strong>Factura</strong></legend>
		<aui:column columnWidth="10" first="true">
			<label for="<portlet:namespace />inv_number" style="padding-top: 5px">Numar factura </label>
		</aui:column>
		<aui:column columnWidth="20">
			<aui:input type="text" id="inv_number" disabled="true" name="inv_number" value="<%=oneInvoiceHeader.get(0).get(\"inv_number\") %>" label=""/>
		</aui:column>
		<aui:column columnWidth="10">
			<label for="<portlet:namespace/>inv_date" style="padding-top: 5px"><%=resmain.getString("datafactura")%></label>
		</aui:column>
		<aui:column columnWidth="20">
			<aui:input type="date" id="inv_date" name="inv_date" disabled="true" value="<%=oneInvoiceHeader.get(0).get(\"inv_date\") %>" 
				placeholder="aaaa-ll-zz" label=""/>
		</aui:column>
		<aui:column columnWidth="10">
			<label for="<portlet:namespace />company"
				style="padding-top: 5px"><%=resmain.getString("societate")%></label>
		</aui:column>
		<aui:column columnWidth="20">
			<aui:input type="text" id="societate" disabled="true" name="societate" value="<%=DefCompanies.getCompanyNameById(oneInvoiceHeader.get(0).get(\"id_company\").toString()) %>" label=""/>
		</aui:column>
	</aui:fieldset>
	<aui:fieldset>
		<aui:layout>
			<aui:column columnWidth="10" first="true">
				<label for="<portlet:namespace />tip-rel-comerciala"
					style="padding-top: 5px"><%=resmain.getString("tip-rel-comerciala")%></label>
			</aui:column>
			<aui:column columnWidth="20">
				<aui:input type="text" id="tip-rel-comerciala" disabled="true" name="tip-rel-comerciala" value="<%=tipRelCom %>" label=""/>
			</aui:column>
			<aui:column columnWidth="10" id = "label_contract_display">
				<label for="<portlet:namespace />nr-contract" style="padding-top: 5px"><%=resmain.getString("nr-contract")%></label>
			</aui:column>
			<aui:column columnWidth="20" id="numar_contract_display">
				<aui:input id="nr-contract" name="nr-contract" label="" disabled="true" value = "<%=oneInvoiceHeader.get(0).get(\"co_number\") %>">
				</aui:input>
			</aui:column>
			<aui:column columnWidth="10" id="label_comanda_display">
				<label for="<portlet:namespace />nr-comanda" style="padding-top: 5px"><%=resmain.getString("nr-comanda")%></label>
			</aui:column>
			<aui:column columnWidth="20" id="numar_comanda_display">
				<aui:input id="nr-comanda" name="nr-comanda" label="" disabled="true" value = "<%=oneInvoiceHeader.get(0).get(\"order_no\") %>">
				</aui:input>
			</aui:column>
		</aui:layout>
	</aui:fieldset>
	
	<aui:fieldset>
		<aui:layout>
			<aui:column columnWidth="10" first="true">
				<label for="<portlet:namespace />tip-factura" style="padding-top: 5px"><%=resmain.getString("tip-factura")%></label>
			</aui:column>
			<aui:column columnWidth="20">
				<aui:input id="tip-factura" name="tip-factura" label="" disabled="true" value = "<%=tipFact %>"/>
			</aui:column>
			<aui:column columnWidth="10">
				<label for="<portlet:namespace />created" style="padding-top: 5px"><%=resmain.getString("datainregistrare")%></label>
			</aui:column>
			<aui:column columnWidth="20">
				<aui:input type="text" id="created" name="created" value="<%=oneInvoiceHeader.get(0).get(\"created\") %>" 
					disabled="true" placeholder="aaaa-ll-zz" label="" readonly="true"/>
			</aui:column>
		</aui:layout>
	</aui:fieldset>
	
	<aui:fieldset>
		<legend><strong>Furnizor</strong></legend>
		<aui:layout>
			<aui:column columnWidth="10" first="true">
				<label for="<portlet:namespace />furnizor" style="padding-top: 5px">Furnizor</label>
			</aui:column>
			<aui:column columnWidth="20">
				<aui:input type="text" id="furnizor" name="furnizor" label="" disabled="true" value="<%=supplierInfo.get(0).get(\"name\") %>"/>
			</aui:column>
			<aui:column columnWidth="10" first="true">
				<label for="<portlet:namespace />cui" style="padding-top: 5px">CUI</label>
			</aui:column>
			<aui:column columnWidth="20">
				<aui:input type="text" id="cui" name="cui" label="" disabled="true" value="<%=supplierInfo.get(0).get(\"cui\") %>"/>
			</aui:column>
			<aui:column columnWidth="10" first="true">
				<label for="<portlet:namespace />account" style="padding-top: 5px">Cont bancar</label>
			</aui:column>
			<aui:column columnWidth="20">
				<aui:input type="text" id="account" name="account" label="" disabled="true" value="<%=supplierInfo.get(0).get(\"bank_account\") %>"/>
			</aui:column>
		</aui:layout>
		<aui:layout>
			<aui:column columnWidth="10" first="true">
				<label for="<portlet:namespace />cont_kontan" style="padding-top: 5px">Cont Kontan</label>
			</aui:column>
			<aui:column columnWidth="20">
				<aui:input type="text" id="cont_kontan" name="cont_kontan" label="" disabled="true" value="<%=oneInvoiceHeader.get(0).get(\"kontan_acc\") %>"/>
			</aui:column>
			<aui:column columnWidth="10" first="true">
				<label for="<portlet:namespace />tip_furnizor" style="padding-top: 5px">Tip furnizor</label>
			</aui:column>
			<aui:column columnWidth="20">
				<aui:input type="text" id="tip_furnizor" name="tip_furnizor" label="" disabled="true" value="<%=oneInvoiceHeader.get(0).get(\"tip_furnizor\") %>"/>
			</aui:column>
		</aui:layout>
	</aui:fieldset>
	
	<aui:fieldset>
		<legend><strong>Info factura</strong></legend>
		<aui:layout>
			<aui:column columnWidth="10" first="true">
				<label for="<portlet:namespace />due_date" style="padding-top: 5px">Data scadentei</label>
			</aui:column>
			<aui:column columnWidth="20">
				<aui:input type="text" id="due_date" name="due_date" label="" disabled="true" value="<%=oneInvoiceHeader.get(0).get(\"due_date\") %>"/>
			</aui:column>
			<aui:column columnWidth="10" first="true">
				<label for="<portlet:namespace />moneda" style="padding-top: 5px">Moneda</label>
			</aui:column>
			<aui:column columnWidth="20">
				<aui:input type="text" id="moneda" name="moneda" label="" disabled="true" value="<%=oneInvoiceHeader.get(0).get(\"currency\") %>"/>
			</aui:column>
			<aui:column columnWidth="10" first="true">
				<label for="<portlet:namespace />exchange_rate" style="padding-top: 5px">Rata de schimb</label>
			</aui:column>
			<aui:column columnWidth="20">
				<aui:input type="text" id="exchange_rate" name="exchange_rate" label="" disabled="true" value="<%=oneInvoiceHeader.get(0).get(\"exchange_rate\") %>"/>
			</aui:column>
		</aui:layout>
		<aui:layout>
			<aui:column columnWidth="10" first="true">
				<label for="<portlet:namespace />tva" style="padding-top: 5px">Cota TVA</label>
			</aui:column>
			<aui:column columnWidth="20">
				<aui:input type="text" id="tva" name="tva" label="" disabled="true" value="<%=vat %>"/>
			</aui:column>
			<aui:column columnWidth="10" first="true">
				<label for="<portlet:namespace />warranty" style="padding-top: 5px">Procent garantie</label>
			</aui:column>
			<aui:column columnWidth="20">
				<aui:input type="text" id="warranty" name="warranty" label="" disabled="true" value="<%=oneInvoiceHeader.get(0).get(\"warranty_proc\") %>"/>
			</aui:column>
		</aui:layout>
	</aui:fieldset>
	<aui:fieldset>
		<legend><strong>Valoare RON</strong></legend>
		<aui:layout>
			<aui:column columnWidth="10" first="true">
				<label for="<portlet:namespace />val_no_vat_ron" style="padding-top: 5px">Valoare fara TVA</label>
			</aui:column>
			<aui:column columnWidth="20">
				<aui:input type="text" id="val_no_vat_ron" name="val_no_vat_ron" label="" disabled="true" value="<%=oneInvoiceHeader.get(0).get(\"total_no_vat_ron\") %>"/>
			</aui:column>
			<aui:column columnWidth="10" first="true">
				<label for="<portlet:namespace />val_vat_ron" style="padding-top: 5px">Valoare TVA</label>
			</aui:column>
			<aui:column columnWidth="20">
				<aui:input type="text" id="val_vat_ron" name="val_no_vat_ron" label="" disabled="true" value="<%=oneInvoiceHeader.get(0).get(\"total_vat_ron\") %>"/>
			</aui:column>
			<aui:column columnWidth="10" first="true">
				<label for="<portlet:namespace />val_with_vat_ron" style="padding-top: 5px">Valoare cu TVA</label>
			</aui:column>
			<aui:column columnWidth="20">
				<aui:input type="text" id="val_with_vat_ron" name="val_with_vat_ron" label="" disabled="true" value="<%=oneInvoiceHeader.get(0).get(\"total_with_vat_ron\") %>"/>
			</aui:column>
		</aui:layout>
	</aui:fieldset>
	<aui:fieldset>
		<legend><strong>Actiuni</strong></legend>
		<aui:layout>
			<aui:column columnWidth="20" first="true">
				<label for="<portlet:namespace />fact-avans-asoc" style="padding-top: 5px">Avans asociat</label>
			</aui:column>
			<% if (oneInvoiceHeader.get(0).get("id_fact_avans").toString().equals("0")) { %>
			<aui:column columnWidth="10">
	             <aui:input label="Da" name="fact-avans-asoc" type="radio" value="1" inlineField="true" disabled="true"/>
	             <aui:input label="Nu" name="fact-avans-asoc" type="radio" value="0" inlineField="true" disabled="true" checked="true"/>
			</aui:column>
			<% } else { %>
			<aui:column columnWidth="10">
	             <aui:input label="Da" name="fact-avans-asoc" type="radio" value="1" inlineField="true" disabled="true" checked="true"/>
	             <aui:input label="Nu" name="fact-avans-asoc" type="radio" value="0" inlineField="true" disabled="true" />
			</aui:column>
			<% } %>
		</aui:layout>
		<aui:layout>
			<aui:column columnWidth="20" first="true">
				<label for="<portlet:namespace />blocked_warranty" style="padding-top: 5px">Garantie blocata la plata</label>
			</aui:column>
			<% if (oneInvoiceHeader.get(0).get("warranty_lock").toString().equals("N")) { %>
			<aui:column columnWidth="10">
	             <aui:input label="Da" name="blocked_warranty" type="radio" value="1" inlineField="true" disabled="true"/>
	             <aui:input label="Nu" name="blocked_warranty" type="radio" value="0" inlineField="true" disabled="true" checked="true"/>
			</aui:column>
			<% } else { %>
			<aui:column columnWidth="10">
	             <aui:input label="Da" name="blocked_warranty" type="radio" value="1" inlineField="true" disabled="true" checked="true"/>
	             <aui:input label="Nu" name="blocked_warranty" type="radio" value="0" inlineField="true" disabled="true" />
			</aui:column>
			<% } %>
		</aui:layout>
		<aui:layout>
			<aui:column columnWidth="20" first="true">
				<label for="<portlet:namespace />missing_docs" style="padding-top: 5px">Lipsa documente justificative</label>
			</aui:column>
			<% if (oneInvoiceHeader.get(0).get("missing_docs") == null || oneInvoiceHeader.get(0).get("missing_docs").toString().equals("")) { %>
			<aui:column columnWidth="10">
	             <aui:input label="Da" name="missing_docs" type="radio" value="1" inlineField="true" disabled="true"/>
	             <aui:input label="Nu" name="missing_docs" type="radio" value="0" inlineField="true" disabled="true" checked="true"/>
			</aui:column>
			<% } else { %>
			<aui:column columnWidth="10">
	             <aui:input label="Da" name="missing_docs" type="radio" value="1" inlineField="true" disabled="true" checked="true"/>
	             <aui:input label="Nu" name="missing_docs" type="radio" value="0" inlineField="true" disabled="true" />
			</aui:column>
			<aui:column columnWidth="10">
				<label for="<portlet:namespace />docs" style="padding-top: 5px">Documente lipsa </label>
			</aui:column>
			<aui:column columnWidth="20">
				<aui:input type="text" id="docs" name="docs" label="" disabled="true" value="<%=oneInvoiceHeader.get(0).get(\"missing_docs\") %>"/>
			</aui:column>
			<% } %>
		</aui:layout>
		<aui:layout>
			<aui:column columnWidth="20" first="true">
				<label for="<portlet:namespace />wrong_invoice" style="padding-top: 5px">Factura eronata</label>
			</aui:column>
			<% if (oneInvoiceHeader.get(0).get("stage").toString().equals("-1")) { %>
			<aui:column columnWidth="10">
	             <aui:input label="Da" name="wrong_invoice" type="radio" value="1" inlineField="true" disabled="true" checked="true"/>
	             <aui:input label="Nu" name="wrong_invoice" type="radio" value="0" inlineField="true" disabled="true" />
			</aui:column>
			<aui:column columnWidth="20">
				<aui:input type="text" id="wrong_invoice_details" name="wrong_invoice_details" label="" disabled="true" value="<%=oneInvoiceHeader.get(0).get(\"error_details\").toString() %>"/>
			</aui:column>
			<% } else { %>
			<aui:column columnWidth="10">
	             <aui:input label="Da" name="wrong_invoice" type="radio" value="1" inlineField="true" disabled="true"/>
	             <aui:input label="Nu" name="wrong_invoice" type="radio" value="0" inlineField="true" disabled="true" checked="true" />
			</aui:column>
			<% } %>
		</aui:layout>
	</aui:fieldset>
	
	<aui:fieldset>
		<legend><strong>Detalii CAPEX</strong></legend>
		<aui:column columnWidth="25">
			<% if (oneInvoiceHeader.get(0).get("tip_capex") != null && oneInvoiceHeader.get(0).get("tip_capex").toString().equals("0")) { %>
				<aui:input type="radio" name="categ_capex" id="capex_diverse" label="Capex Diverse" value="0" disabled="true" checked="true" />
			<% } else { %>
				<aui:input type="radio" name="categ_capex" id="capex_diverse" label="Capex Diverse" value="0" disabled="true" />
			<% } %>
		</aui:column>
		
		<% if (oneInvoiceHeader.get(0).get("tip_capex") != null && oneInvoiceHeader.get(0).get("tip_capex").toString().equals("1")) { %>
		<aui:column columnWidth="10">
			<aui:input type="radio" name="initiative" id="initiative" label="Lista initiative" value="0" disabled="true" checked="true" />
		</aui:column>
		<aui:column columnWidth="15">
			<aui:input type="text" name="init" id="init" disabled="true" label="" value="<%=ListaInit.getInitiativeNameById(oneInvoiceHeader.get(0).get(\"id_initiative\").toString()) %>"/>
		</aui:column>
		<% } else { %>
		<aui:column columnWidth="25">
			<aui:input type="radio" name="initiative" id="initiative" label="Lista initiative" value="0" disabled="true" />
		</aui:column>
		<% } %>
		
		<% if (oneInvoiceHeader.get(0).get("tip_capex") != null && oneInvoiceHeader.get(0).get("tip_capex").toString().equals("2")) { %>
		<aui:column columnWidth="10">
			<aui:input type="radio" name="new_prj" id="new_prj" label="Proiect nou" value="0" disabled="true" checked="true" />
		</aui:column>
		<aui:column columnWidth="15">
			<aui:input type="text" name="new_prj" id="new_prj" disabled="true" label="" value="<%=newProjName %>"/>
		</aui:column>
		<% } else { %>
		<aui:column columnWidth="25">
			<aui:input type="radio" name="new_prj" id="new_prj" label="Proiect nou" value="0" disabled="true" />
		</aui:column>
		<% } %>
	</aui:fieldset>
	
	<aui:fieldset>
		<legend><strong>Linii factura</strong></legend>
		<aui:layout>
			<aui:column first="true" columnWidth="100" last="true">
				<div id="linii_factura"></div>
			</aui:column>
		</aui:layout>
		<legend><strong>Alocare articole pe linii</strong></legend>
		<aui:layout>
			<aui:column first="true" columnWidth="100" last="true">
				<div id="alocare_magazine"></div>
			</aui:column>
		</aui:layout>
	</aui:fieldset>
</aui:form>

<aui:layout>
	<aui:column columnWidth="10" first="true">
		<label for="<portlet:namespace />val_fara_tva" style="padding-top: 5px">Valoare fara TVA</label>
	</aui:column>
	<aui:column columnWidth="20">
		<aui:input type="text" id="val_fara_tva" name="val_fara_tva" label="" disabled="true" value="<%=oneInvoiceHeader.get(0).get(\"total_no_vat_curr\") %>"/>
	</aui:column>
	<aui:column columnWidth="15" first="true">
		<label for="<portlet:namespace />val_fara_tva_linii" style="padding-top: 5px">Valoare fara TVA alocarepe linii</label>
	</aui:column>
	<aui:column columnWidth="20">
		<aui:input type="text" id="val_fara_tva_linii" name="val_fara_tva_linii" label="" disabled="true" value="<%=sumaPeLinii%>"/>
	</aui:column>
</aui:layout>

<aui:layout>
	<legend><strong>Valori factura</strong></legend>
	<aui:column>
		<label for="<portlet:namespace />val-netaplata-ron" style="padding-top: 5px">Valoare neta de plata</label>
	</aui:column>
	<aui:column>
		<aui:input type="text" id="val-netaplata-ron" name="val-netaplata-ron" value="<%=oneInvoiceHeader.get(0).get(\"payment_value_ron\") %>" label="" suffix="RON" 
				inlineField="true" style="width:100px" placeholder="0.00" disabled="true">	
		</aui:input>
	</aui:column>
	<aui:column id="net_plata_currency">
		<aui:input type="text" id="val-netaplata-eur" name="val-netaplata-eur" disabled="true"
			value="<%=oneInvoiceHeader.get(0).get(\"payment_value_curr\") %>" label="" suffix="Valuta" style="width:100px" placeholder="0.00">
			<aui:validator name="required" errorMessage="field-required"></aui:validator>
		</aui:input>
	</aui:column>
</aui:layout>

<div id="overlay-detalii-inv" class="yui3-overlay-loading"
	style="left: -350px; position: absolute; width: 280px">
	<div class="yui3-widget-hd">
		Detalii inventar pentru articolul: [<span id="cod_art_header">&nbsp;</span>]<span
			id="den_art_header">&nbsp;</span>
	</div>
	<div class="yui3-widget-bd">
		<aui:layout>
			<aui:column first="true" columnWidth="100" last="true">
				<div id="detalii-inventar"></div>
			</aui:column>
		</aui:layout>
		<aui:layout>
			<aui:column columnWidth="50" first="true">
				<aui:input value="Inchide" id="finvcancel" name="finvcancel" type="button" label=""
					cssClass="btn btn-primary-red fl"></aui:input>
			</aui:column>
			<aui:column columnWidth="50" last="true">
				<aui:input id="salveaza-inv" name="salveaza-inv" value="Salveaza"
					type="button" label="" cssClass="btn btn-primary fr"></aui:input>
			</aui:column>
		</aui:layout>
	</div>
</div>

<div id="vizualizareDPI" class="yui3-overlay-loading"
	style="right: 100px; z-index: 1; position: absolute; width: 800px; display: none">
	<div class="yui3-widget-hd">Solicitare vizualizare DPI</div>
	<div class="yui3-widget-bd">
		<aui:layout>
			<aui:fieldset>
				<aui:column columnWidth="20">
					<label for="<portlet:namespace />nr_dpi" style="padding-top: 5px">Nr
						DPI: </label>
				</aui:column>
				<aui:column columnWidth="30">
					<aui:input id="nr_dpi" name="nr_dpi" label="" readonly="true"></aui:input>
				</aui:column>
				<aui:column columnWidth="20">
					<label for="<portlet:namespace />societate"
						style="padding-top: 5px">Societate:</label>
				</aui:column>
				<aui:column columnWidth="30">
					<aui:input id="societate" name="societate" label="" readonly="true"></aui:input>
				</aui:column>
			</aui:fieldset>

			<aui:fieldset>
				<aui:column columnWidth="20">
					<label for="<portlet:namespace />data_dpi" style="padding-top: 5px">Data
						DPI: </label>
				</aui:column>
				<aui:column columnWidth="30">
					<aui:input id="data_dpi" name="data_dpi" label="" readonly="true"></aui:input>
				</aui:column>
				<aui:column columnWidth="20">
					<label for="<portlet:namespace />categ_capex_dpi"
						style="padding-top: 5px">Categorie Capex:</label>
				</aui:column>
				<aui:column columnWidth="30">
					<aui:input id="categ_capex_dpi" name="categ_capex_dpi" label=""
						readonly="true"></aui:input>
				</aui:column>
			</aui:fieldset>

			<aui:fieldset>
				<aui:column columnWidth="20">
					<label for="<portlet:namespace />solicitant"
						style="padding-top: 5px">Solicitant: </label>
				</aui:column>
				<aui:column columnWidth="30">
					<aui:input id="solicitant" name="solicitant" label=""
						readonly="true"></aui:input>
				</aui:column>
			</aui:fieldset>

			<aui:fieldset>
				<aui:column columnWidth="20">
					<label for="<portlet:namespace />aprobator"
						style="padding-top: 5px">Aprobator: </label>
				</aui:column>
				<aui:column columnWidth="80">
					<aui:input id="aprobator" name="aprobator" label="" readonly="true"
						style="width: 97.6%"></aui:input>
				</aui:column>
			</aui:fieldset>
			<aui:layout>
				<aui:column columnWidth="100" last="true" first="true">
					<div id="tabelDPI"></div>
				</aui:column>
			</aui:layout>
			<aui:layout>
				<aui:column columnWidth="50" last="true" style="text-align:right">
					<aui:input value="Inchide" name="fcancel" type="button" label=""
						cssClass="btn btn-primary-red fr"
						onclick="$('#vizualizareDPI').hide()"></aui:input>
					<div class="clear"></div>
				</aui:column>
			</aui:layout>
		</aui:layout>
	</div>
</div>

<script type="text/javascript">
var denArt = "asd";
var codArt = "fgh";
var dataTable;
var dataTableSplit;
var dataTableInv;
var dataTableDPI;
$("#<portlet:namespace/>finvcancel").click(function () { closeInventoryOverlay() });
$("#<portlet:namespace/>salveaza-inv").click(function () { closeInventoryOverlay() });

YUI({lang: 'ro'}).use( 'aui-datatable', 'aui-datatype', 'datatable-sort', 'overlay', 'aui-form-validator', 'datatable-mutable',
	function(Y) {
		var remoteData = [
		                  <% for(int i = 0; i < invoiceLinesByInvoiceNr.size(); i++) { 
		                  		sumaPeLinii = sumaPeLinii + Double.parseDouble(invoiceLinesByInvoiceNr.get(i).get("price_no_vat_curr").toString());
								if (invoiceLinesByInvoiceNr.get(i).get("product_code") != null){
									productCode = DefProduct.getProductCodeWithNameById(invoiceLinesByInvoiceNr.get(i).get("product_code").toString());
								} else {
									productCode = "0";
								}
		                  		%>
		                	  {
		                		product_code: 		'<%=productCode %>',
		                		denumire_articol:	'<%=invoiceLinesByInvoiceNr.get(i).get("description").toString()%>',
		                		um:					'<%=invoiceLinesByInvoiceNr.get(i).get("um").toString()%>',
		                		quantity:			'<%=invoiceLinesByInvoiceNr.get(i).get("quantity").toString()%>',
		                		unit_price: 		'<%=invoiceLinesByInvoiceNr.get(i).get("unit_price_curr")%>',
		                		val_no_vat: 		'<%=invoiceLinesByInvoiceNr.get(i).get("price_no_vat_curr")%>',
		                		cota_tva: 			'<%=(invoiceLinesByInvoiceNr.get(i).get("vat") == null ? "24.00" : String.format("%.2f", Float.parseFloat(invoiceLinesByInvoiceNr.get(i).get("vat").toString() )))%>',
		                		val_with_vat: 		<%=Float.parseFloat(invoiceLinesByInvoiceNr.get(i).get("price_no_vat_curr").toString()) + Float.parseFloat(invoiceLinesByInvoiceNr.get(i).get("price_vat_curr").toString())%>,
		                		warranty: 			'<%=(invoiceLinesByInvoiceNr.get(i).get("warranty")  == null ? "0" : invoiceLinesByInvoiceNr.get(i).get("warranty"))%>',
		                		id_ias: 			'<%=DefIAS.getAnalithicGroupById(invoiceLinesByInvoiceNr.get(i).get("id_ias").toString())%>',
		        				id_ifrs: 			'<%=DefIFRS.getAnalithicGroupById(invoiceLinesByInvoiceNr.get(i).get("id_ifrs").toString())%>',
		        				dt_ias: 			'<%=invoiceLinesByInvoiceNr.get(i).get("dt_ras")%>',
		        				dt_ifrs: 			'<%=invoiceLinesByInvoiceNr.get(i).get("dt_ifrs")%>',
		        				lot: 				'<%=DefLot.getLotCodeWithNameById(invoiceLinesByInvoiceNr.get(i).get("lot").toString())%>'
		                	  }
		                	  <%if (i != (invoiceLinesByInvoiceNr.size() - 1)) { out.print(","); }%>
		                  <% } %>
		                  ]; 		
		var nestedCols = [
	      			{key: 'product_code', 		label: 'Cod produs'},
	      			{key: 'denumire_articol', 	label: 'Den. articol'},
	      			{key: 'um', 				label: 'UM'},
	      			{key: 'quantity', 			label: 'Cantitate'},
	      			{key: 'unit_price', 		label: 'Pret unitar'},
	      			{key: 'val_no_vat', 		label: 'Val fara TVA'},
	      			{key: 'cota_tva', 			label: 'Cota TVA'},
	      			{key: 'val_with_vat', 		label: 'Val cu TVA'},
	      			{key: 'warranty', 			label: 'Garantie'},
	      			{key: 'id_ias', 			label: 'Grupa IAS'},
	      			{key: 'id_ifrs', 			label: 'Grupa IFRS'},
	      			{key: 'dt_ias', 			label: 'Dt. IAS'},
	      			{key: 'dt_ifrs', 			label: 'Dt. IFRS'},
	      			{key: 'lot', 				label: 'Lot'}
	      			];
		
			// Invoice lines datatable
			dataTable = new Y.DataTable({
			    columns: nestedCols,
			    data: remoteData,
			    editEvent: 'click',
			}).render('#linii_factura');
			
			var remoteDataSplit = [
			                       <% for(int i = 0; i < invoiceStoreLinesByInvoiceNr.size(); i++) { 
			                       	String tipInventar = "Individidual";
			                       	if(invoiceStoreLinesByInvoiceNr.get(i).get("id_tip_inventar") != null && invoiceStoreLinesByInvoiceNr.get(i).get("id_tip_inventar").toString().equals("2")) {
			                       		tipInventar = "De grup";
			                       	}
			                       	if(invoiceStoreLinesByInvoiceNr.get(i).get("product_code").toString().equals("3562")) {
			                       		tipInventar = " ";
			                       	}
			                       %>
			                       	{
			                       		id_store_line:			'<%=invoiceStoreLinesByInvoiceNr.get(i).get("id")%>',
			                       		denumire_articol:		'<%=invoiceStoreLinesByInvoiceNr.get(i).get("description")%>',
			                       		quantity:				'<%=invoiceStoreLinesByInvoiceNr.get(i).get("quantity")%>',
			                       		val_no_vat: 			'<%=invoiceStoreLinesByInvoiceNr.get(i).get("price_no_vat_curr")%>',
			                       		store: 					'<%=DefStore.getStoreNameById(invoiceStoreLinesByInvoiceNr.get(i).get("id_store").toString())%>',
			                       		raion: 					'<%=invoiceStoreLinesByInvoiceNr.get(i).get("id_department")%>',
			                       		account: 				'<%=invoiceStoreLinesByInvoiceNr.get(i).get("associated_acc")%>',
			                       		reg_type: 				'<%=DefRegEquip.getNameById(invoiceStoreLinesByInvoiceNr.get(i).get("id_tip_inreg").toString())%>',
			                       		id_act_mf: 				'<%=DefMFAct.getNameById(invoiceStoreLinesByInvoiceNr.get(i).get("id_act_mf").toString())%>',
			                       		id_cat_gestiune: 		'<%=DefCatGest.getNameById(invoiceStoreLinesByInvoiceNr.get(i).get("id_cat_gestiune").toString())%>',
			                       		id_dpi:					'<%=invoiceStoreLinesByInvoiceNr.get(i).get("id_dpi")%>',
			                       		link_dpi:				'<%=invoiceStoreLinesByInvoiceNr.get(i).get("id_dpi")%>',
			                       		tip_inventar: 			'<%=tipInventar%>',
			                       		detalii:				('<%=invoiceStoreLinesByInvoiceNr.get(i).get("product_code")%>' != '3562') ? <%=invoiceStoreLinesByInvoiceNr.get(i).get("id")%> : 'a3562' ,
			                       		inventory : 			'<%=invoiceStoreLinesByInvoiceNr.get(i).get("inventory")%>'
			                       	}
			                        <%if (i != (invoiceStoreLinesByInvoiceNr.size() - 1)) { out.print(","); }%>
			                       <% } %>
			                       ]; 		
			var nestedColsSplit = [
						{key: 'id_store_line', 			className: 'hiddencol'},
						{key: 'denumire_articol', 	label: 'Den. articol'},
						{key: 'quantity', 			label: 'Cantitate'},
						{key: 'val_no_vat', 		label: 'Val fara TVA'},
						{key: 'store', 				label: 'Magazin'},
						{key: 'raion', 				label: 'Raion/Dept'},
						{key: 'account', 			label: 'Cont'},
						{key: 'reg_type', 			label: 'Tip inregistrare'},
						{key: 'id_act_mf', 			label: 'Actiune MF'},
						{key: 'id_cat_gestiune', 	label: 'Gestiune'},
						{key: 'id_dpi', 			label: 'Nr. DPI'},
						{ key: 'link_dpi', label: 'Link DPI', 
		   					allowHTML: true, 
		   					formatter: function(o) { 
		   						return '<a href="#" class="dpilink" id="dpi_' + o.data.id_dpi + '">DPI</a>';
		   					},
		   					emptyCellValue: '<a href="#" class="dpilink" id="dpi_{id_dpi}">DPI</a>'},
						{key: 'tip_inventar', 		label: 'Tip nr. inventar'},
						{ key: 'detalii', label: 'Nr. Inv.', allowHTML: true, 
			   				formatter: function(o) {
			   						if(o.value != 'a3562') {
			   							return '<a href="#" id="line-' + o.value + '" class="proddet">detalii</a>';
			   						} else {
			   							return ' ';
			   						}
			   						
			   					}, 
			   				emptyCellValue: '<a href="#" id="line-{value}" class="proddet">detalii</a>',
			   			},
			   			{key: 'inventory', 			className: 'hiddencol'}
		      			];
			
				// Invoice store lines datatable
				dataTableSplit = new Y.DataTable({
				    columns: nestedColsSplit,
				    data: remoteDataSplit,
				    editEvent: 'click',
				}).render('#alocare_magazine');
				
				dataTableSplit.delegate("click", function(e){
					e.preventDefault();
					var loadedInventory = "";
					var idStoreLine = e.target.getAttribute("id").replace('line-', '');
					console.log(idStoreLine);
					makeAjaxToGetInventoryDetails(idStoreLine, e, Y);
				}, ".proddet", dataTableSplit);
				
				function makeAjaxToGetInventoryDetails(idStoreLine, e, Y){
		   			$.ajax({
						type: "POST",
						url: '<%=ajaxURL%>',
						data: 	"<portlet:namespace />action=getInventoryDetails" +
								"&<portlet:namespace />idStoreLine=" + idStoreLine,
						success: function(msgjson) {
							console.log(msgjson);
							if (msgjson != "" && msgjson.length > 2 ) {
							        var jsonEntries = jQuery.parseJSON(msgjson);
									dataTableInv.set('data', eval(jsonEntries));
									showInventoryOverlay(e, Y);
								} else {
									alert("Nu exista numar de inventar pe aceasta linie");
									return;
								}
						},
						error: function(msg) {
							alert('A aparut o problema in preluarea datelor de pe server. Va rugam incercati din nou!');
						}
					});
		   		}
				
				var remoteDataInv = []; 		
				var nestedColsInv = [
				      			{ key: 'nr_inv', label: 'Nr. inv.'},
				      			{ key: 'data_pif', label: 'Data PIF'},
				      			{ key: 'receptie', label: 'Receptie'}
				      			];
		  		
		  		// TABLE INIT
		  		dataTableInv = new Y.DataTable({
		  		    columns: nestedColsInv,
		  		    data: remoteDataInv,
		  		    editEvent: 'click',
		  		}).render('#detalii-inventar');
		  		
		  		var datahDPI = [];
				
				var colsDPI = [
					{ key: 'id_prod', 				label:'Cod produs'},
					{ key: 'description', 			label:'Denumire produs'},
					{ key: 'unit_price_curr', 		label:'Pret Unitar', formatter: formatCurrency, className: 'currencyCol'},
					{ key: 'quantity', 				label:'Cantitate'},
					{ key: 'total_with_vat_curr', 	label:'Valoare totala', formatter: formatCurrency, className: 'currencyCol'},
					{ key: 'magazin', 				label:'Magazin'},
					{ key: 'clasificare_it', 		label:'Clasificare IT'}
				];
				
				dataTableDPI = new Y.DataTable({
				    columns: colsDPI,
				    data:datahDPI
				}).render('#tabelDPI');	
				
				function formatCurrency(cell) {
				    format = {
				    	thousandsSeparator: ",",
				        decimalSeparator: ".",
				        decimalPlaces: noOfDecimalsToDisplay
				    };
				    return Y.DataType.Number.format(Number(cell.value), format);
				}
				
				dataTableSplit.delegate('click', function (e) {
		   			e.preventDefault();
		   		    var target = e.target.get('id');
		   		    var dpiId = target.replace('dpi_','');
		   		 	showDPIPopupInvoice(e.target, dpiId,'<portlet:namespace/>', '<%=ajaxURL%>', Y);
				}, '.dpilink', dataTableSplit);
				
				$(document).ready(function(e){
					$('#<portlet:namespace/>val_fara_tva_linii').val(<%=sumaPeLinii%>);
				});
				
				$('#inapoi').click(function(e){
					<% if (sourcePage.equals("exported_docs") ){ %>
						$('#<portlet:namespace />back').attr("action", "<%=backToExportedDocsURL.toString() %>");
					<% } else { %>
						$('#<portlet:namespace />back').attr("action", "<%=backToMissingDocsURL.toString() %>");
					<% } %>
					$('#<portlet:namespace/>back').submit();
				});
	});
</script>

<aui:layout>
	<aui:form style="display:none" method="post" action="<%=backToMissingDocsURL.toString()%>" name="back" id="back">
		<aui:input name="invoiceNo" id="invoiceNo" value="<%=invoiceNo%>"></aui:input>
		<aui:input name="regDate" id="regDate" value="<%=regDate%>"></aui:input>
		<aui:input name="supplierName" id="supplierName" value="<%=supplierName%>"></aui:input>
		<aui:input name="invoiceDate" id="invoiceDate" value="<%=invoiceDate%>"></aui:input>
		<aui:input name="currency" id="currency" value="<%=currency%>"></aui:input>
		<aui:input name="companyId" id="companyId" value="<%=companyId%>"></aui:input>
		<aui:input name="docType" id="docType" value="<%=docType%>"></aui:input>
	</aui:form>
</aui:layout>
