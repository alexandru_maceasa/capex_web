<%@page import="com.profluo.ecm.model.db.DefLot"%>
<%@page import="com.profluo.ecm.model.db.DefIFRS"%>
<%@page import="com.profluo.ecm.model.db.DefIAS"%>
<%@page import="com.profluo.ecm.model.db.DefProduct"%>
<%@page import="com.profluo.ecm.model.vo.DefDpis"%>
<%@page import="com.profluo.ecm.model.vo.DefLotVo"%>
<%@page import="com.profluo.ecm.model.vo.DefProductVo"%>
<%@page import="com.profluo.ecm.model.vo.DefIFRSVo"%>
<%@page import="com.profluo.ecm.model.vo.DefIASVo"%>
<%@page import="com.profluo.ecm.model.vo.DefUMVo"%>
<%@page import="com.profluo.ecm.model.vo.DefStoreVo"%>
<%@page import="com.profluo.ecm.model.vo.DefRegEquipVo"%>
<%@page import="com.profluo.ecm.model.vo.DefVATVo"%>
<%@page import="com.profluo.ecm.model.vo.DefMFActVo"%>
<%@page import="com.profluo.ecm.model.db.DefNewProj"%>
<%@page import="com.profluo.ecm.model.db.ListaInit"%>
<%@page import="com.profluo.ecm.model.vo.DefAisles"%>
<%@page import="com.profluo.ecm.model.db.DefStore"%>
<%@page import="com.profluo.ecm.model.db.DefCompanies"%>
<%@page import="com.profluo.ecm.model.db.GenericDBStatements"%>
<%@ page import="com.profluo.ecm.model.db.VoucherHeader"%>
<%@ page import="com.profluo.ecm.model.vo.ListaInitVo" %>
<%@ page import="com.profluo.ecm.model.vo.DefNewProjVo" %>
<%@ page import="java.util.ResourceBundle"%>
<%@ page import="java.util.Locale"%>
<%@ page import="java.util.HashMap"%>
<%@ page import="java.util.List"%>
<%@page import="com.liferay.portal.util.PortalUtil"%>

<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme"%>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui"%>
<%@ taglib uri="http://alloy.liferay.com/tld/aui" prefix="aui"%>
<%@ taglib uri="http://liferay.com/tld/util" prefix="liferay-util"%>

<liferay-theme:defineObjects />
<portlet:defineObjects />

<%-- Define URLs --%>
<portlet:renderURL var="BackURL" >
	<portlet:param name="mvcPath" value="/html/exporteddocs/view.jsp"></portlet:param>
</portlet:renderURL>

<portlet:resourceURL id="ajaxURL" var="ajaxURL"/>
<%-- Retrieve Usefull info --%>
<%
	locale = renderRequest.getLocale();
	ResourceBundle resmainIV = portletConfig.getResourceBundle(locale);
	// retrieve the id of the displayed voucher
	String id = "";
	String vat = "";
	String status = "";
	String storeId = "";
	String aisle = "";
	Double sumaPeLinii 	= 0.00;
	try {
		id						= renderRequest.getParameter("id").toString();
	} catch (Exception e) {
		System.out.println("AICI EEEEEE");
	}
	
	List<HashMap<String,Object>> voucherHeader = null;
	List<HashMap<String, Object>> voucherLines = null;
	List<HashMap<String, Object>> voucherStoreLines = null;
	if (!id.equals("")) {
		voucherHeader = VoucherHeader.getValuesById(Integer.parseInt(id), "voucher_header", "id");
		voucherLines = VoucherHeader.getLinesAndInvNo(Integer.parseInt(id), "voucher_line", "id_voucher");
		storeId = voucherHeader.get(0).get("id_store").toString();
		aisle = DefAisles.getAisleByStoreAndShortId(storeId, voucherHeader.get(0).get("id_aisle").toString());
		Float valCuTVA = Float.parseFloat(voucherHeader.get(0).get("val_no_vat_ron").toString())
						+ Float.parseFloat(voucherHeader.get(0).get("vat_val_ron").toString());
		vat = String.format("%.2f", (float)(Math.round(100 * (valCuTVA / Float.parseFloat(voucherHeader.get(0).get("val_no_vat_ron").toString()))) - 100));
	}
	
	String companyName = DefCompanies.getCompanyNameById(voucherHeader.get(0).get("id_company").toString());
	String storeName = DefStore.getStoreNameById(voucherHeader.get(0).get("id_store").toString());
%>

<legend>Antet bon de cesiune</legend>
<aui:layout>
	<aui:fieldset>
		<aui:column columnWidth="10">
			<label for="<portlet:namespace />nr_bon" style="padding-top: 5px"><%=resmainIV.getString("nr_bon")%>:
			</label>
		</aui:column>
		<aui:column columnWidth="20">
			<aui:input id="nr_bon" name="nr_bon" label="" value="<%=voucherHeader.get(0).get(\"voucher_no\") %>" readonly="true"></aui:input>
		</aui:column>
		<aui:column columnWidth="10">
			<label for="<portlet:namespace />data_bon" style="padding-top: 5px"><%=resmainIV.getString("data_bon")%>:
			</label>
		</aui:column>
		<aui:column columnWidth="20">
			<aui:input id="data_bon" name="data_bon" label="" value="<%=voucherHeader.get(0).get(\"voucher_date\") %>" readonly="true" placeholder="aaaa-ll-zz"></aui:input>
		</aui:column>
		<aui:column columnWidth="10">
			<label for="<portlet:namespace />cota_tva" style="padding-top: 5px"><%=resmainIV.getString("cota_tva")%>:</label>
		</aui:column>
		<aui:column columnWidth="20">
			<aui:input id="cota_tva" name="cota_tva" label="" value="<%=vat %>" readonly="true"></aui:input>
		</aui:column>
	</aui:fieldset>
	<%-- ROW 2 --%>
	<aui:fieldset>
		<aui:column columnWidth="10">
			<label for="<portlet:namespace />societate_v" style="padding-top: 5px"><%=resmainIV.getString("societate_v")%>:
			</label>
		</aui:column>
		<aui:column columnWidth="20">
			<aui:input id="societate_v" name="societate_v" value="<%=companyName%>" label="" readonly="true" ></aui:input>
		</aui:column>
		<aui:column columnWidth="10">
			<label for="<portlet:namespace />magazin" style="padding-top: 5px"><%=resmainIV.getString("magazin")%>:
			</label>
		</aui:column>
		<aui:column columnWidth="20">
			<aui:input id="magazin" name="magazin" value="<%=storeName%>" label=""  readonly="true"></aui:input>
		</aui:column>
		<aui:column columnWidth="10">
			<label for="<portlet:namespace />raion" style="padding-top: 5px"><%=resmainIV.getString("raion")%>:
			</label>
		</aui:column>
		<aui:column columnWidth="20">
			<aui:input id="raion" name="raion" label="" value="<%= aisle%>" readonly="true"></aui:input>
		</aui:column>
	</aui:fieldset>
	<%-- ROW 3 --%>
	<aui:fieldset>
	<aui:column columnWidth="10">
			<label for="<portlet:namespace />valoare_ron" style="padding-top: 5px"><%=resmainIV.getString("valoare_ron")%>:</label>
		</aui:column>
		<aui:column columnWidth="20">
			<aui:input cssClass="currencyCol" id="valoare_ron" name="valoare_ron" label="" readonly="true" value="<%=voucherHeader.get(0).get(\"val_no_vat_ron\") %>"></aui:input>
		</aui:column>
		<aui:column columnWidth="10">
			<label for="<portlet:namespace />tva_ron" style="padding-top: 5px"><%=resmainIV.getString("tva_ron")%>:</label>
		</aui:column>
		<aui:column columnWidth="20">
			<aui:input cssClass="currencyCol" id="tva_ron" name="tva_ron" label="" readonly="true" value="<%=voucherHeader.get(0).get(\"vat_val_ron\") %>"></aui:input>
		</aui:column>
			<aui:column columnWidth="10">
			<label for="<portlet:namespace />val_cu_tva_ron" style="padding-top: 5px"><%=resmainIV.getString("val_cu_tva_ron")%>:
			</label>
		</aui:column>
		<aui:column columnWidth="20">
			<aui:input cssClass="currencyCol" id="val_cu_tva_ron" name="val_cu_tva_ron" label="" readonly="true" value="<%=(Float.parseFloat(voucherHeader.get(0).get(\"val_no_vat_ron\").toString()) 
						+ Float.parseFloat(voucherHeader.get(0).get(\"vat_val_ron\").toString())) %>"></aui:input>
			</aui:column>
	</aui:fieldset>
	<%-- ROW 4 --%>
	<aui:fieldset>
		<aui:column columnWidth="10">
			<label for="<portlet:namespace />cont_kontan" style="padding-top: 5px">Cont Kontan:</label>
		</aui:column>
		<aui:column columnWidth="20">
			<aui:input id="cont_kontan" name="cont_kontan" label="" readonly="true" value="<%=voucherHeader.get(0).get(\"kontan_acc\") %>"></aui:input>
		</aui:column>
	</aui:fieldset>
</aui:layout>

<%-- table ROW  --%>
<legend><strong>Articole</strong></legend>
<aui:layout>
	<aui:column first="true" columnWidth="100" last="true">
		<div id="alocare-articole"></div>
	</aui:column>
</aui:layout>
<legend><strong>Alocare articol pe linii</strong></legend>
<aui:layout>
	<aui:column first="true" columnWidth="100" last="true">
		<div id="alocare-linii"></div>
	</aui:column>
</aui:layout>
<aui:layout>
	<aui:column cssClass="currencyCol">
		<label for="<portlet:namespace />val_no_vat_aloc_linii"
			style="padding-top: 5px"><liferay-ui:message key="val_no_vat_aloc_linii"/></label>
	</aui:column>
	<aui:column>
		<aui:input cssClass="currencyCol" name="valoare_totala" id="valoare_totala"
			value="<%=sumaPeLinii %>" label="" readonly="readonly" />
	</aui:column>
</aui:layout>

<div id="overlay-detalii-inv" class="yui3-overlay-loading" style="left:-350px;position:absolute;width: 280px;z-index: 1">
    <div class="yui3-widget-hd" style="font-size: 12px">
    	Detalii inventar pentru articolul: <span id="cod_art_header" style="display:none">&nbsp;</span> <span id="den_art_header">&nbsp;</span>
    </div>
    <div class="yui3-widget-bd">
		<aui:layout>
			<aui:column first="true" columnWidth="100" last="true" style="position:relative">
				<div id="detalii-inventar"></div>
			</aui:column>
		</aui:layout>
		<aui:layout>
			<aui:column columnWidth="50" first="true">
				<aui:input value="Inchide" name="fcancel" id="finvcancel" type="button" label="" cssClass="btn btn-primary-red fl"/>
			</aui:column>
			<aui:column columnWidth="50" last="true">
				<aui:input id="salveaza-inv" name="salveaza-inv" value="Salveaza" type="button" label="" cssClass="btn btn-primary fr"/>
			</aui:column>
		</aui:layout>
    </div>
</div>
<%-- overlay solicita vizualizare DPI --%>
<div id="vizualizareDPI" class="yui3-overlay-loading"
	style="right: 100px; z-index: 1; position: absolute; width: 800px; display: none">
	<div class="yui3-widget-hd">Solicitare vizualizare DPI</div>
	<div class="yui3-widget-bd">
		<aui:layout>
			<aui:fieldset>
				<aui:column columnWidth="20">
					<label for="<portlet:namespace />nr_dpi" style="padding-top: 5px">Nr
						DPI: </label>
				</aui:column>
				<aui:column columnWidth="30">
					<aui:input id="nr_dpi" name="nr_dpi" label="" readonly="true"></aui:input>
				</aui:column>
				<aui:column columnWidth="20">
					<label for="<portlet:namespace />societate"
						style="padding-top: 5px">Societate:</label>
				</aui:column>
				<aui:column columnWidth="30">
					<aui:input id="societate" name="societate" label="" readonly="true"></aui:input>
				</aui:column>
			</aui:fieldset>

			<aui:fieldset>
				<aui:column columnWidth="20">
					<label for="<portlet:namespace />data_dpi" style="padding-top: 5px">Data
						DPI: </label>
				</aui:column>
				<aui:column columnWidth="30">
					<aui:input id="data_dpi" name="data_dpi" label="" readonly="true"></aui:input>
				</aui:column>
				<aui:column columnWidth="20">
					<label for="<portlet:namespace />categ_capex_dpi"
						style="padding-top: 5px">Categorie Capex:</label>
				</aui:column>
				<aui:column columnWidth="30">
					<aui:input id="categ_capex_dpi" name="categ_capex_dpi" label=""
						readonly="true"></aui:input>
				</aui:column>
			</aui:fieldset>

			<aui:fieldset>
				<aui:column columnWidth="20">
					<label for="<portlet:namespace />solicitant"
						style="padding-top: 5px">Solicitant: </label>
				</aui:column>
				<aui:column columnWidth="30">
					<aui:input id="solicitant" name="solicitant" label=""
						readonly="true"></aui:input>
				</aui:column>
			</aui:fieldset>

			<aui:fieldset>
				<aui:column columnWidth="20">
					<label for="<portlet:namespace />aprobator"
						style="padding-top: 5px">Aprobator: </label>
				</aui:column>
				<aui:column columnWidth="80">
					<aui:input id="aprobator" name="aprobator" label="" readonly="true"
						style="width: 97.6%"></aui:input>
				</aui:column>
			</aui:fieldset>
			<aui:layout>
				<aui:column columnWidth="100" last="true" first="true">
					<div id="tabelDPI"></div>
				</aui:column>
			</aui:layout>
			<aui:layout>
				<aui:column columnWidth="50" last="true" style="text-align:right">
					<aui:input value="Inchide" name="fcancel" type="button" label=""
						cssClass="btn btn-primary-red fr"
						onclick="$('#vizualizareDPI').hide()"></aui:input>
					<div class="clear"></div>
				</aui:column>
			</aui:layout>
		</aui:layout>
	</div>
</div>


<script type="text/javascript">
		$(document).ready(function(){
			$('#<portlet:namespace />tva_ron').autoNumeric('init', {aSep: ',', mDec: 4, vMin : -999999999, vMax : 999999999 });
			$('#<portlet:namespace />val_cu_tva_ron').autoNumeric('init', {aSep: ',', mDec: 4, vMin : -999999999, vMax : 999999999 });
			$('#<portlet:namespace />valoare_ron').autoNumeric('init', {aSep: ',', mDec: 4, vMin : -999999999, vMax : 999999999 });
			$('#<portlet:namespace />valoare_totala').autoNumeric('init', {aSep: ',', mDec: 4, vMin : -999999999, vMax : 999999999 });
			$('#<portlet:namespace />difference').autoNumeric('init', {aSep: ',', mDec: 4, vMin : -999999999, vMax : 999999999 });
		});
</script>

<%-- detalii capex -> --%>
<aui:fieldset>
		<legend><strong>Detalii CAPEX</strong></legend>
		<aui:column columnWidth="25">
			<% if (voucherHeader.get(0).get("tip_capex").toString().equals("0")) { %>
				<aui:input type="radio" name="categ_capex" id="capex_diverse" label="Capex Diverse" value="0" disabled="true" checked="true" />
			<% } else { %>
				<aui:input type="radio" name="categ_capex" id="capex_diverse" label="Capex Diverse" value="0" disabled="true" />
			<% } %>
		</aui:column>
		
		<% if (voucherHeader.get(0).get("tip_capex").toString().equals("1")) { %>
		<aui:column columnWidth="10">
			<aui:input type="radio" name="initiative" id="initiative" label="Lista initiative" value="0" disabled="true" checked="true" />
		</aui:column>
		<aui:column columnWidth="15">
			<aui:input type="text" name="init" id="init" disabled="true" label="" value="<%=ListaInit.getInitiativeNameById(voucherHeader.get(0).get(\"id_initiative\").toString()) %>"/>
		</aui:column>
		<% } else { %>
		<aui:column columnWidth="25">
			<aui:input type="radio" name="initiative" id="initiative" label="Lista initiative" value="0" disabled="true" />
		</aui:column>
		<% } %>
		
		<% if (voucherHeader.get(0).get("tip_capex").toString().equals("2")) { %>
		<aui:column columnWidth="10">
			<aui:input type="radio" name="new_prj" id="new_prj" label="Proiect nou" value="0" disabled="true" checked="true" />
		</aui:column>
		<aui:column columnWidth="15">
			<aui:input type="text" name="new_prj" id="new_prj" disabled="true" label="" value="<%=DefNewProj.getNewPrjNameById(voucherHeader.get(0).get(\"id_new_prj\").toString()) %>"/>
		</aui:column>
		<% } else { %>
		<aui:column columnWidth="25">
			<aui:input type="radio" name="new_prj" id="new_prj" label="Proiect nou" value="0" disabled="true" />
		</aui:column>
		<% } %>
	</aui:fieldset>

<aui:layout>
	<aui:column  first="true">
		<aui:button name="inapoi" cssClass="btn btn-primary-red" onClick ="<%=BackURL.toString()%>" value="<%=resmainIV.getString(\"back\")%>"/>
	</aui:column>
</aui:layout>

<script type="text/javascript">
// global variable for tables in order to be able use them in other scripts
var dataTable, dataTableSplit;
// global variables used to save inventory numbers on input type hidden
var idLine, denArt, codArt, tipInv;


YUI({lang: 'ro'}).use( 'aui-datatable', 'aui-datatype', 'datatable-sort', 'overlay', 'datatable-scroll', 'aui-base', 'liferay-portlet-url', 'aui-node','datatable-mutable',
	function(Y) {
		/***********************************/
		/**	   GENERIC TABLE METHODS	  **/
		/***********************************/
		/** GENERIC CURRENCY FORMATTER **/
		function formatCurrency(cell) {
		    format = {
		    	thousandsSeparator: ",",
		        decimalSeparator: ".",
		        decimalPlaces: noOfDecimalsToDisplay
		    };
		    return Y.DataType.Number.format(Number(cell.value), format);
		}
		
		// editor attributes, actions and formats based on datatypes
		var attrNumberEditor = {inputFormatter: Y.DataType.Number.evaluate, validator: { rules: { value: { number: true } } }, on : {keyup: saveOnEnter}, showToolbar: false };
		var attrStringEditor = {on :{keyup: saveOnEnter}, showToolbar: false};

//=============================================================================================================================================================
//													POP-UP INFO DPI LINES
//=============================================================================================================================================================			

		var tipinregistrare = <%=DefRegEquipVo.getJsonStringRegEquip()%>;
		var actiunimf=<%=DefMFActVo.getJsonStringMfAct()%>;
		var tipinventar =  {1: "individual", 2: "de grup"};
		
		var datahDPI = [];
		
		var colsDPI = [
				{ key: 'id_prod', 				label:'Cod produs'},
				{ key: 'description', 			label:'Denumire produs'},
				{ key: 'unit_price_curr', 		label:'Pret Unitar'},
				{ key: 'quantity', 				label:'Cantitate'},
				{ key: 'total_with_vat_curr', 	label:'Valoare totala'},
				{ key: 'magazin', 				label:'Magazin'},
				{ key: 'clasificare_it', 		label:'Clasificare IT'}
		];
		
		dataTableDPI = new Y.DataTable({
			columns: colsDPI,
			data:datahDPI
		}).render('#tabelDPI');	
		
		// data array - main table
		var remoteData = [
		                  <% for(int i = 0; i < voucherLines.size(); i++) { %>
		                	  {
		          				id_voucher: 	'<%=voucherLines.get(i).get("id").toString()%>',
		          				line: 			'<%=voucherLines.get(i).get("line").toString()%>',
		          				product_code: 	'<%=DefProduct.getProductCodeWithNameById(voucherLines.get(i).get("product_code").toString())%>',
		          				description: 	'<%=voucherLines.get(i).get("description").toString()%>',
		          				um: 			'<%=voucherLines.get(i).get("um").toString()%>',
		          				quantity: 		'<%=voucherLines.get(i).get("quantity").toString()%>',
		          				unit_price_ron :'<%=voucherLines.get(i).get("unit_price_ron").toString()%>',
		          				price_no_vat_ron : '<%=voucherLines.get(i).get("price_no_vat_ron").toString()%>',
		          				vat: '<%=(voucherLines.get(i).get("vat") == null ? "24.00" : String.format("%.2f", Float.parseFloat(voucherLines.get(i).get("vat").toString())))%>',
		          				price_with_vat_ron : '<%=voucherLines.get(i).get("price_with_vat_ron").toString()%>',
		          				ras: '<%=DefIAS.getAnalithicGroupById(voucherLines.get(i).get("ras").toString())%>',
		          				ifrs: '<%=DefIFRS.getAnalithicGroupById(voucherLines.get(i).get("ifrs").toString())%>',
		          				dt_ras: '<%=DefIAS.getOneColumn(voucherLines.get(i).get("ras").toString(), "def_grp_ias" , "durata_implicita")%>',
		          				dt_ifrs: '<%=DefIFRS.getOneColumn(voucherLines.get(i).get("ras").toString(), "def_grp_ifrs", "durata_implicita")%>',
		          				lot: '<%=DefLot.getLotCodeWithNameById(DefProduct.getOneColumn(voucherLines.get(i).get("product_code").toString(), "def_products", "lot_id"))%>',
		          				is_it:	'<%=DefProduct.getOneColumn(voucherLines.get(i).get("product_code").toString(), "def_products", "flag_it")%>',
		                	  }
		                	  <%if (i != (voucherLines.size() - 1)) { out.print(","); }%>
		                  <% } %>
		                  ]; 		
		
		// COLUMN INFO and ATTRIBUTES
		var nestedCols = [
			{ key: 'line', 			label: 'Nr.' },
			{ key: 'product_code', 		label: 'Cod Prod'},
			{ key: 'description',		label: 'Den. articol.'},
			{ key: 'um', 				label: 'UM'},
			{ key: 'quantity', 			label: 'Cant'},
			{ key: 'unit_price_ron', 	label: 'Pret unitar', formatter: formatCurrency},
			{ key: 'price_no_vat_ron', 	label: 'Val fara TVA', 	formatter: formatCurrency, className: 'currencyCol' /*editor: new Y.TextCellEditor(attrNumberEditor)*/},
			{ key: 'vat', 			label: 'Cota TVA'},
			{ key: 'price_with_vat_ron', 	label: 'Val cu TVA', formatter: formatCurrency, className: 'currencyCol' 	/*editor: new Y.TextCellEditor(attrNumberEditor), formatter: formatCurrency*/},			
			{ key: 'ras', 	label: 'Grupa IAS'},
			{ key: 'ifrs', 	label: 'Grupa IFRS'},
			{ key: 'dt_ras', 		label: 'Dt. RAS'},
			{ key: 'dt_ifrs', 		label: 'Dt. IFRS'},
			{ key: 'lot', 			label: 'Lot'},
			{ key: 'is_it', 		label: 'Clasif.',
				formatter: function(o) {
					if (o.value == '0'){
						return 'Active.';
					} else if (o.value == '1'){
						return 'IT Hardware';
					} else if (o.value == '2'){
						return 'IT Software';
					}
				}
			}
		];

		// TABLE INIT
		dataTable = new Y.DataTable({
		    columns: nestedCols,
		    data: remoteData,
		    editEvent: 'click',
		    recordType: ['id_voucher', 'line', 'product_code', 'description', 
		                 'um', 'quantity', 'unit_price_ron', 'price_no_vat_ron', 'price_vat_ron', 'vat', 
		                 'price_with_vat_ron', 'lot', 'ras', 'ifrs', 'dt_ras', 'dt_ifrs', 
		                 'is_it', 'detalii', 'cont_imob_in_fct', 
		                 'cont_imob_in_curs', 'cont_imob_avans']
		}).render('#alocare-articole');
		
		// bottom table values
		var remoteDataSplit = [
				                  <% for(int i = 0; i < voucherLines.size(); i++) { 
				                  		sumaPeLinii = sumaPeLinii + Double.parseDouble(voucherLines.get(i).get("price_no_vat_ron").toString());%>
				                	  {
				          				description: 	'<%=voucherLines.get(i).get("description").toString()%>',
				          				line : 			'<%=voucherLines.get(i).get("line").toString()%>',
				          				quantity: 		'<%=voucherLines.get(i).get("quantity").toString()%>',
				          				price_no_vat_ron : '<%=voucherLines.get(i).get("price_no_vat_ron").toString()%>',
				          				associated_acc : '<%=voucherLines.get(i).get("associated_acc").toString()%>',
				          				id_tip_inreg : 	'<%=voucherLines.get(i).get("id_tip_inreg").toString()%>',
				          				id_act_mf : 	'<%=voucherLines.get(i).get("id_act_mf").toString()%>',
				          				id_dpi : '<%=voucherLines.get(i).get("id_dpi").toString()%>',
				          				id_tip_inventar : '<%=voucherLines.get(i).get("id_tip_inventar").toString()%>',
				          				detalii : '<%=voucherLines.get(i).get("line").toString()%>',
				          				inventory : '<%=voucherLines.get(i).get("inventory").toString()%>'
				                	  }
				                	  <%if (i != (voucherLines.size() - 1)) { out.print(","); }%>
				                  <% } %>
				                  ]; 
		
		
		// bottom table header
		var nestedColsSplit = [
		   	{ key: 'description', label: 'Den. articol.'},
		    { key: 'line', className: 'hiddencol'},
			{ key: 'quantity', label:  'Cant'},
			{ key: 'price_no_vat_ron', label: 'Val fara TVA', formatter: formatCurrency},
			{ key: 'associated_acc', label: 'Cont'}, 
			{ key: 'id_tip_inreg', label: 'Tip inregistrare', 
				emptyCellValue: '1',
				formatter: function(o) {
					var obj = JSON.parse(JSON.stringify(tipinregistrare));
					return obj[o.value].toString();
				}
			},
			{ key: 'id_act_mf', label: 'Actiune MF', 
				emptyCellValue: '1',
				formatter: function(o) {
					var obj = JSON.parse(JSON.stringify(actiunimf));
					return obj[o.value].toString();
				}
			},
			{ key: 'id_dpi', label: 'Nr. DPI'},
   			{ key: 'vizualizare_dpi', label: 'Link DPI', 
   					allowHTML: true, 
   					formatter: function(o) { 
   						return '<a href="#" class="dpilink" id="dpi_' + o.data.line + '_' + o.value + '">DPI</a>';
   					},
   					emptyCellValue: '<a href="#" class="dpilink" id="dpi_{line}_{value}">DPI</a>'},
			{ key: 'id_tip_inventar', label: 'Tip nr. inventar',
				emptyCellValue: '2',  // de grup - default
				formatter: function(o) {
					var obj = JSON.parse(JSON.stringify(tipinventar));
					return obj[o.value].toString();
				}
			},
			{ key: 'detalii', label: 'Nr. inventar', allowHTML: true, 
				// gets the id of the row and creates an unique link for every row
				// formatter: '<a href="#" id="line-{nr}" class="proddet">detalii</a>', 
				formatter: function(o) {
					return '<a href="#" id="line-' + o.value + '" class="proddet">detalii</a>';
				},
			},
			{ key: 'inventory', 		className: 'hiddencol'}
		];
		
		// TABLE INIT - bottom
		dataTableSplit = new Y.DataTable({
		    columns: nestedColsSplit,
		    data: remoteDataSplit,
		    editEvent: 'click',
		    recordType: ['delete_line', 'line', 'description', 'quantity', 'price_no_vat_ron', 'associated_acc', 'id_tip_inreg', 'id_act_mf', 
		                 'id_dpi', 'vizualizare_dpi', 'id_tip_inventar', 'detalii', 'cont_imob_in_fct', 'cont_imob_in_curs', 'cont_imob_avans']
		}).render('#alocare-linii');
		
		dataTableSplit.delegate("click", function(e){
			e.preventDefault();
			var loadedInventory = "";
			idLine = e.target.getAttribute("id").replace('line-', '');
			var idStoreLine = e.target.getAttribute("id").replace('line-', '');
			var ml = dataTableSplit.data, msg = '', template = '';
			    ml.each(function (item, i) {
			    	var dataS = item.getAttrs(['line', 'inventory']);
			    	if(dataS.line == idLine){
			    		loadedInventory = dataS.inventory;
				    	denArt = dataS.description;
				    	codArt = dataS.line;
				    }
			    });
		    var invComponents = loadedInventory.split(',');
	    	loadedInventory = invComponents[0] + ',' + invComponents[1] + ',' + invComponents[2] + ']';
	    	loadedInventory = loadedInventory.replace("]]","]");
	    	loadedInventory = loadedInventory.replace("inventory_no","nr_inv");
	    	loadedInventory = loadedInventory.replace("pif_date","data_pif");
	    	loadedInventory = loadedInventory.replace("reception","receptie");
	        var jsonEntries = jQuery.parseJSON(loadedInventory);
			dataTableInv.set('data', eval(jsonEntries));
			showInventoryOverlay(e, Y);
		}, ".proddet", dataTableSplit);
		
		// click on dpi link
   		dataTableSplit.delegate('click', function (e) {
   			e.preventDefault();
   		    var target = e.target.get('id');
   		    var ids = target.replace('dpi_','').split("_");
   		    var lineid = ids[0];
   		    var dpiId = ids[1];
   		 showDPIPopupVoucher(e.target, dpiId,'<portlet:namespace/>', '<%=ajaxURL%>', Y);
		}, '.dpilink', dataTableSplit);
		
		/***********************************/
		/**	   inventory table - START	  **/
		/***********************************/
		var remoteDataInv = []; 
		         		
		var nestedColsInv = [
		      			{ key: 'nr_inv', 	label: 'Nr. inv.'},
		      			{ key: 'data_pif', 	label: 'Data PIF'},
		      			{ key: 'receptie', 	label: 'Receptie'}
		      			];
  		
  		// TABLE INIT
  		var dataTableInv = new Y.DataTable({
  		    columns: nestedColsInv,
  		    data: remoteDataInv,
  		    editEvent: 'click'
  		}).render('#detalii-inventar');
  		
  		//dataTableInv.get('boundingBox').unselectable();
  		
  		function showInventoryOverlay(e) {
			// end of prepare table
	        $('#den_art_header').text(denArt);
	        $('#cod_art_header').text(codArt);
			
	        // prepare overlay
	        var currentNode = this;
			var overlay = new Y.Overlay({
			    srcNode:"#overlay-detalii-inv",
			    width:"500px",
			    align: {
			        node: e.target,
			        points:["bl", "tl"]
			    }
			});
			
			overlay.render();

	        $('#overlay-detalii-inv').show();
	        
			// bind actions on pop-up
	  		$("#<portlet:namespace/>finvcancel").click(function () { closeInventoryOverlay(); });
	  		$("#<portlet:namespace/>salveaza-inv").click(function () { closeInventoryOverlay(); });
  		}
  		
  		function closeInventoryOverlay() {
  			$('#overlay-detalii-inv').hide();
  			return false;
  		}
	}
);
</script>