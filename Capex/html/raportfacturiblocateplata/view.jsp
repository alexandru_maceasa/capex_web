<%@page import="com.profluo.ecm.model.vo.DefSupplierVo"%>
<%@page import="com.profluo.ecm.model.db.DefSupplier"%>
<%@page import="com.profluo.ecm.model.db.FacturiBlocatePlata"%>
<%@page import="com.profluo.ecm.flows.InvoiceStages"%>
<%@page import="com.profluo.ecm.controller.UserTeamIdUtils"%>
<%@page import="com.profluo.ecm.model.db.DefCompanies"%>
<%@page import="com.profluo.ecm.model.db.DefSupplier"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.List"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme"%>

<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui"%>
<%@ taglib uri="http://alloy.liferay.com/tld/aui" prefix="aui"%>
<%@ taglib uri="http://liferay.com/tld/util" prefix="liferay-util"%>

<portlet:resourceURL id="ajaxURL" var="ajaxURL"/>
 
<liferay-theme:defineObjects />
<portlet:defineObjects />

<portlet:resourceURL id="exportRaport" var="exportRaportURL">
	<portlet:param name="action" value="exportRaport"/>
</portlet:resourceURL>

<%
	List<HashMap<String,Object>> allCompanies = DefCompanies.getAll();
%>

<aui:layout>
	<aui:form id="exportFormRaport" name="exportFormRaport" action="<%=exportRaportURL.toString()%>">
	<aui:layout>
		<aui:column columnWidth="15">
			<label for="<portlet:namespace />societate" style="padding-top: 5px">Societatea</label>
		</aui:column>
		<aui:column>
			<aui:select id="societate" name="societate" label="">
				<aui:option selected="selected" value="0"> ----------- </aui:option>
				<% for (int i = 0 ; i < allCompanies.size(); i++) { %>
					<aui:option value="<%=i+1%>"> <%=allCompanies.get(i).get("name").toString()%> </aui:option>
				<% } %>
			</aui:select>
		</aui:column>
	</aui:layout>
	<aui:layout>
		<aui:column  columnWidth="15">
			<label for="<portlet:namespace />motiv" style="padding-top: 5px;">Motiv blocare la plata</label>
		</aui:column>
		<aui:column>
			<aui:select id="motiv" name="motiv" label="">
				<aui:option selected="selected" value="0"> ----------- </aui:option>
					<aui:option value="0">-----------</aui:option>
					<aui:option value="1">Amanare la plata</aui:option>
					<aui:option value="2">Litigii/Bunuri nelivrate sau cu probleme</aui:option>
			</aui:select>
		</aui:column>
	</aui:layout>
<script type="text/javascript">
YUI().use('autocomplete-list', "datasource-io", 'aui-base', 'aui-io-request', 
		  'aui-form-validator', 'autocomplete-filters', 'autocomplete-highlighters', 
		  'overlay',
function (A) {
	var contactSearchDS = new A.DataSource.IO({source: '<%=ajaxURL.toString()%>'});
	var supplierData = "";
	var supplierCreated = false;
	
	var contactSearchFormatter = function (query, results) {
		return A.Array.map(results, function (result) {
			return '<strong>'+result.raw.name+'</strong><br/><b>' + result.raw.supplier_code + '</b> - CUI: ' + result.raw.cui;
		});
	};

	var testData;
	var contactSearchInput = new A.AutoCompleteList({
		allowBrowserAutocomplete: 'false',
		resultHighlighter: 'phraseMatch',
		activateFirstItem: 'true',
		resultTextLocator: 'name',
		inputNode: '#<portlet:namespace/>supplier_list',
		render: 'true',
		width: '300px',
        resultFormatter: contactSearchFormatter,
		source: function(){
			
			$('#<portlet:namespace/>id_supplier_raport').val("");
			supplierData = "";
			var inputValue = A.one("#<portlet:namespace />supplier_list").get('value');
			var myAjaxRequest = A.io.request('<%=ajaxURL.toString()%>',{
					dataType: 'json',
					method:'POST',
					data:{
						 <portlet:namespace/>action: 'findSupplier',
						 <portlet:namespace/>keywords: $('#<portlet:namespace />supplier_list').val(),
						 <portlet:namespace/>id_company: $('#<portlet:namespace />societate').val()
						},
					autoLoad: false,
					sync: true,
					on: {
					success:function(){
						var data = this.get('responseData');
						testData = data;
					}}
			});
			myAjaxRequest.start();
			return testData;
		},	
			on: {
			select: function(event) {
				// after user selects an option from the list
				var result = event.result.raw;
				// global variable to be used also in the create new supplier pop-up
				supplierData = result;
				A.one('#<portlet:namespace/>id_supplier_raport').val(result.id);
			}
		}
	});	
});
</script>
<aui:layout>
	<aui:column columnWidth="15" first="true">
		<label for="<portlet:namespace />supplier_list" style="padding-top: 5px;">Furnizor</label>
	</aui:column>
	<aui:column>
		<aui:input id="supplier_list" name="supplier_list" disabled="true" label="" style="width:200px;"></aui:input>
		<aui:input id="id_supplier_raport" name="id_supplier_raport" value="" type="hidden" label=""/>
	</aui:column>
</aui:layout>
	<aui:layout>
	<aui:column columnWidth="15" first="true">
		<label style="padding-top:5px" for="start_date">Data contabila inceput: </label>
	</aui:column>
	<aui:column columnWidth="10"  style="text-align:right">
		<aui:input label="" name="start_date_an" id="start_date_an" type="text" value="" style="width:90%" placeholder="An contabil">
<%-- 			 <aui:validator name="required" errorMessage="Campul este obligatoriu." /> --%>
		</aui:input>
	</aui:column>
	<aui:column columnWidth="15"  style="text-align:right">
		<aui:select id="start_date_luna" name="start_date_luna" label="" style="width:100%" required="true">
				<aui:option selected="selected" value="0">Luna contabil</aui:option>
				<aui:option value="1">Ianuarie</aui:option>
				<aui:option value="2">Februarie</aui:option>
				<aui:option value="3">Martie</aui:option>
				<aui:option value="4">Aprilie</aui:option>
				<aui:option value="5">Mai</aui:option>
				<aui:option value="6">Iunie</aui:option>
				<aui:option value="7">Iulie</aui:option>
				<aui:option value="8">August</aui:option>
				<aui:option value="9">Septembrie</aui:option>
				<aui:option value="10">Octombrie</aui:option>
				<aui:option value="11">Noiembrie</aui:option>
				<aui:option value="12">Decembrie</aui:option>
			</aui:select>
	</aui:column>
</aui:layout>
<aui:layout>
	<aui:column columnWidth="15" first="true">
		<label style="padding-top:5px" for="end_date">Data contabila sfarsit: </label>
	</aui:column>
	<aui:column columnWidth="10"  style="text-align:right">
		<aui:input label="" name="end_date_an" id="end_date_an" type="text" value="" style="width:90%" placeholder="An contabil">
<%-- 			 <aui:validator name="required" errorMessage="Campul este obligatoriu." /> --%>
		</aui:input>
	</aui:column>
	<aui:column columnWidth="15"  style="text-align:right">
		<aui:select id="end_date_luna" name="end_date_luna" label="" style="width:100%" required="true">
				<aui:option selected="selected" value="0">Luna contabil</aui:option>
				<aui:option value="1">Ianuarie</aui:option>
				<aui:option value="2">Februarie</aui:option>
				<aui:option value="3">Martie</aui:option>
				<aui:option value="4">Aprilie</aui:option>
				<aui:option value="5">Mai</aui:option>
				<aui:option value="6">Iunie</aui:option>
				<aui:option value="7">Iulie</aui:option>
				<aui:option value="8">August</aui:option>
				<aui:option value="9">Septembrie</aui:option>
				<aui:option value="10">Octombrie</aui:option>
				<aui:option value="11">Noiembrie</aui:option>
				<aui:option value="12">Decembrie</aui:option>
			</aui:select>
	</aui:column>
</aui:layout>
	<aui:column columnWidth="100">
		<aui:column>
			<a class="btn btn-primary" onclick="filterListing('<portlet:namespace />')"> Aplica Filtre </a>
		</aui:column>
		<aui:column>
			<a class="btn btn-primary" onclick="exportRaport('<portlet:namespace />')"> Export Raport </a>
		</aui:column>
	</aui:column>
	</aui:form>
</aui:layout>
<br/>
<aui:layout>
	<aui:column first="true" columnWidth="100" last="true">
		<div id="validare_div"></div>
	</aui:column>
</aui:layout>

<%-- HTML for PAGINATION --%>
<%@ include file="/html/includes/pagination.jsp" %>

<%

int start 	= 0; 
int count 	= 15;
int total = FacturiBlocatePlata.getRejectedInvoicesTotalPaginare(0, 0, 0, 0, 0, 0, 0);
int companyId = 0;
int supplierid = 0;
int motiv = 0;
int startLuna = 0;
int startAn = 0;
int endLuna = 0;
int endAn = 0;

		List<HashMap<String,Object>> allInvoices = null;
		allInvoices = FacturiBlocatePlata.getRejectedInvoices(start, count, companyId, supplierid, motiv, startLuna, startAn, endLuna, endAn, "");
		allInvoices.addAll(FacturiBlocatePlata.getRejectedInvoicesTotal(start, count, companyId, supplierid, motiv, startLuna, startAn, endLuna, endAn, ""));
		
%>
<script type="text/javascript">;


$('#<portlet:namespace/>societate').change(function(e){
	var val = $('#<portlet:namespace/>societate').val();
	if (val != 0){
		$('#<portlet:namespace/>supplier_list').prop("disabled", false);
	} else {
		$('#<portlet:namespace/>supplier_list').prop("disabled", true);
	}
});

var dataTable;
var start = <%=start%>
var count = <%=count%>
var total = <%=total%>
var societate = <%=companyId%>
var furnizor = <%=supplierid%>
var motiv = <%=motiv%>
var start_date_an = <%=startAn%>
var start_date_luna = <%=startLuna%>
var end_date_luna = <%=endLuna%>
var end_date_an = <%=endAn%>

function filterListing(portletId){
	societate = $('#<portlet:namespace/>societate').val();
	furnizor = $('#<portlet:namespace/>id_supplier_raport').val();
	motiv = $('#<portlet:namespace/>motiv').val();
	start_date_an = $('#<portlet:namespace/>start_date_an').val();
	start_date_luna = $('#<portlet:namespace/>start_date_luna').val();
	end_date_luna = $('#<portlet:namespace/>end_date_luna').val();
	end_date_an = $('#<portlet:namespace/>end_date_an').val();
	
	var action = "filter";
	makeAjaxCall(action);
}

function exportRaport(portletId){
	$('#<portlet:namespace/>exportFormRaport').submit();
}





YUI({lang: 'ro'}).use( 'aui-datatable', 'aui-datatype', 'datatable-sort', 'aui-datepicker',
	function(Y) {
		// data array
			
		var remoteData = [
		   <% for (int i = 0; i < allInvoices.size(); i++) { %>
					{
						inv_id: '<%=allInvoices.get(i).get("inv_id")%>',
						fullname: '<%=allInvoices.get(i).get("fullname")%>',
						societate: '<%=allInvoices.get(i).get("societate")%>',
						data_import: '<%=allInvoices.get(i).get("data_import")%>',
						inv_number : '<%=allInvoices.get(i).get("inv_number")%>',
						inv_date: '<%=allInvoices.get(i).get("inv_date")%>',
						supplier_code: '<%=allInvoices.get(i).get("supplier_code")%>',
						supplier_name: '<%=allInvoices.get(i).get("supplier_name")%>',
						magazin: '<%=allInvoices.get(i).get("magazin")%>',
						data_scadenta: '<%=allInvoices.get(i).get("data_scadenta")%>',
						total_with_vat_ron: '<%=allInvoices.get(i).get("total_with_vat_ron")%>',
						currency: '<%=allInvoices.get(i).get("currency")%>',
						total_with_vat_curr : '<%=allInvoices.get(i).get("total_with_vat_curr")%>',
						motiv_blocat : '<%=allInvoices.get(i).get("motiv_blocat")%>',
						detalii_motiv : '<%=allInvoices.get(i).get("detalii_motiv")%>',
						neajuns_scadenta : '<%=allInvoices.get(i).get("neajuns_scadenta")%>',
						mai_mic_30_zile : '<%=allInvoices.get(i).get("mai_mic_30_zile")%>',
						intre_30_90 : '<%=allInvoices.get(i).get("intre_30_90")%>',
						intre_90_365 : '<%=allInvoices.get(i).get("intre_90_365")%>',
						mai_mare_365 : '<%=allInvoices.get(i).get("mai_mare_365")%>',
					}
				<% if (i != allInvoices.size() - 1) {  out.print(",");} %>
			<% } %>
		];
		
		function formatCurrency(cell) {
		    format = {
		    	thousandsSeparator: ",",
		        decimalSeparator: ".",
		        decimalPlaces: noOfDecimalsToDisplay
		    };
		    return Y.DataType.Number.format(Number(cell.value), format);
		}
		
		
		// COLUMN INFO and ATTRIBUTES
		var nestedCols = [
		    { key: 'inv_id', className: 'hiddencol'},
		    { key: 'societate', label: 'Societate'},	
		    { key: 'data_import', label: 'Data import'},	
		    { key: 'inv_number', label: 'Factura nr.'},
		    { key: 'inv_date', label: 'Data factura'},
		    { key: 'fullname', label: 'Utilizator'},
		    { key: 'supplier_code', label: 'Cod furnizor'},
		    { key: 'supplier_name', label: 'Nume furnizor'},
		    { key: 'magazin', label: 'Magazin'},
		    { key: 'data_scadenta', label: 'Data scadenta'},
		    { key: 'total_with_vat_ron',formatter: formatCurrency, label: 'Val cu TVA (RON)',className: 'currencyCol'},
		    { key: 'total_with_vat_curr',formatter: formatCurrency, label: 'Val cu TVA (EUR)',className: 'currencyCol'},
		    { key: 'motiv_blocat', label: 'Motiv blocare plata'},
		    { key: 'detalii_motiv', label: 'Detalii motiv'},
		    { key: 'neajuns_scadenta',formatter: formatCurrency, label: 'Neajuns la scadenta',className: 'currencyCol'},
		    { key: 'mai_mic_30_zile',formatter: formatCurrency, label: 'Intarziere mai mica de 30 zile',className: 'currencyCol'},
		    { key: 'intre_30_90',formatter: formatCurrency, label: 'Intarziere intre 30 si 90 zile',className: 'currencyCol'},
		    { key: 'intre_90_365',formatter: formatCurrency, label: 'Intarziere intre 90 si 365 zile',className: 'currencyCol'},
		    { key: 'mai_mare_365',formatter: formatCurrency, label: 'Intarziere mai mare 365 zile',className: 'currencyCol'}
		];
		
					
		// TABLE INIT
			 dataTable = new Y.DataTable({
		    columns: nestedCols,
		    data: remoteData,	
		    scrollable: 'x',
		    width: '100%',		    
		    recordType: ['inv_id','societate', 'data_import', 'inv_number', 'inv_date', 'supplier_code', 'supplier_name', 'magazin', 'data_scadenta',
		                 'total_with_vat_ron','total_with_vat_curr', 'motiv_blocat', 'detalii_motiv','neajuns_scadenta','mai_mic_30_zile',
		                 'mai_mic_30_zile','intre_30_90','intre_90_365','mai_mare_365']
		}).render('#validare_div');
		
	 	// PAGINATION
  		// set status of navigation buttons on page load
  		setNavigationButtonsState('<portlet:namespace />');
  		$('.navigation').click(function(e) {
  			var elementT 	= e.target;
  			var elementId 	= elementT.id;
  			var action 		= "";
  			
  			// determine which button was pressed and set the value of start accordingly
  			if (elementId.indexOf("first") > -1)	{ action = "first"; start = 0; }
  			if (elementId.indexOf("prev") > -1) 	{ action = "prev"; if (start - count <= 0) {start = 0} else { start -= count; } }
  			if (elementId.indexOf("next") > -1) 	{ action = "next"; start = parseInt(start) + parseInt(count); }
  			if (elementId.indexOf("last") > -1) 	{ action = "last"; start = parseInt(parseInt(total) / parseInt(count)) * count; if (start == total) { start -= count; } }
  			
  			makeAjaxCall(action);
  		});
  		
  		}
);

function makeAjaxCall(action) {
		$.ajax({
			type: "POST",
			url: "<%=ajaxURL%>",
			data: 	"<portlet:namespace />action=" + action + 
					"&<portlet:namespace />start=" + start + 
					"&<portlet:namespace />count=" + count +
					"&<portlet:namespace />total=" + total +
					"&<portlet:namespace />societate=" + societate +
					"&<portlet:namespace />furnizor=" + furnizor +
					"&<portlet:namespace />motiv=" + motiv +
					"&<portlet:namespace />start_date_an=" + start_date_an +
					"&<portlet:namespace />start_date_luna=" + start_date_luna +
					"&<portlet:namespace />end_date_luna=" + end_date_luna +
					"&<portlet:namespace />end_date_an=" + end_date_an,
			success: function(msg) {
				// get table data
				if (msg != ""){
					var jsonEntries = jQuery.parseJSON(msg);
					total = jsonEntries.total;
					dataTable.set('data', eval(jsonEntries.values));
				} else {
					dataTable.set('data', []);
				}
				
				// set status of navigation buttons on page load
				setNavigationButtonsState('<portlet:namespace />');
			}
		});
	}
</script>
