<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@page import="com.profluo.ecm.controller.UserTeamIdUtils"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.List"%>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme"%>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui"%>
<%@ taglib uri="http://alloy.liferay.com/tld/aui" prefix="aui"%>
<%@ taglib uri="http://liferay.com/tld/util" prefix="liferay-util"%>
<%@page import="com.profluo.ecm.model.db.FacturiProblemeExport"%>

<liferay-theme:defineObjects />
<portlet:defineObjects />

<aui:layout>
	<aui:column first="true" columnWidth="100" last="true">
		<div id="validare_div"></div>
	</aui:column>
</aui:layout>

<%

int total = FacturiProblemeExport.getFacturiPaginare();

		List<HashMap<String,Object>> allInvoices = null;
		allInvoices = FacturiProblemeExport.getRejectedInvoices();
		
		
%>
<script type="text/javascript">;
var dataTable;

YUI({lang: 'ro'}).use( 'aui-datatable', 'aui-datatype', 'datatable-sort', 'aui-datepicker',
	function(Y) {
		// data array
			
		var remoteData = [
		   <% for (int i = 0; i < allInvoices.size(); i++) { %>
					{
						id: '<%=allInvoices.get(i).get("id")%>',
						inv_number: '<%=allInvoices.get(i).get("inv_number")%>',
						inv_date: '<%=allInvoices.get(i).get("inv_date")%>',
						supp_name: '<%=allInvoices.get(i).get("name")%>',
						problema : '<%=allInvoices.get(i).get("problema")%>',
						dataProblema: '<%=allInvoices.get(i).get("dataProblema")%>'
					}
					<% if (i != allInvoices.size() - 1) {  out.print(",");} %>
				<% } %>
			];
		
		// COLUMN INFO and ATTRIBUTES
		var nestedCols = [
		    { key: 'id', className: 'hiddencol'},
		    { key: 'inv_number', label: 'Numar factura'},	
		    { key: 'inv_date', label: 'Data factura'},	
		    { key: 'supp_name', label: 'Furnizor'},
		    { key: 'problema', label: 'Problema'},
		    { key: 'dataProblema', label: 'Data problema'}
		];
		
					
		// TABLE INIT
			 dataTable = new Y.DataTable({
		    columns: nestedCols,
		    data: remoteData,	
		    scrollable: 'x',
		    width: '100%',		    
		    recordType: ['id','inv_number','inv_date','supp_name','problema','dataProblema']
		}).render('#validare_div');
	}
);

</script>

