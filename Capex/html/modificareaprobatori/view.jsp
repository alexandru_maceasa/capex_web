<%@page import="com.profluo.ecm.model.db.InvoiceHeader"%>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui"%>
<%@ taglib uri="http://alloy.liferay.com/tld/aui" prefix="aui"%>
<%@ taglib uri="http://liferay.com/tld/util" prefix="liferay-util"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme"%>

<%@page import="java.util.HashMap"%>
<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>


<liferay-theme:defineObjects />
<portlet:defineObjects />

<portlet:resourceURL id="ajaxURL" var="ajaxURL"/>
<portlet:renderURL var="viewInvoiceURL" >
	<portlet:param name="mvcPath" value="/html/modificareaprobatori/modifica_aprobator.jsp"></portlet:param>
</portlet:renderURL>

<%
	int start = 0;
	int count = 15;
	int total = 0;
	List<HashMap<String,Object>> allInvoices = InvoiceHeader.getAllRegisteredInvoices(start, count, "", "");
	total = InvoiceHeader.getAllRegisteredInvoicesCount("");
%>

<aui:layout>
	<aui:column first="true" columnWidth="100">
		<div id="invoices"></div>
	</aui:column>
</aui:layout>
<%-- HTML for PAGINATION --%>
<%@ include file="/html/includes/pagination.jsp" %>

<script type="text/javascript">
	var start = "<%=start%>";
	var count = "<%=count%>";
	var total = "<%=total%>";
	

	YUI().use( 'aui-datatable', 'aui-datatype', 'datatable-sort',
		function(Y) {
			var remoteData = [
			                  <% for(int i = 0; i< allInvoices.size(); i++) { %>
			                  	{
			                  	  id:				'<%=allInvoices.get(i).get("id")%>',
			                  	  inv_number: 		'<%=allInvoices.get(i).get("inv_number")%>',
			                  	  inv_date: 		'<%=allInvoices.get(i).get("inv_date")%>',
			                  	  store_name: 		'<%=allInvoices.get(i).get("store_name")%>',
			                  	  supplier:			'<%=allInvoices.get(i).get("supplier")%>'
		                		}
			                  <% if (i != (allInvoices.size() - 1)) { out.print(","); } %>
			                  <% } %>
			                  ];
			var nestedCols = [
							  { key	: 'id', 		className: 'hiddencol'},
			                  { key : 'inv_number', label : '<input type="text" id="filtru_facturi" placeholder="Nr. factura" style="width:50%;margin:0;"/>'},
			                  { key : 'inv_date', 	label : 'Data factura'},
			                  { key : 'store_name', label : 'Magazin'},
			                  { key : 'supplier', 	label : '<input type="text" id="filtru_furnizori" placeholder="Furnizor" style="width:50%;margin:0;"/>'},
			                  { key	: 'detalii', 	label: 'Detalii', allowHTML: true, 
			                	  formatter: '<a href="#" id="row{id}" class="editrow">Modifica aprobator</a>' 
			                  }
			                  ];
			var dataTable = new Y.DataTable({
			    columns: nestedCols,
			    data: remoteData,	
			    editEvent: 'click',
			    recordType: ['id' ,'inv_number', 'inv_date', 'store_name', 'supplier', 'detalii']
			}).render('#invoices');
			
			
			// PAGINATION
	  		// set status of navigation buttons on page load
	  		setNavigationButtonsState('<portlet:namespace />');
	  		$('.navigation').click(function(e) {
	  			var elementT 	= e.target;
	  			var elementId 	= elementT.id;
	  			var action 		= "";
	  			
	  			// determine which button was pressed and set the value of start accordingly
	  			if (elementId.indexOf("first") > -1)	{ action = "first"; start = 0; }
	  			if (elementId.indexOf("prev") > -1) 	{ action = "prev"; if (start - count <= 0) {start = 0} else { start -= count; } }
	  			if (elementId.indexOf("next") > -1) 	{ action = "next"; start = parseInt(start) + parseInt(count); }
	  			if (elementId.indexOf("last") > -1) 	{ action = "last"; start = parseInt(parseInt(total) / parseInt(count)) * count; if (start == total) { start -= count; } }
	  			
	  			makeAjaxCall(action, "");
	  		});
	  		
	  		function makeAjaxCall(action, factNo, numeFurnizor) {
	  			$.ajax({
	  				type: "POST",
	  				url: "<%=ajaxURL%>",
	  				data: 	"<portlet:namespace />action=" + action + 
	  						"&<portlet:namespace />start=" + start + 
	  						"&<portlet:namespace />count=" + count +
	  						"&<portlet:namespace />total=" + total + 
	  						"&<portlet:namespace />factNo=" + factNo +
	  						"&<portlet:namespace />furnizor=" + numeFurnizor,
	  				success: function(msg) {
	  					// get table data
	  					if (msg != ""){
	  						var jsonEntries = jQuery.parseJSON(msg);
	  						total = jsonEntries.total;
	  						dataTable.set('data', eval(jsonEntries.values));
	  					} else {
	  						dataTable.set('data', []);
	  					}
	  					
	  					// set status of navigation buttons on page load
	  					setNavigationButtonsState('<portlet:namespace />');
	  				}
	  			});
	  		}
	  		
	  		$('#filtru_facturi').keyup(function (e) {
			    if ($("#filtru_facturi").is(":focus") && (e.keyCode == 13)) {
					var action 		= "filter";
					invoiceNo = e.target.value;
					numeFurnizor = $('#filtru_furnizori').val();
					start = 0;
					total = 0;
					makeAjaxCall(action, invoiceNo, numeFurnizor);			
			    }
			});
	  		
	  		$('#filtru_furnizori').keyup(function (e) {
			    if ($("#filtru_furnizori").is(":focus") && (e.keyCode == 13)) {
					var action 		= "filter";
					numeFurnizor = e.target.value;
					invoiceNo = $('#filtru_facturi').val();
					start = 0;
					total = 0;
					makeAjaxCall(action, invoiceNo, numeFurnizor);			
			    }
			});
	  		
	  		dataTable.delegate('click', function (e) {
	  		    // undefined to trigger the emptyCellValue
	  		    e.preventDefault();
	  		    var elem = e.target.get('id');
	  		    var invoiceId = elem.replace('row',"");
	  		  	$('#<portlet:namespace />id').val(invoiceId);
	  		  	$('#<portlet:namespace />viewInvoice').submit();
	  		}, '.editrow', dataTable);
	});
</script>

<aui:form method="post" action="<%=viewInvoiceURL.toString() %>" name="viewInvoice" id="viewInvoice" style="margin:0;padding:0">
	<aui:input type="hidden" id="id" name="id"/>
</aui:form>