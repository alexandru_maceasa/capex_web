<%@page import="com.profluo.ecm.model.vo.StatusFacturiBD"%>
<%@page import="com.profluo.ecm.model.vo.DefDpis"%>
<%@page import="com.profluo.ecm.model.db.UsersInformations"%>
<%@page import="com.profluo.ecm.model.db.InvoiceApprovals"%>
<%@page import="com.profluo.ecm.model.db.DefProduct"%>
<%@page import="com.profluo.ecm.model.db.DefStore"%>
<%@page import="com.profluo.ecm.model.db.DefRelComer"%>
<%@page import="com.profluo.ecm.model.db.DefCompanies"%>
<%@page import="com.profluo.ecm.model.db.DefSupplier"%>
<%@page import="com.profluo.ecm.model.db.InvoiceHeader"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.List"%>
<%@page import="java.util.ResourceBundle"%>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui"%>
<%@ taglib uri="http://alloy.liferay.com/tld/aui" prefix="aui"%>
<%@ taglib uri="http://liferay.com/tld/util" prefix="liferay-util"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme"%>

<liferay-theme:defineObjects />
<portlet:defineObjects />

<portlet:resourceURL id="ajaxURL" var="ajaxURL"/>
<portlet:actionURL name="updateApproval" var="submitURL" />

<%
	locale 					= renderRequest.getLocale();
	ResourceBundle resmain 	= portletConfig.getResourceBundle(locale);
	String id			= "";
	try {
		id						= renderRequest.getParameter("id").toString();
	} catch (Exception e){ }
	
	
	System.out.println(" ID : " + id);
	
	
	List<HashMap<String,Object>> oneInvoiceHeader 				= InvoiceHeader.getHeaderById(id, "invoice_header");
	List<HashMap<String,Object>> supplierInfo 					= DefSupplier.getSupplierInfo(oneInvoiceHeader.get(0).get("id_supplier").toString());
	//List<HashMap<String,Object>> invoiceLinesByInvoiceNr 		= InvoiceHeader.getLinesById(id, "invoice_line");
	List<HashMap<String,Object>> invoiceStoreLinesByInvoiceNr 	= InvoiceHeader.getStoreLinesForApprovals(id, "invoice_store_line");
	List<HashMap<String,Object>> aprobatori						= InvoiceApprovals.getAllUsers();
%>
<aui:form method="post" id="unId" name="unName">
	<aui:fieldset>
		<legend><strong>Factura</strong></legend>
		<aui:column columnWidth="10" first="true">
			<label for="<portlet:namespace />inv_number" style="padding-top: 5px">Numar factura </label>
		</aui:column>
		<aui:column columnWidth="20">
			<aui:input type="text" id="inv_number" disabled="true" name="inv_number" value="<%=oneInvoiceHeader.get(0).get(\"inv_number\") %>" label=""/>
		</aui:column>
		<aui:column columnWidth="10">
			<label for="<portlet:namespace/>inv_date" style="padding-top: 5px"><%=resmain.getString("datafactura")%></label>
		</aui:column>
		<aui:column columnWidth="20">
			<aui:input type="date" id="inv_date" name="inv_date" disabled="true" value="<%=oneInvoiceHeader.get(0).get(\"inv_date\") %>" 
				placeholder="aaaa-ll-zz" label=""/>
		</aui:column>
		<aui:column columnWidth="10">
			<label for="<portlet:namespace />company"
				style="padding-top: 5px"><%=resmain.getString("societate")%></label>
		</aui:column>
		<aui:column columnWidth="20">
			<aui:input type="text" id="societate" disabled="true" name="societate" value="<%=DefCompanies.getCompanyNameById(oneInvoiceHeader.get(0).get(\"id_company\").toString()) %>" label=""/>
		</aui:column>
	</aui:fieldset>
	<aui:fieldset>
		<aui:layout>
			<aui:column columnWidth="10" first="true">
				<label for="<portlet:namespace />tip-rel-comerciala"
					style="padding-top: 5px"><%=resmain.getString("tip-rel-comerciala")%></label>
			</aui:column>
			<aui:column columnWidth="20">
				<aui:input type="text" id="tip-rel-comerciala" disabled="true" name="tip-rel-comerciala" value="<%=DefRelComer.getTipRelComNameById(Integer.parseInt(oneInvoiceHeader.get(0).get(\"tip_rel_com\").toString())) %>" label=""/>
			</aui:column>
			<aui:column columnWidth="10" id = "label_contract_display">
				<label for="<portlet:namespace />nr-contract" style="padding-top: 5px"><%=resmain.getString("nr-contract")%></label>
			</aui:column>
			<aui:column columnWidth="20" id="numar_contract_display">
				<aui:input id="nr-contract" name="nr-contract" label="" disabled="true" value = "<%=oneInvoiceHeader.get(0).get(\"co_number\") %>">
				</aui:input>
			</aui:column>
			<aui:column columnWidth="10" id="label_comanda_display">
				<label for="<portlet:namespace />nr-comanda" style="padding-top: 5px"><%=resmain.getString("nr-comanda")%></label>
			</aui:column>
			<aui:column columnWidth="20" id="numar_comanda_display">
				<aui:input id="nr-comanda" name="nr-comanda" label="" disabled="true" value = "<%=oneInvoiceHeader.get(0).get(\"order_no\") %>">
				</aui:input>
			</aui:column>
		</aui:layout>
	</aui:fieldset>
	<aui:fieldset>
		<legend><strong>Furnizor</strong></legend>
		<aui:layout>
			<aui:column columnWidth="10" first="true">
				<label for="<portlet:namespace />furnizor" style="padding-top: 5px">Furnizor</label>
			</aui:column>
			<aui:column columnWidth="20">
				<aui:input type="text" id="furnizor" name="furnizor" label="" disabled="true" value="<%=supplierInfo.get(0).get(\"name\") %>"/>
			</aui:column>
			<aui:column columnWidth="10" first="true">
				<label for="<portlet:namespace />cui" style="padding-top: 5px">CUI</label>
			</aui:column>
			<aui:column columnWidth="20">
				<aui:input type="text" id="cui" name="cui" label="" disabled="true" value="<%=supplierInfo.get(0).get(\"cui\") %>"/>
			</aui:column>
			<aui:column columnWidth="10" first="true">
				<label for="<portlet:namespace />account" style="padding-top: 5px">Cont bancar</label>
			</aui:column>
			<aui:column columnWidth="20">
				<aui:input type="text" id="account" name="account" label="" disabled="true" value="<%=supplierInfo.get(0).get(\"bank_account\") %>"/>
			</aui:column>
		</aui:layout>
		<aui:layout>
			<aui:column columnWidth="10" first="true">
				<label for="<portlet:namespace />cont_kontan" style="padding-top: 5px">Cont Kontan</label>
			</aui:column>
			<aui:column columnWidth="20">
				<aui:input type="text" id="cont_kontan" name="cont_kontan" label="" disabled="true" value="<%=oneInvoiceHeader.get(0).get(\"kontan_acc\") %>"/>
			</aui:column>
			<aui:column columnWidth="10" first="true">
				<label for="<portlet:namespace />tip_furnizor" style="padding-top: 5px">Tip furnizor</label>
			</aui:column>
			<aui:column columnWidth="20">
				<aui:input type="text" id="tip_furnizor" name="tip_furnizor" label="" disabled="true" value="<%=oneInvoiceHeader.get(0).get(\"tip_furnizor\") %>"/>
			</aui:column>
		</aui:layout>
	</aui:fieldset>
	<aui:fieldset>
		<legend><strong>Valoare RON</strong></legend>
		<aui:layout>
			<aui:column columnWidth="10" first="true">
				<label for="<portlet:namespace />val_no_vat_ron" style="padding-top: 5px">Valoare fara TVA</label>
			</aui:column>
			<aui:column columnWidth="20">
				<aui:input type="text" id="val_no_vat_ron" name="val_no_vat_ron" label="" disabled="true" value="<%=oneInvoiceHeader.get(0).get(\"total_no_vat_ron\") %>"/>
			</aui:column>
			<aui:column columnWidth="10" first="true">
				<label for="<portlet:namespace />val_vat_ron" style="padding-top: 5px">Valoare TVA</label>
			</aui:column>
			<aui:column columnWidth="20">
				<aui:input type="text" id="val_vat_ron" name="val_no_vat_ron" label="" disabled="true" value="<%=oneInvoiceHeader.get(0).get(\"total_vat_ron\") %>"/>
			</aui:column>
			<aui:column columnWidth="10" first="true">
				<label for="<portlet:namespace />val_with_vat_ron" style="padding-top: 5px">Valoare cu TVA</label>
			</aui:column>
			<aui:column columnWidth="20">
				<aui:input type="text" id="val_with_vat_ron" name="val_with_vat_ron" label="" disabled="true" value="<%=oneInvoiceHeader.get(0).get(\"total_with_vat_ron\") %>"/>
			</aui:column>
		</aui:layout>
	</aui:fieldset>
</aui:form>

<aui:layout>
	<legend><strong>Linii factura</strong></legend>
	<aui:column columnWidth="10" first="true">
		<label for="<portlet:namespace />aprobatorGeneral" style="padding-top: 5px">Setare aprobator</label>
	</aui:column>
	<aui:column columnWidth="20">
		<aui:select label="" name="aprobatorGeneral" id="aprobatorGeneral">
			<% for(int i = 0 ; i < aprobatori.size(); i++ ) { %>
				<aui:option value="<%=aprobatori.get(i).get(\"id\")%>"> 
					<%=aprobatori.get(i).get("fullname")%>
				</aui:option>
			<% } %>
		</aui:select>
	</aui:column>
	<aui:column columnWidth="10">
		<aui:button id="alocaAprobator" value="Aloca aprobator"></aui:button>
	</aui:column>
	<aui:column columnWidth="10">
		<aui:button id="stergeAprobatori" value="Sterge aprobatori"></aui:button>
	</aui:column>
	<aui:column columnWidth="10">
		<label for="<portlet:namespace />aprobatorGeneralFinal" style="padding-top: 5px">Aprobator final</label>
	</aui:column>
	<aui:column columnWidth="10">
		<aui:select style="width:50%" label="" name="aprobatorGeneralFinal" id="aprobatorGeneralFinal">
			<aui:option value="1">Da</aui:option>
			<aui:option value="0">Nu</aui:option>
		</aui:select>
	</aui:column>
	<aui:column columnWidth="20">
		<aui:button id="alocaAprobatorFinal" value="Setare aprobator final"></aui:button>
	</aui:column>
	<aui:column first="true" columnWidth="100">
		<div id="invoice_lines"></div>
	</aui:column>
</aui:layout>

<aui:layout>
	<aui:column last="true">
		<a href="#" class="btn btn-primary fr submit_form" style="margin-right:0px">Validare</a>
	</aui:column>
</aui:layout>

<script type="text/javascript">
$(document).ready(function() {
	$("#<portlet:namespace/>aprobatorGeneral").chosen();
});
YUI().use( 'aui-datatable', 'aui-datatype', 'datatable-sort',
	function(Y) {
	
	var tip_aprobator = {0:'Nu', 1:'Da'};
	
	var allUsers = <%=InvoiceApprovals.getJsonStringUsers()%>;
		var remoteData = [
		                  <% for(int i = 0; i< invoiceStoreLinesByInvoiceNr.size(); i++) {
		                	  String userId = InvoiceApprovals.getfirstApprovalByStoreLineId(invoiceStoreLinesByInvoiceNr.get(i).get("id").toString());
		                	  String currentApproval = "";
		                	  if(!userId.contains("fictive") && !userId.equals("") ) {
		                		String [] userIds = userId.split(",");
		                		if(userIds.length > 0 ){
		                			for(int j = 0 ; j < userIds.length; j++ ){
			                			List<HashMap<String,Object>> userDetail = UsersInformations.getUserDetailsByUserIdForApprovals(Integer.parseInt(userIds[j]));
				                 	  	currentApproval = currentApproval + userDetail.get(0).get("fullname").toString() + ", ";
			                		}
		                		} else {
		                			currentApproval = "Linie aprobata";
		                		}
		                	  } else {
		                		  if(StatusFacturiBD.isApproved(Integer.parseInt(invoiceStoreLinesByInvoiceNr.get(i).get("id").toString()))) { 
		                		 	 currentApproval = "Linie aprobata";
		                		  } else {
		                		 	currentApproval = "Linia nu are aprobator setat";
		                		 } 
		                	  }
		                	  if(currentApproval.endsWith(", ")) {
		                		  currentApproval = currentApproval.substring(0, currentApproval.length()-2);
		                	  }
		                 	  %>
		                  	{
		                  	  line_id:			'<%=invoiceStoreLinesByInvoiceNr.get(i).get("id")%>',
		                  	  product_code: 	'<%=DefProduct.getProductCodeWithNameById(invoiceStoreLinesByInvoiceNr.get(i).get("product_code").toString())%>',
		                  	  article: 			'<%=invoiceStoreLinesByInvoiceNr.get(i).get("description")%>',
		                  	  store: 			'<%=DefStore.getStoreNameById(invoiceStoreLinesByInvoiceNr.get(i).get("id_store").toString())%>',
		                	  aprobator:		'<%=currentApproval%>',
		                	  aprobator_final:	1,
		                	  next_approvals:	'<%=userId%>',
		                	  old_approvals:	'<%=userId%>'
	                		}
		                  <% if (i != (invoiceStoreLinesByInvoiceNr.size() - 1)) { out.print(","); } %>
		                  <% } %>
		                  ];
		var nestedCols = [
						  { key: 'line_id', className: 'hiddencol'},
						  { key: 'next_approvals', className: 'hiddencol'},
						  { key: 'old_approvals', className: 'hiddencol'},
		                  { key : 'product_code', label : 'Cod produs'},
		                  { key : 'article', label : 'Denumire articol'},
		                  { key : 'store', label : 'Magazin'},
		                  { key: 'user_id', label: 'Lista utilizatori', editor: new Y.DropDownCellEditor({options: allUsers, id: 'allUsers', elementName: 'allUsers',
		         				after: {
		         		            focus: function(event) {
		         			            $("select[name=allUsers]").chosen();
		         		            }
		         				}
		         			}), formatter: function(o) {
		      	   				var obj = JSON.parse(JSON.stringify(allUsers));
		      	   				if (typeof o.value != 'undefined') {
		      	   					return obj[o.value].toString();
		      	   				} else {
		      	   					return '0';
		      	   				}
		         				}
		         			},
		                  { key : 'aprobator', label : 'Aprobator' },
		                  { key : 'aprobator_final' , label : 'Aprobator final?', 
		                	  	editor: new Y.DropDownCellEditor({options: tip_aprobator, on : {change: saveOnChange}, showToolbar: false}), emptyCellValue: '', 
		          				formatter: function(o) { var obj = JSON.parse(JSON.stringify(tip_aprobator)); return obj[o.value].toString(); }
		         		  },
		                  { key : 'remove', label : 'Sterge aprobator', allowHTML: true, 
		                	  formatter: function(o) {
		         						if (o.data.line_id != ''){
		         	   						return '<a href="#" id="row' + o.data.line_id + '" class="deleteApprovals" ><img src="${themeDisplay.getPathThemeImages()}/delete.png" width="20"/></a>';
		         						}else {
		         							return '<a href="#" id="row' + o.value + '" class="deleteApprovals"><img src="${themeDisplay.getPathThemeImages()}/delete.png" width="20"/></a>';
		         						}
		         				}
		                  }
		                  ];
		var dataTable = new Y.DataTable({
		    columns: nestedCols,
		    data: remoteData,	
		    editEvent: 'click',
		    recordType: ['line_id', 'old_approvals', 'next_approvals', 'product_code', 'article', 'store', 'user_id', 'aprobator_final', 'aprobator']
		}).render('#invoice_lines');
		
		dataTable.delegate('click', function (e) {
   			e.preventDefault();
   			var rows = [];
   		  	var target = e.currentTarget.get('id');
   		  	var lineId = target.replace("row", "");
   		 	var ml = dataTable.data, msg = '', template = '';
	   		 ml.each(function (item, i) {
	   	    	var data = item.getAttrs(['line_id', 'old_approvals', 'next_approvals', 'product_code', 'article', 'store', 'user_id', 'aprobator_final', 'aprobator']);
	   	    	if(lineId == data.line_id) {
	   	    		data.aprobator = "";
	   	    		data.next_approvals = "";
	   	    	}
	   	    	rows.push(data);
	   		  });
	   		 dataTable.set('recordset', rows);
		}, '.deleteApprovals', dataTable);
		
		//stergerea tuturor aprobatorilor
		$('#stergeAprobatori').click(function(e) {
			e.preventDefault();
			var rows = [];
			var ml = dataTable.data, msg = '', template = '';
			 ml.each(function (item, i) {
				 var data = item.getAttrs(['line_id', 'old_approvals', 'next_approvals', 'product_code', 'article', 'store', 'user_id', 'aprobator_final', 'aprobator']);
				 data.aprobator = "";
	   	    	 data.next_approvals = "";
	   	    	 rows.push(data);
			 });
			 dataTable.set('recordset', rows);
		});
		
		//alocarea unui aprobator pe toate liniile
		$('#alocaAprobator').click(function(e) {
			e.preventDefault();
			var user_id = $('#<portlet:namespace/>aprobatorGeneral').val();
			var rows = [];
			var ml = dataTable.data, msg = '', template = '';
			ml.each(function (item, i) {
				var data = item.getAttrs(['line_id', 'old_approvals', 'next_approvals', 'product_code', 'article', 'store', 'user_id', 'aprobator_final', 'aprobator']);
				if(data.aprobator != "Linie aprobata") {
	   	    		if(data.aprobator != "" ) {
	   	    			var obj = JSON.parse(JSON.stringify(allUsers));
	   	    			data.aprobator = data.aprobator + ", " + obj[user_id].toString();
	   	    			if (data.next_approvals.charAt(data.next_approvals.length-1) == ',') {
	   	    				data.next_approvals = data.next_approvals + user_id;
	   	    			} else {
	   	    				data.next_approvals = data.next_approvals + "," + user_id;
	   	    			}
	   	    		} else {
	   	    			var obj = JSON.parse(JSON.stringify(allUsers));
	   	    			data.aprobator = obj[user_id].toString();
	   	    			data.next_approvals = user_id;
	   	    		}
   	    		} else {
   	    			alert("Linia este aprobata, nu se poate modifica aprobatorul.");
   	    		}
				rows.push(data);
			 });
			 dataTable.set('recordset', rows);
		});
		
		//alocarea unui aprobator pe toate liniile
		$('#alocaAprobatorFinal').click(function(e) {
			e.preventDefault();
			var value = $('#<portlet:namespace/>aprobatorGeneralFinal').val();
			var rows = [];
			var ml = dataTable.data, msg = '', template = '';
			ml.each(function (item, i) {
				var data = item.getAttrs(['line_id', 'old_approvals', 'next_approvals', 'product_code', 'article', 'store', 'user_id', 'aprobator_final', 'aprobator']);
				if(data.aprobator != "Linie aprobata") {
	   	    		data.aprobator_final = value;
   	    		} else {
   	    			alert("Linia este aprobata, nu se poate modifica aprobatorul.");
   	    		}
				rows.push(data);
			 });
			 dataTable.set('recordset', rows);
		});
		
		dataTable.after('record:change', function (e) { 
			var rows = [];
			e.preventDefault();
			var obj = e.target.changed;
			var existingData = e.target._state.data;
			if (typeof obj.user_id != 'undefined') {
				var user_id = obj.user_id;
				var ml = dataTable.data, msg = '', template = '';
		   		 ml.each(function (item, i) {
		   	    	var data = item.getAttrs(['line_id', 'old_approvals' ,  'next_approvals', 'product_code', 'article', 'store', 'user_id', 'aprobator_final', 'aprobator']);
		   	    	if(existingData.line_id.value == data.line_id) {
		   	    		if(data.aprobator != "Linie aprobata") {
			   	    		if(data.aprobator != "" ) {
			   	    			var obj = JSON.parse(JSON.stringify(allUsers));
			   	    			data.aprobator = data.aprobator + ", " + obj[user_id].toString();
			   	    			if (data.next_approvals.charAt(data.next_approvals.length-1) == ',') {
			   	    				data.next_approvals = data.next_approvals + user_id;
			   	    			} else {
			   	    				data.next_approvals = data.next_approvals + "," + user_id;
			   	    			}
			   	    		} else {
			   	    			var obj = JSON.parse(JSON.stringify(allUsers));
			   	    			data.aprobator = obj[user_id].toString();
			   	    			data.next_approvals = user_id;
			   	    		}
		   	    		} else {
		   	    			alert("Linia este aprobata, nu se poate modifica aprobatorul.");
		   	    		}
		   	    	}
		   	    	rows.push(data);
		   		  });
		   		 dataTable.set('recordset', rows);
			}
		});
		
		$(".submit_form").click(function(e){
			var ml  = dataTable.data, msg = '', template = '';
			var json = ml.toJSON();
			var stringData = JSON.stringify(json);
			$('#<portlet:namespace/>datatable_lines').val(stringData);
			$("#<portlet:namespace/>updateApproval").submit();
		});
});
</script>

<aui:form action="<%=submitURL%>" method="post" id="updateApproval" name="updateApproval" style="display:none">
	<aui:input type="hidden" name="datatable_lines" id="datatable_lines" value="" />
</aui:form>