<%@page import="com.profluo.ecm.flows.InvoiceStages"%>
<%@page import="com.profluo.ecm.model.db.InvoiceHeader"%>
<%@page import="com.profluo.ecm.model.vo.DefCurrencyVo"%>
<%@page import="com.profluo.ecm.model.vo.DefCompaniesVo"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme"%>

<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui"%>
<%@ taglib uri="http://alloy.liferay.com/tld/aui" prefix="aui"%>
<%@ taglib uri="http://liferay.com/tld/util" prefix="liferay-util"%>

<%@page import="java.util.HashMap"%>
<%@page import="java.util.List"%>

<portlet:resourceURL id="ajaxURL" var="ajaxURL"/>

<liferay-theme:defineObjects />
<portlet:defineObjects />

<% // PAGINATION Start - env setup 
int start 	= 0; 
int count 	= 15;
int total 	= 0;
int uid 	= 0;

String datafactura 	= "";
String supplierCode = "";
String supplierName = "";
%>

<%
	//locale = renderRequest.getLocale();
	//ResourceBundle resmain = portletConfig.getResourceBundle(locale);
	// retrieve all invoices headers from DB
	List<HashMap<String,Object>> allInvoices = null;
	
	//retrieve all companies from DB
	List<HashMap<String,Object>> allCompanies = DefCompaniesVo.getInstance();

	// retrieve all currencies from DB
	List<HashMap<String,Object>> allCurrencies = DefCurrencyVo.getInstance();
	

	String pageType   	= "";
	String storeCode 	= "";
	int companyId 		= 0;
	try{
		uid = (int)user.getUserId();
	} catch (Exception e) { }
	
	try {
		if(user.getAddresses().size() != 0 && user.getAddresses().get(0).getStreet2() != null) {
			storeCode = user.getAddresses().get(0).getStreet2();
		}
	} catch (Exception e) { }

	try {
		pageType				= renderRequest.getAttribute("type").toString();
	} catch (Exception e) { }
	

	if(pageLayout.equals(InvoiceStages.PAGE_FACTURI_BLOCATE_CONTABILITATE)) {
		allInvoices = InvoiceHeader.getRejectedInvoicesForAccounting(start, count, companyId, "", 0, datafactura, supplierCode, supplierName);
		total = InvoiceHeader.getRejectedInvoicesForAccountingCount(start, count, companyId, "", 0, datafactura, supplierCode, supplierName);
	} else {
		allInvoices = InvoiceHeader.getRejectedInvoices(start, count, companyId, "", 0, datafactura, supplierCode, supplierName, (int)user.getUserId());
		total = InvoiceHeader.getRejectedInvoicesCount(start, count, companyId, "", 0, datafactura, supplierCode, supplierName, (int)user.getUserId());
	}
%>
<script type="text/javascript">
var start = "<%=start%>";
var count = "<%=count%>";
var total = "<%=total%>";
var uid	  = "<%=uid%>";
//filtre
var company_id = 0;
var moneda = "";
var number = 0;
var datafactura = "<%=datafactura%>";
var supplierCode = "<%=supplierCode%>";
var supplierName = "<%=supplierName%>";
var page_layout  = "<%=pageLayout%>";
</script>

<portlet:renderURL var="vizualizareURL" >
	<portlet:param name="mvcPath" value="/html/definitions/aprobarefactura/view.jsp"></portlet:param>
</portlet:renderURL>

<portlet:renderURL var="editareURL" >
	<portlet:param name="mvcPath" value="/html/includes/visualizations/vizualizare_factura.jsp"></portlet:param>
</portlet:renderURL>

<%--  datepicker for table header --%>
<div id="filtru_datafactura_div" style="display:none">
	<aui:input type="text" id="datafactura_header" name="datafactura_header" placeholder="Data factura" 
				value="" label="" style="width:100px; margin:0;margin-bottom:-5px">
	</aui:input>
</div>

<aui:column first="true" columnWidth="100" last="true">
	<div id="validare_div"></div>
</aui:column>

<%-- HTML for PAGINATION --%>
<%@ include file="/html/includes/pagination.jsp" %>

<script type="text/javascript">
YUI({lang: 'ro'}).use( 'aui-datatable', 'aui-datatype', 'datatable-sort', 'aui-datepicker',
	function(Y) {
		// data array
		var oldDateValue = $('#<portlet:namespace />datafactura_header').val();
		
		var remoteData = [
		   <% for (int i = 0; i < allInvoices.size(); i++) { %>
					{
						id: '<%=allInvoices.get(i).get("id")%>',
						nr: '<%=allInvoices.get(i).get("nr")%>',
						categ_capex : '<%=allInvoices.get(i).get("categ_capex")%>',
						code: '<%=allInvoices.get(i).get("code")%>',
						furnizor: '<%=allInvoices.get(i).get("furnizor")%>',
						inv_date: '<%=allInvoices.get(i).get("inv_date")%>',
						datascadenta: '<%=allInvoices.get(i).get("datascadenta")%>',
						val_cu_tva: '<%=allInvoices.get(i).get("val_cu_tva")%>',
						currency: '<%=allInvoices.get(i).get("currency")%>',
						company: '<%=allInvoices.get(i).get("company")%>',
						motiv_blocat : '<%=allInvoices.get(i).get("motiv_blocat")%>',
						supplier_fault : '<%=allInvoices.get(i).get("supplier_fault")%>',
					}
				<% if (i != allInvoices.size() - 1) {  out.print(",");} %>
			<% } %>
		];
		
		function formatCurrency(cell) {
		    format = {
		    	thousandsSeparator: ",",
		        decimalSeparator: ".",
		        decimalPlaces: noOfDecimalsToDisplay
		    };
		    return Y.DataType.Number.format(Number(cell.value), format);
		}
		// COLUMN INFO and ATTRIBUTES
		var nestedCols = [
		    { key: 'id', className: 'hiddencol'},
		    { key: 'nr', allowHTML: true,
				label: '<input name="nr_filtrare" id="nr_filtrare" placeholder="Factura nr." style="width:80px;margin-bottom:0px"></input>'},	
			{ key: 'categ_capex', label: 'Tip CAPEX'},
			{ key: 'code', allowHTML: true,
				label: '<input name="filtru_code" id="filtru_code" placeholder="Cod" style="width:80px;margin-bottom:0px"></input>'},	
			{ key: 'furnizor', allowHTML: true,
					label: '<input name="filtru_furnizor" id="filtru_furnizor" placeholder="Nume Furnizor" style="width:150px;margin-bottom:0px"></input>'},	
			{ key: 'inv_date' , allowHTML: true, label: '<div id="header_inv_date_blocked"></div>'},
			{ key: 'datascadenta' , label: 'Data scadenta'},
			{ key: 'val_cu_tva', label: 'Val cu TVA'},
			{ key: 'currency', label: 'Moneda'},
			{ key: 'company', label: '<select id="filter-company" name="filter-company" label="" style="width:100px;margin:0">' +
				'<option selected="selected" value="0">Societate</option>' +
				<% for (int i = 0; i < allCompanies.size(); i++) { %>
					<% if (allCompanies.get(i).get("status").equals("A")) {%>
						<% out.println("'<option value=\"" + allCompanies.get(i).get("id") +"\">" + allCompanies.get(i).get("name") +"</option>' + "); %>
				<% } } %>
				'</select>'},
			{ key: 'motiv_blocat', label: 'Motiv blocare'},	
			{ key: 'supplier_fault', label: 'Vina Furnizor'},	
			{ key: 'actiuni', label: 'Actiuni', allowHTML: true, 
				// gets the id of the row and creates an unique link for every row
				formatter: '<a href="#" id="row{id}" class="btn btn-primary edit" style="width:80px">Vizualizare</a>'
				}	      
		];
		
		function redirect(sel_id) {
		    var ml  = dataTable.data, msg = '', template = '';
	
		    ml.each(function (item, i) {
		        var data = item.getAttrs(['id', 'nr', 'categ_capex', 'code', 'furnizor', 'inv_date', 'datascadenta', 'val_cu_tva','currency','company', 'motiv_blocat', 'detalii_motiv']);
		        if (data.id == sel_id) {
					$('#<portlet:namespace />id').val(data.id);
					$('#<portlet:namespace />supplier_name').val(data.furnizor);
					$('#<portlet:namespace />supplier_code').val(data.code);
					$('#<portlet:namespace />toRedirect').submit();
					
		        }
		    });
		}
		
		function editInvoice(sel_id) {
		    var ml  = dataTable.data, msg = '', template = '';
	
		    ml.each(function (item, i) {
		        var data = item.getAttrs(['id', 'nr', 'categ_capex', 'code', 'furnizor', 'inv_date', 'datascadenta', 'val_cu_tva','currency','company', 'motiv_blocat', 'detalii_motiv']);
		        if (data.id == sel_id) {
					$('#<portlet:namespace />id').val(data.id);
					$('#<portlet:namespace />supplier_name').val(data.furnizor);
					$('#<portlet:namespace />toRedirect').attr("action", "<%=editareURL.toString() %>");
					$('#<portlet:namespace />toRedirect').submit();
					
		        }
		    });
		}
	
		// TABLE INIT
		var dataTable = new Y.DataTable({
		    columns: nestedCols,
		    data: remoteData,	
		    editEvent: 'click',
		    recordType: ['id', 'nr', 'categ_capex', 'code', 'furnizor', 'inv_date', 'datascadenta', 'val_cu_tva','currency','company', 'motiv_blocat', 'detalii_motiv']
		}).render('#validare_div');
		
		//dataTable.get('boundingBox').unselectable();
		//vizualizare factura
	    dataTable.delegate('click', function (e) {
	    	e.preventDefault();
	    	 // undefined to trigger the emptyCellValue
		    var target = e.target.get('id');
	    	if (page_layout == '<%=InvoiceStages.PAGE_FACTURI_BLOCATE_CONTABILITATE%>'){
	    		editInvoice(target.replace('row',''));
	    		return;
	    	}
			redirect(target.replace('row',''));
			//window.location.href=redirect_add;
		}, '.edit', dataTable);
		
	 	// PAGINATION
  		// set status of navigation buttons on page load
  		setNavigationButtonsState('<portlet:namespace />');
  		$('.navigation').click(function(e) {
  			var elementT 	= e.target;
  			var elementId 	= elementT.id;
  			var action 		= "";
  			
  			// determine which button was pressed and set the value of start accordingly
  			if (elementId.indexOf("first") > -1)	{ action = "first"; start = 0; }
  			if (elementId.indexOf("prev") > -1) 	{ action = "prev"; if (start - count <= 0) {start = 0} else { start -= count; } }
  			if (elementId.indexOf("next") > -1) 	{ action = "next"; start = parseInt(start) + parseInt(count); }
  			if (elementId.indexOf("last") > -1) 	{ action = "last"; start = parseInt(parseInt(total) / parseInt(count)) * count; if (start == total) { start -= count; } }
  			
  			makeAjaxCall(action);
  		});
  		
  		
  		// filtrare societate
  		$('#filter-company').change(function(e) {
  			var elementT 	= e.target;
  			var elementId 	= elementT.id;
  			var action 		= "filter";
  			// get selected value
  			company_id = elementT.value;
  			moneda = $('#filter-currency').val();
  			supplierCode = $('#filtru_code').val();
  			supplierName = $('#filtru_furnizor').val();
  			number = $('#nr_filtrare').val();
			datafactura = oldDateValue;
  			// reset start page 
  			start = 0;
  			// reset count
  			total = 0;
  			// make ajax call
  			makeAjaxCall(action);
  		});
  		
  		// filtrare moneda
  		$('#filter-currency').change(function(e) {
  			var elementT 	= e.target;
  			var elementId 	= elementT.id;
  			var action 		= "filter";
  			// get selected value
  			moneda = elementT.value;
  			supplierCode = $('#filtru_code').val();
  			supplierName = $('#filtru_furnizor').val();
  			number = $('#nr_filtrare').val();
			datafactura = oldDateValue;
			company_id = $('#filter-company').val();
  			// reset start page 
  			start = 0;
  			// reset count
  			total = 0;
  			// make ajax call
  			makeAjaxCall(action);
  		});

  		// filtrare dupa cod furnizor
  		$('#filtru_code').keyup(function (e) {
  		    if ($("#filtru_code").is(":focus") && (e.keyCode == 13)) {
  				var elementT 	= e.target;
  				var elementId 	= elementT.id;
  				var action 		= "filter";
  				// get selected value
  				supplierCode = elementT.value;

  	  			moneda = $('#filter-currency').val();
  	  			supplierName = $('#filtru_furnizor').val();
  	  			number = $('#nr_filtrare').val();
  				datafactura = oldDateValue;
  				company_id = $('#filter-company').val();
  				if (number == "") {
  					number = 0;
  				}
  				// reset start page 
  				start = 0;
  				// reset count
  				total = 0;
  				// make ajax call
  				makeAjaxCall(action);
  		    }
  		});
  		
  		// filtrare dupa nume furnizor
  		$('#filtru_furnizor').keyup(function (e) {
  		    if ($("#filtru_furnizor").is(":focus") && (e.keyCode == 13)) {
  				var elementT 	= e.target;
  				var elementId 	= elementT.id;
  				var action 		= "filter";
  				// get selected value
  				supplierName = elementT.value;

  	  			moneda = $('#filter-currency').val();
  	  			supplierCode = $('#filtru_code').val();
  	  			number = $('#nr_filtrare').val();
  				datafactura = oldDateValue;
  				company_id = $('#filter-company').val();
  				if (number == "") {
  					number = 0;
  				}
  				// reset start page 
  				start = 0;
  				// reset count
  				total = 0;
  				// make ajax call
  				makeAjaxCall(action);
  		    }
  		});
  		
  		// filtrare dupa nr factura
  		$('#nr_filtrare').keyup(function (e) {
  		    if ($("#nr_filtrare").is(":focus") && (e.keyCode == 13)) {
  				var elementT 	= e.target;
  				var elementId 	= elementT.id;
  				var action 		= "filter";
  				// get selected value
  				number = elementT.value;

  	  			moneda = $('#filter-currency').val();
  	  			supplierCode = $('#filtru_code').val();
  	  			supplierName = $('#filtru_furnizor').val();
  				datafactura = oldDateValue;
  				company_id = $('#filter-company').val();
  				if (number == "") {
  					number = 0;
  				}
  				// reset start page 
  				start = 0;
  				// reset count
  				total = 0;
  				// make ajax call
  				makeAjaxCall(action);
  		    }
  		});
  		
  		function makeAjaxCall(action) {
  			$.ajax({
  				type: "POST",
  				url: "<%=ajaxURL%>",
  				data: 	"<portlet:namespace />action=" + action + 
  						"&<portlet:namespace />start=" + start + 
  						"&<portlet:namespace />count=" + count +
  						"&<portlet:namespace />total=" + total +
  						"&<portlet:namespace />moneda=" + moneda +
  						"&<portlet:namespace />number=" + number +
  						"&<portlet:namespace />company_id=" + company_id +
  						"&<portlet:namespace />datafactura=" + datafactura +
  						"&<portlet:namespace />supplier_code=" + supplierCode +
  						"&<portlet:namespace />supplier_name=" + supplierName +
  						"&<portlet:namespace />uid=" + uid + 
  						"&<portlet:namespace />page_layout=" + page_layout,
  				success: function(msg) {
  					// get table data
  					if (msg != ""){
  						var jsonEntries = jQuery.parseJSON(msg);
  						total = jsonEntries.total;
  						dataTable.set('data', eval(jsonEntries.values));
  					} else {
  						dataTable.set('data', []);
  					}
  					
  					// set status of navigation buttons on page load
  					setNavigationButtonsState('<portlet:namespace />');
  				}
  			});
  		}
  		
  		// called whenever a change in date selection was performed
		function dateChanged(input) {
			console.log('date changed: ' + input.val());
			oldDateValue = input.val();
			// get selected value
			console.log(oldDateValue);
			datafactura = oldDateValue;

  			moneda = $('#filter-currency').val();
  			supplierCode = $('#filtru_code').val();
  			supplierName = $('#filtru_furnizor').val();
  			number = $('#nr_filtrare').val();
			company_id = $('#filter-company').val();
			// reset start page 
			start = 0;
			// reset count
			total = 0;
			action = "filter";
			// make ajax call
			makeAjaxCall(action);
		}
  		
  		var datepicker = new Y.DatePicker({
			trigger : '#<portlet:namespace />datafactura_header',
			mask : '%Y-%m-%d',
			popover : {
				zIndex : 1
			},
			popover: {
				zIndex : 1,
		          toolbars: {
		            header: [[
		              {
		                icon:'icon-trash',
		                label: 'Reseteaza filtru',
		                on: {
		                  click: function() {
		                	  datepicker.clearSelection();
		                  }
		                }
		              }]]
		          }
			},
			after: {
	            selectionChange: function(event) {
	            	$('#filtru_datafactura_div').html('');
	            	if (oldDateValue != $('#<portlet:namespace />datafactura_header').val()) {
	            		oldDateValue = $('#<portlet:namespace />datafactura_header').val();
	            		dateChanged($('#<portlet:namespace />datafactura_header'));
	            	}
	            	$('#filtru_datafactura_div').html('');
	            }
	        },
			calendar : {
				dateFormat : '%Y-%m-%d'
			}
		});
  		
	}
);
</script>
<aui:form method="post" action="<%=vizualizareURL.toString() %>" name="toRedirect" id="toRedirect" style="margin:0;padding:0">
	<aui:input type="hidden" id="id" name="id"/>
	<aui:input type="hidden" id="listingType" name="listingType" value = "<%=pageType %>"/>
	<aui:input type="hidden" id="storeId" name="storeId" value = "<%=storeCode %>"/>
	<aui:input type="hidden" id="supplier_name" name="supplier_name"/>
	<aui:input type="hidden" id="supplier_code" name="supplier_code"/>
	<aui:input type="hidden" id="blocked_page" name="blocked_page" value="blocked"/>
</aui:form>