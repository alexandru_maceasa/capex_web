<%@page import="com.profluo.ecm.flows.InvoiceStages"%>
<%@page import="com.profluo.ecm.controller.UserTeamIdUtils"%>
<%@page import="com.profluo.ecm.model.vo.DefCurrencyVo"%>
<%@page import="com.profluo.ecm.model.vo.DefCompaniesVo"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.List"%>
<%@page import="com.profluo.ecm.model.db.InvoiceHeader"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme"%>

<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui"%>
<%@ taglib uri="http://alloy.liferay.com/tld/aui" prefix="aui"%>
<%@ taglib uri="http://liferay.com/tld/util" prefix="liferay-util"%>
 
<liferay-theme:defineObjects />
<portlet:defineObjects />

<portlet:resourceURL id="exportAction" var="exportURL">
	<portlet:param name="action" value="exportExcel"/>
</portlet:resourceURL>


<%
// retrieve all companies from DB
//List<HashMap<String,Object>> allCompanies = DefCompaniesVo.getInstance();
String pageLayout 	= "";
try {
	pageLayout				= renderRequest.getAttribute("page-layout").toString();
} catch (Exception e) { }
%>

<liferay-ui:success message="update_ok" key="update_ok" />

<%--
<aui:fieldset>
	<aui:layout>
		<aui:column columnWidth="50" last="true" style="text-align:right">
			<aui:select id="societate" name="societate" label="">
				<aui:option selected="selected" value="0">Societate</aui:option>
				<% for (int i = 0; i < allCompanies.size(); i++) { %>
					<% if (allCompanies.get(i).get("status").equals("A")) {%>
						<aui:option value="<%=allCompanies.get(i).get(\"name\")%>"><%=allCompanies.get(i).get("name")%></aui:option>
					<% } %>
				<% } %>
			</aui:select>
		</aui:column>
	</aui:layout>
</aui:fieldset>
 --%>
 
 <% if(pageLayout.equals(InvoiceStages.PAGE_FACTURI_BLOCATE_CONTABILITATE)) { %>
<aui:layout>
	<aui:column columnWidth="100" first="true">
		<a class="btn btn-primary" href="<%=exportURL.toString()%>"> Export Excel </a>
	</aui:column>
</aui:layout>
 <% } %>
 
<aui:fieldset>
	<aui:layout>
		<aui:layout  style="margin-top:10px">
			<%@ include file="table/listing.jsp" %>
		</aui:layout>
	</aui:layout>
</aui:fieldset>
