<%@page import="java.util.HashMap"%>
<%@page import="java.util.List"%>
<%@page import="com.profluo.ecm.model.db.InvoiceHeader"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme"%>
<%@page import="com.profluo.ecm.model.vo.DefStoreVo"%>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui"%>
<%@ taglib uri="http://alloy.liferay.com/tld/aui" prefix="aui"%>
<%@ taglib uri="http://liferay.com/tld/util" prefix="liferay-util"%>

<%@ page import="java.util.ResourceBundle"%>
<%@ page import="java.util.Locale"%>

<liferay-theme:defineObjects />
<portlet:defineObjects />

<portlet:resourceURL id="ajaxURL" var="ajaxURL"/>
<portlet:actionURL name="updateStores" var="submitURL" />

<%
	locale = renderRequest.getLocale();
	ResourceBundle resmainProv = portletConfig.getResourceBundle(locale);
	
	//retrieve all inexistent suppliers
	List<HashMap<String, Object>> allUsers = InvoiceHeader.getAllUsersCG();
	
%>


<aui:layout>
	<aui:column first="true">
		<div id="controlor_gest"></div>
	</aui:column>
</aui:layout>


<script type="text/javascript">
var id = 0;
</script>

<script type="text/javascript">
$(document).ready(function(e) {
	$('#<portlet:namespace/>company').chosen();
})

var allStores = <%=InvoiceHeader.getJsonStringStores()%>;

YUI().use( 'aui-datatable', 'aui-datatype', 'datatable-sort',
	function(Y) {
		// data array
		var remoteData = [
		    <% for ( int i = 0; i < allUsers.size(); i++) { %>
			{
				id: '<%=allUsers.get(i).get("user_id")%>',
				username:'<%=allUsers.get(i).get("username")%>',
				email:'<%=allUsers.get(i).get("email")%>',
				first_name:'<%=allUsers.get(i).get("first_name")%>',
				last_name:'<%=allUsers.get(i).get("last_name")%>',
				stores:'<%=allUsers.get(i).get("stores")%>',
				id_store_concat:'<%=allUsers.get(i).get("id_store_concat")%>',
				store_id: 0,
	  			actiuni: '<%=allUsers.get(i).get("actiuni")%>'
			}
			<% if (i != allUsers.size() - 1) {  out.print(",");} %>
			<% } %>
		];

		// COLUMN INFO and ATTRIBUTES
		var nestedCols = [
		    { key: 'id', className: 'hiddencol'},
			{ key: 'username' , label : 'Username'},
			{ key: 'email', label : 'Email' },
// 			{ key: 'first_name', label : 'Prenume' },
//			{ key: 'last_name', label : 'Nume' },
			{ key: 'stores', label : 'Magazine alocate' },
			{ key: 'id_store_concat', className: 'hiddencol' },
            { key: 'store_id', label: 'Adauga magazine', editor: new Y.DropDownCellEditor({closeOnSelect: false, options: allStores, id: 'allStores', elementName: 'allStores',
 				after: {
 		            focus: function(event) {
 			            $("select[name=allStores]").chosen();
 		            }
 				}
 			}), formatter: function(o) {
	   				var obj = JSON.parse(JSON.stringify(allStores));
	   				if (typeof o.value != 'undefined') {
	   					return obj[o.value].toString();
	   				} else {
	   					return '0';
	   				}
 				}
 			},
 			{ key: 'actiuni', label: 'Actiuni', allowHTML: true, 
  				formatter: '<a href="#" id="row{id}--{stores}" class="save">Salveaza</a>' 
  			},
  			{ key : 'remove', label : 'Sterge alocari', allowHTML: true, 
          	  formatter: function(o) {
   						if (o.data.line_id != ''){
   	   						return '<a href="#" id="row' + o.data.id + '" class="deleteApprovals" ><img src="${themeDisplay.getPathThemeImages()}/delete.png" width="20"/></a>';
   						}else {
   							return '<a href="#" id="row' + o.value + '" class="deleteApprovals"><img src="${themeDisplay.getPathThemeImages()}/delete.png" width="20"/></a>';
   						}
   				}
            }
		];
		// TABLE INIT
		var dataTable = new Y.DataTable({
		    columns: nestedCols,
		    data: remoteData,	
		    editEvent: 'click',
		    recordType: ['id','username','email','first_name','last_name','stores','id_store_concat','store_id'],
		    plugins: [{
		        cfg: { highlightRange: false },
		        fn: Y.Plugin.DataTableHighlight
		    }]
		}).render('#controlor_gest');
		
		//dataTable.get('boundingBox').unselectable();
	
		function showAllInvoices (sel_id){
			var ml  = dataTable.data, msg = '', template = '';
			  	
	  		ml.each(function (item, i) {
	  			var data = item.getAttrs(['id']);					
	  			if (data.id == sel_id) {
	  				id = data.id;
	  				return;
	  			}
	  		});
	  		if ( id != 0 ){
	  			var action = "get";
	  			makeAjaxCall(action);
	  		}
	  	}
		//sterge alocari
		dataTable.delegate('click', function (e) {
   			e.preventDefault();
   			if (confirm("Sunteti sigur ca vreti sa stergeti alocarile?. Pentru a se inregistra solicitarea este obligatoriu sa actionati butonul Salveaza")){
	   			var rows = [];
	   		  	var target = e.currentTarget.get('id');
	   		  	var lineId = target.replace("row", "");
	   		 	var ml = dataTable.data, msg = '', template = '';
		   		 ml.each(function (item, i) {
		   	    	var data = item.getAttrs(['id','username','email','first_name','last_name','stores','id_store_concat','store_id']);
		   	    	if(lineId == data.id) {
		   	    		data.stores = "";
		   	    	}
		   	    	rows.push(data);
		   		  });
		   		 dataTable.set('recordset', rows);
   			}
		}, '.deleteApprovals', dataTable);
		
		dataTable.delegate('click', function (e) {
			e.preventDefault();
	    	 // undefined to trigger the emptyCellValue
		    var target = e.target.get('id');
		    alert(target.replace('row',''));
	    }, '.showStores', dataTable);
		
		dataTable.after('record:change', function (e) { 
			var rows = [];
			e.preventDefault();
			var obj = e.target.changed;
			var existingData = e.target._state.data;
			if (typeof obj.store_id != 'undefined') {
				var store_id = obj.store_id;
				var ml = dataTable.data, msg = '', template = '';
		   		 ml.each(function (item, i) {
		   	    	var data = item.getAttrs(['id','username','email','first_name','last_name','stores','id_store_concat','store_id']);
		   	    	if(existingData.id.value == data.id) {
			   	    		if(data.stores != "" ) {
			   	    			var obj = JSON.parse(JSON.stringify(allStores));
			   	    			data.stores = data.stores + ", " + obj[store_id].toString();
			   	    		} else {
			   	    			var obj = JSON.parse(JSON.stringify(allStores));
			   	    			data.stores = obj[store_id].toString();
			   	    		}
		   	    	}
		   	    	rows.push(data);
		   		  });
		   		 dataTable.set('recordset', rows);
			}
		});
		
		dataTable.delegate('click', function (e) {
  		    // undefined to trigger the emptyCellValue
  		    e.preventDefault();
  		    var elem = e.target.get('id');
  		 	var userid = elem.replace('row',"");
  			$('#<portlet:namespace />id').val(userid);
  			$('#<portlet:namespace />updateStores').submit();
   		}, '.save', dataTable);
	});
</script>

<aui:form action="<%=submitURL%>" method="post" id="updateStores" name="updateStores" style="display:none">
	<aui:input type="hidden" name="id" id="id" value="" />
</aui:form>
