<%@page import="com.profluo.ecm.model.vo.DefCompaniesVo"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.List"%>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui"%>
<%@ taglib uri="http://alloy.liferay.com/tld/aui" prefix="aui"%>
<%@ taglib uri="http://liferay.com/tld/util" prefix="liferay-util"%>

<%@ page import="java.util.ResourceBundle"%>
<%@ page import="java.util.Locale"%>

<liferay-theme:defineObjects />
<portlet:defineObjects />


<%
	locale = renderRequest.getLocale();
	ResourceBundle resmain = portletConfig.getResourceBundle(locale);
	//retrieve all companies from DB
	List<HashMap<String,Object>> allCompanies = DefCompaniesVo.getInstance();
%>


<%-- datepicker for table header --%>
<div id="filtru_data_factura" style="display:none">
<aui:input type="text" id="inv_date_header" name="inv_date_header" value="" placeholder="Data factura" label="" style="width:100px;margin-bottom:0px">
</aui:input>
<script type="text/javascript">
	YUI().use('aui-datepicker', function(Y) {
		new Y.DatePicker({
			trigger : '#<portlet:namespace />inv_date_header',
			mask : '%d/%m/%Y',
			popover : {
				zIndex : 1
			},
			calendar : {
				dateFormat : '%d/%m/%Y'
			}
		});
	});
</script>
</div>

<%-- second datepicker for table header --%>
<div id="filtru_data_scadenta" style="display:none">
<aui:input type="text" id="datascadenta_header" name="datascadenta_header" value="" placeholder="Data scadenta" label="" style="width:100px;margin-bottom:0px">
</aui:input>
<script type="text/javascript">
	YUI().use('aui-datepicker', function(Y) {
		new Y.DatePicker({
			trigger : '#<portlet:namespace />datascadenta_header',
			mask : '%d/%m/%Y',
			popover : {
				zIndex : 1
			},
			calendar : {
				dateFormat : '%d/%m/%Y'
			}
		});
	});
</script>
</div>
<%-- third datepicker for table header --%>

<div id="data_inregistrare_div" style="display:none">
<aui:input type="text" id="datascadenta_header" name="datascadenta_header" value="" placeholder="Data inregistrare" label="" style="width:110px;margin-bottom:0px">
</aui:input>
<script type="text/javascript">
	YUI().use('aui-datepicker', function(Y) {
		new Y.DatePicker({
			trigger : '#<portlet:namespace />datascadenta_header',
			mask : '%d/%m/%Y',
			popover : {
				zIndex : 1
			},
			calendar : {
				dateFormat : '%d/%m/%Y'
			}
		});
	});
</script>
</div>
<aui:layout>
	<aui:column columnWidth="50" last="true" style="text-align:right">
		<aui:select id="societate" name="societate" label="">
			<aui:option selected="selected" value="0">Societate</aui:option>
			<% for (int i = 0; i < allCompanies.size(); i++) { %>
				<% if (allCompanies.get(i).get("status").equals("A")) {%>
					<aui:option value="<%=allCompanies.get(i).get(\"name\")%>"><%=allCompanies.get(i).get("name")%></aui:option>
				<% } %>
			<% } %>
		</aui:select>
	</aui:column>
</aui:layout>
<aui:layout>
	<aui:layout  style="margin-top:10px">
		<%@ include file="tabel_asteapta_validare/asteapta_validare.jsp" %>
	</aui:layout>
</aui:layout>