<%@page import="com.profluo.ecm.model.vo.DefCurrencyVo"%>
<%@page import="com.profluo.ecm.model.vo.DefCompaniesVo"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme"%>

<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui"%>
<%@ taglib uri="http://alloy.liferay.com/tld/aui" prefix="aui"%>
<%@ taglib uri="http://liferay.com/tld/util" prefix="liferay-util"%>

<%@page import="java.util.HashMap"%>
<%@page import="java.util.List"%>

<%@ page import="java.util.ResourceBundle"%>
<%@ page import="java.util.Locale"%>

<liferay-theme:defineObjects />
<portlet:defineObjects />

<portlet:renderURL var="validateURL" >
	<portlet:param name="mvcPath" value="/html/asteaptavalidare/view.jsp"></portlet:param>
</portlet:renderURL>

<%
	// retrieve all currencies from DB
	List<HashMap<String,Object>> allCurrencies = DefCurrencyVo.getInstance();

%>
<%--redirect to page add_request.jsp --%>

<aui:column first="true" columnWidth="100" last="true">
	<div id="asteapta_validare_div"></div>
</aui:column>

<script type="text/javascript">

YUI().use( 'aui-datatable', 'aui-datatype', 'datatable-sort',
	function(Y) {
		// data array
		var remoteData = [];
		
		function formatCurrency(cell) {
		    format = {
		    	thousandsSeparator: ",",
		        decimalSeparator: ".",
		        decimalPlaces: noOfDecimalsToDisplay
		    };
		    return Y.DataType.Number.format(Number(cell.value), format);
		}

		// COLUMN INFO and ATTRIBUTES
		var nestedCols = [
		    { key: 'id', className: 'hiddencol'},
		    { key: 'nr', allowHTML: true,
				label: '<input name="nr_filtrare" id="nr_filtrare" placeholder="Factura nr." style="width:80px;margin-bottom:0px"></input>'},	
			{ key: 'data_inregistrare' , allowHTML: true, label: '<div id="header_data_inreg"></div>'},
			{ key: 'cui', label: 'Cod Furnizor'},		
			{ key: 'furnizor', allowHTML: true,
				label: '<input name="furnizor_filtrare" id="furnizor_filtrare" placeholder="Nume Furnizor" style="width:100px;margin-bottom:0px"></input>'},	
			{ key: 'inv_date' , allowHTML: true, label: '<div id="header_inv_date"></div>'},
			{ key: 'datascadenta' , allowHTML: true, label: '<div id="header_datascadenta"></div>'},
			{ key: 'val_cu_tva', label: 'Val cu TVA', formatter: formatCurrency, className: 'currencyCol'},
			{ key: 'currency', label: '<select id="filter-currency" name="filter-currency" label="" style="width:100px;margin:0">' +
				'<option selected="selected" value="">Moneda</option>' +
				<% for (int i = 0; i < allCurrencies.size(); i++) { %>
				<% if (allCurrencies.get(i).get("status").equals("A")) {%>
				<% out.println("'<option value=\"" + allCurrencies.get(i).get("ref") +"\">" + allCurrencies.get(i).get("ref") +"</option>' + "); %>
				<% } } %>
				'</select>'},
			{ key: 'company', label: '<select id="filter-company" name="filter-company" label="" style="width:100px;margin:0">' +
				'<option selected="selected" value="">Societate</option>' +
				<% for (int i = 0; i < allCompanies.size(); i++) { %>
				<% if (allCompanies.get(i).get("status").equals("A")) {%>
				<% out.println("'<option value=\"" + allCompanies.get(i).get("name") +"\">" + allCompanies.get(i).get("name") +"</option>' + "); %>
				<% } } %>
				'</select>'},
			{ key: 'actiuni', label: 'Actiuni', allowHTML: true, 
				// gets the id of the row and creates an unique link for every row
				formatter: '<a href="#" id="row{id}" class="goTo">Actualizare informatii</a>' }	      
		];
		
		function redirect(sel_id) {
		    var ml  = dataTable.data, msg = '', template = '';
	
		    ml.each(function (item, i) {
		        var data = item.getAttrs(['id' ,'data_inregistrare','nr', 'cui', 'furnizor', 'inv_date', 'datascadenta', 'val_cu_tva','currency','company']);
		        if (data.id == sel_id) {
					$('#<portlet:namespace />id').val(data.id);
					$('#<portlet:namespace />data_inregistrare').val(data.data_inregistrare);
					$('#<portlet:namespace />nr').val(data.nr);
					$('#<portlet:namespace />cui').val(data.cui);
					$('#<portlet:namespace />furnizor').val(data.furnizor);
					$('#<portlet:namespace />inv_date').val(data.inv_date);
					$('#<portlet:namespace />datascadenta').val(data.datascadenta);
					$('#<portlet:namespace />val_cu_tva').val(data.val_cu_tva);
					$('#<portlet:namespace />currency').val(data.currency);
					$('#<portlet:namespace />company').val(data.company);
					
					$('#<portlet:namespace />toRedirect').submit();
		        }
		    });
		}
		// TABLE INIT
		var dataTable = new Y.DataTable({
		    columns: nestedCols,
		    data: remoteData,	
		    editEvent: 'click',
		    recordType: ['id' ,'data_inregistrare','nr', 'cui', 'furnizor', 'inv_date', 'datascadenta', 'val_cu_tva','currency','company']
		}).render('#asteapta_validare_div');
		
		//dataTable.get('boundingBox').unselectable();

	    dataTable.delegate('click', function (e) {
	    	 // undefined to trigger the emptyCellValue
		    var target = e.target.get('id');
			redirect(target.replace('row',''));
			//window.location.href=redirect_add;
		}, '.goTo', dataTable);
	    
}
);
</script>
<aui:form method="post" action="<%=validateURL.toString() %>" name="toRedirect" id="toRedirect" style="margin:0;padding:0">
	<aui:input type="hidden" id="id" name="id"/>
	<aui:input type="hidden" id="data_inregistrare" name="data_inregistrare"/>
	<aui:input type="hidden" id="nr" name="nr"/>
	<aui:input type="hidden" id="cui" name="cui"/>
	<aui:input type="hidden" id="furnizor" name="furnizor"/>
	<aui:input type="hidden" id="inv_date" name="inv_date"/>
	<aui:input type="hidden" id="datascadenta" name="datascadenta"/>
	<aui:input type="hidden" id="val_cu_tva" name="val_cu_tva"/>
	<aui:input type="hidden" id="currency" name="currency"/> 
	<aui:input type="hidden" id="company" name="company"/>
</aui:form>