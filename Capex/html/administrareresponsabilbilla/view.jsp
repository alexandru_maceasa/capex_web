<%@page import="com.profluo.ecm.model.db.InvoiceApprovals"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.profluo.ecm.controller.UserTeamIdUtils"%>
<%@page import="com.profluo.ecm.model.db.UsersInformations"%>
<%@page import="com.profluo.ec.carrefour.htmlutils.DropDownUtils"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme" %>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>
<%@ taglib uri="http://alloy.liferay.com/tld/aui" prefix="aui" %>
<%@ taglib uri="http://liferay.com/tld/util" prefix="liferay-util" %>

<liferay-theme:defineObjects />
<portlet:defineObjects />

<portlet:actionURL name="salveazaResponsabil" var="submitURL" />
<% 
int userId = 0;
try {
	userId = Integer.parseInt(request.getAttribute("userId").toString());
} catch (Exception e) {}
%>

<aui:form action="<%=submitURL %>" id="submitForm" name="submitForm" method="post">
	<aui:fieldset>
		<legend>Modificare responsabil Billa</legend>
		<aui:layout>
			<aui:column columnWidth="10" first="true">
				<label for="<portlet:namespace/>id_user">Utilizator</label>
			</aui:column>
			<aui:column columnWidth="35">
				<aui:select id="id_user" name="id_user" label="" required="true">
					<%=DropDownUtils.generateSimpleDropdown(InvoiceApprovals.getAllUsers(), "id", "fullname", "0", "---------------") %>
				</aui:select>
			</aui:column>
			<aui:column columnWidth="5" style="margin-top:-5px;">
				<aui:button  value="Salveaza" id="salveaza" cssClass="btn btn-primary"/>
			</aui:column>
		</aui:layout>
	</aui:fieldset>
</aui:form>

<script>
var userId = <%=userId%>;

$(document).ready(function() {
	if(userId != 0) {
		$('#<portlet:namespace/>id_user').val(userId);
	}
	$('#<portlet:namespace/>id_user').chosen();
});

$('#salveaza').click( function (e) {
	$('#<portlet:namespace/>submitForm').append('<input type="submit" name="submit-button" id="submit-button"></input>');
	$('#submit-button').click();
	setTimeout(function() { $('#submit-button').remove(); }, 100);
});

</script>