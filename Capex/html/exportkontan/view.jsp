<%@page import="com.profluo.ecm.export.ExportInvoiceToKontan"%>
<%@taglib uri="http://java.sun.com/portlet_2_0" 	prefix="portlet"%>
<%@taglib uri="http://liferay.com/tld/theme" 		prefix="liferay-theme"%>
<%@taglib uri="http://liferay.com/tld/ui" 			prefix="liferay-ui"%>
<%@taglib uri="http://alloy.liferay.com/tld/aui" 	prefix="aui"%>
<%@taglib uri="http://liferay.com/tld/util" 		prefix="liferay-util"%>

<%@page import="java.util.HashMap"%>
<%@page import="java.util.List"%>

<liferay-theme:defineObjects />
<portlet:defineObjects />

<portlet:resourceURL id="ajaxURL" var="ajaxURL"/>

<portlet:actionURL name="generateKim" var="submitURL" />


<aui:form id="exportForm"  action="<%=submitURL%>">
	<aui:layout>
<%-- 		<aui:column first="true" columnWidth="4"> --%>
<%-- 				<label for="<portlet:namespace/>luna">Luna:</label> --%>
<%-- 		</aui:column> --%>
<%-- 		<aui:column columnWidth="10"  style="width:80px"> --%>
<%-- 			<aui:select name= "" id="month" label=""  style="width:80px"> --%>
<%-- 				<aui:option selected="selected" value =""/> --%>
<%-- 				<% for (int i = 0 ; i < 12; i++){ %> --%>
<%-- 					<aui:option value="<%= i+1 %>"> <%= i+1 %> </aui:option> --%>
<%-- 				<%} %> --%>
<%-- 			</aui:select> --%>
<%-- 		</aui:column> --%>
	
<%-- 		<aui:column columnWidth="4"> --%>
<%-- 				<label for="<portlet:namespace/>an">An:</label> --%>
<%-- 		</aui:column> --%>
<%-- 		<aui:column columnWidth="20"  style="width:120px"> --%>
<%-- 			<aui:input name="" id="year" label=""  style="width:80px"></aui:input> --%>
<%-- 		</aui:column> --%>
<%-- 		<aui:column> --%>
<%--  			<a class="btn btn-primary" onclick="checkInput('<portlet:namespace />')"> Generare KIM </a> 
<%-- 			<aui:button value=" Generare KIM" cssClass="btn btn-primary" style="margin:0px" type="submit"  onclick="checkInput('<portlet:namespace />')"/> --%>
<%-- 		</aui:column> --%>
		
	</aui:layout>
	
<%-- 	<aui:layout> --%>
<%-- 		<aui:column style="display:none"> --%>
<%-- 			<aui:button type="submit" value="submit" id="exportExcel" name="exportExcel"/> --%>
<%-- 			<aui:button value=" Generare KIM" cssClass="btn btn-primary" style="margin:0px" type="submit"/> --%>
<%-- 		</aui:column> --%>
<%-- 	</aui:layout> --%>
	
</aui:form>


<div id="facturi_export"></div>


<%
int start = 0; 
int count = 10;
int total = ExportInvoiceToKontan.getInvoiceTotal();
%>

<script type="text/javascript">
var start = <%=start%>
var count = <%=count%>
var total = <%=total%>

var supplier = "";
var invoiceNo = "";
</script>

<%
	List<HashMap<String,Object>> allInvLines = ExportInvoiceToKontan.getAllInfoWithLimit(start, count, "", "");
%>


<%-- HTML for PAGINATION --%>
<%@ include file="/html/includes/pagination.jsp" %>

<script type="text/javascript">

function checkInput(portletId){
	var luna = $('#' + portletId + 'month').val();
	var an = $('#' + portletId + 'year').val();
	
	if ( !luna ){
		alert("Luna nu a fost selectata.");
		return;
	} else if ( !an ){
		alert("Nu ati introdus anul.");
		return;
	} else if ( an < 2005 && an > new Date().getFullYear() ){
		alert("Anul introdus nu corespunde");
		return;
	} 
	
	//refresh la pagina cand se apasa butonul de generare
// 	$('#'+portletId+'exportKim').click();
// 	location.reload();
}

YUI({ lang: 'ro' }).use('aui-datatable','aui-datatype','datatable-sort', 'aui-datepicker', 'aui-base', 'liferay-portlet-url', 'aui-node',
		function(Y){
		var remoteData = [
	      			<% for (int i = 0; i < allInvLines.size(); i++) { %>
	      				{
	      					inv_number: '<%=allInvLines.get(i).get("inv_number")%>',
	      					data_fact:	'<%=allInvLines.get(i).get("data_fact")%>', 
	      					company :	'<%=allInvLines.get(i).get("company")%>',
	      					furnizor: 	'<%=allInvLines.get(i).get("furnizor")%>',
	      					total_tva:	'<%=allInvLines.get(i).get("total_tva")%>',
	      					currency: 	'<%=allInvLines.get(i).get("currency")%>',
	      					status:		'<%=allInvLines.get(i).get("status")%>'
	      				}
	      				<% if (i != (allInvLines.size() - 1)) { out.print(","); } %>
	      			<% } %>
	      		];
		var nestedColumns = [		
		         			{key: 'inv_number', label: '<input id="inv_number" name="inv_number" placeholder="Numar factura" style="width:100px"', allowHTML:true,resizable:true}, 
		         			{key: 'data_fact', label:'Data factura'},
		         	        {key: 'company', label:'Societate'},
		         	        {key: 'furnizor', label: '<input id="furnizor" name="furnizor" placeholder="Furnizor" style="width:200px"/>', allowHTML:true,resizable:true},
		         	        {key: 'total_tva',label:'Total cu TVA'},
		         	        {key: 'currency', label :'Moneda'},
		         	        {key: 'status', label:'Status'}
		         	     
		         		];
		var dataTableIV = new Y.DataTable({
			columns:nestedColumns,
			data:remoteData,
			editEvent:'click'
		}).render('#facturi_export');
		
		
		// PAGINATION
		// set status of navigation buttons on page load
		setNavigationButtonsState('<portlet:namespace />');
		$('.navigation').click(function(e) {
			var elementT 	= e.target;
			var elementId 	= elementT.id;
			var action 		= "";
			
			// determine which button was pressed and set the value of start accordingly
			if (elementId.indexOf("first") > -1)	{ action = "first"; start = 0; }
			if (elementId.indexOf("prev") > -1) 	{ action = "prev"; if (start - count <= 0) {start = 0} else { start -= count; } }
			if (elementId.indexOf("next") > -1) 	{ action = "next"; start = parseInt(start) + parseInt(count); }
			if (elementId.indexOf("last") > -1) 	{ action = "last"; start = parseInt(parseInt(total) / parseInt(count)) * count; if (start == total) { start -= count; } }
			
			makeAjaxCall(action);
		});
		function makeAjaxCall(action) {
			$.ajax({
				type: "POST",
				url: "<%=ajaxURL%>",
				data: 	"<portlet:namespace />action=" + action + 
						"&<portlet:namespace />start=" + start + 
						"&<portlet:namespace />count=" + count +
						"&<portlet:namespace />total=" + total +
						"&<portlet:namespace />InvoiceNo=" + invoiceNo +
						"&<portlet:namespace />supplier=" + supplier,
				success: function(msg) {
					// get table data
					if (msg != ""){
						var jsonEntries = jQuery.parseJSON(msg);
						total = jsonEntries.total;
						dataTableIV.set('data', eval(jsonEntries.values));
					} else {
						dataTableIV.set('data', []);
					}
					
					// set status of navigation buttons on page load
					setNavigationButtonsState('<portlet:namespace />');
				}
			});
		}
		// PAGINATION
		
		
 		$('#inv_number').keyup(function (e) {
 		    if ($("#inv_number").is(":focus") && (e.keyCode == 13)) {
 				var action 		= "filter";
 				invoiceNo = e.target.value;
 				console.log(invoiceNo);
 				supplier = $('#furnizor').val();
 				console.log(supplier);
 				// reset start page 
 				start = 0;
 				// reset count
 				total = 0;
 				// make ajax call
 				makeAjaxCall(action);				
 		    }
 		});
		
 		$('#furnizor').keyup(function (e) {
 		    if ($("#furnizor").is(":focus") && (e.keyCode == 13)) {
 				var action 		= "filter";
 				supplier = e.target.value;
 				invoiceNo = $('#inv_number').val();
 				// reset start page 
 				start = 0;
 				// reset count
 				total = 0;
 				// make ajax call
 				makeAjaxCall(action);				
 		    }
 		});
		

});

</script>
