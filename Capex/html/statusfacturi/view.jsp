
<%@page import="com.profluo.ec.carrefour.portlet.StatusFacturi"%>
<%@page import="com.profluo.ecm.model.vo.StatusFacturiBD"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme"%>

<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui"%>
<%@ taglib uri="http://alloy.liferay.com/tld/aui" prefix="aui"%>
<%@ taglib uri="http://liferay.com/tld/util" prefix="liferay-util"%>

<%@page import="java.util.HashMap"%>
<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>


<liferay-theme:defineObjects />
<portlet:defineObjects />

<portlet:resourceURL id="ajaxURL" var="ajaxURL"/>

<portlet:resourceURL id="exportAction" var="exportURL">
	<portlet:param name="action" value="exportExcel"/>
</portlet:resourceURL>

<portlet:renderURL var="viewInvoiceURL" >
	<portlet:param name="mvcPath" value="/html/includes/visualizations/vizualizare_factura.jsp"></portlet:param>
</portlet:renderURL>

<portlet:resourceURL id="exportAction" var="exportUsersURL">
	<portlet:param name="action" value="exportUsersExcel"/>
</portlet:resourceURL>

<portlet:actionURL name="importMissingDocs" var="importMissingDocs" />

<portlet:actionURL name="importAllInvoices" var="importAllInvoices" />

<%
	int count = 15;
	int start = 0;
	int total = 0;
	List<HashMap<String,Object>> allInvoicesWithoutApprovals = StatusFacturiBD.getAllInvoices(start, count, "", "", null, null, null, null, null);
	total = StatusFacturiBD.getAllInvoicesCount("", "");
	
	HashMap<String,Object> allApprovals = StatusFacturi.getAllApprovals(allInvoicesWithoutApprovals);
	
	List<HashMap<String,Object>> allInvoices = new ArrayList<HashMap<String,Object>>();
	
	//construire lista de facturi cu aprobatori
	for(int i = 0 ; i < allInvoicesWithoutApprovals.size(); i++){
		HashMap<String,Object> hash = allInvoicesWithoutApprovals.get(i);
		hash.put("aprobator", allApprovals.get(String.valueOf(i)));
		allInvoices.add(hash);
	}
%>

<aui:layout>
	<aui:column>
		<aui:form method="post" action="<%=exportURL.toString() %>" name="exportExcel" id="exportExcel">
		    <aui:column>
					<label> Data inregistrare de la:</label>
				<aui:input type="date" name="dataDeLa" id="dataDeLa" max="2030-01-01" label=""></aui:input>
		    </aui:column>
		
		    <aui:column>
						<label> Data inregistrare pana la:  </label>
				<aui:input type="date" name="dataPanaLa" id="dataPanaLa" min="2015-01-01" label="">	</aui:input>		
			</aui:column>
			
			<aui:column>
				<div id="DenumireMagazin">
					<label>Magazin:</label>
					<aui:input type="text"  name="DenumireMagazin"  id="DenumireMagazin" placeholder="Cauta"  style="margin-bottom: 10px;" label=""></aui:input>
				</div>
			</aui:column>
			
			<aui:column>
				<div id="DenumireFurnizor">
					<label>Furnizor:</label>
					<aui:input type="text"  name="DenumireFurnizor"  id="DenumireFurnizor" placeholder="Cauta"  style="margin-bottom: 10px;" label=""></aui:input>
				</div>
			</aui:column>	
			
			<aui:column>
				<label>Aprobator:</label>     
				<aui:select name="statusFactura" id="statusFactura" label="">  
			    <aui:option value="">...</aui:option>
			    <aui:option value="-1">Factura Eronata</aui:option>
			    <aui:option value="3">Factura in curs de aprobare</aui:option>
			    <aui:option value="2">Factura cu lipsa documente justificative</aui:option>
			    <aui:option value="10&2">Factura aprobata si exportata in Kontan</aui:option>
			    <aui:option value="10%1">Factura aprobata si exportata in Optimal</aui:option>
			    <aui:option value="10&2&1">Factura aprobata si exportata in Kontan si in Optimal</aui:option>
			    <aui:option value="10">Factura aprobata si neexportata in Kontan si in Optimal</aui:option>
			    <aui:option value="3&" > Factura in curs de aprobare, fara validator setat</aui:option>
			  </aui:select>               
			</aui:column>

			<aui:column>
				<a class="btn btn-primary" id="exportExcelBtn" style="margin-top: 24% !important"> Export Excel </a>
			</aui:column>
			<aui:column>
				<a class="btn btn-primary" style="margin-top: 24% !important" href="<%=exportUsersURL.toString()%>"> Export Users </a>
			</aui:column>
		</aui:form>
	</aui:column>
	
	
<%-- 	<% if(user.getScreenName().contains("test")){ %> --%>
<%-- 		<aui:column> --%>
<%-- 			<a class="btn btn-primary" href="<%=importMissingDocs.toString()%>"> Import Missing docs </a> --%>
<%-- 		</aui:column> --%>
<%-- 		<aui:column> --%>
<%-- 			<a class="btn btn-primary" href="<%=importAllInvoices.toString()%>"> Import Invoices </a> --%>
<%-- 		</aui:column> --%>
<%-- 	<% } %> --%>
	<aui:column first="true" columnWidth="100">
		<div id="invoices_status"></div>
	</aui:column>
</aui:layout>

<%-- HTML for PAGINATION --%>
<%@ include file="/html/includes/pagination.jsp" %>

<script type="text/javascript">

var start = "<%=start%>";
var count = "<%=count%>";
var total = "<%=total%>";
var allApprovals = "<%=allApprovals%>";
var factNo = "";
var supplier = "";

YUI().use( 'aui-datatable', 'aui-datatype', 'datatable-sort',
	function(Y) {
		var remoteData = [
		                  <% for(int i = 0; i< allInvoices.size(); i++) { %>
		                  	{
		                  	  inv_id:			'<%=allInvoices.get(i).get("inv_id")%>',
		                	  nrFactura: 		'<%=allInvoices.get(i).get("nrFactura")%>',
		                	  dataFactura: 		'<%=allInvoices.get(i).get("dataFactura")%>',
		                	  dataInregistrare:	'<%=allInvoices.get(i).get("dataInregistrare")%>',
		                	  magazin_name: 	'<%=allInvoices.get(i).get("magazin_name")%>',
		                	  furnizor:			'<%=allInvoices.get(i).get("furnizor")%>',
		                	  totalFaraTva:		'<%=allInvoices.get(i).get("totalFaraTva")%>',
		                	  totalCuTva:		'<%=allInvoices.get(i).get("totalCuTva")%>',
		                	  aprobator:		'<%=allInvoices.get(i).get("aprobator")%>'
	                		}
		                  <% if (i != (allInvoices.size() - 1)) { out.print(","); } %>
		                  <% } %>
		                  ];
		var nestedCols = [
						  { key: 'inv_id', label:"ID intern"},
		                  { key : 'nrFactura', label : '<input type="text" id="filtru_facturi" placeholder="Nr. factura" style="width:50%;margin:0;"/>'},
		                  { key : 'dataFactura', label : 'Data factura'},
		                  { key : 'dataInregistrare', label : 'Data inregistrare'},
		                  { key : 'magazin_name', label : 'Magazin'},
		                  { key : 'furnizor', label : '<input type="text" id="filtru_furnizor" placeholder="Furnizor" style="width:50%;margin:0;"/>'},
		                  { key : 'totalFaraTva', label : 'Total fara Tva'},
		                  { key : 'totalCuTva', label : 'Total cu Tva'},
		                  { key : 'aprobator', label : 'Aprobator'},
		                  { key: 'detalii', label: 'Detalii', allowHTML: true, 
		                	  formatter: '<a href="#" id="row{inv_id}" class="editrow">Vizualizare</a>' 
		                  }
		                  ];
		
		var dataTable = new Y.DataTable({
		    columns: nestedCols,
		    data: remoteData,	
		    editEvent: 'click',
		    recordType: ['nrFactura' ,'dataFactura','dataInregistrare', 'magazin_name', 'furnizor', 'aprobator']
		}).render('#invoices_status');
	
		// PAGINATION
  		// set status of navigation buttons on page load
  		setNavigationButtonsState('<portlet:namespace />');
  		$('.navigation').click(function(e) {
  			var elementT 	= e.target;
  			var elementId 	= elementT.id;
  			var action 		= "";
  			
  			// determine which button was pressed and set the value of start accordingly
  			if (elementId.indexOf("first") > -1)	{ action = "first"; start = 0; }
  			if (elementId.indexOf("prev") > -1) 	{ action = "prev"; if (start - count <= 0) {start = 0} else { start -= count; } }
  			if (elementId.indexOf("next") > -1) 	{ action = "next"; start = parseInt(start) + parseInt(count); }
  			if (elementId.indexOf("last") > -1) 	{ action = "last"; start = parseInt(parseInt(total) / parseInt(count)) * count; if (start == total) { start -= count; } }
  			
  			makeAjaxCall(action, factNo, supplier);
  		});
  		
  		dataTable.delegate('click', function (e) {
  		    // undefined to trigger the emptyCellValue
  		    e.preventDefault();
  		    var elem = e.target.get('id');
  		    var invoiceId = elem.replace('row',"");
  		  	$('#<portlet:namespace />id').val(invoiceId);
  		  	$('#<portlet:namespace />viewInvoice').submit();
  		}, '.editrow', dataTable);
  		
  		//filtru dupa numarul de factura
  		$('#filtru_facturi').change(function(e){
			var action 		= "filter";
			factNo = $('#filtru_facturi').val();
			supplier = $('#filtru_furnizor').val();
			start = 0;
			makeAjaxCall(action, factNo, supplier);
  		});
  		
  		//filtru dupa numarul de furnizor
  		$('#filtru_furnizor').change(function(e){
			var action 		= "filter";
			factNo = $('#filtru_facturi').val();
			supplier = $('#filtru_furnizor').val();
			start = 0;
			makeAjaxCall(action, factNo, supplier);
			
  		});
  		
  		$('#exportExcelBtn').click(function(e) {
  			$('#<portlet:namespace />exportExcel').submit();
  		});
  		
  		function makeAjaxCall(action, factNo, supplier) {
  			$.ajax({
  				type: "POST",
  				url: "<%=ajaxURL%>",
  				data: 	"<portlet:namespace />action=" + action + 
  						"&<portlet:namespace />start=" + start + 
  						"&<portlet:namespace />count=" + count +
  						"&<portlet:namespace />total=" + total + 
  						"&<portlet:namespace />factNo=" + factNo + 
  						"&<portlet:namespace />supplier=" + supplier,
  				success: function(msg) {
  					// get table data
  					if (msg != ""){
  						var jsonEntries = jQuery.parseJSON(msg);
  						total = jsonEntries.total;
  						dataTable.set('data', eval(jsonEntries.values));
  					} else {
  						dataTable.set('data', []);
  					}
  					
  					// set status of navigation buttons on page load
  					setNavigationButtonsState('<portlet:namespace />');
  				}
  			});
  		}
		
});
</script>

<aui:form method="post" action="<%=viewInvoiceURL.toString() %>" name="viewInvoice" id="viewInvoice" style="margin:0;padding:0">
	<aui:input type="hidden" id="id" name="id"/>
</aui:form>