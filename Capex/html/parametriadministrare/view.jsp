<%@page import="com.profluo.ecm.model.db.Administrare"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme"%>

<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui"%>
<%@ taglib uri="http://alloy.liferay.com/tld/aui" prefix="aui"%>
<%@ taglib uri="http://liferay.com/tld/util" prefix="liferay-util"%>

<%@page import="java.util.HashMap"%>
<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>

<liferay-theme:defineObjects />
<portlet:defineObjects />

<portlet:resourceURL id="ajaxURL" var="ajaxURL"/>
<aui:layout>
<aui:column first="true" columnWidth="100">
		<div id="administrare"></div>
	</aui:column>
</aui:layout>

<%-- HTML for PAGINATION --%>
<%@ include file="/html/includes/pagination.jsp" %>

<%
	int count = 15;
	int start = 0;
	int total = Administrare.getAllRulesCount();
	List<HashMap<String,Object>> allRules = Administrare.getAllRules();
%>

<script type="text/javascript">
	var start = "<%=start%>";
	var count = "<%=count%>";
	var total = "<%=total%>";
	
	YUI().use( 'aui-datatable', 'aui-datatype', 'datatable-sort',
			function(Y) {
		
				/** GENERIC SAVE ON ENTER **/
				function saveOnEnter(e) {
					if (e.domEvent.charCode == 13) {
						if (!e.target.validator.hasErrors()) {
							e.target.fire('save', {
				                newVal: e.target.getValue(),
				                prevVal: e.target.get('value')
				            });
						}
					}
				}
		
				var remoteData = [
				                  <% for(int i = 0; i< allRules.size(); i++) { %>
				                  	{
				                  		code:				'<%=allRules.get(i).get("code")%>',
				                  		category: 			'<%=allRules.get(i).get("category")%>',
				                  		description: 		'<%=allRules.get(i).get("description")%>',
				                  		actiuni:			'<%=allRules.get(i).get("category")%>'
			                		}
				                  <% if (i != (allRules.size() - 1)) { out.print(","); } %>
				                  <% } %>
				                  ];
				var nestedCols = [
								  { key: 'code', 	 	label : 'Valoare', editor: new Y.TextCellEditor({on : {keyup: saveOnEnter}, showToolbar: false})},
								  { key: 'category', 	label : 'Categorie'},
								  { key: 'description', label : 'Descriere'},
								  { key: 'actiuni', label : 'Actiuni', allowHTML:true,
					                	 formatter: function (o){ return "<button id='salveaza" + o.value + "' class='buton'>Salveaza</button>" } 
					               }
				                  ];
				var dataTable = new Y.DataTable({
				    columns: nestedCols,
				    data: remoteData,	
				    editEvent: 'click',
				    recordType: ['code' ,'category', 'description', 'actiuni']
				}).render('#administrare');
				
				// PAGINATION
		  		// set status of navigation buttons on page load
		  		setNavigationButtonsState('<portlet:namespace />');
		  		$('.navigation').click(function(e) {
		  			var elementT 	= e.target;
		  			var elementId 	= elementT.id;
		  			var action 		= "";
		  			
		  			// determine which button was pressed and set the value of start accordingly
		  			if (elementId.indexOf("first") > -1)	{ action = "first"; start = 0; }
		  			if (elementId.indexOf("prev") > -1) 	{ action = "prev"; if (start - count <= 0) {start = 0} else { start -= count; } }
		  			if (elementId.indexOf("next") > -1) 	{ action = "next"; start = parseInt(start) + parseInt(count); }
		  			if (elementId.indexOf("last") > -1) 	{ action = "last"; start = parseInt(parseInt(total) / parseInt(count)) * count; if (start == total) { start -= count; } }
		  			
		  			makeAjaxCall(action, "", "");
		  		});
		  		
		  		function makeAjaxCall(action, category, code) {
		  			$.ajax({
		  				type: "POST",
		  				url: "<%=ajaxURL%>",
		  				data: 	"<portlet:namespace />action=" + action + 
		  						"&<portlet:namespace />start=" + start + 
		  						"&<portlet:namespace />count=" + count +
		  						"&<portlet:namespace />total=" + total +
		  						"&<portlet:namespace />category=" + category +
		  						"&<portlet:namespace />code=" + code,
		  				success: function(msg) {
		  					if(action != "updateRule") {
			  					// get table data
			  					if (msg != ""){
			  						var jsonEntries = jQuery.parseJSON(msg);
			  						total = jsonEntries.total;
			  						dataTable.set('data', eval(jsonEntries.values));
			  					} else {
			  						dataTable.set('data', []);
			  					}
			  					// set status of navigation buttons on page load
			  					setNavigationButtonsState('<portlet:namespace />');
		  					} else {
		  						alert("Modificarea a fost facuta cu success!");
		  					}
		  				}
		  			});
		  		}
		  		
		  		dataTable.delegate('click', function (e) {
					 e.preventDefault();
			  		 var elem = e.target.get('id');
			  		 var category = elem.replace("salveaza", "");
			  		 var code = "";
			  		 var ml = dataTable.data, msg = '', template = '';
			  		  ml.each(function (item, i) {
			  			var data = item.getAttrs(['code' ,'category', 'description', 'actiuni']);
			  			if(data.category == category){
			  				code = data.code;
			  			}
			  		 });
			  		 makeAjaxCall("updateRule", category, code);
				}, '.buton', dataTable);
		});
</script>