<%@page import="com.profluo.ecm.model.db.DefCategory"%>
<%@page import="com.profluo.ecm.model.vo.DefCurrencyVo"%>
<%@page import="com.profluo.ecm.model.vo.DefCompaniesVo"%>
<%@page import="com.profluo.ecm.model.db.WorkEquipmentHeader"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme"%>

<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui"%>
<%@ taglib uri="http://alloy.liferay.com/tld/aui" prefix="aui"%>
<%@ taglib uri="http://liferay.com/tld/util" prefix="liferay-util"%>
 
<liferay-theme:defineObjects />
<portlet:defineObjects />
<portlet:resourceURL id="ajaxURL" var="ajaxURL"/>

<% // PAGINATION Start - env setup 
int start 			= 0; 
int count 			= 15;
int total 			= 0;
int stage 			= 1;
String categorie 	= "";
String datafactura 	= "";
String datainreg 	= "";
String supplierCode = "";
String supplierName = "";
%>

<%
	// retrieve all invoices headers from DB
	List<HashMap<String,Object>> allWorkEquipment = new ArrayList<HashMap<String,Object>>();
	// retrieve all companies from DB
	List<HashMap<String,Object>> allCompanies = DefCompaniesVo.getInstance();
	// retrieve all currencies from DB
	List<HashMap<String,Object>> allCurrencies = DefCurrencyVo.getInstance();
	
	List<HashMap<String,Object>> allLotCat = DefCategory.getAll();
	
 	String storeCode 	= "";
	
// 	String email 	= "";

	if(user.getAddresses().size() != 0 && user.getAddresses().get(0).getStreet2() != null){
		storeCode = user.getAddresses().get(0).getStreet2();
	}
	
// 	if(user.getEmailAddress() != null){
// 		email = user.getEmailAddress();
// 	}
	allWorkEquipment = WorkEquipmentHeader.getAllFiltered(start, count, 0, "", "0", stage, "", "", "", "", user);
	total = WorkEquipmentHeader.getAllFilteredCount(start, count, 0, "", "0", stage, "", "", "", "", user);
	
%>

<script type="text/javascript">
var start = "<%=start%>";
var count = "<%=count%>";
var total = "<%=total%>";
var stage = "<%=stage%>";
var storeName = "<%=storeCode%>";
var datainreg = "<%=datainreg%>";
var datafactura = "<%=datafactura%>";
var supplierCode = "<%=supplierCode%>";
var supplierName = "<%=supplierName%>";
//filters
var company_id = 0;
var moneda = "";
var number = 0;
</script>

<liferay-ui:success message="delete_ok" key="delete_ok"/>
<liferay-ui:success message="invoiceAdded" 	key="invoiceAdded"/>
<liferay-ui:success message="invoiceUpdated" 	key="invoiceUpdated"/>
<liferay-ui:success message="update_ok" key="update_ok"/>

<%-- Links --%>

<portlet:renderURL var="registerHaineDeLucru" >
	<portlet:param name="mvcPath" value="/html/workequipment/registerWorkEquipment.jsp"></portlet:param>
</portlet:renderURL>

<aui:layout>
	<aui:column>
	<div id="numberOfInvoices">
		Numarul de facturi nealocate este :  <%= total %>
	</div>
	</aui:column>
</aui:layout>

<aui:layout>
<%-- 	<aui:column columnWidth="50" first="true"> --%>
<%-- 		<aui:button cssClass="btn btn-primary" onClick="<%=registerHaineDeLucru.toString()%>" value="Adauga factura"></aui:button> --%>
<%-- 	</aui:column> --%>
	<div id="filtru_dataimport_div" style="display:none">
		<aui:input type="text" id="data_fact" name="data_fact" value="" placeholder="aaaa-ll-zz" label=""></aui:input>
	</div>
</aui:layout>

<aui:layout>
	<aui:column first="true" columnWidth="100" last="true">
		<div id="listing-facturi"></div>
	</aui:column>
</aui:layout>

<%-- HTML for PAGINATION --%>
<%@ include file="/html/includes/pagination.jsp" %>

<%--  datepicker for table header --%>
<div id="filtru_datafactura_div" style="display:none">
	<aui:input type="text" id="datafactura_header" name="datafactura_header" placeholder="Data factura" value=""  label="" style="width:100px;margin:0">
	</aui:input>
</div>
<%--  second datepicker for table header --%>
<div id="filtru_datainregfactura_div" style="display:none">
	<aui:input type="text" id="datainreg_header" name="datainreg_header" placeholder="Data inregistrare" value="" label="" style="width:110px;margin:0">
	</aui:input>
</div>

<script type="text/javascript">
	var oldDateValue2 = '';
	var oldDateValue = '';
	YUI({lang: 'ro'}).use('aui-datepicker', 'aui-datatable', 'aui-datatype', 'datatable-sort', function(Y) {
		// called whenever a change in date selection was performed
		var oldDateValue2 = $('#<portlet:namespace />datafactura_header').val();
		// called whenever a change in date selection was performed
		var oldDateValue = $('#<portlet:namespace />datainreg_header').val();
		
		var remoteData = [
  		<%
  		if (allWorkEquipment != null) {
  		for (int i = 0; i < allWorkEquipment.size(); i++) {	
  		%>
  		{
  			id: 			<%=allWorkEquipment.get(i).get("id")%>,
  			inv_number: 	'<%=allWorkEquipment.get(i).get("inv_number")%>',
  			created: 		'<%=allWorkEquipment.get(i).get("created")%>',
  			supplier_code: 	'<%=allWorkEquipment.get(i).get("supplier_code")%>',
  			supplier_name: 	'<%=allWorkEquipment.get(i).get("supplier_name")%>',
  			inv_date: 		'<%=allWorkEquipment.get(i).get("inv_date")%>',
  			pay_date: 		'<%=allWorkEquipment.get(i).get("pay_date")%>', 
  			total_with_vat_curr: <%=allWorkEquipment.get(i).get("total_with_vat_curr")%>,
  			currency: 		'<%=allWorkEquipment.get(i).get("currency")%>',
  			company_name: 	'<%=allWorkEquipment.get(i).get("company_name")%>'
  		}
  		<% if (i != (allWorkEquipment.size() - 1)) { out.print(","); } %>
  		<% } } %>
  		];
		
		function formatCurrency(cell) {
		    format = {
		    	thousandsSeparator: ",",
		        decimalSeparator: ".",
		        decimalPlaces: noOfDecimalsToDisplay
		    };
		    return Y.DataType.Number.format(Number(cell.value), format);
		}
  		
  		// COLUMN INFO and ATTRIBUTES
  		var nestedCols = [
  		    { key: 'id', className: 'hiddencol'},
  			{ key: 'inv_number', 
  		    	label: '<input class="field" style="width:60px !important;margin:0" type="text" value="" id="filter-nr" name="filter-nr" placeholder="Nr."></input>' },
  			{ key: 'created', label: '<div id="filter-data-inreg"></div>'},
  			{ key: 'supplier_code',
  				label: '<input class="field" style="width:70px !important;margin:0" type="text" value="" id="filter-supplier_code" name="filter-supplier_code" placeholder="Cod furnizor" style="margin:0"/>'
  			},
  			{ key: 'supplier_name', 
  				label: '<input class="field" type="text" value="" id="filter-supplier_name" name="filter-supplier_code" placeholder="Furnizor" style="margin:0"/>'
  			},
  			{ key: 'inv_date', label:  '<div id="filter-dataf"></div>'},
  			{ key: 'pay_date', label:  'Data Scadenta'},
  			{ key: 'total_with_vat_curr', label: 'Val cu TVA', formatter: formatCurrency, className: 'currencyCol'},
  			{ key: 'currency', label: '<select id="filter-moneda" name="filter-moneda" label="" style="width:100px;margin:0">' +
  				'<option selected="selected" value="">Moneda</option>' +
  				<% for (int i = 0; i < allCurrencies.size(); i++) { %>
  				<% if (allCurrencies.get(i).get("status").equals("A")) {%>
  				<% out.println("'<option value=\"" + allCurrencies.get(i).get("ref") +"\">" + allCurrencies.get(i).get("ref") +"</option>' + "); %>
  				<% } } %>
  				'</select>'},
  			{ key: 'company_name', label: '<select id="filter-societate" name="filter-societate" label="" style="width:100px;margin:0">' +
  				'<option selected="selected" value="0">Societate</option>' +
  				<% for (int i = 0; i < allCompanies.size(); i++) { %>
  				<% if (allCompanies.get(i).get("status").equals("A")) {%>
  				<% out.println("'<option value=\"" + allCompanies.get(i).get("id") +"\">" + allCompanies.get(i).get("name") +"</option>' + "); %>
  				<% } } %>
  				'</select>'},
  			{ key: 'actiuni', label: 'Actiuni', allowHTML: true, formatter: '<a href="#" id="row{id}" class="editrow">Inregistrare</a>'
  				
  			}
  		];

  		// TABLE INIT
  		var dataTable = new Y.DataTable({
  		    columns: nestedCols,
  		    data: remoteData,
  		    recordType: ['id', 'inv_number', 'created', 'supplier_code', 
  		                 'supplier_name', 'inv_date', 'pay_date', 'total_no_vat_curr', 
  		                 'total_with_vat_curr', 'currency', 'company_name'],
  		    editEvent: 'click'
  		}).render('#listing-facturi');
  		
  		//dataTable.get('boundingBox').unselectable();
  		
  		function edit(sel_id, action) {
  		    var ml  = dataTable.data, msg = '', template = '';
  	
  		    ml.each(function (item, i) {
  		        var data = item.getAttrs(['id', 'inv_number', 'created', 'supplier_code', 
  		                                 'supplier_name', 'inv_date', 'pay_date', 'total_no_vat_curr', 
  		         		                 'total_with_vat_curr', 'currency', 'company_name']);

  		        if (data.id == sel_id) {
  					$('#<portlet:namespace />id').val(data.id);
  					$('#<portlet:namespace />inv_number').val(data.inv_number);
  					$('#<portlet:namespace />created').val(data.created);
  					$('#<portlet:namespace />supplier_code').val(data.supplier_code);
  					$('#<portlet:namespace />supplier_name').val(data.supplier_name);
  					$('#<portlet:namespace />inv_date').val(data.inv_date);
  					$('#<portlet:namespace />datascadenta').val(data.pay_date);
  					$('#<portlet:namespace />total_no_vat_curr').val(data.total_no_vat_curr);
  					$('#<portlet:namespace />total_with_vat_curr').val(data.total_with_vat_curr);
  					$('#<portlet:namespace />param_currency').val(data.currency);
					$('#<portlet:namespace />register').submit();
  		        }
  		    });
  		}
  		
  		dataTable.delegate('click', function (e) {
  		    // undefined to trigger the emptyCellValue
  		    e.preventDefault();
  		    var elem = e.target.get('id');
  		    if(elem.indexOf("row") >= 0) {
  		    	edit(elem.replace('row',""));
  		    }
  		    
  		}, '.editrow', dataTable);
  		
  		
  		// PAGINATION
  		// set status of navigation buttons on page load
  		setNavigationButtonsState('<portlet:namespace />');
  		$('.navigation').click(function(e) {
  			var elementT 	= e.target;
  			var elementId 	= elementT.id;
  			var action 		= "";
  			
  			// determine which button was pressed and set the value of start accordingly
  			if (elementId.indexOf("first") > -1)	{ action = "first"; start = 0; }
  			if (elementId.indexOf("prev") > -1) 	{ action = "prev"; if (start - count <= 0) {start = 0} else { start -= count; } }
  			if (elementId.indexOf("next") > -1) 	{ action = "next"; start = parseInt(start) + parseInt(count); }
  			if (elementId.indexOf("last") > -1) 	{ action = "last"; start = parseInt(parseInt(total) / parseInt(count)) * count; if (start == total) { start -= count; } }
  			
  			makeAjaxCall(action);
  		});
  		
  		// filtrare societate
  		$('#filter-societate').change(function(e) {
  			var elementT 	= e.target;
  			var elementId 	= elementT.id;
  			var action 		= "filter";
  			// get selected value
  			company_id = elementT.value;
  			moneda = $('#filter-moneda').val();
  			supplier_code = $('#filter-supplier_code').val();
  			supplierName = $('#filter-supplier_name').val();
  			number = $('#filter-nr').val();
			datafactura = oldDateValue2;
			datainreg = oldDateValue;
  			// reset start page 
  			start = 0;
  			// reset count
  			total = 0;
  			// make ajax call
  			makeAjaxCall(action);
  		});
  		
  		// filtrare moneda
  		$('#filter-moneda').change(function(e) {
  			var elementT 	= e.target;
  			var elementId 	= elementT.id;
  			var action 		= "filter";
  			// get selected value
  			moneda = elementT.value;
  			supplier_code = $('#filter-supplier_code').val();
  			supplierName = $('#filter-supplier_name').val();
  			number = $('#filter-nr').val();
  			company_id = $('#filter-societate').val();
			datafactura = oldDateValue2;
			datainreg = oldDateValue;
  			// reset start page 
  			start = 0;
  			// reset count
  			total = 0;
  			// make ajax call
  			makeAjaxCall(action);
  		});

  		// filtrare dupa cod furnizor
  		$('#filter-supplier_code').keyup(function (e) {
  		    if ($("#filter-supplier_code").is(":focus") && (e.keyCode == 13)) {
  				var elementT 	= e.target;
  				var elementId 	= elementT.id;
  				var action 		= "filter";
  				// get selected value
  				supplierCode = elementT.value;
  				moneda = $('#filter-moneda').val();
  	  			supplierName = $('#filter-supplier_name').val();
  	  			number = $('#filter-nr').val();
  	  			company_id = $('#filter-societate').val();
  				datafactura = oldDateValue2;
  				datainreg = oldDateValue;
  				if (number == "") {
  					number = 0;
  				}
  				// reset start page 
  				start = 0;
  				// reset count
  				total = 0;
  				// make ajax call
  				makeAjaxCall(action);
  		    }
  		});
  		
  		// filtrare dupa nume furnizor
  		$('#filter-supplier_name').change(function (e) {
  				var elementT 	= e.target;
  				var elementId 	= elementT.id;
  				var action 		= "filter";
  				// get selected value
  				supplierName = elementT.value;
  				moneda = $('#filter-moneda').val();
  	  			supplier_code = $('#filter-supplier_code').val();
  	  			number = $('#filter-nr').val();
  	  			company_id = $('#filter-societate').val();

  				datafactura = oldDateValue2;
  				datainreg = oldDateValue;
  				if (number == "") {
  					number = 0;
  				}
  				// reset start page 
  				start = 0;
  				// reset count
  				total = 0;
  				// make ajax call
  				makeAjaxCall(action);
  		    
  		});
  		
  		// filtrare dupa nr factura
  		$('#filter-nr').keyup(function (e) {
  		    if ($("#filter-nr").is(":focus") && (e.keyCode == 13)) {
  				var elementT 	= e.target;
  				var elementId 	= elementT.id;
  				var action 		= "filter";
  				// get selected value
  				number = elementT.value;
  				moneda = $('#filter-moneda').val();
  	  			supplier_code = $('#filter-supplier_code').val();
  	  			supplierName = $('#filter-supplier_name').val();
  	  			company_id = $('#filter-societate').val();

  				datafactura = oldDateValue2;
  				datainreg = oldDateValue;
  				if (number == "") {
  					number = 0;
  				}
  				// reset start page 
  				start = 0;
  				// reset count
  				total = 0;
  				// make ajax call
  				makeAjaxCall(action);
  		    }
  		});
  		
  		function makeAjaxCall(action) {
  			$.ajax({
  				type: "POST",
  				url: "<%=ajaxURL%>",
  				data: 	"<portlet:namespace />action=" + action + 
  						"&<portlet:namespace />start=" + start + 
  						"&<portlet:namespace />count=" + count +
  						"&<portlet:namespace />total=" + total +
  						"&<portlet:namespace />moneda=" + moneda +
  						"&<portlet:namespace />number=" + number +
  						"&<portlet:namespace />company_id=" + company_id +
  						"&<portlet:namespace />datafactura=" + datafactura +
  						"&<portlet:namespace />datainreg=" + datainreg +
  						"&<portlet:namespace />stage=" + stage +
  						"&<portlet:namespace />supplier_code=" + supplierCode +
  						"&<portlet:namespace />supplier_name=" + supplierName,
  				success: function(msg) {
  					// get table data
  					if (msg != ""){
  						var jsonEntries = jQuery.parseJSON(msg);
  						total = jsonEntries.total;
  						$('#numberOfInvoices').html("");
  						$('#numberOfInvoices').html("Numarul de facturi nealocate este : " + total);
  						dataTable.set('data', eval(jsonEntries.values));
  					} else {
  						dataTable.set('data', []);
  					}
  					
  					// set status of navigation buttons on page load
  					setNavigationButtonsState('<portlet:namespace />');
  				}
  			});
  		}
		
		function dateChangedFact(input) {
			oldDateValue2 = input.val();
			// get selected value
			if ($('#<portlet:namespace />datainreg_header').val() == ""){
				datainreg = "";
			} else {
				datainreg = $('#<portlet:namespace />datainreg_header').val();
			}
			datafactura = oldDateValue2;
			moneda = $('#filter-moneda').val();
  			supplier_code = $('#filter-supplier_code').val();
  			supplierName = $('#filter-supplier_name').val();
  			number = $('#filter-nr').val();
  			company_id = $('#filter-societate').val();
			// reset start page 
			start = 0;
			// reset count
			total = 0;
			action = "filter";
			// make ajax call
			makeAjaxCall(action);
		}
		
		var datepicker2 = new Y.DatePicker({
			trigger : '#<portlet:namespace />datafactura_header',
			mask : '%Y-%m-%d',
			popover : {
				zIndex : 1
			},
			popover: {
				zIndex : 1,
		          toolbars: {
		            header: [[
		              {
		                icon:'icon-trash',
		                label: 'Reseteaza filtru',
		                on: {
		                  click: function() {
		                	  datepicker2.clearSelection();
		                  }
		                }
		              }]]
		          }
			},
			after: {
	            selectionChange: function(event) {
	            	if (oldDateValue2 != $('#<portlet:namespace />datafactura_header').val()) {
	            		oldDateValue2 = $('#<portlet:namespace />datafactura_header').val();
	            		dateChangedFact($('#<portlet:namespace />datafactura_header'));
	            	}
	            	//$('#data_filter').html('');
	            }
	        },
			calendar : {
				dateFormat : '%Y-%m-%d'
			}
		});
		

		function dateChangedInreg(input) {
			
			oldDateValue = input.val();
			// get selected value
			if ($('#<portlet:namespace />datafactura_header').val() == ""){
				datafactura = "";
			} else {
				datafactura = $('#<portlet:namespace />datafactura_header').val();
			}
			datainreg = oldDateValue;
			moneda = $('#filter-moneda').val();
  			supplier_code = $('#filter-supplier_code').val();
  			supplierName = $('#filter-supplier_name').val();
  			number = $('#filter-nr').val();
  			company_id = $('#filter-societate').val();
			// reset start page 
			start = 0;
			// reset count
			total = 0;
			action = "filter";
			// make ajax call
			makeAjaxCall(action);
		}
		
		var datepicker = new Y.DatePicker({
			trigger : '#<portlet:namespace />datainreg_header',
			mask : '%Y-%m-%d',
			popover : {
				zIndex : 1
			},
			popover: {
				zIndex : 1,
		          toolbars: {
		            header: [[
		              {
		                icon:'icon-trash',
		                label: 'Reseteaza filtru',
		                on: {
		                  click: function() {
		                	  datepicker.clearSelection();
		                  }
		                }
		              }]]
		          }
			},
			calendar : {
				dateFormat : '%Y-%m-%d'
			},
			after: {
	            selectionChange: function(event) {
	            	if (oldDateValue != $('#<portlet:namespace />datainreg_header').val()) {
	            		oldDateValue = $('#<portlet:namespace />datainreg_header').val();
	            		dateChangedInreg($('#<portlet:namespace />datainreg_header'));
	            	}
	            	//$('#data_filter').html('');
	            }
	        }
		});
	});
</script>

<aui:form method="post" action="<%=registerHaineDeLucru.toString() %>" name="register" id="register" style="margin:0;padding:0">
	<aui:input type="hidden" id="id" name="id"/>
	<aui:input type="hidden" id="inv_number" name="inv_number"/>
	<aui:input type="hidden" id="created" name="created"/>
	<aui:input type="hidden" id="supplier_code" name="supplier_code"/>
	<aui:input type="hidden" id="supplier_name" name="supplier_name"/>
	<aui:input type="hidden" id="inv_date" name="inv_date"/>
	<aui:input type="hidden" id="datascadenta" name="datascadenta"/>
	<aui:input type="hidden" id="total_no_vat_curr" name="total_no_vat_curr"/>
	<aui:input type="hidden" id="total_with_vat_curr" name="total_with_vat_curr"/>
	<aui:input type="hidden" id="param_currency" name="currency"/>
	<aui:input type="hidden" id="storeId" name="storeId" value="<%=storeCode %>"/>
</aui:form>