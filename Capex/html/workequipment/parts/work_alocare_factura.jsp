<%@page import="com.profluo.ecm.model.db.DefStore"%>
<%@page import="com.profluo.ecm.model.vo.DefNewProjVo"%>
<%@page import="com.profluo.ecm.model.vo.ListaInitVo"%>
<%@page import="com.profluo.ecm.model.db.DatabaseConnectionManager"%>
<%@page import="com.profluo.ecm.model.vo.DefDpis"%>
<%@page import="com.profluo.ecm.model.vo.DefAisles"%>
<%@page import="com.profluo.ecm.model.vo.DefCatGestVo"%>
<%@page import="com.profluo.ecm.model.vo.DefIFRSVo"%>
<%@page import="com.profluo.ecm.flows.InvoiceStages"%>
<%@page import="com.profluo.ecm.model.vo.DefProductVo"%>
<%@page import="com.profluo.ecm.model.vo.DefLotVo"%>
<%@page import="com.profluo.ecm.model.vo.DefIASVo"%>
<%@page import="com.profluo.ecm.model.vo.DefUMVo"%>
<%@page import="com.profluo.ecm.model.db.InvoiceHeader"%>
<%@page import="com.profluo.ecm.model.db.GenericDBStatements"%>
<%@page import="com.profluo.ecm.model.vo.DefStoreVo"%>
<%@page import="com.profluo.ecm.model.vo.DefRegEquipVo"%>
<%@page import="com.profluo.ecm.model.vo.DefVATVo"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.List"%>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui"%>
<%@ taglib uri="http://alloy.liferay.com/tld/aui" prefix="aui"%>
<%@ taglib uri="http://liferay.com/tld/util" prefix="liferay-util"%>
<%@page import="com.profluo.ecm.model.vo.DefMFActVo"%>
<%@ page import="java.util.ResourceBundle"%>
<%@ page import="java.util.Locale"%>

<liferay-theme:defineObjects />
<portlet:defineObjects />
<%
	// retrieve all VAT values
	List<HashMap<String,Object>> allVAT = DefVATVo.getInstance();
	//get all units of measure VO
	List<HashMap<String,Object>> allUM = DefUMVo.getInstance();
%>

<script type="text/javascript">
	var allInitiatives = <%=ListaInitVo.getJsonStringInitiatives()%>;
	var allNewProjects = <%=DefNewProjVo.getJsonStringNewProjects()%>;
</script>

<legend>
	<strong>Linii factura</strong>
</legend>
<aui:layout>
	<aui:column first="true" columnWidth="100" last="true">
		<div id="alocare-magazine"></div>
	</aui:column>
</aui:layout>

<aui:layout>
<%-- 	<aui:column first="true"> --%>
<%-- 		<label for="<portlet:namespace />val_no_vat_art" --%>
<%-- 			style="padding-top: 5px"><liferay-ui:message key="val_no_vat_art"/></label> --%>
<%-- 	</aui:column> --%>
<%-- 	<aui:column> --%>
<%-- 		<aui:input cssClass="currencyCol" name="val_no_vat_art" id="val_no_vat_art" value="" label="" --%>
<%--  			readonly="readonly" />  --%>
<%--  	</aui:column>  --%>
	<aui:column>
		<label for="<portlet:namespace />val_no_vat_aloc_linii"
			style="padding-top: 5px"><liferay-ui:message key="val_no_vat_aloc_linii"/></label>
	</aui:column>
	<aui:column>
		<aui:input cssClass="currencyCol" name="val_no_vat_aloc_linii" id="val_no_vat_aloc_linii"
 			value="" label="" readonly="readonly" /> 
 	</aui:column> 
	<aui:column>
		<label for="<portlet:namespace />difference" style="padding-top: 5px"><liferay-ui:message key="difference"/></label>
	</aui:column>
	<aui:column>
		<aui:input cssClass="currencyCol" name="difference" id="difference" value="" label=""
			readonly="readonly">
			<aui:validator name="custom" errorMessage="incorect">
				function (val, fieldNode, ruleValue) {
					var result = false;
					if (val == 0) {
						result = true;
					}
					return result;
				}
			</aui:validator>
		</aui:input>
	</aui:column>
</aui:layout>

<div id="overlay-detalii-inv" class="yui3-overlay-loading"
	style="left: -350px; position: absolute; width: 280px">
	<div class="yui3-widget-hd">
		Detalii inventar pentru articolul: [<span id="cod_art_header">&nbsp;</span>]<span
			id="den_art_header">&nbsp;</span>
	</div>
	<div class="yui3-widget-bd">
		<aui:layout>
			<aui:column first="true" columnWidth="100" last="true">
				<div id="detalii-inventar"></div>
			</aui:column>
		</aui:layout>
		<aui:layout>
			<aui:column columnWidth="50" first="true">
				<aui:input value="Inchide" id="finvcancel" name="finvcancel" type="button" label=""
					cssClass="btn btn-primary-red fl"></aui:input>
			</aui:column>
			<aui:column columnWidth="50" last="true">
				<aui:input id="salveaza-inv" name="salveaza-inv" value="Salveaza"
					type="button" label="" cssClass="btn btn-primary fr"></aui:input>
			</aui:column>
		</aui:layout>
	</div>
</div>
<%-- overlay solicita vizualizare DPI --%>
<div id="vizualizareDPI" class="yui3-overlay-loading"
	style="right: 100px; z-index: 1; position: absolute; width: 800px; display: none">
	<div class="yui3-widget-hd">Solicitare vizualizare DPI</div>
	<div class="yui3-widget-bd">
		<aui:layout>
			<aui:fieldset>
				<aui:column columnWidth="20">
					<label for="<portlet:namespace />nr_dpi" style="padding-top: 5px">Nr
						DPI: </label>
				</aui:column>
				<aui:column columnWidth="30">
					<aui:input id="nr_dpi" name="nr_dpi" label="" readonly="true"></aui:input>
				</aui:column>
				<aui:column columnWidth="20">
					<label for="<portlet:namespace />societate"
						style="padding-top: 5px">Societate:</label>
				</aui:column>
				<aui:column columnWidth="30">
					<aui:input id="societate" name="societate" label="" readonly="true"></aui:input>
				</aui:column>
			</aui:fieldset>

			<aui:fieldset>
				<aui:column columnWidth="20">
					<label for="<portlet:namespace />data_dpi" style="padding-top: 5px">Data
						DPI: </label>
				</aui:column>
				<aui:column columnWidth="30">
					<aui:input id="data_dpi" name="data_dpi" label="" readonly="true"></aui:input>
				</aui:column>
				<aui:column columnWidth="20">
					<label for="<portlet:namespace />categ_capex_dpi"
						style="padding-top: 5px">Categorie Capex:</label>
				</aui:column>
				<aui:column columnWidth="30">
					<aui:input id="categ_capex_dpi" name="categ_capex_dpi" label=""
						readonly="true"></aui:input>
				</aui:column>
			</aui:fieldset>

			<aui:fieldset>
				<aui:column columnWidth="20">
					<label for="<portlet:namespace />solicitant"
						style="padding-top: 5px">Solicitant: </label>
				</aui:column>
				<aui:column columnWidth="30">
					<aui:input id="solicitant" name="solicitant" label=""
						readonly="true"></aui:input>
				</aui:column>
			</aui:fieldset>

			<aui:fieldset>
				<aui:column columnWidth="20">
					<label for="<portlet:namespace />aprobator"
						style="padding-top: 5px">Aprobator: </label>
				</aui:column>
				<aui:column columnWidth="80">
					<aui:input id="aprobator" name="aprobator" label="" readonly="true"
						style="width: 97.6%"></aui:input>
				</aui:column>
			</aui:fieldset>
			<aui:layout>
				<aui:column columnWidth="100" last="true" first="true">
					<div id="tabelDPI"></div>
				</aui:column>
			</aui:layout>
			<aui:layout>
				<aui:column columnWidth="50" last="true" style="text-align:right">
					<aui:input value="Inchide" name="fcancel" type="button" label=""
						cssClass="btn btn-primary-red fr"
						onclick="$('#vizualizareDPI').hide()"></aui:input>
					<div class="clear"></div>
				</aui:column>
			</aui:layout>
		</aui:layout>
	</div>
</div>

<!-- Used for saving idLine on click on detalii button -->
<aui:input type="hidden" name="idStoreLine" id="idStoreLine" value=""/>

<script type="text/javascript">


//=============================================================================================================================================================
//																	GLOBAL VARIABLES	
//=============================================================================================================================================================	

//counts the number of invoice line (only on adding a new invoice)
var counter = 0;

//counter for store lines
var storeLineCounter = 0;

var dataTableDPI;
var dataTableSplit;
var dataTableInv;
var selectedCompany = '';
var storeid = <%=storeId%>;
var companyHasChanged = false;
var idLine, denArt, codArt, tipInv;
YUI({lang: 'ro'}).use( 'aui-datatable', 'aui-datatype', 'datatable-sort', 'overlay', 'aui-form-validator', 'datatable-mutable',
	function(Y) {
	
//=============================================================================================================================================================
//																		GENERICS	
//=============================================================================================================================================================	
	/** GENERIC CURRENCY FORMATTER **/
		function formatCurrency(cell) {
		    format = {
		    	thousandsSeparator: ",",
		        decimalSeparator: ".",
		        decimalPlaces: noOfDecimalsToDisplay
		    };
		    return Y.DataType.Number.format(Number(cell.value), format);
		}
		
		/** GENERIC SAVE ON ENTER **/
		function saveOnEnter(e) {
			if (e.domEvent.charCode == 13) {
				if (!e.target.validator.hasErrors()) {
					e.target.fire('save', {
		                newVal: e.target.getValue(),
		                prevVal: e.target.get('value')
		            });
				}
			}
		}
		
		/** GENERIC SAVE ON CHANGE ON SELECT **/
		function saveOnChange(e) {
			e.target.fire('save', {
	            newVal: e.target.getValue(),
	            prevVal: e.target.get('value')
	        });
		}
		
		// editor attributes, actions and formats based on datatypes
		var attrNumberEditor = {inputFormatter: Y.DataType.Number.evaluate, validator: { rules: { value: { number: true } } }, on : {keyup: saveOnEnter}, showToolbar: false };
		var attrStringEditor = {on :{keyup: saveOnEnter}, showToolbar: false};	

		
//=============================================================================================================================================================
//																	 START - COLUMN INFO and ATTRIBUTES	
//=============================================================================================================================================================			
			
		var firstStore = "";
		
		var raionDepartament = <%=DefAisles.getJsonStringAisles("326")%>;
		
		var allProds = <%=DefProductVo.getJsonStringProduct()%>;
		var allDPIs = <%=DefDpis.getJsonStringDpis(true)%>;
		var raionOrDept = 0;
		
		var UMList = [<%for (int i = 0; i < allUM.size(); i++) { %>
						<%if (allUM.get(i).get("status").equals("A")) {%>
							<%if (allUM.size() - 1 == i) {%>
								'<%=allUM.get(i).get("ref")%>'
							<%} else {%>
								'<%=allUM.get(i).get("ref")%>',
							<%}%>
						<%}%>
					<%}%>];
		var iasList = <%=DefIASVo.getJsonStringIAS()%>;
		var ifrsList = <%=DefIFRSVo.getJsonStringIFRS()%>;
		var allLots = <%=DefLotVo.getJsonStringLots()%>;
		var clasif = {1:'IT Hardware', 2:'IT Software', 0:'Active.'};
		var tvalist = [<%for (int i = 0; i < allVAT.size(); i++) {%>
							<%if (allVAT.get(i).get("status").equals("A")) {%>
								<%if (allVAT.size() - 1 == i) {%>
								'<%=allVAT.get(i).get("ref")%>'
								<%} else {%>
								'<%=allVAT.get(i).get("ref")%>',
								<%}%>
							<%}%>
						<%}%>];
		//initialize store variables based on companies
		<%for (int i = 0; i < allCompanies.size(); i++) { %>
			<%if (allCompanies.get(i).get("status").toString().equals("A")) { %>
			var store_<%=allCompanies.get(i).get("id")%> = <%=DefStoreVo.getJsonStringStoresByCompany(allCompanies.get(i).get("id").toString())%>
			<% } %>
		<% } %>
		
		var tipinventar =  {1: "individual", 2: "de grup"}; 
		var tipinregistrare = <%=DefRegEquipVo.getJsonStringRegEquip()%>;
		var actiunimf=<%=DefMFActVo.getJsonStringMfAct()%>;
		var cat_gestiune=<%=DefCatGestVo.getJsonStringCatGestForWorkingEquipment()%>;

//=============================================================================================================================================================
//																	POP-UP INFO DPI LINES
//=============================================================================================================================================================			

		var datahDPI = [];
		
		var colsDPI = [
			{ key: 'id_prod', 				label:'Cod produs'},
			{ key: 'description', 			label:'Denumire produs'},
			{ key: 'unit_price_curr', 		label:'Pret Unitar', formatter: formatCurrency, className: 'currencyCol'},
			{ key: 'quantity', 				label:'Cantitate'},
			{ key: 'total_with_vat_curr', 	label:'Valoare totala', formatter: formatCurrency, className: 'currencyCol'},
			{ key: 'magazin', 				label:'Magazin'},
			{ key: 'clasificare_it', 		label:'Clasificare IT'}
		];
		
		dataTableDPI = new Y.DataTable({
		    columns: colsDPI,
		    data:datahDPI
		}).render('#tabelDPI');	
		
//=============================================================================================================================================================
//																		NR DE INVENTAR POP-UP
//=============================================================================================================================================================			

		var remoteDataInv = []; 		
		var nestedColsInv = [
		      			{ key: 'nr_inv', label: 'Nr. inv.', editor: new Y.TextCellEditor({on : {keyup: saveOnEnter}, showToolbar: false})},
		      			{ key: 'data_pif', label: 'Data PIF', editor: new Y.DateCellEditor({dateFormat: '%Y-%m-%d'})},
		      			{ key: 'receptie', label: 'Receptie', editor: new Y.TextCellEditor(attrStringEditor)}
		      			];
  		
  		// TABLE INIT
  		dataTableInv = new Y.DataTable({
  		    columns: nestedColsInv,
  		    data: remoteDataInv,
  		    editEvent: 'click',
  		}).render('#detalii-inventar');	

//=============================================================================================================================================================
//																		INVOICE STORE LINE TABLE
//=============================================================================================================================================================			
		
		var remoteDataSplit = [
		                       <% int count = 1; 
		                       int noOfCommas = workEquipmentLinesByWorkEqNr.size() - 1; %>   
		   					<%for (int i = 0; i < workEquipmentLinesByWorkEqNr.size(); i++ ) { %>
		           			{
		           				invoice_line_id	: <%=workEquipmentLinesByWorkEqNr.get(i).get("id")%>,
		           				delete_line 	: '<%=i+1%>',
		        				inv_number		: '<%=workEquipmentLinesByWorkEqNr.get(i).get("inv_number")%>',
		        				<%if(workEquipmentLinesByWorkEqNr.get(i).get("currency").toString().equals("RON")){%>
		        				unit_price_ron 	: <%=workEquipmentLinesByWorkEqNr.get(i).get("unit_price_curr")%>,
		        				price_no_vat_ron: '<%=workEquipmentLinesByWorkEqNr.get(i).get("price_no_vat_curr")%>',
		        				price_vat_ron 	: '<%=workEquipmentLinesByWorkEqNr.get(i).get("price_vat_curr")%>',
		        				<%} else {%>
		        				unit_price_ron 	: <%=workEquipmentLinesByWorkEqNr.get(i).get("unit_price_ron")%>,
		        				price_no_vat_ron: '<%=workEquipmentLinesByWorkEqNr.get(i).get("price_no_vat_ron")%>',
		        				price_vat_ron 	: '<%=workEquipmentLinesByWorkEqNr.get(i).get("price_vat_ron")%>',
		        				<%}%>
		        				price_vat_curr 	: '<%=workEquipmentLinesByWorkEqNr.get(i).get("price_vat_curr")%>',
		        				currency:'<%=workEquipmentLinesByWorkEqNr.get(i).get("currency")%>',
		        				article_code	: '<%=(workEquipmentLinesByWorkEqNr.get(i).get("article_code") == null ? "0" : workEquipmentLinesByWorkEqNr.get(i).get("article_code"))%>', 
		        				product_code	: '<%=(workEquipmentLinesByWorkEqNr.get(i).get("product_code")  == null ? "" : workEquipmentLinesByWorkEqNr.get(i).get("product_code"))%>', 
		        				description		: '<%=(workEquipmentLinesByWorkEqNr.get(i).get("description") == null ? "" : workEquipmentLinesByWorkEqNr.get(i).get("description"))%>',
		        				um: '<%=workEquipmentLinesByWorkEqNr.get(i).get("um")%>',
		        				quantity		: '<%=(workEquipmentLinesByWorkEqNr.get(i).get("quantity")  == null ?  "" : workEquipmentLinesByWorkEqNr.get(i).get("quantity"))%>',
		        				unit_price_curr	: '<%=workEquipmentLinesByWorkEqNr.get(i).get("unit_price_curr")%>',
		        				price_no_vat_curr: '<%=workEquipmentLinesByWorkEqNr.get(i).get("price_no_vat_curr")%>',
		        				vat: '<%=(workEquipmentLinesByWorkEqNr.get(i).get("vat") == null ? "24.00" : String.format("%.2f", Float.parseFloat(workEquipmentLinesByWorkEqNr.get(i).get("vat").toString() )))%>',
		        				val_cu_tva		: <%=Float.parseFloat(workEquipmentLinesByWorkEqNr.get(i).get("price_no_vat_curr").toString()) + Float.parseFloat(workEquipmentLinesByWorkEqNr.get(i).get("price_vat_curr").toString())%>,
		        				warranty		: '<%=(workEquipmentLinesByWorkEqNr.get(i).get("warranty")  == null ? "0" : workEquipmentLinesByWorkEqNr.get(i).get("warranty"))%>',
		        				id_ias			: '<%=workEquipmentLinesByWorkEqNr.get(i).get("id_ias")%>',
		        				id_ifrs			: '<%=workEquipmentLinesByWorkEqNr.get(i).get("id_ifrs")%>',
		        				dt_ras			: '<%=workEquipmentLinesByWorkEqNr.get(i).get("dt_ras")%>',
		        				dt_ifrs			: '<%=workEquipmentLinesByWorkEqNr.get(i).get("dt_ifrs")%>',
		        				lot				: '<%=workEquipmentLinesByWorkEqNr.get(i).get("lot")%>',
		        				clasif			: '<%=workEquipmentLinesByWorkEqNr.get(i).get("clasif")%>',
		        				cont_imob_in_fct:'<%=workEquipmentLinesByWorkEqNr.get(i).get("cont_imob_in_fct")%>',
		             		    cont_imob_in_curs:'<%=workEquipmentLinesByWorkEqNr.get(i).get("cont_imob_in_curs")%>',
		             		    cont_imob_avans:'<%=workEquipmentLinesByWorkEqNr.get(i).get("cont_imob_avans")%>',
		        				detalii			: <%=workEquipmentLinesByWorkEqNr.get(i).get("line")%>,
		           				id_store_line : '<%=workEquipmentStoreLinesByWorkEqNr.get(i).get("id")%>',
		           				store_name : '<%=DefStore.getStoreNameById(workEquipmentStoreLinesByWorkEqNr.get(i).get("id_store").toString())%>',
		           				id_store : '<%=workEquipmentStoreLinesByWorkEqNr.get(i).get("id_store")%>',
		           				unit_price: '<%=workEquipmentLinesByWorkEqNr.get(i).get("unit_price_curr")%>',
		           				id :'<%=workEquipmentStoreLinesByWorkEqNr.get(i).get("id")%>',
		           				id_department : '<%=workEquipmentStoreLinesByWorkEqNr.get(i).get("id_department")%>',
		           				associated_acc:'<%=(workEquipmentLinesByWorkEqNr.get(i).get("cont_imob_in_curs") == null ? 0 : workEquipmentLinesByWorkEqNr.get(i).get("cont_imob_in_curs"))%>',
		           				<% if (workEquipmentStoreLinesByWorkEqNr.get(i).get("reception_date").toString().compareTo(oneWorkEquipment.get(0).get("inv_date").toString()) > 0){ %>
		           					id_tip_inreg: '<%=(workEquipmentStoreLinesByWorkEqNr.get(i).get("id_tip_inreg") == null ? 1 : workEquipmentStoreLinesByWorkEqNr.get(i).get("id_tip_inreg"))%>',
	           					<% } else { %>
		           					id_tip_inreg: '<%=(workEquipmentStoreLinesByWorkEqNr.get(i).get("id_tip_inreg") == null ? 2 : workEquipmentStoreLinesByWorkEqNr.get(i).get("id_tip_inreg"))%>',
		           				<% } %>
		           				id_act_mf 			: '<%=(workEquipmentStoreLinesByWorkEqNr.get(i).get("id_act_mf") == null ? 1 : workEquipmentStoreLinesByWorkEqNr.get(i).get("id_act_mf"))%>',
		           				id_cat_gestiune		: '<%=workEquipmentStoreLinesByWorkEqNr.get(i).get("id_cat_gestiune")%>',
		           				id_dpi 				:'<%=(workEquipmentStoreLinesByWorkEqNr.get(i).get("id_dpi") == null ? 0 : workEquipmentStoreLinesByWorkEqNr.get(i).get("id_dpi"))%>',
		           				vizualizare_dpi		: '<%=(workEquipmentStoreLinesByWorkEqNr.get(i).get("id_dpi") == null ? 0 : workEquipmentStoreLinesByWorkEqNr.get(i).get("id_dpi"))%>',
								id_tip_inventar		: '<%=(workEquipmentStoreLinesByWorkEqNr.get(i).get("id_tip_inventar") == null ? 2 : workEquipmentStoreLinesByWorkEqNr.get(i).get("id_tip_inventar"))%>',
		           				initiative 			: '<%=workEquipmentStoreLinesByWorkEqNr.get(i).get("initiative")%>',
		           				new_prj 			: '<%=workEquipmentStoreLinesByWorkEqNr.get(i).get("new_prj")%>',
		           				pif_date			: '<%=workEquipmentStoreLinesByWorkEqNr.get(i).get("pif_date")%>',
		           				reception			: '<%=workEquipmentStoreLinesByWorkEqNr.get(i).get("reception")%>',
		           				inventory_no		: '<%=workEquipmentStoreLinesByWorkEqNr.get(i).get("inventory_no")%>'
		           			}
		           			<%if (noOfCommas > 0) { out.print(","); noOfCommas--;count++;}%>
		   	        	<%}%>
		                       
		                       
		                       ];
		//INVOICE STORE LINE COLUMN DEFINITION
		
		var nestedColsSplit = [
			{ key: 'invoice_line_id', 	className: 'hiddencol'},  
			{ key: 'id_store_line', 	className: 'hiddencol'},    
			{ key: 'delete_line', label: ' ', allowHTML: true, 
				formatter: function(o) {
					return '<a href="#" id="row' + o.data.invoice_line_id + '" class="deleteLineFromDatabase"><img src="${themeDisplay.getPathThemeImages()}/delete.png" width="20"/></a>';
				}
			},
			{ key: 'inv_number', 		className: 'hiddencol'},
			{ key: 'unit_price_ron', 	className: 'hiddencol'},
			{ key: 'price_no_vat_ron', 	className: 'hiddencol'},
			{ key: 'price_vat_ron', 	className: 'hiddencol'}, 
			{ key: 'price_vat_curr', 	className: 'hiddencol'},
		    { key: 'currency', 			className: 'hiddencol'}, 
			{ key: 'article_code', 		className: 'hiddencol'},
			{ key: 'product_code', 		label: 'Cod Prod', },
			{ key: 'description', 		label: 'Den. articol',	editor: new Y.TextCellEditor(attrStringEditor)},
			{ key: 'um', 				label: 'UM', 			editor: new Y.DropDownCellEditor({options: UMList, on : {change: saveOnChange}, showToolbar: false})},
			{ key: 'quantity', 			label:  'Cant', 		editor: new Y.TextCellEditor(attrNumberEditor), formatter: formatCurrency, className: 'currencyCol'},
			{ key: 'unit_price_curr', 	label: 'Pret unitar', 	editor: new Y.TextCellEditor(attrNumberEditor), formatter: formatCurrency, className: 'currencyCol'},		
			{ key: 'price_no_vat_curr', label: 'Val fara TVA', 	editor: new Y.TextCellEditor(attrNumberEditor), formatter: formatCurrency, className: 'currencyCol'},		
			{ key: 'vat', 				label: 'Cota TVA', 		editor: new Y.DropDownCellEditor({options: tvalist, on : {change: saveOnChange}, showToolbar: false}),
				formatter : function(o) {return o.value + '%';}	
			},
			{ key: 'val_cu_tva', 		label: 'Val cu TVA', formatter: formatCurrency, className: 'currencyCol'},
			{ key: 'id_ias', 			label: 'Grupa IAS', 	
				editor: new Y.DropDownCellEditor( {options: iasList, id: 'allias', elementName: 'allias',
					after: { focus: function(event) {  $("select[name=allias]").chosen(); } } }),
				emptyCellValue: '',
				formatter: function(o) {
					var obj = JSON.parse(JSON.stringify(iasList));
					if (typeof o.value != 'undefined') {
						return obj[o.value].toString();
					} else {
						return '';
					}
				}
			},
			{ key: 'id_ifrs', 	label: 'Grupa IFRS', 	
				editor: new Y.DropDownCellEditor({options: ifrsList, id: 'allifrs', elementName: 'allifrs',
						after: {focus: function(event) { $("select[name=allifrs]").chosen();} } }),
				emptyCellValue: '',
				formatter: function(o) {
					var obj = JSON.parse(JSON.stringify(ifrsList));
					if (typeof o.value != 'undefined') {
						return obj[o.value].toString();
					} else {
						return '';
					}
				}
			},
			{ key: 'dt_ras', 		label: 'Dt. IAS', 		editor: new Y.TextCellEditor({on : {keyup: saveOnEnter}, showToolbar: false}) },
			{ key: 'dt_ifrs', 		label: 'Dt. IFRS', 		editor: new Y.TextCellEditor({on : {keyup: saveOnEnter}, showToolbar: false})},
			{ key: 'lot', 			label: 'Lot', 			editor: new Y.DropDownCellEditor({options: allLots, on : {change: saveOnChange}, showToolbar: false}),
				emptyCellValue: '',
				formatter: function(o) {
					var obj = JSON.parse(JSON.stringify(allLots));
					if (typeof o.value != 'undefined' ) {
						return obj[o.value].toString();
					} else {
						return '';
					}
				}
			},
			{ key: 'clasif', 		label: 'Clasif.', 		editor: new Y.DropDownCellEditor({options: clasif, on : {change: saveOnChange}, showToolbar: false}), 
				emptyCellValue: '',
				formatter: function(o) {
					var obj = JSON.parse(JSON.stringify(clasif));
					if(obj[o.value] != null && obj[o.value] !='null') {
						return obj[o.value].toString();
					} else {
						return '';
					}
				}
			},
		    { key: 'cont_imob_in_fct', className: 'hiddencol'},
		    { key: 'cont_imob_in_curs', className: 'hiddencol'},
		    { key: 'cont_imob_avans', className: 'hiddencol'},
  		    { key: 'unit_price', className: 'hiddencol'},
			{ key: 'store_name', label: 'Magazin', className :'stores' },
			{ key: 'id_store', className: 'hiddencol' },
       		{ key: 'id_department', label: 'Raion/Dep.',editor: new Y.DropDownCellEditor({options: raionDepartament, id: 'raionDepartament', elementName: 'raionDepartament',
				after: { focus: function(event) { $("select[name=raionDepartament]").chosen(); } } }),
				emptyCellValue: '',
				formatter: function(o) {
					var obj = JSON.parse(JSON.stringify(raionDepartament));
					if (typeof o.value != 'undefined') {
						if(o.value != null) {
							raionOrDept = o.value;
							return obj[o.value].toString();
						} else return '';
					} else {
						return '';
					}
				}
			},
        	{ key: 'associated_acc', label: 'Cont', editor: new Y.TextCellEditor(attrNumberEditor)},
        	{ key: 'id_tip_inreg', label: 'Tip inregistrare',  editor: new Y.DropDownCellEditor({options: tipinregistrare, on : {change: saveOnChange}, showToolbar: false}),
        		emptyCellValue: '',
        		formatter: function(o) {
        			var obj = JSON.parse(JSON.stringify(tipinregistrare));
        				if (typeof o.value != 'undefined') {
        					return obj[o.value].toString();
        				} else {
        					return '0';
        				}
        			}
        		},
   			{ key: 'id_act_mf', label: 'Actiune MF', editor: new Y.DropDownCellEditor({options: actiunimf, on : {change: saveOnChange}, showToolbar: false}),
   				emptyCellValue: '',
   				formatter: function(o) {
   					var obj = JSON.parse(JSON.stringify(actiunimf));
   					if (typeof o.value != 'undefined') {
   						return obj[o.value].toString();
   					} else {
   						return '0';
   					}
   				}
   			},
   			{ key: 'id_cat_gestiune', label : 'Gestiune', editor: new Y.DropDownCellEditor({options: cat_gestiune, on : {change: saveOnChange}, showToolbar: false}),
   				emptyCellValue: '',
   				formatter: function(o) {
   					var obj = JSON.parse(JSON.stringify(cat_gestiune));
   					if (typeof o.value != 'undefined') {
   						return obj[o.value].toString();
   					} else {
   						return '0';
   					}
   				}
   			},
   			{ key: 'id_dpi', label: 'Nr. DPI', editor: new Y.DropDownCellEditor({options: allDPIs, id: 'alldpis', elementName: 'alldpis',
   				after: { focus: function(event) {  $("select[name=alldpis]").chosen(); } } }), 
   				formatter: function(o) {
	   				var obj = JSON.parse(JSON.stringify(allDPIs));
	   				if (typeof o.value != 'undefined') {
	   					return obj[o.value].toString();
	   				} else {
	   					return '0';
	   				}
   				}
   			},
   			{ key: 'vizualizare_dpi', label: 'Link DPI', 
   					allowHTML: true, 
   					formatter: function(o) { return '<a href="#" class="dpilink" id="dpi_' + o.data.nr + '_' + o.value + '">DPI</a>'; },
   					emptyCellValue: '<a href="#" class="dpilink" id="dpi_{nr}_{value}">DPI</a>'},
  			{ key: 'id_tip_inventar', label: 'Tip nr. inventar', editor: new Y.DropDownCellEditor({options: tipinventar, on : {change: saveOnChange}, showToolbar: false}),
				emptyCellValue: '',
				formatter: function(o) {
					var obj = JSON.parse(JSON.stringify(tipinventar));
					if (typeof o.value != 'undefined') {
						return obj[o.value].toString();
					} else {
						return '';
					}
				}
			},
			{ key: 'initiative', 	label: 'Initiative', 	
   				editor: new Y.DropDownCellEditor(
   							{options: allInitiatives, id: 'alist', elementName: 'alist',
							after: {
			            focus: function(event) {
				            $("select[name=alist]").chosen();
			           	 		}
							}
   						}),
				emptyCellValue: '',
				formatter: function(o) {
						var obj = JSON.parse(JSON.stringify(allInitiatives));
					if (typeof o.value != 'undefined' && o.value != 'null' && obj[o.value]) {
						return obj[o.value].toString();
					} else {
						return '';
					}
				}
  			},
  			{ key: 'new_prj', 	label: 'Proiecte Noi', 	
   				editor: new Y.DropDownCellEditor(
   							{options: allNewProjects, id: 'alist', elementName: 'alist',
							after: {
			            focus: function(event) {
				            $("select[name=alist]").chosen();
			           	 		}
							}
   						}),
				emptyCellValue: '',
				formatter: function(o) {
						var obj = JSON.parse(JSON.stringify(allNewProjects));
					if (typeof o.value != 'undefined' && o.value != 'null' && obj[o.value]) {
						return obj[o.value].toString();
					} else {
						return '';
					}
				}
  			},
   			{ key: 'inventory_no', 		label: 'Nr. Inventar', editor: new Y.TextCellEditor({on : {keyup: saveOnEnter}, showToolbar: false})},
   			{ key: 'pif_date', 			label: 'Data PIF', editor: new Y.DateCellEditor({dateFormat: '%Y-%m-%d'})},
   			{ key: 'reception', 		label: 'Receptie', editor: new Y.DateCellEditor({dateFormat: '%Y-%m-%d'})}
   			
       ];
        		
   		// INVOICE STORE LINE TABLE INIT
   		dataTableSplit = new Y.DataTable({
   		    columns: nestedColsSplit,
   		    data: remoteDataSplit,
   		 	scrollable: "x",
	        width: "100%",
   		    editEvent: 'click',
   		    recordType: ['invoice_line_id', 'delete_line', 'inv_number', 'unit_price_ron', 'price_no_vat_ron', 'price_vat_ron', 'price_vat_curr', 'currency', 'article_code', 
	   		              'product_code', 'description', 'um', 'quantity', 'unit_price_curr', 'price_no_vat_curr', 'vat', 'val_cu_tva', 'id_ias', 'id_ifrs', 
		   		           'dt_ras', 'dt_ifrs', 'lot', 'clasif', 'cont_imob_in_fct', 'cont_imob_in_curs', 'cont_imob_avans', 
		   		           'id_store', 'store_name', 'unit_price',  'id_department', 'associated_acc', 'id_tip_inreg', 'id_act_mf', 'id_cat_gestiune', 'id_dpi', 'vizualizare_dpi', 
		   		           'id_tip_inventar', 'initiative', 'new_prj', 'detalii', 'inventory_no', 'pif_date', 'reception']
   		}).render('#alocare-magazine');
   		
//=============================================================================================================================================================
//																				TABLESPLIT.DELEGATE
//=============================================================================================================================================================			

	
   		//recalculate val no vat when the quantity changes in invoice_store_line
   		dataTableSplit.after('record:change', function (e) { 
			var obj = e.target.changed;
			var existingData = e.target._state.data;
			//change associated account based on reg type and mf act
			if (typeof obj.id_act_mf != 'undefined' || typeof obj.id_tip_inreg != 'undefined') {
				setAssociatedAccountWork();
			}
			//if new project has changed
			if (typeof obj.new_prj != 'undefined') {
				resetInitiatives(existingData.delete_line.value);
			}
			//if new project has changed
			if (typeof obj.initiative != 'undefined') {
				resetNewProjects(existingData.delete_line.value);
			}
			//if the quantity has changed
			if (typeof obj.quantity != 'undefined') {
				setValNoVatStoreLineWork('<portlet:namespace />');
			}
			//it DPI was set/changed
			if (typeof obj.id_dpi != 'undefined') {
				setDPILinkWork(obj.id_dpi);
			}
			//if the price was set/changed
			if(typeof obj.price_no_vat_curr != 'undefined'){
				setValueWithVatLineWork();
				//recalculate payment value
				setPaymentValuesWork('<portlet:namespace />');
			}
			//there was a change on ias
			if (typeof obj.id_ias != 'undefined') {
				setIasDtValuesWork(obj.id_ias, '<portlet:namespace />', '<%=ajaxURL%>');
			}
			
			//there was a change on ifrs
			if (typeof obj.id_ifrs != 'undefined') {
				setIfrsDtValuesWork(obj.id_ifrs, '<portlet:namespace />', '<%=ajaxURL%>');
			}
			
			// there was a change on quantity
			if (typeof obj.quantity != 'undefined'  || typeof obj.unit_price_curr != 'undefined') {
				if ( existingData.quantity.value != '' &&  existingData.unit_price_curr.value != '' ) {
					setLineValuesWork(existingData.quantity.value, existingData.unit_price_curr.value, '<portlet:namespace />');
				}
			}
			//$('#<portlet:namespace />val_no_vat_art').autoNumeric('update', {aSep: ',', mDec: 4, vMin : -999999999, vMax : 999999999 });
			$('#<portlet:namespace />val_no_vat_aloc_linii').autoNumeric('update', {aSep: ',', mDec: 4, vMin : -999999999, vMax : 999999999 });
			$('#<portlet:namespace />difference').autoNumeric('update', {aSep: ',', mDec: 4, vMin : -999999999, vMax : 999999999 });

   		});
		
   		// click on dpi link
   		dataTableSplit.delegate('click', function (e) {
   			e.preventDefault();
   		    var target = e.target.get('id');
   		    var ids = target.replace('dpi_','').split("_");
   		    var lineid = ids[0];
   		    var dpiId = ids[1];
   		 	showDPIPopupInvoice(e.target, dpiId,'<portlet:namespace/>', '<%=ajaxURL%>', Y);
		}, '.dpilink', dataTableSplit);
   		
   		var wasClicked = [];
   		var wasClickedMajorare = [];
   		wasClicked[0] = false;
   		wasClickedMajorare[0] = false;
   		var invTypes = [];
		var invIndex = 0;
		var numarFactura = "";
   		// click for inventory number link
		dataTableSplit.delegate("click", function(e){
	        e.preventDefault();
	        // id of line, name of article and type of inventory
	        var iasId = 0;
	        var store = "";
	        var loadedInventory = "";
	        var quantity = 0;
	        var flag = false;
	        idLine = e.target.getAttribute("id").replace('line-', '');
	        $('#<portlet:namespace />idStoreLine').val(idLine);
			
	        var majorare = "";
	        // loop through table
	        var ml = dataTableSplit.data, msg = '', template = '';
		    ml.each(function (item, i) {
		    	var dataS = item.getAttrs(['invoice_line_id', 'id_store_line', 'delete_line', 'inv_number', 'unit_price_ron', 'price_no_vat_ron', 'price_vat_ron', 'price_vat_curr', 'currency', 'article_code', 
		    	 	   		              'product_code', 'description', 'um', 'quantity', 'unit_price_curr', 'price_no_vat_curr', 'vat', 'val_cu_tva', 'id_ias', 'id_ifrs', 
		    			   		           'dt_ras', 'dt_ifrs', 'lot', 'clasif', 'cont_imob_in_fct', 'cont_imob_in_curs', 'cont_imob_avans', 
		    			   		           'id_store', 'store_name', 'unit_price', 'id_department', 'associated_acc', 'id_tip_inreg', 'id_act_mf', 'id_cat_gestiune', 'id_dpi', 'vizualizare_dpi', 
		    			   		           'id_tip_inventar', 'initiative', 'new_prj', 'detalii', 'inventory_no', 'pif_date', 'reception']);
		    	if (dataS.delete_line == idLine) {
		    		var dataS_store = 0;
		    		dataS_store = dataS.id_store;
		    		if (dataS_store != "0" ){
				    	tipInv = dataS.id_tip_inventar;
				    	denArt = dataS.description;
				    	codArt = dataS.delete_line;
				    	quantity = dataS.quantity;
				    	loadedInventory = dataS.inventory;
				    	store = dataS.id_store;
				    	majorare = dataS.id_act_mf;
				    	numarFactura = dataS.invoice_line_id; // numar factura = invoice_line_id, asa trebuie
				    	iasId = dataS.id_ias;
		    		} else {
		    			alert("Atentie! Pe aceasta linie magazinul nu a fost alocat!");
		    			flag = true;
		    			return;
		    		}
		    	}
		    });
		    
		    if (loadedInventory == "" || loadedInventory == null || loadedInventory == 'null') {
			    makeAjaxToGetInventoryDetails(numarFactura, wasClicked, idLine, e, Y);
			    showInventoryOverlay(e, Y);
		    } else {
		    	var invComponents = loadedInventory.split(',');
		    	loadedInventory = invComponents[0] + ',' + invComponents[1] + ',' + invComponents[2] + ']';
		    	loadedInventory = loadedInventory.replace("]]","]");
		    	loadedInventory = loadedInventory.replace("inventory_no","nr_inv");
		    	loadedInventory = loadedInventory.replace("pif_date","data_pif");
		    	loadedInventory = loadedInventory.replace("reception","receptie");
		        var jsonEntries = jQuery.parseJSON(loadedInventory);
				dataTableInv.set('data', eval(jsonEntries));
				showInventoryOverlay(e, Y);
		    }
	    }, ".proddet", dataTableSplit);
   		
		function makeAjaxToGetInventoryDetails(idStoreLine, wasClicked, idLine, e, Y){
   			$.ajax({
				type: "POST",
				url: '<%=ajaxURL%>',
				data: 	"<portlet:namespace />action=getInventoryDetails" +
						"&<portlet:namespace />idStoreLine=" + idStoreLine,
				success: function(msgjson) {
					if (msgjson != "" && msgjson.length > 2 ) {
							wasClicked[idLine] = true;
					        var jsonEntries = jQuery.parseJSON(msgjson);
							dataTableInv.set('data', eval(jsonEntries));
						} else {
							alert("Nu exista numar de inventar pe aceasta linie");
							return;
						}
				},
				error: function(msg) {
					alert('A aparut o problema in preluarea datelor de pe server. Va rugam incercati din nou!');
				}
			});
   		}

//=============================================================================================================================================================
//																	DOCUMENT READY FUNCTIONS
//=============================================================================================================================================================			
  		
	
	
	$("#<portlet:namespace/>finvcancel").click(function () { saveInventoryOverlayWork('<portlet:namespace />','close'); });
	$("#<portlet:namespace/>salveaza-inv").click(function () { saveInventoryOverlayWork('<portlet:namespace />','save'); });

	$('#<portlet:namespace />val_vat_ron').change(function(){
		$('#<portlet:namespace />val_vat_ron').autoNumeric('update', {aSep: ',', mDec: 4, vMin : -999999999, vMax : 999999999 });
	});
	$('#<portlet:namespace />val_with_vat_ron').change(function(){
		$('#<portlet:namespace />val_with_vat_ron').autoNumeric('update', {aSep: ',', mDec: 4, vMin : -999999999, vMax : 999999999 });
	});
	
	
		// calculate total values once page loads
		$(document).ready(function() { 
			$('#<portlet:namespace />val_no_vat_ron').autoNumeric('init', {aSep: ',', mDec: 4, vMin : -999999999, vMax : 999999999 });
			$('#<portlet:namespace />val_vat_ron').autoNumeric('init', {aSep: ',', mDec: 4, vMin : -999999999, vMax : 999999999 });
			$('#<portlet:namespace />val_with_vat_ron').autoNumeric('init', {aSep: ',', mDec: 4, vMin : -999999999, vMax : 999999999 });
			$('#<portlet:namespace />val_vat_curr').autoNumeric('init', {aSep: ',', mDec: 4, vMin : -999999999, vMax : 999999999 });
			$('#<portlet:namespace />val_with_vat_curr').autoNumeric('init', {aSep: ',', mDec: 4, vMin : -999999999, vMax : 999999999 });
			//$('#<portlet:namespace />val_no_vat_art').autoNumeric('init', {aSep: ',', mDec: 4, vMin : -999999999, vMax : 999999999 });
			$('#<portlet:namespace />val_no_vat_aloc_linii').autoNumeric('init', {aSep: ',', mDec: 4, vMin : -999999999, vMax : 999999999 });
			$('#<portlet:namespace />difference').autoNumeric('init', {aSep: ',', mDec: 4, vMin : -999999999, vMax : 999999999 });
			
			//setTotalNoVat('<portlet:namespace />');
			//setTotalNoVatLines('<portlet:namespace />');
			//setPaymentValues('<portlet:namespace />');
		
			//get exchange rate on document ready
			var currency_select = $('#<portlet:namespace />currency');
			var date 			= $('#<portlet:namespace />inv_date2');
			// autocomplete the exchange rate when the currency is not RON
			currency_select.change(function(e) {
					var elementT 	= e.target;
					var elementId 	= elementT.id;
					var action 		= "getExchangeRate";
					// make ajax call
					if (currency_select.val() != "RON") {
						//if currency is not RON
						if (date.val() != ""){
							//if the invoice date is selected
							makeAjaxCallGetExchangeRate(action, date.val(), currency_select.val(), '<portlet:namespace />', '<%=ajaxURL%>');
						} else {
							//reverse the change to RON
							currency_select.val("RON");
							alert("Va rugam selectati data facturii!");
						}
					}
				});
			
				//adauga_factura.jsp : make ajax call to get all all invoices that have type = 1 and belong to the selected supplier
				var invoice_type = $('#<portlet:namespace />tip-factura');
				var supplier = $('#<portlet:namespace />vendor');
				var company = $('#<portlet:namespace />company');
				var ajaxAction = 'getBaseInvoices';
				if(invoice_type && supplier && company) {
					invoice_type.on('change',function(e){
						//if invoice type is "de corectie"
						if (invoice_type.val() == 2) {
							makeAjaxCallGetBaseInvoices('<portlet:namespace />', ajaxAction, supplier.val(), company.val(), '<%=ajaxURL%>');
						}
					});
					
					//if the supplier is changed and invoice type is "de corectie"
					supplier.on('change', function(e){
						if (supplier.val() != ""){
							//if invoice type is "de corectie"
							if (invoice_type.val() == 2) {
								makeAjaxCallGetBaseInvoices('<portlet:namespace />', ajaxAction, supplier.val(), company.val(), '<%=ajaxURL%>');
							}
						} else {
							//reset the invoice type
							invoice_type.val("");
							$('#<portlet:namespace />nr-fact-baza2').hide();
							$('#<portlet:namespace />nr-fact-baza3').hide();
						}
					});
					company.on('change', function(e){
						if (company.val() != ""){
							//save the new company
							selectedCompany = company.val();
							//set the flag to true
							companyHasChanged = true;
						}
					});
				}
			});
//=============================================================================================================================================================
//																				DELETE
//=============================================================================================================================================================			
	
		//DELETE LINE FROM DATABASE
		dataTableSplit.delegate('click', function (e) {
			e.preventDefault();
			//get target
			var elem = e.currentTarget.get('id');
			//get the id of the target
			var elementId	= elem.replace('row', '');
			if (confirm("Sunteti sigur/a ca doriti sa stergeti aceasta linie?")){
				makeAjaxCallDeleteLineWork(elementId, "deleteLine", '<portlet:namespace/>', '<%=ajaxURL%>');
			}
		}, '.deleteLineFromDatabase', dataTableSplit);


//=============================================================================================================================================================
//																		FUNCTIONS
//=============================================================================================================================================================			
	
		$(document).ready(function(e){
			setPaymentValuesWork('<portlet:namespace/>');
			setTotalNoVatLinesWork('<portlet:namespace/>');
		});
		
		function saveInventoryOverlayWork(portletId, action){
			updateInventoryOverlayWork(portletId);
			closeInventoryOverlay();
		}
		
		function updateInventoryOverlayWork(portletId) {
			remoteDataSplit = [];
				
		    // loop through table
			var ml = dataTableSplit.data, msg = '', template = '';
			ml.each(function (item, i) {
				var dataS = item.getAttrs(['invoice_line_id', 'id_store_line', 'delete_line', 'inv_number', 'unit_price_ron', 'price_no_vat_ron', 'price_vat_ron', 'price_vat_curr', 'currency', 'article_code', 
			    	 	   		              'product_code', 'description', 'um', 'quantity', 'unit_price_curr', 'price_no_vat_curr', 'vat', 'val_cu_tva', 'id_ias', 'id_ifrs', 
			    			   		           'dt_ras', 'dt_ifrs', 'lot', 'clasif', 'cont_imob_in_fct', 'cont_imob_in_curs', 'cont_imob_avans', 
			    			   		           'id_store', 'store_name', 'unit_price', 'id_department', 'associated_acc', 'id_tip_inreg', 'id_act_mf', 'id_cat_gestiune', 'id_dpi', 'vizualizare_dpi', 
			    			   		           'id_tip_inventar', 'initiative', 'new_prj', 'detalii', 'inventory_no', 'pif_date', 'reception']);
			if (codArt == dataS.delete_line) {
				dataS.inventory = JSON.stringify(dataTableInv.data.toJSON());
				}
				remoteDataSplit.push(dataS);
			}	);
			dataTableSplit.set('recordset', remoteDataSplit);
		}
	
		function resetNewProjects(deleteLineId){
	 		var rows = [];
	 		var wasChanged = false;
			var ml = dataTableSplit.data, msg = '', template = '';
		    ml.each(function (item, i) {
		    	var dataS = item.getAttrs(['invoice_line_id', 'id_store_line', 'delete_line', 'inv_number', 'unit_price_ron', 'price_no_vat_ron', 'price_vat_ron', 'price_vat_curr', 'currency', 'article_code', 
		    	 	   		              'product_code', 'description', 'um', 'quantity', 'unit_price_curr', 'price_no_vat_curr', 'vat', 'val_cu_tva', 'id_ias', 'id_ifrs', 
		    			   		           'dt_ras', 'dt_ifrs', 'lot', 'clasif', 'cont_imob_in_fct', 'cont_imob_in_curs', 'cont_imob_avans', 
		    			   		           'id_store', 'store_name', 'unit_price', 'id_department', 'associated_acc', 'id_tip_inreg', 'id_act_mf', 'id_cat_gestiune', 'id_dpi', 'vizualizare_dpi', 
		    			   		           'id_tip_inventar', 'initiative', 'new_prj', 'detalii', 'inventory_no', 'pif_date', 'reception']);
		    	if (dataS.delete_line == deleteLineId) {
		    		dataS.new_prj = 0;
		    		wasChanged = true;
		    	}
		    	rows.push(dataS);
		    });
		    if(wasChanged) {
		    	dataTableSplit.set('recordset', rows);
			}
	 	}
	 	
	 	function resetInitiatives(deleteLineId){
	 		var rows = [];
	 		var wasChanged = false;
	 		var ml = dataTableSplit.data, msg = '', template = '';
		    ml.each(function (item, i) {
		    	var dataS = item.getAttrs(['invoice_line_id', 'id_store_line', 'delete_line', 'inv_number', 'unit_price_ron', 'price_no_vat_ron', 'price_vat_ron', 'price_vat_curr', 'currency', 'article_code', 
		    	 	   		              'product_code', 'description', 'um', 'quantity', 'unit_price_curr', 'price_no_vat_curr', 'vat', 'val_cu_tva', 'id_ias', 'id_ifrs', 
		    			   		           'dt_ras', 'dt_ifrs', 'lot', 'clasif', 'cont_imob_in_fct', 'cont_imob_in_curs', 'cont_imob_avans', 
		    			   		           'id_store', 'store_name', 'unit_price', 'id_department', 'associated_acc', 'id_tip_inreg', 'id_act_mf', 'id_cat_gestiune', 'id_dpi', 'vizualizare_dpi', 
		    			   		           'id_tip_inventar', 'initiative', 'new_prj', 'detalii', 'inventory_no', 'pif_date', 'reception']);
		    	if (dataS.delete_line == deleteLineId) {
		    		dataS.initiative = 0;
		    		wasChanged = true;
		    	}
		    	rows.push(dataS);
		    });
		    if(wasChanged) {
		    	dataTableSplit.set('recordset', rows);
			}
	 	}	
	
	
		function setAssociatedAccountWork(){
			var rows = [];
			//all store lines
			var ml = dataTableSplit.data, msg = '', template = '';
			
			ml.each(function (item, i) {
				//for each store line
				var data = item.getAttrs(['invoice_line_id', 'id_store_line', 'delete_line', 'inv_number', 'unit_price_ron', 'price_no_vat_ron', 'price_vat_ron', 'price_vat_curr', 'currency', 'article_code', 
					   		              'product_code', 'description', 'um', 'quantity', 'unit_price_curr', 'price_no_vat_curr', 'vat', 'val_cu_tva', 'id_ias', 'id_ifrs', 
						   		           'dt_ras', 'dt_ifrs', 'lot', 'clasif', 'cont_imob_in_fct', 'cont_imob_in_curs', 'cont_imob_avans', 
						   		           'id_store', 'store_name', 'unit_price', 'id_department', 'associated_acc', 'id_tip_inreg', 'id_act_mf', 'id_cat_gestiune', 'id_dpi', 'vizualizare_dpi', 
						   		           'id_tip_inventar', 'initiative', 'new_prj', 'detalii', 'inventory_no', 'pif_date', 'reception']);
				if (data.id_act_mf == '4' ){
					// tipul de actiune mf este "avans"
					data.associated_acc = data.cont_imob_avans;
				} else if (data.id_tip_inreg == '1' || data.id_tip_inreg == '3'){
					//tipul de inregistrare este "obiect de inventar in curs" sau "mijloc fix in curs"
					data.associated_acc = data.cont_imob_in_curs;
				} else { 
					//tipul de inregistrare este "obiect de inventar in functiune" sau "mijloc fix in functiune"
					data.associated_acc = data.cont_imob_in_fct;
				}
				// set value on matrix
				rows.push(data);
			});
			// set value on table
			dataTableSplit.set('recordset', rows);
		}
		
		function setTotalNoVatLinesWork(portletId){
			var ml = dataTableSplit.data, msg = '', template = '';
			var sum = 0;	
			//retrieve the currency
			var invoice_currency = $('#' + portletId + 'currency').val();
			//retrive the sum of all store lines 
			var invoice_value = 0;
			if(invoice_currency == 'RON') {
				invoice_value = $('#' + portletId + 'val_no_vat_ron');
			} else {
				invoice_value = $('#' + portletId + 'val_no_vat_curr');
			}
			//reset value
			if($('#' + portletId + 'val_no_vat_aloc_linii')){
				$('#' + portletId + 'val_no_vat_aloc_linii').val("");
		    }
			ml.each(function (item, i) {
		    	var data = item.getAttrs(['price_no_vat_curr']);
		    	sum = parseFloat(parseFloat(sum) + parseFloat(data.price_no_vat_curr)).toFixed(4);
		    });
			//total sum no vat
			if (invoice_currency != "RON"){
				var total = $('#' + portletId + 'val_no_vat_curr').val().replace(',','');
			} else {
				var total = $('#' + portletId + 'val_no_vat_ron').val().replace(',','');
			}
			var difference = $('#' + portletId + 'difference');
			//set difference
			if(difference){
				var dif = parseFloat(parseFloat(total) - parseFloat(sum)).toFixed(4);
				difference.val(dif);
				if(dif != 0 ){
					difference.css("border-color", "#FE0000");
				} else {
					difference.css("border-color", "#488F06");
				}
				
			}
		    if($('#' + portletId + 'val_no_vat_aloc_linii')){
		    	$('#' + portletId + 'val_no_vat_aloc_linii').val(sum);
		    	if(parseFloat(invoice_value.val().replace(toReplace,'')) != parseFloat(sum)){
		    		$('#' + portletId + 'val_no_vat_aloc_linii').css("border-color", "#FE0000");
		    		$('#' + portletId + 'val_no_vat_ron').css("border-color", "#FE0000");
		    		if (invoice_currency != "RON"){
		    			$('#' + portletId + 'val_no_vat_curr').css("border-color", "#FE0000");
		    		}
		    	} else {
		    		$('#' + portletId + 'val_no_vat_aloc_linii').css("border-color", "#488F06");
		    		$('#' + portletId + 'val_no_vat_ron').css("border-color", "#488F06");
		    		if (invoice_currency != "RON"){
		    			$('#' + portletId + 'val_no_vat_curr').css("border-color", "#488F06");
		    		}
		    	}
		    }
			$('#' + portletId + 'difference').autoNumeric('update', {aSep: ',', mDec: 4, vMin : -999999999, vMax : 999999999 });
			$('#' + portletId + 'val_no_vat_aloc_linii').autoNumeric('update', {aSep: ',', mDec: 4, vMin : -999999999, vMax : 999999999 });
		}
		
		function setValNoVatStoreLineWork(portletId){
			var rows = [];
			//all store lines
			var ml = dataTableSplit.data, msg = '', template = '';
			//all lines
			var mlh = dataTableSplit.data;
			var error_line = 0;
			ml.each(function (item, i) {
				//for each store line
		    	var data = item.getAttrs(['invoice_line_id', 'id_store_line', 'delete_line', 'inv_number', 'unit_price_ron', 'price_no_vat_ron', 'price_vat_ron', 'price_vat_curr', 'currency', 'article_code', 
					   		              'product_code', 'description', 'um', 'quantity', 'unit_price_curr', 'price_no_vat_curr', 'vat', 'val_cu_tva', 'id_ias', 'id_ifrs', 
						   		           'dt_ras', 'dt_ifrs', 'lot', 'clasif', 'cont_imob_in_fct', 'cont_imob_in_curs', 'cont_imob_avans', 
						   		           'id_store', 'store_name', 'unit_price', 'id_department', 'associated_acc', 'id_tip_inreg', 'id_act_mf', 'id_cat_gestiune', 'id_dpi', 'vizualizare_dpi', 
						   		           'id_tip_inventar', 'initiative', 'new_prj', 'detalii', 'inventory_no', 'pif_date', 'reception']);
		    	var unit_price = 0; 
		    	//find  the line for each store line
				mlh.each(function (itemH,j){
		    		var dataH = itemH.getAttrs(['line','unit_price_curr','quantity']);
		    		if(data.nr == dataH.line){
		    			//get the modified unit price from the invoice line
		    			unit_price = dataH.unit_price_curr;
		    			//cant = dataH.quantity;
		    			//line = dataH.line;
		    			return false;
		    		}
		    	});
		    	//for each invoice store line recalculate price no vat with the new unit price
				if(unit_price > 0){
					data.price_no_vat_curr = parseFloat(parseFloat(data.quantity) * parseFloat(unit_price)).toFixed(4);
				} else {
					//for prorata or discount
					data.price_no_vat_curr = parseFloat(unit_price).toFixed(4);
				}
		    	// set value on matrix
		    	rows.push(data);
			});	
			// set value on table
		    dataTableSplit.set('recordset', rows);
			//recalculate total no vat for all store lines
		   	setTotalNoVatLinesWork(portletId);
		}
		
		function setDPILinkWork(dpiId) {
			var rows = [];
			//all store lines
			var ml = dataTableSplit.data, msg = '', template = '';
			ml.each(function (item, i) {
				//for each store line
				var data = item.getAttrs(['invoice_line_id', 'id_store_line', 'delete_line', 'inv_number', 'unit_price_ron', 'price_no_vat_ron', 'price_vat_ron', 'price_vat_curr', 'currency', 'article_code', 
					   		              'product_code', 'description', 'um', 'quantity', 'unit_price_curr', 'price_no_vat_curr', 'vat', 'val_cu_tva', 'id_ias', 'id_ifrs', 
						   		           'dt_ras', 'dt_ifrs', 'lot', 'clasif', 'cont_imob_in_fct', 'cont_imob_in_curs', 'cont_imob_avans', 
						   		           'id_store', 'store_name', 'unit_price', 'id_department', 'associated_acc', 'id_tip_inreg', 'id_act_mf', 'id_cat_gestiune', 'id_dpi', 'vizualizare_dpi', 
						   		           'id_tip_inventar', 'initiative', 'new_prj', 'detalii', 'inventory_no', 'pif_date', 'reception']);
				if (data.id_dpi == dpiId) {
					data.vizualizare_dpi = dpiId;
				}
				// set value on matrix
				rows.push(data);
			});
			// set value on table
			dataTableSplit.set('recordset', rows);
		}
		
		function setValueWithVatLineWork(){
			var rows = [];
			var ml = dataTableSplit.data, msg = '', template = '';
			ml.each(function (item, i) {
				var data = item.getAttrs(['invoice_line_id', 'id_store_line', 'delete_line', 'inv_number', 'unit_price_ron', 'price_no_vat_ron', 'price_vat_ron', 'price_vat_curr', 'currency', 'article_code', 
					   		              'product_code', 'description', 'um', 'quantity', 'unit_price_curr', 'price_no_vat_curr', 'vat', 'val_cu_tva', 'id_ias', 'id_ifrs', 
						   		           'dt_ras', 'dt_ifrs', 'lot', 'clasif', 'cont_imob_in_fct', 'cont_imob_in_curs', 'cont_imob_avans', 
						   		           'id_store', 'store_name', 'unit_price','id_department', 'associated_acc', 'id_tip_inreg', 'id_act_mf', 'id_cat_gestiune', 'id_dpi', 'vizualizare_dpi', 
						   		           'id_tip_inventar', 'initiative', 'new_prj', 'detalii', 'inventory_no', 'pif_date', 'reception']);
				data.val_cu_tva = parseFloat(parseFloat(data.price_no_vat_curr) + parseFloat(data.price_no_vat_curr) * parseFloat(data.vat) / 100).toFixed(4);
				// set value on matrix
				rows.push(data);
			});
			// set value on table
		    dataTableSplit.set('recordset', rows);
		}
		
		function setIasDtValuesWork(iasId, portletNamespace, ajaxURL){
			$.ajax({
				type: "POST",
				url: ajaxURL,
				data: 	portletNamespace + "action=getIasInfo" +
					"&" + portletNamespace + "iasId=" + iasId,
				success: function(msgjson) {
					// get table data
					if (msgjson != ""){
			   			var rows = [];
					    var ml = dataTableSplit.data, msg = '', template = '';
					    
					    ml.each(function (item, i) {
					    	var data = item.getAttrs(['invoice_line_id', 'id_store_line', 'delete_line', 'inv_number', 'unit_price_ron', 'price_no_vat_ron', 'price_vat_ron', 'price_vat_curr', 'currency', 'article_code', 
								   		              'product_code', 'description', 'um', 'quantity', 'unit_price_curr', 'price_no_vat_curr', 'vat', 'val_cu_tva', 'id_ias', 'id_ifrs', 
									   		           'dt_ras', 'dt_ifrs', 'lot', 'clasif', 'cont_imob_in_fct', 'cont_imob_in_curs', 'cont_imob_avans', 
									   		           'id_store', 'store_name', 'unit_price', 'id_department', 'associated_acc', 'id_tip_inreg', 'id_act_mf', 'id_cat_gestiune', 'id_dpi', 'vizualizare_dpi', 
									   		           'id_tip_inventar', 'initiative', 'new_prj', 'detalii', 'inventory_no', 'pif_date', 'reception']);
							
					        if (data.id_ias == iasId) {
								var jsonEntries = jQuery.parseJSON(msgjson);
								var product = jsonEntries.product;
					        	data.dt_ras = product[0].durata_implicita;
					        	data.id_ias = product[0].id;
					        	data.cont_imob_in_fct = product[0].cont_imob_in_fct;
					        	data.cont_imob_in_curs = product[0].cont_imob_in_curs;
					        	data.cont_imob_avans = product[0].cont_imob_avans;
					        	updateContInvoices(data.cont_imob_in_curs, data.product_code);
					        }
				        	rows.push(data);
						});
					    
					    dataTableSplit.set('recordset', rows);
					} else {
						alert('IAS-ul ales nu exista in baza de date!');
					}
				},
				error: function(msg) {
					alert('A aparut o problema in preluarea datelor de pe server. Va rugam incercati din nou!');
				}
			});
		}
		
		function setIfrsDtValuesWork(ifrsId, portletNamespace, ajaxURL){
			$.ajax({
				type: "POST",
				url: ajaxURL,
				data: 	portletNamespace + "action=getIfrsInfo" +
					"&" + portletNamespace + "ifrsId=" + ifrsId,
				success: function(msgjson) {
					// get table data
					if (msgjson != ""){
			   			var rows = [];
					    var ml = dataTableSplit.data, msg = '', template = '';
					    
					    ml.each(function (item, i) {
					    	var data = item.getAttrs(['invoice_line_id',  'id_store_line', 'delete_line', 'inv_number', 'unit_price_ron', 'price_no_vat_ron', 'price_vat_ron', 'price_vat_curr', 'currency', 'article_code', 
								   		              'product_code', 'description', 'um', 'quantity', 'unit_price_curr', 'price_no_vat_curr', 'vat', 'val_cu_tva', 'id_ias', 'id_ifrs', 
									   		           'dt_ras', 'dt_ifrs', 'lot', 'clasif', 'cont_imob_in_fct', 'cont_imob_in_curs', 'cont_imob_avans', 
									   		           'id_store', 'store_name', 'unit_price', 'id_department', 'associated_acc', 'id_tip_inreg', 'id_act_mf', 'id_cat_gestiune', 'id_dpi', 'vizualizare_dpi', 
									   		           'id_tip_inventar', 'initiative', 'new_prj', 'detalii', 'inventory_no', 'pif_date', 'reception']);
							
					        if (data.id_ifrs == ifrsId) {
								var jsonEntries = jQuery.parseJSON(msgjson);
								var product = jsonEntries.product;
					        	data.dt_ifrs = product[0].durata_implicita;
					        	data.id_ifrs = product[0].id;
					        }
				        	rows.push(data);
						});
					    
					    dataTableSplit.set('recordset', rows);
					} else {
						alert('IFRS-ul ales nu exista in baza de date!');
					}
				},
				error: function(msg) {
					alert('A aparut o problema in preluarea datelor de pe server. Va rugam incercati din nou!');
				}
			});
		}
		
		function setLineValuesWork(quantity, pricePerUnit, portletId) {
			if (pricePerUnit != '' && quantity != '') {
				var rows = [];
			    var ml = dataTableSplit.data, msg = '', template = '';
			    ml.each(function (item, i) {
			    	var data = item.getAttrs(['invoice_line_id', 'id_store_line', 'delete_line', 'inv_number', 'unit_price_ron', 'price_no_vat_ron', 'price_vat_ron', 'price_vat_curr', 'currency', 'article_code', 
						   		              'product_code', 'description', 'um', 'quantity', 'unit_price_curr', 'price_no_vat_curr', 'vat', 'val_cu_tva', 'id_ias', 'id_ifrs', 
							   		           'dt_ras', 'dt_ifrs', 'lot', 'clasif', 'cont_imob_in_fct', 'cont_imob_in_curs', 'cont_imob_avans', 
							   		           'id_store', 'store_name', 'unit_price', 'id_department', 'associated_acc', 'id_tip_inreg', 'id_act_mf', 'id_cat_gestiune', 'id_dpi', 'vizualizare_dpi', 
							   		           'id_tip_inventar', 'initiative', 'new_prj', 'detalii', 'inventory_no', 'pif_date', 'reception']);
			        if (data.quantity == quantity && data.unit_price_curr == pricePerUnit) {
			        	data.price_no_vat_curr = parseFloat(parseFloat(quantity) * parseFloat(pricePerUnit)).toFixed(4);
				        data.val_cu_tva = parseFloat(parseFloat(data.price_no_vat_curr) + parseFloat(quantity) * parseFloat(pricePerUnit) * parseFloat(data.vat) / 100).toFixed(4);
				        if(data.lot_warranty == "Y") {
			        		data.warranty = $('#' + portletId + 'procent_garantie').val() / 100 * data.price_no_vat_curr;
			        	} else {
			        		data.warranty = 0;
			        	}
			        }
		        	// set value on matrix
		        	rows.push(data);

			    });
			}
			// set value on table
		    dataTableSplit.set('recordset', rows);
		}
		
		function setPaymentValuesWork(portletId) {
			var sum = 0.0000;
			var ml = dataTableSplit.data, msg = '', template = '';
			//retrieve the exchange rate
			var exchange = $('#' + portletId + 'rata-schimb').val();
			//retrieve the currency
			var currency = $('#' + portletId + 'currency').val();
			//retrieve the ron value with vat
			var	valueRON = $('#' + portletId + 'val_with_vat_ron').val().replace(toReplace, "");
			//calculate the payment value ron
			var paymentRON = parseFloat(parseFloat(valueRON));
			
		    ml.each(function (item, i) {
		    	var data = item.getAttrs(['invoice_line_id', 'id_store_line', 'delete_line', 'inv_number', 'unit_price_ron', 'price_no_vat_ron', 'price_vat_ron', 'price_vat_curr', 'currency', 'article_code', 
					   		              'product_code', 'description', 'um', 'quantity', 'unit_price_curr', 'price_no_vat_curr', 'vat', 'val_cu_tva', 'id_ias', 'id_ifrs', 
						   		           'dt_ras', 'dt_ifrs', 'lot', 'clasif', 'cont_imob_in_fct', 'cont_imob_in_curs', 'cont_imob_avans', 
						   		           'id_store', 'store_name', 'unit_price', 'id_department', 'associated_acc', 'id_tip_inreg', 'id_act_mf', 'id_cat_gestiune', 'id_dpi', 'vizualizare_dpi', 
						   		           'id_tip_inventar', 'initiative', 'new_prj', 'detalii', 'inventory_no', 'pif_date', 'reception']);
		    	//adaugare valoare cu tva
		    	sum = sum + parseFloat(data.val_cu_tva);
		    });
		   
		    if(currency != 'RON') {
		    	//set the payment value currency
		    	$('#' + portletId + 'val-netaplata-eur').val(sum.toFixed(4));
		        $('#' + portletId + 'val-netaplata-eur').autoNumeric('update', {aSep: ',', mDec: 4, vMin : -999999999, vMax : 999999999 });
		      //set the payment value ron
		        $('#' + portletId + 'val-netaplata-ron').val((sum*exchange).toFixed(4));
		    	$('#' + portletId + 'val-netaplata-ron').autoNumeric('update', {aSep: ',', mDec: 4, vMin : -999999999, vMax : 999999999 });
		    } else {
		    	 //set the payment value ron
		        $('#' + portletId + 'val-netaplata-ron').val(sum.toFixed(4));
		    	$('#' + portletId + 'val-netaplata-ron').autoNumeric('update', {aSep: ',', mDec: 4, vMin : -999999999, vMax : 999999999 });
		    }
		}
		
		function makeAjaxCallDeleteLineWork(lineID, action, portletNamespace, ajaxURL){
			var ml = dataTableSplit.data, msg = '', template = '';
		    var localArrayPosition = 0;
		    ml.each(function (item, i) {
		    	var data = item.getAttrs(['invoice_line_id']);
		    	if (lineID == data.invoice_line_id){
		    		localArrayPosition = i;
		    		return;
		    	}
		    	
		    });
			$.ajax({
				type: "POST",
				url: ajaxURL,
				data: 	portletNamespace + "action=" + action +  
				"&" + portletNamespace + "line_id=" +  lineID,
				success: function(msg) {
					// get table data
					if (msg != ""){
						var jsonEntries = jQuery.parseJSON(msg);
						var error		= jsonEntries.error;
						if(error != 0){
							//remove row from local line array
							dataTableSplit.removeRow(localArrayPosition);
							//calculate sum of line
							setTotalNoVatLinesWork(portletNamespace);
							//recalculate payment values
							setPaymentValuesWork(portletNamespace);
						    alert("Linia a fost stearsa cu succes!");
						} else {
							alert("A aparut o eroare in procesul de stergere  !");
						}
					}
					}
			});
			//$('#' + portletNamespace + 'val_no_vat_art').autoNumeric('update', {aSep: ',', mDec: 4, vMin : -999999999, vMax : 999999999 });
			$('#' + portletNamespace + 'val_no_vat_aloc_linii').autoNumeric('update', {aSep: ',', mDec: 4, vMin : -999999999, vMax : 999999999 });
			$('#' + portletNamespace + 'difference').autoNumeric('update', {aSep: ',', mDec: 4, vMin : -999999999, vMax : 999999999 });
		}
		
		function submitWorkForm(e, portletNamespace){
			//daca nu setata data scadenta
			if($('#' + portletNamespace + 'datascadenta').val()== null 
					|| $('#' + portletNamespace + 'datascadenta').val() == ""
					||  $('#' + portletNamespace + 'datascadenta').val() == "NaN-NaN-NaN") {
				alert("Va rugam completati data scadenta!");
				$('#' + portletNamespace + 'datascadenta').focus();
				return;
			}
			
			var hasKontan2 = true;
			if($('#' + portletNamespace + 'kontan_acc2').val()== "" || $('#' + portletNamespace + 'kontan_acc2').val()== null){
				hasKontan2 = false;
			}
			
			var hasKontan7 = true;
			if($('#' + portletNamespace + 'kontan_acc7').val()== "" || $('#' + portletNamespace + 'kontan_acc7').val()== null){
				hasKontan7 = false;
			}
			
			var hasKontan8 = true;
			if($('#' + portletNamespace + 'kontan_acc8').val()== "" || $('#' + portletNamespace + 'kontan_acc8').val()== null){
				hasKontan8 = false;
			}
			
			//daca nu e completat contul kontan
			if(!hasKontan2 && !hasKontan7 && !hasKontan8){
				alert("Va rugam completati contul Kontan!");
				return;
			}
			
			if (checkAccountLength()){
				return;
			}
			
			if (checkAislesOnStoreLines(portletNamespace)){
				return;
			}
			
			var mlA = dataTableSplit.data;
			var jsonA = mlA.toJSON();
			var stringDataA = JSON.stringify(jsonA);
			$('#' + portletNamespace + 'datatable_lines').val(stringDataA);
			$('#' + portletNamespace + 'ADD-invoice').append('<input type="submit" name="submit-add-invoice" id="submit-add-invoice"></input>');
			$('#' + portletNamespace + 'ADD-invoice').submit();
			setTimeout(function() { $('#submit-add-invoice').remove(); }, 200);
		}
		
//=============================================================================================================================================================
//																			SUBMIT 
//=============================================================================================================================================================			
		// bind the submit button on form
		Y.on('click', function(e){
			e.preventDefault();
			submitWorkForm(e, '<portlet:namespace/>');
		},'.validate_inv');
	}
);
</script>