<%@page import="com.profluo.ecm.model.vo.DefStoreVo"%>
<%@page import="com.profluo.ecm.model.db.WorkEquipmentHeader"%>
<%@page import="com.profluo.ecm.flows.SupplierStages"%>
<%@page import="com.profluo.ecm.model.db.DatabaseConnectionManager"%>
<%@page import="com.profluo.ecm.model.db.DefSupplier"%>
<%@page import="com.profluo.ecm.model.db.DefExchRates"%>
<%@page import="com.profluo.ecm.controller.ControllerUtils"%>
<%@page import="com.profluo.ecm.model.vo.DefNewProjVo"%>
<%@page import="com.profluo.ecm.model.vo.ListaInitVo"%>
<%@page import="com.profluo.ecm.model.db.InvoiceHeader"%>
<%@page import="com.profluo.ecm.model.vo.DefAttDocsVo"%>
<%@page import="com.profluo.ecm.model.vo.DefVATVo"%>
<%@page import="com.profluo.ecm.model.vo.DefRelComerVo"%>
<%@page import="com.profluo.ecm.model.vo.DefTipFactVo"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.List"%>
<%@page import="com.profluo.ecm.model.vo.DefCurrencyVo"%>
<%@page import="com.profluo.ecm.model.vo.DefCompaniesVo"%>
<%@page import="com.liferay.portal.util.PortalUtil"%>

<%@page import="com.profluo.ecm.flows.Links"%>

<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme"%>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui"%>
<%@ taglib uri="http://alloy.liferay.com/tld/aui" prefix="aui"%>
<%@ taglib uri="http://liferay.com/tld/util" prefix="liferay-util"%>

<%@ page import="java.util.ResourceBundle"%>
<%@ page import="java.util.Locale"%>

<liferay-theme:defineObjects />
<portlet:defineObjects />

<%
	// USED FOR LOCALIZATION
	locale 					= renderRequest.getLocale();
	ResourceBundle resmain 	= portletConfig.getResourceBundle(locale);
	
	// RETRIEVE NR FACT AND ID IN CASE THOSE ARE SENT BY REQUEST
	String nrFactura 	= "";
	String id			= "";
	String whatPage     = "";
	boolean isFromLipsaDoc = false;
	//used for details_capex.jsp include
	String status 		= "";
	try {
		id				= renderRequest.getParameter("id").toString();
	} catch (Exception e){}
	
	try {
		whatPage		= renderRequest.getAttribute("pagina").toString();
	} catch (Exception e){ }
	
 	if ( (id.equals("") || id == null ) ) {
 		String url = PortalUtil.getCurrentCompleteURL(request);
 		String [] params = url.split("&");
 		String [] lastParameter = params[params.length-1].split("=");
 		if (lastParameter[0].equals("id")){
 			id = lastParameter[1];
 		}
 	}
	
	System.out.println(" ID : " + id);
	System.out.println("whatPage -> " + whatPage);
	
	// retrieve all companies from DB
	List<HashMap<String,Object>> allCompanies = DefCompaniesVo.getInstance();
	// retrieve all currencies from DB
	List<HashMap<String,Object>> allCurrencies = DefCurrencyVo.getInstance();
	// retrieve all invoice types from DB
	List<HashMap<String,Object>> allInvoiceTypes = DefTipFactVo.getInstance();
	// retrieve commercial relations from the database
	List<HashMap<String,Object>> allRelTypes = DefRelComerVo.getInstance();
	//retrieve all vat values
	List<HashMap<String,Object>> allVat = DefVATVo.getInstance();
	//retrieve all types of missing docs
	List<HashMap<String,Object>> allDocs = DefAttDocsVo.getInstance();
	
	//get invoice header by id
	List<HashMap<String,Object>> oneWorkEquipment 				= null;
	//get supplier info
	List<HashMap<String,Object>> supplierInfo 					= null;
	//get all invocie lines bu invoice header id
	List<HashMap<String,Object>> workEquipmentLinesByWorkEqNr 		= null;
	List<HashMap<String,Object>> workEquipmentStoreLinesByWorkEqNr 	= null;
	String vat = "";
	String stage = "";
	String currency = "";
	// used for store lines - retrieve it in case we can get it from invoice GLN, otherwise leave it to 236 - SEDIU CARREFOUR - 9150
	//int storeId = 326;
			
	String tip_capex = "0";
	String initiativa = "0";
	String prj = "0";
	int storeId = 326;
	if (!id.equals("")) {
		oneWorkEquipment 					= WorkEquipmentHeader.getHeaderById(id, "working_equipment_header");
		workEquipmentStoreLinesByWorkEqNr	= WorkEquipmentHeader.getStoreLinesById(id, "working_equipment_store_line");
		supplierInfo 						= DefSupplier.getSupplierInfo(oneWorkEquipment.get(0).get("id_supplier").toString());
		workEquipmentLinesByWorkEqNr 		= WorkEquipmentHeader.getLinesById(id, "working_equipment_line");
		currency = oneWorkEquipment.get(0).get("currency").toString();
		float valCuTva = Float.parseFloat(oneWorkEquipment.get(0).get("total_with_vat_ron").toString());
		float valFaraTva = Float.parseFloat(oneWorkEquipment.get(0).get("total_no_vat_ron").toString());
		vat = String.format("%.2f", (float)(Math.round(((valCuTva - valFaraTva) / valFaraTva) * 100)));
		//for comparing strings
		
		//get current stage
		stage = oneWorkEquipment.get(0).get("stage").toString();
		// get the store ID based on GLN on invoice header	
		if (oneWorkEquipment.get(0).get("GLN") != null) { storeId = DefStoreVo.getStoreIdbyGLN(oneWorkEquipment.get(0).get("GLN").toString(),oneWorkEquipment.get(0).get("id_company")); }
		try{ tip_capex = oneWorkEquipment.get(0).get("tip_capex").toString(); } catch (Exception e){}
		try{ initiativa = oneWorkEquipment.get(0).get("id_initiative").toString(); } catch (Exception e){}
		try{ prj = oneWorkEquipment.get(0).get("id_new_prj").toString(); } catch (Exception e){}
	}
	
	System.out.println("workEquipmentStoreLinesByWorkEqNr -> " + workEquipmentStoreLinesByWorkEqNr);
%>
<%-- Links used in the view --%>
<%--Trimite factura in Ch Gen : delete invoice from system --%>
<portlet:actionURL name="deleteInvoice" var="deleteInvoiceURL" />
<%-- Submit URL, mapped to the addInvoice method in the class Invoice.java --%>
<portlet:actionURL name="addInvoice" var="submitURL" />
<%-- Default portletURL --%>
<portlet:renderURL var="portletURL"></portlet:renderURL>

<portlet:resourceURL id="ajaxURL" var="ajaxURL"/>

<portlet:renderURL var="backURL">
	<portlet:param name="mvcPath" value="/html/facturi/view.jsp"></portlet:param>
</portlet:renderURL>

<portlet:actionURL var="deleteURL" name="deleteFact"/>
<portlet:actionURL name="deleteInvoice" var="deleteInvoiceURL" />

<a href="<%=portletURL%>" class="btn btn-primary-red fr" style="margin:0px">Inapoi</a>
<% if (!id.equals("")) {%>
	
	<% if(oneWorkEquipment.get(0).get("image") != null) { %>
		<a href="<%=ControllerUtils.alfrescoURLOld + oneWorkEquipment.get(0).get("image") %>" <%--+ ControllerUtils.alfrescoDirectLoginOld  --%>
		class="btn btn-primary fr" style="margin-right:10px" target="_blank">Vizualizare Factura</a>
	<% } %>
	<aui:form id="sendChGen" style="display:none" action="<%=deleteInvoiceURL%>">
		<aui:input type="hidden" name="fact_id" value="<%= id %>"/>
		<aui:input type="submit" name="update" id="update"/>
	</aui:form>
<%-- 	<a class="btn btn-primary fr" style="margin:0px" onclick="sendToChGen('<portlet:namespace />')">Trimite factura in Ch. Gen.</a> --%>
<% } %>

<div class="clear"></div>

<liferay-ui:error message="insert_nok" key="insert_nok"/>
<liferay-ui:error message="update_nok" key="update_nok"/>

<%-- Creare formular adauga factura --%>
<aui:form action="<%=submitURL%>" method="post" id="ADD-invoice" name="ADD-invoice">
	<%-- if id="" => insert else update --%>
	<aui:input name="insertORupdate" id="insertORupdate" type="hidden" value="<%=id %>"/>
	<%-- send current stage to the controller  --%>
	<aui:input name="currentStage" type="hidden" value="<%=stage%>"/>
	 
	<aui:fieldset>
		<legend><strong>Factura</strong></legend>
		<aui:layout>
			<aui:column columnWidth="10" first="true">
				<label for="<portlet:namespace />inv_number" style="padding-top: 5px"><%=resmain.getString("nr-factura")%></label>
			</aui:column>
			<% if (id.equals("")) {%>
			<aui:column columnWidth="20">
				<aui:input type="text" id="inv_number" name="inv_number" value="" label="">
				<aui:validator name="required"  errorMessage="field-required"></aui:validator>
				<aui:validator name="alphanum" errorMessage="alpha-num"></aui:validator>
				</aui:input>
			</aui:column>
			<% } else { %>
			<aui:column columnWidth="20">
				<aui:input type="text" id="inv_number" name="inv_number" value="<%=oneWorkEquipment.get(0).get(\"inv_number\") %>" label="">
				<aui:validator name="required"  errorMessage="field-required"></aui:validator>
				<aui:validator name="alphanum" errorMessage="alpha-num"></aui:validator>
				</aui:input>
			</aui:column>
			<% } %>
			<aui:column columnWidth="10">
				<label for="<portlet:namespace/>inv_date2" style="padding-top: 5px"><%=resmain.getString("datafactura")%></label>
			</aui:column>
			<% if (id.equals("")) {%>
			<aui:column columnWidth="20">
				<aui:input type="date" id="inv_date2" name="inv_date2" value="" placeholder="aaaa-ll-zz" label="">
				<aui:validator name="required"  errorMessage="field-required"></aui:validator>
				</aui:input>
			</aui:column>
			<% } else { %>
			<aui:column columnWidth="20">
				<aui:input type="date" id="inv_date2" name="inv_date2" value="<%=oneWorkEquipment.get(0).get(\"inv_date\") %>" 
				placeholder="aaaa-ll-zz" label="">
				<aui:validator name="required"  errorMessage="field-required"></aui:validator>
				</aui:input>
			</aui:column>
			<% } %>
			
			
			
			<script type="text/javascript">
				var oldInvoiceDateValue = "";
				YUI({lang:'ro'}).use('aui-datepicker', function(Y) {
					oldInvoiceDateValue = $('#<portlet:namespace />inv_date2').val();
					new Y.DatePicker({
						trigger : '#<portlet:namespace />inv_date2',
						mask : '%Y-%m-%d',
						popover : {
							zIndex : 1
						},
						calendar : {
							dateFormat : '%Y-%m-%d'
						},
						after: {
				            selectionChange: function(event) {
				            	if (oldInvoiceDateValue != $('#<portlet:namespace />inv_date2').val()) {
				            		oldInvoiceDateValue = $('#<portlet:namespace />inv_date2').val();
				            		invoiceDateChanged($('#<portlet:namespace />inv_date2').val(), '<portlet:namespace />');
				            		setDueDate('<portlet:namespace/>');
				            	}
				            }
				        }
					});
				});
			</script>
			<aui:column columnWidth="10">
				<label for="<portlet:namespace />company"
					style="padding-top: 5px"><%=resmain.getString("societate")%></label>
			</aui:column>
			<% if (id.equals("")) {%>
			<aui:column columnWidth="20" id="societate2">
				<aui:select id="company" name="company" label="" required="true">
					<aui:option selected="selected" value=""><%=resmain.getString("societate")%></aui:option>
					<% for (int i = 0; i < allCompanies.size(); i++) { %>
						<% if (allCompanies.get(i).get("status").equals("A")) {%>
							<aui:option value="<%=allCompanies.get(i).get(\"id\")%>"><%=allCompanies.get(i).get("name")%></aui:option>
						<% } %>
					<% } %>
				</aui:select>
			</aui:column>
			<% } else { %>
			<aui:column columnWidth="20" id="societate2">
				<aui:select id="company" name="company" label="" required="true">
					<% for (int i = 0; i < allCompanies.size(); i++) { %>
						<% if (allCompanies.get(i).get("status").equals("A")) { %>
							<aui:option value="<%=allCompanies.get(i).get(\"id\")%>"><%=allCompanies.get(i).get("name")%></aui:option>
						<% } %>
					<% } %>
				</aui:select>
			</aui:column>
			<% } %>
		</aui:layout>
		<%if (!id.equals("")) { %>
		<% for (int i = 0; i < allCompanies.size(); i++) { %>
			<% if (allCompanies.get(i).get("status").equals("A")) { %>
				<% if (allCompanies.get(i).get("id").toString().equals(oneWorkEquipment.get(0).get("id_company").toString())) { %>
				<script type="text/javascript">
					$(document).ready(function(){  
						$('#<portlet:namespace />company').val('<%=allCompanies.get(i).get("id")%>');
					});
				</script>
				<% } %>
			<% } %>
		<% } } %>
	</aui:fieldset>

	<%-- creare al 2-lea fieldset pentru info factura  --%>
	<aui:fieldset>
		<aui:layout>
			<aui:column columnWidth="10" first="true">
				<label for="<portlet:namespace />tip-rel-comerciala"
					style="padding-top: 5px"><%=resmain.getString("tip-rel-comerciala")%></label>
			</aui:column>
			<% if(id.equals("")) {%>
			<aui:column columnWidth="20">
				<aui:select id="tip-rel-comerciala" name="tip-rel-comerciala" label="" required="true">
					<aui:option selected="selected" value=""><%=resmain.getString("selecteaza-tipul")%></aui:option>
					<% for (int i = 0; i < allRelTypes.size(); i++) { %>
						<% if (allRelTypes.get(i).get("status").equals("A")) {%>
							<aui:option value="<%=allRelTypes.get(i).get(\"id\")%>"><%=allRelTypes.get(i).get("name")%></aui:option>
						<% } %>
					<% } %>
				</aui:select>
			</aui:column>
			<% } else { %>
			<aui:column columnWidth="20">
				<aui:select id="tip-rel-comerciala" name="tip-rel-comerciala" label="" required="true">
					<% for (int i = 0; i < allRelTypes.size(); i++) { %>
						<% if (allRelTypes.get(i).get("status").equals("A")) {%>
							<% if ( (oneWorkEquipment.get(0).get("tip_rel_com") != null) && 
									allRelTypes.get(i).get("id").toString().equals(oneWorkEquipment.get(0).get("tip_rel_com").toString())) {%>
								<aui:option selected="selected" value='<%=allRelTypes.get(i).get("id")%>'>'<%=allRelTypes.get(i).get("name")%>'</aui:option>
							<% } else { %>
								<aui:option value="<%=allRelTypes.get(i).get(\"id\")%>"><%=allRelTypes.get(i).get("name")%></aui:option>
							<% } %>
						<% } %>
					<% } %>
				</aui:select>
			</aui:column>
			<% } %>
			
			<script type="text/javascript">
				$(document).ready(function(){
					$('#<portlet:namespace/>tip-rel-comerciala').val(<%=oneWorkEquipment.get(0).get("tip_rel_com").toString()%>);
				});
			</script>
			
			<aui:column columnWidth="10" id = "label_contract_display">
				<label for="<portlet:namespace />nr-contract" style="padding-top: 5px"><%=resmain.getString("nr-contract")%></label>
			</aui:column>
			<% if(id.equals("")) {%>
			<aui:column columnWidth="20" id="numar_contract_display">
				<aui:input id="nr-contract" name="nr-contract" label="" value="0">
				<aui:validator name="required"  errorMessage="field-required"></aui:validator>
				</aui:input>
			</aui:column>
			<% } else { %>
				<aui:column columnWidth="20" id="numar_contract_display">
				<aui:input id="nr-contract" name="nr-contract" label="" value = "<%=oneWorkEquipment.get(0).get(\"co_number\") %>">
				</aui:input>
			</aui:column>
			<% } %>
			<aui:column columnWidth="10" id="label_comanda_display">
				<label for="<portlet:namespace />nr-comanda" style="padding-top: 5px"><%=resmain.getString("nr-comanda")%></label>
			</aui:column>
			<% if(id.equals("")) {%>
			<aui:column columnWidth="20" id="numar_comanda_display">
				<aui:input id="nr-comanda" name="nr-comanda" label="" value="0">
				<aui:validator name="required"  errorMessage="field-required"></aui:validator>
				</aui:input>
			</aui:column>
			<% } else { %>
				<aui:column columnWidth="20" id="numar_comanda_display">
				<aui:input id="nr-comanda" name="nr-comanda" label="" value = "<%=oneWorkEquipment.get(0).get(\"order_no\") %>">
				</aui:input>
			</aui:column>
			<% } %>
		</aui:layout>
	</aui:fieldset>

	<%-- creare al 3-lea fieldset pentru info factura  --%>
	<aui:fieldset>
		<aui:layout>
			<aui:column columnWidth="10" first="true">
				<label for="<portlet:namespace />tip-factura"
					style="padding-top: 5px"><%=resmain.getString("tip-factura")%></label>
			</aui:column>
			<% if(id.equals("")) {%>
			<aui:column columnWidth="20">
				<aui:select id="tip-factura" name="tip-factura" label="" required="true">
					<aui:option selected="selected" value=""><%=resmain.getString("tip-factura")%></aui:option>
					<% for (int i = 0; i < allInvoiceTypes.size(); i++) { %>
						<% if (allInvoiceTypes.get(i).get("status").equals("A")) {%>
							<aui:option value="<%=allInvoiceTypes.get(i).get(\"id\")%>"><%=allInvoiceTypes.get(i).get("name")%></aui:option>
						<% } %>
					<% } %>
				</aui:select>
			</aui:column>
			<% } else { %>
				<aui:column columnWidth="20">
				<aui:select id="tip-factura" name="tip-factura" label="" required="true">
					<% for (int i = 0; i < allInvoiceTypes.size(); i++) { %>
						<% if (allInvoiceTypes.get(i).get("status").equals("A")) {%>
							<% if( (oneWorkEquipment.get(0).get("tip_fact") != null) &&
									allInvoiceTypes.get(i).get("id").equals(oneWorkEquipment.get(0).get("tip_fact").toString())) { %>
								<aui:option selected = "selected" value="<%=allInvoiceTypes.get(i).get(\"id\")%>"><%=allInvoiceTypes.get(i).get("name")%></aui:option>
							<% } else { %>
								<aui:option value="<%=allInvoiceTypes.get(i).get(\"id\")%>"><%=allInvoiceTypes.get(i).get("name")%></aui:option>						
							<% } %>
						<% } %>
					<% } %>
				</aui:select>
			</aui:column>
			<% } %>
			<aui:column columnWidth="10" id="nr-fact-baza2" style="display:none">
				<label for="<portlet:namespace />nr-fact-baza"
					style="padding-top: 5px"><%=resmain.getString("nr-fact-baza")%></label>
			</aui:column>
			<aui:column columnWidth="20" id="nr-fact-baza3" style="display:none">
				<aui:select id="nr-fact-baza" name="nr-fact-baza" label="">
				</aui:select>
			</aui:column>
			<aui:column columnWidth="10">
				<label for="<portlet:namespace />created"
					style="padding-top: 5px"><%=resmain.getString("datainregistrare")%></label>
			</aui:column>
			<% if(id.equals("")) { %>
			<aui:column columnWidth="20">
				<aui:input type="text" id="created" name="created" value="" placeholder="aaaa-ll-zz" label="" readonly="true">
				<aui:validator name="required"  errorMessage="field-required"></aui:validator>
				</aui:input>
			</aui:column>
			<% } else { %>
				<aui:column columnWidth="20">
				<aui:input type="text" id="created" name="created" value="<%=oneWorkEquipment.get(0).get(\"created\") %>" 
				placeholder="aaaa-ll-zz" label="" readonly="true">
				<aui:validator name="required"  errorMessage="field-required"></aui:validator>
				</aui:input>
			</aui:column>
			<% } %>
		</aui:layout>
	</aui:fieldset>
	
	<%-- al 4-lea rand din info factura --%>
	<aui:fieldset>
		<aui:layout>
			
		</aui:layout>
	</aui:fieldset>
	<%-- adaugare sectiune furnizor --%>
	<%@ include file="/html/includes/supplier.jsp" %>
	<% if (!id.equals("")) { %>
	<script type="text/javascript"> 
		$(document).ready(function(){
			$('#<portlet:namespace />id_supplier').val('<%=oneWorkEquipment.get(0).get("id_supplier").toString()%>');
			$('#<portlet:namespace />vendor').val('<%=supplierInfo.get(0).get("name")%>');
			$('#<portlet:namespace />payment_term').val('<%=supplierInfo.get(0).get("payment_term")%>');
			$('#<portlet:namespace />cui').val('<%=(supplierInfo.get(0).get("cui") == null ? "" : supplierInfo.get(0).get("cui"))%>');
			$('#<portlet:namespace />cont-asociat').val('<%=(supplierInfo.get(0).get("bank_account") == null ? "" : supplierInfo.get(0).get("bank_account"))%>');
			$('#<portlet:namespace/>supplier_code').val('<%=(supplierInfo.get(0).get("supplier_code") == null ? "" : supplierInfo.get(0).get("supplier_code"))%>');
			$('#<portlet:namespace/>supplier_data').val('<%=(supplierInfo.get(0) == null ? "" : 
				DatabaseConnectionManager.convertListToJson(supplierInfo).toJSONString())%>');
			var acc = $('#<portlet:namespace/>supplier_code').val();
			var kon_acc = <%=oneWorkEquipment.get(0).get("kontan_acc").toString()%>;
			//invoice supplier option
			
			var supplierName = '<%=supplierInfo.get(0).get("name").toString()%>';
			setAssociatedKontanAccInvoice('<portlet:namespace/>', acc, kon_acc, supplierName, false);
		});
	</script>
	<% } %>
	<%--Adaugare sectiune Info factura --%>
	<aui:fieldset>
		<legend><strong><%=resmain.getString("info-factura")%></strong></legend>
		<aui:layout>
			<aui:column columnWidth="10" first="true">
				<label for="<portlet:namespace />datascadenta" style="padding-top: 5px"><%=resmain.getString("datascadenta")%></label>
			</aui:column>
			<% if(id.equals("")) { %>
			<aui:column columnWidth="20">
				<aui:input type="text" id="datascadenta" name="datascadenta" value="" placeholder="aaaa-ll-zz" label="">
					<aui:validator name="required"  errorMessage="field-required"></aui:validator>
				</aui:input>
			</aui:column>
			<% } else { %>
				<aui:column columnWidth="20">
				<aui:input type="text" id="datascadenta" name="datascadenta" value="<%=oneWorkEquipment.get(0).get(\"due_date\") %>" placeholder="aaaa-ll-zz" label="">
					<aui:validator name="required"  errorMessage="field-required"></aui:validator>
				</aui:input>
			</aui:column>
			<% } %>
			<script type="text/javascript">
				YUI({lang: ''}).use('aui-datepicker', function(Y) {
					new Y.DatePicker({
						trigger : '#<portlet:namespace />datascadenta',
						mask : '%Y-%m-%d',
						popover : {
							zIndex : 1
						},
						calendar : {
							dateFormat : '%Y-%m-%d'
						}
					});
				});
				
				$(document).ready(function(){
					setDueDate('<portlet:namespace />');
				});
			</script>
			<aui:column columnWidth="10" first="true">
				<label for="<portlet:namespace />currency" style="padding-top: 5px"><%=resmain.getString("moneda")%></label>
			</aui:column>
			<% if (id.equals("")) {%>
			<aui:column columnWidth="20">
				<aui:select id="currency" name="currency" label="" required="true" >
				<% for (int i = 0; i < allCurrencies.size(); i++) { %>
					<% if (allCurrencies.get(i).get("status").equals("A")) {%>
						<aui:option value="<%=allCurrencies.get(i).get(\"ref\")%>"><%=allCurrencies.get(i).get("ref")%></aui:option>
					<% } %>
				<% } %>
			</aui:select>
				<script type="text/javascript">
					$(document).ready(function(){  
					<% for (int i = 0; i < allCurrencies.size(); i++) { %>
						<% if (allCurrencies.get(i).get("status").equals("A")) {%>
							<% if (allCurrencies.get(i).get("ref").equals("RON")) { %>
							$('#<portlet:namespace />currency').val('<%=allCurrencies.get(i).get("ref")%>');
						<% } %>
					<% }  }%>
					});
				</script>
			</aui:column>
			<% } else { %>
				<aui:column columnWidth="20">
				<aui:select id="currency" name="currency" label="" required="true" >
				<% for (int i = 0; i < allCurrencies.size(); i++) { %>
					<% if (allCurrencies.get(i).get("status").equals("A")) {%>
						<% if (allCurrencies.get(i).get("ref").toString().equals(oneWorkEquipment.get(0).get("currency").toString())) { %>
							<aui:option selected="selected"
								value="<%=allCurrencies.get(i).get(\"ref\")%>"><%=allCurrencies.get(i).get("ref")%></aui:option>
						<% }else { %> 
							<aui:option value="<%=allCurrencies.get(i).get(\"ref\")%>"><%=allCurrencies.get(i).get("ref")%></aui:option>
						<% } %>
					<% } %>
				<% } %>
			</aui:select>
			</aui:column>
			<% } %>
			<aui:column columnWidth="10">
				<label for="<portlet:namespace />rata-schimb"
					style="padding-top: 5px"><%=resmain.getString("rata-schimb")%></label>
			</aui:column>
			<% if (id.equals("")) { %>
			<aui:column columnWidth="20">
				<aui:input type="text" id="rata-schimb" name="rata-schimb" value="1"
					label="">
					<aui:validator name="required"  errorMessage="field-required"></aui:validator>
				</aui:input>
			</aui:column>
			<% } else { %>
				<aui:column columnWidth="20">
				<aui:input type="text" id="rata-schimb" name="rata-schimb" value="<%=oneWorkEquipment.get(0).get(\"exchange_rate\")%>"
					label="">
					<aui:validator name="required"  errorMessage="field-required"></aui:validator>
				</aui:input>
			</aui:column>
			<% } %>
		</aui:layout>
	</aui:fieldset>
	<aui:fieldset>
		<aui:column columnWidth="10" first="true">
			<label for="<portlet:namespace />cota_tva" style="padding-top: 5px"><%=resmain.getString("cota-tva")%></label>
		</aui:column>
		<% if (id.equals("")) { %>
		<aui:column columnWidth="20">
			<aui:select id="cota_tva" name="cota_tva" label="" required="true">
				<aui:option selected="selected" value=""><%=resmain.getString("cota-tva")%></aui:option>
				<% for (int i = 0; i < allVat.size(); i++) { %>
					<% if (allVat.get(i).get("status").equals("A")) {%>
						<aui:option value="<%=allVat.get(i).get(\"ref\")%>"><%=allVat.get(i).get("ref")%>%</aui:option>
					<% } %>
				<% } %>
			</aui:select>
		</aui:column>
		<%} else { %>
		<aui:column columnWidth="20">
			<aui:select id="cota_tva" name="cota_tva" label="" required="true" autocomplete="off">
				<% for (int i = 0; i < allVat.size(); i++) { %>
					<% if (allVat.get(i).get("status").equals("A")) {%>
						<% if (allVat.get(i).get("ref").toString().equals(vat)) { %>
						<aui:option selected="true" value="<%=allVat.get(i).get(\"ref\")%>"><%=allVat.get(i).get("ref")%>%</aui:option>
						<% } else { %>
						<aui:option value="<%=allVat.get(i).get(\"ref\")%>"><%=allVat.get(i).get("ref")%>%</aui:option>
						<% } %>
					<% } %>
				<% } %>
			</aui:select>
		</aui:column>
		<% } %>
		<aui:column columnWidth="20">
			<aui:input type="hidden" id="procent_garantie" name="procent_garantie" value="0"></aui:input>
		</aui:column>
	</aui:fieldset>
	
<%--calculare valoare currency --%>
<aui:fieldset id="valoareCurrency" style="display:none">
	<legend id="currency_label"></legend>
	<aui:layout>
		<aui:column columnWidth="10">
			<label for="<portlet:namespace />val_no_vat_curr"
				style="padding-top: 5px"><%=resmain.getString("val_fara_tva")%></label>
		</aui:column>
		<% if (id.equals("")) { %>
		<aui:column columnWidth="20">
			<aui:input cssClass="currencyCol" type="text" id="val_no_vat_curr" name="val_no_vat_curr" value="" label="">
			</aui:input>
		</aui:column>
		<% } else { %>
		<aui:column columnWidth="20">
			<aui:input cssClass="currencyCol" type="text" id="val_no_vat_curr" name="val_no_vat_curr" value="<%=oneWorkEquipment.get(0).get(\"total_no_vat_curr\")%>" label="">
			</aui:input>
		</aui:column>
		<% } %>
		<aui:column columnWidth="10">
			<label for="<portlet:namespace />val_vat_curr" style="padding-top: 5px"><%=resmain.getString("val_tva")%></label>
		</aui:column>
		<% if (id.equals("")) { %>
		<aui:column columnWidth="20">
			<aui:input cssClass="currencyCol" type="text" id="val_vat_curr" name="val_vat_curr" value="" label="" >
			</aui:input>
		</aui:column>
		<% } else { %>
		<aui:column columnWidth="20">
			<aui:input cssClass="currencyCol" type="text" id="val_vat_curr" name="val_vat_curr" value="<%=oneWorkEquipment.get(0).get(\"total_vat_curr\")%>" label="" >
			</aui:input>
		</aui:column>
		<% } %>
		<aui:column columnWidth="10">
			<label for="<portlet:namespace />val_with_vat_curr"
				style="padding-top: 5px"><%=resmain.getString("val_cu_tva")%></label>
		</aui:column>
		<% if (id.equals("")) { %>
		<aui:column columnWidth="20">
			<aui:input cssClass="currencyCol" type="text" id="val_with_vat_curr" name="val_with_vat_curr" value="" label="">
			</aui:input>
		</aui:column>
		<% } else { %>
		<aui:column columnWidth="20">
			<aui:input cssClass="currencyCol" type="text" id="val_with_vat_curr" name="val_with_vat_curr" value="<%=oneWorkEquipment.get(0).get(\"total_with_vat_curr\")%>" label="">
			</aui:input>
		</aui:column>
		<% } %>	
	</aui:layout>
	</aui:fieldset>

	<%--calculare valoare RON --%>
	<aui:fieldset id="valoareRON">
		<legend><%=resmain.getString("valoare-ron")%></legend>
		<aui:layout>
			<aui:column columnWidth="10">
				<label for="<portlet:namespace />val_no_vat_ron"
					style="padding-top: 5px"><%=resmain.getString("val_fara_tva")%></label>
			</aui:column>
			<% if (id.equals("")) { %>
			<aui:column columnWidth="20">
				<aui:input cssClass="currencyCol" type="text" id="val_no_vat_ron" name="val_no_vat_ron" 
					value="" label="">
					<aui:validator name="required"  errorMessage="field-required"></aui:validator>
				</aui:input>
			</aui:column>
			<% } else { %>
			<aui:column columnWidth="20">
				<aui:input cssClass="currencyCol" type="text" id="val_no_vat_ron" name="val_no_vat_ron" 
					value="<%=oneWorkEquipment.get(0).get(\"total_no_vat_ron\")%>" label="">
					<aui:validator name="required"  errorMessage="field-required"></aui:validator>
				</aui:input>
			</aui:column>
			<% } %>
			<aui:column columnWidth="10">
				<label for="<portlet:namespace />val_vat_ron" style="padding-top: 5px"><%=resmain.getString("val_tva")%></label>
			</aui:column>
			<% if (id.equals("")) { %>
			<aui:column columnWidth="20">
				<aui:input cssClass="currencyCol" type="text" id="val_vat_ron" name="val_vat_ron" value="" label="">
					<aui:validator name="required"  errorMessage="field-required"></aui:validator>
				</aui:input>
			</aui:column>
			<% } else { %>
			<aui:column columnWidth="20">
				<aui:input cssClass="currencyCol" type="text" id="val_vat_ron" name="val_vat_ron" value="<%=oneWorkEquipment.get(0).get(\"total_vat_ron\")%>" label="">
					<aui:validator name="required"  errorMessage="field-required"></aui:validator>
				</aui:input>
			</aui:column>
			<% } %>
			<aui:column columnWidth="10">
				<label for="<portlet:namespace />val_with_vat_ron"
					style="padding-top: 5px"><%=resmain.getString("val_cu_tva")%></label>
			</aui:column>
			<% if (id.equals("")) { %>
			<aui:column columnWidth="20">
				<aui:input cssClass="currencyCol" type="text" id="val_with_vat_ron" name="val_with_vat_ron" value="" label="">
					<aui:validator name="required"  errorMessage="field-required"></aui:validator>
				</aui:input>
			</aui:column>	
			<% } else { %>	
			<aui:column columnWidth="20">
				<aui:input cssClass="currencyCol" type="text" id="val_with_vat_ron" name="val_with_vat_ron" value="<%=oneWorkEquipment.get(0).get(\"total_with_vat_ron\")%>" label="">
					<aui:validator name="required"  errorMessage="field-required"></aui:validator>
				</aui:input>
			</aui:column>	
			<% } %>
		</aui:layout>
	</aui:fieldset>
	<aui:layout  style="margin-top:10px">
		<%@ include file="parts/work_alocare_factura.jsp" %>
	</aui:layout>
	<aui:layout  style="margin-top:10px">
		<%@ include file="parts/valori_factura.jsp" %>
	</aui:layout>
	<aui:input type="hidden" name="datatable_lines" id="datatable_lines" value="" />
	<% if(!isFromLipsaDoc && !whatPage.equals("raport")) { %>
		<a href="#" class="btn btn-primary fr validate_inv" style="margin-right:0px">Validare</a>
	<% } %>
	<%--<aui:button type="submit" value="Trimite form" cssClass="btn btn-primary fr" style="display:none" id="trimite-form" name="trimite-form"/>
	 <aui:button type="button" value="Trimite factura in Kontan" cssClass="btn btn-primary fr" style="margin-right:10px"/>--%>
	<div class="clear"></div>
</aui:form>

<script type="text/javascript">
//onClick function for button "Trimite in Ch Gen" 
function sendToChGen(namespace){
	if (confirm("Sunteti sigur/a ca doriti sa trimiteti factura curenta in Cheltuieli Generale?")){
		$('#' + namespace + 'update').click();
	}
}

//la click pe vizualizare document
$('#go-to-advance-request').click(function(e) {
	e.preventDefault();
	var link = "<%=Links.individualAdvanceRequestForAccounting%>";
	link = link.substring(link.indexOf("url")+4);
	link = link + "&id=" + $('#<portlet:namespace />nr-fact-avans-asoc').val();
	//console.log(link);
	//window.location = link;
	//link = "http://localhost:28080/web/capex-ii/cereri-plata-in-avans?p_p_id=validateadvanceapproval_WAR_C4Capex2portlet&p_p_lifecycle=0&p_p_state=normal&p_p_mode=view&p_p_col_id=column-1&p_p_col_count=1&_validateadvanceapproval_WAR_C4Capex2portlet_mvcPath=%2Fhtml%2Fvalidateadvanceapproval%2Fvalidate.jsp&id=" + $('#<portlet:namespace />nr-fact-avans-asoc').val();
	window.location.href = link;
});

function makeAjaxToPopulateDropdown(action, company, supplierName){
	$.ajax({
		type: "POST",
		url: "<%=ajaxURL%>",
		data: 	"<portlet:namespace />action=" + action + 
				"&<portlet:namespace />company=" + company + 
				"&<portlet:namespace />supplierName=" + supplierName,
		success: function(msg) {
			// get table data
			if (msg != ""){
				var jsonEntries = jQuery.parseJSON(msg);
				lista = jsonEntries.list;
				var dropDown = $('#<portlet:namespace />nr-fact-avans-asoc');
				dropDown.empty();
				dropDown.append('<option value="">-</option>');
				if (dropDown) {
						//append all the options resulted from the ajax call
						for ( i = 0; i < lista.length; i++){
							dropDown.append('<option value=' + lista[i].id + '>' + lista[i].id + '</option>');
						};
					}
			} else {
				
			}
		}
	});
}
</script>