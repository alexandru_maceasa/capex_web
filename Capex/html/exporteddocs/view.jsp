<%@page import="com.profluo.ecm.model.db.Administrare"%>
<%@page import="com.profluo.ecm.model.db.DefCategory"%>
<%@page import="com.profluo.ecm.model.vo.DefCurrencyVo"%>
<%@page import="com.profluo.ecm.model.vo.DefCompaniesVo"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.List"%>
<%@page import="com.profluo.ecm.model.db.InvoiceHeader"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme"%>

<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui"%>
<%@ taglib uri="http://alloy.liferay.com/tld/aui" prefix="aui"%>
<%@ taglib uri="http://liferay.com/tld/util" prefix="liferay-util"%>
 
<liferay-theme:defineObjects />
<portlet:defineObjects />

<portlet:resourceURL id="ajaxURL" var="ajaxURL"/>


<% // PAGINATION Start - env setup 
int start 			= 0; 
int count 			= 15;
int total 			= 0;
String datafactura 	= "";
String datainreg 	= "";
String supplierCode = "";
String supplierName = "";
int company_id		=  0;
String number 		= "0";
String moneda		= "";
String doc_type		= "";
String idUser = null;

try {
	idUser = request.getRemoteUser();
} catch (Exception e) {}
System.out.println("userul logat este --------->>>>> " + idUser);
%>
<%
	List<HashMap<String,Object>> allInvoices = new ArrayList<HashMap<String,Object>>();
	// retrieve all companies from DB
	List<HashMap<String,Object>> allCompanies = DefCompaniesVo.getInstance();
	// retrieve all currencies from DB
	List<HashMap<String,Object>> allCurrencies = DefCurrencyVo.getInstance();
	
	List<HashMap<String,Object>> allLotCat = DefCategory.getAll();
	
	boolean isControlorGestiune = InvoiceHeader.checkControlorGestiune(idUser);
	
	System.out.println("este controlor de gestiune --------->>>>> " + isControlorGestiune);
	
	allInvoices = InvoiceHeader.getAllFilteredExportedDocs(start, count, company_id, moneda, number, datafactura, datainreg, supplierCode, supplierName, doc_type, isControlorGestiune,idUser);
	total = InvoiceHeader.getAllFilteredCountExportedDocs(company_id, moneda, number, datafactura, datainreg, supplierCode, supplierName, doc_type,isControlorGestiune,idUser);

	boolean isAuthorized = Administrare.isAuthorizedToEdit(user.getEmailAddress());
	
	
%>
<%-- Links --%>
<portlet:renderURL var="goToVouchers" >
	<portlet:param name="mvcPath" value="/html/includes/visualizations/voucher.jsp"></portlet:param>
</portlet:renderURL>

<portlet:renderURL var="goToInvoices" >
	<portlet:param name="mvcPath" value="/html/includes/visualizations/vizualizare_factura.jsp"></portlet:param>
</portlet:renderURL>

<portlet:renderURL var="goToWorkingClothes" >
	<portlet:param name="mvcPath" value="/html/includes/visualizations/vizualizare_haine_lucru.jsp"></portlet:param>
</portlet:renderURL>

<portlet:renderURL var="goToCashExp" >
	<portlet:param name="mvcPath" value="/html/includes/visualizations/vizualizare_decont.jsp"></portlet:param>
</portlet:renderURL>

<portlet:renderURL var="editInvoiceURL" >
	<portlet:param name="mvcPath" value="/html/facturi/adauga_factura.jsp"></portlet:param>
</portlet:renderURL>

<aui:layout>
	<div id="filtru_dataimport_div" style="display:none">
		<aui:input type="text" id="data_fact" name="data_fact" value="" placeholder="aaaa-ll-zz" label=""></aui:input>
	</div>
</aui:layout>

<aui:layout>
	<aui:column first="true" columnWidth="100" last="true">
		<div id="listing-documente-exportate"></div>
	</aui:column>
</aui:layout>

<%-- HTML for PAGINATION --%>
<%@ include file="/html/includes/pagination.jsp" %>

<%--  datepicker for table header --%>
<div id="filtru_datafactura_div" style="display:none">
	<aui:input type="text" id="datafactura_header" name="datafactura_header" placeholder="Data factura" value=""  label="" style="width:100px;margin:0">
	</aui:input>
</div>
<%--  second datepicker for table header --%>
<div id="filtru_datainregfactura_div" style="display:none">
	<aui:input type="text" id="datainreg_header" name="datainreg_header" placeholder="Data import" value="" label="" style="width:110px;margin:0">
	</aui:input>
</div>

<script type="text/javascript">
var start = "<%=start%>";
var count = "<%=count%>";
var total = "<%=total%>"
var doc_type = "<%=doc_type%>";
var datainreg = "<%=datainreg%>";
var datafactura = "<%=datafactura%>";
var supplierCode = "<%=supplierCode%>";
var supplierName = "<%=supplierName%>";
//filters
var company_id = "<%=company_id%>";
var moneda = "<%=moneda%>";
var number = "<%=number%>";
var isAuthorized = <%=isAuthorized%>;
var isControlorGestiune = <%=isControlorGestiune%>;
var idUser = <%=idUser%>;
</script>

<script type="text/javascript">

	var oldDateValue2 = '';
	var oldDateValue = '';
	YUI({lang: 'ro'}).use('aui-datepicker', 'aui-datatable', 'aui-datatype', 'datatable-sort',
			function(Y) {
		// called whenever a change in date selection was performed
		var oldDateValue2 = $('#<portlet:namespace />datafactura_header').val();
		// called whenever a change in date selection was performed
		var oldDateValue = $('#<portlet:namespace />datainreg_header').val();
		
		var remoteData = [
  		<%
  		if (allInvoices != null) {
  		for (int i = 0; i < allInvoices.size(); i++) {	
  		%>
  		{
  			id: 			<%=allInvoices.get(i).get("id")%>,
  			inv_number: 	'<%=allInvoices.get(i).get("inv_number")%>',
  			created: 		'<%=allInvoices.get(i).get("created")%>',
  			supplier_code: 	'<%=allInvoices.get(i).get("supplier_code")%>',
  			supplier_name: 	'<%=allInvoices.get(i).get("supplier_name")%>',
  			inv_date: 		'<%=allInvoices.get(i).get("inv_date")%>',
  			pay_date: 		'<%=allInvoices.get(i).get("pay_date")%>', 
  			total_with_vat_curr: <%=allInvoices.get(i).get("total_with_vat_curr")%>,
  			currency: 		'<%=allInvoices.get(i).get("currency")%>',
  			company_name: 	'<%=allInvoices.get(i).get("company_name")%>',
  			doc_type:		'<%=allInvoices.get(i).get("doc_type")%>',
  			actiuni:		'<%=allInvoices.get(i).get("actiuni")%>'
  		}
  		<% if (i != (allInvoices.size() - 1)) { out.print(","); } %>
  		<% } } %>
  		];
		
		function formatCurrency(cell) {
		    format = {
		    	thousandsSeparator: ",",
		        decimalSeparator: ".",
		        decimalPlaces: noOfDecimalsToDisplay
		    };
		    return Y.DataType.Number.format(Number(cell.value), format);
		}
  		
  		// COLUMN INFO and ATTRIBUTES
  		var nestedCols = [
  		    { key: 'id', className: 'hiddencol'},
  			{ key: 'inv_number', 
  		    	label: '<input class="field" style="width:60px !important;margin:0" type="text" value="" id="filter-nr" name="filter-nr" placeholder="Nr."></input>' },
  			{ key: 'created', label: '<div id="filter-data-inreg"></div>'},
  			{ key: 'supplier_code',
  				label: '<input class="field" style="width:70px !important;margin:0" type="text" value="" id="filter-supplier_code" name="filter-supplier_code" placeholder="Cod furnizor" style="margin:0"/>'
  			},
  			{ key: 'supplier_name', 
  				label: '<input class="field" type="text" value="" id="filter-supplier_name" name="filter-supplier_code" placeholder="Furnizor" style="margin:0"/>'
  			},
  			{ key: 'inv_date', label:  '<div id="filter-dataf"></div>'},
  			{ key: 'pay_date', label:  'Data Scadenta'},
  			{ key: 'total_with_vat_curr', label: 'Val cu TVA', formatter: formatCurrency, className: 'currencyCol'},
  			{ key: 'currency', label: '<select id="filter-moneda" name="filter-moneda" label="" style="width:100px;margin:0">' +
  				'<option selected="selected" value="">Moneda</option>' +
  				<% for (int i = 0; i < allCurrencies.size(); i++) { %>
  				<% if (allCurrencies.get(i).get("status").equals("A")) {%>
  				<% out.println("'<option value=\"" + allCurrencies.get(i).get("ref") +"\">" + allCurrencies.get(i).get("ref") +"</option>' + "); %>
  				<% } } %>
  				'</select>'},
  			{ key: 'company_name', label: '<select id="filter-societate" name="filter-societate" label="" style="width:100px;margin:0">' +
  				'<option selected="selected" value="0">Societate</option>' +
  				<% for (int i = 0; i < allCompanies.size(); i++) { %>
  				<% if (allCompanies.get(i).get("status").equals("A")) {%>
  				<% out.println("'<option value=\"" + allCompanies.get(i).get("id") +"\">" + allCompanies.get(i).get("name") +"</option>' + "); %>
  				<% } } %>
  				'</select>'},
  			{ key: 'doc_type', label: '<select id="filter-doc_type" name="filter-doc_type" label="" style="width:100px;margin:0">' +
 				'<option selected="selected" value="">-</option>' +
 				'<option value="1">Factura</option>' +
 				'<option value="2">Bon de cesine</option>' +
 				'<option value="3">Haine de lucru</option>' +
 				'<option value="4">Deconturi</option>' +
 				'</select>',
 				formatter: function(o) {
					if (typeof o.value != 'undefined') {
						if(o.value == 1) {
							return 'Factura';
						} else if(o.value == 2){
							return 'Bon de cesiune';
						} else if(o.value == 3){
							return 'Haine de lucru';
						} else if(o.value == 4){
							return 'Deconturi';
						}
					} else {
						return '';
					}
				}
 			},
  			{ key: 'actiuni', label: 'Actiuni', allowHTML: true, 
  				formatter: '<a href="#" id="row{id}-{value}-{doc_type}" class="editrow">Vizualizare</a>' 
  			}
  		];

  		// TABLE INIT
  		var dataTable = new Y.DataTable({
  		    columns: nestedCols,
  		    data: remoteData,
	  		scrollable: "x",
	        width: "100%",
  		    recordType: ['id', 'inv_number', 'created', 'supplier_code', 
  		                 'supplier_name', 'inv_date', 'pay_date', 'total_no_vat_curr', 
  		                 'total_with_vat_curr', 'currency', 'company_name', 'doc_type'],
  		    editEvent: 'click'
  		}).render('#listing-documente-exportate');
  		
  		//dataTable.get('boundingBox').unselectable();
  		
  		function edit(sel_id, sel_doc_type, isEdit) {
  		    var ml  = dataTable.data, msg = '', template = '';
  	
  		    ml.each(function (item, i) {
  		        var data = item.getAttrs(['id', 'inv_number', 'created', 'supplier_code', 
  		                                 'supplier_name', 'inv_date', 'pay_date', 'total_no_vat_curr', 
  		         		                 'total_with_vat_curr', 'currency', 'company_name', 'doc_type']);
  		        if (data.id == sel_id && data.doc_type == sel_doc_type) {
  					$('#<portlet:namespace />id').val(data.id);
  					$('#<portlet:namespace />supplier_code_filter').val($('#filter-supplier_code').val());
  					$('#<portlet:namespace />supplier_name_filter').val($('#filter-supplier_name').val());
  					$('#<portlet:namespace />number_filter').val($('#filter-nr').val());
  					$('#<portlet:namespace />company_id_filter').val($('#filter-societate').val());
  					$('#<portlet:namespace />moneda_filter').val($('#filter-moneda').val());
  					$('#<portlet:namespace />datafactura_filter').val(oldDateValue2);
  					$('#<portlet:namespace />datainreg_filter').val(oldDateValue);
  					$('#<portlet:namespace />filter_doc_type').val($('#filter-doc_type').val());
  					$('#<portlet:namespace />param_currency').val(data.currency);
					if (sel_doc_type == 1 && !isEdit){
						$('#<portlet:namespace />registerInvoice').attr("action", "<%=goToInvoices.toString() %>");
						$('#<portlet:namespace />registerInvoice').submit();
					} else if (sel_doc_type == 1 && isEdit){
						$('#<portlet:namespace />registerInvoice').attr("action", "<%=editInvoiceURL.toString() %>");
						$('#<portlet:namespace />registerInvoice').submit();
					} else if (sel_doc_type == 2){
						$('#<portlet:namespace />registerInvoice').attr("action", "<%=goToVouchers.toString() %>");
						$('#<portlet:namespace />registerInvoice').submit();
					} else if (sel_doc_type == 3){
						$('#<portlet:namespace />registerInvoice').attr("action", "<%=goToWorkingClothes.toString() %>");
						$('#<portlet:namespace />registerInvoice').submit();
					} else if (sel_doc_type == 4){
						$('#<portlet:namespace />registerInvoice').attr("action", "<%=goToCashExp.toString() %>");
						$('#<portlet:namespace />registerInvoice').submit();
					}
  				}
			});
  		}
  		
  		dataTable.delegate('click', function (e) {
  		    // undefined to trigger the emptyCellValue
  		    e.preventDefault();
  		    var elem = e.target.get('id');
  		    var info = elem.replace('row','').split("-");
  		    if(info[2] == 'Factura' && isAuthorized) {
  		    	edit(info[0], info[1], true);
  		    } else {
  				edit(info[0], info[1], false);
  		    }
  		}, '.editrow', dataTable);
  		
  		
  		// PAGINATION
  		// set status of navigation buttons on page load
  		setNavigationButtonsState('<portlet:namespace />');
  		$('.navigation').click(function(e) {
  			var elementT 	= e.target;
  			var elementId 	= elementT.id;
  			var action 		= "";
  			
  			// determine which button was pressed and set the value of start accordingly
  			if (elementId.indexOf("first") > -1)	{ action = "first"; start = 0; }
  			if (elementId.indexOf("prev") > -1) 	{ action = "prev"; if (start - count <= 0) {start = 0} else { start -= count; } }
  			if (elementId.indexOf("next") > -1) 	{ action = "next"; start = parseInt(start) + parseInt(count); }
  			if (elementId.indexOf("last") > -1) 	{ action = "last"; start = parseInt(parseInt(total) / parseInt(count)) * count; if (start == total) { start -= count; } }
  			
  			makeAjaxCall(action);
  		});
  		
  		// filtrare societate
  		$('#filter-societate').change(function(e) {
  			var elementT 	= e.target;
  			var elementId 	= elementT.id;
  			var action 		= "filter";
  			// get selected value
  			company_id = elementT.value;
  			doc_type = $('#filter-doc_type').val();
  			moneda = $('#filter-moneda').val();
  			supplier_code = $('#filter-supplier_code').val();
  			supplierName = $('#filter-supplier_name').val();
  			number = $('#filter-nr').val();
			datafactura = oldDateValue2;
			datainreg = oldDateValue;
  			// reset start page 
  			start = 0;
  			// reset count
  			total = 0;
  			// make ajax call
  			makeAjaxCall(action);
  		});
  		
  	// filtrare societate
  		$('#filter-doc_type').change(function(e) {
  			var elementT 	= e.target;
  			var elementId 	= elementT.id;
  			var action 		= "filter";
  			// get selected value
  			company_id = $('#filter-societate').val();
  			doc_type = elementT.value;
  			moneda = $('#filter-moneda').val();
  			supplier_code = $('#filter-supplier_code').val();
  			supplierName = $('#filter-supplier_name').val();
  			number = $('#filter-nr').val();
			datafactura = oldDateValue2;
			datainreg = oldDateValue;
  			// reset start page 
  			start = 0;
  			// reset count
  			total = 0;
  			// make ajax call
  			makeAjaxCall(action);
  		});
  		// filtrare moneda
  		$('#filter-moneda').change(function(e) {
  			var elementT 	= e.target;
  			var elementId 	= elementT.id;
  			var action 		= "filter";
  			// get selected value
  			moneda = elementT.value;
  			doc_type = $('#filter-doc_type').val();
  			supplier_code = $('#filter-supplier_code').val();
  			supplierName = $('#filter-supplier_name').val();
  			number = $('#filter-nr').val();
  			company_id = $('#filter-societate').val();
			datafactura = oldDateValue2;
			datainreg = oldDateValue;
  			// reset start page 
  			start = 0;
  			// reset count
  			total = 0;
  			// make ajax call
  			makeAjaxCall(action);
  		});

  		// filtrare dupa cod furnizor
  		$('#filter-supplier_code').keyup(function (e) {
  		    if ($("#filter-supplier_code").is(":focus") && (e.keyCode == 13)) {
  				var elementT 	= e.target;
  				var elementId 	= elementT.id;
  				var action 		= "filter";
  				// get selected value
  				supplierCode = elementT.value;
  				doc_type = $('#filter-doc_type').val();
  				moneda = $('#filter-moneda').val();
  	  			supplierName = $('#filter-supplier_name').val();
  	  			number = $('#filter-nr').val();
  	  			company_id = $('#filter-societate').val();
  				datafactura = oldDateValue2;
  				datainreg = oldDateValue;
  				if (number == "") {
  					number = 0;
  				}
  				// reset start page 
  				start = 0;
  				// reset count
  				total = 0;
  				// make ajax call
  				makeAjaxCall(action);
  		    }
  		});
  		
  		// filtrare dupa nume furnizor
  		$('#filter-supplier_name').change(function (e) {
  				var elementT 	= e.target;
  				var elementId 	= elementT.id;
  				var action 		= "filter";
  				// get selected value
  				supplierName = elementT.value;
  				doc_type = $('#filter-doc_type').val();
  				moneda = $('#filter-moneda').val();
  	  			supplier_code = $('#filter-supplier_code').val();
  	  			number = $('#filter-nr').val();
  	  			company_id = $('#filter-societate').val();

  				datafactura = oldDateValue2;
  				datainreg = oldDateValue;
  				if (number == "") {
  					number = 0;
  				}
  				// reset start page 
  				start = 0;
  				// reset count
  				total = 0;
  				// make ajax call
  				makeAjaxCall(action);
  		    
  		});
  		
  		// filtrare dupa nr factura
  		$('#filter-nr').keyup(function (e) {
  		    if ($("#filter-nr").is(":focus") && (e.keyCode == 13)) {
  				var elementT 	= e.target;
  				var elementId 	= elementT.id;
  				var action 		= "filter";
  				// get selected value
  				number = elementT.value;
  				doc_type = $('#filter-doc_type').val();
  				moneda = $('#filter-moneda').val();
  	  			supplier_code = $('#filter-supplier_code').val();
  	  			supplierName = $('#filter-supplier_name').val();
  	  			company_id = $('#filter-societate').val();

  				datafactura = oldDateValue2;
  				datainreg = oldDateValue;
  				if (number == "") {
  					number = 0;
  				}
  				// reset start page 
  				start = 0;
  				// reset count
  				total = 0;
  				// make ajax call
  				console.log("number = "+number);
  				makeAjaxCall(action);
  		    }
  		});
  		
  		function makeAjaxCall(action) {
  			$.ajax({
  				type: "POST",
  				url: "<%=ajaxURL%>",
  				data: 	"<portlet:namespace />action=" + action + 
  						"&<portlet:namespace />start=" + start + 
  						"&<portlet:namespace />count=" + count +
  						"&<portlet:namespace />total=" + total +
  						"&<portlet:namespace />moneda=" + moneda +
  						"&<portlet:namespace />number=" + number +
  						"&<portlet:namespace />company_id=" + company_id +
  						"&<portlet:namespace />datafactura=" + datafactura +
  						"&<portlet:namespace />datainreg=" + datainreg +
  						"&<portlet:namespace />supplier_code=" + supplierCode +
  						"&<portlet:namespace />supplier_name=" + supplierName +
  						"&<portlet:namespace />isControlorGestiune=" + isControlorGestiune +
  						"&<portlet:namespace />idUser=" + idUser +
  						"&<portlet:namespace />doc_type=" + doc_type,
  				success: function(msg) {
  					// get table data
  					if (msg != ""){
  						var jsonEntries = jQuery.parseJSON(msg);
  						total = jsonEntries.total;
  						dataTable.set('data', eval(jsonEntries.values));
  					} else {
  						dataTable.set('data', []);
  					}
  					
  					// set status of navigation buttons on page load
  					setNavigationButtonsState('<portlet:namespace />');
  				}
  			});
  		}
		
		function dateChangedFact(input) {
			oldDateValue2 = input.val();
			// get selected value
			if ($('#<portlet:namespace />datainreg_header').val() == ""){
				datainreg = "";
			} else {
				datainreg = $('#<portlet:namespace />datainreg_header').val();
			}
			datafactura = oldDateValue2;
			moneda = $('#filter-moneda').val();
  			supplier_code = $('#filter-supplier_code').val();
  			supplierName = $('#filter-supplier_name').val();
  			number = $('#filter-nr').val();
  			company_id = $('#filter-societate').val();
  			doc_type = $('#filter-doc_type').val();
			// reset start page 
			start = 0;
			// reset count
			total = 0;
			action = "filter";
			// make ajax call
			makeAjaxCall(action);
		}
		
		var datepicker2 = new Y.DatePicker({
			trigger : '#<portlet:namespace />datafactura_header',
			mask : '%Y-%m-%d',
			popover : {
				zIndex : 1
			},
			popover: {
				zIndex : 1,
		          toolbars: {
		            header: [[
		              {
		                icon:'icon-trash',
		                label: 'Reseteaza filtru',
		                on: {
		                  click: function() {
		                	  datepicker2.clearSelection();
		                  }
		                }
		              }]]
		          }
			},
			after: {
	            selectionChange: function(event) {
	            	if (oldDateValue2 != $('#<portlet:namespace />datafactura_header').val()) {
	            		oldDateValue2 = $('#<portlet:namespace />datafactura_header').val();
	            		dateChangedFact($('#<portlet:namespace />datafactura_header'));
	            	}
	            	//$('#data_filter').html('');
	            }
	        },
			calendar : {
				dateFormat : '%Y-%m-%d'
			}
		});
		

		function dateChangedInreg(input) {
			
			oldDateValue = input.val();
			// get selected value
			if ($('#<portlet:namespace />datafactura_header').val() == ""){
				datafactura = "";
			} else {
				datafactura = $('#<portlet:namespace />datafactura_header').val();
			}
			datainreg = oldDateValue;
			moneda = $('#filter-moneda').val();
  			supplier_code = $('#filter-supplier_code').val();
  			supplierName = $('#filter-supplier_name').val();
  			number = $('#filter-nr').val();
  			company_id = $('#filter-societate').val();
  			doc_type = $('#filter-doc_type').val();
			// reset start page 
			start = 0;
			// reset count
			total = 0;
			action = "filter";
			// make ajax call
			makeAjaxCall(action);
		}
		
		var datepicker = new Y.DatePicker({
			trigger : '#<portlet:namespace />datainreg_header',
			mask : '%Y-%m-%d',
			popover : {
				zIndex : 1
			},
			popover: {
				zIndex : 1,
		          toolbars: {
		            header: [[
		              {
		                icon:'icon-trash',
		                label: 'Reseteaza filtru',
		                on: {
		                  click: function() {
		                	  datepicker.clearSelection();
		                  }
		                }
		              }]]
		          }
			},
			calendar : {
				dateFormat : '%Y-%m-%d'
			},
			after: {
	            selectionChange: function(event) {
	            	if (oldDateValue != $('#<portlet:namespace />datainreg_header').val()) {
	            		oldDateValue = $('#<portlet:namespace />datainreg_header').val();
	            		dateChangedInreg($('#<portlet:namespace />datainreg_header'));
	            	}
	            	//$('#data_filter').html('');
	            }
	        }
		});
	});
	
	$(document).ready(function() {
		isAuthorized = <%=isAuthorized%>;
	});
</script>

<aui:form method="post" action="<%=goToVouchers.toString() %>" name="registerInvoice" id="registerInvoice" style="margin:0;padding:0">
	<aui:input type="hidden" id="id" name="id"/>
	<aui:input type="hidden" id="number_filter" name="number_filter"/>
	<aui:input type="hidden" id="company_id_filter" name="company_id_filter"/>
	<aui:input type="hidden" id="moneda_filter" name="moneda_filter"/>
	<aui:input type="hidden" id="datafactura_filter" name="datafactura_filter"/>
	<aui:input type="hidden" id="datainreg_filter" name="datainreg_filter"/>
	<aui:input type="hidden" id="supplier_code_filter" name="supplier_code_filter"/>
	<aui:input type="hidden" id="supplier_name_filter" name="supplier_name_filter"/>
	<aui:input type="hidden" id="filter_doc_type" name="filter_doc_type"/>
	<aui:input type="hidden" id="param_currency" name="currency"/>
	<aui:input type="hidden" id="source_page" name="source_page" value="exported_docs"/>
</aui:form>