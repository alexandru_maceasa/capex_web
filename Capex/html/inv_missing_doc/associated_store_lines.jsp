<%@page import="com.profluo.ecm.model.vo.DefDpis"%>
<%@page import="com.profluo.ecm.model.vo.DefAttDocsVo"%>
<%@page import="com.profluo.ecm.flows.InvoiceStages"%>
<%@page import="com.profluo.ecm.model.db.InvoiceHeader"%>
<%@page import="com.profluo.ecm.model.vo.DefCurrencyVo"%>
<%@page import="com.profluo.ecm.model.vo.DefCompaniesVo"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.List"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme"%>

<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui"%>
<%@ taglib uri="http://alloy.liferay.com/tld/aui" prefix="aui"%>
<%@ taglib uri="http://liferay.com/tld/util" prefix="liferay-util"%>

<%@ page import="java.util.ResourceBundle"%>
<%@ page import="java.util.Locale"%>

<liferay-theme:defineObjects />
<portlet:defineObjects />
<aui:layout>
	<div id="assoc_store_lines" style="display:none">
		<br/><br/><br/>
		<a><font color="#FF0000">Completati DPI-urile pentru urmatoarele alocari de articole corespunzatoare facturii selectate!</font></a>
	</div>
</aui:layout>

<%
String whatPage     = "";
try{
	whatPage				= renderRequest.getAttribute("page-layout").toString();
} catch (Exception e){}

System.out.println("PLM");
%>

<script type="text/javascript">

YUI().use( 'aui-datatable', 'aui-datatype', 'datatable-sort',
	function(Y) {
	
		function formatCurrency(cell) {
		    format = {
		    	thousandsSeparator: ",",
		        decimalSeparator: ".",
		        decimalPlaces: noOfDecimalsToDisplay
		    };
		    return Y.DataType.Number.format(Number(cell.value), format);
		}
		
		var allDpis = <%=DefDpis.getJsonStringDpis(true)%>;
	
		var remoteDataSplit = [];
		
		var nestedColsSplit = [
			{ key: 'store_line_id', className: 'hiddencol'},
			{ key: 'nr', className: 'hiddencol'},
        	{ key: 'description', label: 'Den. articol.'},
        	{ key: 'quantity', label:  'Cant', formatter: formatCurrency, className: 'currencyCol'},
        	{ key: 'price_no_vat_curr', label: 'Val fara TVA', formatter: formatCurrency, className: 'currencyCol'},
        	{ key: 'id_store', label: 'Magazin'},
       		{ key: 'id_department', label: 'Raion/Dep.'},
        	{ key: 'associated_acc', label: 'Cont'},
        	{ key: 'id_tip_inreg', label: 'Tip inregistrare'},
   			{ key: 'id_act_mf', label: 'Actiune MF'},
   			{ key: 'id_dpi', label: 'Nr. DPI', editor: new Y.DropDownCellEditor({options: allDpis, id: 'alldpis', elementName: 'alldpis',
   				after: {
   		            focus: function(event) {
   			            $("select[name=alldpis]").chosen();
   		            }
   				}
   			}), formatter: function(o) {
	   				var obj = JSON.parse(JSON.stringify(allDpis));
	   				
	   				if (typeof o.value != 'undefined') {
	   					return obj[o.value].toString();
	   				} else {
	   					return '0';
	   				}
   				}
   			},
   			{ key: 'vizualizare_dpi', label: 'Link DPI', 
   					allowHTML: true, 
   					formatter: function(o) { 
   						return '<a href="#" class="dpilink" id="dpi_' + o.data.nr + '_' + o.value + '">DPI</a>';
   					},
   					emptyCellValue: '<a href="#" class="dpilink" id="dpi_{nr}_{value}">DPI</a>'},
  			{ key: 'id_tip_inventar', label: 'Tip nr. inventar'},
       ];
        		
   		// INVOICE STORE LINE TABLE INIT
   		dataTableSplit = new Y.DataTable({
   		    columns: nestedColsSplit,
   		    data: remoteDataSplit,
   		    editEvent: 'click',
   		    recordType: ['nr', 'store_line_id', 'description', 'quantity', 'price_no_vat_curr', 'id_store', 'id_department', 'associated_acc', 'id_tip_inreg',
   		                  'id_act_mf', 'id_dpi','vizualizare_dpi', 'id_tip_inventar']
   		}).render('#assoc_store_lines');
	}
);
</script>




