<%@page import="com.profluo.ecm.model.db.UsersInformations"%>
<%@page import="com.profluo.ecm.model.db.Administrare"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.profluo.ecm.model.vo.DefAttDocsVo"%>
<%@page import="com.profluo.ecm.flows.InvoiceStages"%>
<%@page import="com.profluo.ecm.model.db.InvoiceHeader"%>
<%@page import="com.profluo.ecm.model.vo.DefCurrencyVo"%>
<%@page import="com.profluo.ecm.model.vo.DefCompaniesVo"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.List"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme"%>

<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui"%>
<%@ taglib uri="http://alloy.liferay.com/tld/aui" prefix="aui"%>
<%@ taglib uri="http://liferay.com/tld/util" prefix="liferay-util"%>

<%@ page import="java.util.ResourceBundle"%>
<%@ page import="java.util.Locale"%>

<liferay-theme:defineObjects />
<portlet:defineObjects />

<%
	locale = renderRequest.getLocale();
	ResourceBundle resmain = portletConfig.getResourceBundle(locale);
%>
<portlet:resourceURL id="ajaxURL" var="ajaxURL"/>
<portlet:actionURL name="saveMissingDocs" var="submitURL" />

<portlet:renderURL var="viewInvoiceURL" >
	<portlet:param name="mvcPath" value="/html/includes/visualizations/vizualizare_factura.jsp"></portlet:param>
</portlet:renderURL>

<portlet:renderURL var="editInvoiceURL" >
	<portlet:param name="mvcPath" value="/html/facturi/adauga_factura.jsp"></portlet:param>
</portlet:renderURL>

<% // PAGINATION Start - env setup 
int start = 0; 
int count = 3;
int total = 0;
String categorie = "";
String pageType = "";
String datafactura 	= "";
String datainreg 	= "";
String supplierCode = "";
String supplierName = "";
String numeMagazin = "";
String number = "";
String moneda = "";
int companyId = 0;

try {
	number					= renderRequest.getParameter("invoiceNo").toString();
} catch (Exception e){ }
try {
	datainreg						= renderRequest.getParameter("regDate").toString();
} catch (Exception e){ }
try {
	supplierName				= renderRequest.getParameter("supplierName").toString();
} catch (Exception e){ }
try {
	datafactura					= renderRequest.getParameter("invoiceDate").toString();
} catch (Exception e){ }
try {
	moneda					= renderRequest.getParameter("currency").toString();
} catch (Exception e){ }
try {
	companyId					= Integer.parseInt(renderRequest.getParameter("companyId").toString());
} catch (Exception e){ }


	//locale = renderRequest.getLocale();
	//ResourceBundle resmain = portletConfig.getResourceBundle(locale);
	// retrieve all invoices headers from DB
	List<HashMap<String,Object>> allInvoices = new ArrayList<HashMap<String,Object>>();
	// retrieve all companies from DB
	List<HashMap<String,Object>> allCompanies = DefCompaniesVo.getInstance();
	// retrieve all currencies from DB
	List<HashMap<String,Object>> allCurrencies = DefCurrencyVo.getInstance();
	// retrieve all attached docs
	List<HashMap<String,Object>> allDocs = DefAttDocsVo.getInstance();
	
	String storeCode 	= "";
	
	
	
	String email 	= "";
	if(user.getEmailAddress() != null){
		email = user.getEmailAddress();
	}
	
	try {
		pageType				= renderRequest.getAttribute("type").toString();
	} catch (Exception e) { }

	
	if(user.getAddresses().size() != 0 && user.getAddresses().get(0).getStreet2() != null){
		storeCode = user.getAddresses().get(0).getStreet2().toString();
	}
	
	allInvoices = InvoiceHeader.getInvociesWithMissingDocs(start, count, companyId, moneda, number, datafactura, datainreg, supplierName, numeMagazin, (int)user.getUserId(), pageType);
	total = InvoiceHeader.getInvociesWithMissingDocsCount(companyId, moneda, number, datafactura, datainreg, supplierName,  numeMagazin, (int)user.getUserId(), pageType);

	boolean isAuthorized = Administrare.isAuthorizedToEdit(user.getEmailAddress());
	boolean isActive = UsersInformations.checkActive(user.getEmailAddress());
	
%>

<script type="text/javascript">
var start 		= "<%=start%>";
var count 		= "<%=count%>";
var total 		= "<%=total%>";
var type 		= "<%=pageType%>";
var storeName 	= "<%=storeCode%>";
var datainreg 	= "<%=datainreg%>";
var datafactura = "<%=datafactura%>";
var supplierName = "<%=supplierName%>";
var numeMagazin = "<%=numeMagazin%>";
//filters
var company_id 	= "<%=companyId%>";
var moneda 		= "<%=moneda%>";
var number 		= "<%=number%>";
var isAuthorized = <%=isAuthorized%>;
</script>

<liferay-ui:success key="invoiceSaved" 		message="Situatia documentelor lipsa a fost salvata cu succes." />
<liferay-ui:success key="invoiceGoesToDPI" 	message="Factura a fost trimisa catre aprobatorul de DPI." />
<liferay-ui:error 	key="invoiceNotSaved" 	message="A aparut o problema in salvarea documentelor lipsa. Va rugam incerati mai tarziu." />

<aui:layout>
	<aui:column>
	<div id="numberOfInvoices">
		Numarul de facturi nealocate este :  <%= total %>
	</div>
	</aui:column>
</aui:layout>

<aui:layout>
	<aui:column first="true" columnWidth="100" last="true" >
		<div id="id_doc_just"></div>
	</aui:column>
	<aui:form name="remove_docs" id="remove_docs" action="<%=submitURL.toString() %>">
		<aui:input id="missing_docs" name="missing_docs" value="" type="hidden"/>
		<aui:input id="inv_number" name="inv_number" value="" type="hidden"/>
		<aui:input id="inv_id" name="inv_id" type="hidden"/>
		<aui:input id="userId" name="userId" type="hidden"/>
	</aui:form>
</aui:layout>



<%-- HTML for PAGINATION --%>
<%@ include file="/html/includes/pagination.jsp" %>

<aui:layout>
	<%@ include file="/html/inv_missing_doc/associated_store_lines.jsp" %>
	<aui:column last="true" id="saveNewDpisButton" style="display:none">
		<a><font color="#FF0000">Salvati pentru a face update cu noile DPI-uri!</font></a>
		<aui:button name="saveNewDpis" id="saveNewDpis" label="" value="Salveaza" class="btn btn-primary saveDpis" />
	</aui:column>
</aui:layout>
<%--  datepicker for table header --%>
<div id="filtru_datafactura_div" style="display:none">
	<aui:input type="text" id="datafactura_header" name="datafactura_header" placeholder="Data factura" value=""  label="" style="width:100px;margin:0">
	</aui:input>
</div>
<%--  second datepicker for table header --%>
<div id="filtru_datainregfactura_div" style="display:none">
	<aui:input type="text" id="datainreg_header" name="datainreg_header" placeholder="Data inregistrare" value="" label="" style="width:100px;margin:0">
	</aui:input>
</div>

<div id="overlay" class="yui3-overlay-loading" style="left:-350px;position:absolute;width: 280px;display:none">
	<div class="yui3-widget-hd">Lipsa documente justificative</div>
	<div class="yui3-widget-bd">
		<strong><%=resmain.getString("doc_primite")%> pentru factura: <span id="doc_missing_title"></span></strong>
		<% if (allDocs != null) {
	      	for (int i = 0; i < allDocs.size(); i++) { %>
	      		<% if (allDocs.get(i).get("status").equals("A")) {%>
					<aui:layout>
						<aui:column columnWidth="100">
							<input type="checkbox" name="docs" id="docs_<%=allDocs.get(i).get("name").toString().replaceAll(" ", "_")%>" class="fl doc_<%=allDocs.get(i).get("name").toString().replaceAll(" ", "_")%>" 
							value="<%=allDocs.get(i).get("name")%>"/>
							<label for="docs_<%=allDocs.get(i).get("name").toString().replaceAll(" ", "_")%>" class="fl"><%=allDocs.get(i).get("name").toString()%></label>
							<div class="clear"></div>
						</aui:column>
					</aui:layout>
				<% } %>
		<% } } %>
		<aui:layout>
			<aui:column columnWidth="35" last="true">
				<aui:input name="over-save" id="over-save" cssClass="btn btn-primary-blue fl" type="button" label=""  value="<%=resmain.getString(\"salveaza\")%>">
				</aui:input>
			</aui:column>
			<aui:column columnWidth="50" first="true">
				<aui:input value="<%=resmain.getString(\"anuleaza\") %>" name="over-cancel" type="button" label="" 
						cssClass="btn btn-primary-red fl" onclick="checkMissingDocs()"></aui:input>
			</aui:column>
		</aui:layout>
	</div>
</div>
<%--
<aui:input name="docs" id="docs<%=i %>" type="checkbox" 
value="1" checked="true" label="<%=allDocs.get(i).get(\"name\").toString()%>"></aui:input>
--%>

<script type="text/javascript">
var dataTable;
var oldDateValue2 = '';
var oldDateValue = '';
var textIsDisplayed = false;
var isDisplayed = false;
var dataTableSplit;

var invoiceHasOnlyDpiMissing = false;

var invoice_id = 0;
var mustSubmit = false;
var hasStoreLinesWithoutDpi = [];
YUI().use('aui-datepicker', 'aui-datatable', 'aui-datatype', 'datatable-sort', 'overlay',
	function(Y){
		// called whenever a change in date selection was performed
		var oldDateValue2 = $('#<portlet:namespace />datafactura_header').val();
		// called whenever a change in date selection was performed
		var oldDateValue = $('#<portlet:namespace />datainreg_header').val();
	
		var remoteData=[
      		<% if (allInvoices != null) {
	      		for (int i = 0; i < allInvoices.size(); i++) { %>
	      	  		{
	      	  			id: 			<%=allInvoices.get(i).get("id")%>,
	      	  			inv_number: 	'<%=allInvoices.get(i).get("inv_number")%>',
	      	  			created: 		'<%=allInvoices.get(i).get("created")%>',
	      	  			supplier_code: 	'<%=allInvoices.get(i).get("supplier_code")%>',
	      	  			supplier_name: 	'<%=allInvoices.get(i).get("supplier_name")%>',
	      	  			inv_date: 		'<%=allInvoices.get(i).get("inv_date")%>',
	      	  			pay_date: 		'<%=allInvoices.get(i).get("pay_date")%>', 
	      	  			total_with_vat_curr: <%=allInvoices.get(i).get("total_with_vat_curr")%>,
	      	  			currency: 		'<%=allInvoices.get(i).get("currency")%>',
	      	  			company_name: 	'<%=allInvoices.get(i).get("company_name")%>',
	      	  			missing_docs: 	'<%=allInvoices.get(i).get("missing_docs")%>',
	      	  			store_name : 	'<%=allInvoices.get(i).get("store_name")%>'
	      	  		}
	      	  		<% if (i != (allInvoices.size() - 1)) { out.print(","); } %>
			<% } } %>
		];
		
		function formatCurrency(cell) {
		    format = {
		    	thousandsSeparator: ",",
		        decimalSeparator: ".",
		        decimalPlaces: noOfDecimalsToDisplay
		    };
		    return Y.DataType.Number.format(Number(cell.value), format);
		}

		//columns info
		var nestedColumns=[
		    {key:'id', className:"hiddencol"},
			{key:'inv_number', 
		    	/*label:'<input id="id_nr_fact" name="id_nr_fact" label="" placeholder=" Nr." style="width:100%"/>',*/
		    	label: '<input class="field" style="width:60px !important;margin:0" type="text" value="" id="filter-nr" name="filter-nr" placeholder="Nr."/>',
				allowHTML:true, resizable:true},
		    {key:'created', label:'Data inregistrare', label: '<div id="filter-data-inreg"></div>'},
		    {key:'supplier_code', label:'Cod Furnizor'},
		    {key:'supplier_name', 
		    	label:'<input id="filter-furnizor" class="field" name="filter-furnizor" label="" placeholder="Nume furnizor" style="width:120px !important;margin:0" type="text" value=""/>', 
		    	allowHTML:true
		    },
		    {key:'inv_date', label: '<div id="filter-dataf"></div>'},
		    {key:'pay_date', label:'Data Scadenta'},
		    {key:'total_with_vat_curr',label:'Val cu TVA', formatter: formatCurrency, className: 'currencyCol'},
		    {key:'currency', label: '<select id="filter-moneda" name="filter-moneda" label="" style="width:100px;margin:0">' +
  				'<option selected="selected" value="">Moneda</option>' +
  				<% for (int i = 0; i < allCurrencies.size(); i++) { %>
  				<% if (allCurrencies.get(i).get("status").equals("A")) {%>
  				<% out.println("'<option value=\"" + allCurrencies.get(i).get("ref") +"\">" + allCurrencies.get(i).get("ref") +"</option>' + "); %>
  				<% } } %>
  				'</select>'},
		    {key:'company_name', label: '<select id="filter-societate" name="filter-societate" label="" style="width:100px;margin:0">' +
  	  				'<option selected="selected" value="0">Societate</option>' +
  	  				<% for (int i = 0; i < allCompanies.size(); i++) { %>
  	  				<% if (allCompanies.get(i).get("status").equals("A")) {%>
  	  				<% out.println("'<option value=\"" + allCompanies.get(i).get("id") +"\">" + allCompanies.get(i).get("name") +"</option>' + "); %>
  	  				<% } } %>
  	  				'</select>'},
		    {key:'missing_docs',label:'Documente Lipsa'},
		    <% if(!pageType.contains("2")){ %>
				{ key : 'store_name', label: '<input class="field" type="text" value="" id="filtru_magazin" name="filtru_magazin" placeholder="Magazin" style="margin:0"/>', className: 'store_names' },
			<% } %>
		    <% if(pageType.equals("2") || pageType.equals("5")){ %>
		    {key:'primire_documente', label:'Primire documente', className:'align-center', allowHTML: true,
    	 		formatter: '<input type="button" name="doc_tot" id="doc_tot{id}" value="Toate" class="btn btn-primary removealldocs" />'+
				'<input type="button" name="doc_partial" id="show_doc_partial{id}" value="Partial" class="btn btn-primary showpopup" />'
    	 	},
    	 	<% } %>
    	 	<% if (isAuthorized) { %>
    	 		{key:'actiuni', label:'Actiuni',  allowHTML: true, formatter: '<a href="#" id="row{id}" class="viewRow">Actualizare informatii</a>'}
    	 	<% } else { %> 
    	 		{key:'actiuni', label:'Actiuni',  allowHTML: true, formatter: '<a href="#" id="row{id}" class="viewRow">Vizualizeaza factura</a>'}
    	 	<% } %>
		];

		//table init
		dataTable = new Y.DataTable({
			columns: nestedColumns,
			data: remoteData,
			editEvent:'click'
		}).render('#id_doc_just');
	
		//la click pe vizualizeaza factura
		dataTable.delegate('click', function (e) {
  		    // undefined to trigger the emptyCellValue
  		    e.preventDefault();
  		    var elem = e.target.get('id');
  		   	var factNo = elem.replace('row', '');
  		   	$('#<portlet:namespace />id').val(factNo);
  			$('#<portlet:namespace />number_filter').val(number);
  			$('#<portlet:namespace />datainreg_filter').val(datainreg);
  			$('#<portlet:namespace />supplier_name_filter').val(supplierName);
  			$('#<portlet:namespace />filtru_magazin').val(numeMagazin);
  			$('#<portlet:namespace />datafactura_filter').val(datafactura);
  			$('#<portlet:namespace />moneda_filter').val(moneda);
  			$('#<portlet:namespace />company_id_filter').val(company_id);
  			$('#<portlet:namespace />filter_doc_type').val('');
  			$('#<portlet:namespace />source_page').val('<%=InvoiceStages.PAGE_CONTABILITATE_LISPA_DOC%>');
  			if(isAuthorized) {
					$('#<portlet:namespace />viewInvoice').attr("action", "<%=editInvoiceURL.toString() %>");
				} else {
					$('#<portlet:namespace />viewInvoice').attr("action", "<%=viewInvoiceURL.toString() %>");
				}
  		   	$('#<portlet:namespace />viewInvoice').submit();
  		}, '.viewRow', dataTable);
		
		// click on save button on pop-up form
		Y.on("click", function(e){
        	e.preventDefault();
        	var missingDocsVal = "";
        	// get all selected docs to be save in the db
        	$('input[type="checkbox"]').each(function () {
        	       var sThisVal = (this.checked ? $(this).val() : "");
        	       console.log(sThisVal);
        	       if (sThisVal != "") {
        	    	   missingDocsVal += sThisVal + ",";
        	       }
			});
        	// remove last comma
        	if (missingDocsVal.length > 0) {
        		missingDocsVal = missingDocsVal.substring(0, missingDocsVal.length - 1);
        	}
        	$('#<portlet:namespace/>missing_docs').val(missingDocsVal);
  			$('#<portlet:namespace />userId').val('<%=user.getUserId()%>');
        	$('#<portlet:namespace/>remove_docs').submit();
		}, "#<portlet:namespace/>over-save");
		
		// click on save all docs
		dataTable.delegate('click', function(e){
        	e.preventDefault();
        	var elem = e.target;
        	var docId = elem.getAttribute("id").replace('doc_tot','');
        	var ml = dataTable.data, msg = '', template = '';
		    ml.each(function (item, i) {
		        var data = item.getAttrs(['id', 'inv_number', 'missing_docs']);
		        if (data.id == docId) {
		        	if (data.missing_docs.indexOf("DPI") >= 0){
		        		alert ("Factura are DPI-uri lipsa. Setati intai DPI-urile din optiunea \"Partial\"!");
		        		return;
		        	}
		        	$('#<portlet:namespace/>inv_number').val(data.inv_number);
		        	$('#<portlet:namespace/>inv_id').val(data.id);
		        	$('#<portlet:namespace/>missing_docs').val("");
		        	$('#<portlet:namespace />userId').val('<%=user.getUserId()%>');
		        	$('#<portlet:namespace/>remove_docs').submit();
		        	return;
		        }
		    });
		}, ".removealldocs", dataTable);
	
		// click on each row on the "partial" button
		dataTable.delegate('click', function(e){
        	e.preventDefault();
	        var currentNode = e.target;
	        var docId = currentNode.getAttribute("id").replace('show_doc_partial','');
	        
	        if (docId == invoice_id && isDisplayed==true){
	        	return;
	        }
			var overlay = new Y.Overlay({
			    srcNode:"#overlay",
			    width:"250",
			    align: {
			        node: currentNode,
			        points:["tl", "tr"]
			    }
			});
			invoiceHasOnlyDpiMissing = false;
			overlay.render();
			
			//set the invoice id on a  global variable
			invoice_id = docId;
			// reset checkboxes from popup
			$('input[name="docs"]').prop('checked', false);
			// set required checkboxes
		    var ml = dataTable.data, msg = '', template = '';
		    ml.each(function (item, i) {
		        var data = item.getAttrs(['id', 'inv_number', 'missing_docs']);
		        
		        if (data.id == docId) {
		        	var docs = data.missing_docs.split(",");
		        	for (var aux = 0; aux < docs.length; aux++) {
		        		$('#docs_'+docs[aux].trim().replace(/ /g, "_")).prop('checked', true);
		        		if (docs[aux] == "DPI"){
		        			if(docs.length == 1){
		        				invoiceHasOnlyDpiMissing = true;
		        			}
		        			hasStoreLinesWithoutDpi[docId] = true; 
		        		}
		        		
		        	}
		        	$('#doc_missing_title').text(data.inv_number);
		        	$('#<portlet:namespace/>inv_number').val(data.inv_number);
		        	$('#<portlet:namespace/>inv_id').val(data.id);
		        }
		    });
	        $('#overlay').show();
    	}, ".showpopup", dataTable);
		
  		// PAGINATION
  		// set status of navigation buttons on page load
  		setNavigationButtonsState('<portlet:namespace />');
  		$('.navigation').click(function(e) {
  			var elementT 	= e.target;
  			var elementId 	= elementT.id;
  			var action 		= "";
  			
  			// determine which button was pressed and set the value of start accordingly
  			if (elementId.indexOf("first") > -1)	{ action = "first"; start = 0; }
  			if (elementId.indexOf("prev") > -1) 	{ action = "prev"; if (start - count <= 0) {start = 0} else { start -= count; } }
  			if (elementId.indexOf("next") > -1) 	{ action = "next"; start = parseInt(start) + parseInt(count); }
  			if (elementId.indexOf("last") > -1) 	{ action = "last"; start = parseInt(parseInt(total) / parseInt(count)) * count; }
  			
  			makeAjaxCall(action);
  		});
  		
  		// filtare dupa nume furnizor
  		$('#filter-furnizor').keyup(function (e) {
  		    if ($("#filter-furnizor").is(":focus") && (e.keyCode == 13)) {
  				var elementT 	= e.target;
  				var elementId 	= elementT.id;
  				var action 		= "filter";	
				supplierName =  elementT.value; 				
  				moneda = $('#filter-moneda').val();
  				numeMagazin = $('#filtru_magazin').val();
  				number = $('#filter-nr').val();
  				datafactura = oldDateValue2;
  				datainreg = oldDateValue;
  	  			// reset start page 
  	  			start = 0;
  	  			// reset count
  	  			total = 0;
  	  			// make ajax call
  	  			makeAjaxCall(action);
  		    }
  		});
  		
  		// filtrare societate
  		$('#filter-societate').change(function(e) {
  			var elementT 	= e.target;
  			var elementId 	= elementT.id;
  			var action 		= "filter";
  			// get selected value
  			company_id = elementT.value;

  			moneda = $('#filter-moneda').val();
  			supplierName = $('#filter-furnizor').val();
  			numeMagazin = $('#filtru_magazin').val();
  			number = $('#filter-nr').val();
			datafactura = oldDateValue2;
			datainreg = oldDateValue;
  			// reset start page 
  			start = 0;
  			// reset count
  			total = 0;
  			// make ajax call
  			makeAjaxCall(action);
  		});
  		
  	// filtrare societate
  		$('#filtru_magazin').change(function(e) {
  			var elementT 	= e.target;
  			var elementId 	= elementT.id;
  			var action 		= "filter";
  			// get selected value
  			numeMagazin = elementT.value;

  			moneda = $('#filter-moneda').val();
  			supplierName = $('#filter-furnizor').val();
  			company_id = $('#filter-societate').val();
  			number = $('#filter-nr').val();
			datafactura = oldDateValue2;
			datainreg = oldDateValue;
  			// reset start page 
  			start = 0;
  			// reset count
  			total = 0;
  			// make ajax call
  			makeAjaxCall(action);
  		});
  		
  		// filtrare moneda
  		$('#filter-moneda').change(function(e) {
  			var elementT 	= e.target;
  			var elementId 	= elementT.id;
  			var action 		= "filter";
  			// get selected value
  			moneda = elementT.value;
  			supplierName = $('#filter-furnizor').val();
  			numeMagazin = $('#filtru_magazin').val();
  			number = $('#filter-nr').val();
			datafactura = oldDateValue2;
			datainreg = oldDateValue;
			company_id = $('#filter-societate').val();
  			// reset start page 
  			start = 0;
  			// reset count
  			total = 0;
  			// make ajax call
  			makeAjaxCall(action);
  		});
  		
  		$('#filter-nr').keyup(function (e) {
  		    if ($("#filter-nr").is(":focus") && (e.keyCode == 13)) {
  				var elementT 	= e.target;
  				var elementId 	= elementT.id;
  				var action 		= "filter";
  				// get selected value
  				number = elementT.value;
  	  			moneda = $('#filter-moneda').val();
  	  			supplierName = $('#filter-furnizor').val();
  	  			numeMagazin = $('#filtru_magazin').val();
  				datafactura = oldDateValue2;
  				datainreg = oldDateValue;
  				company_id = $('#filter-societate').val();
  				if (number == "") {
  					number = 0;
  				}
  				// reset start page 
  				start = 0;
  				// reset count
  				total = 0;
  				// make ajax call
  				makeAjaxCall(action);
  		    }
  		});
  		
  		
  		function makeAjaxCall(action) {
  			$.ajax({
  				type: "POST",
  				url: "<%=ajaxURL%>",
  				data: 	"<portlet:namespace />action=" + action + 
  						"&<portlet:namespace />start=" + start + 
  						"&<portlet:namespace />count=" + count +
  						"&<portlet:namespace />total=" + total +
  						"&<portlet:namespace />moneda=" + moneda +
  						"&<portlet:namespace />number=" + number +
  						"&<portlet:namespace />company_id=" + company_id +
  						"&<portlet:namespace />datafactura=" + datafactura +
  						"&<portlet:namespace />datainreg=" + datainreg +
  						"&<portlet:namespace />type=" + type +
  						"&<portlet:namespace />nume_magazin=" + numeMagazin +
  						"&<portlet:namespace />supplier_name=" + supplierName,
  				success: function(msg) {
  					// get table data
  					if (msg != ""){
  						var jsonEntries = jQuery.parseJSON(msg);
  						total = jsonEntries.total;
  						$('#numberOfInvoices').html("");
  						$('#numberOfInvoices').html("Numarul de facturi nealocate este : " + total);
  						dataTable.set('data', eval(jsonEntries.values));
  					} else {
  						dataTable.set('data', []);
  					}
  					
  					// set status of navigation buttons on page load
  					setNavigationButtonsState('<portlet:namespace />');
  				}
  			});
  		}
		
		function dateChanged(input) {
			console.log('date changed: ' + input.val());
			oldDateValue2 = input.val();
			// get selected value
			if ($('#<portlet:namespace />datainreg_header').val() == ""){
				datainreg = "";
			} else {
				datainreg = $('#<portlet:namespace />datainreg_header').val();
			}
			datafactura = oldDateValue2;
			

			console.log(datafactura + " " + datainreg);
  			moneda = $('#filter-moneda').val();
  			supplierName = $('#filter-furnizor').val();
  			numeMagazin = $('#filtru_magazin').val();
  			number = $('#filter-nr').val();
			company_id = $('#filter-societate').val();
			// reset start page 
			start = 0;
			// reset count
			total = 0;
			action = "filter";
			// make ajax call
			makeAjaxCall(action);
		}
		
		var datepicker2 = new Y.DatePicker({
			trigger : '#<portlet:namespace />datafactura_header',
			mask : '%Y-%m-%d',
			popover : {
				zIndex : 1
			},
			popover: {
				zIndex : 1,
		          toolbars: {
		            header: [[
		              {
		                icon:'icon-trash',
		                label: 'Reseteaza filtru',
		                on: {
		                  click: function() {
		                	  datepicker2.clearSelection();
		                  }
		                }
		              }]]
		          }
			},
			after: {
	            selectionChange: function(event) {
	            	if (oldDateValue2 != $('#<portlet:namespace />datafactura_header').val()) {
	            		oldDateValue2 = $('#<portlet:namespace />datafactura_header').val();
	            		dateChanged($('#<portlet:namespace />datafactura_header'));
	            	}
	            	//$('#data_filter').html('');
	            }
	        },
			calendar : {
				dateFormat : '%Y-%m-%d'
			}
		});
		

		function dateChanged1(input) {
			console.log('date changed: ' + input.val());
			oldDateValue = input.val();
			if ($('#<portlet:namespace />datainreg_header').val() == ""){
				datafactura = "";
			} else {
				dataFactura = $('#<portlet:namespace />datainreg_header').val();
			}
			
			// get selected value
			datainreg = oldDateValue;
			console.log(datafactura + " " + datainreg);
  			moneda = $('#filter-moneda').val();
  			supplierName = $('#filter-furnizor').val();
  			numeMagazin = $('#filtru_magazin').val();
  			number = $('#filter-nr').val();
			company_id = $('#filter-societate').val();
			// reset start page 
			start = 0;
			// reset count
			total = 0;
			action = "filter";
			// make ajax call
			makeAjaxCall(action);
		}
		
		
		var datepicker = new Y.DatePicker({
			trigger : '#<portlet:namespace />datainreg_header',
			mask : '%Y-%m-%d',
			popover : {
				zIndex : 1
			},
			popover: {
				zIndex : 1,
		          toolbars: {
		            header: [[
		              {
		                icon:'icon-trash',
		                label: 'Reseteaza filtru',
		                on: {
		                  click: function() {
		                	  datepicker.clearSelection();
		                  }
		                }
		              }]]
		          }
			},
			calendar : {
				dateFormat : '%Y-%m-%d'
			},
			after: {
	            selectionChange: function(event) {
	            	if (oldDateValue != $('#<portlet:namespace />datainreg_header').val()) {
	            		oldDateValue = $('#<portlet:namespace />datainreg_header').val();
	            		dateChanged1($('#<portlet:namespace />datainreg_header'));
	            	}
	            	//$('#data_filter').html('');
	            }
	        }
		});
});

$('.doc_DPI').click(function(e){
	var ml = dataTableSplit.data, msg = '', template = '';
	var json = ml.toJSON();
	var tableContent = JSON.stringify(json);
	var sThisVal = (this.checked ? $(this).val() : "");
	console.log("this val : "+sThisVal);
	//if the table is empty and the checkbox is unchecked and the invoice still has store lines without dpi 
	console.log("table content : " + tableContent);
	console.log (hasStoreLinesWithoutDpi[invoice_id]);
	if (sThisVal == "" && hasStoreLinesWithoutDpi[invoice_id] == true){
		makeAjaxCall("getStoreLinesByInvoiceId");
		 $('#overlay').hide();
	} else {
		$('#assoc_store_lines').hide();
		isDisplayed = false;
		$('#<portlet:namespace />saveNewDpisButton').hide();
		dataTableSplit.set('data', []);
	}
	
});

$('#<portlet:namespace />saveNewDpis').click(function(e){
	var mlns = dataTableSplit.data, msg = '', template = '';
	var invoiceHasAllDpis = true;
	mlns.each(function (item, i) {
			var data = item.getAttrs(['store_line_id', 'id_dpi']);
			if (data.id_dpi == "0"){
				invoiceHasAllDpis = false;
			} else {
				updateDpisOnStoreLines(data.store_line_id, data.id_dpi, "updateDpiOnStoreLine");
			}
	});
	if (invoiceHasAllDpis == false){
		$('#docs_DPI').prop('checked', true);
	} else {
		hasStoreLinesWithoutDpi[invoice_id] = false;
		var mlt = dataTable.data, msg = '', template = '';
		var rows = [];
		mlt.each(function(item, i){
			var data = item.getAttrs(['id', 'inv_number', 'created', 'supplier_code', 'supplier_name', 'inv_date', 
			                         		 'pay_date', 'total_with_vat_curr', 'currency', 'company_name', 'missing_docs']);
			if(data.id == invoice_id){
				var miss = data.missing_docs;
				if (miss.length > 3){
					if (miss.indexOf("DPI") == 0){
						data.missing_docs = miss.replace("DPI,", "");
					}else {
						data.missing_docs = miss.replace(",DPI", "");
					}
				} else {
					data.missing_docs = miss.replace("DPI", "");
				} 
			}
			rows.push(data);
		});
		dataTable.set('data', rows);
		
		$('#docs_DPI').prop('checked', false);
		mustSubmit = true;
	}
	$('#assoc_store_lines').hide();
	isDisplayed = false;
	$('#<portlet:namespace />saveNewDpisButton').hide();
	$('#overlay').show();
});

function makeAjaxCall(action) {
		$.ajax({
			type: "POST",
			url: "<%=ajaxURL%>",
			data: 	"<portlet:namespace />invoice_id=" + invoice_id +
					"&<portlet:namespace />action=" + action,
			success: function(msg) {
				// get table data
				if (msg != ""){
					var jsonEntries = jQuery.parseJSON(msg);
					dataTableSplit.set('data', eval(jsonEntries.values));
				} else {
					dataTableSplit.set('data', []);
				}
				isDisplayed = true;
				$('#assoc_store_lines').show();
				$('#<portlet:namespace />saveNewDpisButton').show();
			}
		});
}

function updateDpisOnStoreLines(store_line_id, id_dpi, action) {
	$.ajax({
		type: "POST",
		url: "<%=ajaxURL%>",
		data: 	"<portlet:namespace />store_line_id=" + store_line_id +
				"&<portlet:namespace />id_dpi=" + id_dpi +
				"&<portlet:namespace />action=" + action,
		success: function(msg) {
			console.log("MSG : " + msg);
			// get table data
			if (msg == ""){
				alert("A aparut o eroare in procesul de modificare al dpi-ului");
			}
		}
	});
}

function checkMissingDocs(){
		if (mustSubmit == true && invoiceHasOnlyDpiMissing == true){
			alert("Factura selectata nu are documente lipsa. Va rugam salvati!");
			return;
		}
		$('#overlay').hide();
	}
	
$(document).ready(function() {
	isAuthorized = <%=isAuthorized%>;
});
</script>

<aui:layout>
	<aui:form target="_blank" style="display:none" method="post" action="<%=viewInvoiceURL.toString()%>" name="viewInvoice" id="viewInvoice">
		<aui:input name="id" id="id"></aui:input>
		<aui:input name="number_filter" id="number_filter"></aui:input>
		<aui:input name="datainreg_filter" id="datainreg_filter"></aui:input>
		<aui:input name="supplier_name_filter" id="supplier_name_filter"></aui:input>
		<aui:input name="nume_magazin" id="nume_magazin"></aui:input>
		<aui:input name="datafactura_filter" id="datafactura_filter"></aui:input>
		<aui:input name="moneda_filter" id="moneda_filter"></aui:input>
		<aui:input name="company_id_filter" id="company_id_filter"></aui:input>
		<aui:input name="filter_doc_type" id="filter_doc_type"></aui:input>
		<aui:input name="source_page" id="source_page"></aui:input>
	</aui:form>
</aui:layout>
