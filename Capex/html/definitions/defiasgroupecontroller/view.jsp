<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme" %>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>
<%@ taglib uri="http://alloy.liferay.com/tld/aui" prefix="aui" %>
<%@ taglib uri="http://liferay.com/tld/util" prefix="liferay-util" %>

<liferay-theme:defineObjects />
<portlet:defineObjects />

<% //-- PAGINARE 
/*int start = 0;
int count = 15;
String grupa_sintetica = "";
String grupa_analitica = "";
int total = DefIAS.getAllFilteredCount(grupa_sintetica, grupa_analitica);*/
%>

<portlet:resourceURL id="ajaxURL" var="ajaxURL"/>
<script type="text/javascript">
var start=0;
var count=15;
var total=0;
//filter
var grupa_sintetica = "";
var grupa_analitica = "";
</script>
	
<%
	// retrieve currencies from the database
	// List<HashMap<String,Object>> allIAS = DefIAS.getAllFiltered(start, count, grupa_sintetica, grupa_analitica);
%>

<%-- Links --%>
<portlet:renderURL var="addEntryURL" >
	<portlet:param name="mvcPath" value="/html/definitions/defiasgroupecontroller/add_IAS.jsp"></portlet:param>
</portlet:renderURL>

<portlet:resourceURL id="exportAction" var="exportURL">
	<portlet:param name="action" value="exportExcel"/>
</portlet:resourceURL>


<liferay-ui:success key="insert_ok" message="insert_ok" />
<liferay-ui:success key="update_ok" message="update_ok" />

<aui:layout>
	<aui:column columnWidth="100" last="true" style="text-align:right">
		<a class="btn btn-primary" href="<%=exportURL.toString()%>"> Export Excel </a>
		<a class="btn btn-primary" href="<%=addEntryURL.toString()%>">Adauga grupa IAS</a>
	</aui:column>
</aui:layout>
<br/>
<aui:layout>
	<aui:column first="true" columnWidth="100" last="true">
		<div id="def-ias-groupe"></div>
	</aui:column>
</aui:layout>

<%-- HTML for PAGINATION --%>
<%@ include file="/html/includes/pagination.jsp" %>

<script type="text/javascript">
YUI().use( 'aui-datatable', 'aui-datatype', 'datatable-sort',
	function(Y) {
		function edit(sel_id) {
		    var ml  = dataTableIAS.data, msg = '', template = '';
	
		    ml.each(function (item, i) {
		        var data = item.getAttrs(['id', 'name', 'grupa_sintetica', 'grupa_analitica', 
		                                  'durata_minima', 'durata_maxima', 'durata_implicita', 
		                                  'cont_imob_in_fct', 'cont_imob_in_curs', 'cont_imob_avans', 
		                                  'status', 'modified', 'created']);

		        if (data.id == sel_id) {
					$('#<portlet:namespace />id').val(data.id);
					$('#<portlet:namespace />name').val(data.name);
					$('#<portlet:namespace />grupa_sintetica').val(data.grupa_sintetica);
					$('#<portlet:namespace />grupa_analitica').val(data.grupa_analitica);
					$('#<portlet:namespace />durata_minima').val(data.durata_minima);
					$('#<portlet:namespace />durata_maxima').val(data.durata_maxima);
					$('#<portlet:namespace />durata_implicita').val(data.durata_implicita);
					$('#<portlet:namespace />cont_imob_in_fct').val(data.cont_imob_in_fct);
					$('#<portlet:namespace />cont_imob_in_curs').val(data.cont_imob_in_curs);
					$('#<portlet:namespace />cont_imob_avans').val(data.cont_imob_avans);
					$('#<portlet:namespace />status').val(data.status);
					$('#<portlet:namespace />created').val(data.created);
					$('#<portlet:namespace />modified').val(data.modified);
					$('#<portlet:namespace />editIAS').submit();
		        }
		    });
		}
	
		// data array
		var remoteDataIAS = [];
		
		// COLUMN INFO and ATTRIBUTES
		var nestedColsIAS = [
			{ key: 'id', label: 'ID', className: 'hiddencol' },
			{ key: 'grupa_sintetica', label: '<input id="filter-gr-sintetica" name="filter-gr-sintetica" value="" placeholder="Grupa sintetica" style="width:140px;margin:0">' +
			'</input>'},
			{ key: 'grupa_analitica', label: '<input id="filter-gr-analitica" name="filter-gr-analitica" value="" placeholder="Grupa analitica" style="width:140px;margin:0">' +
			'</input>'},
			{ key: 'name', label: 'Denumire grupa'},
			{ key: 'durata_minima', label: 'Durata minima'},
			{ key: 'durata_maxima', label: 'Durata maxima'},
			{ key: 'cont_imob_in_fct', className : 'hiddencol'},
			{ key: 'cont_imob_in_curs', className : 'hiddencol'},
			{ key: 'cont_imob_avans', className : 'hiddencol'},
			{ key: 'durata_implicita', label: 'Durata implicita'},
			{ key: 'status', label: 'Status', allowHTML: true, formatter: function(o) { 
				if(o.value == 'A') {return '<font color="green">Activ<font>';} 
				else { return '<font color="red">Inactiv</font>'; } } },
			{ key: 'actiuni', label: 'Actiuni', allowHTML: true, 
				// gets the id of the row and creates an unique link for every row
				formatter: '<a href="#" id="rowias{id}" class="editrowias">Editare</a>' }		      
		];

		// TABLE INIT
		var dataTableIAS = new Y.DataTable({
		    columns: nestedColsIAS,
		    data: remoteDataIAS,
		    recordType: ['id', 'name', 'grupa_sintetica', 'grupa_analitica', 
		                 'cont_imob_in_fct', 'cont_imob_in_curs', 'cont_imob_avans',
		                 'durata_minima', 'durata_maxima', 'durata_implicita', 
		                 'status', 'modified', 'created'],
		    editEvent: 'click'
		}).render('#def-ias-groupe');
		
		//dataTableIAS.get('boundingBox').unselectable();
		
		// Also define a listener on the single TH checkbox to
		// toggle all of the checkboxes
		dataTableIAS.delegate('click', function (e) {
		    // undefined to trigger the emptyCellValue
		    var targetIas = e.target.get('id');
			edit(targetIas.replace('rowias',''));
		}, '.editrowias', dataTableIAS);
		
		// PAGINATION
		// set status of navigation buttons on page load
		setNavigationButtonsState('<portlet:namespace />');
		
		$('.navigation').click(function(e) {
			var elementT = e.target;
			var elementId = elementT.id;
			var action = "";
			
			// determine which button was pressed and set the value of start accordingly
			if (elementId.indexOf("first") > -1) { action = "first"; start = 0; }
			if (elementId.indexOf("prev") > -1) { action = "prev"; if (start - count <= 0) {start = 0} else { start -= count; } }
			if (elementId.indexOf("next") > -1) { action = "next"; start = parseInt(start) + parseInt(count); }
			if (elementId.indexOf("last") > -1) { action = "last"; start = parseInt(parseInt(total) / parseInt(count)) * count;}
			
			makeAjaxCall(action);
		});

		// filter code based on change
		$('#filter-gr-sintetica').keyup(function (e) {
  		    if ($("#filter-gr-sintetica").is(":focus") && (e.keyCode == 13)) {
				var elementT 	= e.target;
				var elementId 	= elementT.id;
				var action 		= "filter";
				// get selected value
				grupa_sintetica = elementT.value;
				grupa_analitica = $('#filter-gr-analitica').val();
				// reset start page 
				start = 0;
				// reset count
				total = 0;
				// make ajax call
				makeAjaxCall(action);
  		    }
		});
		
		$('#filter-gr-analitica').keyup(function (e) {
			if ($("#filter-gr-analitica").is(":focus") && (e.keyCode == 13)) {
				var elementT 	= e.target;
				var elementId 	= elementT.id;
				var action 		= "filter";
				// get selected value
				grupa_analitica = elementT.value;
				grupa_sintetica = $('#filter-gr-sintetica').val();
				// reset start page 
				start = 0;
				// reset count
				total = 0;
				// make ajax call
				makeAjaxCall(action);
			}
		});

		
		function makeAjaxCall(action) {
			$.ajax({
				type: 	"POST",
				url: 	"<%=ajaxURL%>",
				data: 	"<portlet:namespace />action=" + action + 
						"&<portlet:namespace />start=" + start +
						"&<portlet:namespace />count=" + count +
						"&<portlet:namespace />total=" + total +						
						"&<portlet:namespace />grupa_sintetica=" + grupa_sintetica +
						"&<portlet:namespace />grupa_analitica=" + grupa_analitica,
				success: function(msg) {
					// get table data
					if (msg != ""){
						var jsonEntries = jQuery.parseJSON(msg);
						total = jsonEntries.total;
						dataTableIAS.set('data', eval(jsonEntries.values));
					} else {
						dataTableIAS.set('data', []);
					}
					
					// set status of navigation buttons on page load
					setNavigationButtonsState('<portlet:namespace />');
				}
			});
		}
		// PAGINATION
		$(document).ready(function(){ makeAjaxCall("first");});
	}
);
</script>

<aui:form method="post" action="<%=addEntryURL.toString() %>" name="editIAS" id="editIAS" style="margin:0;padding:0">
	<aui:input type="hidden" id="id" name="id"/>
	<aui:input type="hidden" id="name" name="name"/>
	<aui:input type="hidden" id="grupa_sintetica" name="grupa_sintetica"/>
	<aui:input type="hidden" id="grupa_analitica" name="grupa_analitica"/>
	<aui:input type="hidden" id="durata_minima" name="durata_minima"/>
	<aui:input type="hidden" id="durata_maxima" name="durata_maxima"/>
	<aui:input type="hidden" id="durata_implicita" name="durata_implicita"/>
	<aui:input type="hidden" id="cont_imob_in_fct" name="cont_imob_in_fct"/>
	<aui:input type="hidden" id="cont_imob_in_curs" name="cont_imob_in_curs"/>
	<aui:input type="hidden" id="cont_imob_avans" name="cont_imob_avans"/>
	<aui:input type="hidden" id="status" name="status"/> 
	<aui:input type="hidden" id="created" name="created"/> 
	<aui:input type="hidden" id="modified" name="modified"/> 
</aui:form>