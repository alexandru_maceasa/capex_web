<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme" %>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>
<%@ taglib uri="http://alloy.liferay.com/tld/aui" prefix="aui" %>
<%@ taglib uri="http://liferay.com/tld/util" prefix="liferay-util" %>

<%@page import="com.profluo.ecm.model.vo.DefCategoryVo"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.List"%>

<liferay-theme:defineObjects />
<portlet:defineObjects />
	
<%
	// retrieve currencies from the database
	// List<HashMap<String,Object>> allCategories = DefCategory.getAll();
	List<HashMap<String,Object>> allCategories = DefCategoryVo.getInstance();
%>


<%-- Links --%>
<portlet:renderURL var="addEntryURL" >
	<portlet:param name="mvcPath" value="/html/definitions/defcategorycontroller/add_cat.jsp"></portlet:param>
</portlet:renderURL>

<portlet:resourceURL id="exportAction" var="exportURL"/>
	

<liferay-ui:success key="insert_ok" message="insert_ok" />
<liferay-ui:success key="update_ok" message="update_ok" />

<aui:layout>
	<aui:column columnWidth="100" last="true" style="text-align:right">
		<a class="btn btn-primary" href="<%=exportURL.toString()%>"> Export Excel </a>
		<a class="btn btn-primary" href="<%=addEntryURL.toString()%>">Adauga categorie</a>
	</aui:column>
</aui:layout>
<br/>
<aui:layout>
	<aui:column first="true" columnWidth="100" last="true">
		<div id="def-cat-group"></div>
	</aui:column>
</aui:layout>

<script type="text/javascript">
YUI().use( 'aui-datatable', 'aui-datatype', 'datatable-sort',
	function(Y) {
		function edit(sel_id) {
		    var ml  = dataTableCat.data, msg = '', template = '';
	
		    ml.each(function (item, i) {
		        var data = item.getAttrs(['id', 'code', 'name', 'flag_it', 'status', 'created', 'modified']);

		        if (data.id == sel_id) {
					$('#<portlet:namespace />id').val(data.id);
					$('#<portlet:namespace />code').val(data.code);
				    $('#<portlet:namespace />name').val(data.name);
					$('#<portlet:namespace />flag_it').val(data.flag_it);
					$('#<portlet:namespace />status').val(data.status);
					/* NO Longer used at category level
					$('#<portlet:namespace />ias_id').val(data.ias_id);
					$('#<portlet:namespace />ifrs_id').val(data.ifrs_id);
					*/
					$('#<portlet:namespace />modified').val(data.modified);
					$('#<portlet:namespace />created').val(data.created);					
					$('#<portlet:namespace />editCat').submit();
		        }
		    });
		}
	
		// data array

		var remoteDataCat = [
		    <% for (int i = 0; i < allCategories.size(); i++) { %>
     		{
   				id: <%=allCategories.get(i).get("id")%>, 
   				code: '<%=allCategories.get(i).get("code").toString().replaceAll("'", "\\\\'")%>',
   				name:'<%=allCategories.get(i).get("name").toString().replaceAll("'", "\\\\'")%>',
   				flag_it:'<%=allCategories.get(i).get("flag_it").toString().replaceAll("'", "\\\\'")%>',
   				status:'<%=allCategories.get(i).get("status")%>',
   				modified: '<%=allCategories.get(i).get("modified")%>',
   	   			created: '<%=allCategories.get(i).get("created")%>'
   			}
   			<% if (i != (allCategories.size() - 1)) { out.print(","); } %>
     		<% } %>
		];
		
		// COLUMN INFO and ATTRIBUTES
		var nestedColsCat = [
			{ key: 'id', label: 'ID', className: 'hiddencol' },
			{ key: 'code', label: 'Cod'},
			{ key: 'name', label: 'Nume'},
			{ key: 'flag_it', label: 'Clasificare', formatter: function(o) { 
					if(o.value == 0) {return 'Active';}
					else if(o.value == 1) { return 'IT-Hardware'; }
					else if(o.value == 2) { return 'IT-Software'; }
					else if(o.value == 3) { return 'Securitate'; }
					else if(o.value == 4) { return 'Deco'; }
					else { return ''; }
				} 
			},
			{ key: 'status', label: 'Status', allowHTML: true, formatter: function(o) { 
				if(o.value == 'A') {return '<font color="green">Activ<font>';} 
				else { return '<font color="red">Inactiv</font>'; } }
			},
			{ key: 'actiuni', label: 'Actiuni', allowHTML: true, 
				// gets the id of the row and creates an unique link for every row
				formatter: '<a href="#" id="rowcat{id}" class="editrowcat">Editare</a>' }		      
		];

		// TABLE INIT
		var dataTableCat = new Y.DataTable({
		    columns: nestedColsCat,
		    data: remoteDataCat,
		    recordType: ['id', 'code', 'name', 'flag_it', 'status', 'modified', 'created'],
		    editEvent: 'click'
		}).render('#def-cat-group');
		
		//dataTableCat.get('boundingBox').unselectable();
		
		// Also define a listener on the single TH checkbox to
		// toggle all of the checkboxes
		dataTableCat.delegate('click', function (e) {
			e.preventDefault();
		    // undefined to trigger the emptyCellValue
		    var targetCat = e.target.get('id');
			edit(targetCat.replace('rowcat',''));
		}, '.editrowcat', dataTableCat);
	}
);
</script>

<aui:form method="post" action="<%=addEntryURL.toString() %>" name="editCat" id="editCat" style="margin:0;padding:0">
	<aui:input type="hidden" id="id" name="id"/>
	<aui:input type="hidden" id="code" name="code"/>
	<aui:input type="hidden" id="name" name="name"/>
	<aui:input type="hidden" id="flag_it" name="flag_it"/>
	<aui:input type="hidden" id="status" name="status"/> 
	<aui:input type="hidden" id="modified" name="modified"/> 	
	<aui:input type="hidden" id="created" name="created"/> 
</aui:form>