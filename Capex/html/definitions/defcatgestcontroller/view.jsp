<%@page import="com.profluo.ecm.model.vo.DefCatGestVo"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.List"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme" %>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>
<%@ taglib uri="http://alloy.liferay.com/tld/aui" prefix="aui" %>
<%@ taglib uri="http://liferay.com/tld/util" prefix="liferay-util" %>

<%@ page import="java.util.ResourceBundle" %>
<%@ page import="java.util.Locale" %>

<liferay-theme:defineObjects />
<portlet:defineObjects />
	
<%
	locale = renderRequest.getLocale();
	ResourceBundle resmain = portletConfig.getResourceBundle(locale);
	// retrieve categories from the database
	List<HashMap<String,Object>> allCategories = DefCatGestVo.getInstance();
%>

<%-- Links --%>
<portlet:renderURL var="addEntryURL" >
	<portlet:param name="mvcPath" value="/html/definitions/defcatgestcontroller/add_cat_gest.jsp"></portlet:param>
</portlet:renderURL>

<liferay-ui:success key="insert_ok" message="insert_ok" />
<liferay-ui:success key="update_ok" message="update_ok" />

<aui:layout>
	<aui:column columnWidth="80" last="true" style="text-align:right">
		<a class="btn btn-primary" href="<%=addEntryURL.toString()%>">Adauga categorie gestiune</a>
	</aui:column>
</aui:layout>
<br/>
<aui:layout>
	<aui:column first="true" columnWidth="100" last="true">
		<div id="def-catgest"></div>
	</aui:column>
</aui:layout>

<script type="text/javascript">
YUI().use( 'aui-datatable', 'aui-datatype', 'datatable-sort',
	function(Y) {
		function edit(sel_id) {
		    var ml  = dataTable.data, msg = '', template = '';
	
		    ml.each(function (item, i) {
		        var data = item.getAttrs(['id', 'ref', 'status', 'modified', 'created']);

		        if (data.id == sel_id) {
					$('#<portlet:namespace />id').val(data.id);
				//	$('#<portlet:namespace />name').val(data.name);
					$('#<portlet:namespace />ref').val(data.ref);
					$('#<portlet:namespace />status').val(data.status);
					$('#<portlet:namespace />created').val(data.created);
					$('#<portlet:namespace />modified').val(data.modified);
					$('#<portlet:namespace />editCategory').submit();
		        }
		    });
		}
	
		// data array
		var remoteDataCatGest = [
		    <% for (int i = 0; i < allCategories.size(); i++) { %>
     		{
   				id: <%=allCategories.get(i).get("id")%>, 
   			   	ref: '<%=allCategories.get(i).get("ref").toString().replaceAll("'", "\\\\'") %>',
   				status: '<%=allCategories.get(i).get("status")%>',
   				modified: '<%=allCategories.get(i).get("modified")%>',
   				created: '<%=allCategories.get(i).get("created")%>'
   			}
   			<% if (i != (allCategories.size() - 1)) { %>,<% } %>
     		<% } %>
		];

		var clasif = {1:'IT Hardware', 2:'IT Software', 3:'Active.'};
		
		// COLUMN INFO and ATTRIBUTES
		var nestedColsCatGest = [
			{ key: 'id', label: 'ID' },
			{ key: 'ref', label: 'Referinta'},
			{ key: 'status', label: 'Status', allowHTML: true, formatter: function(o) { 
				if(o.value == 'A') {return '<font color="green">Activ<font>';} 
				else { return '<font color="red">Inactiv</font>'; } } },
			/*
			{ key: 'updated', label:  'Updated'},
			{ key: 'created', label:  'Created'},
			*/
			{ key: 'actiuni', label: 'Actiuni', allowHTML: true, 
				// gets the id of the row and creates an unique link for every row
				formatter: '<a href="#" id="row{id}" class="editrow">Editare</a>' }		      
		];

		// TABLE INIT
		var dataTable = new Y.DataTable({
		    columns: nestedColsCatGest,
		    data: remoteDataCatGest,
		    recordType: ['id', 'ref', 'status', 'modified', 'created'],
		    editEvent: 'click'
		}).render('#def-catgest');
		
		//dataTable.get('boundingBox').unselectable();
		
		// Also define a listener on the single TH checkbox to
		// toggle all of the checkboxes
		dataTable.delegate('click', function (e) {
		    // undefined to trigger the emptyCellValue
		    var target = e.target.get('id');
			edit(target.replace('row',''));
		}, '.editrow', dataTable);
	}
);
</script>

<aui:form method="post" action="<%=addEntryURL.toString() %>" name="editCategory" id="editCategory" style="margin:0;padding:0">
	<aui:input type="hidden" id="id" name="id"/>
	<aui:input type="hidden" id="ref" name="ref"/>
	<aui:input type="hidden" id="status" name="status"/> 
	<aui:input type="hidden" id="created" name="created"/> 
	<aui:input type="hidden" id="modified" name="modified"/> 
</aui:form>