<%@page import="com.profluo.ecm.model.vo.DefCompaniesVo"%>

<%@page import="java.util.HashMap"%>
<%@page import="java.util.List"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme" %>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>
<%@ taglib uri="http://alloy.liferay.com/tld/aui" prefix="aui" %>
<%@ taglib uri="http://liferay.com/tld/util" prefix="liferay-util" %>

<liferay-theme:defineObjects />
<portlet:defineObjects />
<portlet:resourceURL id="ajaxURL" var="ajaxURL"/>
<% // PAGINATION Start - env setup 
int start 	= 0; 
int count 	= 15;
String code = "";
String name = "";
//int total 	= DefStore.getAllFilteredCount(code, name);
int total 	= 0;
%>

<script type="text/javascript">
var start = "<%=start %>";
var count = "<%=count %>";
var total = "<%=total %>";
//filters
var code = "<%=code%>";
var name = "<%=name%>";
</script>
<%
	// retrieve currencies from the database
	// List<HashMap<String,Object>> allStores = DefStore.getAllFiltered(start, count, code, name);
	// get allCompanies VO
	List<HashMap<String,Object>> allCompanies = DefCompaniesVo.getInstance();
%>

<%-- Links --%>
<portlet:renderURL var="addEntryURL" >
	<portlet:param name="mvcPath" value="/html/definitions/defstorescontroller/add_store.jsp"></portlet:param>
</portlet:renderURL>

<liferay-ui:success key="insert_ok" message="insert_ok" />
<liferay-ui:success key="update_ok" message="update_ok" />
<%-- 
<aui:layout>
	<aui:column columnWidth="100" last="true" style="text-align:right">
		<a class="btn btn-primary" href="<%=addEntryURL.toString()%>">Adauga magazin</a>
	</aui:column>
</aui:layout>
<br/>
--%>
 
<aui:layout>
	<aui:column first="true" columnWidth="100" last="true">
		<div id="def-store-groupe"></div>
	</aui:column>
</aui:layout>

<%-- HTML for PAGINATION --%>
<%@ include file="/html/includes/pagination.jsp" %>

<script type="text/javascript">
YUI().use( 'aui-datatable', 'aui-datatype', 'datatable-sort', "datasource-get", "datasource-jsonschema", "datatable-datasource", 
		'datatable-paginator', 'datatype-number',
	function(Y) {
		function edit(sel_id) {
		    var ml  = dataTableStores.data, msg = '', template = '';
		    ml.each(function (item, i) {
		        var data = item.getAttrs(['id', 'store_id', 'store_name', 'company_name', 
		                                  'entity_code' ,'CUI', 'storetype', 'GLN', 'repart_on_ca', 'status', 'created', 'modified', 'cod_bfc']);

		        if (data.id == sel_id) {
					$('#<portlet:namespace />id').val(data.id);
					$('#<portlet:namespace />store_id').val(data.store_id);
				    $('#<portlet:namespace />store_name').val(data.store_name);
					$('#<portlet:namespace />company_name').val(data.company_name);
					$('#<portlet:namespace />entity_code').val(data.entity_code);
					$('#<portlet:namespace />CUI').val(data.CUI);
					$('#<portlet:namespace />storetype').val(data.storetype);
					$('#<portlet:namespace />GLN').val(data.GLN);	
					$('#<portlet:namespace />repart_on_ca').val(data.repart_on_ca);
					$('#<portlet:namespace />status').val(data.status);
					$('#<portlet:namespace />created').val(data.created);
					$('#<portlet:namespace />modified').val(data.modified);
					$('#<portlet:namespace />cod_bfc').val(data.cod_bfc);
					$('#<portlet:namespace />editStore').submit();
		        }
		    });
		}
	
		// data array
		var remoteDataStore = [

		];
		
		var nestedColsStore = [
			{ key: 'id', label: 'ID', className: "hiddencol" },
			{ key: 'store_id', label: '<input id="filter-cod" name="filter-cod" label="" placeholder="Cod magazin" style="width:140px;margin:0">' +
				'</input>'},
			{ key: 'store_name', label: 'Nume magazin'},
			{ key: 'company_name', label: '<select id="filter-nume" name="filter-nume" label="" style="width:100px;margin:0">' +
				'<option selected="selected" value="">Nume companie</option>' +
				<% for (int i = 0; i < allCompanies.size(); i++) { %>
				<% if (allCompanies.get(i).get("status").equals("A")) {%>
				<% out.println("'<option value=\"" + allCompanies.get(i).get("id") +"\">" + allCompanies.get(i).get("name") +"</option>' + "); %>
				<% } } %>
				'</select>'},
			{ key: 'entity_code', label: 'Cod companie'},
			{ key: 'CUI', label: 'CUI companie'},
			{ key: 'storetype', label: 'Tip Magazin', allowHTML: true, formatter: function(o) { 
				if (o.value == 'F') { return 'Franchisa'; }
				else if (o.value == 'H') { return 'Hypermarket'; }
				else if (o.value == 'S') { return 'Supermarket'; }
				else if (o.value == 'Z') { return 'Supeco'; }
				else if (o.value == 'W') { return 'Merchandising'; }
				else if (o.value == 'M') { return 'Militari Gal. Com.'; }
				else if (o.value == 'P') { return 'PSC'; }
				else { return ''; } } },
			{ key: 'GLN', label: 'GLN'},
			{ key: 'status', label: 'Status', allowHTML: true, formatter: function(o) { 
				if(o.value == 'A') {return '<font color="green">Activ<font>';} 
				else { return '<font color="red">Inactiv</font>'; } } },				
			{ key: 'actiuni', label: 'Actiuni', allowHTML: true, 
				formatter: '<a href="#" id="rowstore{id}" class="editrowstore">Editare</a>' },
			{key: 'created', className:"hiddencol" },
			{key: 'modified', className:"hiddencol" },
			{key: 'cod_bfc', className:"hiddencol" }
		];

		// TABLE INIT
		var dataTableStores = new Y.DataTable({
		    columns: nestedColsStore,
		    data: remoteDataStore,
		    recordType: ['id', 'store_id', 'store_name', 'company_name', 
                         'entity_code' ,'CUI', 'storetype', 'GLN', 'repart_on_ca', 'status', 'created', 'modified', 'cod_bfc'],
            autoSync: true
		}).render('#def-store-groupe');
		
		//dataTableStores.get('boundingBox').unselectable();
		
		// Also define a listener on the single TH checkbox to
		// toggle all of the checkboxes
		dataTableStores.delegate('click', function (e) {
		    // undefined to trigger the emptyCellValue
		    e.preventDefault();
		    var targetStore = e.target.get('id');
			edit(targetStore.replace('rowstore',''));
		}, '.editrowstore', dataTableStores);
		
		// PAGINATION
		// set status of navigation buttons on page load
		setNavigationButtonsState('<portlet:namespace />');
		$('.navigation').click(function(e) {
			var elementT 	= e.target;
			var elementId 	= elementT.id;
			var action 		= "";
			
			// determine which button was pressed and set the value of start accordingly
			if (elementId.indexOf("first") > -1)	{ action = "first"; start = 0; }
			if (elementId.indexOf("prev") > -1) 	{ action = "prev"; if (start - count <= 0) {start = 0} else { start -= count; } }
			if (elementId.indexOf("next") > -1) 	{ action = "next"; start = parseInt(start) + parseInt(count); }
			if (elementId.indexOf("last") > -1) 	{ action = "last"; start = parseInt(parseInt(total) / parseInt(count)) * count; }
			
			makeAjaxCall(action);
		});

		//get the initial value of the date input and use it in 
		//order not to make too many ajas request because of an 
		//existing bug in the alloy ui framework
		var oldDateValue = $('#<portlet:namespace />datafactura_header').val();
		
		// filter currency based on change
		$('#filter-cod').change(function(e) {
			var elementT 	= e.target;
			var elementId 	= elementT.id;
			var action 		= "filter";
			// get selected value
			code = elementT.value;
			name = $('#filter-nume').val();
			// reset start page 
			start = 0;
			// reset count
			total = 0;
			// make ajax call
			makeAjaxCall(action);
		});
		// filter currency based on change
		$('#filter-nume').change(function(e) {
			var elementT 	= e.target;
			var elementId 	= elementT.id;
			var action 		= "filter";
			// get selected value
			name = elementT.value;
			code = $('#filter-cod').val();
			// reset start page 
			start = 0;
			// reset count
			total = 0;
			// make ajax call
			makeAjaxCall(action);
		});
		
				
		// general method for ajax calls.
		function makeAjaxCall(action) {
			$.ajax({
				type: 	"POST",
				url: 	"<%=ajaxURL%>",
				data: 	"<portlet:namespace />action=" + action + 
						"&<portlet:namespace />start=" + start +
						"&<portlet:namespace />count=" + count +
						"&<portlet:namespace />total=" + total +						
						"&<portlet:namespace />code=" + code +
						"&<portlet:namespace />name=" + name,
				success: function(msg) {
					// get table data
					if (msg != ""){
						var jsonEntries = jQuery.parseJSON(msg);
						total = jsonEntries.total;
						dataTableStores.set('data', eval(jsonEntries.values));
					} else {
						dataTableStores.set('data', []);
					}
					
					// set status of navigation buttons on page load
					setNavigationButtonsState('<portlet:namespace />');
				}
			});
		}
		// PAGINATION
		
		// get All entries after page load
		$(document).ready(function (){
			makeAjaxCall("first");
		});
	}
);
</script>

<aui:form method="post" action="<%=addEntryURL.toString() %>" name="editStore" id="editStore" style="margin:0;padding:0">
	<aui:input type="hidden" id="id" name="id"/>
	<aui:input type="hidden" id="store_id" name="store_id"/>
	<aui:input type="hidden" id="store_name" name="store_name"/>
	<aui:input type="hidden" id="entity_code" name="entity_code"/>
	<aui:input type="hidden" id="CUI" name="CUI"/>
	<aui:input type="hidden" id="GLN" name="GLN"/>
	<aui:input type="hidden" id="company_name" name="company_name"/>
	<aui:input type="hidden" id="storetype" name="storetype"/>
	<aui:input type="hidden" id="status" name="status"/> 
	<aui:input type="hidden" id="created" name="created"/> 
	<aui:input type="hidden" id="modified" name="modified"/> 
	<aui:input type="hidden" id="cod_bfc" name="cod_bfc"/> 
</aui:form>