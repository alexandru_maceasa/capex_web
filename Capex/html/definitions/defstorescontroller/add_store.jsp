<%@page import="com.profluo.ec.carrefour.htmlutils.DropDownUtils"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.List"%>
<%@page import="com.profluo.ecm.model.db.DefStore"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme" %>

<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>
<%@ taglib uri="http://alloy.liferay.com/tld/aui" prefix="aui" %>
<%@ taglib uri="http://liferay.com/tld/util" prefix="liferay-util" %>


<liferay-theme:defineObjects />
<portlet:defineObjects />
	
<%
	String id = renderRequest.getParameter("id");
	List<HashMap<String,Object>> allBFC = DefStore.getAllBFC();
	String selectedBFC = renderRequest.getParameter("cod_bfc");
%>

<%-- Links used in the view --%>
<%-- Submit URL, mapped to the addInvoice method in the class Invoice.java --%>
<portlet:actionURL name="saveStore" var="submitURL" />

<portlet:renderURL var="backURL">
	<portlet:param name="mvcPath" value="/html/definitions/defstorescontroller/view.jsp"></portlet:param>
</portlet:renderURL>

<aui:form action="<%= submitURL %>" method="post">

<aui:fieldset>
	<% if (id == null) { %>
	<legend>Adaugare magazin</legend>
	<% } else { %>
	<legend>Editare magazin</legend>
	<% } %>
	
	<liferay-ui:success key="update_ok" message="update_ok" />
	<liferay-ui:error key="insert_nok" message="update_nok" />
	<liferay-ui:error key="insert_nok" message="insert_nok" />
	<liferay-ui:error key="params_nok" message="params_nok" />
	<liferay-ui:error key="entry_exists" message="entry_exists" />
	
	<% if (id != null) { %>
	<aui:layout>
		<aui:column columnWidth="25" first="true">
			<label style="padding-top:5px" for="id">ID: </label>
		</aui:column>
		<aui:column columnWidth="25" style="text-align:right">
			<aui:input label="" name="id" id="id" type="text" value="" style="width:90%" readonly="true"/>
		</aui:column>
	</aui:layout>
	<% } %>	
	<aui:layout>
		<aui:column columnWidth="25" first="true">
			<label style="padding-top:5px" for="store_name">Nume : </label>
		</aui:column>
		<aui:column columnWidth="25" style="text-align:right">
			<aui:input label="" name="store_name" id="store_name" type="text" value="" style="width:90%" readonly="true">
			</aui:input>
		</aui:column>
	</aui:layout>
	
	<aui:layout>
		<aui:column columnWidth="25" first="true">
			<label style="padding-top:5px" for="store_id">Cod magazin: </label>
		</aui:column>
		<aui:column columnWidth="25" style="text-align:right">
			<aui:input label="" name="store_id" id="store_id" type="text" value="" style="width:90%" readonly="true">
			</aui:input>
		</aui:column>
	</aui:layout>
	
	<aui:layout>
		<aui:column columnWidth="25" first="true">
			<label style="padding-top:5px" for="CUI">CUI: </label>
		</aui:column>
		<aui:column columnWidth="25" style="text-align:right">
			<aui:input label="" name="CUI" id="CUI" type="text" value="" style="width:90%" readonly="true">
			</aui:input>
		</aui:column>
	</aui:layout>
	
	<aui:layout>
		<aui:column columnWidth="25" first="true">
			<label style="padding-top:5px" for="company_id">Companie: </label>
		</aui:column>
		<aui:column columnWidth="25" style="text-align:right">
			<aui:input label="" name="company_name" id="company_name" type="text" value="" style="width:90%" readonly="true">
			</aui:input>
		</aui:column>
	</aui:layout>
	
	<aui:layout>
		<aui:column columnWidth="25" first="true">
			<label style="padding-top:5px" for="GLN">GLN: </label>
		</aui:column>
		<aui:column columnWidth="25" style="text-align:right">
			<aui:input label="" name="GLN" id="GLN" type="text" value="" style="width:90%">
			</aui:input>
		</aui:column>
	</aui:layout>
	
	<aui:layout>
		<aui:column columnWidth="25" first="true">
			<label style="padding-top:5px" for="storetype">Tip magazin: </label>
		</aui:column>
		<aui:column columnWidth="25" style="text-align:right">
			<aui:select id="storetype" name="storetype" label="" style="width:95%" required="true">
				<aui:option selected="selected" value="">Tip Magazin</aui:option>
				<aui:option value="F">Franchisa</aui:option>
				<aui:option value="S">Supermarket</aui:option>
				<aui:option value="H">Hypermarket</aui:option>
				<aui:option value="Z">Supeco</aui:option>
				<aui:option value="Q">Sediu</aui:option>
				<aui:option value="W">Merchandising</aui:option>
				<aui:option value="P">PSC</aui:option>
				<aui:option value="M">Militari Gal. Com.</aui:option>
			</aui:select>
		</aui:column>
	</aui:layout>
	
	<aui:layout>
		<aui:column columnWidth="25" first="true">
			<label for="<portlet:namespace />status" style="padding-top:5px">Status: </label>
		</aui:column>
		<aui:column columnWidth="25" style="text-align:right">
			<aui:select id="status" name="status" label="" style="width:95%" required="true">
				<aui:option selected="selected" value="">Status</aui:option>
				<aui:option value="A">Activ</aui:option>
				<aui:option value="I">Inactiv</aui:option>
			</aui:select>
		</aui:column>
	</aui:layout>
	
	<aui:layout>
		<aui:column columnWidth="25" first="true">
			<label for="<portlet:namespace />cod_bfc" style="padding-top:5px">Cod BFC: </label>
		</aui:column>
		<aui:column columnWidth="25" style="text-align:right">
			<aui:select id="cod_bfc" name="cod_bfc" label="" style="width:95%" required="true">
				<%= DropDownUtils.generateSimpleDropdown(allBFC, "cod", "nume", selectedBFC, "Cod BFC") %>
			</aui:select>
		</aui:column>
	</aui:layout>
	
	<% if (id != null) { %>
	<aui:layout>
		<aui:column columnWidth="25" first="true">
			<label style="padding-top:5px" for="created">Adaugat la data: </label>
		</aui:column>
		<aui:column columnWidth="25" style="text-align:right">
			<aui:input label="" name="created" id="created" type="text" value="" style="width:90%"  readonly="readonly"/>
		</aui:column>
	</aui:layout>
	
	<aui:layout>
		<aui:column columnWidth="25" first="true">
			<label style="padding-top:5px" for="modified">Modificat la data: </label>
		</aui:column>
		<aui:column columnWidth="25" style="text-align:right">
			<aui:input label="" name="modified" id="modified" type="text" value="" style="width:90%"  readonly="readonly"/>
		</aui:column>
	</aui:layout>
	<% } %>
	<aui:layout>
		<aui:column last="true">
			<a class="btn btn-primary-red" href="<%= backURL %>">Inapoi</a>
			<aui:button value="Salveaza" cssClass="btn btn-primary" style="margin:0px" type="submit"/>
		</aui:column>
	</aui:layout>
</aui:fieldset>
</aui:form>