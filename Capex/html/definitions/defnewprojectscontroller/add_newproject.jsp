<%@page import="com.profluo.ecm.model.vo.DefCompaniesVo"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.List"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme" %>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>
<%@ taglib uri="http://alloy.liferay.com/tld/aui" prefix="aui" %>
<%@ taglib uri="http://liferay.com/tld/util" prefix="liferay-util" %>

<liferay-theme:defineObjects />
<portlet:defineObjects />
<portlet:resourceURL id="ajaxURL" var="ajaxURL"/>	
<%
	String id = null;
	try {
		id = renderRequest.getParameter("id").toString();
	} catch(Exception e) {}
	
	String retea = null;
	try {
		retea = renderRequest.getParameter("storetype").toString();
	} catch(Exception e){}
	
	System.out.println("reteaa " + retea);
	System.out.println("id " + id);
	
	// retrieve all companies from DB
	List<HashMap<String,Object>> allCompanies = DefCompaniesVo.getInstance();
%>

<%-- Links used in the view --%>
<%-- Submit URL, mapped to the addInvoice method in the class Invoice.java --%>
<portlet:actionURL name="saveNewProj" var="submitURL" />

<portlet:renderURL var="backURL">
	<portlet:param name="mvcPath" value="/html/definitions/defnewprojectscontroller/view.jsp"></portlet:param>
</portlet:renderURL>

<aui:form action="<%= submitURL %>" method="post">

<aui:fieldset>
	<% if (id == null) { %>
	<legend>Adaugare proiect nou</legend>
	<% } else { %>
	<legend>Editare proiect nou</legend>
	<% } %>
	
	<liferay-ui:success key="update_ok" message="update_ok" />
	<liferay-ui:error key="insert_nok" message="update_nok" />
	<liferay-ui:error key="insert_nok" message="insert_nok" />
	<liferay-ui:error key="params_nok" message="params_nok" />
	<liferay-ui:error key="entry_exists" message="entry_exists" />
	<liferay-ui:error key="error" message="A aparut o eroare in sistem, va rugam incercati mai tarziu." />
	
	<% if (id != null) { %>
	<aui:layout>
		<aui:column columnWidth="25" first="true">
			<label style="padding-top:5px" for="id">ID: </label>
		</aui:column>
		<aui:column columnWidth="25"  style="text-align:right">
			<aui:input label="" name="id" id="id" type="text" value="" style="width:90%" readonly="readonly" />
		</aui:column>
	</aui:layout>
	<% } %>
	
	
	<aui:layout>
		<aui:column columnWidth="25" first="true">
			<label style="padding-top:5px" for="start_date">Data inceput: </label>
		</aui:column>
		<aui:column columnWidth="25"  style="text-align:right">
			<aui:input label="" name="start_date" id="start_date" type="text" value="" style="width:90%">
				 <aui:validator name="required" errorMessage="Campul este obligatoriu." />
			</aui:input>
		</aui:column>
	</aui:layout>
	<aui:layout>
		<aui:column columnWidth="25" first="true">
			<label style="padding-top:5px" for="end_date">Data sfarsit: </label>
		</aui:column>
		<aui:column columnWidth="25"  style="text-align:right">
			<aui:input label="" name="end_date" id="end_date" type="text" value="" style="width:90%">
				 <aui:validator name="required" errorMessage="Campul este obligatoriu." />
			</aui:input>
		</aui:column>
	</aui:layout>
	<aui:layout>
		<aui:column columnWidth="25" first="true">
			<label style="padding-top:5px" for="retea">Retea: </label>
		</aui:column>
		<aui:column columnWidth="25" style="text-align:right">
			<aui:select id="retea" name="retea" label="" style="width:95%" required="true">
				<aui:option selected="selected" value="0">Retea</aui:option>
				<aui:option value="1">Super</aui:option>
				<aui:option value="2">Hyper</aui:option>
				<aui:option value="3">Supeco</aui:option>
				<aui:option value="4">Sediu</aui:option>
			</aui:select>
		</aui:column>
	</aui:layout>
	<aui:layout>
		<aui:column columnWidth="25" first="true">
			<label style="padding-top:5px" for="estimate_date">Data deschidere estimata: </label>
		</aui:column>
		<aui:column columnWidth="25"  style="text-align:right">
			<aui:input label="" name="estimate_date" id="estimate_date" type="text" value="" style="width:90%">
				 <aui:validator name="required" errorMessage="Campul este obligatoriu." />
			</aui:input>
		</aui:column>
	</aui:layout>
	<aui:layout>
		<aui:column columnWidth="25" first="true">
			<label style="padding-top:5px" for="open_date">Data deschidere reala: </label>
		</aui:column>
		<aui:column columnWidth="25"  style="text-align:right">
			<aui:input label="" name="open_date" id="open_date" type="text" value="" style="width:90%">
<%-- 			<aui:validator name="required" errorMessage="Campul este obligatoriu." /> --%>
<%-- 			<aui:validator name="date" errorMessage="Informatia nu este de tip data"></aui:validator> --%>
			</aui:input>
		</aui:column>
	</aui:layout>
	<aui:layout>
		<aui:column columnWidth="25" first="true">
			<label style="padding-top:5px" for="modified">Data deschidere reala fixa? </label>
		</aui:column>
		<aui:column  columnWidth="25" style="text-align:right">
			<aui:select id="lock_open_date" name="lock_open_date" label="" style="width:95%" required="true">
				<aui:option selected="selected" value="">Select</aui:option>
				<aui:option value="0">Nu</aui:option>
				<aui:option value="1">Da</aui:option>
			</aui:select>
		</aui:column>
	</aui:layout>
	<script type="text/javascript">
		YUI({lang:'ro'}).use('aui-datepicker', function(Y) {
			new Y.DatePicker({
				trigger : '#<portlet:namespace />start_date',
				mask : '%Y-%m-%d',
				popover : {
					zIndex : 1
				},
				calendar : {
					dateFormat : '%Y-%m-%d'
				}
			});
			new Y.DatePicker({
				trigger : '#<portlet:namespace />estimate_date',
				mask : '%Y-%m-%d',
				popover : {
					zIndex : 1
				},
				calendar : {
					dateFormat : '%Y-%m-%d'
				}
			});
			new Y.DatePicker({
				trigger : '#<portlet:namespace />end_date',
				mask : '%Y-%m-%d',
				popover : {
					zIndex : 1
				},
				calendar : {
					dateFormat : '%Y-%m-%d'
				}
			});
			new Y.DatePicker({
				trigger : '#<portlet:namespace />open_date',
				mask : '%Y-%m-%d',
				popover : {
					zIndex : 1
				},
				calendar : {
					dateFormat : '%Y-%m-%d'
				}
			});
		});
	</script>
	
	<aui:layout>
		<aui:column columnWidth="25" first="true">
			<label style="padding-top:5px" for="name">Nume proiect: </label>
		</aui:column>
		<aui:column columnWidth="25"  style="text-align:right">
			<aui:input label="" name="name" id="name" type="text" value="" style="width:90%">
				 <aui:validator name="required" errorMessage="Campul este obligatoriu." />
			</aui:input>
		</aui:column>
	</aui:layout>
	
	<aui:layout>
		<aui:column columnWidth="25" first="true">
			<label style="padding-top:5px" for="store_id">Cod magazin: </label>
		</aui:column>
		<aui:column columnWidth="25" style="text-align:right">
			<aui:input id="store_id" name="store_id" label="" value="" style="width:90%"></aui:input>
		</aui:column>
	</aui:layout>
	
	<aui:layout>
		<aui:column columnWidth="25" first="true">
			<label style="padding-top:5px" for="company_id">Societate: </label>
		</aui:column>
		<aui:column columnWidth="25" style="text-align:right">
			<aui:select id="company_id" name="company_id" label="" style="width:95%" required="true">
				<aui:option selected="selected" value="0">Societate</aui:option>
				<% for (int i = 0; i < allCompanies.size(); i++) { %>
					<% if (allCompanies.get(i).get("status").equals("A")) {%>
						<aui:option value="<%=allCompanies.get(i).get(\"id\")%>"><%=allCompanies.get(i).get("name")%></aui:option>
					<% } %>
				<% } %>
			</aui:select>
		</aui:column>
	</aui:layout>
	
	<aui:layout>
		<aui:column columnWidth="25" first="true">
			<label for="<portlet:namespace />owner" style="padding-top:5px">Responsabil: </label>
		</aui:column>
		<aui:column  columnWidth="25" style="text-align:right">
			<aui:select id="owner" name="owner" label="" style="width:95%" required="true">
				<aui:option selected="selected" value="0">Active</aui:option>
				<aui:option value="1">Property</aui:option>
				<aui:option value="2">Active + Property</aui:option>
			</aui:select>
		</aui:column>
	</aui:layout>
	
	<aui:layout>
		<aui:column columnWidth="25" first="true">
			<label for="<portlet:namespace />status" style="padding-top:5px">Status: </label>
		</aui:column>
		<aui:column  columnWidth="25" style="text-align:right">
			<aui:select id="status" name="status" label="" style="width:95%" required="true">
				<aui:option selected="selected" value="">Status</aui:option>
				<aui:option value="A">Activ</aui:option>
				<aui:option value="I">Inactiv</aui:option>
			</aui:select>
		</aui:column>
	</aui:layout>
	
	<% if (id != null) { %>
	<aui:layout>
		<aui:column columnWidth="25" first="true">
			<label style="padding-top:5px" for="created">Adaugat la data: </label>
		</aui:column>
		<aui:column columnWidth="25" style="text-align:right">
			<aui:input label="" name="created" id="created" type="text" value="" style="width:90%"  readonly="readonly"/>
		</aui:column>
	</aui:layout>
	
	<aui:layout>
		<aui:column columnWidth="25" first="true">
			<label style="padding-top:5px" for="modified">Modificat la data: </label>
		</aui:column>
		<aui:column columnWidth="25" style="text-align:right">
			<aui:input label="" name="modified" id="modified" type="text" value="" style="width:90%"  readonly="readonly"/>
		</aui:column>
	</aui:layout>
	<% } %>
	
	<aui:layout>
		<aui:column last="true">
			<a class="btn btn-primary-red" href="<%= backURL %>">Inapoi</a>
			<aui:button value="Salveaza" cssClass="btn btn-primary" style="margin:0px" type="submit"/>
		</aui:column>
	</aui:layout>
</aui:fieldset>
</aui:form>

<script type="text/javascript">

	$(document).ready(function(e){
		<% if (retea != null) { %>
			$('#<portlet:namespace/>retea').val(<%=retea%>);
		<% } %>
	});

	$('#<portlet:namespace/>retea').change(function(e){
		var startDateInput = $('#<portlet:namespace/>start_date').val();
		var startDateParts = startDateInput.split("-");
		var startDate = new Date(startDateParts[0], startDateParts[1], startDateParts[2]);
		if(!startDateInput) {
			alert("Pentru completarea automata a datei estimata de deschidere va rugam sa completati data de inceput.");
		} else {
			var tipRetea = $('#<portlet:namespace/>retea').val();
			$.ajax({
  				type: "POST",
  				url: "<%=ajaxURL%>",
  				data: 	"<portlet:namespace />action=get_estimate_date" +
  						"&<portlet:namespace />tip_retea=" + tipRetea + 
  						"&<portlet:namespace />start_date=" + startDateInput,
  				success: function(msg) {
  					//var dataEstimata = new Date(startDate.setMonth(startDate.getMonth()+(msg-1)));
  					$('#<portlet:namespace/>estimate_date').val(msg);
  					$('#<portlet:namespace/>open_date').val(msg);
  				}
  			});
			
		}
	});
</script>