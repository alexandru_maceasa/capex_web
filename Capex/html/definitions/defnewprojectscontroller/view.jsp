<%@page import="com.profluo.ecm.model.db.DefNewProj"%>
<%@page import="com.profluo.ecm.model.vo.DefNewProjVo"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.List"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme" %>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>
<%@ taglib uri="http://alloy.liferay.com/tld/aui" prefix="aui" %>
<%@ taglib uri="http://liferay.com/tld/util" prefix="liferay-util" %>

<%@ page import="java.util.ResourceBundle" %>
<%@ page import="java.util.Locale" %>

<liferay-theme:defineObjects />
<portlet:defineObjects />

<portlet:resourceURL id="ajaxURL" var="ajaxURL"/>

	<liferay-ui:success key="update_ok" message="update_ok" />
	<liferay-ui:error key="insert_nok" message="update_nok" />
	<liferay-ui:error key="insert_nok" message="insert_nok" />
	<liferay-ui:error key="params_nok" message="params_nok" />
	<liferay-ui:error key="entry_exists" message="entry_exists" />
	
<%
	int start = 0; 
	int count = 10;
	
	locale = renderRequest.getLocale();
	ResourceBundle resmain = portletConfig.getResourceBundle(locale);
	// retrieve currencies from the database
	List<HashMap<String,Object>> allNewProjects = DefNewProj.getAllFiltered(start, count, "", "", "", "");
	int total = DefNewProj.getAllFilteredCount("", "", "", "");
	
%>

<%-- Links --%>
<portlet:renderURL var="addEntryURL" >
	<portlet:param name="mvcPath" value="/html/definitions/defnewprojectscontroller/add_newproject.jsp"></portlet:param>
</portlet:renderURL>
<portlet:resourceURL id="exportAction" var="exportURL">
	<portlet:param name="action" value="exportExcel"/>
</portlet:resourceURL>

<liferay-ui:success key="insert_ok" message="insert_ok" />
<liferay-ui:success key="update_ok" message="update_ok" />

<aui:layout>
	<aui:column columnWidth="50">
		<aui:column columnWidth="35">
			<label style="padding-top:5px; padding-left:15px;" for="retea">Retea: </label>
		</aui:column>
		<aui:column columnWidth="15">
				<label style="padding-top:5px" for="nr_luni">Numar luni: </label>
			</aui:column>
	</aui:column>
	<aui:column columnWidth="50" last="true" style="text-align:right">
		<a class="btn btn-primary" href="<%=exportURL.toString()%>"> Export Excel </a>
		<a class="btn btn-primary" href="<%=addEntryURL.toString()%>">Adauga proiect nou</a>
	</aui:column>
	<aui:column columnWidth="100" style="height:40px;">
		<aui:column columnWidth="15">
			<aui:select id="retea" name="retea" label="" style="width:95%" required="true">
				<aui:option selected="selected" value="0">Retea</aui:option>
				<aui:option value="1">Super</aui:option>
				<aui:option value="2">Hyper</aui:option>
				<aui:option value="3">Supeco</aui:option>
				<aui:option value="4">Sediu</aui:option>
			</aui:select>
		</aui:column>
			<aui:column columnWidth="25">
				<aui:input type="text" id="nr_luni" name="nr_luni" label=""/>
			</aui:column>
	</aui:column>
	<aui:column columnWidth="100">
		<aui:column first="true">
			<a class="btn btn-primary"  href="#" id="modifica">Modifica data estimata</a>
		</aui:column>
	</aui:column>
</aui:layout>
<br/>
<aui:layout>
	<aui:column first="true" columnWidth="100" last="true">
		<div id="def-new-proj"></div>
	</aui:column>
</aui:layout>

<%-- HTML for PAGINATION --%>
<%@ include file="/html/includes/pagination.jsp" %>

<script type="text/javascript">

var start 			= <%=start%>;
var count 			= <%=count%>;
var total 			= <%=total%>;
var new_prj_name 	= "";
var store_code 		= "";
var company_name 	= "";
var status			= "";
var dataTableNewProj;

YUI().use( 'aui-datatable', 'aui-datatype', 'datatable-sort',
	function(Y) {
		function edit(sel_id) {
		    var ml  = dataTableNewProj.data, msg = '', template = '';
	
		    ml.each(function (item, i) {
		        var data = item.getAttrs(['id_prj', 'name', 'store_id', 'company_id', 'owner', 'start_date', 'end_date', 'open_date',
		                                  'estimate_date', 'company_name', 'status', 'modified', 'created','retea', 'lock_open_date']);

		        if (data.id_prj == sel_id) {
					$('#<portlet:namespace />id').val(data.id_prj);
					$('#<portlet:namespace />name').val(data.name);
					$('#<portlet:namespace />store_id').val(data.store_id);
					$('#<portlet:namespace />company_id').val(data.company_id);
					$('#<portlet:namespace />company_name').val(data.company_name);
					$('#<portlet:namespace />owner').val(data.owner);
					$('#<portlet:namespace />start_date').val(data.start_date);
					$('#<portlet:namespace />estimate_date').val(data.estimate_date);
					$('#<portlet:namespace />end_date').val(data.end_date);
					$('#<portlet:namespace />open_date').val(data.open_date);
					$('#<portlet:namespace />status').val(data.status);
					$('#<portlet:namespace />created').val(data.created);
					$('#<portlet:namespace />modified').val(data.modified);
					$('#<portlet:namespace />storetype').val(data.retea);
					$('#<portlet:namespace />lock_open_date').val(data.lock_open_date);
					$('#<portlet:namespace />editNewProj').submit();
		        }
		    });
		}
	
		// data array
		var remoteDataNewProj = [
		    <% for (int i = 0; i < allNewProjects.size(); i++) { %>
     		{
   				id_prj: <%=allNewProjects.get(i).get("id_prj")%>, 
   				name: '<%=allNewProjects.get(i).get("name").toString().replaceAll("'", "\\\\'") %>',
   				store_id: '<%=allNewProjects.get(i).get("store_id") %>',
   				company_id: '<%=allNewProjects.get(i).get("company_id") %>',
   				company_name: '<%=allNewProjects.get(i).get("company_name") %>',
   				start_date: '<%=allNewProjects.get(i).get("start_date")%>',
   				end_date: '<%=allNewProjects.get(i).get("end_date")%>',
   				open_date: '<%=allNewProjects.get(i).get("open_date")%>',
   				owner: '<%=allNewProjects.get(i).get("owner")%>',
   				status: '<%=allNewProjects.get(i).get("status")%>',
   				modified: '<%=allNewProjects.get(i).get("modified")%>',
   				created: '<%=allNewProjects.get(i).get("created")%>',
   				estimate_date: '<%=allNewProjects.get(i).get("estimate_date")%>',
   				retea: <%=allNewProjects.get(i).get("storetype")%>,
   				lock_open_date: <%=allNewProjects.get(i).get("lock_open_date")%>
   			}
   			<% if (i != (allNewProjects.size() - 1)) { out.print(","); } %>
     		<% } %>
		];
		
		// COLUMN INFO and ATTRIBUTES
		var nestedColsNewProj = [
			{ key: 'id_prj', label: 'ID' },
			{ key: 'retea', className: 'hiddencol' },
			{ key: 'lock_open_date', className: 'hiddencol' },
			{ key: 'company_id', className: "hiddencol"},
			{ key: 'estimate_date', className: "hiddencol"},
			{ key: 'name', label: '<input class="field" style="width:60px !important;margin:0" type="text" value="" id="filter_nume" name="filter_nume" placeholder="Nume"></input>'},
			{ key: 'store_id', label: '<input class="field" style="width:100px !important;margin:0" type="text" value="" id="filter_magazin" name="filter_magazin" placeholder="Cod magazin"></input>'},
			{ key: 'company_name', label: '<input class="field" style="width:150px !important;margin:0" type="text" value="" id="filter_companie" name="filter_companie" placeholder="Nume companie"></input>'},
			{ key: 'start_date', label: 'Data inceput'},
			{ key: 'end_date', label: 'Data sfarsit'},
			{ key: 'open_date', label: 'Data deschidere'},
			{ key: 'owner', label: 'Responsabil', formatter: function(o) { 
				if(o.value == 0) {return 'Departament Active';} 
				else if(o.value == 1) { return 'Departament Property'; }
				else return 'Active + Property'} },
			{ key: 'status', label: '<select id="filter_status" name="filter_status" label="" style="width:100px;margin:0">' +
	  				'<option selected="selected" value="">Status</option><option value="A">Active</option><option value="I">Inactive</option></select>', 
	  				allowHTML: true, formatter: function(o) { 
				if(o.value == 'A') {return '<font color="green">Activ<font>';} 
				else { return '<font color="red">Inactiv</font>'; } } },			
			{ key: 'actiuni', label: 'Actiuni', allowHTML: true, 
				// gets the id of the row and creates an unique link for every row
				formatter: '<a href="#" id="rownewproj{id_prj}" class="editrownewproj">Editare</a>' }
			 ];

		// TABLE INIT
		dataTableNewProj = new Y.DataTable({
		    columns: nestedColsNewProj,
		    data: remoteDataNewProj,
		    recordType: ['id_prj', 'name', 'status', 'store_id', 'estimate_date', 'company_id', 'owner', 'company_name', 'start_date', 'end_date', 'open_date', 'modified', 'created','retea', 'lock_open_date'],
		    editEvent: 'click'
		}).render('#def-new-proj');
		
		setNavigationButtonsState('<portlet:namespace />');
		$('.navigation').click(function(e) {
			var elementT 	= e.target;
			var elementId 	= elementT.id;
			var action 		= "";
			
			// determine which button was pressed and set the value of start accordingly
			if (elementId.indexOf("first")> -1)	{ action = "first"; start = 0; }
			if (elementId.indexOf("prev") > -1) 	{ action = "prev"; if (start - count <= 0) {start = 0} else { start -= count; } }
			if (elementId.indexOf("next") > -1) 	{ action = "next"; start = parseInt(start) + parseInt(count); }
			if (elementId.indexOf("last") > -1) 	{ action = "last"; start = parseInt(parseInt(total) / parseInt(count)) * count; if (start == total) { start -= count; } }
			
			makeAjaxCall(action);
		});
		
		$('#filter_nume').keyup(function (e) {
		    if ($("#filter_nume").is(":focus") && (e.keyCode == 13)) {
  			var action 		= "filter";
  			new_prj_name 	= e.target.value;
  			store_code 		= $('#filter_magazin').val();
  			company_name 	= $('#filter_companie').val();
  			status 			= $('#filter_status').val();
  			start = 0;
  			total = 0;
  			makeAjaxCall(action);
		    }
  		});
		
		$('#filter_magazin').keyup(function (e) {
		    if ($("#filter_magazin").is(":focus") && (e.keyCode == 13)) {
  			var action 		= "filter";
  			store_code	 	= e.target.value;
  			new_prj_name 	= $('#filter_nume').val();
  			company_name 	= $('#filter_companie').val();
  			status 			= $('#filter_status').val();
  			start = 0;
  			total = 0;
  			makeAjaxCall(action);
		    }
  		});
		
		$('#filter_companie').keyup(function (e) {
		    if ($("#filter_companie").is(":focus") && (e.keyCode == 13)) {
  			var action 		= "filter";
  			company_name	 	= e.target.value;
  			new_prj_name 	= $('#filter_nume').val();
  			store_code 		= $('#filter_magazin').val();
  			status 			= $('#filter_status').val();
  			start = 0;
  			total = 0;
  			makeAjaxCall(action);
		    }
  		});
		
		$('#filter_status').change(function (e) {
  			var action 		= "filter";
  			status	 		= e.target.value;
  			new_prj_name 	= $('#filter_nume').val();
  			store_code 		= $('#filter_magazin').val();
  			company_name 	= $('#filter_companie').val();
  			start = 0;
  			total = 0;
  			makeAjaxCall(action);
  		});
		
		function makeAjaxCall(action) {
			$.ajax({
				type: "POST",
				url: "<%=ajaxURL%>",
				data: 	"<portlet:namespace />action=" + action + 
						"&<portlet:namespace />start=" + start + 
						"&<portlet:namespace />count=" + count +
						"&<portlet:namespace />total=" + total +
						"&<portlet:namespace />new_prj_name=" + new_prj_name +
						"&<portlet:namespace />store_code=" + store_code +
						"&<portlet:namespace />company_name=" + company_name +
						"&<portlet:namespace />status=" + status,
				success: function(msg) {
					// get table data
					if (msg != ""){
						var jsonEntries = jQuery.parseJSON(msg);
						total = jsonEntries.total;
						dataTableNewProj.set('data', eval(jsonEntries.values));
					} else {
						dataTableNewProj.set('data', []);
					}
					// set status of navigation buttons on page load
					setNavigationButtonsState('<portlet:namespace />');
				}
			});
		}
		
		
		// Also define a listener on the single TH checkbox to
		// toggle all of the checkboxes
		dataTableNewProj.delegate('click', function (e) {
			e.preventDefault();
		    // undefined to trigger the emptyCellValue
		    var targetNewProj = e.target.get('id');
			edit(targetNewProj.replace('rownewproj',''));
		}, '.editrownewproj', dataTableNewProj);
		
		$('#<portlet:namespace/>retea').change(function(e){
			var action = "get_nr_of_months"
			$.ajax({
  				type: "POST",
  				url: "<%=ajaxURL%>",
  				data: 	"<portlet:namespace />action=" + action +
  						"&<portlet:namespace />tip_retea=" + $('#<portlet:namespace/>retea').val(),
  				success: function(msg) {
  					$('#<portlet:namespace/>nr_luni').val(msg);
  				}
  			});
		});
		
		$('#modifica').click(function(e){
			e.preventDefault();
			var retea = $('#<portlet:namespace/>retea').val();
			if(retea == 0) {
				alert("Va rugam selectati reteaua.");
			} else {
				var nrLuni = $('#<portlet:namespace/>nr_luni').val();
				if(!nrLuni) {
					alert("Va rugam selectati numarul de luni.");
				} else {
					var action = "update_estimate_date";
					$.ajax({
		  				type: "POST",
		  				url: "<%=ajaxURL%>",
		  				data: 	"<portlet:namespace />action=" + action +
		  						"&<portlet:namespace />storetype=" + retea + 
		  						"&<portlet:namespace />nr_luni=" + nrLuni,
		  				success: function(msg) {
		  					alert("Proiectele noi de pe canalul selectat au fost modificate cu succes");
		  				}
		  			});
				}
			}
		});
		
	});
</script>

<aui:form method="post" action="<%=addEntryURL.toString() %>" name="editNewProj" id="editNewProj" style="margin:0;padding:0">
	<aui:input type="hidden" id="id" name="id"/>
	<aui:input type="hidden" id="name" name="name"/>
	<aui:input type="hidden" id="store_id" name="store_id"/>
	<aui:input type="hidden" id="company_id" name="company_id"/>
	<aui:input type="hidden" id="company_name" name="company_name"/>
	<aui:input type="hidden" id="owner" name="owner"/>
	<aui:input type="hidden" id="start_date" name="start_date"/>
	<aui:input type="hidden" id="estimate_date" name="estimate_date"/>
	<aui:input type="hidden" id="end_date" name="end_date"/>
	<aui:input type="hidden" id="open_date" name="open_date"/>
	<aui:input type="hidden" id="status" name="status"/> 
	<aui:input type="hidden" id="created" name="created"/> 
	<aui:input type="hidden" id="modified" name="modified"/> 
	<aui:input type="hidden" id="storetype" name="storetype"/>
	<aui:input type="hidden" id="lock_open_date" name="lock_open_date"/>
</aui:form>