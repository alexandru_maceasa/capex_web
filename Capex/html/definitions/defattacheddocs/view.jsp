<%@page import="com.profluo.ecm.model.vo.DefAttDocsVo"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.List"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>

<portlet:defineObjects />

<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme" %>

<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>
<%@ taglib uri="http://alloy.liferay.com/tld/aui" prefix="aui" %>
<%@ taglib uri="http://liferay.com/tld/util" prefix="liferay-util" %>

<%@ page import="java.util.ResourceBundle" %>
<%@ page import="java.util.Locale" %>

<liferay-theme:defineObjects />
<portlet:defineObjects />
	
<%
	locale = renderRequest.getLocale();
	ResourceBundle resmain = portletConfig.getResourceBundle(locale);
	// retrieve currencies from the database
	List<HashMap<String,Object>> allAttDocs = DefAttDocsVo.getInstance();
%>

<%-- Links --%>
<portlet:renderURL var="addEntryURL" >
	<portlet:param name="mvcPath" value="/html/definitions/defattacheddocs/add_doc.jsp"></portlet:param>
</portlet:renderURL>

<liferay-ui:success key="insert_ok" message="insert_ok" />
<liferay-ui:success key="update_ok" message="update_ok" />

<aui:layout>
	<aui:column columnWidth="100" last="true" style="text-align:right">
		<a class="btn btn-primary" href="<%=addEntryURL.toString()%>">Adauga document justificativ</a>
	</aui:column>
</aui:layout>
<br/>
<aui:layout>
	<aui:column first="true" columnWidth="100" last="true">
		<div id="def-attached-doc"></div>
	</aui:column>
</aui:layout>

<script type="text/javascript">
YUI().use( 'aui-datatable', 'aui-datatype', 'datatable-sort',
	function(Y) {
		function edit(sel_id) {
		    var ml  = dataTableAttDoc.data, msg = '', template = '';
	
		    ml.each(function (item, i) {
		        var data = item.getAttrs(['id', 'name', 'status', 'modified', 'created']);

		        if (data.id == sel_id) {
					$('#<portlet:namespace />id').val(data.id);
					$('#<portlet:namespace />name').val(data.name);
					$('#<portlet:namespace />status').val(data.status);
					$('#<portlet:namespace />created').val(data.created);
					$('#<portlet:namespace />modified').val(data.modified);
					$('#<portlet:namespace />editAttDocs').submit();
		        }
		    });
		}
	
		// data array
		var remoteDataDoc = [
		    <% for (int i = 0; i < allAttDocs.size(); i++) { %>
     		{
   				id: <%=allAttDocs.get(i).get("id")%>, 
   				name: '<%=allAttDocs.get(i).get("name").toString().replaceAll("'", "\\\\'") %>',
   				status: '<%=allAttDocs.get(i).get("status")%>',
   				modified: '<%=allAttDocs.get(i).get("modified")%>',
   				created: '<%=allAttDocs.get(i).get("created")%>'
   			}
   			<% if (i != (allAttDocs.size() - 1)) { %>,<% } %>
     		<% } %>
		];
		
		// COLUMN INFO and ATTRIBUTES
		var nestedColsDoc = [
			{ key: 'id', label: 'ID' },
			{ key: 'name', label: 'Nume', formatter: '{value}'},
			{ key: 'status', label: 'Status', allowHTML: true, formatter: function(o) { 
				if(o.value == 'A') {return '<font color="green">Activ<font>';} 
				else { return '<font color="red">Inactiv</font>'; } } },
			{ key: 'actiuni', label: 'Actiuni', allowHTML: true, 
				// gets the id of the row and creates an unique link for every row
				formatter: '<a href="#" id="rowattdoc{id}" class="editrowattdoc">Editare</a>' }		      
		];

		// TABLE INIT
		var dataTableAttDoc = new Y.DataTable({
		    columns: nestedColsDoc,
		    data: remoteDataDoc,
		    recordType: ['id', 'name', 'status', 'modified', 'created'],
		    editEvent: 'click'
		}).render('#def-attached-doc');
		
		//dataTableAttDoc.get('boundingBox').unselectable();
		
		// Also define a listener on the single TH checkbox to
		// toggle all of the checkboxes
		dataTableAttDoc.delegate('click', function (e) {
		    // undefined to trigger the emptyCellValue
		    var targetDoc = e.target.get('id');
			edit(targetDoc.replace('rowattdoc',''));
		}, '.editrowattdoc', dataTableAttDoc);
	}
);
</script>

<aui:form method="post" action="<%=addEntryURL.toString() %>" name="editAttDocs" id="editAttDocs" style="margin:0;padding:0">
	<aui:input type="hidden" id="id" name="id"/>
	<aui:input type="hidden" id="name" name="name"/>
	<aui:input type="hidden" id="status" name="status"/> 
	<aui:input type="hidden" id="created" name="created"/> 
	<aui:input type="hidden" id="modified" name="modified"/> 
</aui:form>