<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme" %>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>
<%@ taglib uri="http://alloy.liferay.com/tld/aui" prefix="aui" %>
<%@ taglib uri="http://liferay.com/tld/util" prefix="liferay-util" %>

<%@ page import="com.profluo.ecm.model.db.DefIFRS"%>
<%@ page import="java.util.HashMap"%>
<%@ page import="java.util.List"%>
<%@ page import="java.util.ResourceBundle" %>
<%@ page import="java.util.Locale" %>

<liferay-theme:defineObjects />
<portlet:defineObjects />

<% //-- PAGINARE 
int start = 0;
int count = 15;
String grupa_sintetica = "";
String grupa_analitica = "";
int total = DefIFRS.getAllFilteredCount(grupa_sintetica, grupa_analitica);
%>
<script type="text/javascript">
var start="<%=start%>";
var count="<%=count%>";
var total="<%=total%>";
//filter
var grupa_sintetica="";
var grupa_analitica="";
</script>
<portlet:resourceURL id="ajaxURL" var="ajaxURL"/>

<portlet:resourceURL id="exportAction" var="exportURL">
	<portlet:param name="action" value="exportExcel"/>
</portlet:resourceURL>	
	
<%
	locale = renderRequest.getLocale();
	ResourceBundle resmain = portletConfig.getResourceBundle(locale);
	// retrieve currencies from the database
	List<HashMap<String,Object>> allIFRS = DefIFRS.getAllFiltered(start, count, grupa_sintetica, grupa_analitica);
%>

<%-- Links --%>
<portlet:renderURL var="addEntryURL" >
	<portlet:param name="mvcPath" value="/html/definitions/defgrupeifrscontroller/add_ifrs.jsp"></portlet:param>
</portlet:renderURL>

<liferay-ui:success key="insert_ok" message="insert_ok" />
<liferay-ui:success key="update_ok" message="update_ok" />

<aui:layout>
	<aui:column columnWidth="100" last="true" style="text-align:right">
		<a class="btn btn-primary" href="<%=exportURL.toString()%>"> Export Excel </a>
		<a class="btn btn-primary" href="<%=addEntryURL.toString()%>">Adauga grupa IFRS</a>
	</aui:column>
</aui:layout>
<br/>
<aui:layout>
	<aui:column first="true" columnWidth="100" last="true">
		<div id="def-ifrs-groupe"></div>
	</aui:column>
</aui:layout>

<%-- HTML for PAGINATION --%>
<%@ include file="/html/includes/pagination.jsp" %>


<script type="text/javascript">
YUI().use( 'aui-datatable', 'aui-datatype', 'datatable-sort',
	function(Y) {
		function edit(sel_id) {
		    var ml  = dataTableIFRS.data, msg = '', template = '';
	
		    ml.each(function (item, i) {
		        var data = item.getAttrs(['id', 'name','grupa_sintetica', 'grupa_analitica', 'durata_minima', 'durata_maxima', 
		                                  'durata_implicita', 'status', 'modified', 'created']);

		        if (data.id == sel_id) {
					$('#<portlet:namespace />id').val(data.id);
					$('#<portlet:namespace />name').val(data.name);
					$('#<portlet:namespace />grupa_sintetica').val(data.grupa_sintetica);
					$('#<portlet:namespace />grupa_analitica').val(data.grupa_analitica);
					$('#<portlet:namespace />durata_minima').val(data.durata_minima);
					$('#<portlet:namespace />durata_maxima').val(data.durata_maxima);
					$('#<portlet:namespace />durata_implicita').val(data.durata_implicita);
					$('#<portlet:namespace />status').val(data.status);
					$('#<portlet:namespace />created').val(data.created);
					$('#<portlet:namespace />modified').val(data.modified);
					$('#<portlet:namespace />editIFRS').submit();
		        }
		    });
		}
	
		// data array
		var remoteDataIFRS = [
		    <% for (int i = 0; i < allIFRS.size(); i++) { %>
     		{
   				id: <%=allIFRS.get(i).get("id")%>, 
   				name: '<%= allIFRS.get(i).get("name").toString().replaceAll("'", "\\\\'") %>',
   				grupa_sintetica:'<%=allIFRS.get(i).get("grupa_sintetica").toString().replaceAll("'", "\\\\'")%>',
   				grupa_analitica:'<%=allIFRS.get(i).get("grupa_analitica").toString().replaceAll("'", "\\\\'")%>',
   				durata_minima:'<%=allIFRS.get(i).get("durata_minima")%>',
   				durata_maxima:'<%=allIFRS.get(i).get("durata_maxima")%>',
   				durata_implicita:'<%=allIFRS.get(i).get("durata_implicita")%>',
   				status: '<%=allIFRS.get(i).get("status")%>',
   				modified: '<%=allIFRS.get(i).get("modified")%>',
   				created: '<%=allIFRS.get(i).get("created")%>'
   			}
   			<% if (i != (allIFRS.size() - 1)) { out.print(","); } %>
     		<% } %>
		];
		
		// COLUMN INFO and ATTRIBUTES
		var nestedColsIFRS = [
			{ key: 'id', label: 'ID', className: 'hiddencol' },
			{ key: 'grupa_sintetica', label: '<input id="filter-gr-sintetica" name="filter-gr-sintetica" label="" placeholder="Grupa sintetica" style="width:140px;margin:0">' +
			'</input>'},
			{ key: 'grupa_analitica', label: '<input id="filter-gr-analitica" name="filter-gr-analitica" label="" placeholder="Grupa analitica" style="width:140px;margin:0">' +
			'</input>'},
			{ key: 'name', label: 'Denumire grupa'},
			{ key: 'durata_minima', label: 'Durata minima'},
			{ key: 'durata_maxima', label: 'Durata maxima'},
			{ key: 'durata_implicita', label: 'Durata implicita'},
			{ key: 'status', label: 'Status', allowHTML: true, formatter: function(o) { 
				if(o.value == 'A') {return '<font color="green">Activ<font>';} 
				else { return '<font color="red">Inactiv</font>'; } } },
			{ key: 'actiuni', label: 'Actiuni', allowHTML: true, 
				// gets the id of the row and creates an unique link for every row
				formatter: '<a href="#" id="rowifrs{id}" class="edit">Editare</a>' }		      
		];

		// TABLE INIT
		var dataTableIFRS = new Y.DataTable({
		    columns: nestedColsIFRS,
		    data: remoteDataIFRS,
		    recordType: ['id', 'name', 'grupa_sintetica', 'grupa_analitica', 'durata_minima', 
		                 'durata_maxima', 'durata_implicita', 'status', 'modified', 'created'],
		    editEvent: 'click'
		}).render('#def-ifrs-groupe');
		
		//dataTableIFRS.get('boundingBox').unselectable();
		
		// Also define a listener on the single TH checkbox to
		// toggle all of the checkboxes
		dataTableIFRS.delegate('click', function (e) {
		    // undefined to trigger the emptyCellValue
		    var targetIfrs = e.target.get('id');
			edit(targetIfrs.replace('rowifrs',''));
		}, '.edit', dataTableIFRS);
		

		// PAGINATION
		// set status of navigation buttons on page load
		setNavigationButtonsState('<portlet:namespace />');
		$('.navigation').click(function(e) {
			var elementT = e.target;
			var elementId = elementT.id;
			var action = "";
			
			// determine which button was pressed and set the value of start accordingly
			if (elementId.indexOf("first") > -1) { action = "first"; start = 0; }
			if (elementId.indexOf("prev") > -1) { action = "prev"; if (start - count <= 0) {start = 0} else { start -= count; } }
			if (elementId.indexOf("next") > -1) { action = "next"; start = parseInt(start) + parseInt(count); }
			if (elementId.indexOf("last") > -1) { action = "last"; start = parseInt(parseInt(total) / parseInt(count)) * count;}
			
			makeAjaxCall(action);
		});

		// filter code based on change
		$('#filter-gr-sintetica').keyup(function (e) {
  		    if ($("#filter-gr-sintetica").is(":focus") && (e.keyCode == 13)) {
				var elementT 	= e.target;
				var elementId 	= elementT.id;
				var action 		= "filter";
				// get selected value
				grupa_sintetica = elementT.value;
				grupa_analitica = $('#filter-gr-analitica').val();
				
				// reset start page 
				start = 0;
				// reset count
				total = 0;
				// make ajax call
				makeAjaxCall(action);
  		    }
		});
		
		$('#filter-gr-analitica').keyup(function (e) {
			if ($("#filter-gr-analitica").is(":focus") && (e.keyCode == 13)) {
				var elementT 	= e.target;
				var elementId 	= elementT.id;
				var action 		= "filter";
				// get selected value
				grupa_analitica = elementT.value;
				grupa_sintetica = $('#filter-gr-sintetica').val();
				// reset start page 
				start = 0;
				// reset count
				total = 0;
				// make ajax call
				makeAjaxCall(action);
			}
		});

		
		function makeAjaxCall(action) {
			$.ajax({
				type: 	"POST",
				url: 	"<%=ajaxURL%>",
				data: 	"<portlet:namespace />action=" + action + 
						"&<portlet:namespace />start=" + start +
						"&<portlet:namespace />count=" + count +
						"&<portlet:namespace />total=" + total +						
						"&<portlet:namespace />grupa_sintetica=" + grupa_sintetica +
						"&<portlet:namespace />grupa_analitica=" + grupa_analitica
						,
				success: function(msg) {
					// get table data
					if (msg != ""){
						var jsonEntries = jQuery.parseJSON(msg);
						total = jsonEntries.total;
						dataTableIFRS.set('data', eval(jsonEntries.values));
					} else {
						dataTableIFRS.set('data', []);
					}
					
					// set status of navigation buttons on page load
					setNavigationButtonsState('<portlet:namespace />');
				}
			});
		}
		// PAGINATION
	}
);
</script>


<aui:form method="post" action="<%=addEntryURL.toString() %>" name="editIFRS" id="editIFRS" style="margin:0;padding:0">
	<aui:input type="hidden" id="id" name="id"/>
	<aui:input type="hidden" id="name" name="name"/>
	<aui:input type="hidden" id="grupa_sintetica" name="grupa_sintetica"/>
	<aui:input type="hidden" id="grupa_analitica" name="grupa_analitica"/>
	<aui:input type="hidden" id="durata_minima" name="durata_minima"/>
	<aui:input type="hidden" id="durata_maxima" name="durata_maxima"/>
	<aui:input type="hidden" id="durata_implicita" name="durata_implicita"/>
	<aui:input type="hidden" id="status" name="status"/> 
	<aui:input type="hidden" id="created" name="created"/> 
	<aui:input type="hidden" id="modified" name="modified"/> 
</aui:form>