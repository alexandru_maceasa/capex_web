<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme" %>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>
<%@ taglib uri="http://alloy.liferay.com/tld/aui" prefix="aui" %>
<%@ taglib uri="http://liferay.com/tld/util" prefix="liferay-util" %>

<liferay-theme:defineObjects />
<portlet:defineObjects />
	
<% String id = renderRequest.getParameter("id"); %>

<%-- Links used in the view --%>
<%-- Submit URL, mapped to the addInvoice method in the class Invoice.java --%>
<portlet:actionURL name="saveIFRS" var="submitURL" />

<portlet:renderURL var="backURL">
	<portlet:param name="mvcPath" value="/html/definitions/defgrupeifrscontroller/view.jsp"></portlet:param>
</portlet:renderURL>

<aui:form action="<%= submitURL %>" method="post">

<aui:fieldset>
	<% if (id == null) { %>
	<legend>Adaugare grupa IFRS</legend>
	<% } else { %>
	<legend>Editare grupa IFRS</legend>
	<% } %>

	<liferay-ui:error key="entry_exists" message="entry_exists" />
	<liferay-ui:success key="update_ok" message="update_ok" />
	<liferay-ui:error key="insert_nok" message="update_nok" />
	<liferay-ui:error key="insert_nok" message="insert_nok" />
	<liferay-ui:error key="params_nok" message="params_nok" />
	
	<% if (id != null) { %>
	<aui:layout>
		<aui:column columnWidth="25" first="true">
			<label style="padding-top:5px" for="id">ID: </label>
		</aui:column>
		<aui:column columnWidth="25" style="text-align:right">
			<aui:input label="" name="id" id="id" type="text" value="" style="width:90%" readonly="readonly" />
		</aui:column>
	</aui:layout>
	<% } %>
	
	<aui:layout>
		<aui:column columnWidth="25" first="true">
			<label style="padding-top:5px" for="name">Nume : </label>
		</aui:column>
		<aui:column columnWidth="25"  style="text-align:right">
			<aui:input label="" name="name" id="name" type="text" value="" style="width:90%">
				 <aui:validator name="required" errorMessage="Campul este obligatoriu." />
			</aui:input>
		</aui:column>
	</aui:layout>
	
	<aui:layout>
		<aui:column columnWidth="25" first="true">
			<label style="padding-top:5px" for="grupa_sintetica">Grupa sintetica: </label>
		</aui:column>
		<aui:column columnWidth="25"  style="text-align:right">
			<aui:input label="" name="grupa_sintetica" id="grupa_sintetica" type="text" value="" style="width:90%">
				 <aui:validator name="required" errorMessage="Campul este obligatoriu." />
			</aui:input>
		</aui:column>
	</aui:layout>
	
	<aui:layout>
		<aui:column columnWidth="25" first="true">
			<label style="padding-top:5px" for="grupa_analitica">Grupa analitica: </label>
		</aui:column>
		<aui:column columnWidth="25"  style="text-align:right">
			<aui:input label="" name="grupa_analitica" id="grupa_analitica" type="text" value="" style="width:90%">
			</aui:input>
		</aui:column>
	</aui:layout>
	
	<aui:layout>
		<aui:column columnWidth="25" first="true">
			<label style="padding-top:5px" for="durata_minima">Durata minima (luni): </label>
		</aui:column>
		<aui:column columnWidth="25" style="text-align:right">
			<aui:input label="" name="durata_minima" id="durata_minima" type="text" value="0" style="width:90%">
				 <aui:validator name="required" errorMessage="Campul este obligatoriu." />
				  <aui:validator name="digits" errorMessage="Introduceti doar cifre. Valoarea este exprimata in luni." />
			</aui:input>
		</aui:column>
	</aui:layout>
	
	<aui:layout>
		<aui:column columnWidth="25" first="true">
			<label style="padding-top:5px" for="durata_maxima">Durata maxima (luni): </label>
		</aui:column>
		<aui:column columnWidth="25" style="text-align:right">
			<aui:input label="" name="durata_maxima" id="durata_maxima" type="text" value="0" style="width:90%">
				 <aui:validator name="required" errorMessage="Campul este obligatoriu." />
				  <aui:validator name="digits" errorMessage="Introduceti doar cifre. Valoarea este exprimata in luni." />
			</aui:input>
		</aui:column>
	</aui:layout>
	
	<aui:layout>
		<aui:column columnWidth="25" first="true">
			<label style="padding-top:5px" for="durata_maxima">Durata implicita (luni): </label>
		</aui:column>
		<aui:column columnWidth="25"  style="text-align:right">
			<aui:input label="" name="durata_implicita" id="durata_implicita" type="text" value="0" style="width:90%">
				 <aui:validator name="required" errorMessage="Campul este obligatoriu." />
				  <aui:validator name="digits" errorMessage="Introduceti doar cifre. Valoarea este exprimata in luni." />
			</aui:input>
		</aui:column>
	</aui:layout>
	
	<aui:layout>
		<aui:column columnWidth="25" first="true">
			<label for="<portlet:namespace />status" style="padding-top:5px">Status: </label>
		</aui:column>
		<aui:column columnWidth="25" style="text-align:right">
			<aui:select id="status" name="status" label="" style="width:95%" required="true">
				<aui:option selected="selected" value="">Status</aui:option>
				<aui:option value="A">Activ</aui:option>
				<aui:option value="I">Inactiv</aui:option>
			</aui:select>
		</aui:column>
	</aui:layout>
	
	<% if (id != null) { %>
	<aui:layout>
		<aui:column columnWidth="25" first="true">
			<label style="padding-top:5px" for="created">Adaugat la data: </label>
		</aui:column>
		<aui:column columnWidth="25"  style="text-align:right">
			<aui:input label="" name="created" id="created" type="text" value="" style="width:90%"  readonly="readonly"/>
		</aui:column>
	</aui:layout>
	
	<aui:layout>
		<aui:column columnWidth="25" first="true">
			<label style="padding-top:5px" for="modified">Modificat la data: </label>
		</aui:column>
		<aui:column columnWidth="25"  style="text-align:right">
			<aui:input label="" name="modified" id="modified" type="text" value="" style="width:90%"  readonly="readonly"/>
		</aui:column>
	</aui:layout>
	<% } %>
	<aui:layout>
		<aui:column last="true">
			<a class="btn btn-primary-red" href="<%= backURL %>">Inapoi</a>
			<aui:button value="Salveaza" cssClass="btn btn-primary" style="margin:0px" type="submit"/>
		</aui:column>
	</aui:layout>
</aui:fieldset>
</aui:form>