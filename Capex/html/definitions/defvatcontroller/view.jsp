<%@page import="com.profluo.ecm.model.vo.DefVATVo"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.List"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>

<portlet:defineObjects />

<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme" %>

<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>
<%@ taglib uri="http://alloy.liferay.com/tld/aui" prefix="aui" %>
<%@ taglib uri="http://liferay.com/tld/util" prefix="liferay-util" %>

<%@ page import="java.util.ResourceBundle" %>
<%@ page import="java.util.Locale" %>

<liferay-theme:defineObjects />
<portlet:defineObjects />
	
<%
	locale = renderRequest.getLocale();
	ResourceBundle resmain = portletConfig.getResourceBundle(locale);
	// retrieve currencies from the database
	List<HashMap<String,Object>> allVAT = DefVATVo.getInstance();
%>

<%-- Links --%>
<portlet:renderURL var="addEntryURL" >
	<portlet:param name="mvcPath" value="/html/definitions/defvatcontroller/add_vat.jsp"></portlet:param>
</portlet:renderURL>

<liferay-ui:success key="insert_ok" message="insert_ok" />
<liferay-ui:success key="update_ok" message="update_ok" />

<aui:layout>
	<aui:column columnWidth="50" last="true" style="text-align:right">
		<a class="btn btn-primary" href="<%=addEntryURL.toString()%>">Adauga valoare TVA</a>
	</aui:column>
</aui:layout>
<br/>
<aui:layout>
	<aui:column first="true" columnWidth="100" last="true">
		<div id="def-vat"></div>
	</aui:column>
</aui:layout>

<script type="text/javascript">
YUI().use( 'aui-datatable', 'aui-datatype', 'datatable-sort',
	function(Y) {
		function edit(sel_id) {
		    var ml  = dataTableVAT.data, msg = '', template = '';
	
		    ml.each(function (item, i) {
		        var data = item.getAttrs(['id', 'ref', 'status', 'modified', 'created']);

		        if (data.id == sel_id) {
					$('#<portlet:namespace />id').val(data.id);
					$('#<portlet:namespace />ref').val(data.ref);
					$('#<portlet:namespace />status').val(data.status);
					$('#<portlet:namespace />created').val(data.created);
					$('#<portlet:namespace />modified').val(data.modified);
					$('#<portlet:namespace />editVAT').submit();
		        }
		    });
		}
	
		// data array
		var remoteDataVAT = [
		    <% for (int i = 0; i < allVAT.size(); i++) { %>
     		{
   				id: <%=allVAT.get(i).get("id")%>, 
   				ref: '<%=allVAT.get(i).get("ref").toString().replaceAll("'", "\\\\'") %>',
   				status: '<%=allVAT.get(i).get("status")%>',
   				modified: '<%=allVAT.get(i).get("modified")%>',
   				created: '<%=allVAT.get(i).get("created")%>'
   			}
   			<% if (i != (allVAT.size() - 1)) { %>,<% } %>
     		<% } %>
		];
		
		// COLUMN INFO and ATTRIBUTES
		var nestedColsVAT = [
			{ key: 'id', label: 'ID' },
			{ key: 'ref', label: 'Valoare', formatter: '{value}%'},
			{ key: 'status', label: 'Status'},
			{ key: 'actiuni', label: 'Actiuni', allowHTML: true, 
				// gets the id of the row and creates an unique link for every row
				formatter: '<a href="#" id="rowvat{id}" class="editrowvat">Editare</a>' }		      
		];

		// TABLE INIT
		var dataTableVAT = new Y.DataTable({
		    columns: nestedColsVAT,
		    data: remoteDataVAT,
		    recordType: ['id', 'ref', 'status', 'modified', 'created'],
		    editEvent: 'click'
		}).render('#def-vat');
		
		//dataTableVAT.get('boundingBox').unselectable();
		
		// bind click event on edit link on each row
		dataTableVAT.delegate('click', function (e) {
		    // undefined to trigger the emptyCellValue
		    var target = e.target.get('id');
			edit(target.replace('rowvat',''));
		}, '.editrowvat', dataTableVAT);
	}
);
</script>

<aui:form method="post" action="<%=addEntryURL.toString() %>" name="editVAT" id="editVAT" style="margin:0;padding:0">
	<aui:input type="hidden" id="id" name="id"/>
	<aui:input type="hidden" id="ref" name="ref"/>
	<aui:input type="hidden" id="status" name="status"/> 
	<aui:input type="hidden" id="created" name="created"/> 
	<aui:input type="hidden" id="modified" name="modified"/> 
</aui:form>