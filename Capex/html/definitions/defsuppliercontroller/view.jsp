<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme" %>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>
<%@ taglib uri="http://alloy.liferay.com/tld/aui" prefix="aui" %>
<%@ taglib uri="http://liferay.com/tld/util" prefix="liferay-util" %>

<liferay-theme:defineObjects />
<portlet:defineObjects />

<portlet:resourceURL id="ajaxURL" var="ajaxURL"/>

<script type="text/javascript">
var start = 0;
var count = 15;
var total = 0;
//filter
var code = "";
</script>

<aui:layout>
	<aui:column first="true" columnWidth="100" last="true">
		<div id="define-suppliers"></div>
	</aui:column>
</aui:layout>

<%-- HTML for PAGINATION --%>
<%@ include file="/html/includes/pagination.jsp" %>

<script type="text/javascript">
YUI().use( 'aui-datatable', 'aui-datatype', 'datatable-sort',
function(Y) {
	// data array
	var remoteDataSupp=[];
		
	var nestedColsSupp = [
  			{ key: 'id', label: 'ID',className: 'hiddencol' },
  			{ key: 'supplier_code', label: '<input id="filter-cod" name="filter-cod" label="" placeholder="Cod" style="width:30px;margin:0"></input>'},
  			{ key: 'name', label: 'Nume'},
  			{ key: 'bank', label: 'Banca'},
  			{ key: 'bank_acc', label: 'Cont bancar'},
  			{ key: 'swift', label: 'Swift'},
  			{ key: 'phone', label:'Telefon',className:'hiddencol'},
  			{ key: 'email', label:'Email', className:'hiddencol'},
  			{ key: 'payment_term', label: 'Termen de plata'},
  			{ key: 'blocked', label: 'Blocat', allowHTML: true, formatter: function(o) { 
				if(o.value == 0) {return '<font color="green">0<font>';} 
				else { return '<font color="red">1</font>'; } } },
  			{ key: 'payment_code', label: 'Cod de plata', formatter: '{value}'},
  			{ key: 'ro', label:'RO'},
  			{ key: 'reg_com', label:'J'},
  			{ key: 'cui', label: 'CUI'}
  		];

   		// TABLE INIT
   		var dataTableSupp = new Y.DataTable({
   		    columns: nestedColsSupp,
   		    data: remoteDataSupp,
   		    recordType: ['id', 'supp_id', 'supp_name','bank', 'bank_acc','swift','phone','email', 'payment_term','block_id','payment_code' ,'ro', 'reg_term','cui','cuiint']
   		}).render('#define-suppliers');
   		
   		dataTableSupp.get('boundingBox').unselectable();
   		
   		// Also define a listener on the single TH checkbox to
   		// toggle all of the checkboxes
   		dataTableSupp.delegate('click', function (e) {
   		    // undefined to trigger the emptyCellValue
   		    var targetSupp = e.target.get('id');
   			edit(targetSupp.replace('rowsupp',''));
   		}, '.editrowsupp', dataTableSupp);
        		
        // PAGINATION
		// set status of navigation buttons on page load
		setNavigationButtonsState('<portlet:namespace />');
   		$('.navigation').click(function(e) {
   			var elementT = e.target;
   			var elementId = elementT.id;
   			var action = "";
   			
   			// determine which button was pressed and set the value of start accordingly
   			if (elementId.indexOf("first") > -1) { action = "first"; start = 0; }
   			if (elementId.indexOf("prev") > -1) { action = "prev"; if (start - count <= 0) {start = 0} else { start -= count; } }
   			if (elementId.indexOf("next") > -1) { action = "next"; start = parseInt(start) + parseInt(count); }
   			if (elementId.indexOf("last") > -1) { action = "last"; start = parseInt(parseInt(total) / parseInt(count)) * count;}
   			
   			makeAjaxCall(action);
   		});

     		// filter code based on change
     		$('#filter-cod').change(function(e) {
     			var elementT 	= e.target;
     			var elementId 	= elementT.id;
     			var action 		= "filter";
     			// get selected value
     			code = elementT.value;
     			// reset start page 
     			start = 0;
     			// reset count
     			total = 0;
     			// make ajax call
     			makeAjaxCall(action);
     		});
     		
     		function makeAjaxCall(action) {
     			$.ajax({
     				type: 	"POST",
     				url: 	"<%=ajaxURL%>",
     				data: 	"<portlet:namespace />action=" + action + 
     						"&<portlet:namespace />start=" + start +
     						"&<portlet:namespace />count=" + count +
     						"&<portlet:namespace />total=" + total +						
     						"&<portlet:namespace />code=" + code,
     				success: function(msg) {
     					// get table data
     					if (msg != ""){
     						var jsonEntries = jQuery.parseJSON(msg);
     						total = jsonEntries.total;
     						dataTableSupp.set('data', eval(jsonEntries.values));
     					} else {
     						dataTableSupp.set('data', []);
     					}
     					
     					// set status of navigation buttons on page load
     					setNavigationButtonsState('<portlet:namespace />');
     				}
     			});
     		}
     		// PAGINATION
     		$(document).ready(function(){ makeAjaxCall("first"); });
     	}
	);	       
</script>

