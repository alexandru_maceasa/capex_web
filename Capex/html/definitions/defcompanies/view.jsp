<%@page import="com.profluo.ecm.model.vo.DefCompaniesVo"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.List"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>

<portlet:defineObjects />

<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme" %>

<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>
<%@ taglib uri="http://alloy.liferay.com/tld/aui" prefix="aui" %>
<%@ taglib uri="http://liferay.com/tld/util" prefix="liferay-util" %>

<%@ page import="java.util.ResourceBundle" %>
<%@ page import="java.util.Locale" %>

<liferay-theme:defineObjects />
<portlet:defineObjects />
	
<%
	locale = renderRequest.getLocale();
	ResourceBundle resmain = portletConfig.getResourceBundle(locale);
	// retrieve currencies from the database
	List<HashMap<String,Object>> allCompanies = DefCompaniesVo.getInstance();
%>

<%-- Links --%>
<portlet:renderURL var="addEntryURL" >
	<portlet:param name="mvcPath" value="/html/definitions/defcurrencycontroller/add_company.jsp"></portlet:param>
</portlet:renderURL>

<liferay-ui:success key="insert_ok" message="insert_ok" />
<liferay-ui:success key="update_ok" message="update_ok" />

<aui:layout>
	<aui:column first="true" columnWidth="100" last="true">
		<div id="def-company"></div>
	</aui:column>
</aui:layout>

<script type="text/javascript">
YUI().use( 'aui-datatable', 'aui-datatype', 'datatable-sort',
	function(Y) {
		// data array
		var remoteDataCompany = [
		    <% for (int i = 0; i < allCompanies.size(); i++) { %>
     		{
   				id: <%=allCompanies.get(i).get("id")%>, 
   				name: '<%=allCompanies.get(i).get("name").toString().replaceAll("'", "\\\\'") %>',
   				company_id: '<%=allCompanies.get(i).get("entity_id")%>',
   				cui: '<%=allCompanies.get(i).get("cui")%>',
   				status: '<%=allCompanies.get(i).get("status")%>'
   			}
   			<% if (i != (allCompanies.size() - 1)) { out.print(","); } %>
     		<% } %>
		];

		// COLUMN INFO and ATTRIBUTES
		var nestedColsCompany = [
			{ key: 'id', label: 'ID' },
			{ key: 'name', label: 'Nume'},
			{ key: 'company_id', label: 'ID Societate'},
			{ key: 'cui', label: 'CUI'},
			{ key: 'status', label: 'Status', allowHTML: true, formatter: function(o) { 
				if(o.value == 'A') {return '<font color="green">Activ<font>';} 
				else { return '<font color="red">Inactiv</font>'; } } }      
		];

		// TABLE INIT
		var dataTableCompany = new Y.DataTable({
		    columns: nestedColsCompany,
		    data: remoteDataCompany,
		    recordType: ['id', 'name', 'company_id', 'cui', 'cuiint', 'status', 'modified', 'created'],
		    editEvent: 'click'
		}).render('#def-company');
		
		//dataTableCompany.get('boundingBox').unselectable();
	}
);
</script>

