<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme" %>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>
<%@ taglib uri="http://alloy.liferay.com/tld/aui" prefix="aui" %>
<%@ taglib uri="http://liferay.com/tld/util" prefix="liferay-util" %>

<%@page import="com.profluo.ecm.model.vo.DefAdvReqTypeVo"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.List"%>

<liferay-theme:defineObjects />
<portlet:defineObjects />
	
<%
	// retrieve currencies from the database
	List<HashMap<String,Object>> allReqType = DefAdvReqTypeVo.getInstance();
%>

<%-- Links --%>
<portlet:renderURL var="addEntryURL" >
	<portlet:param name="mvcPath" value="/html/definitions/defadvrequesttype/add_adv_req_type.jsp"></portlet:param>
</portlet:renderURL>

<liferay-ui:success key="insert_ok" message="insert_ok" />
<liferay-ui:success key="update_ok" message="update_ok" />

<aui:layout>
	<aui:column columnWidth="100" last="true" style="text-align:right">
		<a class="btn btn-primary" href="<%=addEntryURL.toString()%>">Adauga tip solicitare</a>
	</aui:column>
</aui:layout>
<br/>
<aui:layout>
	<aui:column first="true" columnWidth="100" last="true">
		<div id="adv-req-type"></div>
	</aui:column>
</aui:layout>

<script type="text/javascript">
YUI({lang: 'ro'}).use( 'aui-datatable', 'aui-datatype', 'datatable-sort',
	function(Y) {
		function edit(sel_id) {
		    var ml  = dataTableReqType.data, msg = '', template = '';
	
		    ml.each(function (item, i) {
		        var data = item.getAttrs(['id', 'name', 'status', 'modified', 'created']);

		        if (data.id == sel_id) {
					$('#<portlet:namespace />id').val(data.id);
					$('#<portlet:namespace />name').val(data.name);
					$('#<portlet:namespace />status').val(data.status);
					$('#<portlet:namespace />created').val(data.created);
					$('#<portlet:namespace />modified').val(data.modified);
					$('#<portlet:namespace />editReqType').submit();
		        }
		    });
		}
	
		// data array
		var remoteDataReqType = [
		    <% for (int i = 0; i < allReqType.size(); i++) { %>
     		{
   				id: <%=allReqType.get(i).get("id")%>, 
   				name: '<%=allReqType.get(i).get("name").toString().replaceAll("'", "\\\\'") %>',
   				status: '<%=allReqType.get(i).get("status")%>',
   				modified: '<%=allReqType.get(i).get("modified")%>',
   				created: '<%=allReqType.get(i).get("created")%>'
   			}
   			<% if (i != (allReqType.size() - 1)) { out.print(","); } %>
     		<% } %>
		];
		
		// COLUMN INFO and ATTRIBUTES
		var nestedColsReqType = [
			{ key: 'id', label: 'ID' },
			{ key: 'name', label: 'Nume', formatter: '{value}'},
			{ key: 'status', label: 'Status'},
			{ key: 'actiuni', label: 'Actiuni', allowHTML: true, 
				// gets the id of the row and creates an unique link for every row
				formatter: '<a href="#" id="rowreqtype{id}" class="editrowreqtype">Editare</a>' }		      
		];

		// TABLE INIT
		var dataTableReqType = new Y.DataTable({
		    columns: nestedColsReqType,
		    data: remoteDataReqType,
		    recordType: ['id', 'name', 'status', 'modified', 'created'],
		    editEvent: 'click'
		}).render('#adv-req-type');
		
		//dataTableReqType.get('boundingBox').unselectable();
		
		// Also define a listener on the single TH checkbox to
		// toggle all of the checkboxes
		dataTableReqType.delegate('click', function (e) {
		    // undefined to trigger the emptyCellValue
		    var targetDoc = e.target.get('id');
			edit(targetDoc.replace('rowreqtype',''));
		}, '.editrowreqtype', dataTableReqType);
	}
);
</script>

<aui:form method="post" action="<%=addEntryURL.toString() %>" name="editReqType" id="editReqType" style="margin:0;padding:0">
	<aui:input type="hidden" id="id" name="id"/>
	<aui:input type="hidden" id="name" name="name"/>
	<aui:input type="hidden" id="status" name="status"/> 
	<aui:input type="hidden" id="created" name="created"/> 
	<aui:input type="hidden" id="modified" name="modified"/> 
</aui:form>