<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme"%>

<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui"%>
<%@ taglib uri="http://alloy.liferay.com/tld/aui" prefix="aui"%>
<%@ taglib uri="http://liferay.com/tld/util" prefix="liferay-util"%>

<%@ page import="java.util.ResourceBundle"%>
<%@ page import="java.util.Locale"%>
<liferay-theme:defineObjects />

<portlet:defineObjects />
<portlet:resourceURL id="ajaxURL" var="ajaxURL"/>

<legend><strong><%=resmain.getString("vizualizare_realizare_buget")%></strong></legend>
<aui:layout>
	<aui:column first="true" columnWidth="100">
		<div id="vizualizare_buget_relizat"></div>
	</aui:column>
</aui:layout>

<aui:layout>
	<aui:column columnWidth="100" last = "true">
		<div id="vizualizare_realizare2"></div>
	</aui:column>
</aui:layout>

<script type="text/javascript">
var firstBugetClick = true;
YUI().use( 'aui-datatable', 'aui-datatype', 'datatable-sort',
	function(Y) {
	
	function formatCurrency(cell) {
	    format = {
	    	thousandsSeparator: ",",
	        decimalSeparator: ".",
	        decimalPlaces: noOfDecimalsToDisplay
	    };
	    return Y.DataType.Number.format(Number(cell.value), format);
	}
	
		var remoteData = [ ];

		var nestedCols = [
							{ key: 'categ_capex', label: 'Categorie CAPEX' },
							{ key: 'cod', label: 'Cod'},
							{ key: 'denumire', label: 'Denumire'},
							{ key: 'bugetat', label: 'Bugetat(1)', formatter: formatCurrency, className: 'currencyCol'},
							{ key: 'rezervat', label:  'Rezervat(2)', formatter: formatCurrency, className: 'currencyCol'},
							 {key: 'realizat',label:'Realizat(3)', formatter: formatCurrency, className: 'currencyCol'},
							{ key: 'disponibil', label: 'Disponibil(1-2-3)', formatter: formatCurrency, className: 'currencyCol'}
			 			];

		// TABLE INIT
		var dataTable = new Y.DataTable({
		    columns: nestedCols,
		    data: remoteData,
		    editEvent: 'click'
		}).render('#vizualizare_buget_relizat');
		
		var remoteData2 = [ ];
		
		

		var nestedCols2 = [
							{ key: 'buget_total', label: 'Buget total', formatter: formatCurrency, className: 'currencyCol' },
							{ key: 'cod_initiativa', label: 'Cod initiativa'},
							{ key: 'denumire_initiativa', label: 'Denumire initiativa'},
							{ key: 'bugetat', label: 'Bugetat(1)', formatter: formatCurrency, className: 'currencyCol'},
							{ key: 'rezervat', label:  'Rezervat(2)', formatter: formatCurrency, className: 'currencyCol'},
							 {key: 'realizat',label:'Realizat(3)', formatter: formatCurrency, className: 'currencyCol'},
							{ key: 'disponibil', label: 'Disponibil(1-2-3)', formatter: formatCurrency, className: 'currencyCol'}
						];
		
		// TABLE INIT
		var dataTable2 = new Y.DataTable({
		    columns: nestedCols2,
		    data: remoteData2,
		    editEvent: 'click'
		}).render('#vizualizare_realizare2');
		
		//BIND ON CLASS
		$('.showBuget').click(function(e) {
			e.preventDefault();
  			var action 		="getBuget";
  			//make ajax call only the first time the button was clicked
  			if (firstBugetClick) {
  				makeAjaxCallBuget(action);
  				firstBugetClick = false;
  			} else if ($('#vizualizare_buget').css('display') =='none'){
  				//the ajax call was already made so just show the section
  				$('#vizualizare_buget').show();
  			}else {
  				//hide the section
  				$('#vizualizare_buget').hide();
  			}
  		});
		
		function makeAjaxCallBuget(action) {
  			$.ajax({
  				type: "POST",
  				url: "<%=ajaxURL%>",
  				data: "<portlet:namespace />action=" + action + 
					"&<portlet:namespace />id=" + '<%=id%>',
  				success: function(msg) {
  					$('#vizualizare_buget').show();
  					// get table data
  					if (msg.trim() != ""){
  						var jsonEntries = jQuery.parseJSON(msg);
  						remoteData = eval(jsonEntries.values);
  						dataTable.set('data', remoteData);
  					} else {
  						dataTable.set('data', []);
  					}
  				}
  			});
  		}
	}
);
</script>