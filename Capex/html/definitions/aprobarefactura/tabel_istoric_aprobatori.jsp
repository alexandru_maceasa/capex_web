<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme"%>

<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui"%>
<%@ taglib uri="http://alloy.liferay.com/tld/aui" prefix="aui"%>
<%@ taglib uri="http://liferay.com/tld/util" prefix="liferay-util"%>

<%@ page import="java.util.ResourceBundle"%>
<%@ page import="java.util.Locale"%>
<liferay-theme:defineObjects />

<portlet:defineObjects />
<%--AJAX --%>
<portlet:resourceURL id="ajaxURL" var="ajaxURL"/>

<legend>
	<strong><%=resmain.getString("istoric_aprobari")%></strong>
</legend>
<aui:column first="true">
<div id="istoric_aprobatori_display"></div>
</aui:column>

<script type="text/javascript">
var firstClick = true;
YUI().use( 'aui-datatable', 'aui-datatype', 'datatable-sort',
	function(Y) {
		// data array
		var remoteData = [];
		// COLUMN INFO and ATTRIBUTES
		var nestedCols = [
			{ key: 'name', label: 'Nume' },
			{ key: 'rol', label: 'Rol'},
			{ key: 'approval_date', label: 'Data aprobare'}
			];

		// TABLE INIT
		var dataTable = new Y.DataTable({
		    columns: nestedCols,
		    data: remoteData,
		    editEvent: 'click'
		}).render('#istoric_aprobatori_display');
		
		//BIND ON CLASS
		$('.showhistory').click(function(e) {
			e.preventDefault();
  			var action 		="getHistory";
  			//make ajax call only the first time the button was clicked
  			if (firstClick) {
  				makeAjaxCall(action);
  				firstClick = false;
  			} else if ($('#vizualizare_istoric_aprobatori').css('display') =='none'){
  				//the ajax call was already made so just show the section
  				$('#vizualizare_istoric_aprobatori').show();
  			}else {
  				//hide the section
  				$('#vizualizare_istoric_aprobatori').hide();
  			}
  		});
		
		//afax function
		function makeAjaxCall(action) {
  			$.ajax({
  				type: "POST",
  				url: "<%=ajaxURL%>",
  				data: "<portlet:namespace />action=" + action + 
					"&<portlet:namespace />id=" + <%=id %>,
  				success: function(msg) {
  					$('#vizualizare_istoric_aprobatori').show();
  					// get table data
  					if (msg.trim() != ""){
  						var jsonEntries = jQuery.parseJSON(msg);
  						remoteData = eval(jsonEntries.values);
  						dataTable.set('data', remoteData);
  					} else {
  						dataTable.set('data', []);
  					}
  				}
  			});
  		}
	}
);
</script>