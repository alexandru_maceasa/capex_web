<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="com.profluo.ecm.model.db.DefNewProj"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.profluo.ecm.flows.InvoiceStages"%>
<%@page import="com.profluo.ecm.controller.UserTeamIdUtils"%>
<%@page import="com.profluo.ecm.model.vo.DefTipFactVo"%>
<%@page import="com.profluo.ecm.model.vo.ListaInitVo"%>
<%@page import="com.profluo.ecm.model.vo.DefNewProjVo"%>
<%@page import="com.profluo.ecm.model.vo.DefCompaniesVo"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.List"%>
<%@page import="com.liferay.portal.util.PortalUtil"%>
<%@page import="com.profluo.ecm.controller.ControllerUtils"%>

<%@page import="com.profluo.ecm.model.db.InvoiceHeader"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme"%>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui"%>
<%@ taglib uri="http://alloy.liferay.com/tld/aui" prefix="aui"%>
<%@ taglib uri="http://liferay.com/tld/util" prefix="liferay-util"%>
<%@ page import="java.util.ResourceBundle"%>
<%@ page import="java.util.Locale"%>



<liferay-theme:defineObjects />
<portlet:defineObjects />

<%-- Submit URL, mapped to the approveInvoice method in the class AprobareFactura.java --%>
<portlet:actionURL name="approveInvoice" var="submitURL" />
<portlet:renderURL var="backURL">
	<portlet:param name="mvcPath" value="/html/facturi/view.jsp"></portlet:param>
</portlet:renderURL>

<portlet:renderURL var="backToBlockedURL">
	<portlet:param name="mvcPath" value="/html/facturiblocatelaplata/view.jsp"></portlet:param>
</portlet:renderURL>


<%
	
	List<HashMap<String,Object>> allCompanies = DefCompaniesVo.getInstance();
	
	locale = renderRequest.getLocale();
	ResourceBundle resmain = portletConfig.getResourceBundle(locale);
	String id = "";
	String storeLineIds = "";
	String type = "";
	int storeId = 0;
	List<HashMap<String,Object>> oneInvoice = null;
	//retrieve all initiatives from DB
	List<HashMap<String,Object>> allInitiatives = ListaInitVo.getInstance();
	//retrieve all new projects from DB
	List<HashMap<String,Object>> allNewProjects = DefNewProj.getAll();
	// retrieve all invoice types from DB
	List<HashMap<String,Object>> allInvoiceTypes = DefTipFactVo.getInstance();
	//retrieve all store line articles
	List<HashMap<String,Object>> allStoreLines = new ArrayList<HashMap<String,Object>>();
	//retrieve all approvers
	List<HashMap<String,Object>> allApprovers = null;
	// for storing the total with vat of the invoice
	float value = 0;
	//folosita pentru a salva adresa de email a initiatorului dpi
	String email = "";
	//pentru a sti care aprobator este pe instanta curenta
	String whatUser = "0";
	if(UserTeamIdUtils.belongsToTeam(user.getUserId(), UserTeamIdUtils.DPI_RESPONSABLE)){
		whatUser = "1";
		email = user.getEmailAddress();
		//System.out.println("AICI : "+email);
	}
	//bun de plata
	String bunDePlata ="OK";
	//refuz la plata
	String refuzLaPlata = "NOK";
	String supplier_name = "";
	String supplierCode = "";
	String allApproversNames = "";
	String whatPage = "";
	String companyName = "";
	
	//get filters
	String categorie	= "";
	String number		= "";
	String company_id 	= "";
	String moneda 		= "";
	String datafactura 	= "";
	String datainreg 	= "";
	String supl_name	= "";
	String supl_code	= "";
	
	
	String isBlocked 	= "";
	
	try {
		storeId = Integer.parseInt(renderRequest.getParameter("storeId"));
	} catch (Exception e) {}
	try {
		whatPage = renderRequest.getAttribute("page-layout").toString();
	} catch (Exception e) {}
	try {
		isBlocked = renderRequest.getParameter("blocked_page").toString();
	} catch (Exception e) {
		isBlocked = "approve";
	}
	try{
		type = renderRequest.getParameter("listingType").toString();
	} catch (Exception e){
		e.printStackTrace();
	}
	try{
		id = renderRequest.getParameter("id").toString();
	} catch (Exception e){
		e.printStackTrace();
	}
	try{
		supplier_name = renderRequest.getParameter("supplier_name").toString();
	} catch (Exception e){
		e.printStackTrace();
	}
	try{
		supplierCode = renderRequest.getParameter("supplier_code").toString();
	} catch (Exception e){
		e.printStackTrace();
	}
	try{
		categorie = renderRequest.getParameter("categorie_filter").toString();
		number = renderRequest.getParameter("number_filter").toString();
		company_id = renderRequest.getParameter("company_id_filter").toString();
		moneda = renderRequest.getParameter("moneda_filter").toString();
		datafactura = renderRequest.getParameter("datafactura_filter").toString();
		datainreg = renderRequest.getParameter("datainreg_filter").toString();
		supl_name = renderRequest.getParameter("supplier_name_filter").toString();
		supl_code = renderRequest.getParameter("supplier_code_filter").toString();
	} catch (Exception e){ }
	if(id.equals("") || id == null) {
		String url = PortalUtil.getCurrentCompleteURL(request);
		String [] params = url.split("&");
		String [] lastParameter = params[params.length-1].split("=");
		if (lastParameter[0].equals("id")){
			id = lastParameter[1];
		}
	}
	
	oneInvoice	= InvoiceHeader.getHeaderById(id, "invoice_header");
	allStoreLines = InvoiceHeader.getStoreLines(id, String.valueOf(user.getUserId()));
	
	for(int i = 0 ; i < allStoreLines.size(); i++ ){
		if( i < allStoreLines.size() - 1 ) {
			storeLineIds += allStoreLines.get(i).get("id").toString() + ",";
		} else {
			storeLineIds += allStoreLines.get(i).get("id").toString();
		}
		
	}

	try {
		whatPage = renderRequest.getAttribute("page-layout").toString();
	} catch (Exception e) {}
%>
<script type="text/javascript">
	var ppid = "<portlet:namespace/>";
</script>
<%-- first row --%>
<liferay-ui:error message="insert_nok" 	key="insert_nok"/>
<liferay-ui:error message="update_nok" 	key="update_nok"/>
<liferay-ui:error message="insert_ok" 	key="insert_ok"/>
<liferay-ui:error message="update_ok" 	key="update_ok"/>

<aui:layout>
<!-- data >= 23 atunci alfresco nou, altfel alfresco vechi -->
	<% DateFormat dateFormat = new  SimpleDateFormat("yyyy-MM-dd");
      	   Date invCreateDate = dateFormat.parse(oneInvoice.get(0).get("created").toString().substring(0, 10));
      	   Date dataReferinta = dateFormat.parse("2018-11-23");
      %>
	<% if(invCreateDate.before(dataReferinta) && oneInvoice.get(0).get("image") != null) { %>
	<a href="<%=ControllerUtils.alfrescoURLOld + oneInvoice.get(0).get("image") %>" <%--+ ControllerUtils.alfrescoDirectLoginOld  --%>
		class="btn btn-primary fr" style="margin-right:10px" target="_blank">Vizualizare Factura</a>
	<% } else if (invCreateDate.after(dataReferinta) && oneInvoice.get(0).get("cid") != null) { %>
		<a onclick="downloadPdfFunction('<%=oneInvoice.get(0).get("cid") %>')" <%--+ ControllerUtils.alfrescoDirectLoginOld  --%>
		class="btn btn-primary fr" style="margin-right:10px" target="_blank">Vizualizare Factura</a>
	<% } %>
</aui:layout>

<legend>
	<strong><%=resmain.getString("detalii-capex")%></strong>
</legend>
<aui:fieldset>
	<aui:layout>
		<aui:column columnWidth="10">
			<label for="<portlet:namespace />company" style="padding-top: 5px">Societate</label>
		</aui:column>
		<aui:column columnWidth="20">
			<aui:select id="company" name="company" label="" disabled = "true">
				<% for (int i = 0; i < allCompanies.size(); i++) { %>
					<% if (allCompanies.get(i).get("status").equals("A")) {%>
						<% if ( allCompanies.get(i).get("id").toString().equals(oneInvoice.get(0).get("id_company").toString())) { 
							companyName = allCompanies.get(i).get("name").toString();
						%>
							<aui:option selected="selected" value="<%=allCompanies.get(i).get(\"id\")%>"><%=allCompanies.get(i).get("name")%></aui:option>
						<% } %>
					<% } %>
				<% } %>
			</aui:select>
		</aui:column>

		<aui:column columnWidth="10">
			<label for="<portlet:namespace />cat-capex" style="padding-top: 5px">Categorie CAPEX</label>
		</aui:column>
		<aui:column columnWidth="20">
			<%if (oneInvoice.get(0).get("tip_capex").toString().equals("0")) { %>
			<aui:input type="text" name="cat-capex" id="cat-capex" value="Capex Diverse" label="" readonly="true"></aui:input>
			<% } else if (oneInvoice.get(0).get("tip_capex").toString().equals("1")) { %>
			<aui:input type="text" name="cat-capex" id="cat-capex" value="Initiativa centralizata" label="" readonly="true"></aui:input>
			<% } else {  %>
			<aui:input type="text" name="cat-capex" id="cat-capex" value="Proiect nou" label="" readonly="true"></aui:input>
			<% } %>	
		</aui:column>

		<aui:column columnWidth="10">
			<label for="<portlet:namespace />detalii-cat" style="padding-top: 5px">Detalii categorie</label>
		</aui:column>
		<aui:column columnWidth="20">
		<% if (oneInvoice.get(0).get("tip_capex").toString().equals("0")) { %>
			<aui:input type="text" name="detalii-cat" id="detalii-cat" value="Capex Diverse" label=""  readonly="true"></aui:input>
		<% } else if (oneInvoice.get(0).get("tip_capex").toString().equals("1")) { 
			for (int i = 0; i < allInitiatives.size(); i++) { 
				if (oneInvoice.get(0).get("id_initiative").toString().equals(allInitiatives.get(i).get("id").toString())) { %>
			<aui:input type="text" name="detalii-cat" id="detalii-cat" value="<%=allInitiatives.get(i).get(\"code\") %>" label=""  readonly="true" inlineField="true"></aui:input>
			<aui:input type="text" name="detalii-cat2" id="detalii-cat2" value="<%=allInitiatives.get(i).get(\"name\") %>" label=""  readonly="true" inlineField="true"></aui:input>
			<% } } %>
		<% } else { 
			for (int i = 0; i < allNewProjects.size(); i++) { 
				if (oneInvoice.get(0).get("id_new_prj").toString().equals(allNewProjects.get(i).get("id").toString())) { %>
				<aui:input type="text" name="detalii-cat" id="detalii-cat" value="<%=allNewProjects.get(i).get(\"store_id\") %>" label=""  readonly="true" inlineField="true"></aui:input>
				<aui:input type="text" name="detalii-cat2" id="detalii-cat2" value="<%=allNewProjects.get(i).get(\"name\") %>" label=""  readonly="true" inlineField="true"></aui:input>
		
		<% } } } %>
		</aui:column>
	</aui:layout>
</aui:fieldset>
<legend>
	<strong><%=resmain.getString("antet-factura")%></strong>
</legend>
<%-- first row --%>
<aui:fieldset>
	<aui:layout>
		<aui:column columnWidth="10">
			<label for="<portlet:namespace/>nr-fact-baza"
				style="padding-top: 5px">Numar factura</label>
		</aui:column>
		<aui:column columnWidth="20">
			<aui:input type="text" name="inv_number" id="inv_number" value="<%=oneInvoice.get(0).get(\"inv_number\") %>" label="" readonly = "true">
				<aui:validator name="required" errorMessage="field-required"></aui:validator>
				<aui:validator name="alphanum" errorMessage="alpha-num"></aui:validator>
			</aui:input>
		</aui:column>
		
		<aui:column columnWidth="10">
			<label for="<portlet:namespace />inv_date" style="padding-top: 5px"><%=resmain.getString("datafactura")%></label>
		</aui:column>
		<aui:column columnWidth="20">
			<aui:input type="text" id="inv_date" name="inv_date" value="<%=oneInvoice.get(0).get(\"inv_date\") %>" placeholder="zz/ll/aaaa" label="" readonly = "true">
				<aui:validator name="required" errorMessage="field-required"></aui:validator>
			</aui:input>
		</aui:column>
	
		<aui:column columnWidth="10">
			<label for="<portlet:namespace />datascadenta"
				style="padding-top: 5px"><%=resmain.getString("datascadenta")%></label>
		</aui:column>
		<aui:column columnWidth="20">
			<aui:input type="text" id="datascadenta" name="datascadenta" value="<%=oneInvoice.get(0).get(\"due_date\") %>" placeholder="zz/ll/aaaa" label="" readonly = "true">
				<aui:validator name="required" errorMessage="field-required"></aui:validator>
			</aui:input>
		</aui:column>
	</aui:layout>
</aui:fieldset>

<%--second row --%>
<aui:fieldset>
	<aui:layout>
		<aui:column columnWidth="10">
			<label for="<portlet:namespace/>supplier_name" style="padding-top: 5px">Furnizor</label>
		</aui:column>
		<aui:column columnWidth="20">
			<aui:input type="text" name="supplier_name" id="supplier_name" value="<%=supplier_name %>" label="" readonly= "true">
				<aui:validator name="required" errorMessage="field-required"></aui:validator>
				<aui:validator name="alphanum" errorMessage="alpha-num"></aui:validator>
			</aui:input>
		</aui:column>
		
		<aui:column columnWidth="10">
			<label for="<portlet:namespace/>tip-factura" style="padding-top: 5px">Tip factura</label>
		</aui:column>
		<aui:column columnWidth="20">
		<% if (oneInvoice != null) {
			for (int i = 0; i < allInvoiceTypes.size(); i++) { 
				if (oneInvoice.get(0).get("tip_fact").toString().equals(allInvoiceTypes.get(i).get("id").toString())) { %>
				<aui:input type="text" name="tip-factura" id="tip-factura" value="<%= allInvoiceTypes.get(i).get(\"name\") %>" label="" readonly = "true">
					<aui:validator name="required" errorMessage="field-required"></aui:validator>
					<aui:validator name="alphanum" errorMessage="alpha-num"></aui:validator>
				</aui:input>
				<% } %>
			<% } %>
		<% } %>
		</aui:column>
		
		<% if (oneInvoice.get(0).get("tip_fact").toString().equals("2")) { %>
		<aui:column columnWidth="10">
			<label for="<portlet:namespace/>nr-fact" style="padding-top: 5px">Nr. fact de baza</label>
		</aui:column>
		<aui:column columnWidth="20">
			<aui:input type="text" name="nr-fact" id="nr-fact" value="<%=oneInvoice.get(0).get(\"id_parent\") %>" label="" readonly = "true">
				<aui:validator name="required" errorMessage="field-required"></aui:validator>
				<aui:validator name="alphanum" errorMessage="alpha-num"></aui:validator>
			</aui:input>
		</aui:column>
		<% } %>
	</aui:layout>
</aui:fieldset>

<%--third row --%>
<aui:fieldset>
	<aui:layout>
		<% if(oneInvoice != null) {
			if(!oneInvoice.get(0).get("id_fact_avans").toString().equals("0")) { %>
		<aui:column columnWidth="15">
			<label for="<portlet:namespace/>nr-fact-pro" style="padding-top: 5px">Numar factura proforma</label>
		</aui:column>
		<aui:column columnWidth="20">
			<aui:input type="text" name="nr-fact-pro" id="nr-fact-pro" value="<%=oneInvoice.get(0).get(\"id_fact_avans\") %>" label="">
				<aui:validator name="required" errorMessage="field-required"></aui:validator>
				<aui:validator name="alphanum" errorMessage="alpha-num"></aui:validator>
			</aui:input>
		</aui:column>
			<% } %>
		<% } %>		
	</aui:layout>
</aui:fieldset>

<aui:layout>
	<%@ include file="tabel_detalii_factura.jsp"%>

	<aui:column columnWidth="10" first="true">
		<label for="<portlet:namespace/>val_neta" style="padding-top: 5px"><%=resmain.getString("val_neta")%>:</label>
	</aui:column>
	<aui:column columnWidth = "20">
		<aui:input name="val_neta" type="text" id="val_neta" value="<%=oneInvoice.get(0).get(\"payment_value_curr\")%>" label="" readonly = "true">
			<aui:validator name="digits" errorMessage="Insert only digits!"></aui:validator>
		</aui:input>
	</aui:column>
</aui:layout>

<aui:input style="display:none" id="approver_class" label="" name="approver_class" value="<%=whatUser %>"  />

<%-- save the supplier fault, blocked_type, details lit, aditional details --%>
<aui:input type ="hidden" name ="database_supplierr_fault" value = "<%=oneInvoice.get(0).get(\"supplier_fault\")%>" />
<aui:input type ="hidden" name ="database_blocked_type" value = "<%=oneInvoice.get(0).get(\"blocked_type\") %>" />
<aui:input type ="hidden" name ="database_detaile_lit" value = "<%=oneInvoice.get(0).get(\"details_litigii\") %>" />
<aui:input type ="hidden" name ="database_aditional_details" value = "<%=oneInvoice.get(0).get(\"details_aditional\") %>" />
	
<aui:form action="<%=submitURL%>" method="post" name="approve_invoice">
	<%--params for nextStage --%>
	<aui:input type ="hidden" name ="idInvoice" value = "<%=id %>" />
	<aui:input type ="hidden" name ="blockedORnot" value = "" />
	<aui:input type ="hidden" name ="valueCurr" value = "<%=oneInvoice.get(0).get(\"total_with_vat_curr\") %>" />
	<aui:input type ="hidden" name ="valueRon" value = "<%=oneInvoice.get(0).get(\"total_with_vat_ron\") %>" />
	<aui:input type ="hidden" name ="lineIds" value = "" />
	<aui:input type ="hidden" name ="invoiceDate" value = "<%=oneInvoice.get(0).get(\"inv_date\") %>" />
	
	<%-- parameters for blocked invoice notification --%>
	<aui:input type ="hidden" name ="invoiceNumber" value = "<%=oneInvoice.get(0).get(\"inv_number\") %>" />
	<aui:input type ="hidden" name ="kontanAccount" value = "<%=oneInvoice.get(0).get(\"kontan_acc\") %>" />
	<%-- values are set on document ready --%>
	<aui:input type ="hidden" name ="supplierName" value = "" />
	<aui:input type ="hidden" name ="supplierCode" value = "" />
	<aui:input type ="hidden" name ="companyName" value = "" />
	
	<aui:input type="hidden" name="idNewProject" value = "<%=oneInvoice.get(0).get(\"id_new_prj\") %>" />
	
		
	<%--sectiune detalii factura --%>
	<legend>
		<strong><%=resmain.getString("decizii_factura")%></strong>
	</legend>
	
	<%-- FIRST ROW --%>
	<aui:layout>
		<aui:column columnWidth="10" first="true">
			<label style="padding-top:5px" for="<portlet:namespace/>existenta_litigiu"><%=resmain.getString("existenta_litigiu")%>:</label>
		</aui:column>
		<aui:column columnWidth="20">
			<% if(oneInvoice.get(0).get("litigii").equals("N")) { %>
				<aui:input label="Da" id="existenta_litigiu1" name="existenta_litigiu" 
					onchange="existaLitigii(ppid);"
					type="radio" value="1" inlineField="true"/>
				<aui:input label="Nu" id="existenta_litigiu2" name="existenta_litigiu" 
					onchange="nuExistaLitigii(ppid);"
					type="radio" value="0" checked="true" inlineField="true"/>
			<% } else { %>
				<aui:input label="Da" id="existenta_litigiu1" name="existenta_litigiu" 
					onchange="existaLitigii(ppid);"
					type="radio" value="1" checked="true" inlineField="true"/>
				<aui:input label="Nu" id="existenta_litigiu2" name="existenta_litigiu" 
					onchange="nuExistaLitigii(ppid);"
					type="radio" value="0" inlineField="true"/>
				<% } %>
		</aui:column>
			<aui:column columnWidth="10">
				<label style="padding-top:5px" for="<portlet:namespace/>vina_furnizor"><%=resmain.getString("vina_furnizor")%>:</label>
			</aui:column>
			<aui:column columnWidth="20">
				<% if (oneInvoice.get(0).get("supplier_fault").equals("N")) { %>
					<aui:input label="Da" id="vina_furnizor1" name="vina_furnizor"
						type="radio" value="1" inlineField="true" />
					<aui:input label="Nu" id="vina_furnizor2" name="vina_furnizor"
						type="radio" value="0" checked="true" inlineField="true"/>
				<% } else { %>
					<aui:input label="Da" id="vina_furnizor1" name="vina_furnizor"
						type="radio" value="1" checked="true" inlineField="true"/>
				
					<aui:input label="Nu" id="vina_furnizor2" name="vina_furnizor"
						type="radio" value="0" inlineField="true"/>
				<% } %>
			</aui:column>
			<aui:column columnWidth="10" >
				<label style="padding-top:5px" for="<portlet:namespace/>detalii-litigiu"><%=resmain.getString("detalii-litigiu")%>:
				</label>
			</aui:column>
			<aui:column columnWidth="20">
				<aui:input type="textarea" id="detalii-litigiu" name="detalii-litigiu" label="" value="<%=oneInvoice.get(0).get(\"details_litigii\") %>">
	 			</aui:input>
			</aui:column>
	</aui:layout>
	
	<%--SECOND ROW --%>
	<aui:layout>
		<aui:column columnWidth="10" first="true">
			<label style="padding-top:5px" for="<portlet:namespace/>radio_bun_de_plata"><%=resmain.getString("radio_bun_de_plata")%>:</label>
		</aui:column>
		<aui:column columnWidth="20">
		<% if (oneInvoice.get(0).get("blocked_type").toString().equals("0")) {%>
			<aui:input label="Da" id="true_bun_de_plata" name="bun_de_plata" onchange="bunDePlata(ppid);"
				type="radio" value="1" inlineField="true" checked="true" />
			<aui:input label="Nu" id="false_bun_de_plata" name="bun_de_plata" onchange="refuzLaPlata(ppid);"
				type="radio" value="0" inlineField="true"/>
		<%} else { %>
			<aui:input label="Da" id="true_bun_de_plata" name="bun_de_plata" onchange="bunDePlata(ppid);"
				type="radio" value="1" inlineField="true"  />
			<aui:input label="Nu" id="false_bun_de_plata" name="bun_de_plata" onchange="refuzLaPlata(ppid);"
				type="radio" value="0" inlineField="true" checked="true"/>
		<% } %>
		</aui:column>
		<aui:column columnWidth="10">
			<label for="<portlet:namespace/>motiv_invalidare"
				style="padding-top: 5px"><%=resmain.getString("motiv_invalidare")%>:</label>
		</aui:column>
		<aui:column columnWidth="20">
		<% if (oneInvoice.get(0).get("blocked_type").toString().equals("1")) {%>
			<aui:input type="radio" value="1" checked="true"
				name="motiv_invalidare" id="motiv_invalidare1"
				label="Amanare la plata" />
		<%} else { %>
			<aui:input type="radio" value="1"
				name="motiv_invalidare" id="motiv_invalidare1"
				label="Amanare la plata"/>
		<% } %>
		<% if (oneInvoice.get(0).get("blocked_type").toString().equals("2")) {%>
			<aui:input type="radio" value="2" label="Litigii/Bunuri nelivrate sau cu probleme"
				name="motiv_invalidare" id="motiv_invalidare2" checked="true" />
		<%} else { %>
			<aui:input type="radio" value="2" label="Litigii/Bunuri nelivrate sau cu probleme"
				name="motiv_invalidare" id="motiv_invalidare2"/>
		<% } %>
		</aui:column>
		<aui:column columnWidth="10" >
				<label style="padding-top:5px" for="<portlet:namespace/>detalii-aditionale"><%=resmain.getString("detalii-aditionale")%>:
				</label>
		</aui:column>
		<aui:column columnWidth="20">
			<aui:input type="textarea" id="detalii-aditionale" name="detalii-aditionale" label="" value="<%=oneInvoice.get(0).get(\"details_aditional\") %>">
 			</aui:input>
		</aui:column>
	</aui:layout>
	
	<%-- butoane vizualizare alte detalii --%>
	<aui:button-row>
		<aui:column columnWidth="15" >
			<label style="padding-top:10px"><%=resmain.getString("vizualizare_detalii")%>:</label>
		</aui:column>
		<%--
		<aui:column>
			<a href="#" class="btn btn-primary" style="margin-right:0px"><%=resmain.getString("realizare_dpi")%></a>
		</aui:column>	
		<aui:column>
			<a href="#" class="btn btn-primary" style="margin-right:0px"><%=resmain.getString("realizare_buget")%></a>
		</aui:column> --%>
		<aui:column>
			<a href="#" class="btn btn-primary showDPIRealizat" style="margin-right:0px">Realizare DPI asociate</a>
<!-- 			<a href="#" class="btn btn-primary showBuget" style="margin-right:0px">Realizare buget</a> -->
			<a href="#" class="btn btn-primary showhistory" style="margin-right:0px"><%=resmain.getString("istoric_aprobari")%></a>
		</aui:column>
	</aui:button-row>
	
	<aui:layout>
		<%-- REALIZARE DPI --%>
		<div id="vizualizare_realizare_dpi" style="display:none">
			<%@ include file="tabel_realizare_dpi.jsp" %>
			<aui:column columnWidth="15" first="true">
				<label for="<portlet:namespace/>val_bugetata" style="padding-top: 5px">Valoare DPI</label>
			</aui:column>
			<aui:column columnWidth="20">
				<aui:input type="text" disabled="true" name="val_bugetata" id="val_bugetata" value=""
					label="" />
			</aui:column>
			<aui:column columnWidth="15">
				<label for="<portlet:namespace/>total_realizat"
					style="padding-top: 5px">Total valoare factura</label>
			</aui:column>
			<aui:column columnWidth="20">
				<aui:input type="text" disabled="true" name="total_realizat" id="total_realizat"
					value="" label="" />
			</aui:column>
			<aui:column columnWidth="10">
				<label for="<portlet:namespace/>diferenta" style="padding-top: 5px">Diferenta</label>
			</aui:column>
			<aui:column columnWidth="10">
				<aui:input type="text" disabled="true" name="diferenta" id="diferenta" value=""	label="" />
			</aui:column>
		</div>
	</aui:layout>
	
	<aui:layout>
		<%--REALIZAT BUGET --%>
		<div id="vizualizare_buget" style="display:none">
			<%@ include file="tabel_realizare_buget.jsp" %>
		</div>
	</aui:layout>
	
	<aui:layout>
		<%-- ISTORIC APROBATORI --%>
		<div id="vizualizare_istoric_aprobatori" style="display:none">
			<%@ include file="tabel_istoric_aprobatori.jsp" %>
		</div>
	</aui:layout>
	
	<%-- LAST BUTTON ROW --%>
	<aui:layout>
		<aui:button-row>
			<aui:column first="true">
			<% if (isBlocked.equals("blocked")) {%>
				<a href="<%=backToBlockedURL.toString()%>" class="btn btn-primary-red" style="margin-right:0px">Inapoi</a>
			<% } else { %>
				<a href="#" class="btn btn-primary-red goBackWithFilters" style="margin-right:0px">Inapoi</a>
			<% } %>
			</aui:column>	
			<aui:column last="true">
				<a href="#" class="btn btn-primary fr approve_inv" style="margin-right:0px"><%=resmain.getString("validare_plata")%></a>
			</aui:column>
			<%-- <aui:column last="true">
				SIMPLE SAVE: modify approve_inv with simpleSave
				<a href="#" class="btn btn-primary fr approve_inv" style="margin-right:0px"><%=resmain.getString("simpleSave")%></a>
			</aui:column> --%>
			<%-- NEVER USED --%>
			<aui:column last="true">
				<a href="#" class="btn btn-primary-red fr refuse_inv" style="margin-right:0px; display:none"><%=resmain.getString("blocat_plata")%></a>
			</aui:column>
			<aui:button cssClass="btn btn-primary-red fr" name="approve_refuse" type="submit" style="display:none"
					id="approve_refuse"></aui:button>
		</aui:button-row>
	</aui:layout>
</aui:form>
<script type="text/javascript">

	$(document).ready(function() {
		//set parameters for blocked invoice notification
		$('#<portlet:namespace />supplierName').val('<%=supplier_name %>');
		$('#<portlet:namespace />supplierCode').val('<%=supplierCode%>');
		$('#<portlet:namespace />companyName').val('<%=companyName%>');
		
		$('#<portlet:namespace />val_neta').autoNumeric('init', {aSep: ',', mDec: 4, vMin : -999999999, vMax : 999999999 });
		
		if ($('#<portlet:namespace/>database_blocked_type').val() == '0'){
			$('#<portlet:namespace/>true_bun_de_plata').trigger('change');
		}
		if($('input[name="<portlet:namespace/>existenta_litigiu"]:checked').val() == '0') {
			$('#<portlet:namespace/>existenta_litigiu2').trigger('change');
		}
	});
	YUI({lang: 'ro'}).use('aui-datepicker', function(Y) {
		new Y.DatePicker({
			trigger : '#<portlet:namespace />datafactura',
			mask : '%Y-%m-%d',
			popover : {
				zIndex : 1
			},
			calendar : {
				dateFormat : '%Y-%m-%d'
			}
		});

		new Y.DatePicker({
			trigger : '#<portlet:namespace />datascadenta',
			mask : '%Y-%m-%d',
			popover : {
				zIndex : 1
			},
			calendar : {
				dateFormat : '%Y-%m-%d'
			}
		});
	});
	$('.goBackWithFilters').click(function(e) {
		e.preventDefault();
		$('#<portlet:namespace />goBack').submit();
	});
</script>
<aui:form method="post" action="<%=backURL.toString() %>" name="goBack" id="goBack" style="margin:0;padding:0">
	<aui:input type="hidden" id="categorie_filter" name="categorie_filter" value="<%=categorie %>"/>
	<aui:input type="hidden" id="number_filter" name="number_filter" value="<%=number %>"/>
	<aui:input type="hidden" id="company_id_filter" name="company_id_filter" value="<%=company_id %>"/>
	<aui:input type="hidden" id="moneda_filter" name="moneda_filter" value="<%=moneda %>"/>
	<aui:input type="hidden" id="datafactura_filter" name="datafactura_filter" value="<%=datafactura %>"/>
	<aui:input type="hidden" id="datainreg_filter" name="datainreg_filter" value="<%=datainreg %>"/>
	<aui:input type="hidden" id="supplier_code_filter" name="supplier_code_filter" value="<%=supl_code %>"/>
	<aui:input type="hidden" id="supplier_name_filter" name="supplier_name_filter" value="<%=supl_name %>"/>
</aui:form>