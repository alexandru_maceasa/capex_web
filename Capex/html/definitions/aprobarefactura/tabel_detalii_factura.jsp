<%@page import="com.profluo.ecm.model.vo.DefStoreVo"%>
<%@page import="com.profluo.ecm.model.vo.DefProductVo"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.List"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme"%>

<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui"%>
<%@ taglib uri="http://alloy.liferay.com/tld/aui" prefix="aui"%>
<%@ taglib uri="http://liferay.com/tld/util" prefix="liferay-util"%>

<%@ page import="java.util.ResourceBundle"%>
<%@ page import="java.util.Locale"%>
<liferay-theme:defineObjects />

<portlet:defineObjects />
<%
//def all prods
List<HashMap<String,Object>> allProds = DefProductVo.getInstance();
//get all stores
List<HashMap<String,Object>> allStores = DefStoreVo.getInstance();

%>

<legend><strong><%=resmain.getString("detalii-factura")%></strong></legend>
<aui:column first="true" columnWidth="100" last="true">
	<div id="id-detalii-factura"></div>
</aui:column>

<script type="text/javascript">
function showLineDetails(id) {
	return false;
}
YUI().use( 'aui-datatable', 'aui-datatype', 'datatable-sort',
	function(Y) {
		// data array
		var value = 0;
		
		var allProds = {'':'', <% for (int i = 0; i < allProds.size(); i++) { %>
			<% if (allProds.get(i).get("status").equals("A")) { %>
				<% if (allProds.size() - 1 == i) { %>
					<%=allProds.get(i).get("id")%>:'<%=allProds.get(i).get("code")%> - <%=allProds.get(i).get("name")%>'
				<% } else { %>
					<%=allProds.get(i).get("id")%>:'<%=allProds.get(i).get("code")%> - <%=allProds.get(i).get("name")%>',
				<% } %>
			<% } %> 
		<% } %>};
		
		var magazine = {'0':'Magazin', <% for (int i = 0; i < allStores.size(); i++) { %>
			<% if (allStores.get(i).get("status").equals("A")) { %>
				<% if (allStores.size() - 1 == i) { %>
				<%=allStores.get(i).get("id")%>:'<%=allStores.get(i).get("store_name")%>'
				<% } else { %>
				<%=allStores.get(i).get("id")%>:'<%=allStores.get(i).get("store_name")%>',
				<% } %>
			<% } %> 
		<% } %>};
		
		var remoteData = [
		    <% if(allStoreLines != null) { 
			    for (int i = 0; i < allStoreLines.size();i++) {  %>
				{
					val_no_vat:'<%=allStoreLines.get(i).get("price_no_vat_curr")%>',
					tva : <%=allStoreLines.get(i).get("vat")%>,
					store_line_id: <%=allStoreLines.get(i).get("id")%>,
					nr_crt: <%=i+1 %>, 
					cod_articol: <%=allStoreLines.get(i).get("article_code")%>, 
					cod_produs: '<%=allStoreLines.get(i).get("product_code")%>', 
					denumire_articol: '<%=allStoreLines.get(i).get("description")%>',
					magazin: <%=allStoreLines.get(i).get("id_store")%>,
					um: '<%=allStoreLines.get(i).get("um")%>',
					cant: <%=allStoreLines.get(i).get("quantity")%>,
					pret_unitar: '<%=allStoreLines.get(i).get("price")%>',
					valoare_bunuri_tva: '',
					nr_receptie: 1234.69				
				}
				<% if (i != allStoreLines.size()-1){ out.print(","); } %>
			<% } %>
		<% } %>
		];
		
		function formatCurrency(cell) {
		    format = {
		    	thousandsSeparator: ",",
		        decimalSeparator: ".",
		        decimalPlaces: noOfDecimalsToDisplay
		    };
		    return Y.DataType.Number.format(Number(cell.value), format);
		}
		// COLUMN INFO and ATTRIBUTES
		var nestedCols = [
		    { key: 'tva', className:'hiddencol'},
		    { key: 'val_no_vat', className:'hiddencol'},
			{ key: 'store_line_id', className:'hiddencol' },
			{ key: 'nr_crt', label: 'Nr Crt' },
			{ key: 'cod_articol', className:'hiddencol'},
			{ key: 'cod_produs', label: 'Cod Produs', emptyCellValue: '',
				formatter: function(o) {
					var obj = JSON.parse(JSON.stringify(allProds));
					
					if (typeof o.value != 'undefined') {
						if(o.value != null){
							return obj[o.value].toString();
						}else return '';
					} else {
						return '';
					}
				}},
			{ key: 'denumire_articol', label: 'Denumire Articol'},
			{ key: 'magazin', label:  'Magazin', emptyCellValue: '',
				formatter: function(o) {
					var obj = JSON.parse(JSON.stringify(magazine));
					
					if (typeof o.value != 'undefined') {
						if(o.value != null){
							return obj[o.value].toString();
						}else return '';
					} else {
						return '';
					}
				}},
			{ key: 'um',label:'UM'},
			{ key: 'cant', label: 'Cant'},
			{ key: 'pret_unitar', label: 'Pret Unitar', formatter: formatCurrency, className: 'currencyCol'}, 
			{ key: 'valoare_bunuri_tva', label: 'Valoare bunuri cu TVA', formatter: formatCurrency, className: 'currencyCol'},
			{ key: 'nr_receptie', className : 'hidden', label: 'Nr receptie', allowHTML: true}
				      
		];

		// TABLE INIT
		var dataTable = new Y.DataTable({
		    columns: nestedCols,
		    data: remoteData,
		    editEvent: 'click',
		    recordType: ['um', 'cant', 'pret_unitar', 'valoare_bunuri_tva', 'nr_receptie', 'magazin', 'denumire_articol', 
                         'tva', 'val_no_vat', 'store_line_id', 'nr_crt', 'cod_articol','cod_produs']
		}).render('#id-detalii-factura');
		
		// bind the approve invoice button on form
		Y.on('click', function(e){
			e.preventDefault();
			var approve = "OK";
			var refuse = "NOK";
			var ml  = dataTable.data, msg = '', template = '';
			
			var lines = "";
			//get invoice_store_lines
		    ml.each(function (item, i) {
		        var data = item.getAttrs(['store_line_id']);
		        lines += data.store_line_id + ",";
		    });
			//the invoice_store_lines that were approved
		    $('#<portlet:namespace />lineIds').val(lines);
			// REFUZ LA PLATA
			
			if($('input[name="<portlet:namespace />bun_de_plata"]:checked').val() == 0) {
				$('#<portlet:namespace />blockedORnot').val(refuse);
				if($('input[name="<portlet:namespace />motiv_invalidare"]:checked').length == 0) {
					alert("Selectati un motiv de invalidare!");
				}else if($('input[name="<portlet:namespace />vina_furnizor"]:checked').length == 0 && 
						$('input[name="<portlet:namespace />existenta_litigiu"]:checked').val() == 1){
					alert("Completati sectiunea \"Vina furnizorului?\"!");
				} else if ($('input[name="<portlet:namespace />existenta_litigiu"]:checked').val() == 1){
					if ($('#<portlet:namespace />detalii-litigiu').val() == ""){
						alert("Completati sectiunea \"Detalii litigiu\" !");
					} else {
						$('#<portlet:namespace />approve_refuse').click();
					}
				} else  {
					$('#<portlet:namespace />approve_refuse').click();
				}
			} else {
				//BUN DE PLATA
				$('#<portlet:namespace />blockedORnot').val(approve);
				$('#<portlet:namespace />approve_refuse').click();
			}
		},'.approve_inv');
		
		
		// bind the approve invoice button on form
		Y.on('click', function(e){
			e.preventDefault();
			$('#<portlet:namespace />blockedORnot').val("simpleSave");
			$('#<portlet:namespace />approve_refuse').click();
		},'.simpleSave');
		

		 //calculate total val with vat for each line
		 function setValueAssetsWithVat(){
			var ml  = dataTable.data, msg = '', template = '';
			var rows = [];
			ml.each(function (item, i) {
				
		        var data = item.getAttrs(['um', 'cant', 'pret_unitar', 'valoare_bunuri_tva', 'nr_receptie', 'magazin', 'denumire_articol', 
		                                  'tva', 'val_no_vat', 'store_line_id', 'nr_crt', 'cod_articol','cod_produs']);
		        
		        data.valoare_bunuri_tva = parseFloat(parseFloat(data.val_no_vat) + parseFloat(data.val_no_vat) * parseFloat(data.tva) / 100).toFixed(4);
			    // set value on matrix
	        	rows.push(data);
	        	// set value on table
			    dataTable.set('recordset', rows);
			});
		 };
		// set total val with vat for each line
		$(document).ready(function() { setValueAssetsWithVat(); });
	}
);
</script>