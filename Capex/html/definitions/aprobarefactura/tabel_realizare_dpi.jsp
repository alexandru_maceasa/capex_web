<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme"%>

<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui"%>
<%@ taglib uri="http://alloy.liferay.com/tld/aui" prefix="aui"%>
<%@ taglib uri="http://liferay.com/tld/util" prefix="liferay-util"%>

<%@ page import="java.util.ResourceBundle"%>
<%@ page import="java.util.Locale"%>
<liferay-theme:defineObjects />

<portlet:defineObjects />
<portlet:resourceURL id="ajaxURL" var="ajaxURL"/>
<legend><strong><%=resmain.getString("vizualizare_realizare_dpi")%></strong></legend>
<aui:column first="true" columnWidth="100" last="true">
<div id="vizualizare_realizare"></div>
</aui:column>

<script type="text/javascript">
var firstDPIClick = true;
YUI().use( 'aui-datatable', 'aui-datatype', 'datatable-sort',
	function(Y) {
		// data array
		var remoteData = [ ];
		function formatCurrency(cell) {
		    format = {
		    	thousandsSeparator: ",",
		        decimalSeparator: ".",
		        decimalPlaces: noOfDecimalsToDisplay
		    };
		    return Y.DataType.Number.format(Number(cell.value), format);
		}
		// COLUMN INFO and ATTRIBUTES
		var nestedCols = [
			{ key: 'nr_dpi', label: 'Nr DPI' },
			{ key: 'nr_lot', label: 'Nr Lot'},
			{ key: 'denumire_lot', label: 'Denumire Lot'},
			{ key: 'magazin', label: 'Magazin'},
			{ key: 'cod_produs', label:  'Cod Produs'},
			{key:'denumire_produs',label:'Denumire Produs'},
			{ key: 'valoare_dpi', label: 'Valoare DPI'},
			{ key: 'valoare_fact_emise', label: 'Valoare Facturi Emise',allowHTML: true, width: '200px', formatter: formatCurrency, className: 'currencyCol' }, 
			{ key: 'diferenta', label: 'Diferenta', formatter: formatCurrency, className: 'currencyCol'}
			];

		// TABLE INIT
		var dataTable = new Y.DataTable({
		    columns: nestedCols,
		    data: remoteData,
		    editEvent: 'click'
		}).render('#vizualizare_realizare');
		
		//BIND ON CLASS
		$('.showDPIRealizat').click(function(e) {
			e.preventDefault();
  			var action 		="getDPIInfo";
  			//make ajax call only the first time the button was clicked
  			if (firstDPIClick) {
  				makeAjaxCallDpi(action);
  				firstDPIClick = false;
  			} else if ($('#vizualizare_realizare_dpi').css('display') =='none'){
  				//the ajax call was already made so just show the section
  				$('#vizualizare_realizare_dpi').show();
  			}else {
  				//hide the section
  				$('#vizualizare_realizare_dpi').hide();
  			}
  		});
		
		
		function makeAjaxCallDpi(action) {
  			$.ajax({
  				type: "POST",
  				url: "<%=ajaxURL%>",
  				data: "<portlet:namespace />action=" + action + 
					"&<portlet:namespace />storeLineIds=" + '<%=storeLineIds %>',
  				success: function(msg) {
  					$('#vizualizare_realizare_dpi').show();
  					// get table data
  					if (msg.trim() != ""){
  						var jsonEntries = jQuery.parseJSON(msg);
  						remoteData = eval(jsonEntries.values);
  						$('#<portlet:namespace/>val_bugetata').val(jsonEntries.valoare_dpi);
  						$('#<portlet:namespace/>total_realizat').val(jsonEntries.valoare_facturi);
  						$('#<portlet:namespace/>diferenta').val(jsonEntries.diferenta);
  						dataTable.set('data', remoteData);
  					} else {
  						dataTable.set('data', []);
  					}
  				}
  			});
  		}
	}
);
</script>