<%@page import="java.util.HashMap"%>
<%@page import="java.util.List"%>
<%@page import="com.profluo.ecm.model.vo.DefCurrencyVo"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme" %>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>
<%@ taglib uri="http://alloy.liferay.com/tld/aui" prefix="aui" %>
<%@ taglib uri="http://liferay.com/tld/util" prefix="liferay-util" %>

<liferay-theme:defineObjects />
<portlet:defineObjects />
	
<%
	// retrieve all currencies from DB
	List<HashMap<String,Object>> allCurrencies = DefCurrencyVo.getInstance();
	
	String id = null;
	String retea = null;
	try {
		if (renderRequest.getParameter("id") != null) {
			id = renderRequest.getParameter("id");
			retea = renderRequest.getParameter("retea");
		}
	} catch (Exception e) { }
%>

<%-- Links used in the view --%>
<%-- Submit URL, mapped to the addInvoice method in the class Invoice.java --%>
<portlet:actionURL name="saveListInit" var="submitURL" />

<portlet:renderURL var="backURL">
	<portlet:param name="mvcPath" value="/html/definitions/deflistainitiativecontroller/view.jsp"></portlet:param>
</portlet:renderURL>

<aui:form action="<%= submitURL%>" method="post">
<aui:fieldset>
	<% if (id == null) { %>
	<legend>Adaugare initiativa</legend>
	<% } else { %>
	<legend>Editare initiativa</legend>
	<% } %>
	
	<liferay-ui:success key="update_ok" message="update_ok" />
	<liferay-ui:error key="insert_nok" message="update_nok" />
	<liferay-ui:error key="insert_nok" message="insert_nok" />
	<liferay-ui:error key="params_nok" message="params_nok" />
	<liferay-ui:error key="entry_exists" message="entry_exists" />
<%-- 	<%System.out.println("fdfdsdfssfsdfsdfsd: "+retea); %> --%>
	
	<% if (id != null) { %>
	<aui:layout>
		<aui:column columnWidth="25" first="true">
			<label style="padding-top:5px" for="<portlet:namespace />id">ID: </label>
		</aui:column>
		<aui:column columnWidth="25">
			<aui:input label="" name="id" id="id" type="text" value="" style="width:90%" readonly="readonly" />
		</aui:column>
	</aui:layout>
	<% } %>
	
	<aui:layout>
		<aui:column columnWidth="25" first="true">
			<label style="padding-top:5px" for="<portlet:namespace />name">Nume initiativa: </label>
		</aui:column>
		<aui:column columnWidth="25">
			<aui:input label="" name="name" id="name" type="text" value="" style="width:90%">
				 <aui:validator name="required" errorMessage="Campul este obligatoriu." />
			</aui:input>
		</aui:column>
	</aui:layout>
	
	<aui:layout>
		<aui:column columnWidth="25" first="true">
			<label style="padding-top:5px" for="<portlet:namespace />code">Cod initiativa: </label>
		</aui:column>
		<aui:column columnWidth="25">
			<aui:input label="" name="code" id="code" type="text" value="" style="width:90%" readonly="true">
<%-- 				 <aui:validator name="required" errorMessage="Campul este obligatoriu." /> --%>
			</aui:input>
		</aui:column>
	</aui:layout>
	
	<aui:layout>
		<aui:column columnWidth="25" first="true">
			<label for="<portlet:namespace />currency" style="padding-top: 5px">Moneda: </label>
		</aui:column>
		<aui:column columnWidth="25">
			<aui:select id="currency" name="currency" label="" style="width:95%" required="true" >
			<% for (int i = 0; i < allCurrencies.size(); i++) { %>
				<% if (allCurrencies.get(i).get("status").equals("A")) {%>
					<aui:option value="<%=allCurrencies.get(i).get(\"ref\")%>"><%=allCurrencies.get(i).get("ref")%></aui:option>
				<% } %>
			<% } %>
			</aui:select>
		</aui:column>
	</aui:layout>
	
		<aui:layout>
		<aui:column columnWidth="25" first="true">
			<label style="padding-top:5px" for="retea">Retea: </label>
		</aui:column>
		<aui:column columnWidth="25">
			<aui:select id="retea" name="retea" label="" style="width:95%" required="true">
				<aui:option selected="selected" value="<%=retea%>"></aui:option>
				<aui:option value="1">Super</aui:option>
				<aui:option value="2">Hyper</aui:option>
				<aui:option value="3">Supeco</aui:option>
				<aui:option value="4">Sediu</aui:option>
				<aui:option value="5">Express</aui:option>
			</aui:select>
		</aui:column>
	</aui:layout>
	<aui:layout>
		<aui:column columnWidth="25" first="true">
			<label style="padding-top:5px" for="start_date">Data inceput: </label>
		</aui:column>
		<aui:column columnWidth="25">
			<aui:input label="" name="start_date" id="start_date" type="text" value="" style="width:90%">
				 <aui:validator name="required" errorMessage="Campul este obligatoriu." />
			</aui:input>
		</aui:column>
	</aui:layout>
	<aui:layout>
		<aui:column columnWidth="25" first="true">
			<label style="padding-top:5px" for="end_date">Data sfarsit: </label>
		</aui:column>
		<aui:column columnWidth="25">
			<aui:input label="" name="end_date" id="end_date" type="text" value="" style="width:90%">
			<aui:validator name="required" errorMessage="Campul este obligatoriu." />
			<aui:validator name="date" errorMessage="Informatia nu este de tip data"></aui:validator>
			</aui:input>
		</aui:column>
	</aui:layout>
	<script type="text/javascript">
		YUI({lang:'ro'}).use('aui-datepicker', function(Y) {
			new Y.DatePicker({
				trigger : '#<portlet:namespace />end_date',
				mask : '%Y-%m-%d',
				popover : {
					zIndex : 1
				},
				calendar : {
					dateFormat : '%Y-%m-%d'
				}
			});
			new Y.DatePicker({
				trigger : '#<portlet:namespace />start_date',
				mask : '%Y-%m-%d',
				popover : {
					zIndex : 1
				},
				calendar : {
					dateFormat : '%Y-%m-%d'
				}
			});
			
		
		});
	</script>
	
	
	<aui:layout>
		<aui:column columnWidth="25" first="true">
			<label for="<portlet:namespace />status" style="padding-top:5px">Status: </label>
		</aui:column>
		<aui:column columnWidth="25">
			<aui:select id="status" name="status" label="" style="width:95%" required="true">
				<aui:option selected="selected" value="">Status</aui:option>
				<aui:option value="A">Activ</aui:option>
				<aui:option value="I">Inactiv</aui:option>
			</aui:select>
		</aui:column>
	</aui:layout>
	
	<aui:layout>
		<aui:column columnWidth="25" first="true">
			<label for="<portlet:namespace />property" style="padding-top:5px">Responsabil: </label>
		</aui:column>
		<% if (id != null) { %>
		<aui:column columnWidth="25">
			<aui:select id="is_property" name="is_property" label="" style="width:95%" required="true">
				<aui:option value="0">Active</aui:option>
				<aui:option value="1">Property</aui:option>
			</aui:select>
		</aui:column>
		<% } else { %>
			<aui:column columnWidth="25">
			<aui:select id="is_property" name="is_property" label="" style="width:95%" required="true">
				<aui:option selected="selected" value="0">Active</aui:option>
				<aui:option value="1">Property</aui:option>
			</aui:select>
		</aui:column>
		<% } %>
	</aui:layout>
	
	<% if (id != null) { %>
	<aui:layout>
		<aui:column columnWidth="25" first="true">
			<label style="padding-top:5px" for="<portlet:namespace />created">Adaugat la data: </label>
		</aui:column>
		<aui:column columnWidth="25" >
			<aui:input label="" name="created" id="created" type="text" value="" style="width:90%"  readonly="readonly"/>
		</aui:column>
	</aui:layout>
	
	<aui:layout>
		<aui:column columnWidth="25" first="true">
			<label style="padding-top:5px" for="<portlet:namespace />modified">Modificat la data: </label>
		</aui:column>
		<aui:column columnWidth="25">
			<aui:input label="" name="modified" id="modified" type="text" value="" style="width:90%"  readonly="readonly"/>
		</aui:column>
	</aui:layout>
	<% } %>
	<aui:layout>
		<aui:column last="true">
			<a class="btn btn-primary-red" href="<%= backURL %>">Inapoi</a>
			<aui:button value="Salveaza" cssClass="btn btn-primary" style="margin:0px" type="submit"/>
		</aui:column>
	</aui:layout>
</aui:fieldset>
</aui:form>