<%@page import="com.profluo.ecm.model.db.ListaInit"%>
<%@page import="com.profluo.ecm.model.vo.ListaInitVo"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.List"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>

<portlet:defineObjects />

<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme" %>

<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>
<%@ taglib uri="http://alloy.liferay.com/tld/aui" prefix="aui" %>
<%@ taglib uri="http://liferay.com/tld/util" prefix="liferay-util" %>

<%@ page import="java.util.ResourceBundle" %>
<%@ page import="java.util.Locale" %>

<liferay-theme:defineObjects />
<portlet:defineObjects />
<% //-- PAGINARE 
int start = 0;
int count = 15;
String code = "";
String name = "";
int total = ListaInit.getAllFilteredCount(code, name);
System.out.println("Total "+total);
%>

<portlet:resourceURL id="ajaxURL" var="ajaxURL"/>
<script type="text/javascript">
var start="<%=start%>";
var count="<%=count%>";
var total="<%=total%>";
//filter
var code = "<%=code%>";
var name = "<%=name%>";

</script>
<%
	locale = renderRequest.getLocale();
	ResourceBundle resmain = portletConfig.getResourceBundle(locale);
	// retrieve initiatives from the database
	List<HashMap<String,Object>> allInitiatives = ListaInit.getAllFiltered(start, count, "", "");
%>

<%-- Links --%>
<portlet:renderURL var="addEntryURL" >
	<portlet:param name="mvcPath" value="/html/definitions/deflistainitiativecontroller/add_lista_ini.jsp"></portlet:param>
</portlet:renderURL>
<portlet:resourceURL id="exportAction" var="exportURL">
	<portlet:param name="action" value="exportExcel"/>
</portlet:resourceURL>

<liferay-ui:success key="insert_ok" message="insert_ok" />
<liferay-ui:success key="update_ok" message="update_ok" />

<aui:layout>
	<aui:column columnWidth="100" last="true" style="text-align:right">
		<%-- TODO: remains commented out untill we have a confirmation of what will happen in Capex I --%>
		<a class="btn btn-primary" href="<%=exportURL.toString()%>"> Export Excel </a>
		<a class="btn btn-primary" href="<%=addEntryURL.toString()%>"> Adauga initiativa</a>
		
	</aui:column>
</aui:layout>
<br/>
<aui:layout>
	<aui:column first="true" columnWidth="100" last="true">
		<div id="def-list-init"></div>
	</aui:column>
</aui:layout>

<%-- HTML for PAGINATION --%>
<%@ include file="/html/includes/pagination.jsp" %>

<script type="text/javascript">
YUI().use( 'aui-datatable', 'aui-datatype', 'datatable-sort',
	function(Y) {
		function edit(sel_id) {
		    var ml  = dataTableInitiative.data, msg = '', template = '';
	
		    ml.each(function (item, i) {
		        var data = item.getAttrs(['id_init', 'name', 'code', 'year', 'currency', 'status', 'modified', 'created', 'owner', 'retea', 'start_date','end_date']);

		        if (data.id_init == sel_id) {
					$('#<portlet:namespace />id').val(data.id_init);
					$('#<portlet:namespace />code').val(data.code);
					$('#<portlet:namespace />name').val(data.name);
					$('#<portlet:namespace />year').val(data.year);
					$('#<portlet:namespace />currency').val(data.currency);
					$('#<portlet:namespace />status').val(data.status);
					$('#<portlet:namespace />created').val(data.created);
					$('#<portlet:namespace />is_property').val(data.owner);
					$('#<portlet:namespace />modified').val(data.modified);
					$('#<portlet:namespace />retea').val(data.retea);
					$('#<portlet:namespace />start_date').val(data.start_date);
					$('#<portlet:namespace />end_date').val(data.end_date);
					$('#<portlet:namespace />editInit').submit();
		        }
		    });
		}
	
		// data array
		var remoteDataInit = [
		    <% for (int i = 0; i < allInitiatives.size(); i++) { %>
     		{
   				id_init: <%=allInitiatives.get(i).get("id_init")%>,
   				code: '<%=allInitiatives.get(i).get("code").toString().replaceAll("'", "\\\\'") %>',
   				name: '<%=allInitiatives.get(i).get("name").toString().replaceAll("'", "\\\\'") %>',
   				year: '<%=allInitiatives.get(i).get("year")%>',
   				owner: '<%=allInitiatives.get(i).get("owner")%>',
   				status: '<%=allInitiatives.get(i).get("status")%>',
   				modified: '<%=allInitiatives.get(i).get("modified")%>',
   				created: '<%=allInitiatives.get(i).get("created")%>',
   				retea: '<%=allInitiatives.get(i).get("retea")%>',
   				start_date: '<%=allInitiatives.get(i).get("start_date")%>',
   				end_date: '<%=allInitiatives.get(i).get("end_date")%>'
   			}
   			<% if (i != (allInitiatives.size() - 1)) { out.print(","); } %>
     		<% } %>
		];
		
		// COLUMN INFO and ATTRIBUTES
		var nestedColsInit = [
			{ key: 'id_init', label: 'ID', className: "hiddencol" },
			{ key: 'code', label: '<input id="filter-cod" name="filter-cod" label="" placeholder="Cod" style="width:30px;margin:0">' +
				'</input>', formatter: '{value}',allowHTML:true},
			{ key: 'name', label: '<input id="filter-nume" name="filter-nume" label="" placeholder="Nume" style="width:100px;margin:0">' +
					'</input>', formatter: '{value}',allowHTML:true },
			{ key: 'year', label: 'An', formatter: '{value}', className: "hiddencol" },
			{ key: 'owner', label: 'Responsabil', 
				formatter: function(o){
					if(o.value == 0 ) {
						return 'Active';
					} else {
						return 'Property';
					}
				} 
			},
			{ key: 'status', label: 'Status', allowHTML: true, formatter: function(o) { 
				if(o.value == 'A') {return '<font color="green">Activ<font>';} 
				else { return '<font color="red">Inactiv</font>'; } } },
			
			{ key: 'actiuni', label: 'Actiuni', allowHTML: true, 
				// gets the id of the row and creates an unique link for every row
				formatter: '<a href="#" id="rowinit{id_init}" class="editrowinit">Editare</a>' },
			{ key: 'retea', label: 'retea', className: "hiddencol" },
			{ key: 'start_date', label: 'start_date', className: "hiddencol" },
			{ key: 'end_date', label: 'end_date', className: "hiddencol" }
					      
		];

		// TABLE INIT
		var dataTableInitiative = new Y.DataTable({
		    columns: nestedColsInit,
		    data: remoteDataInit,
		    recordType: ['id_init', 'name', 'code', 'status', 'year', 'currency', 'modified', 'created','retea', 'start_date', 'end_date'],
		    editEvent: 'click'
		}).render('#def-list-init');
		
		//dataTableInitiative.get('boundingBox').unselectable();
		
		// Also define a listener on the single TH checkbox to
		// toggle all of the checkboxes
		dataTableInitiative.delegate('click', function (e) {
		    // undefined to trigger the emptyCellValue
		    var targetDoc = e.target.get('id');
			edit(targetDoc.replace('rowinit',''));
		}, '.editrowinit', dataTableInitiative);
		// PAGINATION
		// set status of navigation buttons on page load
		setNavigationButtonsState('<portlet:namespace />');
		
		$('.navigation').click(function(e) {
			var elementT = e.target;
			var elementId = elementT.id;
			var action = "";
			
			// determine which button was pressed and set the value of start accordingly
			if (elementId.indexOf("first") > -1) { action = "first"; start = 0; }
			if (elementId.indexOf("prev") > -1) { action = "prev"; if (start - count <= 0) {start = 0} else { start -= count; } }
			if (elementId.indexOf("next") > -1) { action = "next"; start = parseInt(start) + parseInt(count); }
			if (elementId.indexOf("last") > -1) { action = "last"; start = parseInt(parseInt(total) / parseInt(count)) * count;}
			
			makeAjaxCall(action);
		});

		// filter code based on change
		$('#filter-cod').keyup(function (e) {
  		    if ($("#filter-cod").is(":focus") && (e.keyCode == 13)) {
				var elementT 	= e.target;
				var elementId 	= elementT.id;
				var action 		= "filter";
				// get selected value
				code = elementT.value;
				name = $('#filter-nume').val();
				// reset start page 
				start = 0;
				// reset count
				total = 0;
				// make ajax call
				makeAjaxCall(action);
  		    }
		});
		
		$('#filter-nume').keyup(function (e) {
			if ($("#filter-nume").is(":focus") && (e.keyCode == 13)) {
				var elementT 	= e.target;
				var elementId 	= elementT.id;
				var action 		= "filter";
				// get selected value
				name = elementT.value;
				code = $('#filter-cod').val();
				// reset start page 
				start = 0;
				// reset count
				total = 0;
				// make ajax call
				makeAjaxCall(action);
			}
		});

		
		function makeAjaxCall(action) {
			$.ajax({
				type: 	"POST",
				url: 	"<%=ajaxURL%>",
				data: 	"<portlet:namespace />action=" + action + 
						"&<portlet:namespace />start=" + start +
						"&<portlet:namespace />count=" + count +
						"&<portlet:namespace />total=" + total +						
						"&<portlet:namespace />code=" + code +
						"&<portlet:namespace />name=" + name,
				success: function(msg) {
					// get table data
					if (msg != ""){
						var jsonEntries = jQuery.parseJSON(msg);
						total = jsonEntries.total;
						dataTableInitiative.set('data', eval(jsonEntries.values));
					} else {
						dataTableInitiative.set('data', []);
					}
					
					// set status of navigation buttons on page load
					setNavigationButtonsState('<portlet:namespace />');
				}
			});
		}
		// PAGINATION
	}
);
</script>

<aui:form method="post" action="<%=addEntryURL.toString() %>" name="editInit" id="editInit" style="margin:0;padding:0">
	<aui:input type="hidden" id="id" name="id"/>
	<aui:input type="hidden" id="is_property" name="is_property"/>
	<aui:input type="hidden" id="name" name="name"/>
	<aui:input type="hidden" id="code" name="code"/>
	<aui:input type="hidden" id="year" name="year"/>
	<aui:input type="hidden" id="currency" name="currency"/>
	<aui:input type="hidden" id="status" name="status"/> 
	<aui:input type="hidden" id="created" name="created"/> 
	<aui:input type="hidden" id="modified" name="modified"/> 
	<aui:input type="hidden" id="retea" name="retea"/> 
	<aui:input type="hidden" id="start_date" name="start_date"/> 
	<aui:input type="hidden" id="end_date" name="end_date"/> 
</aui:form>