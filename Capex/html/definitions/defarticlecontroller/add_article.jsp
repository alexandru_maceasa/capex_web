<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme" %>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>
<%@ taglib uri="http://alloy.liferay.com/tld/aui" prefix="aui" %>
<%@ taglib uri="http://liferay.com/tld/util" prefix="liferay-util" %>

<%@page import="com.profluo.ecm.model.vo.DefCategoryVo" %>
<%@page import="com.profluo.ecm.model.vo.DefIASVo" %>
<%@page import="com.profluo.ecm.model.vo.DefProductVo" %>
<%@page import="com.profluo.ecm.model.vo.DefLotVo" %>
<%@page import="com.profluo.ecm.model.vo.DefIFRSVo" %>

<%@page import="java.util.HashMap"%>
<%@page import="java.util.List"%>

<liferay-theme:defineObjects />
<portlet:defineObjects />
	
<%
	String id = renderRequest.getParameter("id");
	List<HashMap<String,Object>> products=DefProductVo.getInstance();
	List<HashMap<String,Object>> categories=DefCategoryVo.getInstance();
	List<HashMap<String,Object>> lots=DefLotVo.getInstance();
	//List<HashMap<String,Object>> articles = DefArticleVo.getInstance();
	List<HashMap<String,Object>> allIas=DefIASVo.getInstance();
	List<HashMap<String,Object>> allIfrs=DefIFRSVo.getInstance();
%>

<%-- Links used in the view --%>
<%-- Submit URL, mapped to the addInvoice method in the class Invoice.java --%>
<portlet:actionURL name="saveArticle" var="submitURL" />

<portlet:renderURL var="backURL">
	<portlet:param name="mvcPath" value="/html/definitions/defarticlecontroller/view.jsp"></portlet:param>
</portlet:renderURL>

<aui:form action="<%= submitURL %>" method="post">

<aui:fieldset>
	<% if (id == null) { %>
	<legend>Adaugare articol</legend>
	<% } else { %>
	<legend>Editare articol</legend>
	<% } %>

	<liferay-ui:error key="update_nok" message="update_nok" />
	<liferay-ui:error key="insert_nok" message="insert_nok" />
	<liferay-ui:error key="params_nok" message="params_nok" />	
	<liferay-ui:success key="update_ok" message="update_ok" />

	
	<% if (id != null) { %>
	<aui:layout>
		<aui:column columnWidth="25" first="true">
			<label style="padding-top:5px" for="<portlet:namespace />id">ID: </label>
		</aui:column>
		<aui:column columnWidth="25" style="text-align:right">
			<aui:input label="" name="id" id="id" type="text" value="" style="width:90%" readonly="readonly" />
		</aui:column>
	</aui:layout>
	<% } %>
	
<%-->	<aui:layout>
		<aui:column columnWidth="25" first="true">
			<label style="padding-top:5px" for="<portlet:namespace />inv_no">Cod Articol : </label>
		</aui:column>
		<aui:column columnWidth="25" style="text-align:right">
			<aui:input label="" name="code_no" id="code_no" type="text" value="" style="width:90%">
				 <aui:validator name="required" errorMessage="Campul este obligatoriu." />
			</aui:input>
		</aui:column>
	</aui:layout>
--%>	
	<aui:layout>
		<aui:column columnWidth="25" first="true">
			<label style="padding-top:5px" for="<portlet:namespace />name">Nume: </label>
		</aui:column>
		<aui:column columnWidth="25" style="text-align:right">
			<aui:input label="" name="name" id="name" type="text" value="" style="width:90%">
				 <aui:validator name="required" errorMessage="Campul este obligatoriu." />
			</aui:input>
		</aui:column>
	</aui:layout>
	
	<aui:layout>
		<aui:column columnWidth="25" first="true">
			<label style="padding-top:5px" for="<portlet:namespace />produs_id">Produs: </label>
		</aui:column>
		<aui:column columnWidth="25" style="text-align:right">
			<aui:select id="produs_id" name="produs_id" type="text" value="" label="" style="width:95%" required="true">
		<aui:option selected="selected" value="">Produs</aui:option>
		<% for (int i = 0;i < products.size(); i++) { %>
		<% if (products.get(i).get("status").equals("A")){ %>
			<aui:option value="<%=products.get(i).get(\"id\") %>"><%=products.get(i).get("name")%></aui:option>
		<% } %>
		<% } %>
			</aui:select>
		</aui:column>
	</aui:layout>
	
	<aui:layout>
		<aui:column columnWidth="25" first="true">
			<label style="padding-top:5px" for="<portlet:namespace />lot_id">Lot </label>
		</aui:column>
		<aui:column columnWidth="25" style="text-align:right">
			<aui:select id="lot_id" name="lot_id" type="text" value="" label="" style="width:95%" required="true">
		<aui:option selected="selected" value="">Lot</aui:option>
		<% for (int i = 0;i < lots.size(); i++) { %>
		<% if (lots.get(i).get("status").equals("A")){ %>
			<aui:option value="<%=lots.get(i).get(\"id\") %>"><%=lots.get(i).get("name")%></aui:option>
		<% } %>
		<% } %>
			</aui:select>
		</aui:column>
	</aui:layout>
	
	<aui:layout>
		<aui:column columnWidth="25" first="true">
			<label style="padding-top:5px" for="<portlet:namespace />cat_id">Categorie </label>
		</aui:column>
		<aui:column columnWidth="25" style="text-align:right">
			<aui:select id="cat_id" name="cat_id" type="text" value="" label="" style="width:95%" required="true">
		<aui:option selected="selected" value="">Lot</aui:option>
		<% for (int i = 0;i < categories.size(); i++) { %>
		<% if (categories.get(i).get("status").equals("A")){ %>
			<aui:option value="<%=categories.get(i).get(\"id\") %>"><%=categories.get(i).get("name")%></aui:option>
		<% } %>
		<% } %>
			</aui:select>
		</aui:column>
	</aui:layout>
	
	
	
	<aui:layout>
		<aui:column columnWidth="25" first="true">
			<label style="padding-top:5px" for="<portlet:namespace />flag_it">Clasificare: </label>
		</aui:column>
		<aui:column columnWidth="25" style="text-align:right">
			<aui:select id="flag_it" name="flag_it" label="" style="width:95%" required="true">
				<aui:option value="0">Active</aui:option>
				<aui:option value="1">IT-Hardware</aui:option>
				<aui:option value="2">IT-Software</aui:option>
				<aui:option value="3">Securitate</aui:option>
				<aui:option value="4">Deco</aui:option>
			</aui:select>
		</aui:column>
	</aui:layout>
	<aui:layout>
		<aui:column columnWidth="25" first="true">
			<label for="<portlet:namespace /><portlet:namespace />status" style="padding-top:5px">Status: </label>
		</aui:column>
		<aui:column columnWidth="25" style="text-align:right">
			<aui:select id="status" name="status" label="" style="width:95%" required="true">
				<aui:option selected="selected" value="">Status</aui:option>
				<aui:option value="A">Activ</aui:option>
				<aui:option value="I">Inactiv</aui:option>
			</aui:select>
		</aui:column>
	</aui:layout>
		
	<aui:layout>
		<aui:column columnWidth="25" first="true">
			<label style="padding-top:5px" for="<portlet:namespace />ias_id">IAS: </label>
		</aui:column>
		<aui:column columnWidth="25" style="text-align:right">
			<aui:select id="ias_id" name="ias_id" type="text" value="" label="" style="width:95%" required="true">
		<aui:option selected="selected" value="">IAS</aui:option>
		<% for (int i = 0;i < allIas.size(); i++) { %>
		<% if (allIas.get(i).get("status").equals("A")){ %>
			<aui:option value="<%=allIas.get(i).get(\"id\") %>"><%=allIas.get(i).get("name")%></aui:option>
		<% } %>
		<% } %>
			</aui:select>
		</aui:column>
	</aui:layout>
	
	<aui:layout>
		<aui:column columnWidth="25" first="true">
			<label style="padding-top:5px" for="<portlet:namespace />ifrs_id">IFRS: </label>
		</aui:column>
		<aui:column columnWidth="25" style="text-align:right">
			<aui:select id="ifrs_id" name="ifrs_id" type="text" value="" label="" style="width:95%" required="true">
		<aui:option selected="selected" value="">IFRS</aui:option>
		<% for (int i = 0;i < allIfrs.size(); i++) { %>
		<% if (allIfrs.get(i).get("status").equals("A")){ %>
			<aui:option value="<%=allIfrs.get(i).get(\"id\") %>"><%=allIfrs.get(i).get("name")%></aui:option>
		<% } %>
		<% } %>
			</aui:select>
		</aui:column>
	</aui:layout>
	
	
	
	<% if (id != null) { %>
		
	<aui:layout>
		<aui:column columnWidth="25" first="true">
			<label style="padding-top:5px" for="<portlet:namespace />modified">Modificat la data: </label>
		</aui:column>
		<aui:column columnWidth="25" style="text-align:right">
			<aui:input label="" name="modified" id="modified" type="text" value="" style="width:90%"  readonly="readonly"/>
		</aui:column>
	</aui:layout>
	<aui:layout>
		<aui:column columnWidth="25" first="true">
			<label style="padding-top:5px" for="<portlet:namespace />created">Adaugat la data: </label>
		</aui:column>
		<aui:column columnWidth="25" style="text-align:right">
			<aui:input label="" name="created" id="created" type="text" value="" style="width:90%"  readonly="readonly"/>
		</aui:column>
	</aui:layout>
	<% } %>
	<aui:layout>
		<aui:column last="true">
			<a class="btn btn-primary-red" href="<%= backURL %>">Inapoi</a>
			<aui:button value="Salveaza" cssClass="btn btn-primary" style="margin:0px" type="submit"/>
		</aui:column>
	</aui:layout>
</aui:fieldset>
</aui:form>