<%@page import="com.profluo.ecm.model.db.DefArticle"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.List"%>

<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme" %>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>
<%@ taglib uri="http://alloy.liferay.com/tld/aui" prefix="aui" %>
<%@ taglib uri="http://liferay.com/tld/util" prefix="liferay-util" %>

<liferay-theme:defineObjects />
<portlet:defineObjects />
		
<% //-- PAGINARE 
int start = 0;
int count = 15;
int total = DefArticle.getCount();
%>

<portlet:resourceURL id="ajaxURL" var="ajaxURL"/>
<script type="text/javascript">
var start="<%=start%>";
var count="<%=count%>";
var total="<%=total%>";
</script>

<%
	// retrieve currencies from the database
	List<HashMap<String,Object>> allArticles = DefArticle.getAll();
%>

<%-- Links --%>
<portlet:renderURL var="addEntryURL" >
	<portlet:param name="mvcPath" value="/html/definitions/defarticlecontroller/add_article.jsp"></portlet:param>
</portlet:renderURL>

<liferay-ui:success key="insert_ok" message="insert_ok" />
<liferay-ui:success key="update_ok" message="update_ok" />

<aui:layout>
	<aui:column columnWidth="100" last="true" style="text-align:right">
		<a class="btn btn-primary" href="<%=addEntryURL.toString()%>">Adauga articol</a>
	</aui:column>
</aui:layout>
<br/>
<aui:layout>
	<aui:column first="true" columnWidth="100" last="true">
		<div id="def-article-group"></div>
	</aui:column>
</aui:layout>

<%-- HTML for PAGINATION --%>
<%@ include file="/html/includes/pagination.jsp" %>

<script type="text/javascript">
YUI().use( 'aui-datatable', 'aui-datatype', 'datatable-sort',
	function(Y) {
		function edit(sel_id) {
		    var ml  = dataTableArticle.data, msg = '', template = '';
	
		    ml.each(function (item, i) {
		        var dataArt = item.getAttrs(['id', 'name','prod_id','lot_id','cat_id','flag_it','status', 'ias_id', 'ifrs_id','created','modified']);

		        if (dataArt.id == sel_id) {
					$('#<portlet:namespace />id').val(dataArt.id);
					$('#<portlet:namespace />name').val(dataArt.name);
					$('#<portlet:namespace />prod_id').val(dataArt.prod_id);
				    $('#<portlet:namespace />lot_id').val(dataArt.lot_id);
				    $('#<portlet:namespace />cat_id').val(dataArt.cat_id);					
					$('#<portlet:namespace />flag_it').val(dataArt.flag_it);
					$('#<portlet:namespace />status').val(dataArt.status);
					$('#<portlet:namespace />ias_id').val(dataArt.ias_id);
					$('#<portlet:namespace />ifrs_id').val(dataArt.ifrs_id);
					$('#<portlet:namespace />modified').val(dataArt.modified);
					$('#<portlet:namespace />created').val(dataArt.created);					
					$('#<portlet:namespace />editArticle').submit();
		        }
		    });
		}
	
		// data array
		var remoteDataArt = [
		    <% if (allArticles != null) { %>
		    <% for (int i = 0; i < allArticles.size(); i++) { %>
     		{
   				id: <%=allArticles.get(i).get("id")%>, 
   				name:'<%=allArticles.get(i).get("name").toString().replaceAll("'", "\\\\'")%>',
   				prod_id:'<%=allArticles.get(i).get("prod_name").toString().replaceAll("'", "\\\\'")%>',
   				lot_id:'<%=allArticles.get(i).get("lot_name").toString().replaceAll("'", "\\\\'")%>',
   				cat_id:'<%=allArticles.get(i).get("cat_name").toString().replaceAll("'", "\\\\'")%>',
   				flag_it:'<%=allArticles.get(i).get("flag_it").toString().replaceAll("'", "\\\\'")%>',
   				status:'<%=allArticles.get(i).get("status")%>',
   				ias_id:'<%=allArticles.get(i).get("ias_id")%>',
   				ifrs_id: '<%=allArticles.get(i).get("ifrs_id")%>', 
   				modified: '<%=allArticles.get(i).get("modified")%>',
   	   			created: '<%=allArticles.get(i).get("created")%>'
   			}
   			<% if (i != (allArticles.size() - 1)) { out.print(","); } %>
     		<% } %>
     		<% } %>
		];
		
		// COLUMN INFO and ATTRIBUTES
		var nestedColsArt = [
			{ key: 'id', label: 'ID' },
			{ key: 'name', label: 'Nume'},
			{ key: 'prod_id', label: 'Produs'},
			{ key: 'lot_id', label: 'Lot'},
			{ key: 'cat_id', label: 'Categorie'},			
			{ key: 'flag_it', label: 'Flag IT'},
			{ key: 'status', label: 'Status', allowHTML: true, formatter: function(o) { 
				if(o.value == 'A') {return '<font color="green">Activ<font>';} 
				else { return '<font color="red">Inactiv</font>'; } } },
			{ key: 'ias_id', label: 'IAS'},
			{ key: 'ifrs_id', label: 'IFRS'},
			{ key: 'actiuni', label: 'Actiuni', allowHTML: true, 
				// gets the id of the row and creates an unique link for every row
				formatter: '<a href="#" id="rowart{id}" class="editrowart">Editare</a>' }		      
		];

		// TABLE INIT
		var dataTableArticle = new Y.DataTable({
		    columns: nestedColsArt,
		    data: remoteDataArt,
		    recordType: ['id', 'name','prod_id','lot_id','cat_id','flag_it','status', 'ias_id', 'ifrs_id','modified','created'],
		    editEvent: 'click'
		}).render('#def-article-group');
		
		//dataTableArticle.get('boundingBox').unselectable();
		
		// Also define a listener on the single TH checkbox to
		// toggle all of the checkboxes
		dataTableArticle.delegate('click', function (e) {
		    // undefined to trigger the emptyCellValue
		    var targetArt = e.target.get('id');
			edit(targetArt.replace('rowart',''));
		}, '.editrowart', dataTableArticle);
		
		// PAGINATION
		// set status of navigation buttons on page load
		setNavigationButtonsState('<portlet:namespace />');
		$('.navigation').click(function(e) {
			var elementTd = e.target;
			var elementId = elementTd.id;
			var action = "";
			
			// determine which button was pressed and set the value of start accordingly
			if (elementId.indexOf("first") > -1) { action = "first"; start = 0; }
			if (elementId.indexOf("prev") > -1) { action = "prev"; if (start - count <= 0) {start = 0} else { start -= count; } }
			if (elementId.indexOf("next") > -1) { action = "next"; start = parseInt(start) + parseInt(count); }
			if (elementId.indexOf("last") > -1) { action = "last"; start = parseInt(parseInt(total) / parseInt(count)) * count;}
			
			$.ajax({
				type: "POST",
				url: "<%=ajaxURL%>",
				data: "<portlet:namespace />action=" + action + "&<portlet:namespace />start=" + start + "&<portlet:namespace />count=" + count,
				success: function(msg) {
					// set status of navigation buttons on page load
					setNavigationButtonsState('<portlet:namespace />');
					
					console.log(msg);
					var jsonEntriesd = jQuery.parseJSON(msg);
					dataTableArticle.set('data', eval(jsonEntriesd.values));
				}
			});
		});
		// PAGINATION
	}
);
</script>

<aui:form method="post" action="<%=addEntryURL.toString() %>" name="editArticle" id="editArticle" style="margin:0;padding:0">
	<aui:input type="hidden" id="id" name="id"/>
	<aui:input type="hidden" id="name" name="name"/>
	<aui:input type="hidden" id="prod_id" name="prod_id"/>
	<aui:input type="hidden" id="lot_id" name="lot_id"/>
	<aui:input type="hidden" id="cat_id" name="cat_id"/>
	<aui:input type="hidden" id="flag_it" name="flag_it"/>
	<aui:input type="hidden" id="status" name="status"/> 
	<aui:input type="hidden" id="ifrs_id" name="ifrs_id"/> 	
	<aui:input type="hidden" id="modified" name="modified"/> 	
	<aui:input type="hidden" id="created" name="created"/> 
</aui:form>