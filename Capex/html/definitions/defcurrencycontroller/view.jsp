<%@page import="com.profluo.ecm.model.vo.DefCurrencyVo"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.List"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>

<portlet:defineObjects />

<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme" %>

<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>
<%@ taglib uri="http://alloy.liferay.com/tld/aui" prefix="aui" %>
<%@ taglib uri="http://liferay.com/tld/util" prefix="liferay-util" %>

<%@ page import="java.util.ResourceBundle" %>
<%@ page import="java.util.Locale" %>

<liferay-theme:defineObjects />
<portlet:defineObjects />
	
<%
	locale = renderRequest.getLocale();
	ResourceBundle resmain = portletConfig.getResourceBundle(locale);
	// retrieve currencies from the database
	List<HashMap<String,Object>> allCurrencies = DefCurrencyVo.getInstance();
%>

<%-- Links --%>
<portlet:renderURL var="addEntryURL" >
	<portlet:param name="mvcPath" value="/html/definitions/defcurrencycontroller/add_currency.jsp"></portlet:param>
</portlet:renderURL>

<liferay-ui:success key="insert_ok" message="insert_ok" />
<liferay-ui:success key="update_ok" message="update_ok" />

<aui:layout>
	<aui:column columnWidth="50" last="true" style="text-align:right">
		<a class="btn btn-primary" href="<%=addEntryURL.toString()%>">Adauga valuta</a>
	</aui:column>
</aui:layout>
<br/>
<aui:layout>
	<aui:column first="true" columnWidth="100" last="true">
		<div id="def-currency"></div>
	</aui:column>
</aui:layout>

<script type="text/javascript">
YUI().use( 'aui-datatable', 'aui-datatype', 'datatable-sort',
	function(Y) {
		function edit(sel_id) {
		    var ml  = dataTable.data, msg = '', template = '';
	
		    ml.each(function (item, i) {
		        var data = item.getAttrs(['id', 'name', 'ref', 'status', 'modified', 'created']);

		        if (data.id == sel_id) {
					$('#<portlet:namespace />id').val(data.id);
					$('#<portlet:namespace />name').val(data.name);
					$('#<portlet:namespace />ref').val(data.ref);
					$('#<portlet:namespace />status').val(data.status);
					$('#<portlet:namespace />created').val(data.created);
					$('#<portlet:namespace />modified').val(data.modified);
					$('#<portlet:namespace />editCurrency').submit();
		        }
		    });
		}
	
		// data array
		var remoteData = [
		    <% for (int i = 0; i < allCurrencies.size(); i++) { %>
     		{
   				id: <%=allCurrencies.get(i).get("id")%>, 
   				name: '<%=allCurrencies.get(i).get("name").toString().replaceAll("'", "\\\\'") %>',
   				ref: '<%=allCurrencies.get(i).get("ref")%>',
   				status: '<%=allCurrencies.get(i).get("status")%>',
   				modified: '<%=allCurrencies.get(i).get("modified")%>',
   				created: '<%=allCurrencies.get(i).get("created")%>'
   			}
   			<% if (i != (allCurrencies.size() - 1)) { out.print(","); } %>
     		<% } %>
		];
		
		// COLUMN INFO and ATTRIBUTES
		var nestedCols = [
			{ key: 'id', label: 'ID' },
			{ key: 'name', label: 'Nume'},
			{ key: 'ref', label: 'Referinta'},
			{ key: 'status', label: 'Status', allowHTML: true, formatter: function(o) { 
				if(o.value == 'A') {return '<font color="green">Activ<font>';} 
				else { return '<font color="red">Inactiv</font>'; } } },
			/*
			{ key: 'updated', label:  'Updated'},
			{ key: 'created', label:  'Created'},
			*/
			{ key: 'actiuni', label: 'Actiuni', allowHTML: true, 
				// gets the id of the row and creates an unique link for every row
				formatter: '<a href="#" id="row{id}" class="editrow">Editare</a>' }		      
		];

		// TABLE INIT
		var dataTable = new Y.DataTable({
		    columns: nestedCols,
		    data: remoteData,
		    recordType: ['id', 'name', 'ref', 'status', 'modified', 'created'],
		    editEvent: 'click'
		}).render('#def-currency');
		
		//dataTable.get('boundingBox').unselectable();
		
		// Also define a listener on the single TH checkbox to
		// toggle all of the checkboxes
		dataTable.delegate('click', function (e) {
		    // undefined to trigger the emptyCellValue
		    var target = e.target.get('id');
			edit(target.replace('row',''));
		}, '.editrow', dataTable);
	}
);
</script>

<aui:form method="post" action="<%=addEntryURL.toString() %>" name="editCurrency" id="editCurrency" style="margin:0;padding:0">
	<aui:input type="hidden" id="id" name="id"/>
	<aui:input type="hidden" id="name" name="name"/>
	<aui:input type="hidden" id="ref" name="ref"/>
	<aui:input type="hidden" id="status" name="status"/> 
	<aui:input type="hidden" id="created" name="created"/> 
	<aui:input type="hidden" id="modified" name="modified"/> 
</aui:form>