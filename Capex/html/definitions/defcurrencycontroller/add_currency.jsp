<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme" %>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>
<%@ taglib uri="http://alloy.liferay.com/tld/aui" prefix="aui" %>
<%@ taglib uri="http://liferay.com/tld/util" prefix="liferay-util" %>

<liferay-theme:defineObjects />
<portlet:defineObjects />
	
<%	String id = renderRequest.getParameter("id"); %>

<%-- Links used in the view --%>
<%-- Submit URL, mapped to the addInvoice method in the class Invoice.java --%>
<portlet:actionURL name="saveCurrency" var="submitURL" />

<portlet:renderURL var="backURL">
	<portlet:param name="mvcPath" value="/html/definitions/defcurrencycontroller/view.jsp"></portlet:param>
</portlet:renderURL>

<aui:form action="<%= submitURL %>" method="post">

<aui:fieldset>
	<% if (id == null) { %>
	<legend>Adaugare valuta</legend>
	<% } else { %>
	<legend>Editare valuta</legend>
	<% } %>
	
	<liferay-ui:success key="update_ok" message="update_ok" />
	<liferay-ui:error key="insert_nok" message="update_nok" />
	<liferay-ui:error key="insert_nok" message="insert_nok" />
	<liferay-ui:error key="params_nok" message="params_nok" />
	<liferay-ui:error key="entry_exists" message="entry_exists" />
	
	
	<% if (id != null) { %>
	<aui:layout>
		<aui:column columnWidth="50" first="true">
			<label style="padding-top:5px" for="id">ID: </label>
		</aui:column>
		<aui:column columnWidth="50" last="true" style="text-align:right">
			<aui:input label="" name="id" id="id" type="text" value="" style="width:90%" readonly="readonly" />
		</aui:column>
	</aui:layout>
	<% } %>
	
	<aui:layout>
		<aui:column columnWidth="50" first="true">
			<label style="padding-top:5px" for="name">Nume: </label>
		</aui:column>
		<aui:column columnWidth="50" last="true" style="text-align:right">
			<aui:input label="" name="name" id="name" type="text" value="" style="width:90%" placeholder="Denumire moneda">
				 <aui:validator name="required" errorMessage="Campul este obligatoriu." />
			</aui:input>
		</aui:column>
	</aui:layout>
	
	<aui:layout>
		<aui:column columnWidth="50" first="true">
			<label style="padding-top:5px" for="ref">Referinta: </label>
		</aui:column>
		<aui:column columnWidth="50" last="true" style="text-align:right">
			<aui:input label="" name="ref" id="ref" type="text" value="" style="width:90%" placeholder="REF" maxlength="3">
				 <aui:validator name="required" errorMessage="Campul este obligatoriu." />
				 <aui:validator name="alpha" errorMessage="Campul accepta doar caractere." />
				 <aui:validator name="minLength" errorMessage="Numarul acceptat de caractere este 3.">3</aui:validator>
				 <aui:validator name="maxLength" errorMessage="Numarul acceptat de caractere este 3.">3</aui:validator>
			</aui:input>
		</aui:column>
	</aui:layout>
	
	<aui:layout>
		<aui:column columnWidth="50" first="true">
			<label for="<portlet:namespace />status" style="padding-top:5px">Status: </label>
		</aui:column>
		<aui:column last="true" columnWidth="50" style="text-align:right">
			<aui:select id="status" name="status" label="" style="width:98%" required="true">
				<aui:option selected="selected" value="">Status moneda</aui:option>
				<aui:option value="A">Activ</aui:option>
				<aui:option value="I">Inactiv</aui:option>
			</aui:select>
		</aui:column>
	</aui:layout>
	
	<% if (id != null) { %>
	<aui:layout>
		<aui:column columnWidth="50" first="true">
			<label style="padding-top:5px" for="created">Adaugat la data: </label>
		</aui:column>
		<aui:column columnWidth="50" last="true" style="text-align:right">
			<aui:input label="" name="created" id="created" type="text" value="" style="width:90%"  readonly="readonly"/>
		</aui:column>
	</aui:layout>
	
	<aui:layout>
		<aui:column columnWidth="50" first="true">
			<label style="padding-top:5px" for="modified">Modificat la data: </label>
		</aui:column>
		<aui:column columnWidth="50" last="true" style="text-align:right">
			<aui:input label="" name="modified" id="modified" type="text" value="" style="width:90%"  readonly="readonly"/>
		</aui:column>
	</aui:layout>
	<% } %>
	<aui:layout>
		<aui:column last="true">
			<a class="btn btn-primary-red" href="<%= backURL %>">Inapoi</a>
			<aui:button value="Salveaza" cssClass="btn btn-primary" style="margin:0px" type="submit"/>
		</aui:column>
	</aui:layout>
</aui:fieldset>
</aui:form>