<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme" %>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>
<%@ taglib uri="http://alloy.liferay.com/tld/aui" prefix="aui" %>
<%@ taglib uri="http://liferay.com/tld/util" prefix="liferay-util" %>

<%@ page import="com.profluo.ecm.model.vo.DefCategoryVo" %>
<%@ page import="java.util.HashMap"%>
<%@ page import="java.util.List"%>

<liferay-theme:defineObjects />
<portlet:defineObjects />
	
<%
	// retrieve id in case of edit
	String id = renderRequest.getParameter("id");
	// get lot vo
	// List<HashMap<String,Object>> lots = DefLotVo.getInstance();
	// get categories vo
	List<HashMap<String,Object>> categories = DefCategoryVo.getInstance();	
%>

<%-- Links used in the view --%>
<%-- Submit URL, mapped to the saveLot method in the class DefLotController.java --%>
<portlet:actionURL name="saveLot" var="submitURL" />

<portlet:renderURL var="backURL">
	<portlet:param name="mvcPath" value="/html/definitions/deflotcontroller/view.jsp"></portlet:param>
</portlet:renderURL>

<aui:form action="<%= submitURL %>" method="post">

<aui:fieldset>
	<% if (id == null) { %>
	<legend>Adaugare Lot</legend>
	<% } else { %>
	<legend>Editare Lot</legend>
	<% } %>
	
	<liferay-ui:success key="update_ok" message="update_ok" />
	<liferay-ui:error key="insert_nok" message="update_nok" />
	<liferay-ui:error key="insert_nok" message="insert_nok" />
	<liferay-ui:error key="params_nok" message="params_nok" />
	<liferay-ui:error key="entry_exists" message="entry_exists" />
	
	<% if (id != null) { %>
	<aui:layout>
		<aui:column columnWidth="25" first="true">
			<label style="padding-top:5px" for="<portlet:namespace/>id">ID: </label>
		</aui:column>
		<aui:column columnWidth="25" style="text-align:right">
			<aui:input label="" name="id" id="id" type="text" value="" style="width:90%" readonly="readonly" />
		</aui:column>
	</aui:layout>
	<% } %>
	
	<aui:layout>
		<aui:column columnWidth="25" first="true">
			<label style="padding-top:5px" for="<portlet:namespace/>name">Nume: </label>
		</aui:column>
		<aui:column columnWidth="25" style="text-align:right">
				
			<aui:input label="" name="name" id="name" type="text" value="" style="width:90%">
				 <aui:validator name="required" errorMessage="Campul este obligatoriu." />
			</aui:input>
		</aui:column>
	</aui:layout>
	 
	<aui:layout style="display:none">
		<aui:column columnWidth="25" first="true">
			<label style="padding-top:5px" for="<portlet:namespace/>code">Cod : </label>
		</aui:column>
		<aui:column columnWidth="25" style="text-align:right">
			<aui:input label="" name="code" id="code" type="text" value="" style="width:90%">
			</aui:input>
		</aui:column>
	</aui:layout>
	 
	<aui:layout>
		<aui:column columnWidth="25" first="true">
			<label style="padding-top:5px" for="<portlet:namespace/>category_id">Categorie: </label>
		</aui:column>
		<aui:column columnWidth="25" style="text-align:right">
			<aui:select id="category_id" name="category_id" type="text" value="" label="" style="width:95%" required="true">
				<aui:option selected="selected" value="">Categorie</aui:option>
				<% for (int i = 0;i < categories.size(); i++) { %>
					<% if (categories.get(i).get("status").equals("A")){ %>
						<aui:option value="<%=categories.get(i).get(\"id\") %>"><%=categories.get(i).get("name")%></aui:option>
					<% } %>
				<% } %>
			</aui:select>
		</aui:column>
	</aui:layout>
	
	
	<aui:layout>
		<aui:column columnWidth="25" first="true">
			<label style="padding-top:5px" for="<portlet:namespace/>flag_it">Clasificare: </label>
		</aui:column>
		<aui:column columnWidth="25" style="text-align:right">
			<aui:select id="flag_it" name="flag_it" label="" style="width:95%" required="true">
				<aui:option value="0">Active</aui:option>
				<aui:option value="1">IT-Hardware</aui:option>
				<aui:option value="2">IT-Software</aui:option>
				<aui:option value="3">Securitate</aui:option>
				<aui:option value="4">Deco</aui:option>
				<aui:option value="5">Property</aui:option>
				<aui:option value="6">Active + Property</aui:option>
			</aui:select>
		</aui:column>
	</aui:layout>
	<aui:layout>
		<aui:column columnWidth="25" first="true">
			<label style="padding-top:5px" for="<portlet:namespace/>warranty">Garantie: </label>
		</aui:column>
		<aui:column columnWidth="25" style="text-align:right">
			<aui:select id="warranty" name="warranty" label="" style="width:95%" required="true">
				<aui:option value="Y">Da</aui:option>
				<aui:option value="N">Nu</aui:option>
			</aui:select>
		</aui:column>
	</aui:layout>
	<aui:layout>
		<aui:column columnWidth="25" first="true">
			<label style="padding-top:5px" for="<portlet:namespace/>ias_dt">Durata IAS : </label>
		</aui:column>
		<aui:column columnWidth="25" style="text-align:right">
			<aui:input label="" name="ias_dt" id="ias_dt" type="text" value="" style="width:90%">
				 <aui:validator name="required" errorMessage="Campul este obligatoriu." />
			</aui:input>
		</aui:column>
	</aui:layout>
	<aui:layout>
		<aui:column columnWidth="25" first="true">
			<label style="padding-top:5px" for="<portlet:namespace/>ifrs_dt">Durata IFRS : </label>
		</aui:column>
		<aui:column columnWidth="25" style="text-align:right">
			<aui:input label="" name="ifrs_dt" id="ifrs_dt" type="text" value="" style="width:90%">
				 <aui:validator name="required" errorMessage="Campul este obligatoriu." />
			</aui:input>
		</aui:column>
	</aui:layout>
	<aui:layout>
		<aui:column columnWidth="25" first="true">
			<label for="<portlet:namespace/><portlet:namespace />status" style="padding-top:5px">Status: </label>
		</aui:column>
		<aui:column columnWidth="25" style="text-align:right">
			<aui:select id="status" name="status" label="" style="width:95%" required="true">
				<aui:option selected="selected" value="">Status</aui:option>
				<aui:option value="A">Activ</aui:option>
				<aui:option value="I">Inactiv</aui:option>
			</aui:select>
		</aui:column>
	</aui:layout>
	
	<% if (id != null) { %>
	<aui:layout>
		<aui:column columnWidth="25" first="true">
			<label style="padding-top:5px" for="<portlet:namespace/>modified">Modificat la data: </label>
		</aui:column>
		<aui:column columnWidth="25" style="text-align:right">
			<aui:input label="" name="modified" id="modified" type="text" value="" style="width:90%"  readonly="readonly"/>
		</aui:column>
	</aui:layout>
	<aui:layout>
		<aui:column columnWidth="25" first="true">
			<label style="padding-top:5px" for="<portlet:namespace/>created">Adaugat la data: </label>
		</aui:column>
		<aui:column columnWidth="25" style="text-align:right">
			<aui:input label="" name="created" id="created" type="text" value="" style="width:90%"  readonly="readonly"/>
		</aui:column>
	</aui:layout>
	<% } %>
	<aui:layout>
		<aui:column last="true">
			<a class="btn btn-primary-red" href="<%=backURL %>">Inapoi</a>
			<aui:button value="Salveaza" cssClass="btn btn-primary" style="margin:0px" type="submit"/>
		</aui:column>
	</aui:layout>
</aui:fieldset>
</aui:form>