<%@page import="com.profluo.ecm.model.vo.DefCategoryVo"%>
<%@ page import="com.profluo.ecm.model.db.DefLot"%>
<%@ page import="java.util.HashMap"%>
<%@ page import="java.util.List"%>

<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme" %>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>
<%@ taglib uri="http://alloy.liferay.com/tld/aui" prefix="aui" %>
<%@ taglib uri="http://liferay.com/tld/util" prefix="liferay-util" %>

<liferay-theme:defineObjects />
<portlet:defineObjects />

<% //-- PAGINARE 
int start = 0;
int count = 15;
int total = DefLot.getCount();

String newProductCode = ""; 
try{
	newProductCode = request.getAttribute("newProductCode").toString();	
} catch (Exception e){}
%>
<script type="text/javascript">
var start="<%=start%>";
var count="<%=count%>";
var total="<%=total%>";
//filter
var code="";
var lot_name = "";
var category = "";
</script>
<portlet:resourceURL id="ajaxURL" var="ajaxURL"/>

<portlet:resourceURL id="exportAction" var="exportURL">
	<portlet:param name="action" value="exportExcel"/>
</portlet:resourceURL>
	
<%
//	locale = renderRequest.getLocale();
//	ResourceBundle resmain = portletConfig.getResourceBundle(locale);
	// retrieve currencies from the database
	List<HashMap<String,Object>> allLots = DefLot.getAll(start,count);
	// retrieve currencies from the database
	List<HashMap<String,Object>> allCategories = DefCategoryVo.getInstance();
%>

<%-- Links --%>
<portlet:renderURL var="addEntryURL" >
	<portlet:param name="mvcPath" value="/html/definitions/deflotcontroller/add_lot.jsp"></portlet:param>
</portlet:renderURL>

<liferay-ui:success key="insert_ok" message="insert_ok" />
<liferay-ui:success key="update_ok" message="update_ok" />

<% if(!newProductCode.equals("")) { %>
<div class="div_product_code">
	<p> Codul lotului salvat este: <%=newProductCode%> </p>
</div>
<% } %>
<aui:layout>
	<aui:column columnWidth="100" last="true" style="text-align:right">
		<a class="btn btn-primary" href="<%=exportURL.toString()%>"> Export Excel </a>
		<a class="btn btn-primary" href="<%=addEntryURL.toString()%>">Adauga lot</a>
	</aui:column>
</aui:layout>
<br/>
<aui:layout>
	<aui:column first="true" columnWidth="100" last="true">
		<div id="def-lot-groupe"></div>
	</aui:column>
</aui:layout>

<%-- HTML for PAGINATION --%>
<%@ include file="/html/includes/pagination.jsp" %>

<script type="text/javascript">
YUI().use( 'aui-datatable', 'aui-datatype', 'datatable-sort',
	function(Y) {
		function edit(sel_id) {
		    var ml  = dataTableLot.data, msg = '', template = '';
	
		    ml.each(function (item, i) {
		        var data = item.getAttrs(['id', 'code', 'category_id', 'warranty', 'category_name', 'name', 'flag_it', 'status', 'created', 'modified', 'ias_dt', 'ifrs_dt']);

		        if (data.id == sel_id) {
					$('#<portlet:namespace />id').val(data.id);
					$('#<portlet:namespace />code').val(data.code);
				    $('#<portlet:namespace />category_id').val(data.category_id);
					$('#<portlet:namespace />name').val(data.name);
					$('#<portlet:namespace />flag_it').val(data.flag_it);
					$('#<portlet:namespace />warranty').val(data.warranty);
					$('#<portlet:namespace />ias_dt').val(data.ias_dt);
					$('#<portlet:namespace />ifrs_dt').val(data.ifrs_dt);
					$('#<portlet:namespace />status').val(data.status);
					$('#<portlet:namespace />modified').val(data.modified);
					$('#<portlet:namespace />created').val(data.created);					
					$('#<portlet:namespace />editLot').submit();
		        }
		    });
		}
	
		// data array
		var remoteDataLot = [
		    <% for (int i = 0; i < allLots.size(); i++) { %>
     		{
   				id: <%=allLots.get(i).get("id")%>, 
   				code: '<%=allLots.get(i).get("code").toString().replaceAll("'", "\\\\'")%>',
   				category_name: '<%=allLots.get(i).get("category_name").toString().replaceAll("'", "\\\\'")%>',
   				category_id: '<%=allLots.get(i).get("category_id").toString().replaceAll("'", "\\\\'")%>',
   				name: '<%=allLots.get(i).get("name").toString().replaceAll("'", "\\\\'")%>',
   				flag_it: '<%=allLots.get(i).get("flag_it").toString().replaceAll("'", "\\\\'")%>',
   				warranty: '<%=allLots.get(i).get("warranty").toString()%>',
   				ias_dt: '<%=allLots.get(i).get("ias_dt").toString()%>',
   				ifrs_dt: '<%=allLots.get(i).get("ifrs_dt").toString()%>',
   				status:'<%=allLots.get(i).get("status")%>',
   				modified: '<%=allLots.get(i).get("modified")%>',
   	   			created: '<%=allLots.get(i).get("created")%>'
   			}
   			<% if (i != (allLots.size() - 1)) { out.print(","); } %>
     		<% } %>
		];
		
		// COLUMN INFO and ATTRIBUTES
		var nestedColsLot = [
			{ key: 'id', className: 'hiddencol'},
			{ key: 'code', label: '<input class="field" id="filter-cod" name="filter-cod" label="" placeholder="Cod lot" style="width:70px;margin:0">' +
			'</input>'},
			{ key: 'name', label: '<input class="field" id="filter-name" name="filter-name" label="" placeholder="Nume lot" style="width:150px;margin:0">'+
			'</input>'},
			{ key: 'category_name',label : '<select id="filter-category" name="filter-category" label="" style="width:180px;margin:0">' +
  				'<option selected="selected" value="">Categorie</option>' +
  				<% for (int i = 0; i < allCategories.size(); i++) { %>
  				<% if (allCategories.get(i).get("status").equals("A")) {%>
  				<% out.println("'<option value=\"" + allCategories.get(i).get("id") +"\">" + allCategories.get(i).get("name") +"</option>' + "); %>
  				<% } } %>
  				'</select>'},
			{ key: 'category_id', className: 'hiddencol'},
			{ key: 'flag_it', label: 'Clasificare', formatter: function(o) { 
					if(o.value == 0) {return 'Active';}
					else if(o.value == 1) { return 'IT-Hardware'; }
					else if(o.value == 2) { return 'IT-Software'; }
					else if(o.value == 3) { return 'Securitate'; }
					else if(o.value == 4) { return 'Deco'; }
					else { return ''; }
				}
			},
			{ key: 'warranty', label: 'Garantie', formatter: function(o) {
					if(o.value == 'Y') { return 'Da'; } else if (o.value == 'N') { return 'Nu'; }
				}
			},
			{ key: 'ias_dt', label: 'Durata IAS', formatter:function(o){
				return o.value;
			}},
			{ key: 'ifrs_dt', label: 'Durata IFRS', formatter: function(o){
				return o.vaue;
			} },
			{ key: 'status', label: 'Status', allowHTML: true, formatter: function(o) { 
				if(o.value == 'A') {return '<font color="green">Activ<font>';} 
				else { return '<font color="red">Inactiv</font>'; } } },
			{ key: 'actiuni', label: 'Actiuni', allowHTML: true, 
				// gets the id of the row and creates an unique link for every row
				formatter: '<a href="#" id="rowlot{id}" class="editrowlot">Editare</a>' }		      
		];

		// TABLE INIT
		var dataTableLot = new Y.DataTable({
		    columns: nestedColsLot,
		    data: remoteDataLot,
		    recordType: ['id', 'code', 'category_id', 'category_name', 'warranty', 'name', 'durata_minima', 'flag_it', 'status', 'modified', 'created', 'ias_dt', 'ifrs_dt'],
		    editEvent: 'click'
		}).render('#def-lot-groupe');
		
		//dataTableLot.get('boundingBox').unselectable();
		//set chosen on category filter
		$('#filter-category').chosen();
		
		// Also define a listener on the single TH checkbox to
		// toggle all of the checkboxes
		dataTableLot.delegate('click', function (e) {
		    // undefined to trigger the emptyCellValue
		    var targetLot = e.target.get('id');
			edit(targetLot.replace('rowlot',''));
		}, '.editrowlot', dataTableLot);
		
		// PAGINATION
		// set status of navigation buttons on page load
		setNavigationButtonsState('<portlet:namespace />');
		$('.navigation').click(function(e) {
			var elementT = e.target;
			var elementId = elementT.id;
			var action = "";
			
			// determine which button was pressed and set the value of start accordingly
			if (elementId.indexOf("first") > -1) { action = "first"; start = 0; }
			if (elementId.indexOf("prev") > -1) { action = "prev"; if (start - count <= 0) {start = 0} else { start -= count; } }
			if (elementId.indexOf("next") > -1) { action = "next"; start = parseInt(start) + parseInt(count); }
			if (elementId.indexOf("last") > -1) { action = "last"; start = parseInt(parseInt(total) / parseInt(count)) * count;}
			
			makeAjaxCall(action);
		});

		//filter name based on change
		$('#filter-name').keyup(function(e) {
			if ($("#filter-name").is(":focus") && (e.keyCode == 13)) {
				var elementT 	= e.target;
				var elementId 	= elementT.id;
				var action 		= "filter";
				// get selected value
				lot_name = elementT.value;
				code = $('#filter-cod').val();
				category = $('#filter-category').val();
				// reset start page 
				start = 0;
				// reset count
				total = 0;
				// make ajax call
				makeAjaxCall(action);
			}
		});
		// filter code based on change
		$('#filter-cod').keyup(function(e) {
			if ($("#filter-cod").is(":focus") && (e.keyCode == 13)) {
				var elementT 	= e.target;
				var elementId 	= elementT.id;
				var action 		= "filter";
				// get selected value
				code = elementT.value;
				category = $('#filter-category').val();
				lot_name = $('#filter-name').val();
				// reset start page 
				start = 0;
				// reset count
				total = 0;
				// make ajax call
				makeAjaxCall(action);
			}
		});
		// filter code based on change
		$('#filter-category').change(function(e) {
			var elementT 	= e.target;
			var elementId 	= elementT.id;
			var action 		= "filter";
			// get selected value
			category = elementT.value;
			code = $('#filter-cod').val();
			lot_name = $('#filter-name').val();
			// reset start page 
			start = 0;
			// reset count
			total = 0;
			// make ajax call
			makeAjaxCall(action);
		});
		function makeAjaxCall(action) {
			$.ajax({
				type: 	"POST",
				url: 	"<%=ajaxURL%>",
				data: 	"<portlet:namespace />action=" + action +
						"&<portlet:namespace />start=" + start +
						"&<portlet:namespace />count=" + count +
						"&<portlet:namespace />total=" + total +
						"&<portlet:namespace />code=" + code +
						"&<portlet:namespace />name=" +lot_name +
						"&<portlet:namespace />category=" +category,
				success: function(msg) {
					// get table data
					if (msg != ""){
						var jsonEntries = jQuery.parseJSON(msg);
						total = jsonEntries.total;
						dataTableLot.set('data', eval(jsonEntries.values));
					} else {
						dataTableLot.set('data', []);
					}
					
					// set status of navigation buttons on page load
					setNavigationButtonsState('<portlet:namespace />');
				}
			});
		}
		// PAGINATION
	}
);
</script>

<aui:form method="post" action="<%=addEntryURL.toString() %>" name="editLot" id="editlot" style="margin:0;padding:0">
	<aui:input type="hidden" id="id" name="id"/>
	<aui:input type="hidden" id="code" name="code"/>
	<aui:input type="hidden" id="category_id" name="category_id"/>
	<aui:input type="hidden" id="name" name="name"/>
	<aui:input type="hidden" id="warranty" name="warranty"/>
	<aui:input type="hidden" id="flag_it" name="flag_it"/>
	<aui:input type="hidden" id="ias_dt" name="ias_dt"/>
	<aui:input type="hidden" id="ifrs_dt" name="ifrs_dt"/>
	<aui:input type="hidden" id="status" name="status"/> 
	<aui:input type="hidden" id="modified" name="modified"/> 	
	<aui:input type="hidden" id="created" name="created"/> 
</aui:form>