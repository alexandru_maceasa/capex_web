<%@page import="com.profluo.ecm.model.db.GenericDBStatements"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.List"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme" %>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>
<%@ taglib uri="http://alloy.liferay.com/tld/aui" prefix="aui" %>
<%@ taglib uri="http://liferay.com/tld/util" prefix="liferay-util" %>

<liferay-theme:defineObjects />
<portlet:defineObjects />

<portlet:actionURL name="saveCurs" var="submitURL" />

<portlet:renderURL var="backURL">
	<portlet:param name="mvcPath" value="/html/definitions/defcurslunar/view.jsp"></portlet:param>
</portlet:renderURL>

<% 
	String id = renderRequest.getParameter("id"); 
	String [][] conditions = new String [1][2];
	conditions [0][0] = "id";
	conditions [0][1] = id;
	List<HashMap<String,Object>> oneExchangeRate = GenericDBStatements.getColumnsWithConditions("def_curs_lunar", "id, year, month, valoare_curs", conditions);
	
	System.out.println("ID -> " + id);
%>

<aui:form action="<%= submitURL %>" method="post">
	<aui:fieldset>
	<% if (id != null && !id.equals("")) { %>
		<legend>Editare curs lunar</legend>
	<% } else { %>
		<legend>Adaugare curs lunar</legend>
	<% } %>
		<aui:layout>
		<aui:input type="hidden" id="id" name="id" value="<%=id%>"/>
			<aui:column columnWidth="15" first="true">
				<label style="padding-top:5px" for="year">An : </label>
			</aui:column>
			<aui:column columnWidth="25"  style="text-align:right">
			<% if (id == null || id.equals("")) { %>
				<aui:input label="" name="year" id="year" type="text" value="" style="width:90%">
					 <aui:validator name="required" errorMessage="Campul este obligatoriu." />
				</aui:input>
			<% } else { %>
				<aui:input label="" name="year" id="year" type="text" value="<%=oneExchangeRate.get(0).get(\"year\") %>" style="width:90%">
					 <aui:validator name="required" errorMessage="Campul este obligatoriu." />
				</aui:input>
			<% } %>
			</aui:column>
		</aui:layout>
		<aui:layout>
			<aui:column columnWidth="15" first="true">
				<label style="padding-top:5px" for="month">Luna : </label>
			</aui:column>
			<aui:column columnWidth="25"  style="text-align:right">
			<% if (id == null || id.equals("")) { %>
				<aui:input label="" name="month" id="month" type="text" value="" style="width:90%">
					 <aui:validator name="required" errorMessage="Campul este obligatoriu." />
				</aui:input>
			<% } else { %>
				<aui:input label="" name="month" id="month" type="text" value="<%=oneExchangeRate.get(0).get(\"month\") %>" style="width:90%">
					 <aui:validator name="required" errorMessage="Campul este obligatoriu." />
				</aui:input>
			<% } %>
			</aui:column>
		</aui:layout>
		<aui:layout>
			<aui:column columnWidth="15" first="true">
				<label style="padding-top:5px" for="valoare">Valoare curs : </label>
			</aui:column>
			<aui:column columnWidth="25"  style="text-align:right">
			<% if (id == null || id.equals("")) { %>
				<aui:input label="" name="valoare" id="valoare" type="text" value="" style="width:90%">
					 <aui:validator name="required" errorMessage="Campul este obligatoriu." />
				</aui:input>
			<% } else { %>
				<aui:input label="" name="valoare" id="valoare" type="text" value="<%=oneExchangeRate.get(0).get(\"valoare_curs\") %>" style="width:90%">
					 <aui:validator name="required" errorMessage="Campul este obligatoriu." />
				</aui:input>
			<% } %>
			</aui:column>
		</aui:layout>
		<aui:layout>
			<aui:column last="true">
				<a class="btn btn-primary-red" href="<%= backURL %>">Inapoi</a>
				<aui:button value="Salveaza" cssClass="btn btn-primary" style="margin:0px" type="submit"/>
			</aui:column>
		</aui:layout>
	</aui:fieldset>
</aui:form>