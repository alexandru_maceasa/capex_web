<%@page import="com.profluo.ecm.model.db.DefCursLunarBd"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.List"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme" %>

<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>
<%@ taglib uri="http://alloy.liferay.com/tld/aui" prefix="aui" %>
<%@ taglib uri="http://liferay.com/tld/util" prefix="liferay-util" %>

<liferay-theme:defineObjects />
<portlet:defineObjects />

<liferay-ui:success key="insert_ok" message="Valoare inserata cu success!" />
<liferay-ui:success key="update_ok" message="Modificare efectuata cu success!" />

<liferay-ui:error key="insert_nok" message="A aparut o eroare la inserare. Va rugam reincercati!" />

<portlet:renderURL var="addEntryURL" >
	<portlet:param name="mvcPath" value="/html/definitions/defcurslunar/add_curs_lunar.jsp"></portlet:param>
</portlet:renderURL>

<aui:layout>
	<aui:column columnWidth="100" last="true" style="text-align:right">
		<a class="btn btn-primary" href="<%=addEntryURL.toString()%>">Adauga Curs</a>
	</aui:column>
</aui:layout>
<br/>
<aui:layout>
	<aui:column first="true" columnWidth="100" last="true">
		<div id="def-curs-lunar"></div>
	</aui:column>
</aui:layout>

<%
	List<HashMap<String, Object>> allExchangeRates = DefCursLunarBd.getFullList();
%>

<script type="text/javascript">
YUI().use( 'aui-datatable', 'aui-datatype', 'datatable-sort',
	function(Y) {
		var remoteDataCompany = [
	             		    <% for (int i = 0; i < allExchangeRates.size(); i++) { %>
	                  		{
	                				id: 			<%=allExchangeRates.get(i).get("id")%>, 
	                				year: 			'<%=allExchangeRates.get(i).get("year")%>',
	                				month: 			'<%=allExchangeRates.get(i).get("month")%>',
	                				valoare_curs: 	'<%=allExchangeRates.get(i).get("valoare_curs")%>',
	                				user_id: 		'<%=allExchangeRates.get(i).get("user_id")%>',
	                				sysdate: 		'<%=allExchangeRates.get(i).get("sysdate")%>',
	                				actions: 		'<%=allExchangeRates.get(i).get("id")%>'
	                			}
	                			<% if (i != (allExchangeRates.size() - 1)) { out.print(","); } %>
	                  		<% } %>
	             		];
		var nestedColsCompany = [
		             			{ key: 'id', label: 'ID' },
		             			{ key: 'year', label: 'An'},
		             			{ key: 'month', label: 'Luna'},
		             			{ key: 'valoare_curs', label: 'Valoare curs'},
		             			{ key: 'user_id', className: 'hiddencol'},
		             			{ key: 'sysdate', label: 'Data adaugare'},
		             			{ key: 'actions', label: 'Actiuni', allowHTML: true,
		             				formatter: function(o) {
		             					return '<a id=\"row-' + o.value + '\" class=\"editrow\">Editare</a>';
		             				}
		             			}
		             		];
		
		var dataTable = new Y.DataTable({
		    columns: nestedColsCompany,
		    data: remoteDataCompany,
		    recordType: ['id', 'year', 'month', 'valoare_curs', 'user_id', 'sysdate'],
		    editEvent: 'click'
		}).render("#def-curs-lunar");
		
		dataTable.delegate('click', function (e) {
			 var target = e.target.get('id');
	   		 var idCurs = target.split("-")[1];
	   		 console.log(idCurs);
	   		 $('#<portlet:namespace/>id').val(idCurs);
	   		 $('#<portlet:namespace/>editCurs').submit();
		}, '.editrow', dataTable);
	});	
</script>

<aui:form method="post" action="<%=addEntryURL.toString() %>" name="editCurs" id="editCurs" style="margin:0;padding:0">
	<aui:input type="hidden" id="id" name="id"/>
</aui:form>