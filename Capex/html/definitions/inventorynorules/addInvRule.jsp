<%@page import="com.profluo.ecm.model.vo.DefIASVo"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.List"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme" %>

<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>
<%@ taglib uri="http://alloy.liferay.com/tld/aui" prefix="aui" %>
<%@ taglib uri="http://liferay.com/tld/util" prefix="liferay-util" %>


<liferay-theme:defineObjects />
<portlet:defineObjects />
	
<%
	List<HashMap<String,Object>> allIas = DefIASVo.getInstance();
	//List<HashMap<String,Object>> allStores=DefStoreVo.getInstance();
	String id = renderRequest.getParameter("id");
%>

<%-- Links used in the view --%>
<%-- Submit URL, mapped to the addInvoice method in the class Invoice.java --%>

<portlet:actionURL name="saveRule" var="submitURL" />
<%-- Default portletURL --%>
<portlet:renderURL var="backURL">
	<portlet:param name="mvcPath" value="/html/definitions/inventorynorules/view.jsp"></portlet:param>
</portlet:renderURL>

<aui:form action="<%= submitURL %>" method="post">

<aui:fieldset>
	<% if (id == null) { %>
	<legend>Adaugare regula formare nr. inventar </legend>
	<% } else { %>
	<legend>Editare regula formare nr. inventar</legend>
	<% } %>
	
	<liferay-ui:success key="update_ok" message="update_ok" />
	
	<% if (id != null) { %>
	<aui:layout>
		<aui:column columnWidth="20" first="true">
			<label  for="<portlet:namespace/>id">ID: </label>
		</aui:column>
		<aui:column columnWidth="20" >
			<aui:input label="" name="id" id="id" type="text" value="" readonly="readonly" />
		</aui:column>
	</aui:layout>
	<% } %>
	
	<aui:layout>
		<aui:column columnWidth="20" first="true">
			<label for="<portlet:namespace/>id_ias">Grupa IAS: </label>
		</aui:column>
		<aui:column columnWidth="20">
			<% if (id != null) { %>
				<aui:select id="id_ias" name="id_ias" label="" required="true" disabled="true">
					<aui:option selected="selected" value="0">Denumire grupa</aui:option>
					<% for (int i = 0; i < allIas.size(); i++) { %>
				 	<% if (allIas.get(i).get("status").equals("A") && allIas.get(i).get("grupa_analitica").equals("")) {%>
						<aui:option value="<%=allIas.get(i).get(\"id\")%>"><%=allIas.get(i).get("grupa_sintetica")%> - <%=allIas.get(i).get("name")%></aui:option>
					<% } } %>
				</aui:select>
			<% } else { %>
				<aui:select id="id_ias" name="id_ias" label="" required="true">
					<aui:option selected="selected" value="0">Denumire grupa</aui:option>
					<% for (int i = 0; i < allIas.size(); i++) { %>
				 	<% if (allIas.get(i).get("status").equals("A") && allIas.get(i).get("grupa_analitica").equals("")) {%>
						<aui:option value="<%=allIas.get(i).get(\"id\")%>"><%=allIas.get(i).get("grupa_sintetica")%> - <%=allIas.get(i).get("name")%></aui:option>
					<% } } %>
				</aui:select>
			<% } %>
		</aui:column>
		<aui:column columnWidth="60">
			<label> * grupa IAS folosita pentru ca radacina pentru formarea numarului de inventar</label>
		</aui:column>
	</aui:layout>
	
	<aui:layout>
		<aui:column columnWidth="20" first="true">
			<label for="<portlet:namespace/>grupa_sintetica">Grupa sintetica: </label>
		</aui:column>
		<aui:column columnWidth="20" >
			<aui:input name="grupa_sintetica" id="grupa_sintetica" value="" label="" readonly="readonly">
			</aui:input>
		</aui:column>
		<aui:column columnWidth="60">
			<label> * grupa analitica asociata grupei IAS</label>
		</aui:column>
	</aui:layout>
	
	<aui:layout>
		<aui:column columnWidth="20" first="true">
			<label for="<portlet:namespace/>den_grupa_sintetica">Denumire grupa IAS: </label>
		</aui:column>
		<aui:column columnWidth="20" >
			<aui:input id="den_grupa_sintetica" name="den_grupa_sintetica" label="" readonly="true"></aui:input>
		</aui:column>
		<aui:column columnWidth="60">
			<label> * denumirea grupei IAS</label>
		</aui:column>
	</aui:layout>
	
	
	<aui:layout>
		<aui:column columnWidth="20" first="true">
				<label for="<portlet:namespace/>sqr_group">Radacina grupei: </label>
		</aui:column>
		<aui:column columnWidth="20" >
			<% if (id != null) { %>
				<aui:input type="text" id="sqr_group" name="sqr_group" value="" label="" readonly="readonly"></aui:input>
			<% } else { %>
				<aui:input type="text" id="sqr_group" name="sqr_group" value="" label="">
					<aui:validator name="required" errorMessage="Campul este obligatoriu"></aui:validator>
				</aui:input>
			<% } %>
		</aui:column>
		<aui:column columnWidth="60">
			<label> * radacina din grupa analitica IAS folosita pentru formarea numarului de inventar</label>
		</aui:column>
	</aui:layout>
	
	
	
	<aui:layout>
		<aui:column columnWidth="20" first="true">
			<label for="<portlet:namespace/>store_code_no">Numar cifre din cod magazin: </label>
		</aui:column>
		<aui:column columnWidth="20">
		<% if (id != null){ %>
				<aui:input type="text" id="store_code_no" name="store_code_no" value="" label="" readonly="readonly"></aui:input>
		<% } else { %>
				<aui:input type="text" id="store_code_no" name="store_code_no" value="" label="">
					<aui:validator name="required" errorMessage="Campul este obligatoriu"></aui:validator>
				</aui:input>
		<% } %>
				
		</aui:column>
	</aui:layout>
	
	<aui:layout>
		<aui:column columnWidth="20" first="true">
			<label for="<portlet:namespace/>store_pos">Pozitie prefix magazin:</label>
		</aui:column>
		<aui:column columnWidth="20" >
		<% if (id != null){ %>
			<aui:select id="store_pos" name="store_pos" label="" required="true" disabled="true">
				<aui:option selected="selected" value="F">Primele cifre</aui:option>
				<aui:option selected="selected" value="L">Ultimele cifre</aui:option>
			</aui:select>
			<% } else { %>
			<aui:select type="text" id="store_pos" name="store_pos" label="">
				<aui:option selected="selected" value="F">Primele cifre</aui:option>
				<aui:option selected="selected" value="L">Ultimele cifre</aui:option>
			</aui:select>
			<% } %>
		</aui:column>
	</aui:layout>
		
		<aui:layout>
		<aui:column columnWidth="20" first="true">
			<label  for="<portlet:namespace/>last_inv_no">Secventa cronologica : </label>
		</aui:column>
		<aui:column columnWidth="20" >
		<% if (id !=null) {%>
			<aui:input type="text" id="last_inv_no" name="last_inv_no" value="" label="" readonly="readonly"></aui:input>
		<% } else { %>
		<aui:input type="text" id="last_inv_no" name="last_inv_no" value="" label="">
					<aui:validator name="required" errorMessage="Campul este obligatoriu"></aui:validator>
		</aui:input>
		<% } %>
		</aui:column>
	</aui:layout>
<% if (id != null) {%>		
	<aui:layout>
		<aui:column columnWidth="20" first="true">
			<label  for="<portlet:namespace/>inventory_no">Numar de inventar: </label>
		</aui:column>
		<aui:column columnWidth="20" >
			<aui:input type="text" id="inventory_no" name="inventory_no" value="" label="" readonly="readonly"></aui:input>
	
		</aui:column>
	</aui:layout>
<% } %>		
	<aui:layout>
		<aui:column columnWidth="20" first="true">
			<label for="<portlet:namespace/>status">Status :</label>
		</aui:column>
		<aui:column columnWidth="20" >
			<aui:select type="text" id="status" name="status" value="" label="" required="true">
				<aui:option value="A">Activ</aui:option>
				<aui:option value="I">Inactiv</aui:option>
			</aui:select>
		</aui:column>
	</aui:layout>	

	<aui:layout>		
		<aui:column columnWidth="100" first="true" last="true" style="text-align:right">
			<a class="btn btn-primary-red" href="<%= backURL %>">Inapoi</a>
			<aui:button value="Salveaza" cssClass="btn btn-primary" style="margin:0px" type="submit"/>
		</aui:column>
	</aui:layout>

</aui:fieldset>
</aui:form>