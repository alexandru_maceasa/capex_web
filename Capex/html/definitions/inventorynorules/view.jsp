<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme" %>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>
<%@ taglib uri="http://alloy.liferay.com/tld/aui" prefix="aui" %>
<%@ taglib uri="http://liferay.com/tld/util" prefix="liferay-util" %>

<%@page import="com.profluo.ecm.model.db.InventoryNoRule" %>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.List"%>

<liferay-theme:defineObjects />
<portlet:defineObjects />
	
<%
	// retrieve the rules from the database
	List<HashMap<String,Object>> allRules = InventoryNoRule.GetDistinctRule();
%>

<%-- Links --%>
<portlet:renderURL var="addEntryURL" >
	<portlet:param name="mvcPath" value="/html/definitions/inventorynorules/addInvRule.jsp"></portlet:param>
</portlet:renderURL>

<liferay-ui:success key="insert_ok" message="insert_ok" />
<liferay-ui:success key="update_ok" message="update_ok" />

<aui:layout>
	<aui:column columnWidth="80" last="true" style="text-align:right">
		<a class="btn btn-primary" href="<%=addEntryURL %>">Adauga regula de formare nr inventar </a>
	</aui:column>
</aui:layout>
<br/>
<aui:layout>
	<aui:column first="true" columnWidth="100" last="true">
		<div id="def-inv-no-rule"></div>
	</aui:column>
</aui:layout>

<script type="text/javascript">
YUI({ lang: 'ro' }).use( 'aui-datepicker', 'aui-datatable', 'aui-datatype', 'datatable-sort', 'datasource-get', 'datasource-jsonschema', 'datatable-datasource', 
		'datatable-paginator', 'datatype-number',
		function(Y) {
			function edit(sel_id) {
			    var ml  = dataTable.data, msg = '', template = '';
			    ml.each(function (item, i) {
			        var data = item.getAttrs(['id', 'id_ias', 'grupa_sintetica', 'den_grupa_sintetica', 'sqr_group',
			                                  'store_code_no', 'store_pos', 'last_inv_no', 'status']);
			       		        
		        if (data.id == sel_id) {
					$('#<portlet:namespace />id').val(data.grupa_sintetica);
					//$('#<portlet:namespace />id_ias').val(data.id_ias);
					$('#<portlet:namespace />grupa_sintetica').val(data.grupa_sintetica);
					$('#<portlet:namespace />den_grupa_sintetica').val(data.den_grupa_sintetica);
					$('#<portlet:namespace />sqr_group').val(data.sqr_group);
					$('#<portlet:namespace />store_code_no').val(data.store_code_no);
					$('#<portlet:namespace />store_pos').val(data.store_pos);
					$('#<portlet:namespace />last_inv_no').val(data.last_inv_no);
					$('#<portlet:namespace />status').val(data.status);
										
					$('#<portlet:namespace />editRule').submit();
		        }
		    });
		}
	
		// data array
		var remoteData = [
		    <% for (int i = 0; i < allRules.size(); i++) { %>
     		{
   				id: 				  '<%=allRules.get(i).get("grupa_sintetica")%>', 
<%--    				id_ias: 		   	 '<%=allRules.get(i).get("id_ias").toString().replaceAll("'", "\\\\'")%>', --%>
   				grupa_sintetica:     '<%=allRules.get(i).get("grupa_sintetica").toString().replaceAll("'", "\\\\'")%>',
   				den_grupa_sintetica: '<%=allRules.get(i).get("den_grupa_sintetica")%>',
   				sqr_group : 		 '<%=allRules.get(i).get("sqr_group")%>',
   				store_code_no :		 '<%=allRules.get(i).get("store_code_no")%>',
   				store_pos :			 '<%=allRules.get(i).get("store_pos")%>',
   				last_inv_no:		 '<%=allRules.get(i).get("last_inv_no").toString()%>',
   				status: 			 '<%=allRules.get(i).get("status")%>'
   				
   			}
   			<% if (i != (allRules.size() - 1)) { out.print(","); } %>
     		<% } %> 
		];
		
		// COLUMN INFO and ATTRIBUTES
		var nestedCols = [
			{ key: 'id', label: 'ID', className: "hiddencol" },
// 			{ key: 'id_ias', label:'Id IAS', className: "hiddencol" },
			{ key: 'grupa_sintetica', label:'Grupa sintetica'},
			{ key: 'den_grupa_sintetica', label: 'Denumire grupa IAS'},
			{ key: 'sqr_group', label: 'Radacina grupa IAS'},
			{ key: 'store_code_no', label: 'Nr caractere magazin'},
			{ key: 'store_pos', label: 'Pozitie'},
			{ key: 'last_inv_no', label:'Ultimul nr de inventar'},			
			{ key: 'status', label: 'Status', allowHTML: true, formatter: function(o) { 
				if(o.value == 'A') {return '<font color="green">Activ<font>';} 
				else { return '<font color="red">Inactiv</font>'; } } },
			{ key: 'actiuni', label: 'Actiuni', allowHTML: true, 
				// gets the id of the row and creates an unique link for every row
				formatter: '<a href="#" id="rowrule{id}" class="editrowrule">Editare</a>' }		      
		];

		// TABLE INIT
		var dataTable = new Y.DataTable({
		    columns: nestedCols,
		    data: remoteData,
		    recordType: ['grupa_sintetica','den_grupa_sintetica','sqr_group', 'store_code_no','store_pos','last_inv_no', 'status'],
		    editEvent: 'click'
		}).render('#def-inv-no-rule');
		
		//dataTable.get('boundingBox').unselectable();
		
		// Also define a listener on the single TH checkbox to
		// toggle all of the checkboxes
		dataTable.delegate('click', function (e) {
		    // undefined to trigger the emptyCellValue
		    var target = e.target.get('id');
			edit(target.replace('rowrule',''));
		}, '.editrowrule', dataTable);
	}
);
</script>

<aui:form method="post" action="<%=addEntryURL.toString() %>" name="editRule" id="editRule" style="margin:0;padding:0">
	<aui:input type="hidden" id="id" name="id"/>
	<aui:input type="hidden" id="id_ias" name="id_ias"/>
	<aui:input type="hidden" id="grupa_sintetica" name="grupa_sintetica"/>
	<aui:input type="hidden" id="den_grupa_sintetica" name="den_grupa_sintetica"/>
	<aui:input type="hidden" id="sqr_group" name="sqr_group"/>
	<aui:input type="hidden" id="store_code_no" name="store_code_no"/>
	<aui:input type="hidden" id="store_pos" name="store_pos" />
	<aui:input type="hidden" id="last_inv_no" name="last_inv_no" />
	<aui:input type="hidden" id="status" name="status" />
</aui:form>