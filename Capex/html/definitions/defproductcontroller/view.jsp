<%@page import="com.profluo.ecm.model.vo.DefLotVo"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme" %>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>
<%@ taglib uri="http://alloy.liferay.com/tld/aui" prefix="aui" %>
<%@ taglib uri="http://liferay.com/tld/util" prefix="liferay-util" %>

<%@ page import="com.profluo.ecm.model.db.DefProduct"%>
<%@ page import="java.util.HashMap"%>
<%@ page import="java.util.List"%>


<liferay-theme:defineObjects />
<portlet:defineObjects />

<% //-- PAGINARE 
int start = 0;
int count = 15;
int total = DefProduct.getCount();
String newProductCode = ""; 
try{
	newProductCode = request.getAttribute("newProductCode").toString();	
} catch (Exception e){}

// retrieve products from the database
List<HashMap<String,Object>> allProducts = DefProduct.getAll(start,count);
//retrieve lots
List<HashMap<String,Object>> allLots = DefLotVo.getInstance();
%>
<script type="text/javascript">
var start="<%=start%>";
var count="<%=count%>";
var total="<%=total%>";
//filter
var code="";
var product_name = "";
var filter_lot_id = "";
</script>

<%-- Links --%>
<portlet:resourceURL id="ajaxURL" var="ajaxURL"/>
<portlet:renderURL var="addEntryURL" >
	<portlet:param name="mvcPath" value="/html/definitions/defproductcontroller/add_product.jsp"></portlet:param>
</portlet:renderURL>

<portlet:resourceURL id="exportAction" var="exportURL">
	<portlet:param name="action" value="exportExcel"/>
</portlet:resourceURL>

<liferay-ui:success key="insert_ok" message="insert_ok" />
<liferay-ui:success key="update_ok" message="update_ok" />
<% if(!newProductCode.equals("")) { %>
<div class="div_product_code">
	<p> Codul produsului salvat este : <%=newProductCode%> </p>
</div>
<% } %>
<aui:layout>
	<aui:column columnWidth="100" last="true" style="text-align:right">
		<a class="btn btn-primary" href="<%=exportURL.toString()%>"> Export Excel </a>
		<a class="btn btn-primary" href="<%=addEntryURL.toString()%>">Adauga produs</a>
	</aui:column>
</aui:layout>
<br/>
<aui:layout>
	<aui:column first="true" columnWidth="100" last="true">
		<div id="def-product-groupe"></div>
	</aui:column>
</aui:layout>

<%-- HTML for PAGINATION --%>
<%@ include file="/html/includes/pagination.jsp" %>

<script type="text/javascript">
YUI().use( 'aui-datatable', 'aui-datatype', 'datatable-sort',
	function(Y) {
		function edit(sel_id) {
		    var ml  = dataTableProd.data, msg = '', template = '';
	
		    ml.each(function (item, i) {
		        var data = item.getAttrs(['id', 'code', 'lot_id', 'cat_id', 'name', 
		                                  'flag_it', 'status', 'ias_id', 'ifrs_id', 'ias_code', 'ifrs_code','price_per_unit',
		                                  'created', 'modified', 'lot_name', 'cat_name']);

		        if (data.id == sel_id) {
					$('#<portlet:namespace />id').val(data.id);
					$('#<portlet:namespace />code').val(data.code);
				    $('#<portlet:namespace />lot_id').val(data.lot_id);
				    $('#<portlet:namespace />cat_id').val(data.cat_id);
				    $('#<portlet:namespace />lot_name').val(data.lot_name);
				    $('#<portlet:namespace />cat_name').val(data.cat_name);
					$('#<portlet:namespace />name').val(data.name);
					$('#<portlet:namespace />flag_it').val(data.flag_it);
					$('#<portlet:namespace />status').val(data.status);
					$('#<portlet:namespace />ias_id').val(data.ias_id);
					$('#<portlet:namespace />ifrs_id').val(data.ifrs_id);
					$('#<portlet:namespace />ias_code').val(data.ias_code);
					$('#<portlet:namespace />ifrs_code').val(data.ifrs_code);
					$('#<portlet:namespace />price_per_unit').val(data.price_per_unit);
					$('#<portlet:namespace />modified').val(data.modified);
					$('#<portlet:namespace />created').val(data.created);					
					$('#<portlet:namespace />editProd').submit();
		        }
		    });
		}
	
		// data array
		var remoteDataProd = [
		    <% for (int i = 0; i < allProducts.size(); i++) { %>
     		{
   				id: <%=allProducts.get(i).get("id")%>, 
   				code: '<%=allProducts.get(i).get("code").toString().replaceAll("'", "\\\\'")%>',
   				lot_name:'<%=allProducts.get(i).get("lot_name").toString().replaceAll("'", "\\\\'")%>',
   				cat_name:'<%=allProducts.get(i).get("cat_name").toString().replaceAll("'", "\\\\'")%>',
   				lot_id:'<%=allProducts.get(i).get("lot_id").toString().replaceAll("'", "\\\\'")%>',
   				cat_id:'<%=allProducts.get(i).get("cat_id").toString().replaceAll("'", "\\\\'")%>',
   				name:'<%=allProducts.get(i).get("name").toString().replaceAll("'", "\\\\'")%>',
   				flag_it:'<%=allProducts.get(i).get("flag_it").toString().replaceAll("'", "\\\\'")%>',
   				status:'<%=allProducts.get(i).get("status")%>',
   				ias_id:'<%=allProducts.get(i).get("ias_id")%>',
   				ifrs_id: '<%=allProducts.get(i).get("ifrs_id")%>',
   				ias_dt:'<%=allProducts.get(i).get("ias_dt")%>',
   				ifrs_dt: '<%=allProducts.get(i).get("ifrs_dt")%>',
   				ias_code:'<%=allProducts.get(i).get("ias_code")%>',
   				ifrs_code: '<%=allProducts.get(i).get("ifrs_code")%>',
   				price_per_unit: '<%=allProducts.get(i).get("price_per_unit")%>',
   				modified: '<%=allProducts.get(i).get("modified")%>',
   	   			created: '<%=allProducts.get(i).get("created")%>'
   			}
   			<% if (i != (allProducts.size() - 1)) { out.print(","); } %>
     		<% } %>
		];
		
		function formatCurrency(cell) {
		    format = {
		    	thousandsSeparator: ",",
		        decimalSeparator: ".",
		        decimalPlaces: noOfDecimalsToDisplay
		    };
		    return Y.DataType.Number.format(Number(cell.value), format);
		}
		
		// COLUMN INFO and ATTRIBUTES
		var nestedColsProd = [
			{ key: 'id', label: 'ID', className: 'hiddencol' },
			{ key: 'code', label: '<input id="filter-cod" name="filter-cod" label="" placeholder="Cod produs" style="width:80px;margin:0">' +
			'</input>'},
			{ key: 'name', label: '<input id="filter-name" name="filter-name" label="" placeholder="Nume produs" style="width:130px;margin:0">' +
			'</input>'},
			{ key: 'lot_id', label: 'Lotid', className: 'hiddencol'},
			{ key: 'lot_name',label : '<select id="filter-lot" name="filter-lot" label="" style="width:180px;margin:0">' +
  				'<option selected="selected" value="">Lot</option>' +
  				<% for (int i = 0; i < allLots.size(); i++) { %>
  				<% if (allLots.get(i).get("status").equals("A")) {%>
  				<% out.println("'<option value=\"" + allLots.get(i).get("id") +"\">" + allLots.get(i).get("name") +"</option>' + "); %>
  				<% } } %>
  				'</select>'},
			{ key: 'cat_id', label: 'Catid', className: 'hiddencol'},
			{ key: 'cat_name', label: 'Categorie'},
			{ key: 'flag_it', label: 'Clasificare', formatter: function(o) { 
					if(o.value == 0) {return 'Active';}
					else if(o.value == 1) { return 'IT-Hardware'; }
					else if(o.value == 2) { return 'IT-Software'; }
					else if(o.value == 3) { return 'Securitate'; }
					else if(o.value == 4) { return 'Deco'; }
					else { return ''; }
				}
			},
			{ key: 'ias_id', className: 'hiddencol'},
			{ key: 'ias_code', label: 'IAS'},
			{ key: 'ias_dt', label: 'IAS dt.'},
			{ key: 'ifrs_id', className: 'hiddencol'},
			{ key: 'ifrs_code', label: 'IFRS'},
			{ key: 'ifrs_dt', label: 'IFRS dt.'},
			{ key: 'price_per_unit', label:'Pret unitar', formatter: formatCurrency, className: 'currencyCol'},
			{ key: 'status', label: 'Status', allowHTML: true, formatter: function(o) { 
				if(o.value == 'A') {return '<font color="green">Activ<font>';} 
				else { return '<font color="red">Inactiv</font>'; } }},
			{ key: 'actiuni', label: 'Actiuni', allowHTML: true, 
				// gets the id of the row and creates an unique link for every row
				formatter: '<a href="#" id="rowprod{id}" class="editrowprod">Editare</a>' }		      
		];

		// TABLE INIT
		var dataTableProd = new Y.DataTable({
		    columns: nestedColsProd,
		    data: remoteDataProd,
		    recordType: ['id', 'code', 'lot_id', 'cat_id', 'lot_name', 'cat_name', 'name', 'durata_minima', 
		                 'flag_it', 'status', 'ias_id', 'ifrs_id', 'ias_dt', 'ifrs_dt', 'ias_code', 'ifrs_code','price_per_unit', 'modified', 'created'],
		    editEvent: 'click'
		}).render('#def-product-groupe');
		
		//dataTableProd.get('boundingBox').unselectable();
		
		// Also define a listener on the single TH checkbox to
		// toggle all of the checkboxes
		dataTableProd.delegate('click', function (e) {
			e.preventDefault();
		    // undefined to trigger the emptyCellValue
		    var targetProd = e.target.get('id');
			edit(targetProd.replace('rowprod',''));
		}, '.editrowprod', dataTableProd);
		
		
		//set chosen plugin on lit id filter
		 $('#filter-lot').chosen();
		
		// PAGINATION
		// set status of navigation buttons on page load
		setNavigationButtonsState('<portlet:namespace />');
		$('.navigation').click(function(e) {
			var elementT = e.target;
			var elementId = elementT.id;
			var action = "";
			
			// determine which button was pressed and set the value of start accordingly
			if (elementId.indexOf("first") > -1) { action = "first"; start = 0; }
			if (elementId.indexOf("prev") > -1) { action = "prev"; if (start - count <= 0) {start = 0} else { start -= count; } }
			if (elementId.indexOf("next") > -1) { action = "next"; start = parseInt(start) + parseInt(count); }
			if (elementId.indexOf("last") > -1) { action = "last"; start = parseInt(parseInt(total) / parseInt(count)) * count;}
			
			makeAjaxCall(action);
		});

		// filter code based on change
		$('#filter-cod').change(function(e) {
			var elementT 	= e.target;
			var elementId 	= elementT.id;
			var action 		= "filter";
			// get selected value
			code = elementT.value;
			product_name = $('#filter-name').val();
			filter_lot_id = $('#filter-lot').val();
			// reset start page 
			start = 0;
			// reset count
			total = 0;
			// make ajax call
			makeAjaxCall(action);
		});
		
		// filter name based on change
		$('#filter-name').change(function(e) {
			var elementT 	= e.target;
			var elementId 	= elementT.id;
			var action 		= "filter";
			// get selected value
			product_name = elementT.value;
			code = $('#filter-cod').val();
			filter_lot_id = $('#filter-lot').val();
			// reset start page 
			start = 0;
			// reset count
			total = 0;
			// make ajax call
			makeAjaxCall(action);
		});
		// filtrare moneda
  		$('#filter-lot').change(function(e) {
  			var elementT 	= e.target;
  			var elementId 	= elementT.id;
  			var action 		= "filter";
  			// get selected value
  			filter_lot_id = elementT.value;
  			product_name = $('#filter-name').val();
			code = $('#filter-cod').val();
  			// reset start page 
  			start = 0;
  			// reset count
  			total = 0;
  			// make ajax call
  			makeAjaxCall(action);
  		});
		
		function makeAjaxCall(action) {
			$.ajax({
				type: 	"POST",
				url: 	"<%=ajaxURL%>",
				data: 	"<portlet:namespace />action=" + action + 
						"&<portlet:namespace />start=" + start +
						"&<portlet:namespace />count=" + count +
						"&<portlet:namespace />total=" + total +						
						"&<portlet:namespace />code=" + code + 
						"&<portlet:namespace />prod_name=" + product_name +
						"&<portlet:namespace />filter_lot_id=" + filter_lot_id,
				success: function(msg) {
					// get table data
					if (msg != ""){
						var jsonEntries = jQuery.parseJSON(msg);
						total = jsonEntries.total;
						dataTableProd.set('data', eval(jsonEntries.values));
					} else {
						dataTableProd.set('data', []);
					}
					
					// set status of navigation buttons on page load
					setNavigationButtonsState('<portlet:namespace />');
				}
			});
		}
		// PAGINATION
	}
);
</script>

<aui:form method="post" action="<%=addEntryURL.toString() %>" name="editProd" id="editProd" style="margin:0;padding:0">
	<aui:input type="hidden" id="id" name="id"/>
	<aui:input type="hidden" id="code" name="code"/>
	<aui:input type="hidden" id="lot_id" name="lot_id"/>
	<aui:input type="hidden" id="cat_id" name="cat_id"/>
	<aui:input type="hidden" id="name" name="name"/>
	<aui:input type="hidden" id="flag_it" name="flag_it"/>
	<aui:input type="hidden" id="status" name="status"/>
	<aui:input type="hidden" id="ias_id" name="ias_id"/> 
	<aui:input type="hidden" id="ifrs_id" name="ifrs_id"/>
	<aui:input type="hidden" id="ias_code" name="ias_code"/> 
	<aui:input type="hidden" id="ifrs_code" name="ifrs_code"/>
	<aui:input type="hidden" id="price_per_unit" name="price_per_unit"/>
	<aui:input type="hidden" id="modified" name="modified"/> 	
	<aui:input type="hidden" id="created" name="created"/> 
</aui:form>