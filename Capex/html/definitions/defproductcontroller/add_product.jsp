<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme" %>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>
<%@ taglib uri="http://alloy.liferay.com/tld/aui" prefix="aui" %>
<%@ taglib uri="http://liferay.com/tld/util" prefix="liferay-util" %>

<%@ page import="com.profluo.ecm.model.vo.DefLotVo" %>
<%@ page import="com.profluo.ecm.model.vo.DefCategoryVo" %>
<%@ page import="com.profluo.ecm.model.vo.DefIASVo" %>
<%@ page import="com.profluo.ecm.model.vo.DefIFRSVo" %>
<%@ page import="java.util.HashMap"%>
<%@ page import="java.util.List"%>

<liferay-theme:defineObjects />
<portlet:defineObjects />
	
<%
	String id 		= "";
	String prodCode = "";
	if (renderRequest.getParameter("id") != null){
		id = renderRequest.getParameter("id");
	}
	if (renderRequest.getParameter("code") != null){
		prodCode = renderRequest.getParameter("code");
	}
	//List<HashMap<String,Object>> products 	= DefProductVo.getInstance();
	List<HashMap<String,Object>> categories = DefCategoryVo.getInstance();
	List<HashMap<String,Object>> lots 		= DefLotVo.getInstance();
	List<HashMap<String,Object>> allIas 	= DefIASVo.getInstance();
	List<HashMap<String,Object>> allIfrs 	= DefIFRSVo.getInstance();
%>

<%-- Links used in the view --%>
<%-- Submit URL, mapped to the addInvoice method in the class Invoice.java --%>
<portlet:actionURL name="saveProduct" var="submitURL" />

<portlet:renderURL var="backURL">
	<portlet:param name="mvcPath" value="/html/definitions/defproductcontroller/view.jsp"></portlet:param>
</portlet:renderURL>

<aui:form action="<%= submitURL %>" method="post">

<aui:input type="hidden" name="productCode" id="productCode" value="<%= prodCode %>" />
<aui:fieldset>
	<% if (id == null) { %>
	<legend>Adaugare produs</legend>
	<% } else { %>
	<legend>Editare produs</legend>
	<% } %>
	
	<liferay-ui:success key="update_ok" message="update_ok" />
	<liferay-ui:error key="insert_nok" message="update_nok" />
	<liferay-ui:error key="insert_nok" message="insert_nok" />
	<liferay-ui:error key="params_nok" message="params_nok" />
	<liferay-ui:error key="entry_exists" message="entry_exists" />
	
	<% if (id != null) { %>
	<aui:layout>
		<aui:column columnWidth="25" first="true">
			<label style="padding-top:5px" for="<portlet:namespace/>id">ID: </label>
		</aui:column>
		<aui:column columnWidth="25" style="text-align:right">
			<aui:input label="" name="id" id="id" type="text" value="" style="width:90%" readonly="readonly" />
		</aui:column>
	</aui:layout>
	<% } %>
	
	<aui:layout>
		<aui:column columnWidth="25" first="true">
			<label style="padding-top:5px" for="<portlet:namespace/>name">Nume: </label>
		</aui:column>
		<aui:column columnWidth="25" style="text-align:right">
				<aui:input label="" name="name" id="name" type="text" value="" style="width:90%">
				 <aui:validator name="required" errorMessage="Campul este obligatoriu." />
			</aui:input>
		</aui:column>
	</aui:layout>
	
	<!--  
	<aui:layout>
		<aui:column columnWidth="25" first="true">
			<label style="padding-top:5px" for="<portlet:namespace/>code">Cod: </label>
		</aui:column>
		<aui:column columnWidth="25" style="text-align:right">
			<aui:input label="" name="code" id="code" type="text" value="" style="width:90%">
				 <aui:validator name="required" errorMessage="Campul este obligatoriu." />
			</aui:input>
		</aui:column>
	</aui:layout>
	-->
	<aui:layout>
		<aui:column columnWidth="25" first="true">
			<label style="padding-top:5px" for="<portlet:namespace/>cat_id">Categorie: </label>
		</aui:column>
		<aui:column columnWidth="25" style="text-align:right">
			<aui:select id="cat_id" name="cat_id" type="text" value="" label="" style="width:95%" required="true">
				<aui:option value="">Categorie Produs</aui:option>
				<% for (int i = 0;i < categories.size(); i++) { %>
					<% if (categories.get(i).get("status").equals("A")){ %>
						<aui:option value="<%=categories.get(i).get(\"id\") %>"><%=categories.get(i).get("name")%></aui:option>
					<% } %>
				<% } %>
			</aui:select>
		</aui:column>
	</aui:layout>
	<script type="text/javascript">
		// leave just the stores that belong to selected company in the drowpdown on next row in the form
		$('#<portlet:namespace/>cat_id').change(function(e) {
			var categoriesSize = <%=categories.size()%>;
			var selectedCat = $('#<portlet:namespace/>cat_id').val();
			if (selectedCat != "") {
				$('#<portlet:namespace/>lot_id').removeAttr("disabled");
				
				$('#<portlet:namespace/>lot_id').val('');
				for (var i = 0; i < categoriesSize; i++) {
					if (i == selectedCat) {
						$('.category-options-' + i).show();
					} else {
						$('.category-options-' + i).hide();
					}
				}
				//$('#<portlet:namespace/>lot_id options:contains("category-options-' + selectedCat + '")');
				$('#<portlet:namespace/>lot_id').trigger("chosen:updated");
				//$('#<portlet:namespace/>lot_id').chosen();
			} else {
				console.log("stf");
				$('#<portlet:namespace/>lot_id').val("");
				$('#<portlet:namespace/>lot_id').prop('disabled', true);
				$('#<portlet:namespace/>flag_it').val("0");
				$('#<portlet:namespace/>flag_it').prop('disabled', true);
			}
		});
	</script>
	<aui:layout>
		<aui:column columnWidth="25" first="true">
			<label style="padding-top:5px" for="<portlet:namespace/>lot_id">Lot: </label>
		</aui:column>
		<aui:column columnWidth="25" style="text-align:right">
			<aui:select id="lot_id" name="lot_id" type="text" value="" label="" style="width:95%" required="true" disabled="true"> 
				<aui:option value="">Lot Produs</aui:option>
				<% for (int i = 0; i < lots.size(); i++) { %>
					<% if (lots.get(i).get("status").equals("A")){ %>
						<option value="<%=lots.get(i).get("id")%>" class="category-options-<%=lots.get(i).get("category_id")%>">
							<%=lots.get(i).get("name") %>
						</option>
					<% } %>
				<% } %>
			</aui:select>
		</aui:column>
	</aui:layout>
	<script type="text/javascript">
	
	$('#<portlet:namespace/>price_per_unit').change(function(){
		 $('#<portlet:namespace/>price_per_unit').autoNumeric('update', {aSep: ',', mDec: 4, vMin : -999999999, vMax : 999999999 });
	});
		$(document).ready(function(){
			$('#<portlet:namespace/>price_per_unit').autoNumeric('init', {aSep: ',', mDec: 4, vMin : -999999999, vMax : 999999999 });
			<% if (!id.equals("")) { %>
				$('#<portlet:namespace/>cat_id').trigger('change');
				$('#<portlet:namespace/>lot_id').prop('disabled', false);
				$('#<portlet:namespace/>flag_it').prop('disabled', false);
				$('#<portlet:namespace/>lot_id').val(<%=renderRequest.getParameter("lot_id")%>);
				$('#<portlet:namespace/>flag_it').val(<%=renderRequest.getParameter("flag_it")%>);
				$('#<portlet:namespace/>lot_id').trigger("chosen:updated");
				$('#<portlet:namespace/>flag_it').trigger("chosen:updated");
			<% } %>
		});
		$('#<portlet:namespace/>lot_id').change(function(e) {
			var selectedLot = $('#<portlet:namespace/>lot_id').val();
			if (selectedLot != ""){
				$('#<portlet:namespace/>flag_it').removeAttr("disabled");
				<% for (int i = 0; i < lots.size(); i++){ %>
					if (selectedLot == <%=lots.get(i).get("id")%>){
						$('#<portlet:namespace/>flag_it').val(<%=lots.get(i).get("flag_it")%>);
					}
				<% } %>
			} else {
				$('#<portlet:namespace/>flag_it').val("0");
				$('#<portlet:namespace/>flag_it').prop('disabled', true);
			}
		});
	</script>
	
	<aui:layout>
		<aui:column columnWidth="25" first="true">
			<label style="padding-top:5px" for="<portlet:namespace/>flag_it">Clasificare: </label>
		</aui:column>
		<aui:column columnWidth="25" style="text-align:right">
			<aui:select id="flag_it" name="flag_it" label="" style="width:95%" required="true" disabled="true">
				<aui:option value="0">Active</aui:option>
				<aui:option value="1">IT-Hardware</aui:option>
				<aui:option value="2">IT-Software</aui:option>
				<aui:option value="3">Securitate</aui:option>
				<aui:option value="4">Deco</aui:option>
			</aui:select>
		</aui:column>
	</aui:layout>
	<aui:layout>
		<aui:column columnWidth="25" first="true">
			<label for="<portlet:namespace/><portlet:namespace />status" style="padding-top:5px">Status: </label>
		</aui:column>
		<aui:column columnWidth="25" style="text-align:right">
			<aui:select id="status" name="status" label="" style="width:95%" required="true">
				<aui:option selected="selected" value="">Status</aui:option>
				<aui:option value="A">Activ</aui:option>
				<aui:option value="I">Inactiv</aui:option>
			</aui:select>
		</aui:column>
	</aui:layout>
		
	<aui:layout>
		<aui:column columnWidth="25" first="true">
			<label style="padding-top:5px" for="<portlet:namespace/>ias_id">IAS: </label>
		</aui:column>
		<aui:column columnWidth="25" style="text-align:right">
			<aui:select id="ias_id" name="ias_id" type="text" value="" label="" style="width:95%" required="true">
				<aui:option selected="selected" value="0">Grupa IAS</aui:option>
				<% for (int i = 0;i < allIas.size(); i++) { %>
					<% if (allIas.get(i).get("status").equals("A")){ %>
						<aui:option value="<%=allIas.get(i).get(\"id\") %>">
							<%=allIas.get(i).get("grupa_analitica")%> - <%=allIas.get(i).get("name")%>
						</aui:option>
					<% } %>
				<% } %>
			</aui:select>
		</aui:column>
	</aui:layout>
	
	<aui:layout>
		<aui:column columnWidth="25" first="true">
			<label style="padding-top:5px" for="<portlet:namespace/>ifrs_id">IFRS: </label>
		</aui:column>
		<aui:column columnWidth="25" style="text-align:right">
			<aui:select id="ifrs_id" name="ifrs_id" type="text" value="" label="" style="width:95%" required="true">
				<aui:option selected="selected" value="0">Grupa IFRS</aui:option>
				<% for (int i = 0;i < allIfrs.size(); i++) { %>
					<% if (allIfrs.get(i).get("status").equals("A")){ %>
						<aui:option value="<%=allIfrs.get(i).get(\"id\") %>">
							<%=allIfrs.get(i).get("grupa_analitica")%> - <%=allIfrs.get(i).get("name")%>
						</aui:option>
					<% } %>
				<% } %>
			</aui:select>
		</aui:column>
	</aui:layout>
	
		<aui:layout>
		<aui:column columnWidth="25" first="true">
			<label style="padding-top:5px" for="<portlet:namespace/>price_per_unit">Pret unitar: </label>
		</aui:column>
		<aui:column columnWidth="25" style="text-align:right">
				<aui:input cssClass="currencyCol" label="" name="price_per_unit" id="price_per_unit" type="text" value="" style="width:90%">
				 <aui:validator name="required" errorMessage="Campul este obligatoriu." />
			</aui:input>
		</aui:column>
	</aui:layout>
	
	<% if (id != null) { %>		
	<aui:layout>
		<aui:column columnWidth="25" first="true">
			<label style="padding-top:5px" for="<portlet:namespace/>modified">Modificat la data: </label>
		</aui:column>
		<aui:column columnWidth="25" style="text-align:right">
			<aui:input label="" name="modified" id="modified" type="text" value="" style="width:90%"  readonly="readonly"/>
		</aui:column>
	</aui:layout>
	<aui:layout>
		<aui:column columnWidth="25" first="true">
			<label style="padding-top:5px" for="<portlet:namespace/>created">Adaugat la data: </label>
		</aui:column>
		<aui:column columnWidth="25" style="text-align:right">
			<aui:input label="" name="created" id="created" type="text" value="" style="width:90%"  readonly="readonly"/>
		</aui:column>
	</aui:layout>
	<% } %>
	<aui:layout>
		<aui:column last="true">
			<a class="btn btn-primary-red" href="<%= backURL %>">Inapoi</a>
			<aui:button value="Salveaza" cssClass="btn btn-primary" style="margin:0px" type="submit"/>
		</aui:column>
	</aui:layout>
</aui:fieldset>
</aui:form>
<script type="text/javascript">
	//set chosen plugin on dropdown
	$('#<portlet:namespace/>cat_id').chosen();
	$('#<portlet:namespace/>lot_id').chosen();
	$('#<portlet:namespace/>ias_id').chosen();
	$('#<portlet:namespace/>ifrs_id').chosen();
</script>