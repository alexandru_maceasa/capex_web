<%@page import="com.profluo.ecm.model.vo.DefUMVo"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.List"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>

<portlet:defineObjects />

<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme" %>

<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>
<%@ taglib uri="http://alloy.liferay.com/tld/aui" prefix="aui" %>
<%@ taglib uri="http://liferay.com/tld/util" prefix="liferay-util" %>

<%@ page import="java.util.ResourceBundle" %>
<%@ page import="java.util.Locale" %>

<liferay-theme:defineObjects />
<portlet:defineObjects />
	
<%
	locale = renderRequest.getLocale();
	ResourceBundle resmain = portletConfig.getResourceBundle(locale);
	// retrieve UM from the database
	List<HashMap<String,Object>> allUMs = DefUMVo.getInstance();
%>

<%-- Links --%>
<portlet:renderURL var="addEntryURL" >
	<portlet:param name="mvcPath" value="/html/definitions/defum/addUM.jsp"></portlet:param>
</portlet:renderURL>

<liferay-ui:success key="insert_ok" message="insert_ok" />
<liferay-ui:success key="update_ok" message="update_ok" />

<aui:layout>
	<aui:column columnWidth="100" last="true" style="text-align:right">
		<a class="btn btn-primary" href="<%=addEntryURL.toString()%>">Adauga unitate de masura</a>
	</aui:column>
</aui:layout>
<br/>
<aui:layout>
	<aui:column first="true" columnWidth="100" last="true">
		<div id="def-um"></div>
	</aui:column>
</aui:layout>

<script type="text/javascript">
YUI().use( 'aui-datatable', 'aui-datatype', 'datatable-sort',
	function(Y) {
		function edit(sel_id) {
		    var ml  = dataTableUM.data, msg = '', template = '';
	
		    ml.each(function (item, i) {
		        var data = item.getAttrs(['id', 'name', 'ref', 'status', 'modified', 'created']);

		        if (data.id == sel_id) {
					$('#<portlet:namespace />id').val(data.id);
					$('#<portlet:namespace />name').val(data.name);
					$('#<portlet:namespace />ref').val(data.ref);
					$('#<portlet:namespace />status').val(data.status);
					$('#<portlet:namespace />created').val(data.created);
					$('#<portlet:namespace />modified').val(data.modified);
					$('#<portlet:namespace />editUM').submit();
		        }
		    });
		}
	
		// data array
		var remoteDataUM = [
		    <% for (int i = 0; i < allUMs.size(); i++) { %>
     		{
   				id: <%=allUMs.get(i).get("id")%>, 
   				name: '<%=allUMs.get(i).get("name").toString().replaceAll("'", "\\\\'")%>',
   				ref: '<%=allUMs.get(i).get("ref").toString().replaceAll("'", "\\\\'")%>',
   				status: '<%=allUMs.get(i).get("status")%>',
   				modified: '<%=allUMs.get(i).get("modified")%>',
   				created: '<%=allUMs.get(i).get("created")%>'
   			}
   			<% if (i != (allUMs.size() - 1)) { out.print(","); } %>
     		<% } %>
		];
		
		// COLUMN INFO and ATTRIBUTES
		var nestedColsUM = [
			{ key: 'id', label: 'ID' },
			{ key: 'name', label: 'Nume', formatter: '{value}'},
			{ key: 'ref', label : 'Referinta'},
			{ key: 'status', label: 'Status', allowHTML: true, formatter: function(o) { 
				if(o.value == 'A') {return '<font color="green">Activ<font>';} 
				else { return '<font color="red">Inactiv</font>'; } } },
			{ key: 'actiuni', label: 'Actiuni', allowHTML: true, 
				// gets the id of the row and creates an unique link for every row
				formatter: '<a href="#" id="rowum{id}" class="editrowum">Editare</a>' }		      
		];

		// TABLE INIT
		var dataTableUM = new Y.DataTable({
		    columns: nestedColsUM,
		    data: remoteDataUM,
		    recordType: ['id', 'name', 'ref', 'status', 'modified', 'created'],
		    editEvent: 'click'
		}).render('#def-um');
		
		//dataTableUM.get('boundingBox').unselectable();
		
		// Also define a listener on the single TH checkbox to
		// toggle all of the checkboxes
		dataTableUM.delegate('click', function (e) {
		    // undefined to trigger the emptyCellValue
		    var targetUM = e.target.get('id');
			edit(targetUM.replace('rowum',''));
		}, '.editrowum', dataTableUM);
	}
);
</script>

<aui:form method="post" action="<%=addEntryURL.toString() %>" name="editUM" id="editUM" style="margin:0;padding:0">
	<aui:input type="hidden" id="id" name="id"/>
	<aui:input type="hidden" id="name" name="name"/>
	<aui:input type="hidden" id="ref" name="ref"/>
	<aui:input type="hidden" id="status" name="status"/> 
	<aui:input type="hidden" id="created" name="created"/> 
	<aui:input type="hidden" id="modified" name="modified"/> 
</aui:form>