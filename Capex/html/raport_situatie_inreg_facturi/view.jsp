<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme" %>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>
<%@ taglib uri="http://alloy.liferay.com/tld/aui" prefix="aui" %>
<%@ taglib uri="http://liferay.com/tld/util" prefix="liferay-util" %>

<portlet:defineObjects />

<portlet:resourceURL id="ajaxURL" var="ajaxURL"/>

<portlet:resourceURL id="exportAction" var="exportURL">
	<portlet:param name="action" value="exportExcel"/>
</portlet:resourceURL>

<aui:form id="exportForm" name="exportForm" action="<%=exportURL.toString()%>">
<aui:layout>
	<aui:column columnWidth="10" first="true">
		<label style="padding-top:5px" for="start_date">Data contabila inceput: </label>
	</aui:column>
	<aui:column columnWidth="10"  style="text-align:right">
		<aui:input label="" name="start_date_an" id="start_date_an" type="text" value="" style="width:90%" placeholder="An contabil">
			 <aui:validator name="required" errorMessage="Campul este obligatoriu." />
		</aui:input>
	</aui:column>
	<aui:column columnWidth="10"  style="text-align:right">
		<aui:select id="start_date_luna" name="start_date_luna" label="" style="width:95%" required="true">
				<aui:option selected="selected" value="0">Luna contabil</aui:option>
				<aui:option value="1">Ianuarie</aui:option>
				<aui:option value="2">Februarie</aui:option>
				<aui:option value="3">Martie</aui:option>
				<aui:option value="4">Aprilie</aui:option>
				<aui:option value="5">Mai</aui:option>
				<aui:option value="6">Iunie</aui:option>
				<aui:option value="7">Iulie</aui:option>
				<aui:option value="8">August</aui:option>
				<aui:option value="9">Septembrie</aui:option>
				<aui:option value="10">Octombrie</aui:option>
				<aui:option value="11">Noiembrie</aui:option>
				<aui:option value="12">Decembrie</aui:option>
			</aui:select>
	</aui:column>
</aui:layout>
<aui:layout>
	<aui:column columnWidth="10" first="true">
		<label style="padding-top:5px" for="end_date">Data contabila sfarsit: </label>
	</aui:column>
	<aui:column columnWidth="10"  style="text-align:right">
		<aui:input label="" name="end_date_an" id="end_date_an" type="text" value="" style="width:90%" placeholder="An contabil">
			 <aui:validator name="required" errorMessage="Campul este obligatoriu." />
		</aui:input>
	</aui:column>
	<aui:column columnWidth="10"  style="text-align:right">
		<aui:select id="end_date_luna" name="end_date_luna" label="" style="width:95%" required="true">
				<aui:option selected="selected" value="0">Luna contabil</aui:option>
				<aui:option value="1">Ianuarie</aui:option>
				<aui:option value="2">Februarie</aui:option>
				<aui:option value="3">Martie</aui:option>
				<aui:option value="4">Aprilie</aui:option>
				<aui:option value="5">Mai</aui:option>
				<aui:option value="6">Iunie</aui:option>
				<aui:option value="7">Iulie</aui:option>
				<aui:option value="8">August</aui:option>
				<aui:option value="9">Septembrie</aui:option>
				<aui:option value="10">Octombrie</aui:option>
				<aui:option value="11">Noiembrie</aui:option>
				<aui:option value="12">Decembrie</aui:option>
			</aui:select>
	</aui:column>
</aui:layout>
<aui:layout>
	<aui:column columnWidth="50" first="true" style="text-align:left">
		<a class="btn btn-primary" onclick="exportFacturi('<portlet:namespace />')"> Export Excel </a>
	</aui:column>
</aui:layout>
</aui:form>

<script type="text/javascript">
		function exportFacturi(portletId){
			
			start_date_an = $('#<portlet:namespace />start_date_an').val();
			start_date_luna = $('#<portlet:namespace />start_date_luna').val();
			end_date_an = $('#<portlet:namespace />end_date_an').val();
			end_date_luna = $('#<portlet:namespace />end_date_luna').val();
			
			if ( (2015 > start_date_an) ||(start_date_an > 2020) || (2015 > end_date_an) ||(end_date_an > 2020) ){
				alert("Va rugam sa introduceti un an valid");
				return;
			}
			if ( (1 > start_date_luna) ||(start_date_luna > 12) || (1 > end_date_luna) ||(end_date_luna > 12) ){
				alert("Va rugam sa selectati luna");
				return;
			}
		$('#'+portletId+'exportForm').submit();
		}
	</script>
	


