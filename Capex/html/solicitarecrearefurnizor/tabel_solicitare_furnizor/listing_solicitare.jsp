<%@page import="com.profluo.ecm.model.db.DefSupplier"%>
<%@page import="com.profluo.ecm.flows.SupplierStages"%>
<%@page import="com.profluo.ecm.flows.InvoiceStages"%>
<%@page import="com.profluo.ecm.model.db.InvoiceHeader"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.List"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme"%>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui"%>
<%@ taglib uri="http://alloy.liferay.com/tld/aui" prefix="aui"%>
<%@ taglib uri="http://liferay.com/tld/util" prefix="liferay-util"%>

<portlet:resourceURL id="ajaxURL" var="ajaxURLSupp"/>

<liferay-theme:defineObjects />
<portlet:defineObjects />

<%
	String currentPage 			= "";
	int stage					= 0;
	try{
		currentPage					= renderRequest.getAttribute("page").toString();
		stage					= Integer.parseInt(renderRequest.getAttribute("stage").toString());
	} catch (Exception e){}

	//retrieve all suppliers stage 2
	List<HashMap<String, Object>> allSuppliers = DefSupplier.getAllFiltered(0, 0, "", "", stage, currentPage);
%>

<aui:layout>
	<aui:column first="true" columnWidth="100" last="true">
		<div id="solicitare_creare_furnizor_div"></div>
	</aui:column>
</aui:layout>



<%--fereastra pop-up solicitare inregistrare furnizor --%>
<aui:layout>
	<aui:fieldset>
		<%--@ include file="/html/includes/popups/solicita_inregistrare_furnizor.jsp" --%>
		<jsp:include page="/html/includes/popups/solicita_inregistrare_furnizor.jsp" />
	</aui:fieldset>
</aui:layout>


<script type="text/javascript">
var dataTable;
var ids = "";
YUI().use( 'aui-datatable', 'aui-datatype', 'datatable-sort',"overlay",
		function(Y) {
	
	
		// data array
		var remoteData = [
		 <% if (allSuppliers != null) { %>
			<% for ( int i = 0; i < allSuppliers.size(); i++) { %>
				{
					id: '<%=allSuppliers.get(i).get("id")%>',
					reg_com : '<%=allSuppliers.get(i).get("reg_com")%>',
					id_company : '<%=allSuppliers.get(i).get("id_company")%>',
					RO : '<%=allSuppliers.get(i).get("RO")%>',
					name: '<%=allSuppliers.get(i).get("name")%>',
					cui:'<%=allSuppliers.get(i).get("cui")%>',
					bank:'<%=allSuppliers.get(i).get("bank")%>',
					bank_acc:'<%=allSuppliers.get(i).get("bank_acc")%>',
					swift:'<%=allSuppliers.get(i).get("swift")%>',
					address:'<%=allSuppliers.get(i).get("address")%>',
					email:'<%=allSuppliers.get(i).get("email")%>',
					phone:'<%=allSuppliers.get(i).get("phone")%>',
					payment_code:'<%=allSuppliers.get(i).get("payment_code")%>',
					payment_term:'<%=allSuppliers.get(i).get("payment_term")%>',
					supplier_type:'<%=allSuppliers.get(i).get("supplier_type")%>',
					tert_group:'<%=allSuppliers.get(i).get("tert_group")%>',
					detalii:'',
					istoric_aprobari:''
				}
				<% if ( i != allSuppliers.size() - 1) { out.print(","); } %>
			<% } } %>
		];
		
		/** GENERIC SAVE ON ENTER **/
		function saveOnEnter(e) {
			if (e.domEvent.charCode == 13) {
				if (!e.target.validator.hasErrors()) {
					e.target.fire('save', {
		                newVal: e.target.getValue(),
		                prevVal: e.target.get('value')
		            });
				}
			}
		}

		// COLUMN INFO and ATTRIBUTES
		var nestedCols = [
		    
			{ key: 'id_company', className: 'hiddencol'}, 
		    { key: 'id', className: 'hiddencol'},
		    { key: 'reg_com', className: 'hiddencol'},
		    { key: 'name' ,label: '<input name="furnizor_filtrare" id="furnizor_filtrare" placeholder="Furnizor" style="width:100px;margin-bottom:0px"></input>'},
		    { key: 'RO', label: 'RO'},
		    { key: 'cui', label: 'CUI'},
		    { key: 'bank', label: 'Banca'},
		    { key: 'bank_acc', label: 'Cont bancar'},
		    { key: 'swift', label: 'Cod SWIFT' },
		    { key: 'address',className: 'hiddencol', label: 'Adresa'},
		    { key: 'email', label: 'E-mail'},
		    { key: 'phone', label: 'Telefon'},
		    { key: 'payment_code', label: 'Modalitate de plata'},
		    { key: 'payment_term', label: 'Termen de plata'},
		    { key: 'supplier_type', label: 'Tip furnizor'},
		    { key: 'tert_group', className: 'hiddencol'},
		    { key: 'detalii', className: 'hiddencol', label: 'Detalii'},
		    { key: 'istoric_aprobari', label: 'Istoric aprobari'},
		    { key: 'editare', label:'Actiune', allowHTML: true,
				formatter: function(o) { 
					//console.log('editare: '+ o.data.id);
					return '<a href="#" class="getId" id="row'+ o.data.id +'">Editare</a>';
				},
				emptyCellValue: function(o) { 
					//console.log('empty: '+ o.data.id);
					return '<a href="#" class="getId" id="row'+ o.data.id +'">Editare</a>';
				}
			},
			{ key: 'select', className:'align_center',
				label:'<input type="checkbox" name="header_checkbox" class="select-all align_center" />', allowHTML:  true,
				formatter: function (o) {
		            if (o.value == true) {
		            	return '<input type="checkbox" checked/>';
		            } else {
		            	return '<input type="checkbox" />';
		            }
		        },
                emptyCellValue: '<input type="checkbox" />'}
		];
		
		function redirect(sel_id) {
		    var ml  = dataTable.data, msg = '', template = '';
	
		    ml.each(function (item, i) {
		    	//console.log("inceputul functiei");
		        var data = item.getAttrs(['id', 'id_company', 'reg_com', 'RO', 'name', 'cui', 'bank', 'bank_acc', 'swift',
		                                  'address', 'email', 'phone', 'payment_code', 'payment_term', 'supplier_type','tert_group', 'detalii','istoric_aprobari']);
		        if (data.id == sel_id) {
		        	//clear fields on popup
		        	clearSupplierValues('<portlet:namespace />');
		        	
		        	$('#<portlet:namespace/>new_company').val(data.id_company);
		        	$('#<portlet:namespace/>new_furnizor_id').val(data.id);
		        	$('#<portlet:namespace/>new_furnizor').val(data.name);
					$('#<portlet:namespace />new_cui').val(data.cui);
					$('#<portlet:namespace />new_banca').val(data.bank);
					$('#<portlet:namespace />new_cont_bancar').val(data.bank_acc);
					$('#<portlet:namespace />new_cod_swift').val(data.swift);
					$('#<portlet:namespace />new_email').val(data.email);
					$('#<portlet:namespace />new_telefon').val(data.phone);
					$('#<portlet:namespace />new_banca_beneficiar').val(data.payment_code);
					$('#<portlet:namespace />new_J_field').val(data.reg_com);
					$('#<portlet:namespace />new_address').val(data.address);
					$('#<portlet:namespace />supplier_tert_group_popup').val(data.tert_group);
					$('#<portlet:namespace />new_termen_plata').val(data.payment_term);
					if(data.supplier_type == "Intern"){
						$('#<portlet:namespace />supplier_type_popup').val("0");
					} else if (data.supplier_type == "Extern"){
						$('#<portlet:namespace />supplier_type_popup').val("1");
					} else {
						$('#<portlet:namespace />supplier_type_popup').val("2");
					}
					
					if (data.RO != "") {
						console.log(data.RO);
						$('#<portlet:namespace />new_roCheckbox').prop("checked", true );
					} else { 
						$('#<portlet:namespace />new_roCheckbox').prop("checked", false );
					}
					
					return;
		        }
		    });
		}
	
		
		
		// TABLE INIT
		dataTable = new Y.DataTable({
		    columns: nestedCols,
		    data: remoteData,	
		    editEvent: 'click',
		    recordType: ['id', 'RO', 'id_company', 'reg_com', 'name', 'cui', 'bank', 'bank_acc', 
		                 'swift', 'address', 'email', 'phone', 'payment_code', 'payment_term', 'supplier_type','tert_group', 
		                 'detalii', 'istoric_aprobari']
		}).render('#solicitare_creare_furnizor_div');
		
		//dataTable.get('boundingBox').unselectable();
	
	    // Also define a listener on the single TH "checkbox" to
	    //   toggle all of the checkboxes
	    dataTable.delegate('click', function (e) {
	        // undefined to trigger the emptyCellValue
	        var checked = e.target.get('checked') || undefined;
	        // Set the selected attribute in all records in the ModelList silently
	        // to avoid each update triggering a table update
	        this.data.invoke('set', 'select', checked, { silent: true });
	        // Update the table now that all records have been updated
	        this.syncUI();
	        // workaraound in order to keep the header checkbox state after table reload
	        if (checked) {
	        	dataTable.get('contentBox').one('.select-all').set('checked', true);
	        }
	    }, '.select-all', dataTable);

	    // Define a listener on the DT first column for each record's "checkbox",
	    //   to set the value of `select` to the checkbox setting
	    dataTable.delegate("click", function(e){
	    	
	        // undefined to trigger the emptyCellValue
	        var checked = e.target.get('checked') || undefined;
	        // Don't pass `{silent:true}` if there are other objects in your app
	        // that need to be notified of the checkbox change.
	        this.getRecord(e.target).set('select', checked, { silent: true });
	        // Uncheck the header checkbox
	        this.get('contentBox').one('.select-all').set('checked', false);
	    }, ".table-col-select input", dataTable);
		
	    
	    dataTable.delegate('click', function (e) {
	    	 // undefined to trigger the emptyCellValue
		    var target = e.target.get('id');
			redirect(target.replace('row',''));
	    },".getId");
	    
	    // save edit => update entry and reset table
	    $('.validateSupplier').click(function(e) {
			var action = "editSupplier";
			// error check
			var error = validateSupplierPopUpFromAccounting('<portlet:namespace/>');
			
			// make ajax request if there are no errors
			if (error == 0) {
				makeAjaxCallSupp(action);
			}
	
		});
		
	    //validare grup de furnizori
	    $('.validateAllSuppliers').click(function(e) {
	    	var action = "updateSuppliers";
	    	var ml  = dataTable.data, msg = '', template = '';
	    	var isValid = true;
	    	var hasChecked = false;
		    ml.each(function (item, i) {
		        var data = item.getAttrs(['id', 'select', 'swift', 'payment_code', 'address', 'tert_group']);
		        if (data.select == true) {
		        	hasChecked = true;
		        	if(data.swift == null || data.swift == "" ){
		        		isValid = false;
		        		return false;
		        	}
		        	if(data.payment_code == null || data.payment_code == "" ){
		        		isValid = false;
		        		return false;
		        	}
		        	ids += data.id + ',';
		        	 if(data.address == "" || data.address == null || data.address == 'null') {
				        	isValid = false;
				        	return false;
				        }
				        
				        if(data.tert_group == "" || data.tert_group == null || data.tert_group == 'null') {
				        	isValid = false;
				        	return false;
				        }
		        }
		       
		   	});
		    if (ids != "") {
			    //remove the last comma
			   	ids = ids.substring(0, ids.length - 1);
		    }
		    //alert(ids);
		   	if(!isValid) {
		   		alert("Va rugam completati toate detaliile pentru furnizorii selectati");
		   	} else {
		   		if(!hasChecked) {
		   			alert("Nu ati selectat nici un supplier.");
		   		} else {
		   			makeAjaxCallSupplierGroupValidation(action);
		   		}
		   	}
		    
	    });
	    
	    //validate group of suppliers
	    function makeAjaxCallSupplierGroupValidation(action) {
			$.ajax({
				type: "POST",
				url: "<%=ajaxURLSupp%>",
				data: 	"<portlet:namespace />action=" + action +  
				"&<portlet:namespace />ids=" + ids +
				"&<portlet:namespace />filter_name=" + $('#<portlet:namespace/>furnizor_filtrare').val(),
				success: function(msg) {
  					// get table data
  					if (msg != ""){
  						var jsonEntries = jQuery.parseJSON(msg);
  						total = jsonEntries.total;
  						dataTable.set('data', eval(jsonEntries.values));
  						//reset list of ids that are to be updated to the next stage
  						ids = "";
  					} else {
  						dataTable.set('data', []);
  					}
  					//uncheck the header checkbox
  					$('#<portlet:namespace />header_checkbox').prop("checked", false );
  					
  				}
	  			});
	  		}
	    
	    
		function makeAjaxCallSupp(action) {
			$.ajax({
				type: "POST",
				url: "<%=ajaxURLSupp%>",
				data: 	"<portlet:namespace />action=" + action +  
					"&<portlet:namespace />id=" + $('#<portlet:namespace/>new_furnizor_id').val() + 
					"&<portlet:namespace />name=" + $('#<portlet:namespace/>new_furnizor').val() +
					"&<portlet:namespace />cui=" + $('#<portlet:namespace/>new_cui').val() +
					"&<portlet:namespace />email=" + $('#<portlet:namespace/>new_email').val() +
					"&<portlet:namespace />phone=" + $('#<portlet:namespace/>new_telefon').val() +
					"&<portlet:namespace />reg_com=" + $('#<portlet:namespace/>new_J_field').val() +
					"&<portlet:namespace />payment_code=" + $('#<portlet:namespace/>new_banca_beneficiar').val() +
					"&<portlet:namespace />ro=" + $('#<portlet:namespace/>new_ro').val() +
					"&<portlet:namespace />bank=" + $('#<portlet:namespace/>new_banca').val() +
					"&<portlet:namespace />bank_acc=" + $('#<portlet:namespace/>new_cont_bancar').val() +
					"&<portlet:namespace />swift=" + $('#<portlet:namespace/>new_cod_swift').val() +
					"&<portlet:namespace />id_company=" + $('#<portlet:namespace/>new_company').val() +
					"&<portlet:namespace />payment_term=" + $('#<portlet:namespace/>new_termen_plata').val() +
					"&<portlet:namespace />new_address=" + $('#<portlet:namespace/>new_address').val() +
					"&<portlet:namespace />supplier_tert_group_popup=" + $('#<portlet:namespace/>supplier_tert_group_popup').val() +
					"&<portlet:namespace />supplier_type_db=" + $('#<portlet:namespace/>supplier_type_popup').val() +
					"&<portlet:namespace />filter_name=" + $('#<portlet:namespace/>furnizor_filtrare').val(),
					success: function(msg) {
	  					// get table data
	  					if (msg != ""){
	  						var jsonEntries = jQuery.parseJSON(msg);
	  						total = jsonEntries.total;
	  						dataTable.set('data', eval(jsonEntries.values));
	  					} else {
	  						dataTable.set('data', []);
	  					}
	  					//close popup
	  					$('#overlay').hide();
	  				}
		  			});
		  		}
	    //show popup
	    dataTable.delegate("click", function(e){
	        e.preventDefault();
	        var currentNode = e.target;
			var overlay = new Y.Overlay({
			    srcNode:"#overlay",
			    width:"900",
			    align: {
			        node: currentNode,
			        points:["tr", "tl"]
			    }
			});
			overlay.render();
	        $('#overlay').show();
	    }, ".getId", dataTable);
}
);
	
</script>