<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme"%>

<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui"%>
<%@ taglib uri="http://alloy.liferay.com/tld/aui" prefix="aui"%>
<%@ taglib uri="http://liferay.com/tld/util" prefix="liferay-util"%>
<%@page import="com.profluo.ecm.model.vo.DefCompaniesVo"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.List"%>
<%@ page import="java.util.ResourceBundle"%>
<%@ page import="java.util.Locale"%>

<liferay-theme:defineObjects />
<portlet:defineObjects />

<%
	locale = renderRequest.getLocale();
	ResourceBundle resmain = portletConfig.getResourceBundle(locale);
	
	// retrieve all companies from DB
	List<HashMap<String,Object>> allCompanies = DefCompaniesVo.getInstance();
%>


<aui:layout>
	<aui:column columnWidth="50" last="true" style="text-align:right">
		<%-- //TODO: add it before going live ... functionality also
		<aui:select id="societate" name="societate" label="">
			<aui:option selected="selected" value="0">Societate</aui:option>
			<% for (int i = 0; i < allCompanies.size(); i++) { %>
				<% if (allCompanies.get(i).get("status").equals("A")) {%>
					<aui:option value="<%=allCompanies.get(i).get(\"name\")%>"><%=allCompanies.get(i).get("name")%></aui:option>
				<% } %>
			<% } %>
		</aui:select>
		--%>
	</aui:column>
</aui:layout>
<aui:layout>
		<%@ include file="tabel_solicitare_furnizor/listing_solicitare.jsp" %>
</aui:layout>
<aui:layout>
	<aui:column last="true">
		<aui:button cssClass="btn btn-primary validateAllSuppliers" name="validare_inreg" id="validare_inreg" value="<%=resmain.getString(\"validare_inreg\") %>"></aui:button>
	</aui:column>
</aui:layout>