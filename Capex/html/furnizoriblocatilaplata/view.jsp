<%@page import="java.util.HashMap"%>
<%@page import="java.util.List"%>
<%@page import="com.profluo.ecm.model.db.InvoiceHeader"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme"%>
<%@page import="com.profluo.ecm.model.vo.DefCompaniesVo"%>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui"%>
<%@ taglib uri="http://alloy.liferay.com/tld/aui" prefix="aui"%>
<%@ taglib uri="http://liferay.com/tld/util" prefix="liferay-util"%>

<%@ page import="java.util.ResourceBundle"%>
<%@ page import="java.util.Locale"%>

<liferay-theme:defineObjects />
<portlet:defineObjects />

<portlet:resourceURL id="ajaxURL" var="ajaxURL"/>

<%
	locale = renderRequest.getLocale();
	ResourceBundle resmainProv = portletConfig.getResourceBundle(locale);
	
	//retrieve all inexistent suppliers
	List<HashMap<String, Object>> allBlockedSuppliers = InvoiceHeader.getBlockedSuppliers();
%>


<aui:layout>
	<aui:column first="true" columnWidth="50">
		<div id="furn_blocati_la_plata"></div>
	</aui:column>
	<aui:column last="true" columnWidth="50">
		<%@ include file="/html/includes/associatedInvoices.jsp" %>
	</aui:column>
</aui:layout>


<script type="text/javascript">
var id = 0;
</script>

<script type="text/javascript">
YUI().use( 'aui-datatable', 'aui-datatype', 'datatable-sort',
	function(Y) {
		// data array
		var remoteData = [
		    <% for ( int i = 0; i < allBlockedSuppliers.size(); i++) { %>
			{
				id: '<%=allBlockedSuppliers.get(i).get("id")%>',
				supplier_name:'<%=allBlockedSuppliers.get(i).get("name")%>',
				supplier_code:'<%=allBlockedSuppliers.get(i).get("supplier_code")%>'
			}
			<% if (i != allBlockedSuppliers.size() - 1) {  out.print(",");} %>
			<% } %>
		];

		// COLUMN INFO and ATTRIBUTES
		var nestedCols = [
		    { key: 'id', className: 'hiddencol'},
			{ key: 'supplier_name' , label : 'Furnizor'},
			{ key: 'supplier_code', label : 'Cod furnizor' },
			{ key: 'vizualizeaza', label: 'Vizualizeaza', allowHTML: true, 
				// gets the id of the row and creates an unique link for every row
				formatter: '<a href="#" id="row{id}" class="showInvoices">Lista facturi</a>' }
		];
		// TABLE INIT
		var dataTable = new Y.DataTable({
		    columns: nestedCols,
		    data: remoteData,	
		    editEvent: 'click',
		    recordType: ['id','supplier_name','supplier_code'],
		    plugins: [{
		        cfg: { highlightRange: false },
		        fn: Y.Plugin.DataTableHighlight
		    }]
		}).render('#furn_blocati_la_plata');
		
		//dataTable.get('boundingBox').unselectable();
	
		function showAllInvoices (sel_id){
			var ml  = dataTable.data, msg = '', template = '';
			  	
	  		ml.each(function (item, i) {
	  			var data = item.getAttrs(['id']);					
	  			if (data.id == sel_id) {
	  				id = data.id;
	  				return;
	  			}
	  		});
	  		if ( id != 0 ){
	  			var action = "get";
	  			makeAjaxCall(action);
	  		}
	  	}
		
		dataTable.delegate('click', function (e) {
			e.preventDefault();
	    	 // undefined to trigger the emptyCellValue
		    var target = e.target.get('id');
	    	if (target.replace('row','') == id ){
	    		if ($('#facturi_furnizor:visible').length != 0 ) {
	    			$('#facturi_furnizor').hide();
	    		} else {
	    			$('#facturi_furnizor').show();
	    		}
	    	} else {
				showAllInvoices(target.replace('row',''));
	    	}
	    }, '.showInvoices', dataTable);
		 
		function makeAjaxCall(action) {
	  			$.ajax({
	  				type: "POST",
	  				url: "<%=ajaxURL%>",
	  				data: 	"<portlet:namespace />id=" + id +
	  						"&<portlet:namespace />action=" + action,
	  				success: function(msg) {
	  					// get table data
	  					if (msg != ""){
	  						var jsonEntries = jQuery.parseJSON(msg);
	  						dataTableInvoices.set('data', eval(jsonEntries.values));
	  					} else {
	  						dataTableInvoices.set('data', []);
	  					}
	  					//this field is in the imported file associatedInvoices.jsp
	  					$('#facturi_furnizor').show();
	  				}
	  			});
	  	}
}
);
</script>