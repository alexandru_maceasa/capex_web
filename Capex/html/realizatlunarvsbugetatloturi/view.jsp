<%@page import="com.profluo.ecm.model.db.ListaInit"%>
<%@page import="com.profluo.ecm.model.db.DefStore"%>
<%@page import="com.profluo.ecm.model.db.DefCompanies"%>
<%@page import="com.profluo.ec.carrefour.htmlutils.TabelUtils"%>
<%@page import="com.profluo.ecm.model.db.Raports"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme"%>

<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui"%>
<%@ taglib uri="http://alloy.liferay.com/tld/aui" prefix="aui"%>
<%@ taglib uri="http://liferay.com/tld/util" prefix="liferay-util"%>

<%@page import="java.util.HashMap"%>
<%@page import="java.util.List"%>


<liferay-theme:defineObjects />
<portlet:defineObjects />
<style>
.tip_capex{
	background-color: rgb(31, 78, 150);
	color : white;
}
.companie{
	background-color: #E3E3E3;
}
.total, th {
	background-color: rgb(32, 55, 100);
	color : white;
}
td {
	border: 1px solid black;
}
.inlineTableEmptycolLoturi {
    display: inline-block;
    width: 20%!important;
}
</style>

<portlet:resourceURL id="ajaxURL" var="ajaxURL"/>

<portlet:resourceURL id="exportAction" var="exportURL">
	<portlet:param name="action" value="exportExcel"/>
</portlet:resourceURL>

<portlet:actionURL name="updateInfo" var="submitURL" />
<portlet:actionURL name="filter" var="filterURL" />

<%
	List<HashMap<String,Object>> list = Raports.getRaport(0, 500000, 3);
	list.addAll(Raports.getTableFooter(true, 3));
	List<HashMap<String,Object>> listTotal = Raports.getSecondRaport();
	listTotal.addAll(Raports.getTableFooter(false, 3));
	
	List<HashMap<String,Object>> allCompanies = DefCompanies.getAll();
	List<HashMap<String,Object>> allStores 	  = DefStore.getFullList();
	List<HashMap<String,Object>> allInit 	  = ListaInit.getAll();
%>
<aui:form id="filtreForm" name="filtreForm" action="<%=filterURL%>">
	<aui:layout>
		<aui:column columnWidth="15" first="true">
			<label for="<portlet:namespace />societate" style="padding-top: 5px">Societatea</label>
		</aui:column>
		<aui:column>
			<aui:select id="societate" name="societate" label="" style="width:200px;">
				<aui:option selected="selected" value=""> ----------- </aui:option>
				<% for (int i = 0 ; i < allCompanies.size(); i++) { %>
					<aui:option value="<%=i+1%>"> <%=allCompanies.get(i).get("name").toString()%> </aui:option>
				<% } %>
			</aui:select>
		</aui:column>
	</aui:layout>
	<aui:layout>
		<aui:column columnWidth="15" first="true">
			<label for="<portlet:namespace />store" style="padding-top: 5px;">Magazin</label>
		</aui:column>
		<aui:column>
			<aui:select id="store" name="store" label="" style="width:200px;">
				<aui:option selected="selected" value=""> ----------- </aui:option>
				<% for (int i = 0 ; i < allStores.size(); i++) { %>
					<aui:option value="<%=allStores.get(i).get(\"id\").toString()%>"> <%=allStores.get(i).get("store_id").toString()%> - <%=allStores.get(i).get("store_name").toString()%> </aui:option>
				<% } %>
			</aui:select>
		</aui:column>
	</aui:layout>
	<aui:layout>
		<aui:column columnWidth="15" first="true">
			<label for="<portlet:namespace />year" style="padding-top: 5px;">An</label>
		</aui:column>
		<aui:column>
			<aui:input type="text" value="" label="" id="year" name="year" />
		</aui:column>
	</aui:layout>
	<aui:layout>
		<aui:column columnWidth="15" first="true">
			<label for="<portlet:namespace />initiativa" style="padding-top: 5px;">Initiativa</label>
		</aui:column>
		<aui:column>
			<aui:select id="initiativa" name="initiativa" label="" style="width:200px;">
				<aui:option selected="selected" value=""> ----------- </aui:option>
				<aui:option value="0"> CAPEX Diverse </aui:option>
				<aui:option value="1"> Initiativa Centralizata </aui:option>
				<aui:option value="2"> Proiecte noi </aui:option>
			</aui:select>
		</aui:column>
	</aui:layout>
</aui:form>
<aui:layout>
	<a class="btn btn-primary" onclick="exportRaport('<portlet:namespace />')"> Export Excel </a>
	<a class="btn btn-primary" onclick="actualizeaza()"> Actualizeaza informatii </a>
	<a class="btn btn-primary" onclick="doFilters()"> Aplica filtre </a> 
	<aui:form id="exportForm" name="exportForm" action="<%=exportURL.toString()%>"> </aui:form>
	<aui:form id="actualizareForm" name="actualizareForm" action="<%=submitURL%>"> </aui:form>
</aui:layout>

<aui:layout>
	<div style="overflow-x:auto!important; white-space: nowrap;">
				<table class="inlineTable">
					<tr>
						<th>Cod</th><th>Denumire</th><th>Buget an</th><th>Actual Ian</th><th>Actual Feb</th><th>Actual Mar</th><th>Actual Apr</th>
						<th>Actual Mai</th><th>Actual Iun</th><th>Actual Iul</th><th>Actual Aug</th><th>Actual Sept</th><th>Actual Oct</th>
						<th>Actual Noi</th><th>Actual Dec</th><th>Total</th><th>Actual vs Bugetat</th>
					</tr>
				<% for (int i = 0; i < list.size(); i++) {%>
				<% if (list.get(i).get("code").equals("1") || list.get(i).get("code").equals("2") || list.get(i).get("code").equals("3"))  { %>
					<tr class='tip_capex'>
						<%=TabelUtils.getTableTdS(list, i) %>
					</tr>
				 <% } else if (list.get(i).get("code").toString().startsWith("D")) { %>
					<tr class='companie'>
						<%=TabelUtils.getTableTdS(list, i) %>
					</tr>
				<% } else if (list.get(i).get("code").toString().equals("Total")) { %>
					<tr class='total'>
						<%=TabelUtils.getTableTdS(list, i) %>
					</tr>
				<% } else { %>
					<tr>
						<%=TabelUtils.getTableTdS(list, i) %>
					</tr>
				<% } } %>
				
				</table>
	</div>
</aui:layout>

<br/><br/>

<aui:layout>
	<div style="overflow-x:auto!important; white-space: nowrap;">
				<table class="inlineTable">
					<tr>
						<th>Denumire</th><th>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp</th><th>Buget an</th><th>Actual Ian</th><th>Actual Feb</th><th>Actual Mar</th><th>Actual Apr</th>
						<th>Actual Mai</th><th>Actual Iun</th><th>Actual Iul</th><th>Actual Aug</th><th>Actual Sept</th><th>Actual Oct</th>
						<th>Actual Noi</th><th>Actual Dec</th><th>Total</th><th>Diferenta</th>
					</tr>
				<% for (int i = 0; i < listTotal.size(); i++) {%>
					<tr>
						<%=TabelUtils.getTotalTableTdS(listTotal, i) %>
					</tr>
				<% }  %>
				</table>
	</div>
</aui:layout>


<script type="text/javascript">
$(document).ready(function(e){
	$('#<portlet:namespace/>store').chosen();
	$('#<portlet:namespace/>societate').chosen();
});
function actualizeaza(){
	$('#<portlet:namespace/>actualizareForm').submit();
}
function exportRaport(portletId){
	$('#'+portletId+'exportForm').submit();
}
function doFilters(portletId){
	
}
</script>
