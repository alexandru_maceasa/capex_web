<%@page import="com.profluo.ecm.model.db.DefSupplier"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>

<%@page import="java.util.HashMap"%>
<%@page import="java.util.List"%>

<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme"%>

<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui"%>
<%@ taglib uri="http://alloy.liferay.com/tld/aui" prefix="aui"%>
<%@ taglib uri="http://liferay.com/tld/util" prefix="liferay-util"%>

<liferay-theme:defineObjects />
<portlet:defineObjects />

<portlet:resourceURL id="ajaxURL" var="ajaxURL"/>

<%
	String supplierName = "";
	// retrieve all blocked suppliers
	List<HashMap<String,Object>> allSuppliers = DefSupplier.getBlockedSuppliersFiltered(supplierName);
%>

<aui:layout>
	<aui:column first="true" columnWidth="100">
		<div id="deblocare_furnizori"></div>
	</aui:column>
	
	<aui:column last="true" columnWidth="15">
		<aui:button id="deblocheaza_furnizor" type="button" value="Deblocheaza furnizor"/>
	</aui:column>
</aui:layout>
<script type="text/javascript">

YUI().use( 'aui-datatable', 'aui-datatype', 'datatable-sort',
		function(Y) {
		
			var remoteData = [
							<%
								if (allSuppliers != null) {
									for (int i = 0; i < allSuppliers.size(); i++) {
							%>
									{
							  			id: 			<%=allSuppliers.get(i).get("id")%>,
							  			name: 	'<%=allSuppliers.get(i).get("name")%>',
							  			cui: 		'<%=allSuppliers.get(i).get("cui")%>',
							  			RO: 	'<%=allSuppliers.get(i).get("RO")%>',
							  			reg_com: 	'<%=allSuppliers.get(i).get("reg_com")%>',
							  			bank: 		'<%=allSuppliers.get(i).get("bank")%>',
							  			bank_acc: 		'<%=allSuppliers.get(i).get("bank_acc")%>', 
							  			swift: '<%=allSuppliers.get(i).get("swift")%>',
							  			adresa: 		'Adresa',
							  			email: 	'<%=allSuppliers.get(i).get("email")%>',
							  			phone: 		'<%=allSuppliers.get(i).get("phone")%>',
							  			payment_code: 		'<%=allSuppliers.get(i).get("payment_code")%>',
							  			payment_term: 		<%=allSuppliers.get(i).get("payment_term")%>,
							  			<% String status = allSuppliers.get(i).get("blocked_stage").toString(); 
							  			if ( status.equals("1")) { %>
											detalii : "Factura"
										<% } else if ( status.equals("2")) { %>
											detalii : "Plata avans"
										<% } else { %>
											detalii : "Deconturi"
										<% } %>
							  		}
							
							<% if (i != (allSuppliers.size() - 1)) { out.print(","); } %>
					  		<% } } %>
					  		];
	
			var nestedCols = [
			                 { key: 'id', className: 'hiddencol'},
			                 { key: 'name', label : '<input style="width:100px;margin-bottom:0px" class="field" type="text" id="filtru_furnizor" placeholder="Furnizor"></input>'},
			                 { key: 'cui', label : 'CUI'},
			                 { key: 'RO', label : 'RO'},
			                 { key: 'reg_com', label : 'J'},
			                 { key: 'bank', label : 'Banca'},
			                 { key: 'bank_acc', label : 'Cont bancar'},
			                 { key: 'swift', label : 'Cod SWIFT'},
			                 { key: 'adresa', label : 'Adresa'},
			                 { key: 'email', label : 'E-mail'},
			                 { key: 'phone', label : 'Telefon'},
			                 { key: 'payment_code', label : 'Banca beneficiara'},
			                 { key: 'payment_term', label : 'Termen de plata'},
			                 { key: 'detalii', label : 'Detalii'},
			                 { key: 'select', className:'align_center',
			     				label:'<input type="checkbox" class="select-all align_center" />', allowHTML:  true,
			     				formatter: function (o) {
			     		            if (o.value == true) {
			     		            	return '<input type="checkbox" checked/>';
			     		            } else {
			     		            	return '<input type="checkbox" />';
			     		            }
			     		        },
			                     emptyCellValue: '<input type="checkbox" />'}]
			// TABLE INIT
			var dataTable = new Y.DataTable({
			    columns: nestedCols,
			    data: remoteData,	
			    editEvent: 'click',
			    recordType: ['id' ,'supplier_name', 'ro', 'jCode', 'banca', 'cont_bancar', 'cod_swift','adresa','mail', 'phone', 
			                 'banca_beneficiara','termen', 'detalii', 'actiuni' ]
			}).render('#deblocare_furnizori');
			
			 dataTable.delegate('click', function (e) {
			        // undefined to trigger the emptyCellValue
			        var checked = e.target.get('checked') || undefined;
			        // Set the selected attribute in all records in the ModelList silently
			        // to avoid each update triggering a table update
			        this.data.invoke('set', 'select', checked, { silent: true });
			        // Update the table now that all records have been updated
			        this.syncUI();
			        // workaraound in order to keep the header checkbox state after table reload
			        if (checked) {
			        	dataTable.get('contentBox').one('.select-all').set('checked', true);
			        }
			    }, '.select-all', dataTable);
			
			 dataTable.delegate("click", function(e) {
			        var checked = e.target.get('checked') || undefined;
			        this.getRecord(e.target).set('select', checked, { silent: true });
			    }, ".table-col-select input", dataTable);
			
			//Click on 'deblocheaza furnizor'
			$('#deblocheaza_furnizor').click(function(){
				 var ml  = dataTable.data, msg = '', template = '';
				 var IDs = "";
				 ml.each(function (item, i) {
					 var data = item.getAttrs(['id', 'select' ]);
					 //getting all selected ids
					 if(data.select == true){
						 IDs += data.id + ",";
					 }
				 });
				 if ( IDs != ""){
					//remove the last ","
					IDs = IDs.substring(0, IDs.length - 1);
					$('#<portlet:namespace />idToUpdate').val(IDs);
					$('#<portlet:namespace />toUpdate').submit();
				} else {
					alert("Pentru a efectua aceasta actiune trebuie sa selectati una sau mai multe intrari din lista.");
					return false;
				} 
			});
			
			//Supplier filter
			$('#filtru_furnizor').change(function(e){
				var action 		 = "filter";
				var supplierName = e.target.value;
				$.ajax({
					type: "POST",
					url: "<%=ajaxURL%>",
					data: 	"<portlet:namespace />action=" + action + 
							"&<portlet:namespace />supplierName=" + supplierName,
					success: function(msg) {
						// get table data
						if (msg != ""){
							var jsonEntries = jQuery.parseJSON(msg);
							dataTable.set('data', eval(jsonEntries.values));
						} else {
							dataTable.set('data', []);
						}
					}
				});
			});
			
			
});
</script>
<portlet:actionURL name="updateStage" var="updateStageURL" />

<aui:form action="<%=updateStageURL%>" name="toUpdate" id="toUpdate" method="post">
	<aui:input type="hidden" id="idToUpdate" name="idToUpdate"/>
</aui:form>	