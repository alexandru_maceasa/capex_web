<%@page import="com.profluo.ecm.model.db.RaportSolicitariAvansBd"%>
<%@page import="com.profluo.ecm.model.db.DefStore"%>
<%@page import="com.profluo.ec.carrefour.htmlutils.DropDownUtils"%>
<%@page import="com.profluo.ecm.model.db.ListaInit"%>
<%@page import="com.profluo.ecm.model.db.DefNewProj"%>
<%@page import="com.profluo.ecm.model.db.InvoiceHeader"%>
<%@page import="com.profluo.ecm.controller.UserTeamIdUtils"%>
<%@page import="com.profluo.ecm.model.db.DefCompanies"%>
<%@page import="com.profluo.ecm.model.db.InventoryHeader"%>
<%@taglib uri="http://java.sun.com/portlet_2_0" 	prefix="portlet"%>
<%@taglib uri="http://liferay.com/tld/theme" 		prefix="liferay-theme"%>
<%@taglib uri="http://liferay.com/tld/ui" 			prefix="liferay-ui"%>
<%@taglib uri="http://alloy.liferay.com/tld/aui" 	prefix="aui"%>
<%@taglib uri="http://liferay.com/tld/util" 		prefix="liferay-util"%>

<%@page import="java.util.HashMap"%>
<%@page import="java.util.List"%>

<liferay-theme:defineObjects />
<portlet:defineObjects />

<%
	List<HashMap<String,Object>> allCompanies = DefCompanies.getAll();
	List<HashMap<String,Object>> allStores = DefStore.getFullList();
%>

<portlet:resourceURL id="ajaxURL" var="ajaxURL"/>

<portlet:resourceURL id="exportAction" var="exportURL">
	<portlet:param name="action" value="exportExcel"/>
</portlet:resourceURL>

<portlet:resourceURL id="exportRaport" var="exportRaportURL">
	<portlet:param name="action" value="exportRaport"/>
</portlet:resourceURL>

<aui:layout>
	<aui:form id="exportCeva" name="exportCeva" action="">
	<aui:layout>
		<aui:column columnWidth="15">
			<label for="<portlet:namespace />societate" style="padding-top: 5px">Societatea</label>
		</aui:column>
		<aui:column>
			<aui:select id="societate" name="societate" label="">
				<aui:option selected="selected" value="0"> ----------- </aui:option>
				<% for (int i = 0 ; i < allCompanies.size(); i++) { %>
					<aui:option value="<%=i+1%>"> <%=allCompanies.get(i).get("name").toString()%> </aui:option>
				<% } %>
			</aui:select>
		</aui:column>
	</aui:layout>
	<aui:layout>
		<aui:column columnWidth="15">
			<label for="<portlet:namespace />store" style="padding-top: 5px;">Magazin</label>
		</aui:column>
		<aui:column>
			<aui:select id="store" name="store" label="">
				<aui:option selected="selected" value="0"> Magazin </aui:option>
				<% for (int i = 0 ; i < allStores.size(); i++) { %>
					<aui:option value="<%=allStores.get(i).get(\"id\").toString()%>"> <%=allStores.get(i).get("store_id").toString()%> - <%=allStores.get(i).get("store_name").toString()%> </aui:option>
				<% } %>
			</aui:select>
		</aui:column>
	</aui:layout>
<script type="text/javascript">
YUI().use('autocomplete-list', "datasource-io", 'aui-base', 'aui-io-request', 
		  'aui-form-validator', 'autocomplete-filters', 'autocomplete-highlighters', 
		  'overlay',
function (A) {
	var contactSearchDS = new A.DataSource.IO({source: '<%=ajaxURL.toString()%>'});
	var supplierData = "";
	var supplierCreated = false;
	
	var contactSearchFormatter = function (query, results) {
		return A.Array.map(results, function (result) {
			return '<strong>'+result.raw.name+'</strong><br/><b>' + result.raw.supplier_code + '</b> - CUI: ' + result.raw.cui;
		});
	};

	var testData;
	var contactSearchInput = new A.AutoCompleteList({
		allowBrowserAutocomplete: 'false',
		resultHighlighter: 'phraseMatch',
		activateFirstItem: 'true',
		resultTextLocator: 'name',
		inputNode: '#<portlet:namespace/>supplier_list',
		render: 'true',
		width: '300px',
        resultFormatter: contactSearchFormatter,
		source: function(){
			
			$('#<portlet:namespace/>id_supplier_raport').val("");
			supplierData = "";
			var inputValue = A.one("#<portlet:namespace />supplier_list").get('value');
			var myAjaxRequest = A.io.request('<%=ajaxURL.toString()%>',{
					dataType: 'json',
					method:'POST',
					data:{
						 <portlet:namespace/>action: 'findSupplier',
						 <portlet:namespace/>keywords: $('#<portlet:namespace />supplier_list').val(),
						 <portlet:namespace/>id_company: $('#<portlet:namespace />societate').val()
						},
					autoLoad: false,
					sync: true,
					on: {
					success:function(){
						var data = this.get('responseData');
						testData = data;
					}}
			});
			myAjaxRequest.start();
			return testData;
		},	
			on: {
			select: function(event) {
				// after user selects an option from the list
				var result = event.result.raw;
				// global variable to be used also in the create new supplier pop-up
				supplierData = result;
				A.one('#<portlet:namespace/>id_supplier_raport').val(result.id);
			}
		}
	});	
});
</script>
<aui:layout>
	<aui:column columnWidth="15" first="true">
		<label for="<portlet:namespace />supplier_list" style="padding-top: 5px;">Furnizor</label>
	</aui:column>
	<aui:column>
		<aui:input id="supplier_list" name="supplier_list" disabled="true" label="" style="width:200px;"></aui:input>
		<aui:input id="id_supplier_raport" name="id_supplier_raport" value="" type="hidden" label=""/>
	</aui:column>
</aui:layout>
	
	<aui:layout>
	<aui:column columnWidth="15" first="true">
		<label style="padding-top:5px" for="start_date">Data contabila inceput: </label>
	</aui:column>
	<aui:column columnWidth="10"  style="text-align:right">
		<aui:input label="" name="start_date_an" id="start_date_an" type="text" value="" style="width:90%" placeholder="An contabil">
		</aui:input>
	</aui:column>
	<aui:column columnWidth="15"  style="text-align:right">
		<aui:select id="start_date_luna" name="start_date_luna" label="" style="width:100%" required="true">
				<aui:option selected="selected" value="0">Luna contabil</aui:option>
				<aui:option value="1">Ianuarie</aui:option>
				<aui:option value="2">Februarie</aui:option>
				<aui:option value="3">Martie</aui:option>
				<aui:option value="4">Aprilie</aui:option>
				<aui:option value="5">Mai</aui:option>
				<aui:option value="6">Iunie</aui:option>
				<aui:option value="7">Iulie</aui:option>
				<aui:option value="8">August</aui:option>
				<aui:option value="9">Septembrie</aui:option>
				<aui:option value="10">Octombrie</aui:option>
				<aui:option value="11">Noiembrie</aui:option>
				<aui:option value="12">Decembrie</aui:option>
			</aui:select>
	</aui:column>
</aui:layout>
<aui:layout>
	<aui:column columnWidth="15" first="true">
		<label style="padding-top:5px" for="end_date">Data contabila sfarsit: </label>
	</aui:column>
	<aui:column columnWidth="10"  style="text-align:right">
		<aui:input label="" name="end_date_an" id="end_date_an" type="text" value="" style="width:90%" placeholder="An contabil">
		</aui:input>
	</aui:column>
	<aui:column columnWidth="15"  style="text-align:right">
		<aui:select id="end_date_luna" name="end_date_luna" label="" style="width:100%" required="true">
				<aui:option selected="selected" value="0">Luna contabil</aui:option>
				<aui:option value="1">Ianuarie</aui:option>
				<aui:option value="2">Februarie</aui:option>
				<aui:option value="3">Martie</aui:option>
				<aui:option value="4">Aprilie</aui:option>
				<aui:option value="5">Mai</aui:option>
				<aui:option value="6">Iunie</aui:option>
				<aui:option value="7">Iulie</aui:option>
				<aui:option value="8">August</aui:option>
				<aui:option value="9">Septembrie</aui:option>
				<aui:option value="10">Octombrie</aui:option>
				<aui:option value="11">Noiembrie</aui:option>
				<aui:option value="12">Decembrie</aui:option>
			</aui:select>
	</aui:column>
</aui:layout>
	<aui:column columnWidth="100">
		<aui:column>
			<a class="btn btn-primary" onclick="filterListing('<portlet:namespace />')"> Aplica Filtre </a>
		</aui:column>
		<aui:column>
			<a class="btn btn-primary" onclick="exportRaport('<portlet:namespace />')"> Export Raport </a>
		</aui:column>
	</aui:column>
	</aui:form>
</aui:layout>
<br/>
<aui:layout>
	<aui:column first="true" columnWidth="100" last="true">
		<div id="inventar"></div>
	</aui:column>
</aui:layout>

<%
	int start = 0; 
	int count = 10;
	List<HashMap<String,Object>> allInvLines = RaportSolicitariAvansBd.getAllInfo(0, 10, "", "", 0, 0, 0, 0,0);
	int total = RaportSolicitariAvansBd.getAllInfoCount("", "", 0, 0, 0, 0, 0);
%>

<script type="text/javascript">
var start = <%=start%>
var count = <%=count%>
var total = <%=total%>
var societate = -1;
var magazin = 0;
var supplier = 0;
var dataTableIV;
var from_year = 0;
var to_year = 0;
var from_month = 0;
var to_month = 0;
</script>

<%-- HTML for PAGINATION --%>
<%@ include file="/html/includes/pagination.jsp" %>

<script type="text/javascript">

$(document).ready(function(e){
	$('#<portlet:namespace/>store').chosen();
});

$('#<portlet:namespace/>societate').change(function(e){
	if($('#<portlet:namespace/>societate').val() != '' && $('#<portlet:namespace/>societate').val() != 0 ) {
		$('#<portlet:namespace/>supplier_list').prop("disabled", false);
	} else {
		$('#<portlet:namespace/>supplier_list').prop("disabled", true);
	}
});

$('#<portlet:namespace/>supplier_list').click(function(e) {
	var selectedCo = $('#<portlet:namespace/>societate').val();
	if (selectedCo == '0'){
		alert("Selectati societatea!");
		return;
	}
});

function filterListing(portletId){
	societate = $('#<portlet:namespace/>societate').val();
	magazin = $('#<portlet:namespace/>store').val();
	supplier = $('#<portlet:namespace/>id_supplier_raport').val();
	from_year = $('#<portlet:namespace/>start_date_an').val();
	to_year = $('#<portlet:namespace/>end_date_an').val();
	from_month = $('#<portlet:namespace/>start_date_luna').val();
	to_month = $('#<portlet:namespace/>end_date_luna').val();
	if(from_year == '') {
		from_year = 0;
	}
	if(to_year == '') {
		to_year = 0;
	}
	if(from_month == '') {
		from_month = 0;
	}
	if(to_month == '') {
		to_month = 0;
	}
	var action = "filter";
	makeAjaxCall(action);
}

function exportRaport(portletId){
	societate = $('#<portlet:namespace/>societate').val();
	magazin = $('#<portlet:namespace/>store').val();
	supplier = $('#<portlet:namespace/>id_supplier_raport').val();
	from_year = $('#<portlet:namespace/>start_date_an').val();
	to_year = $('#<portlet:namespace/>end_date_an').val();
	from_month = $('#<portlet:namespace/>start_date_luna').val();
	to_month = $('#<portlet:namespace/>end_date_luna').val();
	if(from_year == '') {
		from_year = 0;
	}
	if(to_year == '') {
		to_year = 0;
	}
	if(from_month == '') {
		from_month = 0;
	}
	if(to_month == '') {
		to_month = 0;
	}
	$('#<portlet:namespace/>company_id').val(societate);
	$('#<portlet:namespace/>id_magazin').val(magazin);
	$('#<portlet:namespace/>supplier_id').val(supplier);
	$('#<portlet:namespace/>from_year').val(from_year);
	$('#<portlet:namespace/>to_year').val(to_year);
	$('#<portlet:namespace/>from_month').val(from_month);
	$('#<portlet:namespace/>to_month').val(to_month);
	$('#<portlet:namespace/>exportFormRaport').submit();
	
}

YUI({ lang: 'ro' }).use('aui-datatable','aui-datatype','datatable-sort', 'aui-datepicker', 'aui-base', 'liferay-portlet-url', 'aui-node',
		function(Y){
		var remoteData = [
	      			<% for (int i = 0; i < allInvLines.size(); i++) { %>
	      				{
	      					nr_cerere: 		'<%=allInvLines.get(i).get("nr_cerere")%>',
	      					solicitant: 	'<%=allInvLines.get(i).get("solicitant")%>',
	      					department: 	'<%=allInvLines.get(i).get("department")%>',
	      					societate:		'<%=allInvLines.get(i).get("societate")%>',
	      					magazin:		'<%=allInvLines.get(i).get("magazin")%>',
	      					codFurnizor:	'<%=allInvLines.get(i).get("codFurnizor")%>',
	      					numeFurnizor:	'<%=allInvLines.get(i).get("numeFurnizor")%>',
	      					tipSolicitare:	'<%=allInvLines.get(i).get("tipSolicitare")%>',
	      					valoareRON:		'<%=allInvLines.get(i).get("valoareRON")%>',
	      					valoareCURR:	'<%=allInvLines.get(i).get("valoareCURR")%>',
	      					moneda:			'<%=allInvLines.get(i).get("moneda")%>',
	      					detalii: 		'<%=allInvLines.get(i).get("detalii")%>',
	      					validation_date: '<%=allInvLines.get(i).get("validation_date")%>',
	      					stare: 		'<%=allInvLines.get(i).get("stare")%>',
	      					facturi: 		'<%=allInvLines.get(i).get("facturi")%>',
	      					data_inchidere: 	'<%=allInvLines.get(i).get("data_inchidere")%>',
	      					doc_inchidere: 	'<%=allInvLines.get(i).get("doc_inchidere")%>'
	      					
	      						
	      				}
	      				<% if (i != (allInvLines.size() - 1)) { out.print(","); } %>
	      			<% } %>
	      		];
		var nestedColumns = [		
							{key: 'nr_cerere', 	label:'Numar cerere avans'},
							{key: 'solicitant', 	label:'Solicitant cerere avans'},
							{key: 'department', 	label:'Departament'},
							{key:'societate', 	label:'Societate'},
							{key: 'magazin', 	label:'Magazin'},
							{key: 'codFurnizor', 	label:'Cod furnizor'},
							{key: 'numeFurnizor', 		label:'Nume furnizor'},
							{key: 'tipSolicitare', 	label:'Tip document'},
							{key: 'facturi', label:'Numar document'},
							{key: 'valoareRON', 	label:'Valoare RON'},
							{key: 'valoareCURR', 	label:'Valoare Euro'},
		         			{key: 'detalii', label: 'Explicatii avans'},
		         			{key: 'validation_date', label:'Data aprobare avans'},
		         			{key: 'stare', label:'Status avans'},
		         			{key: 'data_inchidere', label:'Data inchidere'},
		         			{key: 'doc_inchidere', 	label:'Document inchidere'}
		         		];
		dataTableIV = new Y.DataTable({
			columns:nestedColumns,
			data:remoteData,
			scrollable: "x",
	        width: "100%",
			recordType:['nr_cerere', 'solicitant', 'department', 'societate', 'magazin', 'codFurnizor', 'numeFurnizor', 'tipSolicitare', 'facturi',
			            'valoareRON', 'valoareCURR', 'detalii', 'validation_date', 'status', 'doc_inchidere'],
			editEvent:'click'
		}).render('#inventar');
		
		// PAGINATION
		// set status of navigation buttons on page load
		setNavigationButtonsState('<portlet:namespace />');
		$('.navigation').click(function(e) {
			var elementT 	= e.target;
			var elementId 	= elementT.id;
			var action 		= "";
			
			// determine which button was pressed and set the value of start accordingly
			if (elementId.indexOf("first")> -1)	{ action = "first"; start = 0; }
			if (elementId.indexOf("prev") > -1) 	{ action = "prev"; if (start - count <= 0) {start = 0} else { start -= count; } }
			if (elementId.indexOf("next") > -1) 	{ action = "next"; start = parseInt(start) + parseInt(count); }
			if (elementId.indexOf("last") > -1) 	{ action = "last"; start = parseInt(parseInt(total) / parseInt(count)) * count; if (start == total) { start -= count; } }
			
			makeAjaxCall(action);
		});
		
});
function makeAjaxCall(action) {
	$.ajax({
		type: "POST",
		url: "<%=ajaxURL%>",
		data: 	"<portlet:namespace />action=" + action + 
				"&<portlet:namespace />start=" + start + 
				"&<portlet:namespace />count=" + count +
				"&<portlet:namespace />total=" + total +
				"&<portlet:namespace />societate=" + societate +
				"&<portlet:namespace />magazin=" + magazin +
				"&<portlet:namespace />supplier_id=" + supplier +
				"&<portlet:namespace />from_year=" + from_year +
				"&<portlet:namespace />to_year=" + to_year +
				"&<portlet:namespace />from_month=" + from_month +
				"&<portlet:namespace />to_month=" + to_month,
		success: function(msg) {
			// get table data
			if (msg != ""){
				var jsonEntries = jQuery.parseJSON(msg);
				total = jsonEntries.total;
				dataTableIV.set('data', eval(jsonEntries.values));
			} else {
				dataTableIV.set('data', []);
			}
			// set status of navigation buttons on page load
			setNavigationButtonsState('<portlet:namespace />');
		}
	});
}
</script>
<aui:form id="exportFormRaport" name="exportFormRaport" style="dysplay:none"  action="<%=exportRaportURL.toString()%>">
	<aui:input type="hidden" id="company_id" name="company_id"/>
	<aui:input type="hidden" id="supplier_id" name="supplier_id"/>
	<aui:input type="hidden" id="id_magazin" name="id_magazin"/>
	<aui:input type="hidden" id="from_year" name="from_year"/>
	<aui:input type="hidden" id="to_year" name="to_year"/>
	<aui:input type="hidden" id="from_month" name="from_month"/>
	<aui:input type="hidden" id="to_month" name="to_month"/>
</aui:form>

