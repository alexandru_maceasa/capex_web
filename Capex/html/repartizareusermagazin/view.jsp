<%@page import="com.profluo.ecm.controller.UserTeamIdUtils"%>
<%@page import="com.profluo.ecm.model.db.UsersInformations"%>
<%@page import="com.profluo.ecm.controller.ControllerUtils"%>
<%@page import="com.profluo.ecm.model.db.InvoiceApprovals"%>
<%@page import="com.profluo.ec.carrefour.htmlutils.DropDownUtils"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme" %>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>
<%@ taglib uri="http://alloy.liferay.com/tld/aui" prefix="aui" %>
<%@ taglib uri="http://liferay.com/tld/util" prefix="liferay-util" %>

<liferay-theme:defineObjects />
<portlet:defineObjects />

<portlet:actionURL name="salveazaRepartizare" var="submitURL" />

<portlet:resourceURL id="ajaxURL" var="ajaxURL"/>

<aui:form action="<%=submitURL %>" id="submitForm" name="submitForm" method="post">
	<aui:fieldset>
		<legend>Repartizare manageri regionali</legend>
		<aui:layout>
			<aui:column columnWidth="10" first="true">
				<label style="padding-top:5px" for="<portlet:namespace/>user_id">Manager</label>
			</aui:column>
			<aui:column columnWidth="15">
				<aui:select id="user_id" name="user_id" label="" required="true" onChange="resetListaMagazine()">
					<%=DropDownUtils.generateDropDownWithTwoNameColumn(UsersInformations.getUserDetailsByTeamId(UserTeamIdUtils.DIRECTOR_REGIONAL), "------------------------", "user_id", "first_name", "last_name")%>
				</aui:select>
				<p class="mesaj" style="display:none; color:red; width:300%;">Atentie! Alocarile trebuie salvate individual.</p>
				<aui:button id="salveazaRepartizare" value="Salveaza" style="display:none; margin:0px" cssClass="btn btn-primary mesaj"/>
			</aui:column>
			<aui:column last="true">
				<div id="user_stores" style="display:none"></div>
			</aui:column>
		</aui:layout>
		<aui:layout>
			<aui:input name="liniiMagazine" label="" id="liniiMagazine" style="display:none"></aui:input>
		</aui:layout>
	</aui:fieldset>
</aui:form>

<script type="text/javascript">
$(document).ready(function() {
	$('#<portlet:namespace/>user_id').chosen();
});

function resetListaMagazine() {
	$.ajax({
			type: "POST",
			url: "<%=ajaxURL%>",
			data: 	"<portlet:namespace />action=reseteazaLista" + 
					"&<portlet:namespace />userId=" + $('#<portlet:namespace/>user_id').val(),
			success: function(msg) {
				if (msg != ""){
					var jsonEntries = jQuery.parseJSON(msg);
					dataTable.set('data', eval(jsonEntries.values));
					$('.mesaj').show();
					$('#user_stores').show();
				} else {
					dataTable.set('data', []);
				}
			}
		});
	
}

var dataTable;
YUI().use('aui-datepicker', 'aui-datatable', 'aui-datatype', 'datatable-sort',
		function(Y) {
			var remoteData = []
			var nestedCols = [
								{key: 'checkBox', className:'align_center', label: '<input type="checkbox" name="header_checkbox" class="select-all align_center" />', 
									allowHTML:  true, 
									formatter: function (o) {
													if (o.value == true) {
														return '<input type="checkbox" checked/>';
													} else {
														return '<input type="checkbox" />';
													}
												},
									emptyCellValue: '<input type="checkbox" />'
								},
								{key: 'magazin', label:'Magazin'},
								{key: 'idMagazin', label:'ID Magazin', className: 'hiddencol'}
							 ];
			
			dataTable = new Y.DataTable({
			    columns: nestedCols,
			    data: remoteData,	
			    editEvent: 'click',
			    recordType: ['magazin']
			}).render('#user_stores');
			
			dataTable.delegate('click', function (e) {
		 	        var checked = e.target.get('checked') || undefined;
		 	        this.data.invoke('set', 'checkBox', checked, { silent: true });
		 	        this.syncUI();
		 	        if (checked) {
		 	        	dataTable.get('contentBox').one('.select-all').set('checked', true);
		 	        }
		 	    }, '.select-all', dataTable);
			
			dataTable.delegate("click", function(e){
	 	        var checked = e.target.get('checked') || undefined;
	 	        this.getRecord(e.target).set('checkBox', checked, { silent: true });
	 	    }, ".table-col-checkBox input", dataTable);
			
			$('#salveazaRepartizare').click( function (e) {
				var ml = dataTable.data;
				var json = ml.toJSON();
				var stringData = JSON.stringify(json);
				$('#<portlet:namespace/>liniiMagazine').val(stringData);
				$('#<portlet:namespace/>submitForm').append('<input type="submit" name="submit-button" id="submit-button"></input>');
				$('#submit-button').click();
				setTimeout(function() { $('#submit-button').remove(); }, 200);
			});
			
			
						 
});
</script>