<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme"%>

<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui"%>
<%@ taglib uri="http://alloy.liferay.com/tld/aui" prefix="aui"%>
<%@ taglib uri="http://liferay.com/tld/util" prefix="liferay-util"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.List"%>
<%@page import="com.profluo.ecm.model.db.InvoiceHeader"%>

<%@ page import="java.util.ResourceBundle"%>
<%@ page import="java.util.Locale"%>

<liferay-theme:defineObjects />
<portlet:defineObjects />

<portlet:resourceURL id="ajaxURL" var="ajaxURL"/>

<%
	locale = renderRequest.getLocale();
	ResourceBundle resmainProv = portletConfig.getResourceBundle(locale);
	
	//retrieve all inexistent suppliers
	List<HashMap<String, Object>> allInexistentSuppliers = InvoiceHeader.getInexistentSuppliers();
	// retrieve all companies from DB
	List<HashMap<String, Object>> allCompanies = DefCompaniesVo.getInstance();
%>

<portlet:actionURL name="updateEntry" var="submitURL" />


<liferay-ui:success message="update_ok" key="update_ok"/>
<liferay-ui:error message="update_nok" 	key="update_nok"/>

<aui:layout>
	<aui:column first="true" columnWidth="50">
		<div id="id_create_supplier"></div>
	</aui:column>
	<aui:column last="true" columnWidth="50">
		<%@ include file="/html/includes/associatedInvoices.jsp" %>
	</aui:column>
</aui:layout>

<script type="text/javascript">
var id = 0;
</script>


<script type="text/javascript">
YUI().use('aui-datatable','aui-datatype','datatable-sort',
		function(Y){
	
		/** GENERIC SAVE ON ENTER **/
		function saveOnEnter(e) {
			if (e.domEvent.charCode == 13) {
				if (!e.target.validator.hasErrors()) {
					e.target.fire('save', {
		                newVal: e.target.getValue(),
		                prevVal: e.target.get('value')
		            });
				}
			}
		}
		
		/** GENERIC SAVE ON CHANGE ON SELECT **/
		function saveOnChange(e) {
			e.target.fire('save', {
	            newVal: e.target.getValue(),
	            prevVal: e.target.get('value')
	        });
		}
		//all companies
		var societati = { '0':'Societate', <% for (int i = 0; i < allCompanies.size(); i++) { %>								
			<% if (allCompanies.size() - 1 == i) { %>
				<%=allCompanies.get(i).get("id")%>:'<%=allCompanies.get(i).get("name")%>'
			<% } else { %>
				<%=allCompanies.get(i).get("id")%>:'<%=allCompanies.get(i).get("name")%>',
			<% } %>
		<% } %>};
		
		// editor attributes, actions and formats based on datatypes
		var attrStringEditor = {on :{keyup: saveOnEnter}, showToolbar: false};
	
		var remoteData = [
      		    <% for ( int i = 0; i < allInexistentSuppliers.size(); i++) { %>
      			{
      				id: '<%=allInexistentSuppliers.get(i).get("id")%>',
      				supplier_name:'<%=allInexistentSuppliers.get(i).get("name")%>',
      				cui:'<%=allInexistentSuppliers.get(i).get("cui")%>',
      				company : '<%=allInexistentSuppliers.get(i).get("company")%>',
      				aloc_code :'',
      				vizualizeaza : '<%=allInexistentSuppliers.get(i).get("id")%>',
      				validare : '<%=allInexistentSuppliers.get(i).get("id")%>',
      				
      			}
      			<% if (i != allInexistentSuppliers.size() - 1) {  out.print(",");} %>
      			<% } %>
	      		];
		//columns info
		var nestedColumns=[
						{key : 'id', className : 'hiddencol'},
	                   	{key : 'supplier_name', label: 'Furnizor'},
		                {key : 'cui', label: 'CUI'},
		                {key : 'company', label: 'Societate', editor: new Y.DropDownCellEditor({options: societati, on : {change: saveOnChange}, showToolbar: false}),
		    				emptyCellValue: '',
		    				formatter: function(o) {
		    					var obj = JSON.parse(JSON.stringify(societati));
		    					
		    					if (typeof o.value != 'undefined') {
		    						return obj[o.value].toString();
		    					} else {
		    						return '0';
		    					}
		    				}
		    			},
		                {key : 'aloc_code', label: 'Alocare cod', editor: new Y.TextCellEditor(attrStringEditor)},
		                {key : 'vizualizeaza', label: 'Vizualizeaza', allowHTML:true,  
		                	// gets the id of the row and creates an unique link for every row
		       				formatter: function(o) {
		       					return '<a href="#" id="row' + o.value + '" class="showInvoices">Lista facturi</a>';
		       				},
		       				emptyCellValue: '<a href="#" id="row{value}" class="showInvoices">Lista facturi</a>',
		       			},
		                {key : 'validare', label: 'Validare',allowHTML:true, 
		    				formatter: function(o) {
		    					return '<a href="#" id="row' + o.value +'" class="btn btn-primary validate">Validare</a>';
		    				},
		    				emptyCellValue: '<a href="#" id="row{value}" class="btn btn-primary validate">Validare</a>',
		                }
		];
		

		//table init
		var dataTable=new Y.DataTable({
			columns:nestedColumns,
			data:remoteData,
			editEvent:'click',
			plugins: [{
		        cfg: { highlightRange: false },
		        fn: Y.Plugin.DataTableHighlight
		    }]
		}).render('#id_create_supplier');
		
		//dataTableInvoices.get('boundingBox').unselectable();
		 
		function validate(sel_id){
			var ml  = dataTable.data, msg = '', template = '';
			ml.each(function (item, i) {
				var data = item.getAttrs(['id', 'aloc_code', 'company']);
				if (data.id == sel_id){
					if (data.aloc_code == ""){
						alert ("Va rugam alocati un cod furnizorului!");
						return;
					}
					$('#<portlet:namespace />id').val(data.id);
					$('#<portlet:namespace />supplier_code').val(data.aloc_code);
					
					//submit the form
					$('#<portlet:namespace />updateentry').submit();
				}
			});
		}
		function showAllInvoices (sel_id){
			var ml  = dataTable.data, msg = '', template = '';
			  	
	  		ml.each(function (item, i) {
	  			var data = item.getAttrs(['id']);					
	  			if (data.id == sel_id) {
	  				id = data.id;
	  				return;
	  			}
	  		});
	  		if ( id != 0 ){
	  			var action = "get";
	  			makeAjaxCall(action);
	  		}
	  	}
		
		dataTable.delegate('click', function (e) {
			e.preventDefault();
	    	 // undefined to trigger the emptyCellValue
		    var target = e.target.get('id');
		    if (target.replace('row','') == id ){
	    		if ($('#facturi_furnizor:visible').length != 0 ) {
	    			$('#facturi_furnizor').hide();
	    		} else {
	    			$('#facturi_furnizor').show();
	    		}
	    	} else {
				showAllInvoices(target.replace('row',''));
	    	}
		}, '.showInvoices', dataTable);
		
		dataTable.delegate('click', function (e) {
			e.preventDefault();
	    	 // undefined to trigger the emptyCellValue
		    var target = e.target.get('id');
			validate(target.replace('row',''));
			//window.location.href=redirect_add;
		}, '.validate', dataTable);
		
		function makeAjaxCall(action) {
	  			$.ajax({
	  				type: "POST",
	  				url: "<%=ajaxURL%>",
	  				data: 	"<portlet:namespace />id=" + id +
	  						"&<portlet:namespace />action=" + action,
	  				success: function(msg) {
	  					// get table data
	  					if (msg != ""){
	  						var jsonEntries = jQuery.parseJSON(msg);
	  						dataTableInvoices.set('data', eval(jsonEntries.values));
	  					} else {
	  						dataTableInvoices.set('data', []);
	  					}
	  					$('#facturi_furnizor').show();
	  				}
	  			});
	  	}
}
);
</script>

<aui:form method="post" action="<%=submitURL.toString() %>" name="updateentry" id="updateentry" style="margin:0;padding:0">
	<aui:input type="hidden" id="id" name="id"/>
	<aui:input type="hidden" id="supplier_code" name="supplier_code"/>
</aui:form>