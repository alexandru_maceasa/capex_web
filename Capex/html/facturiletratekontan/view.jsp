<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme" %>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>
<%@ taglib uri="http://alloy.liferay.com/tld/aui" prefix="aui" %>
<%@ taglib uri="http://liferay.com/tld/util" prefix="liferay-util" %>
<%@page import="com.profluo.ecm.model.db.RaportSituatieInregistrari"%>

<%@page import="java.util.HashMap"%>
<%@page import="java.util.List"%>

<portlet:defineObjects />

<portlet:resourceURL id="ajaxURL" var="ajaxURL"/>

<portlet:resourceURL id="exportAction" var="exportURL">
	<portlet:param name="action" value="exportExcel"/>
</portlet:resourceURL>

<aui:form id="exportForm" name="exportForm" action="<%=exportURL.toString()%>">
<aui:layout>
	<aui:column columnWidth="10" first="true">
		<label style="padding-top:5px" for="start_date">Data contabila inceput: </label>
	</aui:column>
	<aui:column columnWidth="10"  style="text-align:right">
		<aui:input label="" name="start_date_an" id="start_date_an" type="text" value="" style="width:90%" placeholder="An contabil">
			 <aui:validator name="required" errorMessage="Campul este obligatoriu." />
		</aui:input>
	</aui:column>
	<aui:column columnWidth="10"  style="text-align:right">
		<aui:select id="start_date_luna" name="start_date_luna" label="" style="width:95%" required="true">
				<aui:option selected="selected" value="0">Luna contabil</aui:option>
				<aui:option value="1">Ianuarie</aui:option>
				<aui:option value="2">Februarie</aui:option>
				<aui:option value="3">Martie</aui:option>
				<aui:option value="4">Aprilie</aui:option>
				<aui:option value="5">Mai</aui:option>
				<aui:option value="6">Iunie</aui:option>
				<aui:option value="7">Iulie</aui:option>
				<aui:option value="8">August</aui:option>
				<aui:option value="9">Septembrie</aui:option>
				<aui:option value="10">Octombrie</aui:option>
				<aui:option value="11">Noiembrie</aui:option>
				<aui:option value="12">Decembrie</aui:option>
			</aui:select>
	</aui:column>
</aui:layout>
<aui:layout>
	<aui:column columnWidth="10" first="true">
		<label style="padding-top:5px" for="end_date">Data contabila sfarsit: </label>
	</aui:column>
	<aui:column columnWidth="10"  style="text-align:right">
		<aui:input label="" name="end_date_an" id="end_date_an" type="text" value="" style="width:90%" placeholder="An contabil">
			 <aui:validator name="required" errorMessage="Campul este obligatoriu." />
		</aui:input>
	</aui:column>
	<aui:column columnWidth="10"  style="text-align:right">
		<aui:select id="end_date_luna" name="end_date_luna" label="" style="width:95%" required="true">
				<aui:option selected="selected" value="0">Luna contabil</aui:option>
				<aui:option value="1">Ianuarie</aui:option>
				<aui:option value="2">Februarie</aui:option>
				<aui:option value="3">Martie</aui:option>
				<aui:option value="4">Aprilie</aui:option>
				<aui:option value="5">Mai</aui:option>
				<aui:option value="6">Iunie</aui:option>
				<aui:option value="7">Iulie</aui:option>
				<aui:option value="8">August</aui:option>
				<aui:option value="9">Septembrie</aui:option>
				<aui:option value="10">Octombrie</aui:option>
				<aui:option value="11">Noiembrie</aui:option>
				<aui:option value="12">Decembrie</aui:option>
			</aui:select>
	</aui:column>
</aui:layout>
<aui:layout>
	<aui:column columnWidth="50" first="true" style="text-align:left">
		<a class="btn btn-primary" onclick="exportFacturi('<portlet:namespace />')"> Export Excel </a>
	</aui:column>
</aui:layout>
</aui:form>

<div id="facturi_letrate"></div>

<script type="text/javascript">
		function exportFacturi(portletId){
			
			start_date_an = $('#<portlet:namespace />start_date_an').val();
			start_date_luna = $('#<portlet:namespace />start_date_luna').val();
			end_date_an = $('#<portlet:namespace />end_date_an').val();
			end_date_luna = $('#<portlet:namespace />end_date_luna').val();
			
			if ( (2015 > start_date_an) ||(start_date_an > 2020) || (2015 > end_date_an) ||(end_date_an > 2020) ){
				alert("Va rugam sa introduceti un an valid");
				return;
			}
			if ( (1 > start_date_luna) ||(start_date_luna > 12) || (1 > end_date_luna) ||(end_date_luna > 12) ){
				alert("Va rugam sa selectati luna");
				return;
			}
		$('#'+portletId+'exportForm').submit();
		}
	</script>
	


<%
int start = 0; 
int count = 10;
int total = RaportSituatieInregistrari.getAllFacturiLetrateTotal();
%>

<script type="text/javascript">
var start = <%=start%>
var count = <%=count%>
var total = <%=total%>

var supplier = "";
var invoiceNo = "";
</script>

<%
	List<HashMap<String,Object>> allInvLines = RaportSituatieInregistrari.getFacturiLetrateWithLimit(start, count, "", "");
%>


<%-- HTML for PAGINATION --%>
<%@ include file="/html/includes/pagination.jsp" %>

<script type="text/javascript">

YUI({ lang: 'ro' }).use('aui-datatable','aui-datatype','datatable-sort', 'aui-datepicker', 'aui-base', 'liferay-portlet-url', 'aui-node',
		function(Y){
		var remoteData = [
	      			<% for (int i = 0; i < allInvLines.size(); i++) { %>
	      				{
	      					numar_factura: 	'<%=allInvLines.get(i).get("numar_factura")%>',
	      					data_factura:		'<%=allInvLines.get(i).get("data_factura")%>',
	      					societate :		'<%=allInvLines.get(i).get("societate")%>',
	      					magazin : 		'<%=allInvLines.get(i).get("magazin")%>',
	      					furnizor: 		'<%=allInvLines.get(i).get("furnizor")%>',
	      					total_with_vat_curr:	'<%=allInvLines.get(i).get("total_with_vat_curr")%>',
	      					reg_date: 		'<%=allInvLines.get(i).get("reg_date")%>',
	      					tip_letraj:		'<%=allInvLines.get(i).get("tip_letraj")%>',
	      					valoare_platita:'<%=allInvLines.get(i).get("valoare_platita")%>',
	      					data_plata:		'<%=allInvLines.get(i).get("data_plata")%>'
	      				}
	      				<% if (i != (allInvLines.size() - 1)) { out.print(","); } %>
	      			<% } %>
	      		];
		var nestedColumns = [		
		         			{key: 'numar_factura', label: '<input id="inv_number" name="inv_number" placeholder="Numar factura" style="width:100px"', allowHTML:true,resizable:true}, 
		         			{key: 'data_factura', label:'Data factura'},
		         	        {key: 'societate', label:'Societate'},
		         	      	{key: 'magazin', label:'Magazin'},
		         	        {key: 'furnizor', label: '<input id="furnizor" name="furnizor" placeholder="Furnizor" style="width:200px"/>', allowHTML:true,resizable:true},
		         	        {key: 'total_with_vat_curr',label:'Total cu TVA'},
		         	        {key: 'reg_date', label :'Data inregistrare'},
		         	        {key: 'tip_letraj', label :'Tip letraj'},
		         	        {key: 'valoare_platita', label :'Valoare platita'},
		         	        {key: 'data_plata', label:'Data plata'}
		         	     
		         		];
		var dataTable = new Y.DataTable({
			columns:nestedColumns,
			data:remoteData,
			editEvent:'click'
		}).render('#facturi_letrate');
		
		
		// PAGINATION
		// set status of navigation buttons on page load
		setNavigationButtonsState('<portlet:namespace />');
		$('.navigation').click(function(e) {
			var elementT 	= e.target;
			var elementId 	= elementT.id;
			var action 		= "";
			
			// determine which button was pressed and set the value of start accordingly
			if (elementId.indexOf("first") > -1)	{ action = "first"; start = 0; }
			if (elementId.indexOf("prev") > -1) 	{ action = "prev"; if (start - count <= 0) {start = 0} else { start -= count; } }
			if (elementId.indexOf("next") > -1) 	{ action = "next"; start = parseInt(start) + parseInt(count); }
			if (elementId.indexOf("last") > -1) 	{ action = "last"; start = parseInt(parseInt(total) / parseInt(count)) * count; if (start == total) { start -= count; } }
			
			makeAjaxCall(action);
		});
		function makeAjaxCall(action) {
			$.ajax({
				type: "POST",
				url: "<%=ajaxURL%>",
				data: 	"<portlet:namespace />action=" + action + 
						"&<portlet:namespace />start=" + start + 
						"&<portlet:namespace />count=" + count +
						"&<portlet:namespace />total=" + total +
						"&<portlet:namespace />InvoiceNo=" + invoiceNo +
						"&<portlet:namespace />supplier=" + supplier,
				success: function(msg) {
					// get table data
// 					alert(msg)
					if (msg != ""){
						var jsonEntries = jQuery.parseJSON(msg);
						total = jsonEntries.total;
						dataTable.set('data', eval(jsonEntries.values));
					} else {
						dataTable.set('data', []);
					}
					
					// set status of navigation buttons on page load
					setNavigationButtonsState('<portlet:namespace />');
				}
			});
		}
		// PAGINATION
		
		
 		$('#inv_number').keyup(function (e) {
 		    if ($("#inv_number").is(":focus") && (e.keyCode == 13)) {
 				var action 		= "filter";
 				invoiceNo = e.target.value;
 				console.log(invoiceNo);
 				supplier = $('#furnizor').val();
 				console.log(supplier);
 				// reset start page 
 				start = 0;
 				// reset count
 				total = 0;
 				// make ajax call
 				makeAjaxCall(action);				
 		    }
 		});
		
 		$('#furnizor').keyup(function (e) {
 		    if ($("#furnizor").is(":focus") && (e.keyCode == 13)) {
 				var action 		= "filter";
 				supplier = e.target.value;
 				invoiceNo = $('#inv_number').val();
 				// reset start page 
 				start = 0;
 				// reset count
 				total = 0;
 				// make ajax call
 				makeAjaxCall(action);				
 		    }
 		});
});

</script>

