<%@page import="com.profluo.ecm.model.db.AdvanceRequest"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme"%>
<%@page import="com.profluo.ecm.model.db.DatabaseConnectionManager"%>
<%@page import="com.liferay.portal.util.PortalUtil"%>

<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui"%>
<%@ taglib uri="http://alloy.liferay.com/tld/aui" prefix="aui"%>
<%@ taglib uri="http://liferay.com/tld/util" prefix="liferay-util"%>

<%@ page import="java.util.ResourceBundle"%>
<%@ page import="java.util.Locale"%>

<liferay-theme:defineObjects />
<portlet:defineObjects />

<portlet:resourceURL id="ajaxURL" var="ajaxURL"/>

<%
	locale = renderRequest.getLocale();
	ResourceBundle resmain = portletConfig.getResourceBundle(locale);
	
	//get supplier info
	List<HashMap<String,Object>> supplier_info = null;
	
	String idSolicitarePlata 	= "";
	String status				= "ro";
	String whereAmI				= "request";
	List<HashMap<String,Object>> oneRow = null;
	String tip_capex = "0";
	String initiativa = "0";
	String prj = "0";
	try {
		idSolicitarePlata = 		renderRequest.getParameter("id").toString();	
	} catch (Exception e) { }
	
	if(idSolicitarePlata.equals("") || idSolicitarePlata == null) {
		String url = PortalUtil.getCurrentCompleteURL(request);
		String [] params = url.split("&");
		String [] lastParameter = params[params.length-1].split("=");
		if (lastParameter[0].equals("id")){
			idSolicitarePlata = lastParameter[1];
		}
	}
	
	if(idSolicitarePlata.equals("")){
		status = "";
	}
	
	if(!idSolicitarePlata.equals("")){
		oneRow = AdvanceRequest.getOne(idSolicitarePlata);
		tip_capex = oneRow.get(0).get("tip_capex").toString();
		initiativa = oneRow.get(0).get("id_initiative").toString();
		prj = oneRow.get(0).get("id_new_prj").toString();
	}
	
	try {
		supplier_info = DefSupplier.getSupplierInfo(oneRow.get(0).get("id_supplier").toString());
	} catch (Exception e ){}
%>
<portlet:renderURL var="BackURL" >
	<portlet:param name="mvcPath" value="/html/requestadvanceapproval/view.jsp"></portlet:param>
</portlet:renderURL>

<portlet:actionURL name="addRequest" var="submitURL" />

<portlet:resourceURL id="ajaxURL" var="ajaxURL"/>

<liferay-ui:success key="update_ok" message="update_ok" />
<liferay-ui:error key="update_nok" message="update_nok" />
<liferay-ui:error key="insert_nok" message="insert_nok" />
<liferay-ui:error key="params_nok" message="params_nok" />
<liferay-ui:error key="error" message="error" />
<script type="text/javascript">
	var unpaidInvoicesFromSupplier = [];
</script>


<aui:form action="<%=submitURL%>"  method="post" name="add-invoice">
	<%@include file="../includes/fact_info.jsp" %>
	
	<aui:layout>
		<aui:column last="true">
			<aui:input type="hidden" name="dpi_list" id="dpi_list" value="">
				<aui:validator name="required" errorMessage="Va rugam sa adaugati cel putin un DPI la cererea de avans!"></aui:validator>
			</aui:input>
			<aui:input type="hidden" name="isReadonly" id="isReadonly" value="<%=status%>"></aui:input>
		</aui:column>
	</aui:layout>
	<aui:layout>
		<aui:column first="true">
			<aui:button name="inapoi" cssClass="btn btn-primary-red" onClick ="<%=BackURL.toString()%>" value="<%=resmain.getString(\"back\")%>"/>
		</aui:column>
		<% if (status.equals("")) { %>
		<aui:column last="true">
			<aui:button id="validate_request" type="button" value="<%=resmain.getString(\"buton_aprobare\")%>"/>
			<aui:button id="add_request" style="display:none" type="submit"/>
		</aui:column>
		<% } %>
	</aui:layout>
</aui:form>
<%-- 
<% if (!idSolicitarePlata.equals("")) { %>
<script type="text/javascript">

	$(document).ready(function(){
		$('#<portlet:namespace />id_supplier').val('<%=oneRow.get(0).get("id_supplier").toString()%>');
		$('#<portlet:namespace />vendor').val('<%=supplier_info.get(0).get("name")%>');
		$('#<portlet:namespace />payment_term').val('<%=supplier_info.get(0).get("payment_term")%>');
		$('#<portlet:namespace />cui').val('<%=(supplier_info.get(0).get("cui") == null ? "" : supplier_info.get(0).get("cui"))%>');
		$('#<portlet:namespace />cont-asociat').val('<%=(supplier_info.get(0).get("bank_account") == null ? "" : supplier_info.get(0).get("bank_account"))%>');
		$('#<portlet:namespace/>supplier_code').val('<%=(supplier_info.get(0).get("supplier_code") == null ? "" : supplier_info.get(0).get("supplier_code"))%>');
		$('#<portlet:namespace/>supplier_data').val('<%=(supplier_info.get(0) == null ? "" : 
			DatabaseConnectionManager.convertListToJson(supplier_info).toJSONString())%>');
		var acc = $('#<portlet:namespace/>supplier_code').val();
		setAssociatedKontanAccInvoice('<portlet:namespace/>', acc, '<%=SupplierStages.INVOICE_SUPPLIER_WITH_2_MF%>');
	});
</script>
<% } %>
--%>
<script type="text/javascript">

YUI({lang: 'ro'}).use( 'aui-datatable', 'aui-datatype', 'datatable-sort', 'overlay', 'aui-form-validator', 'datatable-mutable',
		function(Y) {
			Y.on('click', function(e){
				e.preventDefault();
				if ( !checkCapexDetails('<portlet:namespace/>') ) {
					return false;
				}
				if ($("select[name=<portlet:namespace/>project]").val()) {
					$('#<portlet:namespace/>dpi_list').val('0');
				}
				$('#add_request').click();
			},'#validate_request');
});

	//get all unpaid invoices from the selected supplier
	var tip_solicitare = $('#<portlet:namespace />tip_solicitare');
	var supplier = $('#<portlet:namespace />vendor');
	var current_date = $('#<portlet:namespace />inv_date');
	var company = $('#<portlet:namespace />company');
	if (tip_solicitare && supplier && current_date && company){
		if (tip_solicitare.val() == 3){
			if (supplier.val() != ""){
				getUnpaidInvoicesFromSupplier("getUnpaidInvoices", current_date.val(), supplier.val(), company.val());
			} else {
				alert("Va rugam selectati un furnizor!");
			}
		}
		tip_solicitare.on('change', function(e){
			if (tip_solicitare.val() == 3) {
				if (supplier.val() != ""){
					getUnpaidInvoicesFromSupplier("getUnpaidInvoices", current_date.val(), supplier.val(), company.val());
				} else {
					alert("Va rugam selectati un furnizor!");
					tip_solicitare.val("");
				}
			}
		});
	}
	//ajax call to get all the unpaid invoices that have the due date > current date andd tha belong to the selected supplier
	function getUnpaidInvoicesFromSupplier(action, date, supplier, company){
		$.ajax({
				type: "POST",
				url: "<%=ajaxURL%>",
				data: 	"<portlet:namespace />action=" +  action +
						"&<portlet:namespace />current_date=" + date + 
						"&<portlet:namespace />supplier_name=" + supplier + 
						"&<portlet:namespace />company_id=" +company,
				success: function(msg) {
					//reset the list that contains the resulted invoices
					var unpaidInvoicesFromSupplier = [];
					
  					// get resulted id
  					if (msg != ""){
  						var jsonEntries = jQuery.parseJSON(msg);
  						//set the id resulted from the insertion
  						unpaidInvoicesFromSupplier = jsonEntries.unpaidInvoices;
  						//get the select object
  						var unpaidInvoicesSelect = $('#<portlet:namespace />fact_neplatite');
  						//remove all the existing options
  						unpaidInvoicesSelect.empty();
  						unpaidInvoicesSelect.append('<option value="">-</option>');
  						if (unpaidInvoicesSelect){
  							//append all the options resulted from the ajax call
  							for ( i = 0; i < unpaidInvoicesFromSupplier.length; i++){
  								unpaidInvoicesSelect.append('<option value=' + unpaidInvoicesFromSupplier[i].id + '>' + unpaidInvoicesFromSupplier[i].inv_number + '</option>');
  							};
  						}
  					}
  				}
  			});
	}
</script>
<%--fereastra pop-up solicitare inregistrare furnizor --%>
<%--@ include file="/html/includes/popups/solicita_inregistrare_furnizor.jsp" --%>
<jsp:include page="/html/includes/popups/solicita_inregistrare_furnizor.jsp" /> 
