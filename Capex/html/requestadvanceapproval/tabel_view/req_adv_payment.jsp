<%@ page import="com.liferay.portal.service.UserLocalServiceUtil"%>
<%@ page import="com.profluo.ecm.model.db.AdvanceRequestApprovals"%>
<%@ page import="com.profluo.ecm.model.db.AdvanceRequest"%>
<%@ page import="com.liferay.portal.service.UserServiceUtil"%>
<%@ page import="com.liferay.portal.model.User"%>
<%@ page import="java.util.ResourceBundle"%>
<%@ page import="java.util.Locale"%>
<%@ page import="java.util.HashMap"%>
<%@ page import="java.util.List"%>

<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui"%>
<%@ taglib uri="http://alloy.liferay.com/tld/aui" prefix="aui"%>
<%@ taglib uri="http://liferay.com/tld/util" prefix="liferay-util"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme"%>

<liferay-theme:defineObjects />
<portlet:defineObjects />

<% // PAGINATION Start - env setup 
//int start = 0; 
//int count = 15;
//int total = 0;// AdvanceRequest.getCountByUserId((int) user.getUserId());
%>
<portlet:resourceURL id="ajaxURL" var="ajaxURL"/>
<script type="text/javascript">
var start = 0;
var count = 15;
var total = 0;
</script>
<%
	locale = renderRequest.getLocale();
	ResourceBundle resmainProv = portletConfig.getResourceBundle(locale);
	
	//retrieve all advance requests
	//List<HashMap<String,Object>> allAdvReq = AdvanceRequest.getAdvanceRequestByUserId((int) user.getUserId(), 0, count);
%>

<aui:column first="true" columnWidth="100" last="true">
	<div id="req-adv-aproval"></div>
</aui:column>

<%-- HTML for PAGINATION --%>
<%@ include file="/html/includes/pagination.jsp" %>

<script type="text/javascript">
YUI().use( 'aui-datatable', 'aui-datatype', 'datatable-sort',
	function(Y) {
		// data array
		var remoteData = [
		
		];

		// COLUMN INFO and ATTRIBUTES
		var nestedCols = [
		    { key: 'id', label: 'Nr. doc'},
			{ key: 'company', label: 'Societate'},
			{ key: 'categorie' , label : 'Categorie', 
				formatter: function(o) {
					if (o.value == 0) {
						return 'Capex Diverse';
					} else if (o.value == 1) {
						return 'Initiativa centralizata';
					} else {
						return 'Magazin nou';
					}
				}
			},
			{ key: 'detalii_categ', label: 'Detalii categorie'},
			{ key: 'furnizor' , label : 'Furnizor'},
			{ key: 'datascadenta' , label : 'Data scadenta'},
			{ key: 'avans' , label : 'Avans'},
			{ key: 'currency' , label : 'Moneda'},
			{ key: 'explicatii_plata' , label : 'Explicatii plata'},
			{ key: 'status' ,label:'Status'},
			{ key: 'istoric' , label : 'Istoric aprobari'},
			{ key: 'actiuni', label: 'Actiuni', allowHTML: true, 
				// gets the id of the row and creates an unique link for every row
				formatter: '<a href="#" id="row{id}" class="editrow" >Vizualizare</a>' }	      
		];
		
		function redirect(sel_id) {
		    var ml  = dataTable.data, msg = '', template = '';
	
		    ml.each(function (item, i) {
		        var data = item.getAttrs(['id' , 'categorie', 'detalii_categ', 'furnizor', 'datascadenta', 'avans','currency','explicatii_plata','status','istoric']);
		        if (data.id == sel_id) {
					$('#<portlet:namespace />id').val(data.id);
					if (data.categorie == 0){
						$('#<portlet:namespace />categorie').val("Capex Diverse");
					} else if (data.categorie == 1){
						$('#<portlet:namespace />categorie').val("Initiativa centralizata");
					}else {
						$('#<portlet:namespace />categorie').val("Magazin nou");
		      		}
					$('#<portlet:namespace />detalii_categ').val(data.detalii_categ);
					$('#<portlet:namespace />nr').val(data.id);
					$('#<portlet:namespace />datascadenta').val(data.datascadenta);
					$('#<portlet:namespace />currency').val(data.currency);
					$('#<portlet:namespace />explicatii_plata').val(data.explicatii_plata);
					$('#<portlet:namespace />toRedirect').submit();
		        }
		    });
		}
		// TABLE INIT
		var dataTable = new Y.DataTable({
		    columns: nestedCols,
		    data: remoteData,	
		    editEvent: 'click',
		    recordType: ['id' ,'company', 'categorie', 'detalii_categ', 'furnizor', 'datascadenta', 'avans','currency','explicatii_plata','status','istoric']
		}).render('#req-adv-aproval');
		
		//dataTable.get('boundingBox').unselectable();

	    dataTable.delegate('click', function (e) {
	    	 // undefined to trigger the emptyCellValue
		    var target = e.target.get('id');
	    	// console.log("aici");
			redirect(target.replace('row',''));
			//window.location.href=redirect_add;
		}, '.editrow', dataTable); 
	    
		// PAGINATION
		// set status of navigation buttons on page load
		setNavigationButtonsState('<portlet:namespace />');
		$('.navigation').click(function(e) {
			var elementT 	= e.target;
			var elementId 	= elementT.id;
			var action 		= "";
			
			// determine which button was pressed and set the value of start accordingly
			if (elementId.indexOf("first") > -1)	{ action = "filterByUser"; start = 0; }
			if (elementId.indexOf("prev") > -1) 	{ action = "filterByUser"; if (start - count <= 0) {start = 0} else { start -= count; } }
			if (elementId.indexOf("next") > -1) 	{ action = "filterByUser"; start = parseInt(start) + parseInt(count); }
			if (elementId.indexOf("last") > -1) 	{ action = "filterByUser"; start = parseInt(parseInt(total) / parseInt(count)) * count; }
			
			makeAjaxCall(action);
		});
		
		// general method for ajax calls.
		function makeAjaxCall(action) {
			$.ajax({
				type: 	"POST",
				url: 	"<%=ajaxURL%>",
				data: 	"<portlet:namespace />action=" + action + 
						"&<portlet:namespace />start=" + start +
						"&<portlet:namespace />count=" + count +
						"&<portlet:namespace />total=" + total,
				success: function(msg) {
					// get table data
					if (msg != ""){
						var jsonEntries = jQuery.parseJSON(msg);
						total = jsonEntries.total;
						dataTable.set('data', eval(jsonEntries.values));
					} else {
						dataTable.set('data', []);
					}
					
					// set status of navigation buttons on page load
					setNavigationButtonsState('<portlet:namespace />');
				}
			});
		}
		// PAGINATION
		
		// get All entries after page load
		$(document).ready(function (){
			makeAjaxCall("first");
		});
});
</script>

<aui:form method="post" action="<%=addRequestURL.toString()%>" name="toRedirect" id="toRedirect" style="margin:0;padding:0">
	<aui:input type="hidden" id="id" name="id" />
	<aui:input type="hidden" id="categorie" name="categorie" />
	<aui:input type="hidden" id="detalii_categ" name="detalii_categ" />
	<aui:input type="hidden" id="nr" name="nr" />
	<aui:input type="hidden" id="datascadenta" name="datascadenta" />
	<aui:input type="hidden" id="currency" name="currency" /> 
	<aui:input type="hidden" id="explicatii_plata" name="explicatii_plata" />
	<aui:input type="hidden" id="status" name="status" />
	<aui:input type="hidden" id="istoric" name="istoric" /> 
</aui:form>