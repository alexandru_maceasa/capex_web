<%@ page import="java.util.ResourceBundle"%>

<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme"%>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui"%>
<%@ taglib uri="http://alloy.liferay.com/tld/aui" prefix="aui"%>
<%@ taglib uri="http://liferay.com/tld/util" prefix="liferay-util"%>
<portlet:resourceURL id="ajaxURL" var="ajaxURL"/>

<liferay-theme:defineObjects />
<portlet:defineObjects />

<%
	locale = renderRequest.getLocale();
	ResourceBundle resmainDPI = portletConfig.getResourceBundle(locale);
%>

<div id="vizualizareDPI" class="yui3-overlay-loading" style="right:100px;z-index: 1;position:absolute;width: 800px;display:none">
	<div class="yui3-widget-hd">Solicitare vizualizare DPI</div>
	<div class="yui3-widget-bd"> 
		<aui:layout>
			<aui:fieldset>
				<aui:column columnWidth="20">
					<label for="<portlet:namespace />nr_dpi" style="padding-top: 5px"><%=resmainDPI.getString("nr_dpi")%>:
					</label>
				</aui:column>
				<aui:column columnWidth="30">
					<aui:input id="nr_dpi" name="nr_dpi" label="" readonly="true"></aui:input>
				</aui:column>
				<aui:column columnWidth="20">
					<label for="<portlet:namespace />societate" style="padding-top: 5px"><%=resmainDPI.getString("societate")%>:</label>
				</aui:column>
				<aui:column columnWidth="30">
					<aui:input id="societate" name="societate" label="" readonly="true"></aui:input>
				</aui:column>
			</aui:fieldset>
			
			<aui:fieldset>
				<aui:column columnWidth="20">
					<label for="<portlet:namespace />data_dpi" style="padding-top: 5px"><%=resmainDPI.getString("data_dpi")%>:
					</label>
				</aui:column>
				<aui:column columnWidth="30">
					<aui:input id="data_dpi" name="data_dpi" label="" readonly="true"></aui:input>
				</aui:column>
				<aui:column columnWidth="20">
					<label for="<portlet:namespace />categ_capex_adv" style="padding-top: 5px"><%=resmainDPI.getString("categ_capex")%>:</label>
				</aui:column>
				<aui:column columnWidth="30">
					<aui:input id="categ_capex_adv" name="categ_capex_adv" label="" readonly="true"></aui:input>
				</aui:column>
			</aui:fieldset>
		 
			<aui:fieldset>
				<aui:column columnWidth="20">
					<label for="<portlet:namespace />solicitant" style="padding-top: 5px"><%=resmainDPI.getString("solicitant")%>:
					</label>
				</aui:column>
				<aui:column columnWidth="30">
					<aui:input id="solicitant" name="solicitant" label="" readonly="true"></aui:input>
				</aui:column>
				<%--
				<aui:column columnWidth="20">
					<label for="<portlet:namespace />detalii_cat" style="padding-top: 5px"><%=resmainDPI.getString("detalii_cat")%>:</label>
				</aui:column>
				<aui:column columnWidth="30">
					<aui:input id="detalii_cat" name="detalii_cat" label="" readonly="true"></aui:input>
				</aui:column>
				--%>
			</aui:fieldset>
			
			<aui:fieldset>
				<aui:column columnWidth="20">
					<label for="<portlet:namespace />aprobator" style="padding-top: 5px"><%=resmainDPI.getString("aprobator")%>:
					</label>
				</aui:column>
				<aui:column columnWidth="80">
					<aui:input id="aprobator" name="aprobator" label="" readonly="true" style="width: 97.6%"></aui:input>
					</aui:column>
			</aui:fieldset>
			<aui:layout>
					<aui:column columnWidth="100" last="true" first="true">
						<div id="tabelDPI" ></div>
					</aui:column>
			</aui:layout>
			<aui:layout>
				<aui:column columnWidth="50" last="true" style="text-align:right">
					<aui:input value="Inchide" name="fcancel" type="button" label="" 
							cssClass="btn btn-primary-red fr" onclick="$('#vizualizareDPI').hide()"></aui:input>
					<div class="clear"></div>
				</aui:column>
			</aui:layout>
		</aui:layout>
	</div>
</div>

<script type="text/javascript">
var existentDPIlinks = "";
var v = "";
var dataTableDPI;
var firstDPI = false;
<%--  Overlay solicita vizualizare DPI--%>
YUI().use('aui-node', 'aui-datatable', 'aui-datatype', 'datatable-sort', 'overlay',
	function(Y) {	
		var hdata = [];
		function formatCurrency(cell) {
		    format = {
		    	thousandsSeparator: ",",
		        decimalSeparator: ".",
		        decimalPlaces: noOfDecimalsToDisplay
		    };
		    return Y.DataType.Number.format(Number(cell.value), format);
		}
		
		var cols = [
			{ key: 'id_prod', 				label:'Cod produs'},
			{ key: 'description', 			label:'Denumire produs'},
			{ key: 'unit_price_curr', 		label:'Pret Unitar', formatter: formatCurrency, className: 'currencyCol'},
			{ key: 'quantity', 				label:'Cantitate'},
			{ key: 'total_with_vat_curr', 	label:'Valoare totala', formatter: formatCurrency, className: 'currencyCol'},
			{ key: 'magazin', 				label:'Magazin'},
			{ key: 'clasificare_it', 		label:'Clasificare IT'}
		];
		
		dataTableDPI = new Y.DataTable({
		    columns: cols,
		    data:hdata
		}).render('#tabelDPI');	
		
		Y.on("click", function(e) {
	    	e.preventDefault();
	    	var currentNode = this;
	    	// retrieve the ID of the DPI to be shown
	    	var dpiId = $('#<portlet:namespace/>select_dpi').val();
	    	// show DPI Pop-up
	    	showDPIPopup(currentNode, dpiId);
	    }, "#vizualizare");
	    
	    Y.on("click", function(e) {
	    	e.preventDefault();
	    	var currentNode = this;
	    	console.log("clicked on added dpi: " + currentNode.get('id'));
	    	// retrieve the ID of the DPI to be shown
	    	var dpiId = currentNode.get('id').replace('added_dpi_','');
	    	// show DPI Pop-up
	    	showDPIPopup(currentNode, dpiId);
	    }, '.added_dpis');
	    
	    ///////
		//add dpi to div for listing->fact_info.jsp
		Y.on('click',function(e){
			if ( v != "" && !(existentDPIlinks.indexOf(v) >= 0)) {
				$('#displayDPi').append('<a href="#" class="added_dpis" id="added_dpi_' + v + '">DPI ' + v + '</a><br/>');
				// used to keep track of added DPIs
				existentDPIlinks += v + ", ";
				// set the new DPIs in the hidden input in order to be sent on the request
				$('#<portlet:namespace />dpi_list').val(existentDPIlinks);
				// create bind on newlly added DPI links
			    Y.on("click", function(e) {
			    	e.preventDefault();
			    	var currentNode = this;
			    	console.log("clicked on added dpi: " + currentNode.get('id'));
			    	// retrieve the ID of the DPI to be shown
			    	var dpiId = currentNode.get('id').replace('added_dpi_','');
			    	// show DPI Pop-up
			    	showDPIPopup(currentNode, dpiId);
			    }, '.added_dpis');
			}
		}, '#<portlet:namespace />addDPI');
		
		//show a href when selecting dpi->fact_info.jsp
		/*Y.on('change',function(e){
			v = e.target.get('value');
			if (v == ""){
				$('#vizualizare').hide();
			} else {
				$('#vizualizare').show();
			}
		}, '#<portlet:namespace/>select_dpi');*/
		$('#<portlet:namespace/>select_dpi').chosen().change(function(){
			v = $(this).val();
			if (v == ""){
				$('#vizualizare').hide();
			} else {
				$('#vizualizare').show();
			}
		});
			
	    ///////
	    
	    function showDPIPopup(currentNode, idDPI) {
			<%-- the URL (ajaxSupplierURL) is defined in the /includes/fact_info.jsp --%>
			$.ajax({
				type: 	"POST",
				url: 	"<%=ajaxURL%>",
				data: 	"<portlet:namespace />dpi=" + idDPI + 
						"&<portlet:namespace />action=showDpi",
				success: function(msg) {
					// get table data
					if (msg != ""){
						// set values on the DPI Form after the AJAX request is over
						var jsonEntries = jQuery.parseJSON(msg);
						var dpiHeader = jsonEntries.header;
						var dpiLines = jsonEntries.lines;

						// set header
						$('#<portlet:namespace/>nr_dpi').val(idDPI);
						$('#<portlet:namespace/>data_dpi').val(dpiHeader[0].dpi_date);
						$('#<portlet:namespace/>aprobator').val(dpiHeader[0].approvers);
						$('#<portlet:namespace/>solicitant').val(
								capitalize(dpiHeader[0].user_email.replace("@carrefour.com", "").replace("_", " ")));
						$('#<portlet:namespace/>societate').val(dpiHeader[0].company_name);
						$('#<portlet:namespace/>categ_capex_adv').val(dpiHeader[0].tip_capex);
						if (dpiHeader[0].tip_capex == 'Capex Diverse'){
							$('#<portlet:namespace />capex_diverse').prop('checked', true);
						} else if (dpiHeader[0].tip_capex == 'Initiativa Centralizata'){
							$('#<portlet:namespace />init_cent').prop('checked', true);
						}
						//$('#<portlet:namespace/>detalii_cat').hide();
						// set lines
						dataTableDPI.set('data', eval(dpiLines));
						
					} else {
						dataTableDPI.set('data', []);
					}
					
				}
			});
	    	
	    	var overlayDPI = new Y.Overlay({
	    	    srcNode:"#vizualizareDPI",
	    	    width:"900px",
	    	    align: {
	    	        node: currentNode,
	    	        points:["bl", "bl"]
	    	    }
	    	});
	    	overlayDPI.render();
	        $('#vizualizareDPI').show();	
	    }
	}
);	
</script>		
