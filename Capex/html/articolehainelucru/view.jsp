<%@page import="com.profluo.ecm.model.db.ArticoleHaineLucru"%>
<%@taglib uri="http://java.sun.com/portlet_2_0" 	prefix="portlet"%>
<%@taglib uri="http://liferay.com/tld/theme" 		prefix="liferay-theme"%>
<%@taglib uri="http://liferay.com/tld/ui" 			prefix="liferay-ui"%>
<%@taglib uri="http://alloy.liferay.com/tld/aui" 	prefix="aui"%>
<%@taglib uri="http://liferay.com/tld/util" 		prefix="liferay-util"%>

<%@page import="java.util.HashMap"%>
<%@page import="java.util.List"%>

<portlet:defineObjects />

<liferay-ui:success key="updateOK" message="Modificarea a fost salvata cu succes" />

<portlet:resourceURL id="ajaxURL" var="ajaxURL"/>
<portlet:renderURL var="editURL" >
	<portlet:param name="mvcPath" value="/html/articolehainelucru/inregistrare_articol.jsp"></portlet:param>
</portlet:renderURL>

<portlet:resourceURL id="exportAction" var="exportURL">
	<portlet:param name="action" value="exportExcel"/>
</portlet:resourceURL>

	<aui:column columnWidth="50" last="true" style="text-align:right">
		<a class="btn btn-primary" href="<%=exportURL.toString()%>"> Export Excel </a>
	</aui:column>
<br/>
<aui:layout>
	<aui:column first="true" columnWidth="100" last="true">
		<div id="working-articles"></div>
	</aui:column>
</aui:layout>

<%-- HTML for PAGINATION --%>
<%@ include file="/html/includes/pagination.jsp" %>

<% 
	int start 				= 0; 
	int count 				= 10;
	String denumireArticol 	= "";
	List<HashMap<String,Object>> allWorkingArticles = ArticoleHaineLucru.getAllWorkingArticles(start, count, denumireArticol);
	int total = ArticoleHaineLucru.getAllWorkingArticlesCount(denumireArticol);
%>

<script type="text/javascript">
	var start 			= <%=start%>
	var count 			= <%=count%>
	var total 			= <%=total%>
	var denumireArticol = '<%=denumireArticol%>'
	YUI({ lang: 'ro' }).use('aui-datatable','aui-datatype','datatable-sort', 'aui-datepicker', 'aui-base', 'liferay-portlet-url', 'aui-node',
			function(Y){
			var remoteData = [
		      			<% for (int i = 0; i < allWorkingArticles.size(); i++) { %>
		      				{
		      					product_id: 		'<%=allWorkingArticles.get(i).get("product_id")%>',	
		      					codArticol: 		'<%=allWorkingArticles.get(i).get("codArticol")%>',
		      					denumire: 			'<%=allWorkingArticles.get(i).get("denumire")%>', 
		      					codProdus : 		'<%=allWorkingArticles.get(i).get("codProdus")%>',
		      					denumireProdus: 	'<%=allWorkingArticles.get(i).get("denumireProdus")%>',
		      					pret:				'<%=allWorkingArticles.get(i).get("pret")%>',
		      					furnizor:			'<%=allWorkingArticles.get(i).get("furnizor")%>'
		      				}
		      				<% if (i != (allWorkingArticles.size() - 1)) { out.print(","); } %>
		      			<% } %>
		      		];
			var nestedColumns = [		
								{key: 'product_id', 	className: 'hiddencol'},  
			         			{key: 'codArticol', 	label: 'Cod Articol'}, 
			         			{key: 'denumire', 		label: '<input class="field" style="width:250px !important;margin:0" type="text" value="" id="filter-denumire" name="filter-denumire" placeholder="Denumire"></input>' },
			         	        {key: 'codProdus', 		label:'Cod Produs'},
			         	        {key: 'denumireProdus', label: 'Denumire Produs'},
			         	        {key:'pret',			label:'Pret'},
			         	        {key:'furnizor', 		label :'Furnizor'},
			         	       	{ key: 'actiuni', 		label: 'Actiuni', allowHTML: true, 
			         	        	formatter: '<a href="#" id="row{product_id}-{codArticol}" class="editrow">Editare</a>'}
			         		];
			var dataTable = new Y.DataTable({
				columns:nestedColumns,
				data:remoteData,
				recordType:['codArticol', 'denumire', 'codProdus', 'denumireProdus', 'pret', 'furnizor'],
				editEvent:'click'
			}).render('#working-articles');
			
			// PAGINATION
			// set status of navigation buttons on page load
			setNavigationButtonsState('<portlet:namespace />');
			$('.navigation').click(function(e) {
				var elementT 	= e.target;
				var elementId 	= elementT.id;
				var action 		= "";
				
				// determine which button was pressed and set the value of start accordingly
				if (elementId.indexOf("first") > -1)	{ action = "first"; start = 0; }
				if (elementId.indexOf("prev") > -1) 	{ action = "prev"; if (start - count <= 0) {start = 0} else { start -= count; } }
				if (elementId.indexOf("next") > -1) 	{ action = "next"; start = parseInt(start) + parseInt(count); }
				if (elementId.indexOf("last") > -1) 	{ action = "last"; start = parseInt(parseInt(total) / parseInt(count)) * count; if (start == total) { start -= count; } }
				
				makeAjaxCall(action);
			});
			
			// filtrare dupa denumire
	  		$('#filter-denumire').keyup(function (e) {
	  		    if ($("#filter-denumire").is(":focus") && (e.keyCode == 13)) {
	  				var elementT 	= e.target;
	  				var elementId 	= elementT.id;
	  				var action 		= "filter";
	  				// get selected value
	  				denumireArticol = elementT.value;
	  				start = 0;
	  				makeAjaxCall(action);
	  		    }
	  		});
			
			function makeAjaxCall(action) {
				$.ajax({
					type: "POST",
					url: "<%=ajaxURL%>",
					data: 	"<portlet:namespace />action=" + action + 
							"&<portlet:namespace />start=" + start + 
							"&<portlet:namespace />count=" + count +
							"&<portlet:namespace />total=" + total + 
							"&<portlet:namespace />denumireArticol=" + denumireArticol,
					success: function(msg) {
						// get table data
						if (msg != ""){
							var jsonEntries = jQuery.parseJSON(msg);
							total = jsonEntries.total;
							dataTable.set('data', eval(jsonEntries.values));
						} else {
							dataTable.set('data', []);
						}
						
						// set status of navigation buttons on page load
						setNavigationButtonsState('<portlet:namespace />');
					}
				});
			}
			//END PAGINATION
			
			//la click pe editare
			dataTable.delegate('click', function (e) {
	  		    // undefined to trigger the emptyCellValue
	  		    e.preventDefault();
	  		    var elem = e.target.get('id');
	  		    	var parts = elem.replace('row',"").split("-");
	  		    	$('#<portlet:namespace/>product_id').val(parts[0]);
	  		    	$('#<portlet:namespace/>product_code').val(parts[1]);
	  		    	$('#<portlet:namespace/>editWorkingArticle').submit();
	  		}, '.editrow', dataTable);
			
			
	});
	
</script>


<aui:form method="post" action="<%=editURL.toString() %>" name="editWorkingArticle" id="editWorkingArticle" style="margin:0;padding:0">
	<aui:input type="hidden" id="product_id" name="product_id"/>
	<aui:input type="hidden" id="product_code" name="product_code"/>
</aui:form>
