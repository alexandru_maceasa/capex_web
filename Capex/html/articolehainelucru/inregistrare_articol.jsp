<%@page import="com.profluo.ecm.model.db.ArticoleHaineLucru"%>
<%@page import="com.profluo.ecm.model.vo.DefProductVo"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.List"%>
<%@taglib uri="http://java.sun.com/portlet_2_0" 	prefix="portlet"%>
<%@taglib uri="http://liferay.com/tld/theme" 		prefix="liferay-theme"%>
<%@taglib uri="http://liferay.com/tld/ui" 			prefix="liferay-ui"%>
<%@taglib uri="http://alloy.liferay.com/tld/aui" 	prefix="aui"%>
<%@taglib uri="http://liferay.com/tld/util" 		prefix="liferay-util"%>

<portlet:defineObjects />

<liferay-ui:error key="updateNOK" message="Modificare nu s-a putut salva. Va rugam reincercati." />

<portlet:actionURL name="editArticle" var="submitURL" />
<portlet:renderURL var="backURL">
	<portlet:param name="mvcPath" value="/html/articolehainelucru/view.jsp"></portlet:param>
</portlet:renderURL>

<%
	String articleCode = "";

	try {
		articleCode = request.getParameter("product_code").toString();
	} catch (Exception e){ }
	
	System.out.println("articleCode ->  " + articleCode);

	List<HashMap<String,Object>> allProductCodes = DefProductVo.getInstance();
	List<HashMap<String,Object>> articleInfo = ArticoleHaineLucru.getWorkingArticlesByArticleCode(articleCode);
	
	System.out.println("articleInfo ->  " + articleInfo);
%>

<aui:layout>
	
	<legend>Editare articol</legend>

	<aui:form method="post" action="<%=submitURL.toString() %>" name="editWorkingArticle" id="editWorkingArticle" style="margin:0;padding:0;background-color:blue !important;">
		<aui:column columnWidth="30">
			<label for="<portlet:namespace />product_id" style="padding-top: 5px">Cod Produs</label>
		</aui:column>
		<aui:column columnWidth="70">
			<aui:select id="product_id" name="product_id" label = "">
				<aui:option value = "0">Selectati produsul</aui:option>
				<% for (int i = 0 ; i < allProductCodes.size(); i++ ) {%>
					<aui:option value="<%=allProductCodes.get(i).get(\"id\").toString()%>"><%=allProductCodes.get(i).get("code").toString()%></aui:option>
				<% } %>
			</aui:select>
		</aui:column>
		<aui:column columnWidth="30">
			<label for="<portlet:namespace />product_name" style="padding-top: 5px">Denumire Produs</label>
		</aui:column>
		<aui:column columnWidth="70">
			<aui:input type="text" id="product_name" name="product_name" label ="" disabled="true" value="<%=articleInfo.get(0).get(\"denumireProdus\").toString()%>"/>
		</aui:column>
		<aui:column columnWidth="30">
			<label for="<portlet:namespace />product" style="padding-top: 5px">Cod Articol</label>
		</aui:column>
		<aui:column columnWidth="70">
			<aui:input type="text" id="product" name="product" label ="" disabled="true" value="<%=articleCode%>"/>
		</aui:column>
		<aui:column columnWidth="30">
			<label for="<portlet:namespace />denumire" style="padding-top: 5px">Denumire Articol</label>
		</aui:column>
		<aui:column columnWidth="70">
			<aui:input type="text" id="denumire" name="denumire" label ="" disabled="true" value="<%=articleInfo.get(0).get(\"denumire\").toString()%>"/>
		</aui:column>
		<aui:column columnWidth="30">
			<label for="<portlet:namespace />pret" style="padding-top: 5px">Pret</label>
		</aui:column>
		<aui:column columnWidth="70">
			<aui:input type="text" id="pret" name="pret" label ="" disabled="true" value="<%=articleInfo.get(0).get(\"pret\").toString()%>"/>
		</aui:column>
		<aui:column columnWidth="30">
			<label for="<portlet:namespace />furnizor" style="padding-top: 5px">Furnizor</label>
		</aui:column>
		<aui:column columnWidth="70">
			<aui:input type="text" id="furnizor" name="furnizor" label ="" disabled="true" value="<%=articleInfo.get(0).get(\"furnizor\").toString()%>"/>
		</aui:column>
		
		<aui:column last="true">
			<a class="btn btn-primary-red" href="<%= backURL %>">Inapoi</a>
			<aui:button value="Salveaza" cssClass="btn btn-primary" style="margin:0px" type="submit"/>
		</aui:column>
		<aui:input type="hidden" id="product_code" name = "product_code" value="<%=articleCode%>" />
	</aui:form>
</aui:layout>

<script type="text/javascript">
	//set chosen plugin on dropdown
	$('#<portlet:namespace/>product_id').chosen();
</script>