<%@page import="java.util.HashMap"%>
<%@page import="java.util.List"%>
<%@page import="com.profluo.ecm.model.db.DefCompanies"%>
<%@taglib uri="http://java.sun.com/portlet_2_0" 	prefix="portlet"%>
<%@taglib uri="http://liferay.com/tld/theme" 		prefix="liferay-theme"%>
<%@taglib uri="http://liferay.com/tld/ui" 			prefix="liferay-ui"%>
<%@taglib uri="http://alloy.liferay.com/tld/aui" 	prefix="aui"%>
<%@taglib uri="http://liferay.com/tld/util" 		prefix="liferay-util"%>

<liferay-theme:defineObjects />
<portlet:defineObjects />

<portlet:resourceURL id="ajaxURL" var="ajaxURL"/>
<portlet:actionURL name="exportExcel" var="exportURL" />

<aui:layout>
	<aui:form id="exportDeconturiKontan" name="exportDeconturiKontan" action="<%=exportURL.toString()%>">
		<aui:column>
			<input class="btn btn-primary" type="submit" value="Export Deconturi"/>
		</aui:column>
	</aui:form>
</aui:layout>
