<%@page import="com.profluo.ecm.model.db.VoucherHeader"%>
<%@taglib uri="http://java.sun.com/portlet_2_0" 	prefix="portlet"%>
<%@taglib uri="http://liferay.com/tld/theme" 		prefix="liferay-theme"%>
<%@taglib uri="http://liferay.com/tld/ui" 			prefix="liferay-ui"%>
<%@taglib uri="http://alloy.liferay.com/tld/aui" 	prefix="aui"%>
<%@taglib uri="http://liferay.com/tld/util" 		prefix="liferay-util"%>

<%@page import="java.util.HashMap"%>
<%@page import="java.util.List"%>
<%@page import="com.profluo.ecm.model.vo.DefCompaniesVo"%>

<liferay-theme:defineObjects />
<portlet:defineObjects />

<portlet:resourceURL id="ajaxURL" var="ajaxURL"/>
<portlet:renderURL var="inregistrareVoucherURL" >
	<portlet:param name="mvcPath" value="/html/vouchers/inreg_voucher.jsp"></portlet:param>
</portlet:renderURL>

<% // PAGINATION Start - env setup 
int start = 0; 
int count = 10;
int stage = 1;
int total = VoucherHeader.getAllFilteredCount(start, count, 0, "", "", stage);
%>

<script type="text/javascript">
var start = "<%=start%>";
var count = "<%=count%>";
var total = "<%=total%>";
var stage = "<%=stage%>";
//filters
var companyId = 0;
var voucherNo = "";
var storeId = 0;
var voucherDate = "";
</script>

<% 
	// get companies for dropdown list
	List<HashMap<String,Object>> allCompanies = DefCompaniesVo.getInstance();
	
	// retrieve 1st page of vouchers
	List<HashMap<String,Object>> allVouchers = VoucherHeader.getAllFiltered(start, count, 0, "", "", stage);
%>	

<liferay-ui:success key="voucherSaved" message="Bonul de comanda a fost salvat cu succes si urmeaza sa fie trimis catre Optimal!" />

<aui:layout>
	<aui:column first="true" columnWidth="100" last="true">
		<div id="id_bon_cesiune"></div>
	</aui:column>
</aui:layout>

<%-- HTML for PAGINATION --%>
<%@ include file="/html/includes/pagination.jsp" %>

<div id="data_filter" style="display:none">
	<aui:input id="data_bon_header" name="data_bon_header" label="" placeholder="Data bon" style="margin:0"></aui:input>
</div>

<script type="text/javascript">
var inreg_voucher = "<%=inregistrareVoucherURL.toString()%>";

function showLineDetails(id) {
	return false;
}

YUI({ lang: 'ro' }).use('aui-datatable','aui-datatype','datatable-sort', 'aui-datepicker', 'aui-base', 'liferay-portlet-url', 'aui-node',
	function(Y){
		var remoteData = [
			<% for (int i = 0; i < allVouchers.size(); i++) { %>
				{
					id: <%=allVouchers.get(i).get("id")%>,
					store_code : <%=allVouchers.get(i).get("store_code")%>,
					voucher_no: '<%=allVouchers.get(i).get("voucher_no")%>', 
					voucher_date : '<%=allVouchers.get(i).get("voucher_date")%>',
					company_name: '<%=allVouchers.get(i).get("company_name")%>',
					store_name:'<%=allVouchers.get(i).get("store_name")%>',
					id_store: '<%=allVouchers.get(i).get("id_store")%>',
					val_no_vat_ron:'<%=allVouchers.get(i).get("val_no_vat_ron")%>',
					store_id:'<%=allVouchers.get(i).get("store_id")%>'
				}
				<% if (i != (allVouchers.size() - 1)) { out.print(","); } %>
			<% } %>
		];
		
		function formatCurrency(cell) {
		    format = {
		    	thousandsSeparator: ",",
		        decimalSeparator: ".",
		        decimalPlaces: noOfDecimalsToDisplay
		    };
		    return Y.DataType.Number.format(Number(cell.value), format);
		}
		
		var nestedColumns = [		
			{key: 'id', label: 'ID', className: 'hiddencol'}, 
			{key: 'store_code', className: 'hiddencol'},
			{key: 'voucher_no', label:'<input id="id_nr_bon" name="id_nr_bon" label="" placeholder="Nr." style="width:100%"/>',allowHTML:true,resizable:true},
	        {key: 'voucher_date', label:'<div id="header_data_factura"></div>', allowHTML:true},
	        {key: 'company_name', label: '<select id="filter-societate" name="filter-societate" label="" style="width:100px;margin:0">' +
				'<option selected="selected" value="0">Societate</option>' +
				<% for (int i = 0; i < allCompanies.size(); i++) { %>
				<% if (allCompanies.get(i).get("status").equals("A")) {%>
				<% out.println("'<option value=\"" + allCompanies.get(i).get("id") +"\">" + allCompanies.get(i).get("name") +"</option>' + "); %>
				<% } } %>
				'</select>',allowHTML:true},
	        {key:'store_name',label:'Magazin',width:200},
	        {key:'id_store', className:'hiddencol'},
	        {key:'store_id', className:'hiddencol'},
	        {key:'val_no_vat_ron', label:'Valoare', allowHTML: true, formatter: formatCurrency, className: 'currencyColCenter'},
	        {key:'actiuni', label:'Actiuni', className:'align-center', allowHTML: true, formatter: '<a href="#" id="row{id}" class="inregistrare">Inregistrare</a>'}
		];
	
		var dataTableIV = new Y.DataTable({
			columns:nestedColumns,
			data:remoteData,
			recordType:['id', 'voucher_no', 'voucher_date', 'company_name', 'store_name', 'val_no_vat_ron', 'actiuni', 'id_store','store_id'],

			editEvent:'click'
		}).render('#id_bon_cesiune');
		
		//dataTableIV.get('boundingBox').unselectable();
		
		function edit(select_line){
			var v=dataTableIV.data, msg='', template='';
			v.each(function(item,i){

				var data=item.getAttrs(['id', 'voucher_no', 'voucher_date', 'company_name', 'store_name', 'val_no_vat_ron', 'actiuni', 'id_store','store_id']);

				
				if (data.id == select_line) {
				 	$('#<portlet:namespace />id').val(data.id);
					$('#<portlet:namespace />nr_bon').val(data.voucher_no);
					$('#<portlet:namespace />data_bon').val(data.voucher_date);
					$('#<portlet:namespace />societate_v').val(data.company_name);

					$('#<portlet:namespace />magazin').val(data.store_id + " - " + data.store_name);
					$('#<portlet:namespace />valoare_ron').val(data.val_no_vat_ron);
					
					$('#<portlet:namespace />registerVoucher').submit();
				}
			});
		}
	
		dataTableIV.delegate('click', function (e) {
			e.preventDefault();
			var target = e.target.get('id');
			edit(target.replace('row',''));
		}, '.inregistrare', dataTableIV);
	
	
		
		// called whenever a change in date selection was performed
		var oldDateValue = $('#<portlet:namespace />data_bon_header').val();
		
		
		// PAGINATION
		// set status of navigation buttons on page load
		setNavigationButtonsState('<portlet:namespace />');
		$('.navigation').click(function(e) {
			var elementT 	= e.target;
			var elementId 	= elementT.id;
			var action 		= "";
			
			// determine which button was pressed and set the value of start accordingly
			if (elementId.indexOf("first") > -1)	{ action = "first"; start = 0; }
			if (elementId.indexOf("prev") > -1) 	{ action = "prev"; if (start - count <= 0) {start = 0} else { start -= count; } }
			if (elementId.indexOf("next") > -1) 	{ action = "next"; start = parseInt(start) + parseInt(count); }
			if (elementId.indexOf("last") > -1) 	{ action = "last"; start = parseInt(parseInt(total) / parseInt(count)) * count; if (start == total) { start -= count; } }
			
			makeAjaxCall(action);
		});
		
		function makeAjaxCall(action) {
			$.ajax({
				type: "POST",
				url: "<%=ajaxURL%>",
				data: 	"<portlet:namespace />action=" + action + 
						"&<portlet:namespace />start=" + start + 
						"&<portlet:namespace />count=" + count +
						"&<portlet:namespace />total=" + total +
						"&<portlet:namespace />stage=" + stage +
						"&<portlet:namespace />voucher_no=" + voucherNo +
						"&<portlet:namespace />voucher_date=" + voucherDate +
						"&<portlet:namespace />company_id=" + companyId,
				success: function(msg) {
					// get table data
					if (msg != ""){
						var jsonEntries = jQuery.parseJSON(msg);
						total = jsonEntries.total;
						dataTableIV.set('data', eval(jsonEntries.values));
					} else {
						dataTableIV.set('data', []);
					}
					
					// set status of navigation buttons on page load
					setNavigationButtonsState('<portlet:namespace />');
				}
			});
		}
		// PAGINATION
		
		// filtare dupa voucher no
		$('#id_nr_bon').keyup(function (e) {
		    if ($("#id_nr_bon").is(":focus") && (e.keyCode == 13)) {
				var elementT 	= e.target;
				var elementId 	= elementT.id;
				var action 		= "filter";
				// get selected value
				voucherNo = elementT.value;
				companyId = $('#filter-societate').val();
				voucherDate = oldDateValue;
				// reset start page 
				start = 0;
				// reset count
				total = 0;
				// make ajax call
				makeAjaxCall(action);				
		    }
		});
		
		// filter currency based on change
		$('#filter-societate').change(function(e) {
			var elementT 	= e.target;
			var elementId 	= elementT.id;
			var action 		= "filter";
			// get selected value
			companyId = elementT.value;
			voucherDate = oldDateValue;
			voucherNo = $('#id_nr_bon').val();
			// reset start page 
			start = 0;
			// reset count
			total = 0;
			// make ajax call
			makeAjaxCall(action);
		});
		
		function dateChanged(input) {
			console.log('date changed: ' + input.val());
			oldDateValue = input.val();
			// get selected value
			voucherDate = oldDateValue;

			companyId = $('#filter-societate').val();
			voucherNo = $('#id_nr_bon').val();
			// reset start page 
			start = 0;
			// reset count
			total = 0;
			action = "filter";
			// make ajax call
			makeAjaxCall(action);
		}
		
		var datepicker2 = new Y.DatePicker({
			trigger : '#<portlet:namespace />data_bon_header',
			mask : '%Y-%m-%d',
			calendar : {
				dateFormat : '%Y-%m-%d'
			},
			popover: {
				zIndex : 1,
		          toolbars: {
		            header: [[
		              {
		                icon:'icon-trash',
		                label: 'Reseteaza filtru',
		                on: {
		                  click: function() {
		                	  datepicker2.clearSelection();
		                  }
		                }
		              }]]
		          }
			},
			after: {
	            selectionChange: function(event) {
	            	if (oldDateValue != $('#<portlet:namespace />data_bon_header').val()) {
	            		oldDateValue = $('#<portlet:namespace />data_bon_header').val();
	            		dateChanged($('#<portlet:namespace />data_bon_header'));
	            	}
	            	$('#data_filter').html('');
	            }
	        }
		});
	});
</script>

<aui:form method="post" action="<%=inregistrareVoucherURL.toString() %>" name="registerVoucher" id="registerVoucher" style="margin:0;padding:0">
	<aui:input type="hidden" id="id" name="id"/>
	<aui:input type="hidden" id="nr_bon" name="nr_bon"/>
	<aui:input type="hidden" id="data_bon" name="data_bon"/>
	<aui:input type="hidden" id="societate_v" name="societate_v"/>
	<aui:input type="hidden" id="magazin" name="magazin"/>
	<aui:input type="hidden" id="valoare_ron" name="valoare_ron"/>
</aui:form>