<%@page import="com.profluo.ecm.model.vo.DefDpis"%>
<%@ page import="com.profluo.ecm.model.vo.DefIFRSVo"%>
<%@ page import="com.profluo.ecm.model.vo.DefLotVo"%>
<%@ page import="com.profluo.ecm.model.vo.DefProductVo"%>
<%@ page import="com.profluo.ecm.model.vo.DefIASVo"%>
<%@ page import="com.profluo.ecm.model.db.DefIAS"%>
<%@ page import="com.profluo.ecm.model.vo.DefUMVo"%>
<%@ page import="com.profluo.ecm.model.vo.DefStoreVo"%>
<%@ page import="com.profluo.ecm.model.vo.DefRegEquipVo"%>
<%@ page import="com.profluo.ecm.model.vo.DefVATVo"%>
<%@ page import="com.profluo.ecm.model.vo.DefMFActVo"%>
<%@ page import="java.util.HashMap"%>
<%@ page import="java.util.List"%>

<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme"%>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui"%>
<%@ taglib uri="http://alloy.liferay.com/tld/aui" prefix="aui"%>
<%@ taglib uri="http://liferay.com/tld/util" prefix="liferay-util"%>

<liferay-theme:defineObjects />
<portlet:defineObjects />

<%
// retrieve all mf actions
List<HashMap<String,Object>> allMF = DefMFActVo.getInstance();
// retrieve all VAT values
List<HashMap<String,Object>> allVAT = DefVATVo.getInstance();
// tipuri inregistrare echipament
List<HashMap<String,Object>> allRegEquip = DefRegEquipVo.getInstance();
// get all stores
List<HashMap<String,Object>> allStores = DefStoreVo.getInstance();
// get all units of measure VO
List<HashMap<String,Object>> allUM = DefUMVo.getInstance();
// def all ias
List<HashMap<String,Object>> allIAS = DefIASVo.getInstance();
// def all ifrs
List<HashMap<String,Object>> allIFRS = DefIFRSVo.getInstance();
// def all prods
List<HashMap<String,Object>> allProds = DefProductVo.getInstance();
// def all lots
List<HashMap<String,Object>> allLots = DefLotVo.getInstance(); 
%>

<portlet:resourceURL id="ajaxURL" var="ajaxURL"/>

<legend><strong>Articole</strong></legend>
<%-- Lines table --%>
<aui:layout>
	<aui:column first="true" columnWidth="100" last="true">
		<div id="alocare-articole"></div>
	</aui:column>
	<aui:column last = "true">
		<a href="#" class="add_all btn btn-primary">Aloca toate articolele</a>
	</aui:column>
</aui:layout>
<aui:layout>
	<aui:column>
		<a href="#" class="add_article_line" onclick="javascript:;return false;">Adauga articol +</a>
	</aui:column>
</aui:layout>
<%-- Distributed lines --%>
<legend><strong>Alocare articol pe linii</strong></legend>
<aui:layout>
	<aui:column first="true" columnWidth="100" last="true">
		<div id="alocare-linii"></div>
	</aui:column>
</aui:layout>

<aui:layout>
	<aui:column cssClass="currencyCol">
		<label for="<portlet:namespace />val_no_vat_aloc_linii"
			style="padding-top: 5px"><liferay-ui:message key="val_no_vat_aloc_linii"/></label>
	</aui:column>
	<aui:column>
		<aui:input cssClass="currencyCol" name="valoare_totala" id="valoare_totala"
			value="" label="" readonly="readonly" />
	</aui:column>
	<aui:column>
		<label for="<portlet:namespace />difference" style="padding-top: 5px"><liferay-ui:message key="difference"/></label>
	</aui:column>
	<aui:column>
		<aui:input cssClass="currencyCol" name="difference" id="difference" value="" label=""
			readonly="readonly">
			<aui:validator name="custom" errorMessage="incorect">
				function (val, fieldNode, ruleValue) {
					var result = false;
					if (val == 0) {
						result = true;
					}
					return result;
				}
			</aui:validator>
		</aui:input>
	</aui:column>
</aui:layout>

<div id="overlay-detalii-inv" class="yui3-overlay-loading" style="left:-350px;position:absolute;width: 280px;z-index: 1">
    <div class="yui3-widget-hd" style="font-size: 12px">
    	Detalii inventar pentru articolul: <span id="cod_art_header" style="display:none">&nbsp;</span> <span id="den_art_header">&nbsp;</span>
    </div>
    <div class="yui3-widget-bd">
		<aui:layout>
			<aui:column first="true" columnWidth="100" last="true" style="position:relative">
				<div id="detalii-inventar"></div>
			</aui:column>
		</aui:layout>
		<aui:layout>
			<aui:column columnWidth="50" first="true">
				<aui:input value="Inchide" name="fcancel" id="finvcancel" type="button" label="" cssClass="btn btn-primary-red fl"/>
			</aui:column>
			<aui:column columnWidth="50" last="true">
				<aui:input id="salveaza-inv" name="salveaza-inv" value="Salveaza" type="button" label="" cssClass="btn btn-primary fr"/>
			</aui:column>
		</aui:layout>
    </div>
</div>
<%-- overlay solicita vizualizare DPI --%>
<div id="vizualizareDPI" class="yui3-overlay-loading"
	style="right: 100px; z-index: 1; position: absolute; width: 800px; display: none">
	<div class="yui3-widget-hd">Solicitare vizualizare DPI</div>
	<div class="yui3-widget-bd">
		<aui:layout>
			<aui:fieldset>
				<aui:column columnWidth="20">
					<label for="<portlet:namespace />nr_dpi" style="padding-top: 5px">Nr
						DPI: </label>
				</aui:column>
				<aui:column columnWidth="30">
					<aui:input id="nr_dpi" name="nr_dpi" label="" readonly="true"></aui:input>
				</aui:column>
				<aui:column columnWidth="20">
					<label for="<portlet:namespace />societate"
						style="padding-top: 5px">Societate:</label>
				</aui:column>
				<aui:column columnWidth="30">
					<aui:input id="societate" name="societate" label="" readonly="true"></aui:input>
				</aui:column>
			</aui:fieldset>

			<aui:fieldset>
				<aui:column columnWidth="20">
					<label for="<portlet:namespace />data_dpi" style="padding-top: 5px">Data
						DPI: </label>
				</aui:column>
				<aui:column columnWidth="30">
					<aui:input id="data_dpi" name="data_dpi" label="" readonly="true"></aui:input>
				</aui:column>
				<aui:column columnWidth="20">
					<label for="<portlet:namespace />categ_capex_dpi"
						style="padding-top: 5px">Categorie Capex:</label>
				</aui:column>
				<aui:column columnWidth="30">
					<aui:input id="categ_capex_dpi" name="categ_capex_dpi" label=""
						readonly="true"></aui:input>
				</aui:column>
			</aui:fieldset>

			<aui:fieldset>
				<aui:column columnWidth="20">
					<label for="<portlet:namespace />solicitant"
						style="padding-top: 5px">Solicitant: </label>
				</aui:column>
				<aui:column columnWidth="30">
					<aui:input id="solicitant" name="solicitant" label=""
						readonly="true"></aui:input>
				</aui:column>
			</aui:fieldset>

			<aui:fieldset>
				<aui:column columnWidth="20">
					<label for="<portlet:namespace />aprobator"
						style="padding-top: 5px">Aprobator: </label>
				</aui:column>
				<aui:column columnWidth="80">
					<aui:input id="aprobator" name="aprobator" label="" readonly="true"
						style="width: 97.6%"></aui:input>
				</aui:column>
			</aui:fieldset>
			<aui:layout>
				<aui:column columnWidth="100" last="true" first="true">
					<div id="tabelDPI"></div>
				</aui:column>
			</aui:layout>
			<aui:layout>
				<aui:column columnWidth="50" last="true" style="text-align:right">
					<aui:input value="Inchide" name="fcancel" type="button" label=""
						cssClass="btn btn-primary-red fr"
						onclick="$('#vizualizareDPI').hide()"></aui:input>
					<div class="clear"></div>
				</aui:column>
			</aui:layout>
		</aui:layout>
	</div>
</div>
<script type="text/javascript">
// global variable for tables in order to be able use them in other scripts
var dataTable, dataTableSplit;
// global variables used to save inventory numbers on input type hidden
var idLine, denArt, codArt, tipInv;

YUI({lang: 'ro'}).use( 'aui-datatable', 'aui-datatype', 'datatable-sort', 'overlay', 'datatable-scroll', 'aui-base', 'liferay-portlet-url', 'aui-node','datatable-mutable',
	function(Y) {
		/***********************************/
		/**	   GENERIC TABLE METHODS	  **/
		/***********************************/
		/** GENERIC CURRENCY FORMATTER **/
		function formatCurrency(cell) {
		    format = {
		    	thousandsSeparator: ",",
		        decimalSeparator: ".",
		        decimalPlaces: noOfDecimalsToDisplay
		    };
		    return Y.DataType.Number.format(Number(cell.value), format);
		}
		
		/** GENERIC SAVE ON ENTER **/
		function saveOnEnter(e) {
			if (e.domEvent.charCode == 13) {
				if (!e.target.validator.hasErrors()) {
					e.target.fire('save', {
		                newVal: e.target.getValue(),
		                prevVal: e.target.get('value')
		            });
				}
			}
		}
		
		/** GENERIC SAVE ON CHANGE ON SELECT **/
		function saveOnChange(e) {
			e.target.fire('save', {
                newVal: e.target.getValue(),
                prevVal: e.target.get('value')
            });
		}
		
		// editor attributes, actions and formats based on datatypes
		var attrNumberEditor = {inputFormatter: Y.DataType.Number.evaluate, validator: { rules: { value: { number: true } } }, on : {keyup: saveOnEnter}, showToolbar: false };
		var attrStringEditor = {on :{keyup: saveOnEnter}, showToolbar: false};
		
		/***********************************/
		/**	   Definitions arrays	      **/
		/***********************************/
		var allDPIs = <%=DefDpis.getJsonStringDpis(true)%>;
		var UMList = [<% for (int i = 0; i < allUM.size(); i++) { %>
						<% if (allUM.get(i).get("status").equals("A")) { %>
							<% if (allUM.size() - 1 == i) { %>
								'<%=allUM.get(i).get("ref")%>'
							<% } else { %>
								'<%=allUM.get(i).get("ref")%>',
							<% } %>
						<% } %>
					<% } %>];
		var iasList = <%=DefIASVo.getJsonStringIAS()%>;
		var ifrsList = <%=DefIFRSVo.getJsonStringIFRS()%>;
		var allProds = <%=DefProductVo.getJsonStringProduct()%>;
		var allLots = <%=DefLotVo.getJsonStringLots()%>;
		var clasif = {1:'IT Hardware', 2:'IT Software', 0:'Active.'};
		var tvalist = {<% for (int i = 0; i < allVAT.size(); i++) { %>
							<% if (allVAT.get(i).get("status").equals("A")) { %>
								<% if (allVAT.size() - 1 == i) { %>
									'<%=allVAT.get(i).get("ref")%>':'<%=allVAT.get(i).get("ref")%>%'
								<% } else { %>
									'<%=allVAT.get(i).get("ref")%>':'<%=allVAT.get(i).get("ref")%>%',
								<% } %>
							<% } %>
						<% } %>};
		var tipinventar = {1: "individual", 2: "de grup"};
		var tipinregistrare = <%=DefRegEquipVo.getJsonStringRegEquip()%>;
		var actiunimf=<%=DefMFActVo.getJsonStringMfAct()%>;

//=============================================================================================================================================================
//													POP-UP INFO DPI LINES
//=============================================================================================================================================================			


		var datahDPI = [];
		
		var colsDPI = [
				{ key: 'id_prod', 				label:'Cod produs'},
				{ key: 'description', 			label:'Denumire produs'},
				{ key: 'unit_price_curr', 		label:'Pret Unitar'},
				{ key: 'quantity', 				label:'Cantitate'},
				{ key: 'total_with_vat_curr', 	label:'Valoare totala'},
				{ key: 'magazin', 				label:'Magazin'},
				{ key: 'clasificare_it', 		label:'Clasificare IT'}
		];
		
		dataTableDPI = new Y.DataTable({
			columns: colsDPI,
			data:datahDPI
		}).render('#tabelDPI');	
		
		// data array - main table
		var remoteData = [];
		
		// COLUMN INFO and ATTRIBUTES
		var nestedCols = [
			{ key: 'delete_line', label: ' ', allowHTML: true, 
				// gets the id of the row and creates an unique link for every row
				formatter: function(o) {
					return '<a href="#" id="row' + o.value + '" class="deleteLineFromArrayVouchers"><img src="${themeDisplay.getPathThemeImages()}/delete.png" width="20"/></a>';
				}
			},
		    { key: 'id_voucher', 	className: 'hiddencol'},
			{ key: 'line', 			label: 'Nr.' },
			{ key: 'article_code',  className: 'hiddencol'},
			{ key: 'product_code', 		label: 'Cod Prod', 		
				editor: new Y.DropDownCellEditor({options: allProds, id: 'allprods', elementName: 'allprods',
					after: {
			            focus: function(event) {
				            $("select[name=allprods]").chosen();
			            }
					}
				}),
				emptyCellValue: '',
				formatter: function(o) {
					var obj = JSON.parse(JSON.stringify(allProds));
					
					if (typeof o.value != 'undefined') {
						return obj[o.value].toString();
					} else {
						return '';
					}
				}
			},
			{ key: 'description',		label: 'Den. articol.', editor: new Y.TextCellEditor(attrStringEditor)},
			{ key: 'um', 				label: 'UM', 			editor: new Y.DropDownCellEditor({options: UMList, on : {change: saveOnChange}, showToolbar: false})},
			{ key: 'quantity', 			label: 'Cant',	 		editor: new Y.TextCellEditor(attrNumberEditor )},
			{ key: 'unit_price_ron', 	label: 'Pret unitar', 	editor: new Y.TextCellEditor(attrNumberEditor), formatter: formatCurrency},
			{ key: 'price_no_vat_ron', 	label: 'Val fara TVA', 	formatter: formatCurrency, className: 'currencyCol' /*editor: new Y.TextCellEditor(attrNumberEditor)*/},
			{ key: 'price_vat_ron', className: 'hiddencol'},
			{ key: 'vat', 			label: 'Cota TVA', 		editor: new Y.DropDownCellEditor({options: tvalist, on : {change: saveOnChange}, showToolbar: false}),
				//formatter:	function(o) { return o.value + '%'; }
				formatter: function(o) {
					var obj = JSON.parse(JSON.stringify(tvalist));
					
					if (typeof o.value != 'undefined') {
						return obj[o.value].toString();
					} else {
						return '';
					}
				}
			},
			{ key: 'price_with_vat_ron', 	label: 'Val cu TVA', formatter: formatCurrency, className: 'currencyCol' 	/*editor: new Y.TextCellEditor(attrNumberEditor), formatter: formatCurrency*/},			
			{ key: 'ras', 	label: 'Grupa IAS', 	
				editor: new Y.DropDownCellEditor(
   						{options: iasList, id: 'allias', elementName: 'allias',
							after: {
			            focus: function(event) {
				            $("select[name=allias]").chosen();
			           	 		}
							}
   						}),
				emptyCellValue: '',
				formatter: function(o) {
					var obj = JSON.parse(JSON.stringify(iasList));
					
					if (typeof o.value != 'undefined') {
						return obj[o.value].toString();
					} else {
						return '';
					}
				}
			},
			{ key: 'ifrs', 	label: 'Grupa IFRS', 	
				editor: new Y.DropDownCellEditor(
   						{options: ifrsList, id: 'allifrs', elementName: 'allifrs',
							after: {
			            focus: function(event) {
				            $("select[name=allifrs]").chosen();
			           	 		}
							}
   						}),
				emptyCellValue: '',
				formatter: function(o) {
					var obj = JSON.parse(JSON.stringify(ifrsList));
					
					if (typeof o.value != 'undefined') {
						return obj[o.value].toString();
					} else {
						return '';
					}
				}
			},
			{ key: 'dt_ras', 		label: 'Dt. RAS'},
			{ key: 'dt_ifrs', 		label: 'Dt. IFRS'},
			{ key: 'lot', 			label: 'Lot', 			editor: new Y.DropDownCellEditor({options: allLots, on : {change: saveOnChange}, showToolbar: false}),
				emptyCellValue: '',
				formatter: function(o) {
					var obj = JSON.parse(JSON.stringify(allLots));
					
					if (typeof o.value != 'undefined') {
						return obj[o.value].toString();
					} else {
						return '';
					}
				}
			},
			{ key: 'is_it', 		label: 'Clasif.', 		editor: new Y.DropDownCellEditor({options: clasif, on : {change: saveOnChange}, showToolbar: false}), 
				emptyCellValue: '',
				formatter: function(o) {
					var obj = JSON.parse(JSON.stringify(clasif));
					return obj[o.value].toString();
				}
			},
			{ key: 'detalii', 		label: 'Actiuni', allowHTML: true, 
				// gets the id of the row and creates an unique link for every row
				formatter: function(o) {
					return '<a id="row' + o.value + '" class="allocline">Aloc. art. pe linii</a>';
				}, 
				emptyCellValue: '<a id="row{value}" class="allocline">Aloc. art. pe linii</a>', 
			},
			{ key: 'cont_imob_in_fct', 	className: 'hiddencol'},
			{ key: 'cont_imob_in_curs', 	className: 'hiddencol'},
			{ key: 'cont_imob_avans', 	className: 'hiddencol'}
		];

		// TABLE INIT
		dataTable = new Y.DataTable({
		    columns: nestedCols,
		    data: remoteData,
		    editEvent: 'click',
		    recordType: ['id_voucher', 'delete_line', 'line', 'article_code', 'product_code', 'description', 
		                 'um', 'quantity', 'unit_price_ron', 'price_no_vat_ron', 'price_vat_ron', 'vat', 
		                 'price_with_vat_ron', 'lot', 'ras', 'ifrs', 'dt_ras', 'dt_ifrs', 
		                 'is_it', 'detalii', 'cont_imob_in_fct', 
		                 'cont_imob_in_curs', 'cont_imob_avans']
		}).render('#alocare-articole');
		
		//dataTable.get('boundingBox').unselectable();
		
		// Changes of info on the main table
		dataTable.after('record:change', function (e) { 
			var obj = e.target.changed;
			var existingData = e.target._state.data;
			if (typeof obj.product_code != 'undefined') {
				setProductValues(obj.product_code);
			}
			
			//there was a change on ias
			if (typeof obj.ras != 'undefined') {
				setIasDtValuesVouchers(obj.ras, '<portlet:namespace />', '<%=ajaxURL%>');
			}
			
			//there was a change on ifrs
			if (typeof obj.ifrs != 'undefined') {
				setIfrsDtValuesVouchers(obj.ifrs, '<portlet:namespace />', '<%=ajaxURL%>');
			}
			
			// there was a change on quantity
			if (typeof obj.quantity != 'undefined' || typeof obj.unit_price_ron != 'undefined') {
				// if both are not empty make changes to all related fields otherwise do nothing
				if (typeof existingData.quantity.value != '' && typeof existingData.unit_price_ron.value != '') {
					setLineValuesVouchers(existingData.quantity.value, existingData.unit_price_ron.value, '<portlet:namespace/>');
				}
			}
		});
		
		
		//click on aloca toate articolele
		$('.add_all').click(function (e) {
			e.preventDefault();
			allocateThemAll('<portlet:namespace/>');
		});
		
		
		// click on alocare linie
		dataTable.delegate('click', function (e) {
			//console.log('aloc lines');
   		    var target = e.target.get('id');
   		    createNewStoreLineVouchers(target.replace('row',''));
   		}, '.allocline', dataTable);

		// bottom table values
		var remoteDataSplit = [];

		// bottom table header
		var nestedColsSplit = [
			{ key: 'delete_line', label: ' ', allowHTML: true, 
				// gets the id of the row and creates an unique link for every row
				formatter: function(o) {
					return '<a href="#" id="row' + o.value + '" class="deleteStoreLineFromArrayVouchers"><img src="${themeDisplay.getPathThemeImages()}/delete.png" width="20"/></a>';
				}
			},
		    { key: 'line', className: 'hiddencol'},
		   	{ key: 'description', label: 'Den. articol.'},
		   	{ key: 'article_code', className: 'hiddencol'},
			{ key: 'quantity', label:  'Cant'},
			{ key: 'price_no_vat_ron', label: 'Val fara TVA', formatter: formatCurrency}, 
			/*{ key: 'price_vat_ron', className: 'hiddencol'},*/
			{ key: 'associated_acc', label: 'Cont', editor: new Y.TextCellEditor(attrNumberEditor)}, 
			{ key: 'id_tip_inreg', label: 'Tip inregistrare', 
				editor: new Y.DropDownCellEditor({options: tipinregistrare, on : {change: saveOnChange}, showToolbar: false}),
				emptyCellValue: '1',
				formatter: function(o) {
					var obj = JSON.parse(JSON.stringify(tipinregistrare));
					return obj[o.value].toString();
				}
			},
			{ key: 'id_act_mf', label: 'Actiune MF', 
				editor: new Y.DropDownCellEditor({options: actiunimf, on : {change: saveOnChange}, showToolbar: false}),
				emptyCellValue: '1',
				formatter: function(o) {
					var obj = JSON.parse(JSON.stringify(actiunimf));
					return obj[o.value].toString();
				}
			},
			{ key: 'id_dpi', label: 'Nr. DPI', editor: new Y.DropDownCellEditor({options: allDPIs, id: 'alldpis', elementName: 'alldpis',
   				after: {
   		            focus: function(event) {
   			            $("select[name=alldpis]").chosen();
   		            }
   				}
   			}), formatter: function(o) {
	   				var obj = JSON.parse(JSON.stringify(allDPIs));
	   				
	   				if (typeof o.value != 'undefined') {
	   					return obj[o.value].toString();
	   				} else {
	   					return '0';
	   				}
   				}
   			},
   			{ key: 'vizualizare_dpi', label: 'Link DPI', 
   					allowHTML: true, 
   					formatter: function(o) { 
   						return '<a href="#" class="dpilink" id="dpi_' + o.data.line + '_' + o.value + '">DPI</a>';
   					},
   					emptyCellValue: '<a href="#" class="dpilink" id="dpi_{line}_{value}">DPI</a>'},
			{ key: 'id_tip_inventar', label: 'Tip nr. inventar', 
				editor: new Y.DropDownCellEditor({options: tipinventar, on : {change: saveOnChange}, showToolbar: false}),
				emptyCellValue: '2',  // de grup - default
				formatter: function(o) {
					var obj = JSON.parse(JSON.stringify(tipinventar));
					return obj[o.value].toString();
				}
			},
			{ key: 'detalii', label: 'Nr. inventar', allowHTML: true, 
				// gets the id of the row and creates an unique link for every row
				// formatter: '<a href="#" id="line-{nr}" class="proddet">detalii</a>', 
				formatter: function(o) {
					return '<a href="#" id="line-' + o.value + '" class="proddet">detalii</a>';
				}, 
				emptyCellValue: '<a href="#" id="line-{line}" class="proddet">detalii</a>', 
			},
			{ key: 'cont_imob_in_fct', 	className: 'hiddencol'},
			{ key: 'cont_imob_in_curs', className: 'hiddencol'},
			{ key: 'cont_imob_avans', 	className: 'hiddencol'},
			{ key: 'inventory', 		className: 'hiddencol'},
		];
		
		// TABLE INIT - bottom
		dataTableSplit = new Y.DataTable({
		    columns: nestedColsSplit,
		    data: remoteDataSplit,
		    
		    editEvent: 'click',
		    recordType: ['delete_line', 'line', 'description', 'quantity', 'price_no_vat_ron', 'associated_acc', 'id_tip_inreg', 'id_act_mf', 
		                 'id_dpi', 'vizualizare_dpi', 'id_tip_inventar', 'detalii', 'cont_imob_in_fct', 'cont_imob_in_curs', 'cont_imob_avans']
		}).render('#alocare-linii');
		
		var invTypes = [];
		var invIndex = 0;
		//dataTableSplit.get('boundingBox').unselectable();
		// click for inventory number link
		dataTableSplit.delegate("click", function(e){
	        e.preventDefault();
			var line = 0;
	        var quantity = 0;
	        var iasId = 0;
	        var loadedInventory = "";
	        var majorare = "";
	        idLine = e.target.getAttribute("id").replace('line-', '');
	        
	        // loop through table
	        var ml = dataTableSplit.data, msg = '', template = '';
		    ml.each(function (item, i) {
		    	var dataS = item.getAttrs(['delete_line', 'line', 'description', 'article_code', 'quantity', 'price_no_vat_ron', 'associated_acc', 
		    	                           'id_tip_inreg', 'id_act_mf','id_dpi', 'vizualizare_dpi', 'id_tip_inventar', 'cont_imob_in_fct', 'cont_imob_in_curs', 
		    	                           'cont_imob_avans', 'detalii', 'inventory']);

		    	if (dataS.line == idLine) {
			    	tipInv = dataS.id_tip_inventar;
			    	denArt = dataS.description;
			    	codArt = dataS.line; // just an attribute to know the selected element
			    	quantity = dataS.quantity;
			    	line = dataS.line;
			    	loadedInventory = dataS.inventory;
			    	majorare = dataS.id_act_mf;
		    	}
		    });
	        
	        // prepare table, insert into remoteDataInv, add to dataTableInv   
			// remoteDataInv = [];
	        var wasChanged = false;
	        if (invTypes[invIndex-1] != tipInv) {
	        	invTypes[invIndex++] = tipInv;
	        	wasChanged = true;
	        }
	        // IF the inventory was already loaded on this line do not get a new sequence from the DB
	        if (loadedInventory != "" && !wasChanged) {
				// incarcare nr de inventar
		        var jsonEntries = jQuery.parseJSON(loadedInventory);
				// show pop-up and load record in pop-up
				dataTableInv.set('data', eval(jsonEntries));
				showInventoryOverlay(e);
	        } else {
	        	console.log("majorarea este : " + majorare);
	        	if(majorare == 1 ){
	        	// GET inventory SEQUENCE form DB, get IAS group for AJAX call
			    var ml1 = dataTable.data, msg = '', template = '';
			    
			    ml1.each(function (item, i) {
			        var data = item.getAttrs(['line', 'ras']);
	
			        if (data.line == line) {
			        	iasId = data.ras;
			        }
			    });
			    
			    if (loadedInventory != "" && tipInv == 2 && wasChanged){
			    	var invComponents = loadedInventory.split(',');
			    	loadedInventory = invComponents[0] + ',' + invComponents[1] + ',' + invComponents[2] + ']';
			    	// incarcare nr de inventar
			        var jsonEntries = jQuery.parseJSON(loadedInventory);
					// show pop-up and load record in pop-up
					dataTableInv.set('data', eval(jsonEntries));
					
					showInventoryOverlay(e);
					updateInventoryOverlay();
			    } else {
		        // perform AJAX call in order to get inventory number(s)
				$.ajax({
					type: "POST",
					url: '<%=ajaxURL%>',
					data: 	"<portlet:namespace />action=getInventoryNo" +
							"&<portlet:namespace />storeId=" + storeId +
							"&<portlet:namespace />iasId=" + iasId +
							"&<portlet:namespace />count=" + quantity +
							"&<portlet:namespace />tipInv=" + tipInv,
					success: function(msgjson) {
						// get table data
						if (msgjson != ""){
							// incarcare nr de inventar
					        var jsonEntries = jQuery.parseJSON(msgjson);
							// show pop-up and load record in pop-up
							dataTableInv.set('data', eval(jsonEntries));
							
							showInventoryOverlay(e);
							updateInventoryOverlay();
						} else {
							//alert('');
						}
					},
					error: function(msg) {
						alert('A aparut o problema in preluarea datelor de pe server. Va rugam incercati din nou!');
					}
				});
	        }
			 //daca e majorare
	        } else {
	        	emptyPopUpInventoryVouchers(dataTableInv);
    			setEntriesDataTableInvVouchers(quantity, tipInv, dataTableInv);
    			showInventoryOverlay(e, Y);
    			updateInventoryOverlay('<portlet:namespace />');
	        }
	        }
	    }, ".proddet", dataTableSplit);

		// listener event to add line for top table
		$('.add_article_line').click(function (e) {
			var elementT 	= e.target;
			var elementId 	= elementT.id;
			createNewLine();
		});
		

		/* set all line values based on quantity and price per unit */
		/*function setLineValues(quantity, pricePerUnit) {
			if (pricePerUnit != '' && quantity != '') {
				var rows = [];
			    var ml = dataTable.data, msg = '', template = '';
			    ml.each(function (item, i) {
			        var data = item.getAttrs(['line', 'article_code', 'product_code', 'description', 'um', 
			                                  'quantity', 'unit_price_ron', 'price_vat_ron', 'price_no_vat_ron', 'vat', 
			         		                 'price_with_vat_ron', 'ras', 'ifrs', 'dt_ras', 'dt_ifrs', 
			         		                 'lot', 'is_it', 'detalii', 'cont_imob_in_fct', 'cont_imob_in_curs', 'cont_imob_avans']);
					//TODO - perform ajax in order to retrieve data
			        if (data.quantity == quantity && data.unit_price_ron == pricePerUnit) {
			        	data.price_no_vat_ron = parseFloat(parseFloat(quantity) * parseFloat(pricePerUnit)).toFixed(4);
			        	data.price_with_vat_ron = parseFloat(parseFloat(quantity) * parseFloat(pricePerUnit) * parseFloat(data.vat) / 10).toFixed(4);
						data.price_vat_ron = data.price_with_vat_ron - data.price_no_vat_ron;
			        }
		        	// set value on matrix
		        	rows.push(data);
		        	// set value on table
				    dataTable.set('recordset', rows);
			    });
			}
		}*/
   		

   		/* set all product related values on a line */
   		function setProductValues(prodId) {
   			//console.log("Set prod values");
        	$.ajax({
				type: "POST",
				url: '<%=ajaxURL%>',
				data: 	"<portlet:namespace />action=getProdInfo" +
						"&<portlet:namespace />prodId=" + prodId,
				success: function(msgjson) {
					// get table data
					if (msgjson != ""){
			   			var rows = [];
					    var ml = dataTable.data, msg = '', template = '';
					    
					    ml.each(function (item, i) {
					        var data = item.getAttrs(['delete_line', 'id_voucher', 'line', 'article_code', 'product_code', 'description', 'um', 
					                                  'quantity', 'unit_price_ron', 'price_no_vat_ron', 'price_vat_ron', 'vat', 
					         		                  'price_with_vat_ron', 'ras', 'ifrs', 'dt_ras', 'dt_ifrs', 'lot', 
					         		                  'is_it', 'detalii', 'cont_imob_in_fct',
					         		                  'cont_imob_in_curs', 'cont_imob_avans']);

							//TODO - perform ajax in order to retrieve data
					        if (data.product_code == prodId) {
								var jsonEntries = jQuery.parseJSON(msgjson);
								var product = jsonEntries.product;
					        	data.dt_ifrs = product[0].ifrs_dt;
					        	data.dt_ras = product[0].ias_dt;
					        	data.ras = product[0].ias_id;
					        	data.ifrs = product[0].ifrs_id;
					        	data.is_it = product[0].flag_it;
					        	data.lot = product[0].lot_id;
					        	data.cont_imob_in_fct = product[0].cont_imob_in_fct;
					        	data.cont_imob_in_curs = product[0].cont_imob_in_curs;
					        	data.cont_imob_avans = product[0].cont_imob_avans;
					        }
					        
				        	rows.push(data);
				        	
						    dataTable.set('recordset', rows);
					    });
					} else {
						alert('Produsul ales nu exista in baza de date!');
					}
				},
				error: function(msg) {
					alert('A aparut o problema in preluarea datelor de pe server. Va rugam incercati din nou!');
				}
			});
   		}
   		
   		/* add new line in main table */
   		function createNewLine() {
			remoteData = dataTable.data.toArray();
			remoteData.push({
				delete_line : remoteData.length + 1,
				id_voucher: '<%=id %>',
				line: remoteData.length + 1,
				article_code: '0',
				product_code: '0',
				description: '',
				um: 'Buc.',
				quantity: '1',
				unit_price_ron : '',
				price_no_vat_ron : '',
				vat: $('#<portlet:namespace/>cota_tva').val(),
				price_with_vat_ron : '',
				ras: '0',
				ifrs: '0',
				dt_ras: '',
				dt_ifrs: '',
				lot: '',
				is_it: 0,
				detalii: remoteData.length + 1,
				cont_imob_in_fct : 0,
				cont_imob_in_curs : 0,
				cont_imob_avans : 0
			});
			dataTable.set('recordset', remoteData);
   		}
   		
		dataTableSplit.after('record:change', function (e) {
			var obj = e.target.changed;
			var existingData = e.target._state.data;
			//change associated account based on reg type and mf act
			if (typeof obj.id_act_mf != 'undefined' || typeof obj.id_tip_inreg != 'undefined') {
				setAssociatedAccountVouchers();
			}
			if (typeof obj.id_dpi != 'undefined') {
				setDPILinkVouchers(obj.id_dpi);
			}
		});
		
		// click on dpi link
   		dataTableSplit.delegate('click', function (e) {
   			e.preventDefault();
   		    var target = e.target.get('id');
   		    var ids = target.replace('dpi_','').split("_");
   		    var lineid = ids[0];
   		    var dpiId = ids[1];
   		 showDPIPopupVoucher(e.target, dpiId,'<portlet:namespace/>', '<%=ajaxURL%>', Y);
		}, '.dpilink', dataTableSplit);
		
		/***********************************/
		/**	   inventory table - START	  **/
		/***********************************/
		var remoteDataInv = []; 
		         		
		var nestedColsInv = [
		      			{ key: 'nr_inv', 	label: 'Nr. inv.', editor: new Y.TextCellEditor({on : {keyup: saveOnEnter}, showToolbar: false})},
		      			{ key: 'data_pif', 	label: 'Data PIF', editor: new Y.DateCellEditor({dateFormat: '%Y-%m-%d'})},
		      			{ key: 'receptie', 	label: 'Receptie', editor: new Y.TextCellEditor(attrStringEditor)}
		      			];
  		
  		// TABLE INIT
  		var dataTableInv = new Y.DataTable({
  		    columns: nestedColsInv,
  		    data: remoteDataInv,
  		    editEvent: 'click'
  		}).render('#detalii-inventar');
  		
  		//dataTableInv.get('boundingBox').unselectable();
  		
  		function showInventoryOverlay(e) {
			// end of prepare table
	        $('#den_art_header').text(denArt);
	        $('#cod_art_header').text(codArt);
			
	        // prepare overlay
	        var currentNode = this;
			var overlay = new Y.Overlay({
			    srcNode:"#overlay-detalii-inv",
			    width:"400px",
			    align: {
			        node: e.target,
			        points:["bl", "tl"]
			    }
			});
			
			overlay.render();

	        $('#overlay-detalii-inv').show();
	        
			// bind actions on pop-up
	  		$("#<portlet:namespace/>finvcancel").click(function () { saveInventoryOverlay(); });
	  		$("#<portlet:namespace/>salveaza-inv").click(function () { saveInventoryOverlay(); });
  		}
  		
  		function closeInventoryOverlay() {
  			$('#overlay-detalii-inv').hide();
  			return false;
  		}
  		
  		function updateInventoryOverlay() {
  			remoteDataSplit = [];
  			
	        // loop through table
	        var ml = dataTableSplit.data, msg = '', template = '';
	        var voucher_date = $('#<portlet:namespace />data_bon').val();
		    ml.each(function (item, i) {
		    	var dataS = item.getAttrs(['delete_line', 'line', 'description', 'article_code', 'quantity', 'price_no_vat_ron', 'associated_acc', 
		    	                           'id_tip_inreg', 'id_act_mf', 'id_dpi', 'vizualizare_dpi', 'id_tip_inventar', 'cont_imob_in_fct', 'cont_imob_in_curs', 
		    	                           'cont_imob_avans', 'detalii', 'inventory']);
		    	if (codArt == dataS.line) {
			    	dataS.inventory = JSON.stringify(dataTableInv.data.toJSON());
			    	//extract the pif date
			    	var pif = getPifDate(dataS.inventory);
			    	// TODO: nu pare corect...
			    	//if the pif date was set
			    	if (pif != ""){
			    		//if pif date > voucher date => reg type = "Obiect de inventar in curs"
			    		if (pif > voucher_date){
			    			dataS.id_tip_inreg = 1;
			    		} else if (pif < voucher_date){
			    			//else reg type = "Obiect de inventar in functiune"
			    			dataS.id_tip_inreg = 2;
			    		}
			    	}
		    	}
		    	remoteDataSplit.push(dataS);
		    }	);
		    
		    dataTableSplit.set('recordset', remoteDataSplit);
		    
  		}
  		
  		//extracts the pif date from dataTableInv.data
  		function getPifDate(row){
  			//position in the array
  			var pos = row.indexOf("data_pif");
  			//start position of the date
  			var start = parseInt(pos) + 12;
  			//end position of the date
  			var end = parseInt(start + 10); 
  			//the date
  			var pif_date = row.substring(start, end);
  			//if the date was set
  			if (pif_date.indexOf('"') < 0){
  				return pif_date;
  			} else {
  				//if the date was not set
  				return "";
  			}
  		}
  		
  		function saveInventoryOverlay() {
  			updateInventoryOverlay();
  			closeInventoryOverlay();
  		}
  		// inventory table - END
   		
		$(document).ready(function(){
			// add new line in the 1st table when the document is created
			createNewLine();
		});
		
		//DELETE STORE LINE FROM THE INTERFACE; THE STORE LINE IS NOT IN THE DATABASE
		dataTableSplit.delegate('click', function (e) {
			e.preventDefault();
			//get target
			var elem = e.currentTarget.get('id');
			//get the id of the target
  			var elementId	= elem.replace('row', '');
			//index of the row that is deleted
  			var indexToRemove = -1;
			//iterate the data to find the index of the row that is deleted
  			var ml = dataTableSplit.data, msg = '', template = '';
 		    ml.each(function (item, i) {
 		    	//console.log(item);
 		    	var dataS = item.getAttrs(['delete_line']);
 		    	if (dataS.delete_line == elementId){
 		    		indexToRemove = i;
 		    		return;
 		    	}
 		    });
 		    //remove row from locat array
 		    if (confirm("Sunteti sigur/a ca doriti sa stergeti aceasta linie?")){
  				dataTableSplit.removeRow(indexToRemove);
 		    }
		}, '.deleteStoreLineFromArrayVouchers', dataTableSplit);
		
		//DELETE LINE FROM THE INTERFACE; THE LINE IS NOT IN THE DATABASE
		dataTable.delegate('click', function (e) {
			e.preventDefault();
			//get target
			var elem = e.currentTarget.get('id');
			//get the id of the target
  			var elementId	= elem.replace('row', '');
  			//index of the row that is deleted
  			var indexToRemove = -1;
  			//number of the line that is deleted; store lines are matched with lines based on this number
  			var lineNumber = -1;
  			//iterate the line data to find the index of the row that is deleted
  			var ml = dataTable.data, msg = '', template = '';
 		    ml.each(function (item, i) {
 		    	var data = item.getAttrs(['delete_line', 'line']);
 		    	//find the index of the line that is to be deleted
 		    	if (elementId == data.delete_line){
 		    		//save the index of the line
 		    		indexToRemove = i;
 		    		//save the invoice line number
 		    		lineNumber = data.delete_line;
 		    		return;
 		    	}
 		    });
 		    //remove the line
 		    if (confirm ("Atentie! La stergerea unei linii de factura se vor sterge si toate alocarile pe magazine asociate acesteia." +
 		    		" Sunteti sigur/a ca doriti sa stergeti aceasta linie?")){
 		    	dataTable.removeRow(indexToRemove);
 		    	//delete all associated store lines
 	 		    deleteAssociatedStoreLinesVouchers(lineNumber);
 		    }
		}, '.deleteLineFromArrayVouchers', dataTable);
		
		//delete all the store lines associated with the invoice line that has the number = @param lineNumber
		function deleteAssociatedStoreLinesVouchers(lineNumber){
			//iterate the store line data to find the index of the row that is deleted
  			var mls = dataTableSplit.data, msg = '', template = '';
 		 	//array with all store line ids that must be deleted
 		 	var storeLines_id = 0;
 		    //remove all associated lines
 		   	mls.each(function (item, i) {
 		   		var dataS = item.getAttrs(['delete_line']);
 		   		//match all the store lines that are associated with the line and must be deleted 
 		   		if (dataS.delete_line == lineNumber) {
 					//remove the matched store line
 		   			storeLines_id = i;
 					return;
 		   		}
 		   	});
 		   	//remove the store line
 			dataTableSplit.removeRow(storeLines_id); 
		}		
	}
);
</script>