<%@page import="com.profluo.ecm.model.db.DefStore"%>
<%@page import="com.profluo.ecm.model.db.DefCompanies"%>
<%@page import="com.profluo.ecm.model.db.GenericDBStatements"%>
<%@ page import="com.profluo.ecm.model.db.VoucherHeader"%>
<%@ page import="com.profluo.ecm.model.vo.ListaInitVo" %>
<%@ page import="com.profluo.ecm.model.vo.DefNewProjVo" %>
<%@ page import="java.util.ResourceBundle"%>
<%@ page import="java.util.Locale"%>
<%@ page import="java.util.HashMap"%>
<%@ page import="java.util.List"%>
<%@page import="com.liferay.portal.util.PortalUtil"%>

<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme"%>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui"%>
<%@ taglib uri="http://alloy.liferay.com/tld/aui" prefix="aui"%>
<%@ taglib uri="http://liferay.com/tld/util" prefix="liferay-util"%>

<liferay-theme:defineObjects />
<portlet:defineObjects />

<%-- Define URLs --%>
<portlet:renderURL var="BackURL" >
	<portlet:param name="mvcPath" value="/html/vouchers/view.jsp"></portlet:param>
</portlet:renderURL>

<portlet:actionURL name="saveVoucher" var="submitURL" />
<portlet:resourceURL id="ajaxURL" var="ajaxURL"/>

<liferay-ui:error key="missingParameters" message="Va rugam sa completati toate campurile obligatorii!" />
<liferay-ui:error key="missingInitiative" message="Va rugam sa alegeti o initiativa din lista de initiative Capex!" />
<liferay-ui:error key="missingProject" message="Va rugam sa alegeti un proiect din lista de proiecte Capex!" />

<%-- Retrieve Usefull info --%>
<%
	locale = renderRequest.getLocale();
	ResourceBundle resmainIV = portletConfig.getResourceBundle(locale);
	// retrieve the id of the displayed voucher
	String id = "";
	String vat = "";
	String status = "";
	String tip_capex = "0";
	String initiativa = "0";
	String prj = "0";
	String storeId = "";
	try {
		id						= renderRequest.getParameter("id").toString();
	} catch (Exception e) {
		System.out.println("AICI EEEEEE");
	}
	
	
	if ( id.equals("") || id == null){
		String url = PortalUtil.getCurrentCompleteURL(request);
		String [] params = url.split("&");
		String [] lastParameter = params[params.length-1].split("=");
		if (lastParameter[0].equals("id")){
			id = lastParameter[1];
		}
	}
	
	System.out.println("ID = " + id);
	
	List<HashMap<String,Object>> voucherHeader = null;
	List<HashMap<String,Object>> aisles = null;
	if (!id.equals("")) {
		voucherHeader = VoucherHeader.getValuesById(Integer.parseInt(id), "voucher_header", "id");
		storeId = voucherHeader.get(0).get("id_store").toString();
		aisles = GenericDBStatements.getAllByFieldName("def_store_aisles", "id_store", storeId);
		Float valCuTVA = Float.parseFloat(voucherHeader.get(0).get("val_no_vat_ron").toString())
						+ Float.parseFloat(voucherHeader.get(0).get("vat_val_ron").toString());
		vat = String.format("%.2f", (float)(Math.round(100 * (valCuTVA / Float.parseFloat(voucherHeader.get(0).get("val_no_vat_ron").toString()))) - 100));
		
		tip_capex = voucherHeader.get(0).get("tip_capex").toString();
		initiativa = voucherHeader.get(0).get("id_initiative").toString();
		prj = voucherHeader.get(0).get("id_new_prj").toString();
	}
	
	String companyName = DefCompanies.getCompanyNameById(voucherHeader.get(0).get("id_company").toString());
	String storeName = DefStore.getStoreNameById(voucherHeader.get(0).get("id_store").toString());
%>
<% if (!id.equals("")) { %>
<script type="text/javascript">
var storeId = '<%=storeId%>';
</script>
<% } %>

<legend>Antet bon de cesiune</legend>
<aui:form action="<%=submitURL.toString()%>" method="post">
<aui:input type="hidden" id="id" name="id" value="<%=id %>"/>
<aui:input type="hidden" id="article_table" name="article_table" value=""/>
<aui:input type="hidden" id="article_table_lines" name="article_table_lines" value=""/>

<%-- ROW 1 --%>
<aui:layout>
	<aui:fieldset>
		<aui:column columnWidth="10">
			<label for="<portlet:namespace />nr_bon" style="padding-top: 5px"><%=resmainIV.getString("nr_bon")%>:
			</label>
		</aui:column>
		<aui:column columnWidth="20">
			<aui:input id="nr_bon" name="nr_bon" label="" value="<%=voucherHeader.get(0).get(\"voucher_no\") %>" readonly="true"></aui:input>
		</aui:column>
		<aui:column columnWidth="10">
			<label for="<portlet:namespace />data_bon" style="padding-top: 5px"><%=resmainIV.getString("data_bon")%>:
			</label>
		</aui:column>
		<aui:column columnWidth="20">
			<aui:input id="data_bon" name="data_bon" label="" value="<%=voucherHeader.get(0).get(\"voucher_date\") %>" readonly="true" placeholder="aaaa-ll-zz"></aui:input>
		</aui:column>
		<aui:column columnWidth="10">
			<label for="<portlet:namespace />cota_tva" style="padding-top: 5px"><%=resmainIV.getString("cota_tva")%>:</label>
		</aui:column>
		<aui:column columnWidth="20">
			<aui:input id="cota_tva" name="cota_tva" label="" value="<%=vat %>" readonly="true"></aui:input>
		</aui:column>
	</aui:fieldset>
	<%-- ROW 2 --%>
	<aui:fieldset>
		<aui:column columnWidth="10">
			<label for="<portlet:namespace />societate_v" style="padding-top: 5px"><%=resmainIV.getString("societate_v")%>:
			</label>
		</aui:column>
		<aui:column columnWidth="20">
			<aui:input id="societate_v" name="societate_v" value="<%=companyName%>" label="" readonly="true" ></aui:input>
		</aui:column>
		<aui:column columnWidth="10">
			<label for="<portlet:namespace />magazin" style="padding-top: 5px"><%=resmainIV.getString("magazin")%>:
			</label>
		</aui:column>
		<aui:column columnWidth="20">
			<aui:input id="magazin" name="magazin" value="<%=storeName%>" label=""  readonly="true"></aui:input>
		</aui:column>
		<aui:column columnWidth="10">
			<label for="<portlet:namespace />raion" style="padding-top: 5px"><%=resmainIV.getString("raion")%>:
			</label>
		</aui:column>
		<aui:column columnWidth="20">
			<%--
			<aui:input id="raion" name="raion" label="" readonly="true"></aui:input>
			--%>
			<aui:select id="raion" name="raion" label="" required="true">
				<aui:option value="">Raion</aui:option>
				<% for (int i = 0; i < aisles.size(); i++) { %>
					<aui:option value="<%=aisles.get(i).get(\"short_id\")%>"><%=aisles.get(i).get("short_id")%> - <%=aisles.get(i).get("name")%></aui:option>
				<% } %>
			</aui:select>
			<script type="text/javascript">
			$(document).ready(function() {
				$("select[name=<portlet:namespace />raion]").chosen();
			});
			</script>
		</aui:column>
	</aui:fieldset>
	<%-- ROW 3 --%>
	<aui:fieldset>
	<aui:column columnWidth="10">
			<label for="<portlet:namespace />valoare_ron" style="padding-top: 5px"><%=resmainIV.getString("valoare_ron")%>:</label>
		</aui:column>
		<aui:column columnWidth="20">
			<aui:input cssClass="currencyCol" id="valoare_ron" name="valoare_ron" label="" readonly="true" value="<%=voucherHeader.get(0).get(\"val_no_vat_ron\") %>"></aui:input>
		</aui:column>
		<aui:column columnWidth="10">
			<label for="<portlet:namespace />tva_ron" style="padding-top: 5px"><%=resmainIV.getString("tva_ron")%>:</label>
		</aui:column>
		<aui:column columnWidth="20">
			<aui:input cssClass="currencyCol" id="tva_ron" name="tva_ron" label="" readonly="true" value="<%=voucherHeader.get(0).get(\"vat_val_ron\") %>"></aui:input>
		</aui:column>
			<aui:column columnWidth="10">
			<label for="<portlet:namespace />val_cu_tva_ron" style="padding-top: 5px"><%=resmainIV.getString("val_cu_tva_ron")%>:
			</label>
		</aui:column>
		<aui:column columnWidth="20">
			<aui:input cssClass="currencyCol" id="val_cu_tva_ron" name="val_cu_tva_ron" label="" readonly="true" value="<%=(Float.parseFloat(voucherHeader.get(0).get(\"val_no_vat_ron\").toString()) 
						+ Float.parseFloat(voucherHeader.get(0).get(\"vat_val_ron\").toString())) %>"></aui:input>
			</aui:column>
	</aui:fieldset>
	<%-- ROW 4 --%>
	<aui:fieldset>
		<aui:column columnWidth="10">
			<label for="<portlet:namespace />cont_kontan" style="padding-top: 5px">Cont Kontan:</label>
		</aui:column>
		<aui:column columnWidth="20">
			<aui:input id="cont_kontan" name="cont_kontan" label="" readonly="true" value="<%=voucherHeader.get(0).get(\"kontan_acc\") %>"></aui:input>
		</aui:column>
	</aui:fieldset>
</aui:layout>

<%-- table ROW  --%>
<aui:layout>
	<%@include file="tabel_articole.jsp" %>
</aui:layout>
<script type="text/javascript">
		$(document).ready(function(){
			$('#<portlet:namespace />tva_ron').autoNumeric('init', {aSep: ',', mDec: 4, vMin : -999999999, vMax : 999999999 });
			$('#<portlet:namespace />val_cu_tva_ron').autoNumeric('init', {aSep: ',', mDec: 4, vMin : -999999999, vMax : 999999999 });
			$('#<portlet:namespace />valoare_ron').autoNumeric('init', {aSep: ',', mDec: 4, vMin : -999999999, vMax : 999999999 });
			$('#<portlet:namespace />valoare_totala').autoNumeric('init', {aSep: ',', mDec: 4, vMin : -999999999, vMax : 999999999 });
			$('#<portlet:namespace />difference').autoNumeric('init', {aSep: ',', mDec: 4, vMin : -999999999, vMax : 999999999 });
		});
</script>

<%-- detalii capex -> --%>
<aui:layout>
	<%@include file="../includes/capex_details.jsp" %>
</aui:layout>


<script type="text/javascript">
//autocomplete section "detalii capex"
	$(document).ready(function() {
		var tip_capex = <%=tip_capex%>;
		if (tip_capex == 0){
			$('#<portlet:namespace />capex_diverse').prop('checked', true);
		} else if (tip_capex == 1){
			$('#<portlet:namespace />init_cent').prop('checked', true);
			$('#<portlet:namespace />lista_initiative').val('<%=initiativa%>');
		} else {
			$('#<portlet:namespace />proiect_nou').prop('checked', true);
			$('#<portlet:namespace />project').val('<%=prj%>');
		}
	});
</script>
<aui:layout>
	<aui:column  first="true">
		<aui:button name="inapoi" cssClass="btn btn-primary-red" onClick ="<%=BackURL.toString()%>" value="<%=resmainIV.getString(\"back\")%>"/>
	</aui:column>
	<aui:column last="true">
		<aui:button type="button" id="validate_voucher_form" value="<%=resmainIV.getString(\"buton_validare\")%>"/>
		<aui:button type="submit" style="display:none" id="submit_voucher_form"></aui:button>
	</aui:column>
</aui:layout>

<script type="text/javascript">

YUI({lang: 'ro'}).use( 'aui-datatable', 'aui-datatype', 'datatable-sort', 'overlay', 'aui-form-validator', 'datatable-mutable',
		function(Y) {
			Y.on('click', function(e){
				e.preventDefault();
				if ( !checkCapexDetails('<portlet:namespace/>') ) {
					return false;
				}
				
				if ($('#<portlet:namespace/>difference').val() != 0 ){
					alert('Sumele nu corespund, va rugam sa verificati!');
					return false;
				}
				
				// check if there are rows in 2nd table
				var datarowssplit = dataTableSplit.data.toArray();
				var datarows = dataTable.data.toArray();
				if (datarowssplit.length == 0) {
					alert('Va rugam sa alocati articolele pe linii!');
					return false;
				}
				if (datarowssplit.length != datarows.length) {
					alert('Va rugam sa alocati toate articolele pe linii!');
					return false;
				}
				// get info from both table and send them on request
				$('#<portlet:namespace/>article_table').val(JSON.stringify(dataTable.data.toJSON()));
				$('#<portlet:namespace/>article_table_lines').val(JSON.stringify(dataTableSplit.data.toJSON()));
				
				//check if the associated account corresponds with the reg type
				var acc_regType = checkAccountRegType();
				if(acc_regType != 0){
					alert("In tabelul \"Alocare articol pe linii\", pe linia " + acc_regType +" contul asociat nu corespunde cu tipul de inregistrare!");
					return;
				}
				
				var flag = verifyPifDate('<portlet:namespace />');
				// submit form
				if (flag){
					$('#submit_voucher_form').click();
					// in case there is a required select, overwrite the error with a localized message
					$(".required").text("Acest camp este obligatoriu");
				}
				
			},'#validate_voucher_form');
});
/*  
$(document).ready(function() { 
	$('#validate_voucher_form').click(function() {
		
		 if ($('#<portlet:namespace/>difference').val() != 0 ){
			alert('Sumele nu corespund, va rugam sa verificati!');
			return false;
		}
		
		// check if there are rows in 2nd table
		var datarowssplit = dataTableSplit.data.toArray();
		var datarows = dataTable.data.toArray();
		if (datarowssplit.length == 0) {
			alert('Va rugam sa alocati articolele pe linii!');
			return false;
		}
		if (datarowssplit.length != datarows.length) {
			alert('Va rugam sa alocati toate articolele pe linii!');
			return false;
		}
		// get info from both table and send them on request
		$('#<portlet:namespace/>article_table').val(JSON.stringify(dataTable.data.toJSON()));
		$('#<portlet:namespace/>article_table_lines').val(JSON.stringify(dataTableSplit.data.toJSON()));
		
		//check if the associated account corresponds with the reg type
		var acc_regType = checkAccountRegType();
		if(acc_regType != 0){
			alert("In tabelul \"Alocare articol pe linii\", pe linia " + acc_regType +" contul asociat nu corespunde cu tipul de inregistrare!");
			return;
		}
		
		var flag = verifyPifDate('<portlet:namespace />');
		// submit form
		if (flag){
			$('#submit_voucher_form').click();
			// in case there is a required select, overwrite the error with a localized message
			$(".required").text("Acest camp este obligatoriu");
		}
	}); 
});*/
</script>
</aui:form>