<%@page import="java.util.HashMap"%>
<%@page import="java.util.List"%>
<%@page import="com.profluo.ecm.model.db.DefCompanies"%>
<%@taglib uri="http://java.sun.com/portlet_2_0" 	prefix="portlet"%>
<%@taglib uri="http://liferay.com/tld/theme" 		prefix="liferay-theme"%>
<%@taglib uri="http://liferay.com/tld/ui" 			prefix="liferay-ui"%>
<%@taglib uri="http://alloy.liferay.com/tld/aui" 	prefix="aui"%>
<%@taglib uri="http://liferay.com/tld/util" 		prefix="liferay-util"%>

<liferay-theme:defineObjects />
<portlet:defineObjects />

<%
	List<HashMap<String,Object>> allCompanies = DefCompanies.getAll();
%>

<portlet:resourceURL id="ajaxURL" var="ajaxURL"/>

<%-- <portlet:actionURL name="exportExcel" var="exportURL" /> --%>
<portlet:resourceURL id="exportAction" var="exportURL">
	<portlet:param name="action" value="exportExcel"/>
</portlet:resourceURL>

<aui:layout>
	<aui:form id="exportForm" name="exportForm" action="<%=exportURL.toString()%>">
		<aui:column>
			<label for="<portlet:namespace />company" style="padding-top: 5px">Societatea</label>
		</aui:column>
		<aui:column>
			<aui:select id="company" name="company" label="">
				<aui:option selected="selected" value="0"/>
				<% for (int i = 0 ; i < allCompanies.size(); i++) { %>
					<aui:option value="<%=i+1%>"> <%=allCompanies.get(i).get("name").toString()%> </aui:option>
				<% } %>
			</aui:select>
		</aui:column>
		<aui:column style="display:none">
			<aui:input type="text" name="export_type" id="export_type"/>
		</aui:column>
		<aui:column>
			<a class="btn btn-primary" onclick="exportDocumente('<portlet:namespace />')"> Export Documente </a>
		</aui:column>
	</aui:form>
</aui:layout>

<script type="text/javascript">
function exportDocumente(portletId){
	var company = $('#' + portletId + 'company').val();
	if ( !company || company == 0){
		alert("Va rugam sa selectati societatea.");
		return;
	}
	$('#'+portletId+'export_type').val("exportDocumente");
	$('#'+portletId+'exportForm').submit();
}
</script>