var noOfDecimals = 4;
var noOfDecimalsToDisplay = 4;

//regex for global substitution
var find = ',';
var toReplace = new RegExp(find, 'g');

function alertDelete(namespace){
	if(confirm("ATENTIE!!! Aceasta operatie va duce la stergerea definitiva a facturii. Continuati?")) {
		$('#'+namespace+'sterge').click();
	}	
}

function makeAjaxToSetInProcessFlag(action, ajaxURL, portletNamespace, invoiceId, value){
	$.ajax({
		type: "POST",
		url: ajaxURL,
		data: 	portletNamespace + "action=" + action + 
				"&" + portletNamespace + "invoiceId=" + invoiceId +
				"&" + portletNamespace + "value=" + value,
		success: function(msg) {
			if(value == 1 ) {
				$('#' + portletNamespace + 'registerInvoice').submit();
			} else {
				$('#go_inapoi').click();
			}
		}
	});
}

function checkInvoiceAccess(action, ajaxURL, portletNamespace, invoiceId, userId){
	
	$.ajax({
		type: "POST",
		url: ajaxURL,
		data: 	portletNamespace + "action=" + action + 
				"&" + portletNamespace + "invoiceId=" + invoiceId +
				"&" + portletNamespace + "userId=" + userId,
		success: function(msg) {
			debugger;
			return msg;
		}
	});
}

function makeAjaxToPopulateDropdown(action, company, supplierName, ajaxURL, portletNamespace){
	$.ajax({
		type: "POST",
		url: ajaxURL,
		data: 	portletNamespace + "action=" + action + 
				"&" + portletNamespace + "company=" + company + 
				"&" + portletNamespace + "supplierName=" + supplierName,
		success: function(msg) {
			// get table data
			if (msg != ""){
				var jsonEntries = jQuery.parseJSON(msg);
				lista = jsonEntries.list;
				var dropDown = $('#' + portletNamespace + 'nr-fact-avans-asoc');
				dropDown.empty();
				dropDown.append('<option value="">-</option>');
				if (dropDown) {
						//append all the options resulted from the ajax call
						for ( i = 0; i < lista.length; i++){
							dropDown.append('<option value=' + lista[i].id + '>' + lista[i].id + '</option>');
						};
					}
				var listaMagazine = jsonEntries.storesDropdown;
				var newStoresDropdown = $('#' + portletNamespace + 'alocare_magazin');
				newStoresDropdown.empty();
				newStoresDropdown.append('<option value="">-</option>');
				if (newStoresDropdown) {
						//append all the options resulted from the ajax call
						for ( i = 0; i < listaMagazine.length; i++){
							newStoresDropdown.append('<option value=' + listaMagazine[i].id + '>' + listaMagazine[i].store_id + ' ' + listaMagazine[i].name + '</option>');
						};
					}
			} else {
				
			}
		}
	});
}

// Prevent the backspace key from navigating back.
$(document).unbind('keydown').bind('keydown', function (event) {
    var doPrevent = false;
    if (event.keyCode === 8) {
        var d = event.srcElement || event.target;
        if ((d.tagName.toUpperCase() === 'INPUT' && 
             (
                 d.type.toUpperCase() === 'TEXT' ||
                 d.type.toUpperCase() === 'PASSWORD' || 
                 d.type.toUpperCase() === 'FILE' || 
                 d.type.toUpperCase() === 'SEARCH' || 
                 d.type.toUpperCase() === 'EMAIL' || 
                 d.type.toUpperCase() === 'NUMBER' || 
                 d.type.toUpperCase() === 'DATE' )
             ) || 
             d.tagName.toUpperCase() === 'TEXTAREA' ||
             d.tagName.toUpperCase() === 'DIV') {
            doPrevent = d.readOnly || d.disabled;
        }
        else {
            doPrevent = true;
        }
    }

    if (doPrevent) {
        event.preventDefault();
    }
});

AUI().ready(function() {
});

Liferay.on('allPortletsReady',
/*
 * This function gets loaded when everything, including the portlets, is on the
 * page.
 */
	function() {

	}
);

//FACTURI -> adaugare_factura.jsp : show/hide campurile nr_contract si nr_comanda in functie de 
//tipul de relatie comerciala selectat
Liferay.Portlet.ready(
		function(portletId, node) {
			AUI().use('aui-node',function(A) {
				var tip_relatie_comerciala = A.one('#_' + portletId + '_tip-rel-comerciala');
				//verificare portlet
				if (tip_relatie_comerciala){
					//valoarea selectata
					var value = tip_relatie_comerciala.val();
					//daca tip relatie comerciala = "cu contract"
					if (value == '1'){
						//verificare campuri
						if ($('#_' + portletId + '_label_comanda_display') && $('#_' + portletId +'_numar_comanda_display')){
							//hide campul "numar comanda" este ascuns
							$('#_' + portletId +'_label_comanda_display').hide();
							$('#_' + portletId + '_numar_comanda_display').hide();
						}
					//daca tipul de relatie comerciala este fara contract si fara comanda
					} else if (value == '3' ) {
						//verificare existenta campuri
						if ($('#_' + portletId + '_label_comanda_display') && $('#_' + portletId +'_numar_comanda_display') && 
								$('#_' + portletId + '_label_contract_display') && $('#_' + portletId + '_numar_contract_display')) {
							
							//ascunde campurile "nr-contract" si "nr-comanda"
							$('#_' + portletId + '_label_comanda_display').hide();
							$('#_' + portletId +'_numar_comanda_display').hide();
							$('#_' + portletId + '_label_contract_display').hide();
							$('#_' + portletId + '_numar_contract_display').hide();
						}
					}
					
					tip_relatie_comerciala.on('change', function(e){
						value = tip_relatie_comerciala.val();
						//daca tipul de relatie comerciala este " cu contract"
						if (value == '1'){
							//verificare existenta campuri
							if ($('#_' + portletId + '_label_comanda_display') && $('#_' + portletId +'_numar_comanda_display')){
								//show camp "nr-contract"
								$('#_' + portletId +'_label_contract_display').show();
								$('#_' + portletId +'_numar_contract_display').show();
								//hide camp "nr-comanda"
								$('#_' + portletId +'_label_comanda_display').hide();
								$('#_' + portletId + '_numar_comanda_display').hide();
							}
						} else if (value == '3' ) {
							//daca tipul de relatie comerciala este fara contract si comanda
							if ($('#_' + portletId + '_label_comanda_display') && $('#_' + portletId +'_numar_comanda_display') && 
									$('#_' + portletId + '_label_contract_display') && $('#_' + portletId + '_numar_contract_display')) {
								//hide campurile "nr-contract" si "nr-comanda"
								$('#_' + portletId + '_label_comanda_display').hide();
								$('#_' + portletId +'_numar_comanda_display').hide();
								$('#_' + portletId + '_label_contract_display').hide();
								$('#_' + portletId + '_numar_contract_display').hide();
							}
						//daca tipul de relatie comerciala este cu contract si comanda simpla
						}else if (value == '2'){
							//verificare existenta campuri
							if ($('#_' + portletId + '_label_comanda_display') && $('#_' + portletId +'_numar_comanda_display') && 
									$('#_' + portletId + '_label_contract_display') && $('#_' + portletId + '_numar_contract_display')) {
								
								//show campuri "nr-comanda" si "nr-contract"
								$('#_' + portletId + '_label_comanda_display').show();
								$('#_' + portletId +'_numar_comanda_display').show();
								$('#_' + portletId + '_label_contract_display').show();
								$('#_' + portletId + '_numar_contract_display').show();
								
							}
						}
					});
				}
			}
	);
});

//datepicker pentru filtrare data in header
Liferay.Portlet.ready(
	function(portletId, node) {
		AUI().use('aui-node',function(A) {
			// header tabel portlet aprobare solicitari avans
			$('#header_inv_date').html($('#filtru_data_factura').html());
			// header tabel portlet ...
			$('#header_datascadenta').html($('#filtru_data_scadenta').html());
			// header tabel portlet ...
			$('#header_data_factura').html($('#data_filter').html());
			// header tabel portlet asteapta validare
			$('#header_data_inreg').html($('#data_inregistrare_div').html());
			//header tabel portlet data import
			$('#filter-dataf').html($('#filtru_datafactura_div').html());
			//header tabel portlet datafactura 
			$('#filter-data-inreg').html($('#filtru_datainregfactura_div').html());
			$('#filter-date').html($('#filtru_datafactura_div').html());
			//header tabel portlet : facturi blocate la plata
			$('#header_inv_date_blocked').html($('#filtru_datafactura_div').html());
		}
	);
});

//Autocomplete sectiune valoare neta de plata ron/eur adauga_factura.jsp
Liferay.Portlet.ready(
		function(portletId, node) {
			AUI().use('aui-node',function(A) {
			var currency = A.one('#_' + portletId + '_currency');
			var warranty = A.one('#_' + portletId + '_procent_garantie');
			var val_with_vat_ron = A.one('#_' + portletId + '_val_with_vat_ron');
			var val_with_vat_curr = A.one('#_' + portletId + '_val_with_vat_curr');
			var val_neta_ron = A.one('#_' + portletId + '_val-netaplata-ron');
			var val_neta_curr = A.one('#_' + portletId + '_val-netaplata-eur');
			
			if(currency && warranty && val_with_vat_curr && val_with_vat_curr && val_neta_curr && val_neta_ron){
//				
//				val_with_vat_ron.on('change', function(e){
//					//set payment value ron
//					calculatePaymentValue(val_with_vat_ron, val_neta_ron, warranty, portletId);
//					if(currency.val() != 'RON'){
//						//set payment value currency
//						calculatePaymentValue(val_with_vat_curr, val_neta_curr, warranty, portletId);
//					}
//				});
//				
				warranty.on('change', function (e) {
					//set payment value ron
					setPaymentValues("_"+portletId+"_");
				});
			}
			}
	);
});

//set contract number and order number when the relation type changes
//adauga_factura.jsp
Liferay.Portlet.ready(
		function(portletId, node) {
			AUI().use('aui-node',function(A) {
				var selectedRelType = A.one('#_' + portletId + '_tip-rel-comerciala');
				var nr_comanda = A.one('#_' + portletId + '_nr-comanda');
				var nr_contract = A.one('#_' + portletId + '_nr-contract');
				if(selectedRelType && nr_comanda && nr_contract){
					selectedRelType.on('change', function(e){
						var selectedValue = this.val();
						if (selectedValue == "1"){
							nr_comanda.val(0);
						} else if (selectedValue == "3"){
							nr_comanda.val(0);
							nr_contract.val(0);
						}
					});
				}
			}
		);
});

//sectiunea din pagina care devine vizibila in functie de tipul solicitarii ->fact_info.jsp
Liferay.Portlet.ready(
	function(portletId, node) {
		AUI().use('aui-node',function(A) {
			//verificare portlet
			var selectedReqType=A.one('#_' + portletId + '_tip_solicitare');
			if(selectedReqType){
				selectedReqType.on('change',function(e){
					var selectedValue=this.val();
					if(selectedValue=="1"){
						$('#fact_neajunsa_scadenta').hide();
						$('#pentru_factura_proforma').show();
					}else{
						if(selectedValue=="3"){
							$('#pentru_factura_proforma').hide();
							$('#fact_neajunsa_scadenta').show();
						}else{
							$('#pentru_factura_proforma').hide();
							$('#fact_neajunsa_scadenta').hide();
						}
					}
				})
			}
		}
	);
});

//if currency is RON => exchange rate is 1
Liferay.Portlet.ready(
	function(portletId, node) {
		AUI().use('aui-node',function(A) {
			//verificare portlet
			var currency 		= A.one('#_' + portletId + '_currency');
			var exchange_rate 	= A.one('#_' + portletId + '_rata-schimb');
			if(currency && exchange_rate) {
				setExchangeRate(currency, exchange_rate);
				currency.on('change',function(e){
					setExchangeRate(currency, exchange_rate);
				});
			}
	});
});

//add dpi to div for listing->fact_info.jsp
Liferay.Portlet.ready(
		function(portletId, node) {
			AUI().use('aui-node',function(A) {
				var button=A.one('#_' + portletId + '_addDoc');
				var selectedDoc=A.one('#_' + portletId + '_doc-lipsa');
				var existentDPIlinks="";
				var v="";
				if(button){
					var dpiList = $('#displayDPi').html().toString();
					//add the docs that were already saved in the database to the current adding sesion
					var list = A.one('#_' + portletId + "_missing_docs").val();
				button.on('click',function(e){
					if( existentDPIlinks.indexOf( v ) < 0 && dpiList.indexOf( v ) < 0){
						
						$('#displayDPi').append('<a href="#">'+ v +'</a><br/>');
						existentDPIlinks+= v + " ";
						list += "," + selectedDoc.val() ;
						A.one('#_' + portletId + "_missing_docs").val(list);
					}
				});
				
				selectedDoc.on('change',function(e){
					v = selectedDoc.val();
					if( v == ""){
						A.one('#_' + portletId + '_addDoc').hide();
					}else{
						A.one('#_' + portletId + '_addDoc').show();
					}
				});
			}
		}
	);
});

Liferay.Portlet.ready(
// show numar factura de baza in create invoice screen
function(portletId, node) {
	AUI().use('aui-node',function(A) {
		var selectTipFactura = A.one('#_' + portletId + '_tip-factura');
		if (selectTipFactura) {
			// initially hidden
			if(A.one('#_' + portletId + '_nr-fact-baza3') && A.one('#_' + portletId + '_nr-fact-baza2')){
			A.one('#_' + portletId + '_nr-fact-baza3').hide();
			A.one('#_' + portletId + '_nr-fact-baza2').hide();
			}
			// bind event on select tip factura
			selectTipFactura.on('change', function() {
				var selectedValue = this.val();
				if (selectedValue == '2') {
					A.one('#_' + portletId + '_nr-fact-baza3').show();
					A.one('#_' + portletId + '_nr-fact-baza2').show();
				} else {
					A.one('#_' + portletId + '_nr-fact-baza3').hide();
					A.one('#_' + portletId + '_nr-fact-baza2').hide();
			
				}
			});
		}
	});
}

);

//html/includes/supplier.jsp remove readonly from supplier name if a company is selected
//aceasta functie se apela si pentru validare cerere avans(cu toate ca nu trebuia) de aceea conditia "&& isReadonly.val() == ''"
Liferay.Portlet.ready(
		// show numar factura de baza in create invoice screen
		function(portletId, node) {
			AUI().use('aui-node',function(A) {
				var company = A.one('#_' + portletId + '_company');
				var vendor 	= A.one('#_' + portletId + '_vendor');
				// fact_info.jsp 
				var isReadonly = A.one('#_' + portletId + '_isReadonly');
				if (company && vendor){
					if (company.val() != ""){
							if (isReadonly != null) {
								if (isReadonly.val() == ''){
									vendor.removeAttribute("readonly");
								}
							} else {
								//pentru a nu fi scos la cererile de avans
								//vendor.removeAttribute("readonly");
							}
					}
					company.on('change', function(e){
						var selected_company = company.val();
						if (selected_company != ""){
							if (isReadonly != null) {
								if (isReadonly.val() == ''){
									vendor.removeAttribute("readonly");
								} else {
									vendor.attr('readonly', true);
								}
							} else {
								vendor.removeAttribute("readonly");
							}
						} else {
							vendor.attr('readonly', true);
						}
					});
					
					
					vendor.on('click', function(e){
						if (company.val() == ""){
							alert("Selectati o societate pentru a putea seta un furnizor!");
						}
					});
				}
			});
		}
);



//show/hide section RON/EUR facturi + solicitari avans
//show/hide sectiune valoare neta de plata facturi
Liferay.Portlet.ready(
		function(portletId, node) {
			AUI().use('aui-node',function(A) {
				var selectMoneda = A.one('#_' + portletId + '_currency');
				if (selectMoneda) {
					//atunci cand campurile sunt readonly nu trebuie onchange
					if($('val_no_vat_ron[readonly="readonly"]')){
					   var valoareAutocompletata = selectMoneda.val();
						if (valoareAutocompletata == 'RON' && A.one('#_' + portletId + '_valoareCurrency')) {
							A.one('#_' + portletId + '_valoareCurrency').hide();
						} else {
							if (A.one('#currency_label')) {
								A.one('#currency_label').text('Valoare ' + valoareAutocompletata);
								A.one('#_' + portletId + '_valoareCurrency').show();
							}
						}
					}

					// bind event on moneda
					selectMoneda.on('change', function() {
						var selectedValue = this.val();
						
						// camp valoare fara TVA pentru RON 
						var valFaraTVARON 			= A.one('#_' + portletId + '_val_no_vat_ron');
						
						// do not indicate change on RON when using another CURRENCY
						valFaraTVARON.borderColor = '#ddd';
						
						if (selectedValue == 'RON') {
							//moneda selectata este RON
							A.one('#_' + portletId + '_valoareCurrency').hide();
							//pentru FACTURI -> adauga_factura.jsp ascunde campul valoare neta de plata
							if (A.one('#_' + portletId + '_net_plata_currency')) {
								A.one('#_' + portletId + '_net_plata_currency').hide();
							}
						}  else {
							//moneda este diferita de RON
							//pentru FACTURI -> adauga_factura.jsp arata campul valoare neta de plata
							if (A.one('#_' + portletId + '_net_plata_currency')) {
								A.one('#_' + portletId + '_net_plata_currency').show();
							}
							A.one('#_' + portletId + '_valoareCurrency').show();
							A.one('#currency_label').text('Valoare ' + selectedValue);
						}
						
					});
				}
			});
		}
);

// Calculare valoare TVA si cu TVA, pentru moneda RON in ecranele 
// de validare/creare factura si creare solicitare avans
Liferay.Portlet.ready(
	function(portletId, node) {
		AUI().use('aui-node', function(A) {
			// camp valoare fara TVA pentru RON
			//console.log(portletId);
			var valFaraTVA 		= A.one('#_' + portletId + '_val_no_vat_ron');
			
			//verificam daca au fost setate valoarea  fara TVA si cota tva
			if (valFaraTVA) {
				// set field format
				//formatInputDisplay(portletId);
				// camp valoare cota TVA
				var cotaTva 		= A.one('#_' + portletId + '_cota_tva');
				// camp valoare procent Garantie
				var warranty 		= A.one('#_' + portletId + '_procent_garantie');
				// camp valoare rata de schimb
				var rate_eur 		= A.one('#_' + portletId + '_rate');
				// camp valoare neta RON
				var val_neta_ron 	= A.one('#_' + portletId + '_val-netaplata-ron');
				// camp valoare neta MONEDA(orice in afara de RON)
				var val_neta_eur 	= A.one('#_' + portletId + '_val-netaplata-eur');
				// valoarea totala a articolelor alocate pe magazine
				var total_store_line_value = A.one('#_' + portletId + '_val_no_vat_aloc_linii');
				// camp tip MONEDA ales
				var currency = A.one('#_' + portletId + '_currency');

				// camp valoare TVA pentru RON
				var sumaValFaraTva = A.one('#_' + portletId + '_val_vat_ron');
				// camp valoare cu TVA pentru RON
				var sumaValCuTva = A.one('#_' + portletId + '_val_with_vat_ron');
				if (total_store_line_value) {
					if(parseFloat(valFaraTVA.val().replace(toReplace,"")) != parseFloat(total_store_line_value.val().replace(toReplace,""))){
						//console.log(parseFloat(valFaraTVA.val()) != parseFloat(total_store_line_value.val()));
						$('#_' + portletId + '_val_no_vat_ron').css("border-color", "#FE0000");
						$('#_' + portletId + '_val_no_vat_curr').css("border-color", "#FE0000");
						$('#_' + portletId + '_val_no_vat_aloc_linii').css("border-color", "#FE0000");
						
					} else {
						$('#_' + portletId + '_val_no_vat_ron').css("border-color", "#488F06");
						$('#_' + portletId + '_val_no_vat_curr').css("border-color", "#488F06");
						$('#_' + portletId + '_val_no_vat_aloc_linii').css("border-color", "#488F06");
					}
				}
				if (cotaTva){
					//la modificari pe campul valoarea fara tvaz
					$('#_' + portletId + '_val_no_vat_ron').on('change', function() {
						$('#_' + portletId + '_val_no_vat_ron').autoNumeric('init', {aSep: ',', mDec: 4, vMin : -999999999, vMax : 999999999 });
						calculateTVA(portletId, valFaraTVA, cotaTva, sumaValFaraTva, sumaValCuTva);
						if (total_store_line_value) {
							//update the difference
							//setTotalNoVatLines('_' + portletId + '_');
							
							if(parseFloat(valFaraTVA.val()) != parseFloat(total_store_line_value.val())){
								//console.log("AICI : " + valFaraTVA.val());
								//total_store_line_value.borderColor = '#FE0000';
								//valFaraTVA.borderColor = '#FE0000';
								$('#_' + portletId + '_val_no_vat_ron').css("border-color", "#FE0000");
								$('#_' + portletId + '_val_no_vat_curr').css("border-color", "#FE0000");
								$('#_' + portletId + '_val_no_vat_aloc_linii').css("border-color", "#FE0000");
							} else {
								//total_store_line_value.borderColor = '#488F06';
								//valFaraTVA.borderColor = '#488F06';
								$('#_' + portletId + '_val_no_vat_ron').css("border-color", "#488F06");
								$('#_' + portletId + '_val_no_vat_curr').css("border-color", "#488F06");
								$('#_' + portletId + '_val_no_vat_aloc_linii').css("border-color", "#488F06");
							}
						}
						val_neta_ron.val(A.one('#_' + portletId + '_val_with_vat_ron').val());
						val_neta_eur.val(A.one('#_' + portletId + '_val_with_vat_curr').val());
					});
				
					//la modificari pe campul cota tva calculam valoare tva si valoarea cu tva
					cotaTva.on('change', function() {
						calculateTVA(portletId, valFaraTVA, cotaTva, sumaValFaraTva, sumaValCuTva);
					});
				}
			}
		});
	}
);

//calculare valoare TVA si cu TVA, pentru moneda MONEDA, in ecranele 
//de validare/creare factura si creare solicitare avans
Liferay.Portlet.ready(
	function(portletId, node) {
		AUI().use('aui-node', function(A) {
			// camp valoare fara TVA pentru MONEDA (orice in afara de RON)
			var valFaraTVA 				= A.one('#_' + portletId + '_val_no_vat_curr');
			
			//verificam daca au fost setate valoarea fara TVA si cota tva
			if (valFaraTVA) {
				// set field format
				//formatInputDisplay(portletId);
				// camp valoare cota TVA
				var cotaTva 				= A.one('#_' + portletId + '_cota_tva');
				// camp valoare rata de schimb
				var rata_schimb 			= A.one('#_' + portletId + '_rata-schimb');
				// camp "Valoare fara TVA alocare pe linii"
				var total_store_line_value 	= A.one('#_' + portletId + '_val_no_vat_aloc_linii');
				// camp valoare fara TVA pentru RON 
				var valFaraTVARON 			= A.one('#_' + portletId + '_val_no_vat_ron');
				//valoare neta plata
				var valNetaDePlataRON 		= A.one('#_' + portletId + '_val-netaplata-ron');
				
				// do not indicate change on RON when using another CURRENCY
				valFaraTVARON.borderColor = '#ddd';
				valFaraTVA.borderColor = '#ddd';
				if (cotaTva) {
					//la modificari pe campul valoarea fara tva calculam valoare tva si valoarea cu tva
					$('#_' + portletId + '_val_no_vat_curr').on('change', function() {
						$('#_' + portletId + '_val_no_vat_curr').autoNumeric('init', {aSep: ',', mDec: 4, vMin : -999999999, vMax : 999999999 });
						if (total_store_line_value) {
							var newValNoVatRon = parseFloat(parseFloat(valFaraTVA.val().replace(toReplace, "")) * rata_schimb.val()).toFixed(4);
							console.log(newValNoVatRon);
							console.log(total_store_line_value.val().replace(toReplace, ""));
							if (newValNoVatRon != parseFloat(total_store_line_value.val().replace(toReplace, ""))) {
								console.log("if");
								$('#_' + portletId + '_val_no_vat_aloc_linii').css("border-color", "#FE0000");
								$('#_' + portletId + '_val_no_vat_curr').css("border-color", "#FE0000");
								$('#_' + portletId + '_val_no_vat_ron').css("border-color", "#FE0000");
							} else {
								$('#_' + portletId + '_val_no_vat_aloc_linii').css("border-color", "#488F06");
								$('#_' + portletId + '_val_no_vat_curr').css("border-color", "#488F06");
								$('#_' + portletId + '_val_no_vat_ron').css("border-color", "#488F06");
							}
						}
						
						var selectedValueFaraTva = valFaraTVA.val().replace(toReplace, "");
						var selectedCotaTva = cotaTva.val();
						if($('#_' + portletId + '_supplier_type_invoice').val() == 1) {
							selectedCotaTva = "0.00";
						} else if ($('#_' + portletId + '_supplier_type_invoice').val() == 0) {
							if($('#_' + portletId + '_intern_invoice_option').val() == 4) {
								selectedCotaTva = "0.00";
							}
						}
						var val_rata=rata_schimb.val();
						var valTva2 = (selectedValueFaraTva * selectedCotaTva/100).toFixed(noOfDecimals);
						var valCuTva2 = parseFloat(valTva2) + parseFloat(selectedValueFaraTva);
						
						var valTvaRon = (valTva2 * val_rata);
						var valNoTvaRon = (selectedValueFaraTva * val_rata);
						var valWithTvaRon = parseFloat(valTvaRon) + parseFloat(valNoTvaRon);
						
						A.one('#_' + portletId + '_val_vat_curr').val(valTva2);
						A.one('#_' + portletId + '_val_with_vat_curr').val(valCuTva2);
						
						A.one('#_' + portletId + '_val_vat_ron').val(valTvaRon.toFixed(noOfDecimals));
						A.one('#_' + portletId + '_val_with_vat_ron').val(valWithTvaRon.toFixed(noOfDecimals));
						valFaraTVARON.val(valNoTvaRon.toFixed(noOfDecimals));
						
						//format value without vat RON
						$('#_' + portletId + '_val_no_vat_ron').autoNumeric('update', {aSep: ',', mDec: 4, vMin : -999999999, vMax : 999999999 });
						//format values for val vat and val with vat CURR
						$('#_' + portletId + '_val_vat_curr').autoNumeric('update', {aSep: ',', mDec: 4, vMin : -999999999, vMax : 999999999 });
						$('#_' + portletId + '_val_with_vat_curr').autoNumeric('update', {aSep: ',', mDec: 4, vMin : -999999999, vMax : 999999999 });

						//after setting the value, trigger a change so that the payment value is calculated
						A.one('#_' + portletId + '_val_with_vat_ron').simulate('change');
						A.one('#_' + portletId + '_val_vat_ron').simulate('change');
						calculeazaTvaPeStoreLines(portletId);
					});
					
					//la modificari pe campul cota tva calculam valoare tva si valuarea cu tva
					cotaTva.on('change', function() {
						var selectedValueFaraTva = valFaraTVA.val().replace(toReplace, "");
						var selectedCotaTva = cotaTva.val();
						if($('#_' + portletId + '_supplier_type_invoice').val() == 1) {
							selectedCotaTva = "0.00";
						} else if ($('#_' + portletId + '_supplier_type_invoice').val() == 0) {
							if($('#_' + portletId + '_intern_invoice_option').val() == 4) {
								selectedCotaTva = "0.00";
							}
						}
						
						var valTva2 = (selectedValueFaraTva * selectedCotaTva/100).toFixed(noOfDecimals);
						var valCuTva2 = parseFloat(valTva2) + parseFloat(selectedValueFaraTva);
						
						A.one('#_' + portletId + '_val_vat_curr').val(valTva2);
						A.one('#_' + portletId + '_val_with_vat_curr').val(valCuTva2);
						//format value without vat RON
						$('#_' + portletId + '_val_no_vat_ron').autoNumeric('update', {aSep: ',', mDec: 4, vMin : -999999999, vMax : 999999999 });
						//format values for val vat and val with vat CURR
						$('#_' + portletId + '_val_vat_curr').autoNumeric('update', {aSep: ',', mDec: 4, vMin : -999999999, vMax : 999999999 });
						$('#_' + portletId + '_val_with_vat_curr').autoNumeric('update', {aSep: ',', mDec: 4, vMin : -999999999, vMax : 999999999 });

						//after setting the value, trigger a change so that the payment value is calculated
						A.one('#_' + portletId + '_val_with_vat_ron').simulate('change');
						A.one('#_' + portletId + '_val_vat_ron').simulate('change');
						calculeazaTvaPeStoreLines(portletId);
					});
					
					//la modificarea ratei de schimb valutar
					rata_schimb.on('change',function(){
						var currency = A.one('#_' + portletId + '_currency').val();
						if(currency != 'RON' && valFaraTVA.val() != ""){
							var selectedValueFaraTva = valFaraTVA.val().replace(toReplace, "");
							var selectedCotaTva = cotaTva.val();
							var val_rata=rata_schimb.val();
							var valTva2 = (selectedValueFaraTva * selectedCotaTva/100).toFixed(noOfDecimals);
							var valCuTva2 = parseFloat(valTva2) + parseFloat(selectedValueFaraTva);
							
							var valTvaRon = (valTva2 * val_rata);
							var valNoTvaRon = (selectedValueFaraTva * val_rata);
							var valWithTvaRon = parseFloat(valTvaRon) + parseFloat(valNoTvaRon);
							
							A.one('#_' + portletId + '_val_vat_curr').val(valTva2);
							A.one('#_' + portletId + '_val_with_vat_curr').val(valCuTva2);
							
							A.one('#_' + portletId + '_val_vat_ron').val(valTvaRon.toFixed(noOfDecimals));
							A.one('#_' + portletId + '_val_with_vat_ron').val(valWithTvaRon.toFixed(noOfDecimals));
							valFaraTVARON.val(valNoTvaRon.toFixed(noOfDecimals));
							//format value without vat RON
							$('#_' + portletId + '_val_no_vat_ron').autoNumeric('update', {aSep: ',', mDec: 4, vMin : -999999999, vMax : 999999999 });
							//format values for val vat and val with vat CURR
							$('#_' + portletId + '_val_vat_curr').autoNumeric('update', {aSep: ',', mDec: 4, vMin : -999999999, vMax : 999999999 });
							$('#_' + portletId + '_val_with_vat_curr').autoNumeric('update', {aSep: ',', mDec: 4, vMin : -999999999, vMax : 999999999 });

							//after setting the value, trigger a change so that the payment value is calculated
							A.one('#_' + portletId + '_val_with_vat_ron').simulate('change');
							A.one('#_' + portletId + '_val_vat_ron').simulate('change');
							calculeazaTvaPeStoreLines(portletId);
						}
					});
				}
			}
		});
	}
);

/**
 * Set exchange rate on currency select change. 
 */
function setExchangeRate(currency, exchange_rate) {
	if (currency.attr("readonly")) {
		// do nothing
	} else {
		if (currency.val() == "RON") {
			exchange_rate.val("1");
			exchange_rate.attr("readonly", true);
		} else {
			exchange_rate.removeAttribute("readonly");
//			if(exchange_rate.val() == null) {
//				exchange_rate.val("");
//			}
		}
	}
}

/**
 * Function to calculate VAT for RON in advance_request
 */
function calculateTVA(portletId, valFaraTVA, cotaTva, sumaValFaraTva, sumaValCuTva) {
	var selectedValueFaraTva = parseFloat(valFaraTVA.val().replace(toReplace, ""));
	var selectedCotaTva = cotaTva.val();
	if($('#_' + portletId + '_supplier_type_invoice').val() == 1) {
		selectedCotaTva = "0.00";
	} else if ($('#_' + portletId + '_supplier_type_invoice').val() == 0) {
		if($('#_' + portletId + '_intern_invoice_option').val() == 4) {
			selectedCotaTva = "0.00";
		}
	}
	
	console.log("cota vat: " + selectedCotaTva);

	var valTva2 = (selectedValueFaraTva * selectedCotaTva/100).toFixed(noOfDecimals);
	var valCuTva2 = parseFloat(valTva2) + parseFloat(selectedValueFaraTva);
	sumaValFaraTva.val(valTva2);
	sumaValCuTva.val(valCuTva2);
	//after setting the value, trigger a change so that the payment value is calculated
	sumaValCuTva.simulate('change');
	calculeazaTvaPeStoreLines(portletId);
	
}

//show hide add dpi button in fact_info.jsp
Liferay.Portlet.ready(
	function(portletId, node) {
		AUI().use('aui-node',function(A) {
			var button=A.one('#_' + portletId + '_realizare_dpi');
			if(button) {
				button.on('click',function(e){
					if(	$('#vizualizare_realizare').css('display')=='none'){
						$('#vizualizare_realizare').show();
					}else
						$('#vizualizare_realizare').hide();
				});
			}
		})
	}
);

// used on invoice_approval screen.
Liferay.Portlet.ready(
	function(portletId, node) {
	AUI().use('aui-node',function(A) {
		var button=A.one('#_' + portletId + '_realizare_buget');
		if(button) {
			button.on('click',function(e){
				if(	$('#vizualizare_buget').css('display')=='none'){
					$('#vizualizare_buget').show();
				}else
					$('#vizualizare_buget').hide();
			});
		}
	})
});

//autocomplete invoice date on create advance request 
Liferay.Portlet.ready(
	function(portletId, node) {
		AUI().use('aui-node',function(A) {
			if ($('#_' + portletId + '_inv_date')) {
				if($('#_' + portletId + '_inv_date').val() == ""){
					$('#_' + portletId + '_inv_date').val(setCurrentDate());
				}
			}
		});
	}
);

//for single approval aprove.jsp
Liferay.Portlet.ready(
		function(portletId, node) {
			AUI().use('aui-node',function(A) {
				var bunDePlata = $('#_' + portletId + '_bun_de_plata');
				var refuzLaPlata = $('#_' + portletId + '_refuz_plata');
				if (bunDePlata) {
					if(refuzLaPlata){
						bunDePlata.on('click',function(e){
							$('#_' + portletId + '_update-stage-aprove').submit();
						});
						refuzLaPlata.on('click',function(e){
							$('#_' + portletId + '_refuzlaplata').val("refuz");
							$('#_' + portletId + '_update-stage-aprove').submit();
						})
					}
				}
		});
	}
);

//autocomplete reg_date based on invoice_date
function invoiceDateChanged (inv_date, portletId){
	var reg_date = $('#' + portletId + 'created');
	
	var parts_inv_date = String(inv_date).split("-");
	var current_date = setCurrentDate();
	var parts_current_date = String(current_date).split("-");
	if (parseInt(parts_inv_date[0]) == parseInt(parts_current_date[0])){
		//console.log("same year");
		//if current_date and inv_date have the same month
		if (parseInt(parts_current_date[1]) == parseInt(parts_inv_date[1])){
			//console.log("same month");
			//reg_date is the same as inv_date
			reg_date.val(inv_date);
		} else if (parseInt(parts_current_date[1]) > parseInt(parts_inv_date[1])){
			//console.log("month in the past");
			//if inv_date month < current_date month
			//reg_date val = first day of the current month of the current year
			reg_date.val(parts_current_date[0] + "-" + parts_current_date[1] + "-" + "01");
		} else {
			//if inv_date month > current_date month
			//reg_date is invocie date
			reg_date.val(inv_date);
		}
	} else if (parseInt(parts_current_date[0]) > parseInt(parts_inv_date[0])){
		//if the current year > invoice year
		//reg_date val = first day of the current month of the current year
		reg_date.val(parts_current_date[0] + "-" + parts_current_date[1] + "-" + "01");
	} else {
		//if inv_date year > current_date year
		//reg_date is invoice date
		reg_date.val(inv_date);
	}
	
}

//adauga_factura.jsp : onClick data inregistrare
Liferay.Portlet.ready(
		function(portletId, node) {
			AUI().use('aui-node',function(A) {
				var reg_date = $('#_' + portletId + '_created');
				reg_date.on('click', function(e){
					if (reg_date.val() == ""){
						alert("Va rugam completati data facturii!");
					}
				});
		});
	}
);




// show vizualizareDPI overlay
Liferay.Portlet.ready(
		function(portletId, node) {
			AUI().use('aui-node',function(A) {
				var link = $('#_' + portletId + '_vizualizare');
				if (link) {					
						link.on('click',function(e){
						   if(	$('#_' + portletId+ '_vizualizareDPI').css(display)=='none'){
							    $('#vizualizareDPI').show();
						   }else{
							    $('#vizualizareDPI').hide();
						 
						   }
						});
				}
		});
	}
);


//adauga_factura.jsp : Cannot select the invoice type if the supplier was not set
Liferay.Portlet.ready(
		function(portletId, node) {
			AUI().use('aui-node',function(A) {
				var supplier = A.one('#_' + portletId + '_vendor');
				var invoice_type = A.one('#_' + portletId + '_tip-factura');

				var pay = A.one('#_' + portletId + '_payment_term');
				if (supplier && invoice_type){
					//if the supplier was not set
					if (supplier.val() == "") {
						//invoice type dropdown is disabled
						invoice_type.set('disabled', true);
					}
					supplier.on('change', function(e){
						//console.log("Payment term : " + pay.val());
						//if the supplier was not set
						if (supplier.val() == "") {
							//invoice type dropdown is disabled
							invoice_type.set('disabled', true);
						} else {
							invoice_type.set('disabled', false);
						}
					});
				}
			});
		});

//adauga_factura.jsp : If the company was not set all the fields associated with the suplier must be empty
Liferay.Portlet.ready(
		function(portletId, node) {
			AUI().use('aui-node',function(A) {
				var company = A.one('#_' + portletId + '_company');
				var supplier = A.one('#_' + portletId + '_vendor');
				var cui = A.one('#_' + portletId + '_cui');
				var cont = A.one('#_' + portletId + '_cont-asociat');
				var invoice_type = A.one('#_' + portletId + '_tip-factura');
				if (company && supplier && cui && cont && invoice_type){
					company.on('change', function(e){
						//empty the supplier associated fields
						supplier.val("");
						cui.val("");
						cont.val("");
						//hide the 2 warning texts
						$('#_' + portletId + '_loccked_supplier').hide();
						$('#_' + portletId + '_missing_supplier').hide();
						//simulate change on supplier name field
						supplier.simulate('change');
					});
				}
			});
		});


/**
 * Function used to calculate total payment value on an invoice in RON on adauga_factura.jsp.
 * @param value 	- The input that holds total invoice value without vat
 * @param payment	- The input that has to be filled with the total payment value
 * @param warranty	- The input that holds the warranty procentage
 */
function calculatePaymentValue(value, payment, warranty, portletId){
	if(value && payment && warranty){
		//console.log("value : "+value.val());
		var warranty_value = parseFloat(parseFloat(value.val().replace(toReplace,'')) * parseFloat(warranty.val()) / 100).toFixed(4);
		payment.val(parseFloat(parseFloat(value.val().replace(toReplace,'')) - parseFloat(warranty_value)).toFixed(4));
		//console.log("payment : " + payment.val());
		$('#_' + portletId + '_val-netaplata-eur').autoNumeric('update', {aSep: ',', mDec: 4, vMin : -999999999, vMax : 999999999 });
		$('#_' + portletId + '_val-netaplata-ron').autoNumeric('update', {aSep: ',', mDec: 4, vMin : -999999999, vMax : 999999999 });
	}
}

/**
 * GENERIC method used to capitalize a string.
 * @param obj - The string to be capitalized.
 * @returns {String} - returns a string with all caps characters.
 */
function capitalize(obj) {
    var strVal = obj;
    var newVal = '';
    var arrVal = strVal.split(' ');
    for(var c = 0; c < arrVal.length; c++) {
        newVal += arrVal[c].substring(0,1).toUpperCase() + arrVal[c].substring(1, arrVal[c].length) + ' ';
    }
    return newVal;
}

/**
 * GENERIC function used to populate current date to HTML inputs
 */
function setCurrentDate() {
	var currentDate = new Date();
	var day = currentDate.getDate();
	var month = currentDate.getMonth() + 1;
	var year = currentDate.getFullYear();
	if (month < 10) {
		month = "0" + month;
	}
	if (day < 10) {
		day = "0" + day;
	}
	console.log(year + "-" + month + "-" + day);
	
	return (year + "-" + month + "-" + day);
}

/**
 * Clear supplier errors on each submit.
 */
function clearSupplierErrors(portlet_id) {
	$('#' + portlet_id + 'new_furnizor').css("border-color", "#ddd");
	$('#' + portlet_id + 'new_cui').css("border-color", "#ddd");
	$('#' + portlet_id + 'new_email').css("border-color", "#ddd");
	$('#' + portlet_id + 'new_telefon').css("border-color", "#ddd");
	$('#' + portlet_id + 'new_J_field').css("border-color", "#ddd");
	$('#' + portlet_id + 'new_banca_beneficiar').css("border-color", "#ddd");
	$('#' + portlet_id + 'new_banca').css("border-color", "#ddd");
	$('#' + portlet_id + 'new_termen_plata').css("border-color", "#ddd");
	$('#' + portlet_id + 'new_cont_bancar').css("border-color", "#ddd");
	$('#' + portlet_id + 'new_cod_swift').css("border-color", "#ddd");
	$('#' + portlet_id + 'new_company').css("border-color", "#ddd");
}

/**
 * Clear supplier pop-up before showing it once again.
 */
function clearSupplierValues(portlet_id) {
	$('#' + portlet_id + 'new_furnizor').val("");
	$('#' + portlet_id + 'new_cui').val("");
	$('#' + portlet_id + 'new_email').val("");
	$('#' + portlet_id + 'new_telefon').val("");
	$('#' + portlet_id + 'new_J_field').val("");
	$('#' + portlet_id + 'new_banca_beneficiar').val("");
	$('#' + portlet_id + 'new_banca').val("");
	$('#' + portlet_id + 'new_termen_plata').val("");
	$('#' + portlet_id + 'new_cont_bancar').val("");
	$('#' + portlet_id + 'new_cod_swift').val("");
	$('#' + portlet_id + 'new_company').val("");
	clearSupplierErrors(portlet_id);
	
}

function validateSupplierPopUpFromAccounting(portletId){
	var error = validateSupplierPopupValues(portletId);
	
	if ($('#' + portletId + 'new_banca_beneficiar').val() == "") {
		$('#' + portletId + 'new_banca_beneficiar').css("border-color", "#FE0000");
		error++;
	}
	
	if ($('#' + portletId + 'new_cod_swift').val() == "") {
		$('#' + portletId + 'new_cod_swift').css("border-color", "#FE0000");
		error++;
	}
	
	return error;
}

/**
 * Validate supplier pop-up for required fields.
 */
function validateSupplierPopupValues(portletid) {
	var error = 0;
	//console.log("Main : " + portletid);
	if ($('#' + portletid + 'new_furnizor').val().trim() == "") {
		$('#' + portletid + 'new_furnizor').css("border-color", "#FE0000");
		error++;
	} else {
		$('#' + portletid + 'new_furnizor').css("border-color", "#488F06");
	}
	if ($('#' + portletid + 'new_cui').val().trim() == "") {
		$('#' + portletid + 'new_cui').css("border-color", "#FE0000");
		error++;
	} else {
		$('#' + portletid + 'new_cui').css("border-color", "#488F06");
	}
	if ($('#' + portletid + 'new_email').val().trim() == "") {
		$('#' + portletid + 'new_email').css("border-color", "#FE0000");
		error++;
	} else {
		$('#' + portletid + 'new_email').css("border-color", "#488F06");
	}
	if ($('#' + portletid + 'new_telefon').val().trim() == "") {
		$('#' + portletid + 'new_telefon').css("border-color", "#FE0000");
		error++;
	} else {
		$('#' + portletid + 'new_telefon').css("border-color", "#488F06");
	}
	if ($('#' + portletid + 'new_J_field').val().trim() == "") {
		$('#' + portletid + 'new_J_field').css("border-color", "#FE0000");
		error++;
	} else {
		$('#' + portletid + 'new_J_field').css("border-color", "#488F06");
	}
	if ($('#' + portletid + 'new_banca_beneficiar').val() == "") {
		$('#' + portletid + 'new_banca_beneficiar').css("border-color", "#FE0000");
		error++;
	}
	if ($('#' + portletid + 'new_banca').val().trim() == "") {
		$('#' + portletid + 'new_banca').css("border-color", "#FE0000");
		error++;
	} else {
		$('#' + portletid + 'new_banca').css("border-color", "#488F06");
	}
	if ($('#' + portletid + 'new_termen_plata').val().trim() == "") {
		$('#' + portletid + 'new_termen_plata').css("border-color", "#FE0000");
		error++;
	} else {
		$('#' + portletid + 'new_termen_plata').css("border-color", "#488F06");
	}
	if ($('#' + portletid + 'new_cont_bancar').val().trim() == "") {
		$('#' + portletid + 'new_cont_bancar').css("border-color", "#FE0000");
		error++;
	} else {
		$('#' + portletid + 'new_cont_bancar').css("border-color", "#488F06");
	}
	if ($('#' + portletid + 'new_cod_swift').val() == "") {
		$('#' + portletid + 'new_cod_swift').css("border-color", "#FE0000");
		error++;
	}
	if ($('#' + portletid + 'new_company').val().trim() == "") {
		$('#' + portletid + 'new_company').css("border-color", "#FE0000");
		error++;
	} else {
		$('#' + portletid + 'new_company').css("border-color", "#488F06");
	}
	if (error != 0)
		alert("Campurile nu sunt setate corespunzator!");
	return error;
}

/**
 * GENERIC function used to add currency formater to invoice and advance request forms and inputs
 * @param portletId - the ECM autogenerated portletId
 */
function formatInputDisplay(portletId) {
	//var currencyDisplay = "?999 999 999.9999";
	/*var options = {aSep: ' ', aDec: '.', mDec: 4};
	$('#_' + portletId + '_val_no_vat_ron').autoNumeric('init', options);
	$('#_' + portletId + '_val_vat_ron').autoNumeric('init', options);
	$('#_' + portletId + '_val_with_vat_ron').autoNumeric('init', options);
	
	$('#_' + portletId + '_val_no_vat_curr').autoNumeric('init', options);
	$('#_' + portletId + '_val_vat_curr').autoNumeric('init', options);
	$('#_' + portletId + '_val_with_vat_curr').autoNumeric('init', options);
	
	$('#_' + portletId + '_val-netaplata-ron').autoNumeric('init', options);
	$('#_' + portletId + '_val-netaplata-eur').autoNumeric('init', options);
	$('#_' + portletId + '_val_no_vat_aloc_linii').autoNumeric('init', options);*/
}

/**
 * GENERIC navigation button state function.
 */
function setNavigationButtonsState(portletId) {
	// activate and deactivate first, prev, next, last buttons
	if ((parseInt(start) + parseInt(count)) >= total) { 
		$('#' + portletId + 'lastButton').prop('disabled', true);
		$('#' + portletId + 'lastButton').addClass('disabled');
		$('#' + portletId + 'nextButton').prop('disabled', true);
		$('#' + portletId + 'nextButton').addClass('disabled');
	} else {
		$('#' + portletId + 'lastButton').prop('disabled', false);
		$('#' + portletId + 'lastButton').removeClass('disabled');
		$('#' + portletId + 'nextButton').prop('disabled', false);
		$('#' + portletId + 'nextButton').removeClass('disabled');
	}
	
	if (start == 0) {
		$('#' + portletId + 'firstButton').prop('disabled', true);
		$('#' + portletId + 'firstButton').addClass('disabled');
		$('#' + portletId + 'prevButton').prop('disabled', true);
		$('#' + portletId + 'prevButton').addClass('disabled');
	} else {
		$('#' + portletId + 'firstButton').prop('disabled', false); 
		$('#' + portletId + 'firstButton').removeClass('disabled'); 
		$('#' + portletId + 'prevButton').prop('disabled', false);
		$('#' + portletId + 'prevButton').removeClass('disabled');
	}
}

/** COMMENTED OUT CODE - TO BE REMOVED BEFORE PRODUCTION **/

/*
 * 
Liferay.Portlet.ready(
		function(portletId, node){
			AUI().use('aui-node', function(A){
				var button=$('#_'+ portletId + '_nr_bon');
				if(button){
					button.on('click',function(e){
						$('#inreg_voucher').show();
					});
				}
				
			});
		}
);
*/
//for validate advance request
/*
Liferay.Portlet.ready(
		function(portletId, node) {
			AUI().use('aui-node',function(A) {
				var validate = $('#_' + portletId + '_validare_document');
				if (validate) {
						validate.on('click',function(e){
							$('#_' + portletId + '_update-stage-validate').submit();
						});
				}
		});
	}
);
*/

//add dpi to div for listing->fact_info.jsp
/*
Liferay.Portlet.ready(
	function(portletId, node) {
		//add dpi to div for listing->fact_info.jsp
		AUI().use('aui-node',function(A) {
			var button = A.one('#_' + portletId + '_addDPI');
			var selectedDPI = A.one('#_' + portletId + '_select_dpi');
			var existentDPIlinks = "";
			var v = "";
			console.log(button);
			if(button) {
				//add dpi to div for listing->fact_info.jsp
				button.on('click',function(e){
					if ( v != "" && !(existentDPIlinks.indexOf( v ) >= 0)) {
						$('#displayDPi').append('<a href="#" class="added_dpis" id="added_dpi_' + v + '">DPI ' + v + '</a><br/>');
						// used to keep track of added DPIs
						existentDPIlinks += v + " ";
						// create bind on newlly added DPI links
					    Y.on("click", function(e) {
					    	e.preventDefault();
					    	var currentNode = this;
					    	console.log("clicked on added dpi: " + currentNode.get('id'));
					    	// retrieve the ID of the DPI to be shown
					    	var dpiId = currentNode.get('id').replace('added_dpi_','');
					    	// show DPI Pop-up
					    	showDPIPopup(currentNode, dpiId);
					    }, '.added_dpis');
					}
				});
				//show a href when selecting dpi->fact_info.jsp
				selectedDPI.on('change',function(e){
					v = selectedDPI.val();
					if (v == ""){
						$('#vizualizare').hide();
					} else {
						$('#vizualizare').show();
					}
				});
			}
		}
	);
});*/


//nomenclator reguli de inventar : FORMARE NUMAR DE INVENTAR - conditii de formare reguli
Liferay.Portlet.ready(
		function(portletId, node) {
			AUI().use('aui-node',function(A) {
				
				var selectGroupeIas = A.one('#_' + portletId + '_id_ias');
				if (selectGroupeIas) {
					//atunci cand campurile sunt readonly nu trebuie onchange ?
					
					// var valoareAutocompletata= selectGroupeIas.val();	
										
					// bind event on change
					selectGroupeIas.on('change', function() {
					var valoareAutocompletataText= $("#_" + portletId +"_id_ias option:selected").text();
					
					var selectedValue = this.val();
					if (selectedValue!=0)	{	    						
						var valGrupaSintetica=$('#_' + portletId + '_grupa_sintetica').val();
						var grupaSintetica = $('#_' + portletId + '_grupa_sintetica');
						var denGrupaSintetica=A.one('#_' +portletId+ '_den_grupa_sintetica');
					
				//		var storeCode=A.one('#_' + portletID + '_store_code_no');
						var res=valoareAutocompletataText.indexOf("-");
						grupaSintetica.val(valoareAutocompletataText.substring(0,res-1));						
						denGrupaSintetica.val(valoareAutocompletataText.substring(res+1));	
						var sqrGroup=$('#_'+ portletId + '_sqr_group');
						
						
						if (valoareAutocompletataText.substring(1,2)=="G" || String(valGrupaSintetica.length) == "5" || String(valGrupaSintetica.length) == "3" ){
							//sqrGroup.val("1");
						//	console.log(String(valGrupaSintetica.length));
							sqrGroup.val(valoareAutocompletataText.substring(3,4));
						}
						else if (parseInt(String(valGrupaSintetica.length)) == "3" || parseInt(String(valGrupaSintetica.length)) == "4" ){
							sqrGroup.val(valoareAutocompletataText.substring(1,2));
							
							//console.log("dim" + String(grupaSintetica.length));
						
						} else 	if (valoareAutocompletataText.substring(1,2)=="G" && parseInt(String(valGrupaSintetica.length) == "7" )){
							sqrGroup.val("1");
						} else if (valoareAutocompletataText.substring(1,2)=="G" && parseInt(String(valGrupaSintetica.length))== "5" || parseInt(String(valGrupaSintetica.length)=="4" )){
							sqrGroup.val(valoareAutocompletataText.substring(3,4));
						
						} else if(valoareAutocompletataText.substring(1,2)=="G" && parseInt(String(valGrupaSintetica.length)=="6")) {
							sqrGroup.val("4");
							
						} else if(valoareAutocompletataText.substring(1,2)=="2" && parseInt(String(valGrupaSintetica.length))=="5")  {
							sqrGroup.val("1");
							
						} else if (parseInt(String(valGrupaSintetica.length)=="1")) {
							sqrGroup.val(valoareAutocompletataText);
						} else if (valoareAutocompletataText.substring(1,2)=="L"){
							sqrGroup.val("L");
						}else if (valoareAutocompletataText.substring(3,4)=="3" && parseInt(String(valGrupaSintetica.length)=="5")){
							sqrGroup.val("303");
						}				
					}
					});
				}
			});
		}
);

//nomenclator reguli de inventar : FORMARE NUMAR DE INVENTAR
Liferay.Portlet.ready(
		function(portletId, node) {
			AUI().use('aui-node',function(A) {
				var valSqrGroup = $('#_' + portletId +"_sqr_group").val();
				var valStoreCode = $('#_' + portletId +"_store_code_no").val();
				
				
				var valLastInvNo  = $('#_' + portletId +"_last_inv_no").val();
				var noOfZeros = "";
				var dim = 	parseInt(9 - String(valLastInvNo).length);
				for (i = 0; i < dim; i++)	{
					noOfZeros = noOfZeros + "0";
				}				
				var concatenate = String(valStoreCode) + "" + noOfZeros + "" + String(valLastInvNo);								
				
				
				var inventoryNo = $('#_'+ portletId + '_inventory_no');
				inventoryNo.val(String(valSqrGroup) + concatenate);
			});
		}
	);

/*
Liferay.Portlet.ready(
		function(portletId, node){
			AUI.use('aui-node', function(A){
				var prefixMagazin = $('#_' + portletId + '_store_pos');
							
				// bind event on change
				
				prefixMagazin.on('change', function(){
					var pozPrefixSelectat = $('#_' + portletId + '_store_pos').val();
					var selectedVal = this.val();
					if (selectedVal != 0) {
						var codMagazin = "9152" ;
						var stocheaza_prefix="";
						if (pozPrefixSelectat = "0") {
								stocheaza_prefix=codMagazin.substring(0,3);
						}else if(pozPrefixSelectat="1"){
								stocheaza_prefix=codMagazin.substring(1,4);
							}
															
					}
					
					console.log(stocheaza_prefix);
				});
				})
		});


*/


//======================================================================================================================================
//													FUNCTIONS MOVED FROM ALOCARE_FACTURA.JSP
//======================================================================================================================================

//returns the position of the store line that has the store unset
function checkStoresOnStoreLines(){
	var ml = dataTableSplit.data, msg = '', template = '';
	var result = 0;
	ml.each(function (item, i) {
		var data = item.getAttrs([ 'id_store']);
		if (data.id_store == "0") {
			result = i + 1;
			return;
		}
	});
	return result;
}

function checkStoresOnStoreLinesCashExp(){
	var ml = dataTableSplit.data, msg = '', template = '';
	var result = 0;
	ml.each(function (item, i) {
		var data = item.getAttrs([ 'id_store_1', 'id_store_2', 'id_store_3', 'id_store_4', 'id_store_5', 'id_store_6','id_store_7','id_store_8', 'id_store_9' ]);
		if (data.id_store_1 == "0" && data.id_store_2 == "0" && data.id_store_3 == "0" && data.id_store_4 == "0" &&
				data.id_store_5 == "0" && data.id_store_6 == "0" && data.id_store_7 == "0" && data.id_store_8 == "0" && data.id_store_9 == "0") {
			result = i + 1;
			return;
		}
	});
	return result;
}

function checkDepartmentId(){
	var ml = dataTableSplit.data, msg = '', template = '';
	var result = 0;
	ml.each(function (item, i) {
		var data = item.getAttrs([ 'id_aisle' ]);
		if (data.id_aisle == "0" ) {
			result = i + 1;
			return;
		}
	});
	return result;
}

function checkGestiune(){
	var ml = dataTableSplit.data, msg = '', template = '';
	var result = 0;
	ml.each(function (item, i) {
		var data = item.getAttrs([ 'gestiune' ]);
		if (data.gestiune == "0" || data.gestiune == 0) {
			result = i + 1;
			return;
		}
	});
	return result;
}

function checkPifDateCashExp(){
	var ml = dataTableSplit.data, msg = '', template = '';
	var result = 0;
	ml.each(function (item, i) {
		var data = item.getAttrs([ 'inventory' ]);
		if (data.inventory == '' ) {
			result = i + 1;
			return;
		} else {
			var dataInventory = JSON.parse(data.inventory);
			for(var j = 0 ; j < dataInventory.length ; j++ ){
				if(dataInventory[j].data_pif == '') {
					result = i + 1;
					return;
				}
			}
		}
	});
	return result;
}

function alocateAllDpi(portletId){
	var idDpi = $('#' + portletId + 'alocare_dpi').val();
	var hasDpi = true;
	var rows = [];
	var hasStoreLines = false;
	if(!idDpi || idDpi == "" || idDpi == "0") {
		hasDpi = false;
	}
	
	if(hasDpi){
		var ml = dataTableSplit.data, msg = '', template = '';
		ml.each(function (item, i) {
		var data = item.getAttrs(['id_store_line', 'id_store', 'delete_line', 'index', 'article_code', 'description', 'quantity', 'price_no_vat_curr','id_store_1',
 	    		                 'id_store_2', 'id_store_3', 'id_store_4', 'id_store_5', 'id_store_6','id_store_7','id_store_8', 'id_store_9', 'id_department', 
 	                             'associated_acc', 'id_tip_inreg', 'id_act_mf', 'id_dpi', 'id_tip_inventar', 'unit_price', 'product_code', 'nr',
 	                             'currency', 'inv_number', 'line_id', 'price_no_vat_ron', 'vizualizare_dpi', 'cont_imob_in_curs', 'cont_imob_in_fct', 
 	                             'cont_imob_avans', 'id_cat_gestiune', 'detalii', 'inventory', 'initiative', 'new_prj']);
			hasStoreLines = true;
			data.id_dpi = idDpi;
			data.vizualizare_dpi = idDpi;
			rows.push(data);
		});	
		if(!hasStoreLines){
			alert("Nu a fost alocata nici o linie. Va rugam alocati cel putin o linie.");
		} else {
			 dataTableSplit.set('recordset', rows);
		}
		$('.stores_' + $('#' + portletId + 'company').val()).show();
	} else {
		alert("Va rugam selectati DPI-ul.");
	}
}

//set payment values ron and currency
function setPaymentValues(portletId) {
	
	var ml = dataTable.data, msg = '', template = '';
	//retrieve the exchange rate
	var exchange = $('#' + portletId + 'rata-schimb').val();
	//retrieve the currency
	var currency = $('#' + portletId + 'currency').val();
	//retrieve the ron value with vat
	var	valueRON = $('#' + portletId + 'val_with_vat_ron').val().replace(toReplace, "");
	var	valueCURR = $('#' + portletId + 'val_with_vat_curr').val().replace(toReplace, "");
	//calculate the payment value ron
	var paymentRON = parseFloat(parseFloat(valueRON));
	var paymentCURR = parseFloat(parseFloat(valueCURR));
	
	var sum;
	
	if (currency == 'RON'){
		sum = paymentRON;
	} else {
		sum = paymentCURR;
	}
    ml.each(function (item, i) {
    	var data = item.getAttrs(['invoice_line_id', 'delete_line', 'counter', 'line', 'inv_number', 'unit_price_ron', 'price_no_vat_ron', 'price_vat_ron', 'price_vat_curr', 'currency',  'article_code', 'product_code', 
	          		                 'description', 'um', 'quantity', 'unit_price_curr', 'price_no_vat_curr', 'vat', 'val_cu_tva', 'warranty',
		           		                'id_ias', 'id_ifrs', 'dt_ras', 'dt_ifrs', 'lot', 'clasif', 'lot_warranty', 'cont_imob_in_fct', 
		           		                'cont_imob_in_curs', 'cont_imob_avans', 'detalii']);
    	//adaugare valoare cu tva
    	//da NAN pentru ca val_cu_vat are virgula
//    	if(servicii.indexOf(data.description.toString().toLowerCase()) < 0) {
//    		sum = sum + parseFloat(data.val_cu_tva);
//    	}
    	//scadere valoare garantie
    	if (data.warranty != 0) {
   			sum = sum - parseFloat(data.warranty);
   		}
    });
   
    if(currency != 'RON'){
    	//set the payment value currency
    	$('#' + portletId + 'val-netaplata-eur').val(sum.toFixed(4));
        $('#' + portletId + 'val-netaplata-eur').autoNumeric('update', {aSep: ',', mDec: 4, vMin : -999999999, vMax : 999999999 });
      //set the payment value ron
        $('#' + portletId + 'val-netaplata-ron').val((sum * exchange).toFixed(4));
    	$('#' + portletId + 'val-netaplata-ron').autoNumeric('update', {aSep: ',', mDec: 4, vMin : -999999999, vMax : 999999999 });
    } else {
    	 //set the payment value ron
        $('#' + portletId + 'val-netaplata-ron').val(sum.toFixed(4));
    	$('#' + portletId + 'val-netaplata-ron').autoNumeric('update', {aSep: ',', mDec: 4, vMin : -999999999, vMax : 999999999 });
    }
    calculeazaTvaPeStoreLines(portletId);
}

//calculate total no vat SUM for all store lines and  computes the difference between
//total no vat for all invoice lines and SUM
function setTotalNoVatLines(portletId){
	var ml = dataTableSplit.data, msg = '', template = '';
	var sum = 0;	
	//retrieve the currency
	var invoice_currency = $('#' + portletId + 'currency').val();
	//retrive the sum of all store lines 
	var invoice_value = 0;
	if(invoice_currency == 'RON') {
		invoice_value = $('#' + portletId + 'val_no_vat_ron');
	} else {
		invoice_value = $('#' + portletId + 'val_no_vat_curr');
	}
	//reset value
	if($('#' + portletId + 'val_no_vat_aloc_linii')){
		$('#' + portletId + 'val_no_vat_aloc_linii').val("");
    }
	ml.each(function (item, i) {
    	var data = item.getAttrs(['price_no_vat_curr']);
    	console.log("Suma e " + sum);
    	sum = parseFloat(parseFloat(sum) + parseFloat(data.price_no_vat_curr)).toFixed(4);
    	//console.log(sum);
    	//console.log(data.price_no_vat_curr);
    });
	console.log("Suma finala " + sum);
	//total sum no vat
	var total = $('#' + portletId + 'val_no_vat_art').val().replace(toReplace,'');
	var difference = $('#' + portletId + 'difference');
	//set difference
	if(difference){
		//console.log("TOTAL -> " + parseFloat(total));
		//console.log("sum -> " + parseFloat(sum));
		var dif = parseFloat(parseFloat(total) - parseFloat(sum)).toFixed(4);
		//console.log("DIF -> " + dif);
		difference.val(dif);
		if(dif != 0 ){
			difference.css("border-color", "#FE0000");
		}else difference.css("border-color", "#488F06");
		
	}
    if($('#' + portletId + 'val_no_vat_aloc_linii')){
    	$('#' + portletId + 'val_no_vat_aloc_linii').val(sum);
    	if(parseFloat(invoice_value.val().replace(toReplace,'')) != parseFloat(sum)){
    		$('#' + portletId + 'val_no_vat_aloc_linii').css("border-color", "#FE0000");
    		$('#' + portletId + 'val_no_vat_ron').css("border-color", "#FE0000");
    		if (invoice_currency != "RON"){
    			$('#' + portletId + 'val_no_vat_curr').css("border-color", "#FE0000");
    		}
    	} else {
    		$('#' + portletId + 'val_no_vat_aloc_linii').css("border-color", "#488F06");
    		$('#' + portletId + 'val_no_vat_ron').css("border-color", "#488F06");
    		if (invoice_currency != "RON"){
    			$('#' + portletId + 'val_no_vat_curr').css("border-color", "#488F06");
    		}
    	}
    }
	$('#' + portletId + 'difference').autoNumeric('update', {aSep: ',', mDec: 4, vMin : -999999999, vMax : 999999999 });
	$('#' + portletId + 'val_no_vat_aloc_linii').autoNumeric('update', {aSep: ',', mDec: 4, vMin : -999999999, vMax : 999999999 });
}

function makeAjaxCallToDeleteLine(lineId, ajaxURL, portletNamespace){
	$.ajax({
		type: "POST",
		url: ajaxURL,
		data: 	portletNamespace + "line_id=" + lineId + 
				"&" + portletNamespace + "action=delete_line",
	success: function(msgjson) { }
	});
}

function makeAjaxCallToDeleteStoreLine(storeLineId, ajaxURL, portletNamespace){
	$.ajax({
		type: "POST",
		url: ajaxURL,
		data: 	portletNamespace + "store_line_id=" + storeLineId + 
				"&" + portletNamespace + "action=delete_store_line",
	success: function(msgjson) { }
	});
}

//invoice_store_line calculate val no vat if the quantity changes
function setValNoVatStoreLine(portletId){
	var rows = [];
	//all store lines
	var ml = dataTableSplit.data, msg = '', template = '';
	//all lines
	var mlh = dataTable.data;
	var error_line = 0;
	ml.each(function (item, i) {
		//for each store line
    	var data = item.getAttrs(['id_store_line', 'id_store', 'delete_line', 'index', 'article_code', 'description', 'quantity', 'price_no_vat_curr','id_store_1',
    	    		                 'id_store_2', 'id_store_3', 'id_store_4', 'id_store_5', 'id_store_6','id_store_7','id_store_8', 'id_store_9', 'id_department', 
    	                             'associated_acc', 'id_tip_inreg', 'id_act_mf', 'id_dpi', 'id_tip_inventar', 'unit_price', 'product_code', 'nr',
    	                             'currency', 'inv_number', 'line_id', 'price_no_vat_ron', 'vizualizare_dpi', 'cont_imob_in_curs', 'cont_imob_in_fct', 
    	                             'cont_imob_avans', 'id_cat_gestiune', 'detalii', 'inventory', 'initiative', 'new_prj']);
    	var unit_price = 0; 
    	
    	//find  the line for each store line
		mlh.each(function (itemH,j){
    		var dataH = itemH.getAttrs(['line','unit_price_curr','quantity']);
    		if(data.nr == dataH.line){
    			//get the modified unit price from the invoice line
    			unit_price = dataH.unit_price_curr;
    			//cant = dataH.quantity;
    			//line = dataH.line;
    			return false;
    		}
    	});
    	
    	//for each invoice store line recalculate price no vat with the new unit price
		if(unit_price > 0){
			data.price_no_vat_curr = parseFloat(parseFloat(data.quantity) * parseFloat(unit_price)).toFixed(4);
		} else {
			//for prorata or discount
			data.price_no_vat_curr = parseFloat(unit_price).toFixed(4);
		}
    	// set value on matrix
    	rows.push(data);
	});	
	// set value on table
    dataTableSplit.set('recordset', rows);
	//recalculate total no vat for all store lines
   	setTotalNoVatLines(portletId);
	//recalculate payment value
    //setPaymentValues(portletId);
    //show the stores column 
	$('.stores_' + selectedCompany).removeClass('hiddencol');
}

//return true if quantity from an invoice line >= sum(quantity) from invoice_store_line
function validQuantity(cant, line){
	//all store lines
	var ml = dataTableSplit.data, msg = '', template = '';
	var sum = 0;
	ml.each(function (item, i) {
		//for each store line
	var data = item.getAttrs(['id_store_line', 'id_store', 'delete_line', 'index', 'article_code', 'description', 'quantity', 'price_no_vat_curr','id_store_1',
	    		                 'id_store_2', 'id_store_3', 'id_store_4', 'id_store_5', 'id_store_6','id_store_7','id_store_8', 'id_store_9', 'id_department', 
	                             'associated_acc', 'id_tip_inreg', 'id_act_mf', 'id_dpi', 'id_tip_inventar', 'unit_price', 'product_code', 'nr',
	                             'currency', 'inv_number', 'line_id', 'price_no_vat_ron', 'vizualizare_dpi', 'cont_imob_in_curs', 'cont_imob_in_fct', 
	                             'cont_imob_avans', 'id_cat_gestiune', 'detalii', 'inventory']);
//		console.log("Data.nr: " +data.nr);
//		console.log("line :" + line);
		if(parseFloat(data.nr) == parseFloat(line)){
			sum = parseFloat(parseFloat(sum) + parseFloat(data.quantity)).toFixed(4);
		}
	});
	if(sum > parseFloat(cant) || sum < parseFloat(cant)){
		console.log("Invalid");
		return false;
	} else {
		console.log("Valid");
		return true;
	}
}

//if the user changes the reg. type or the mf action => the associated account must change
function setAssociatedAccount(){
	var rows = [];
	//all store lines
	var ml = dataTableSplit.data, msg = '', template = '';
	
	ml.each(function (item, i) {
		//for each store line
		var data = item.getAttrs(['id_store_line', 'id_store', 'delete_line', 'index', 'article_code', 'description', 'quantity', 'price_no_vat_curr','id_store_1',
		    		                 'id_store_2', 'id_store_3', 'id_store_4', 'id_store_5', 'id_store_6','id_store_7','id_store_8', 'id_store_9', 'id_department', 
		                             'associated_acc', 'id_tip_inreg', 'id_act_mf', 'id_dpi', 'id_tip_inventar', 'unit_price', 'product_code', 'nr',
		                             'currency', 'inv_number', 'line_id', 'price_no_vat_ron', 'vizualizare_dpi', 'cont_imob_in_curs', 'cont_imob_in_fct', 
		                             'cont_imob_avans', 'id_cat_gestiune', 'detalii', 'inventory', 'initiative', 'new_prj']);
		if (data.id_act_mf == '4' ){
			// tipul de actiune mf este "avans"
			data.associated_acc = data.cont_imob_avans;
		} else if (data.id_tip_inreg == '1' || data.id_tip_inreg == '3'){
			//tipul de inregistrare este "obiect de inventar in curs" sau "mijloc fix in curs"
			data.associated_acc = data.cont_imob_in_curs;
		} else { 
			//tipul de inregistrare este "obiect de inventar in functiune" sau "mijloc fix in functiune"
			data.associated_acc = data.cont_imob_in_fct;
		}
		// set value on matrix
		rows.push(data);
	});
	// set value on table
	dataTableSplit.set('recordset', rows);
	//show the stores column 
	$('.stores_' + selectedCompany).removeClass('hiddencol');
}

//if user changes the number of dpi, change the link also
function setDPILink(dpiId) {
	var rows = [];
	//all store lines
	var ml = dataTableSplit.data, msg = '', template = '';
	
	ml.each(function (item, i) {
		//for each store line
		var data = item.getAttrs(['id_store_line', 'id_store', 'delete_line', 'index', 'article_code', 'description', 'quantity', 'price_no_vat_curr','id_store_1',
		    		                 'id_store_2', 'id_store_3', 'id_store_4', 'id_store_5', 'id_store_6','id_store_7','id_store_8', 'id_store_9', 'id_department', 
		                             'associated_acc', 'id_tip_inreg', 'id_act_mf', 'id_dpi', 'id_tip_inventar', 'unit_price', 'product_code', 'nr',
		                             'currency', 'inv_number', 'line_id', 'price_no_vat_ron', 'vizualizare_dpi', 'cont_imob_in_curs', 'cont_imob_in_fct', 
		                             'cont_imob_avans', 'id_cat_gestiune', 'detalii', 'inventory', 'initiative', 'new_prj']);
		
		if (data.id_dpi == dpiId) {
			data.vizualizare_dpi = dpiId;
		}
		
		// set value on matrix
		rows.push(data);
	});
	
	// set value on table
	dataTableSplit.set('recordset', rows);
	//show the stores column 
	$('.stores_' + selectedCompany).removeClass('hiddencol');
}

//invoice_line recalculate val with vat when the vat changes
function setValueWithVatLine(portletId){
	var rows = [];
	var ml = dataTable.data, msg = '', template = '';
	ml.each(function (item, i) {
		var data = item.getAttrs(['invoice_line_id', 'delete_line', 'counter', 'line', 'inv_number', 'unit_price_ron', 'price_no_vat_ron', 'price_vat_ron', 'price_vat_curr', 'currency',  'article_code', 'product_code', 
		  		                 'description', 'um', 'quantity', 'unit_price_curr', 'price_no_vat_curr', 'vat', 'val_cu_tva', 'warranty',
		   		                'id_ias', 'id_ifrs', 'dt_ras', 'dt_ifrs', 'lot', 'clasif', 'lot_warranty', 'cont_imob_in_fct', 
		   		                'cont_imob_in_curs', 'cont_imob_avans', 'detalii']);
		data.val_cu_tva = parseFloat(parseFloat(data.price_no_vat_curr) + parseFloat(data.price_no_vat_curr) * parseFloat(data.vat) / 100).toFixed(4);
		// set value on matrix
		rows.push(data);
	});
	// set value on table
    dataTable.set('recordset', rows);
    calculeazaTvaPeStoreLines(portletId);
}


//calculate total no vat for lines
function setTotalNoVat(portletId){
	var ml = dataTable.data, msg = '', template = '';
	var sum = 0;
	//console.log("merge");
	//reset value
	//console.log($('#' + portletId + 'val_no_vat_art'));
	
	if($('#' + portletId + 'val_no_vat_art')){
		$('#' + portletId + 'val_no_vat_art').val(0);
	}
	
	ml.each(function (item, i) {
		var data = item.getAttrs(['invoice_line_id', 'delete_line', 'counter', 'line', 'inv_number', 'unit_price_ron', 'price_no_vat_ron', 'price_vat_ron', 'price_vat_curr', 'currency',  'article_code', 'product_code', 
		  		                 'description', 'um', 'quantity', 'unit_price_curr', 'price_no_vat_curr', 'vat', 'val_cu_tva', 'warranty',
		   		                'id_ias', 'id_ifrs', 'dt_ras', 'dt_ifrs', 'lot', 'clasif', 'lot_warranty', 'cont_imob_in_fct', 
		   		                'cont_imob_in_curs', 'cont_imob_avans', 'detalii']);
		if(servicii.indexOf(data.description.toString().toLowerCase()) < 0){
			sum = parseFloat(parseFloat(sum) + parseFloat(data.price_no_vat_curr)).toFixed(4);
		}
	});

	if($('#' + portletId + 'val_no_vat_art')){
		$('#' + portletId + 'val_no_vat_art').val(sum);
		$('#' + portletId + 'val_no_vat_art').autoNumeric('update', {aSep: ',', mDec: 4, vMin : -999999999, vMax : 999999999 });
	}
}

//set all line values based on quantity and price per unit 
function setLineValues(quantity, pricePerUnit, portletId) {
	if (pricePerUnit != '' && quantity != '') {
		var rows = [];
	    var ml = dataTable.data, msg = '', template = '';
	    ml.each(function (item, i) {
	    	var data = item.getAttrs(['invoice_line_id', 'delete_line', 'counter', 'line', 'inv_number', 'unit_price_ron', 'price_no_vat_ron', 'price_vat_ron', 'price_vat_curr', 'currency',  'article_code', 'product_code', 
	    	  		                 'description', 'um', 'quantity', 'unit_price_curr', 'price_no_vat_curr', 'vat', 'val_cu_tva', 'warranty',
	    	   		                'id_ias', 'id_ifrs', 'dt_ras', 'dt_ifrs', 'lot', 'clasif', 'lot_warranty', 'cont_imob_in_fct', 
	    	   		                'cont_imob_in_curs', 'cont_imob_avans', 'detalii']);
	        if (data.quantity == quantity && data.unit_price_curr == pricePerUnit) {
	        	data.price_no_vat_curr = parseFloat(parseFloat(quantity) * parseFloat(pricePerUnit)).toFixed(4);
		        data.val_cu_tva = parseFloat(parseFloat(data.price_no_vat_curr) + parseFloat(quantity) * parseFloat(pricePerUnit) * parseFloat(data.vat) / 100).toFixed(4);
		        if(data.lot_warranty == "Y") {
	        		data.warranty = parseFloat($('#' + portletId + 'procent_garantie').val() / 100 * data.price_no_vat_curr).toFixed(4);
	        	} else {
	        		data.warranty = 0;
	        	}
	        }
        	// set value on matrix
        	rows.push(data);

	    });
	}
	// set value on table
    dataTable.set('recordset', rows);
  	//calculate total no vat for all articles
    setTotalNoVat(portletId);
  	//recalculate total no vat for all store lines
    setValNoVatStoreLine(portletId);
 	 //recalculate total no vat for all store lines an set the result 
  	setTotalNoVatLines(portletId);
  	//recalculate payment values
  	setPaymentValues(portletId);
	
}

//check if line has at least one store line
function checkAllocatedStoreLine(lineNumber){
	var result = false;
	var mls = dataTableSplit.data, msg = '', template = '';
	mls.each(function (item, i) {
		//for each store line
		var data = item.getAttrs(['nr']);
		if (data.nr == lineNumber){
			result = true;
			return;
		}
	});
	return result;
}

function checkAllocatedVouchers(lineNumber){
	var result = false;
	var mls = dataTableSplit.data, msg = '', template = '';
	mls.each(function (item, i){
		var data = item.getAttrs(['line']);
		if(data.line == lineNumber) {
			result = true;
			return;
		}
	});
}

function createStoreLine(data, portletId, storeid, multiplyRow){
	
	var remoteDataSplit = dataTableSplit.data.toArray();
	if (multiplyRow == ""){
		//contains prorata or discount
		var prorata = false;
		//in case prorata == true save price no vat
		var associated_acc = "";
		//store the unit price
		var priceNoVat = 0;
		//store the mf action type
		var mf_action = 1;
		//exchange rate
	    var exch_rate = $('#' + portletId + 'rata-schimb').val();
	    //currency
	    var selected_currency = $('#' + portletId + 'currency');
	    
	    //check if the description was selected
		if (data.description == "" || data.description == 'null') {
	    	alert('Va rugam sa completati campul "Denumire produs" pe linia ' + data.counter + ' !');
	    	return false;
	    }
		
		if(data.product_code == 3562) {
			prorata = true;
		}
		
	    if (data.description.toLowerCase().indexOf("discount") < 0	){
		    //check if the product was selected
		    if (data.product_code == "0" || data.product_code == 'null'){
		    	alert('Va rugam sa completati campul "Cod produs" pe linia ' + data.counter + ' !');
		    	return false;
		    }
		    
			//check if the quantity was set
		    if (data.quantity == "" || data.quantity == 'null') {
		    	alert('Va rugam sa completati campul "Cantitate" pe linia ' + data.counter + ' !');
		    	return false;
		    }
		    //check if the unit price was not set
		    if (data.price_no_vat_curr == "" || data.price_no_vat_curr == 'null') {
		    	alert('Va rugam sa completati campul "Pret unitar" pe linia ' + data.counter + ' !');
		    	return false;
		    }
		    //if the lot allows warranty then check that it was set
	//	    if (data.lot_warranty == 'Y' && ( data.warranty == "" || data.warranty == 'null')){
	//	    	alert('Va rugam sa completati campul "Garantie" pe linia ' + data.counter + ' !');
	//	    	return false;
	//	    }
	//	    if (data.lot_warranty == 'Y' &&  data.warranty == "0"){
	//	    	alert('Valoarea campului "Garantie" pe linia ' + data.counter + ' este 0! Va rugam sa modificati !');
	//	    	return false;
	//	    }
//	    if (prorata == true){
//	    	alert("Articolul de pe linia " + data.counter + " nu poate fi alocat!");
//	    	return true;
//	    }
	    }
	    var detaliiProrata = storeLineCounter + 1;
	    if(prorata == true) {
	    	detaliiProrata = '';
	    }
	    
		var tipInregistrare = 1;
	    //if currency is RON
	    if(selected_currency == "RON"){
		//if unit prince > 2500
		if (parseFloat(data.unit_price_curr) > 2500) {
			//tip inregistrare este obiect de inventar in curs
				tipInregistrare = 3;
			} else {
				tipInregistrare = 1;
			}
		 } else { 
			var ronValue = parseFloat(parseFloat(data.unit_price_curr) * parseFloat(exch_rate)).toFixed(noOfDecimals);
			if (ronValue > 2500) {
				//tip inregistrare este obiect de inventar in curs
				tipInregistrare = 3;
			} else {
				tipInregistrare = 1;
			}
		 }
	
		priceNoVat = data.price_no_vat_curr;
		associated_acc = data.cont_imob_in_curs;
		var tipInv = 2;
	    if (prorata == true){
	    	tipInv = 0;
	    	detaliiProrata = '';
	    	if (priceNoVat == 0){
	    		priceNoVat = data.unit_price_curr;
	    	}
	    	associated_acc = "40170000";
	    	mf_action = 3;
	    }
	    
	 	remoteDataSplit.push({
			id_store: storeid, /* get store ID by GLN, returns 1 in case of missing GLN or no GLN found */
			delete_line: storeLineCounter + 1,
			index : remoteDataSplit.length + 1,
			inv_number : data.inv_number,
			line_id : '',
			nr: data.line,
			article_code : data.article_code ,
			product_code : data.product_code,
			price_no_vat_ron : '',
			unit_price : data.unit_price_curr,
			currency : data.currency,
			description: data.description,
			quantity: data.quantity,
			price_no_vat_curr: priceNoVat,
			id_store_1: (selectedCompany != 1) ? 0 : storeid,
			id_store_2: (selectedCompany != 2) ? 0 : storeid,
			id_store_3: (selectedCompany != 3) ? 0 : storeid,
			id_store_4: (selectedCompany != 4) ? 0 : storeid,
			id_store_5: (selectedCompany != 5) ? 0 : storeid,
			id_store_6: (selectedCompany != 6) ? 0 : storeid,
			id_store_7: (selectedCompany != 7) ? 0 : storeid,
			id_store_8: (selectedCompany != 8) ? 0 : storeid,
			id_store_9: (selectedCompany != 9) ? 0 : storeid,
			id_department: 91,
			associated_acc: associated_acc,
			id_tip_inreg: tipInregistrare,
			id_act_mf: mf_action,
			id_cat_gestiune : 0,
			id_dpi: 0,
			vizualizare_dpi: 0,
			id_tip_inventar: tipInv,
			cont_imob_in_fct : data.cont_imob_in_fct,
			cont_imob_in_curs : data.cont_imob_in_curs,
			cont_imob_avans : data.cont_imob_avans,
			detalii: detaliiProrata,
			inventory : ''
		});
	} else {
		remoteDataSplit.push(multiplyRow);
	}
	dataTableSplit.set('recordset', remoteDataSplit);
	$('.stores_' + selectedCompany).removeClass('hiddencol');
	storeLineCounter = storeLineCounter +1;
	calculeazaTvaPeStoreLines(portletId);
    return true;
}


//ALOCATE NEW INVOICE LINE
function createNewLineTopTable(type, nextLineNumber, portletNamespace) {
	console.log("Counter invoice lines = " + counter);
	console.log("NextLineNumber = " + nextLineNumber);
	remoteData = dataTable.data.toArray();
	//debugger;
	if(counter == 0){
		counter = nextLineNumber;
	}
	
	nextLineNumber = counter;
	
	//remoteData = remoteData._state.data;
	//console.log(remoteData);
	var name = "PRORATA";
	if(type == "article"){
		name = "";
	}
	
	var detail = '';
	if(type == "servici") {
		name = "SERVICII";
		detail = nextLineNumber + ',servici';
	} else {
		detail = nextLineNumber + ',produs';
	}
	
	var cotaTVA = $('#' + portletNamespace + 'cota_tva').val();
	if($('#' + portletNamespace + 'supplier_type_invoice').val() == 1) {
		cotaTVA = "0.00";
	} else if ($('#' + portletNamespace + 'supplier_type_invoice').val() == 0) {
		if($('#' + portletNamespace + 'intern_invoice_option').val() == 4) {
			cotaTVA = "0.00";
		}
	}
	
	remoteData.push({
		invoice_line_id: '',
		delete_line: nextLineNumber,
		counter    : remoteData.length + deletedCounter,
		line: nextLineNumber, 
		inv_number : '',
		unit_price_ron : '',
		price_no_vat_ron : '',
		price_vat_ron : '',
		price_vat_curr : '',
		currency:'',
		article_code: '0', 
		product_code: '0', 
		description: name,
		um: 'Buc.',
		quantity: '',
		unit_price_curr: '',
		price_no_vat_curr: '',
		vat: cotaTVA,
		val_cu_tva: '',
		warranty: 0,
		id_ias: '0',
		id_ifrs: '0',
		dt_ras: '',
		dt_ifrs: '',
		lot: '',
		clasif: 0,
		lot_warranty: 0,
		cont_imob_in_fct : 0,
		cont_imob_in_curs : 0,
		cont_imob_avans : 0,
		detalii: detail
	});
	counter++;
	dataTable.set('recordset', remoteData);
	$('#'+ portletNamespace + 'val_no_vat_art').autoNumeric('update', {aSep: ',', mDec: 4, vMin : -999999999, vMax : 999999999 });
	$('#'+ portletNamespace + 'val_no_vat_aloc_linii').autoNumeric('update', {aSep: ',', mDec: 4, vMin : -999999999, vMax : 999999999 });
	$('#'+ portletNamespace + 'difference').autoNumeric('update', {aSep: ',', mDec: 4, vMin : -999999999, vMax : 999999999 });
}

function allocateAllArticles(portletId, storeid, servicii){
	//check if the company was selected
	if ($('#' + portletId +'company').val() == ""){
		alert ("Va rugam selectati o societate!");
		return false;
	}
	
	//all invoice lines
	var ml = dataTable.data, msg = '', template = '';
	//used for breaking the loop
	var missingLine = -1;
	if (ml == ""){
		alert("Atentie! Nu a fost adaugat niciun articol!");
	} else {
		ml.each(function (item, i) {
			if (missingLine == -1) {
				//for each line
				var data = item.getAttrs(['invoice_line_id', 'delete_line', 'counter', 'line', 'inv_number', 'unit_price_ron', 'price_no_vat_ron', 'price_vat_ron', 'price_vat_curr', 'currency',  'article_code', 'product_code', 
			    	  		                 'description', 'um', 'quantity', 'unit_price_curr', 'price_no_vat_curr', 'vat', 'val_cu_tva', 'warranty',
			    	   		                'id_ias', 'id_ifrs', 'dt_ras', 'dt_ifrs', 'lot', 'clasif', 'lot_warranty', 'cont_imob_in_fct', 
			    	   		                'cont_imob_in_curs', 'cont_imob_avans', 'detalii']);
				//check if this line has at least one store line allocated
				var alreadyAllocated = checkAllocatedStoreLine(data.line);
				if (!alreadyAllocated){
					//allocate article
					if(servicii.indexOf(data.description.toString().toLowerCase()) < 0 && data.description.toLowerCase().indexOf("servicii") < 0) {
						var result = createStoreLine(data, portletId, storeid, "");
					}
					//if the process encountered an error ( meaning that the invoice line was not complete)
					if (!result){
						missingLine = data.line;
						return false;
					}
				}
			}
		});
		
		//setTotalNoVat(portletId);
		//calculeaza total fara tva alocat pe linii
	    setTotalNoVatLines(portletId);
	    //recalculate payment values
	    //setPaymentValues(portletId);
	}
	
}


function allocateThemAll(portletId){
	
	//all invoice lines
	var ml = dataTable.data, msg = '', template = '';
	//used for breaking the loop
	var missingLine = -1;
	if (ml == ""){
		alert("Atentie! Nu a fost adaugat niciun articol!");
	} else {
		ml.each(function (item, i) {
			if (missingLine == -1) {
				var data = item.getAttrs( ['id_voucher', 'delete_line', 'line', 'article_code', 'product_code', 'description', 
				  		                 'um', 'quantity', 'unit_price_ron', 'price_no_vat_ron', 'price_vat_ron', 'vat', 
						                 'price_with_vat_ron', 'lot', 'ras', 'ifrs', 'dt_ras', 'dt_ifrs', 
						                 'is_it', 'detalii', 'cont_imob_in_fct', 
						                 'cont_imob_in_curs', 'cont_imob_avans']);
				var isAllocated = checkAllocatedVouchers(data.line);
				if ( !isAllocated ){
					createNewStoreLineVouchers(data.line);
				} 
			}
		});
	}
}

function updateVatWithTvaCashExp(vat, lineId){
	var rows = [];
	var ml = dataTable.data, msg = '', template = '';
	ml.each(function (item, i) {
		var data = item.getAttrs( ['id_cash_exp', 'cash_exp_line_id', 'delete_line', 'line', 'counter', 'product_code', 'description', 'um', 'quantity', 'unit_price_curr', 
	     			                 'price_no_vat_curr', 'price_vat_curr', 'vat_code', 'price_with_vat_curr', 'id_ias', 'id_ifrs', 'dt_ias', 'dt_ifrs', 'lot', 'clasif', 
	    			                 'detalii', 'cont_imob_in_fct', 'cont_imob_in_curs', 'cont_imob_in_avans', 'is_new_line']);
			if(data.cash_exp_line_id == lineId) {
				data.price_vat_curr = parseFloat((parseFloat(vat)/100)*parseFloat(data.price_no_vat_curr)).toFixed(4);
				data.price_with_vat_curr = parseFloat(parseFloat(data.price_no_vat_curr) + parseFloat(data.price_vat_curr)).toFixed(4);
			}
			rows.push(data);
	});
	dataTable.set('recordset', rows);
}

function allocateThemAllCashExp(portletId){
	
	//all invoice lines
	var ml = dataTable.data, msg = '', template = '';
	//used for breaking the loop
	var missingLine = -1;
	if (ml == ""){
		alert("Atentie! Nu a fost adaugat niciun articol!");
	} else {
		ml.each(function (item, i) {
			if (missingLine == -1) {
				var data = item.getAttrs( ['id_cash_exp', 'cash_exp_line_id', 'delete_line', 'line', 'counter', 'product_code', 'description', 'um', 'quantity', 'unit_price_curr', 
			     			                 'price_no_vat_curr', 'price_vat_curr', 'vat_code', 'price_with_vat_curr', 'id_ias', 'id_ifrs', 'dt_ias', 'dt_ifrs', 'lot', 'clasif', 
			    			                 'detalii', 'cont_imob_in_fct', 'cont_imob_in_curs', 'cont_imob_in_avans', 'is_new_line']);
				var isAllocated = checkAllocatedCashExp(data.cash_exp_line_id);
				if ( !isAllocated ){
					createNewStoreLineCashExp(data.cash_exp_line_id, portletId);
				} 
			}
		});
	}
}

function checkAllocatedCashExp(lineId){
	var isAllocated = false;
	var ml = dataTableSplit.data, msg = '', template = '';
	ml.each(function (item, i) {
		var data = item.getAttrs( ['id_cash_exp', 'id_cash_exp_line','id_store_line','delete_store_line', 'line', 'description','id_store_1','id_store_2','id_store_3','id_store_4','id_store_5',
					                 'id_store_6','id_store_7','id_store_8', 'id_store_9','quantity','price_no_vat_curr','id_aisle','associated_acc','id_tip_inreg','id_act_mf','gestiune','id_dpi','vizualizare_dpi',
					                 'id_tip_inventar','detalii','cont_imob_in_fct','cont_imob_in_curs','cont_imob_avans','inventory']);
			if(data.line == lineId) {
				isAllocated = true;
			}
	});
	return isAllocated;
}

function createNewStoreLineVouchers(lineId) {
    var ml = dataTable.data, msg = '', template = '';
    ml.each(function (item, i) {
        var data = item.getAttrs(['delete_line', 'id_voucher', 'line', 'article_code', 'product_code', 'description', 'um', 'quantity', 'unit_price_ron', 'price_no_vat_ron', 'vat', 
	         		                 'price_with_vat_ron', 'ras', 'ifrs', 'dt_ras', 'dt_ifrs', 'lot', 'is_it', 'detalii', 'cont_imob_in_fct', 'cont_imob_in_curs', 'cont_imob_avans']);
        
		//lines in second table are 1 on 1 with the entries in the 1st table
		// get the row on which it was clicked.
        if (data.line == lineId) {
	        if (data.description == "") {
	        	alert('Va rugam sa completati numele articolului!');
	        	return false;
	        }
	        if (data.quantity == "") {
	        	alert('Va rugam sa completati campul cantitate!');
	        	return false;
	        }
	        if (data.price_no_vat_ron == "") {
	        	alert('Va rugam sa completati campul valoare fara tva!');
	        	return false;
	        }
        	
			// DO not add same line twice
        	remoteDataSplit = dataTableSplit.data.toArray();
        	var addRow = true;
        	for (var k = 0; k < remoteDataSplit.length; k++) {
        		if (remoteDataSplit[k].get('line') == lineId) {
        			addRow = false;
        		}
        	}
        	if (addRow == true) {
        		var tipInregistrare = 1;
	        	//if unit prince > 2500
	        	if (parseFloat(data.unit_price_ron) > 2500) {
	        		//tip inregistrare este obiect de inventar in curs
	        		tipInregistrare = 3;
	        	} else {
	        		tipInregistrare = 1;
	        	}
	        	
	        	remoteDataSplit.push({
	        		delete_line : data.delete_line,
	   				line: data.line,
	   				article_code: data.article_code,
	   				description: data.description,
	   				quantity: data.quantity,
	   				price_no_vat_ron: data.price_no_vat_ron,
	   				associated_acc: data.cont_imob_in_curs,
	   				//id_tip_inreg: 1,
	   				id_tip_inreg: tipInregistrare,
	   				id_act_mf: 1,
	   				id_dpi: 0,
	   				vizualizare_dpi: 0,
	   				id_tip_inventar: 2, // de grup - default
	   				detalii: data.line,
	   				cont_imob_in_fct : data.cont_imob_in_fct,
	   				cont_imob_in_curs : data.cont_imob_in_curs,
	        		cont_imob_avans : data.cont_imob_avans,
	        		detalii: data.line,
	        		inventory: ''
	        	});
        	}
        }
        
    });
    // refresh table values
    dataTableSplit.set('recordset', remoteDataSplit);
}

function calculateCashExpValue(portletId, lineId){
	 var ml = dataTableSplit.data, msg = '', template = '';
	 var sum = 0;
	  ml.each(function (item, i) {
		  var data = item.getAttrs(['quantity', 'price_no_vat_curr']);
		  sum = parseFloat(parseFloat(sum) + parseFloat(data.quantity * data.price_no_vat_curr)).toFixed(4);
	  });
	  $('#' + portletId + 'valoare_totala_alocata').val(parseFloat(sum).toFixed(4));
	  $('#' + portletId + 'difference').val(parseFloat( parseFloat($('#' + portletId + 'valoare_totala').val()) - 0 - sum).toFixed(4));
	  
	  if ( $('#'+portletId + 'difference').val() == 0){
			$('#'+portletId + 'valoare_totala_alocata').css("border-color", "#488F06");
			$('#'+portletId + 'difference').css("border-color", "#488F06");
			$('#'+portletId + 'valoare_totala').css("border-color", "#488F06");
		} else {
			$('#'+portletId + 'valoare_totala_alocata').css("border-color", "#FE0000");
			$('#'+portletId + 'difference').css("border-color", "#FE0000");
			$('#'+portletId + 'valoare_totala').css("border-color", "#FE0000");
		}
}

function createNewStoreLineCashExp(lineId, portletId) {
	var isGoodStoreLine = true;
	var cashExpLineId = '';
    var ml = dataTable.data, msg = '', template = '';
    ml.each(function (item, i) {
        var data = item.getAttrs(['id_cash_exp', 'cash_exp_line_id', 'delete_line', 'line', 'counter', 'product_code', 'description', 'um', 'quantity', 'unit_price_curr', 
     			                 'price_no_vat_curr', 'price_vat_curr', 'vat_code', 'price_with_vat_curr', 'id_ias', 'id_ifrs', 'dt_ias', 'dt_ifrs', 'lot', 'clasif', 
    			                 'detalii', 'cont_imob_in_fct', 'cont_imob_in_curs', 'cont_imob_in_avans', 'is_new_line']);
        
		//lines in second table are 1 on 1 with the entries in the 1st table
		// get the row on which it was clicked.
        if (data.cash_exp_line_id == lineId) {
        	if(data.is_new_line != 1) {
        		cashExpLineId = data.cash_exp_line_id;
        	}
        	 if (data.product_code == 0) {
 	        	alert('Va rugam sa completati codul de produs!');
 	        	isGoodStoreLine = false;
 	        	return;
 	        }
	        if (data.description == "") {
	        	alert('Va rugam sa completati numele articolului!');
	        	isGoodStoreLine = false;
	        	return;
	        }
	        if (data.quantity == "") {
	        	alert('Va rugam sa completati campul cantitate!');
	        	isGoodStoreLine = false;
	        	return;
	        }
	        if (data.price_no_vat_curr == "") {
	        	alert('Va rugam sa completati campul valoare fara tva!');
	        	isGoodStoreLine = false;
	        	return;
	        }
        	
			// DO not add same line twice
        	remoteDataSplit = dataTableSplit.data.toArray();
        	var addRow = true;
        	for (var k = 0; k < remoteDataSplit.length; k++) {
        		if (remoteDataSplit[k].get('id_cash_exp_line') == lineId) {
        			addRow = false;
        		}
        	}
        	if (addRow == true) {
        		var tipInregistrare = 1;
	        	//if unit prince > 2500
	        	if (parseFloat(data.unit_price_curr) > 2500) {
	        		//tip inregistrare este obiect de inventar in curs
	        		tipInregistrare = 3;
	        	} else {
	        		tipInregistrare = 1;
	        	}
	        	
	        
	        	
	        	remoteDataSplit.push({
	        		id_cash_exp : data.id_cash_exp,
	        		id_cash_exp_line: cashExpLineId,
	        		id_store_line: '',
	        		line: data.cash_exp_line_id,
	        		delete_store_line: data.cash_exp_line_id,
	        		description: data.description,
	        		id_store_1 : 0,
	        		id_store_2 : 0,
	        		id_store_3 : 0,
	        		id_store_4 : 0,
	        		id_store_5 : 0,
	        		id_store_6 : 0,
	        		id_store_7 : 0,
	        		id_store_8 : 0,
	        		id_store_9 : 0,
	   				quantity: data.quantity,
	   				price_no_vat_curr: data.price_no_vat_curr,
	   				id_aisle: '0',
	   				associated_acc: data.cont_imob_in_curs,
	   				id_act_mf : 1,
	   				id_tip_inreg: tipInregistrare,
	   				gestiune: 0,
	   				id_dpi: '0',
	   				vizualizare_dpi: 0,
	   				id_tip_inventar: 2, // de grup - default
	   				detalii: data.cash_exp_line_id,
	   				cont_imob_in_fct : data.cont_imob_in_fct,
	   				cont_imob_in_curs : data.cont_imob_in_curs,
	        		cont_imob_avans : data.cont_imob_avans,
	        		inventory: ''
	        	});
        	}
        }
        
    });
    // refresh table values
    if(isGoodStoreLine){
    	dataTableSplit.set('recordset', remoteDataSplit);
    	setLineAndStoreLineValues(portletId);
    	$('.stores_' + $('#' + portletId + 'company').val()).removeClass('hiddencol');
    }
    
}


/**
 * Redo the line indexes after a line was deleted
 */
function associateCounterWithLines(index){
	var rows = [];
    var ml = dataTable.data, msg = '', template = '';
    ml.each(function (item, i) {
    	var data = item.getAttrs(['invoice_line_id', 'delete_line', 'counter', 'line', 'inv_number', 'unit_price_ron', 'price_no_vat_ron', 'price_vat_ron', 'price_vat_curr', 'currency',  'article_code', 'product_code', 
    	  		                 'description', 'um', 'quantity', 'unit_price_curr', 'price_no_vat_curr', 'vat', 'val_cu_tva', 'warranty',
    	   		                'id_ias', 'id_ifrs', 'dt_ras', 'dt_ifrs', 'lot', 'clasif', 'lot_warranty', 'cont_imob_in_fct', 
    	   		                'cont_imob_in_curs', 'cont_imob_avans', 'detalii']);
    	//from the next line 
    	if (data.counter >= index + 1){
    		data.counter = i + 1;
    	}
    	rows.push(data);
    });
    
    // set value on table
    dataTable.set('recordset', rows);
}
	
//extracts the pif date from dataTableInv.data
function getPifDate(row){
	//position in the array
	var pos = row.indexOf("data_pif");
	//start position of the date
	var start = parseInt(pos) + 12;
	//end position of the date
	var end = parseInt(start + 10); 
	//the date
	var pif_date = row.substring(start, end);
	//if the date was set
	if (pif_date.indexOf('"') < 0){
		return pif_date;
	} else {
		//if the date was not set
		return "";
	}
}
//save data from dataTableInv
function saveInventoryOverlay(portletId, action, ajaxURL) {
	//verify if pif date was set
	if(action == 'save' && isNotSetPifDate()) {
		alert("Data PIF nu a fost setata!");
		return;
	} else if(action == 'save') {
		var ml = dataTableInv.data, msg = '', template = '';
		ml.each(function(item, i) {
			var data = item.getAttrs(['nr_inv', 'id_store']);
			//TODO change false with data.id_store for inventory number validation
			if(false) {
				$.ajax({
					type: "POST",
					url: ajaxURL,
					data: 	portletId + "action=getinventoryHistory" +  
					"&" + portletId + "storeIdInv=" + data.id_store +
					"&" + portletId + "invNo=" + data.nr_inv,
					success: function(msg) {
						if (msg != "") {
							console.log("Mesaj : " + msg);
							if(msg == "eroare") {
								alert("Numarul de inventar nu exista in istoric!");
							} else {
								//update information from table
								updateInventoryOverlay(portletId);
								// close the popup
								closeInventoryOverlay();
							}
						} else {
							alert("A aparut o eroare in procesul de preluare a istoricului de numere de inventar!");
						}
					}
				});
			} else {
				//update information from table
				updateInventoryOverlay(portletId);
				// close the popup
				closeInventoryOverlay();
			}
		});
		
	} else {
		closeInventoryOverlay();
	}
	$('.stores_' + $('#' + portletId + 'company').val()).removeClass('hiddencol');
}

function isNotSetPifDate() {
	var result = true;
	var ml = dataTableInv.data, msg = '', template = '';
	ml.each(function(item, i) {
		if(!result){
			return;
		}
		var data = item.getAttrs(['data_pif']);
		if(data.data_pif != '') {
			result = false;
			return;
		}
	});
	return result;
}

function isNotValidInvNo(portletId, ajaxURL){
	
	return result;
}

function getInventoryDetails(portletId, lineId){
	remoteDataSplit = [];
	var jsonEntry;
	var secondJson;
    // loop through table
	var ml = dataTableSplit.data, msg = '', template = '';
	ml.each(function (item, i) {
		var dataS = item.getAttrs(['id_store_line', 'id_store', 'delete_line', 'index', 'article_code', 'description', 'quantity', 'price_no_vat_curr','id_store_1',
		     		                 'id_store_2', 'id_store_3', 'id_store_4', 'id_store_5', 'id_store_6','id_store_7','id_store_8', 'id_store_9', 'id_department', 
		                             'associated_acc', 'id_tip_inreg', 'id_act_mf', 'id_dpi', 'id_tip_inventar', 'unit_price', 'product_code', 'nr',
		                             'currency', 'inv_number', 'line_id', 'price_no_vat_ron', 'vizualizare_dpi', 'cont_imob_in_curs', 'cont_imob_in_fct', 
		                             'cont_imob_avans', 'id_cat_gestiune', 'detalii', 'inventory']);
	if (codArt == dataS.delete_line) {
		var jsonEntryString = "{\"inv_number\":\"value1\", \"data_pif\":\"value2\", \"receptie\":\"\"}";
		console.log(dataS.inventory);
		console.log(dataS.inventory.substring(dataS.inventory.indexOf("nr_inv") + 9, dataS.inventory.indexOf("data_pif") - 3));
		if (dataS.inventory.substring(dataS.inventory.indexOf("nr_inv") + 9, dataS.inventory.indexOf("data_pif") - 3) != ""){
			jsonEntryString = jsonEntryString.replace("value1", dataS.inventory.substring(dataS.inventory.indexOf("nr_inv") + 9, dataS.inventory.indexOf("data_pif") - 3));
		} else {
			jsonEntryString = jsonEntryString.replace("value1", "");
		}
		
		var p_date = "";
		console.log(dataS.inventory.substring(dataS.inventory.indexOf("data_pif") + 12, dataS.inventory.indexOf("receptie") - 4));
		if(dataS.inventory.substring(dataS.inventory.indexOf("data_pif") + 12, dataS.inventory.indexOf("receptie") - 4) != "\"\""){
			p_date = dataS.inventory.substring(dataS.inventory.indexOf("data_pif") + 12, dataS.inventory.indexOf("receptie") - 4);
		}
		console.log("p_date -> " + p_date);
		
		if(p_date.indexOf("[") < 0){
			p_date = "[" + p_date;
		}
		if(p_date.indexOf("]") < 0){
			p_date = p_date + "]";
		}
		console.log(p_date);
		jsonEntryString = jsonEntryString.replace("value2", p_date);

		jsonEntry = JSON.parse(jsonEntryString);
		}
	}	);
	var ml = dataTableInv.data, msg = '', template = '';
	var rows = [];
	ml.each(function (item, i) {
		var data = item.getAttrs(['inv_number', 'pif_date', 'reception']);
		//console.log("Un text : "+jsonEntry.inv_number);
		data.nr_inv = jsonEntry.inv_number;
		data.data_pif = jsonEntry.data_pif;
		data.receptie = jsonEntry.receptie;
		rows.push(data);
	});
	dataTableInv.set('data', rows);
	
}

function updateInventoryOverlay(portletId) {
	remoteDataSplit = [];
		
    // loop through table
	var ml = dataTableSplit.data, msg = '', template = '';
	var inv_date = $('#' + portletId + 'inv_date2').val();
	console.log("uploadInv = "+codArt);
	ml.each(function (item, i) {
		var dataS = item.getAttrs(['line', 'id_store_line', 'id_store', 'delete_line', 'index', 'article_code', 'description', 'quantity', 'price_no_vat_curr','id_store_1',
		     		                 'id_store_2', 'id_store_3', 'id_store_4', 'id_store_5', 'id_store_6','id_store_7','id_store_8', 'id_store_9', 'id_department', 
		                             'associated_acc', 'id_tip_inreg', 'id_act_mf', 'id_dpi', 'id_tip_inventar', 'unit_price', 'product_code', 'nr',
		                             'currency', 'inv_number', 'line_id', 'price_no_vat_ron', 'vizualizare_dpi', 'cont_imob_in_curs', 'cont_imob_in_fct', 
		                             'cont_imob_avans', 'id_cat_gestiune', 'detalii', 'inventory', 'initiative', 'new_prj']);
	if (codArt == dataS.delete_line) {
		dataS.inventory = JSON.stringify(dataTableInv.data.toJSON());
		//extract the pif date
		var pif = getPifDate(dataS.inventory);
		//if the pif date was set
//		if (pif != ""){
//			//if pif date > voucher date => reg type = "Obiect de inventar in curs"
//			if (pif > inv_date){
//				//console.log("in curs");
//				dataS.id_tip_inreg = 1;
//			} else if (pif < inv_date){
//				//else reg type = "Obiect de inventar in functiune"
//				//console.log("in functiune");
//	    			dataS.id_tip_inreg = 2;
//	    		}
//	    	}
		}
		remoteDataSplit.push(dataS);
	}	);
	dataTableSplit.set('recordset', remoteDataSplit);
	//display the store column
	$('.stores_' + selectedCompany).removeClass('hiddencol');
}

function saveInventoryOverlayCashExp(portletId){
	remoteDataSplit = [];
	var mlSplit = dataTableSplit.data, msg = '', template = '';
	mlSplit.each(function (item, i) {
		var dataS = item.getAttrs(['id_cash_exp', 'id_cash_exp_line','id_store_line','delete_store_line', 'line', 'description','id_store_1','id_store_2','id_store_3','id_store_4','id_store_5',
					                 'id_store_6','id_store_7','id_store_8', 'id_store_9','quantity','price_no_vat_curr','id_aisle','associated_acc','id_tip_inreg','id_act_mf','gestiune','id_dpi','vizualizare_dpi',
					                 'id_tip_inventar','detalii','cont_imob_in_fct','cont_imob_in_curs','cont_imob_avans','inventory']);
		console.log("codArt -> " + codArt + " -> " + dataS.line);
		if(dataS.line == codArt) {
			dataS.inventory = JSON.stringify(dataTableInv.data.toJSON());
		}
		remoteDataSplit.push(dataS);
	});
	dataTableSplit.set('recordset', remoteDataSplit);
	$('#overlay-detalii-inv').hide();
	$('.stores_' + $('#' + portletId + 'company').val()).removeClass('hiddencol');
}

function closeInventoryOverlay() {
		$('#overlay-detalii-inv').hide();
		return false;
}
//set datascadenta adauga_factura.jsp
function setDueDate(portletId){
	//if this function is called for invoice portlet
	var invoice_date = $('#' + portletId +'inv_date2');
	//if this function is called for advance request portlet
	if(typeof invoice_date.val() == 'undefined'){
		invoice_date = $('#' + portletId +'inv_date');
	}
	var term = $('#' + portletId + 'payment_term');
	var due_date = $('#' + portletId + 'datascadenta');
	var newDate = new Date();
	if (invoice_date && term && due_date){
		if(term.val() != ""){
			var ddate;
			//convert to date format
			if(invoice_date.val() == ""){
				//if the invoice date was not set
				var today = new Date();
				var dd = today.getDate();
				var mm = today.getMonth()+1; //January is 0!
				var yyyy = today.getFullYear();
				ddate = yyyy + '/' + mm + '/' + dd;
			} else {
				ddate = invoice_date.val().split("-").join('/');
			}
			//create a Date object
			newDate = new Date(ddate);
			//calculate the new date
			newDate.setDate(newDate.getDate() + parseInt(term.val()));
			//set the new date
			var resulted_date = [newDate.getDate(),newDate.getMonth()+1,newDate.getFullYear()].reverse().join('-');
			verifyDate(resulted_date, due_date);
		} else {
			due_date.val("");
		}
	}
}

//converts from 2015-4-3 to 2015-04-03
function verifyDate(newdate, due_date){
	//split the date to get the month and day
	var parts = newdate.split('-');
	if (parts[1].length == 1){
		//if the month has only one cipher
		parts[1] = "0" + parts[1];
	}
	if(parts[2].length == 1){
		//if the day has only one cipher
		parts[2] = "0" + parts[2];
	}
	//reassembly date
	newdate = parts.join('-');
	//set the date
	due_date.val(newdate);
}

function getRaionListByStoreId(portletNamespace, storeId, ajaxURL, Y, selectedCompany){
	$.ajax({
		type: "POST",
		url: ajaxURL,
		data: 	portletNamespace+"action=getStoreAislesByStore" +  
		"&" + portletNamespace + "storeId=" + storeId,
		success: function(msg) {
			console.log("Asdgf : " + selectedCompany);
			// get table data
			if (msg != ""){
				console.log("Mesaj : " + msg);
				raionDepartament = jQuery.parseJSON(msg);
				var obj = JSON.parse(JSON.stringify(raionDepartament));
				dataTableSplit.modifyColumn('id_department',{
					editor: new Y.DropDownCellEditor({options: raionDepartament, id: 'raionDepartament', elementName: 'raionDepartament',
						after: {
				            focus: function(event) {
					            $("select[name=raionDepartament]").chosen();
				            	}
							}
						}),
						emptyCellValue: '',
						formatter: function(o) {
							if (typeof o.value != 'undefined') {
								if(o.value != null) {
									raionOrDept = o.value;
									return obj[o.value].toString();
								} else return '';
							} else {
								return '';
							}
						}
				});
			} else {
				alert("A aparut o eroare in procesul de preluare a listei de raioane !");
			}
		}
	});
}

function saveOnChange(e) {
	
	e.target.fire('save', {
        newVal: e.target.getValue(),
        prevVal: e.target.get('value')
    });
}

function saveOnEnter(e) {
	if (e.domEvent.charCode == 13) {
		if (!e.target.validator.hasErrors()) {
			e.target.fire('save', {
                newVal: e.target.getValue(),
                prevVal: e.target.get('value')
            });
		}
	}
}

function getDpiListByRaionAndStore(portletNamespace, raionOrDept, storeId, ajaxURL, dataTableSplit, Y){
	
	$.ajax({
		type: "POST",
		url: ajaxURL,
		data: 	portletNamespace+"action=getDpisByAisle" +  
		"&" + portletNamespace + "storeId=" + storeId +
		"&" + portletNamespace + "raion=" + raionOrDept,
		success: function(msg) {
			// get table data
			if (msg != ""){
				var jsonEntries = jQuery.parseJSON(msg);
			} else {
				alert("A aparut o eroare in procesul de preluare a listei de DPI-uri !");
			}
		}
	});
}


//======================================================================================================================================
//													FUNCTIONS FOR VOUCHERS(tabel_articole.jsp)
//======================================================================================================================================

//if user changes the number of dpi, change the link also
function setDPILinkVouchers(dpiId) {
	var rows = [];
	//all store lines
	var ml = dataTableSplit.data, msg = '', template = '';
	
	ml.each(function (item, i) {
		//for each store line
		var data = item.getAttrs(['id_store_line', 'delete_line', 'line', 'description', 'article_code', 'quantity', 'price_no_vat_ron', 'associated_acc', 
   	                           'id_tip_inreg', 'id_act_mf', 'id_dpi', 'vizualizare_dpi', 'id_tip_inventar', 'cont_imob_in_fct', 'cont_imob_in_curs', 
	                           'cont_imob_avans', 'detalii', 'inventory']);
		
		if (data.id_dpi == dpiId) {
			data.vizualizare_dpi = dpiId;
		}
		
		// set value on matrix
		rows.push(data);
	});
	// set value on table
	dataTableSplit.set('recordset', rows);
}

function setDPILinkCashExp(dpiId, portletId) {
	var rows = [];
	//all store lines
	var ml = dataTableSplit.data, msg = '', template = '';
	
	ml.each(function (item, i) {
		//for each store line
		var data = item.getAttrs(['id_cash_exp', 'id_cash_exp_line','id_store_line','delete_store_line', 'line', 'description','id_store_1','id_store_2','id_store_3','id_store_4','id_store_5',
					                 'id_store_6','id_store_7','id_store_8', 'id_store_9','quantity','price_no_vat_curr','id_aisle','associated_acc','id_tip_inreg','id_act_mf','gestiune','id_dpi','vizualizare_dpi',
					                 'id_tip_inventar','detalii','cont_imob_in_fct','cont_imob_in_curs','cont_imob_avans','inventory']);
		
		if (data.id_dpi == dpiId) {
			data.vizualizare_dpi = dpiId;
		}
		
		// set value on matrix
		rows.push(data);
	});
	// set value on table
	dataTableSplit.set('recordset', rows);
	$('.stores_' + $('#' + portletId + 'company').val()).removeClass('hiddencol');
}

function setLineAndStoreLineValues(portletId) {
	var lineValue = 0;
	 var ml = dataTable.data, msg = '', template = '';
	 ml.each(function (item, i) {
	    	var data = item.getAttrs(['id_cash_exp', 'cash_exp_line_id', 'delete_line', 'line', 'counter', 'product_code', 'description', 'um', 'quantity', 'unit_price_curr', 
	    				                 'price_no_vat_curr', 'price_vat_curr', 'vat_code', 'price_with_vat_curr', 'id_ias', 'id_ifrs', 'dt_ias', 'dt_ifrs', 'lot', 'clasif', 
	    				                 'detalii', 'cont_imob_in_fct', 'cont_imob_in_curs', 'cont_imob_in_avans', 'is_new_line']);
	    	lineValue = parseFloat(parseFloat(lineValue) + parseFloat(data.price_no_vat_curr)).toFixed(4);
	 });
	 
	 var storeLineValue = 0;
	 var mlS = dataTableSplit.data, msg = '', template = '';
	 mlS.each(function (item, i) {
	    	var dataS = item.getAttrs(['id_cash_exp', 'id_cash_exp_line','id_store_line','delete_store_line','description','id_store_1','id_store_2','id_store_3','id_store_4','id_store_5',
	    				                 'id_store_6','id_store_7','id_store_8', 'id_store_9','quantity','price_no_vat_curr','id_aisle','associated_acc','id_tip_inreg','id_act_mf','gestiune','id_dpi','vizualizare_dpi',
	    				                 'id_tip_inventar','detalii','cont_imob_in_fct','cont_imob_in_curs','cont_imob_avans','inventory']);
	    	storeLineValue = parseFloat(parseFloat(storeLineValue) + parseFloat(dataS.price_no_vat_curr)).toFixed(4);
	 });
	 $('#' + portletId + 'valoare_totala').val(lineValue);
	 $('#' + portletId + 'valoare_totala_alocata').val(storeLineValue);
	 $('#' + portletId + 'difference').val(parseFloat(lineValue - 0 - storeLineValue).toFixed(4));
	 if ( $('#'+portletId + 'difference').val() == 0){
			$('#'+portletId + 'valoare_totala').css("border-color", "#488F06");
			$('#'+portletId + 'difference').css("border-color", "#488F06");
			$('#'+portletId + 'valoare_totala_alocata').css("border-color", "#488F06");
		} else {
			$('#'+portletId + 'valoare_totala').css("border-color", "#FE0000");
			$('#'+portletId + 'difference').css("border-color", "#FE0000");
			$('#'+portletId + 'valoare_totala_alocata').css("border-color", "#FE0000");
		}
}


//set all line values based on quantity and price per unit 
function setLineValuesCashExp(quantity, pricePerUnit, portletId) {
	//console.log("Set line values");
	
	var sumaLinii = 0;
	console.log(pricePerUnit + "  " + quantity );
	if (pricePerUnit != '' && quantity != '') {
		var rows = [];
	    var ml = dataTable.data, msg = '', template = '';
	    ml.each(function (item, i) {
	    	var data = item.getAttrs(['id_cash_exp', 'cash_exp_line_id', 'delete_line', 'line', 'counter', 'product_code', 'description', 'um', 'quantity', 'unit_price_curr', 
	    				                 'price_no_vat_curr', 'price_vat_curr', 'vat_code', 'price_with_vat_curr', 'id_ias', 'id_ifrs', 'dt_ias', 'dt_ifrs', 'lot', 'clasif', 
	    				                 'detalii', 'cont_imob_in_fct', 'cont_imob_in_curs', 'cont_imob_in_avans', 'is_new_line']);
	        if (data.quantity == quantity && data.unit_price_curr == pricePerUnit) {
	        	data.price_no_vat_curr = parseFloat(parseFloat(quantity) * parseFloat(pricePerUnit)).toFixed(4);
	        	data.price_with_vat_curr = parseFloat(parseFloat(data.price_no_vat_curr) + parseFloat(quantity) * parseFloat(pricePerUnit) * parseFloat(data.vat_code) / 100).toFixed(4);
	        	data.price_vat_curr = data.price_with_vat_curr - data.price_no_vat_curr;
	        }
	        sumaLinii += parseFloat(data.price_no_vat_curr);
        	// set value on matrix
        	rows.push(data);
	    });
	    
	    $('#'+portletId + 'valoare_totala').val(parseFloat(sumaLinii).toFixed(4));
	    var dif = parseFloat(parseFloat(sumaLinii));
	    if($('#'+portletId + 'valoare_totala_alocata').val()) {
	    	dif = dif - parseFloat($('#'+portletId + 'valoare_totala_alocata').val());
	    }
	    dif = parseFloat(dif).toFixed(4);
	    $('#'+portletId + 'difference').val(dif);
	    $('#'+portletId + 'difference').autoNumeric('update', {aSep: ',', mDec: 4, vMin : -999999999, vMax : 999999999 });
	    $('#'+portletId + 'val_ron').autoNumeric('update', {aSep: ',', mDec: 4, vMin : -999999999, vMax : 999999999 });
	    $('#'+portletId + 'valoare_totala').autoNumeric('update', {aSep: ',', mDec: 4, vMin : -999999999, vMax : 999999999 });
	    $('#'+portletId + 'valoare_totala_alocata').autoNumeric('update', {aSep: ',', mDec: 4, vMin : -999999999, vMax : 999999999 });
	    
	    if ( $('#'+portletId + 'difference').val() == 0){
			$('#'+portletId + 'val_ron').css("border-color", "#488F06");
			$('#'+portletId + 'difference').css("border-color", "#488F06");
			$('#'+portletId + 'valoare_totala').css("border-color", "#488F06");
		} else {
			$('#'+portletId + 'val_ron').css("border-color", "#FE0000");
			$('#'+portletId + 'difference').css("border-color", "#FE0000");
			$('#'+portletId + 'valoare_totala').css("border-color", "#FE0000");
		}
	    
    	// set value on table
	    dataTable.set('recordset', rows);
	  	//recalculate total no vat for all store lines
	    setValNoVatStoreLineCashExp(portletId);

	}
	
	setLineAndStoreLineValues(portletId);
}

//set all line values based on quantity and price per unit 
function setLineValuesVouchers(quantity, pricePerUnit, portletId) {
	//console.log("Set line values");
	
	var sumaLinii = 0;
	//console.log(pricePerUnit + "  " + quantity );
	if (pricePerUnit != '' && quantity != '') {
		var rows = [];
	    var ml = dataTable.data, msg = '', template = '';
	    ml.each(function (item, i) {
	    	var data = item.getAttrs(['id_voucher', 'delete_line', 'line', 'article_code', 'product_code', 
	    	                          'description', 'um', 'quantity', 'unit_price_ron', 'price_no_vat_ron', 'price_vat_ron', 'vat', 
	    	 		                  'price_with_vat_ron', 'lot', 'ras', 'ifrs', 'dt_ras', 'dt_ifrs', 'is_it', 
	    	 		                  'detalii', 'cont_imob_in_fct', 'cont_imob_in_curs', 'cont_imob_avans']);
			//TODO - perform ajax in order to retrieve data
	        if (data.quantity == quantity && data.unit_price_ron == pricePerUnit) {
	        	data.price_no_vat_ron = parseFloat(parseFloat(quantity) * parseFloat(pricePerUnit)).toFixed(4);
	        	data.price_with_vat_ron = parseFloat(parseFloat(data.price_no_vat_ron) + parseFloat(quantity) * parseFloat(pricePerUnit) * parseFloat(data.vat) / 100).toFixed(4);
	        	data.price_vat_ron = data.price_with_vat_ron - data.price_no_vat_ron;
	        }
	        sumaLinii += parseFloat(data.price_no_vat_ron);
        	// set value on matrix
        	rows.push(data);
	    });
	    
	    $('#'+portletId + 'valoare_totala').val(parseFloat(sumaLinii).toFixed(4));
	    var dif = parseFloat(parseFloat($('#'+portletId + 'valoare_ron').val().replace(toReplace,''))-parseFloat(sumaLinii)).toFixed(4);
	    $('#'+portletId + 'difference').val(dif);
	    $('#'+portletId + 'difference').autoNumeric('update', {aSep: ',', mDec: 4, vMin : -999999999, vMax : 999999999 });
	    $('#'+portletId + 'valoare_ron').autoNumeric('update', {aSep: ',', mDec: 4, vMin : -999999999, vMax : 999999999 });
	    $('#'+portletId + 'valoare_totala').autoNumeric('update', {aSep: ',', mDec: 4, vMin : -999999999, vMax : 999999999 });
	    
	    if ( $('#'+portletId + 'difference').val() == 0){
			$('#'+portletId + 'valoare_ron').css("border-color", "#488F06");
			$('#'+portletId + 'difference').css("border-color", "#488F06");
			$('#'+portletId + 'valoare_totala').css("border-color", "#488F06");
		} else {
			$('#'+portletId + 'valoare_ron').css("border-color", "#FE0000");
			$('#'+portletId + 'difference').css("border-color", "#FE0000");
			$('#'+portletId + 'valoare_totala').css("border-color", "#FE0000");
		}
	    
    	// set value on table
	    dataTable.set('recordset', rows);
	  	//recalculate total no vat for all store lines
        setValNoVatStoreLineVouchers();

	}
}

//calculate val no vat if the quantity/unit price changes
function setValNoVatStoreLineCashExp(portletId){
	var rows = [];
	//all store lines
	var ml = dataTableSplit.data, msg = '', template = '';
	//all lines
	var mlh = dataTable.data;
	var error_line = 0;
	ml.each(function (item, i) {
		//for each store line
    	var data = item.getAttrs(['id_cash_exp', 'id_cash_exp_line','id_store_line','delete_store_line', 'line', 'description','id_store_1','id_store_2','id_store_3','id_store_4','id_store_5',
    				                 'id_store_6','id_store_7','id_store_8', 'id_store_9','quantity','price_no_vat_curr','id_aisle','associated_acc','id_tip_inreg','id_act_mf','gestiune','id_dpi','vizualizare_dpi',
    				                 'id_tip_inventar','detalii','cont_imob_in_fct','cont_imob_in_curs','cont_imob_avans','inventory']);
    	var unit_price = 0;
    	var new_quantity = 0;
    	
    	//find  the line for each store line
		mlh.each(function (itemH,j){
    		var dataH = itemH.getAttrs(['cash_exp_line_id','unit_price_curr','quantity']);
    		if(data.line == dataH.cash_exp_line_id){
    			//get the modified unit price from the invoice line
    			unit_price = dataH.unit_price_curr;
    			new_quantity = dataH.quantity;
    			//cant = dataH.quantity;
    			//line = dataH.line;
    			return false;
    		}
    	});
    	
		data.quantity = new_quantity;
		//for each invoice store line recalculate price no vat with the new unit price
		data.price_no_vat_curr = parseFloat(parseFloat(new_quantity) * parseFloat(unit_price)).toFixed(4);
    	// set value on matrix
    	rows.push(data);
	});	
	// set value on table
    dataTableSplit.set('recordset', rows);
    $('.stores_' + $('#' + portletId + 'company').val()).removeClass('hiddencol');
}
	//calculate val no vat if the quantity/unit price changes
	function setValNoVatStoreLineVouchers(){
		var rows = [];
		//all store lines
		var ml = dataTableSplit.data, msg = '', template = '';
		//all lines
		var mlh = dataTable.data;
		var error_line = 0;
		ml.each(function (item, i) {
			//for each store line
	    	var data = item.getAttrs(['delete_line', 'line', 'description', 'quantity', 'price_no_vat_ron', 'associated_acc', 'id_tip_inreg', 'id_act_mf','id_dpi', 'vizualizare_dpi', 
	    	  		                 'id_tip_inventar', 'detalii', 'cont_imob_in_fct', 'cont_imob_in_curs', 'cont_imob_avans', 'inventory']);
	    	var unit_price = 0;
	    	var new_quantity = 0;
	    	
	    	//find  the line for each store line
			mlh.each(function (itemH,j){
	    		var dataH = itemH.getAttrs(['line','unit_price_ron','quantity']);
	    		if(data.line == dataH.line){
	    			//get the modified unit price from the invoice line
	    			unit_price = dataH.unit_price_ron;
	    			new_quantity = dataH.quantity;
	    			//cant = dataH.quantity;
	    			//line = dataH.line;
	    			return false;
	    		}
	    	});
	    	
			data.quantity = new_quantity;
			//for each invoice store line recalculate price no vat with the new unit price
			data.price_no_vat_ron = parseFloat(parseFloat(new_quantity) * parseFloat(unit_price)).toFixed(4);
	    	// set value on matrix
	    	rows.push(data);
		});	
		// set value on table
	    dataTableSplit.set('recordset', rows);
	}
	//if the user changes the reg. type or the mf action => the associated account must change
	function setAssociatedAccountVouchers(){
		var rows = [];
		//all store lines
		var ml = dataTableSplit.data, msg = '', template = '';
		
		ml.each(function (item, i) {
			//for each store line
			var data = item.getAttrs(['delete_line', 'line', 'description', 'article_code', 'quantity', 'price_no_vat_ron', 'associated_acc', 
	    	                           'id_tip_inreg', 'id_act_mf', 'id_dpi', 'vizualizare_dpi', 'id_tip_inventar', 'cont_imob_in_fct', 'cont_imob_in_curs', 
	    	                           'cont_imob_avans', 'detalii', 'inventory']);
			if (data.id_act_mf == '4' ){
				// tipul de actiune mf este "avans"
				data.associated_acc = data.cont_imob_avans;
			} else if (data.id_tip_inreg == '1' || data.id_tip_inreg == '3'){
				//tipul de inregistrare este "obiect de inventar in curs" sau "mijloc fix in curs"
				data.associated_acc = data.cont_imob_in_curs;
			} else {
				//tipul de inregistrare este "obiect de inventar in functiune" sau "mijloc fix in functiune"
				data.associated_acc = data.cont_imob_in_fct;
			}
			// set value on matrix
			rows.push(data);
		});
		// set value on table
		dataTableSplit.set('recordset', rows);
	}
	
	function setAssociatedAccountCashExp(portletId){
		var rows = [];
		//all store lines
		var ml = dataTableSplit.data, msg = '', template = '';
		ml.each(function (item, i) {
			//for each store line
			var data = item.getAttrs(['id_cash_exp', 'id_cash_exp_line','id_store_line','delete_store_line', 'line', 'description','id_store_1','id_store_2','id_store_3','id_store_4','id_store_5',
						                 'id_store_6','id_store_7','id_store_8', 'id_store_9','quantity','price_no_vat_curr','id_aisle','associated_acc','id_tip_inreg','id_act_mf','gestiune','id_dpi','vizualizare_dpi',
						                 'id_tip_inventar','detalii','cont_imob_in_fct','cont_imob_in_curs','cont_imob_avans','inventory']);
			if (data.id_act_mf == '4' ){
				// tipul de actiune mf este "avans"
				data.associated_acc = data.cont_imob_avans;
			} else if (data.id_tip_inreg == '1' || data.id_tip_inreg == '3'){
				//tipul de inregistrare este "obiect de inventar in curs" sau "mijloc fix in curs"
				data.associated_acc = data.cont_imob_in_curs;
			} else {
				//tipul de inregistrare este "obiect de inventar in functiune" sau "mijloc fix in functiune"
				data.associated_acc = data.cont_imob_in_fct;
			}
			// set value on matrix
			rows.push(data);
		});
		// set value on table
		dataTableSplit.set('recordset', rows);
		$('.stores_' + $('#' + portletId + 'company').val()).removeClass('hiddencol');
	}
	
//======================================================================================================================================
//														APROBARE FACTURA (definitions/aprobarefactura/view.jsp)
//															Functionalitati pentru sectiunea "Decizii factura"
//======================================================================================================================================

	
function existaLitigii(portletId){
	var vina_furnizor_true = $('#' + portletId + 'vina_furnizor1');
	var vina_furnizor_false = $('#' + portletId + 'vina_furnizor2');
	var detalii_litigiu = $('#' + portletId + 'detalii-litigiu');
	//seteaza vina furnizor
	vina_furnizor_true.prop('disabled', false);
	vina_furnizor_false.prop('disabled', false);
	if ($('#' + portletId + 'database_supplierr_fault').val() == 'N'){
		vina_furnizor_false.prop('checked', true);
	} else {
		vina_furnizor_true.prop('checked', true);
	}
	
	//seteaza detalii litigiu
	detalii_litigiu.removeAttr('readonly');
	detalii_litigiu.val($('#' + portletId + 'database_detaile_lit').val());
	//seteaza refuz la plata pe true doar daca valoarea lui actuala este diferita de true
	if($('input[name="' + portletId + 'bun_de_plata"]:checked').val() != 0) {
		$('#' + portletId + 'false_bun_de_plata').prop('checked', true);
		$('#' + portletId + 'false_bun_de_plata').trigger('change');
	}
}

function nuExistaLitigii(portletId){
	var vina_furnizor_true = $('#' + portletId + 'vina_furnizor1');
	var vina_furnizor_false = $('#' + portletId + 'vina_furnizor2');
	var detalii_litigiu = $('#' + portletId + 'detalii-litigiu');
	
	//seteaza vina furnizor
	vina_furnizor_true.prop('disabled', true);
	vina_furnizor_false.prop('checked', true);
	vina_furnizor_false.prop('disabled', true);
	detalii_litigiu.val("");
	detalii_litigiu.attr('readonly', true);
}

function refuzLaPlata(portletId){
	
	var motiv_amanare = $('#' + portletId + 'motiv_invalidare1');
	var motiv_litigiu = $('#' + portletId + 'motiv_invalidare2');
	
	var detalii_aditionale = $('#' + portletId + 'detalii-aditionale');

	//setare motiv invalidare

	motiv_litigiu.prop('disabled', false);
	motiv_amanare.prop('disabled', false);
	if ( $('#' + portletId + 'database_blocked_type').val() == '2'){
		motiv_litigiu.prop('checked', true);
	} else {
		motiv_amanare.prop('checked', true);
	}
	
	detalii_aditionale.removeAttr('readonly');
	detalii_aditionale.val( $('#' + portletId + 'database_aditional_details').val());
}

function bunDePlata(portletId){
	
	var motiv_amanare = $('#' + portletId + 'motiv_invalidare1');
	var motiv_litigiu = $('#' + portletId + 'motiv_invalidare2');
	var detalii_aditionale = $('#' + portletId + 'detalii-aditionale');
	
	motiv_litigiu.prop('checked', false);
	motiv_amanare.prop('checked', false);
	motiv_amanare.prop('disabled', true);
	motiv_litigiu.prop('disabled', true);
	detalii_aditionale.val("");
	detalii_aditionale.attr('readonly', true);
	
	//seteaza exista litigiu pe false
	$('#' + portletId + 'existenta_litigiu2').prop('checked', true);
	$('#' + portletId + 'existenta_litigiu2').trigger('change');
}

//verify thet the pif date was set for every inv no
function verifyPifDate(portletId){
	//all store lines
	var ml = dataTableSplit.data, msg = '', template = '';
	var result = true;
	ml.each(function (item, i) {
		//to stop the "each" function
		if(!result){
			return;
		}
		//for each store line
		var data = item.getAttrs(['product_code', 'inventory', 'id_tip_inventar', 'quantity']);
		//console.log("data.inventory = " + data.inventory);
		//console.log("data.id_tip_inventar = " + data.id_tip_inventar);
		//console.log("data.quantity = " + data.quantity);
		var inventory_data = data.inventory;
		console.log('product_code = ' + data.product_code);
		//daca nu este prorata
		if(data.product_code != 3562) {
			if (inventory_data != null && inventory_data != 'null' && inventory_data != ""){
				//tip inventar = individual
				if (data.id_tip_inventar == 1){
					var json = JSON.parse(inventory_data);
					for (j = 0; j < json.length; j++ ) {
						if (json[j].data_pif == ""){
							alert("Atentie! In sectiunea \"Alocare articol pe linii\",  pe linia " + (i+1) + ", pentru articolul numarul " +
									(j+1) + " data PIF nu a fost setata!");
							result = false;
							return;
						}
					}
				} else {
					//tip inventar = de grup
					//console.log("DE aici : _|_ "  + inventory_data);
					//console.log(inventory_data.substring(1, inventory_data.length - 1));
					var json = JSON.parse(inventory_data.substring(1, inventory_data.length - 1));
					if (json.data_pif == ""){
						alert("Atentie! In sectiunea \"Alocare articol pe linii\",  pe linia " + (i+1) + " data PIF nu a fost setata!");
						result = false;
					}
				}
			} else {
				alert("Atentie! In sectiunea \"Alocare articol pe linii\",  pe linia " + (i+1) + " data PIF nu a fost setata!");
				result = false;
			}
		}
	});
	return result;
}

/**
 * If mf act = majorare : add empty rows to dataTableInv
 * @param quantity
 * @param inv_type
 */
function setEntriesDataTableInv(quantity, inv_type, store){
	if (inv_type == 1){
		//individual
		for (i = 0; i < parseInt(quantity); i++){
			dataTableInv.data.add({ nr_inv: '', data_pif: '', receptie: '', id_store: store });
		}
	} else{
		//de grup
		dataTableInv.data.add({ nr_inv: '', data_pif: '', receptie: '',  id_store: store });
	}
}

function setEntriesDataTableInvVouchers(quantity, inv_type, dataTableInv){
	if (inv_type == 1){
		//individual
		for (i = 0; i < parseInt(quantity); i++){
			dataTableInv.data.add({ nr_inv: '', data_pif: '', receptie: '' });
		}
	} else{
		//de grup
		dataTableInv.data.add({ nr_inv: '', data_pif: '', receptie: '' });
	}
}

function emptyPopUpInventory (){
	for(var i = 0 ; i <= dataTableInv.data.size(); i++ ) {
		dataTableInv.removeRow(i-1);
		dataTableInv.removeRow(i);
		dataTableInv.removeRow(i+1);
	}
	//dataTableInv.deleteRows(0, dataTableInv.data.size());
	//dataTableInv.removeRow(0);
}

function emptyPopUpInventoryVouchers (dataTableInv){
	//dataTableInv.deleteRows(0, dataTableInv.get("data").size());
	dataTableInv.removeRow(0);
}

//delete store lines from local array
function deleteStoreLineFromInterface(e, portletId){
	//get target
	var elem = e.currentTarget.get('id');
	//get the id of the target
	var elementId	= elem.replace('row', '');
	//index of the row that is deleted
	var indexToRemove = -1;
	//iterate the data to find the index of the row that is deleted
	var ml = dataTableSplit.data, msg = '', template = '';
    ml.each(function (item, i) {
    	//console.log(item);
    	var dataS = item.getAttrs(['delete_line']);
    	if (dataS.delete_line == elementId){
    		indexToRemove = i;
    		return;
    	}
    });
    //remove row from locat array
	dataTableSplit.removeRow(indexToRemove);
	//calculate total no vat for lines
	setTotalNoVat(portletId);
	//calculate total no vat for store lines
	setTotalNoVatLines(portletId);
    //recalculate payment values
    //setPaymentValues(portletId);
	calculeazaTvaPeStoreLines(portletId);
    $('#' + portletId + 'val_no_vat_art').autoNumeric('update', {aSep: ',', mDec: 4, vMin : -999999999, vMax : 999999999 });
    $('#' + portletId + 'val_no_vat_aloc_linii').autoNumeric('update', {aSep: ',', mDec: 4, vMin : -999999999, vMax : 999999999 });
	$('#' + portletId + 'difference').autoNumeric('update', {aSep: ',', mDec: 4, vMin : -999999999, vMax : 999999999 });
}

function deleteLineFromInterface(e, portletId){
	//get target
	deletedCounter++;
	var elem = e.currentTarget.get('id');
	//get the id of the target
	var elementId	= elem.replace('row', '');
	//index of the row that is deleted
	var indexToRemove = -1;
	//number of the line that is deleted; store lines are matched with lines based on this number
	var lineNumber = -1;
	//iterate the line data to find the index of the row that is deleted
	var ml = dataTable.data, msg = '', template = '';
    ml.each(function (item, i) {
    	var data = item.getAttrs(['delete_line', 'line']);
    	//find the index of the line that is to be deleted
    	if (elementId == data.delete_line){
    		//save the index of the line
    		indexToRemove = i;
    		//save the invoice line number
    		lineNumber = data.line;
    		return;
    	}
    });
    //remove the line
    dataTable.removeRow(indexToRemove);
    //delete all associated store lines
    deleteAssociatedStoreLines(lineNumber);
	 //calculate total no vat for lines
	setTotalNoVat(portletId);
	//calculate total no vat for store lines
	setTotalNoVatLines(portletId);
    //recalculate payment values
    setPaymentValues(portletId);
}

function deleteLineFromInterfaceByDatabaseId(elementId, portletId){
	//get target
	deletedCounter++;
	//index of the row that is deleted
	var indexToRemove = -1;
	//number of the line that is deleted; store lines are matched with lines based on this number
	var lineNumber = -1;
	//iterate the line data to find the index of the row that is deleted
	var ml = dataTable.data, msg = '', template = '';
    ml.each(function (item, i) {
    	var data = item.getAttrs(['invoice_line_id', 'line']);
    	//find the index of the line that is to be deleted
    	if (elementId == data.invoice_line_id){
    		//save the index of the line
    		indexToRemove = i;
    		//save the invoice line number
    		lineNumber = data.line;
    		return;
    	}
    });
    //remove the line
    dataTable.removeRow(indexToRemove);
    //delete all associated store lines
    deleteAssociatedStoreLines(lineNumber);
	 //calculate total no vat for lines
	setTotalNoVat(portletId);
	//calculate total no vat for store lines
	setTotalNoVatLines(portletId);
    //recalculate payment values
    setPaymentValues(portletId);
    calculeazaTvaPeStoreLines(portletId);
}

function deleteLineFromInterfaceById(elementId, portletId){
	//get target
	deletedCounter++;
	//index of the row that is deleted
	var indexToRemove = -1;
	//number of the line that is deleted; store lines are matched with lines based on this number
	var lineNumber = -1;
	//iterate the line data to find the index of the row that is deleted
	var ml = dataTable.data, msg = '', template = '';
    ml.each(function (item, i) {
    	var data = item.getAttrs(['delete_line', 'line']);
    	//find the index of the line that is to be deleted
    	if (elementId == data.delete_line){
    		//save the index of the line
    		indexToRemove = i;
    		//save the invoice line number
    		lineNumber = data.line;
    		return;
    	}
    });
    //remove the line
    dataTable.removeRow(indexToRemove);
    //delete all associated store lines
    deleteAssociatedStoreLines(lineNumber);
	 //calculate total no vat for lines
	setTotalNoVat(portletId);
	//calculate total no vat for store lines
	setTotalNoVatLines(portletId);
    //recalculate payment values
    setPaymentValues(portletId);
}

function getLocalArrayIndex(data){
	
}

//delete all the store lines associated with the invoice line that has the number = @param lineNumber
function deleteAssociatedStoreLines(lineNumber){
	//iterate the store line data to find the index of the row that is deleted
	var mls = dataTableSplit.data, msg = '', template = '';
 	//array with all store line ids that must be deleted
 	var storeLines_ids = [];
    //remove all associated lines
   	mls.each(function (item, i) {
   		var dataS = item.getAttrs(['nr']);
   		//match all the store lines that are associated with the line and must be deleted 
   		if (dataS.nr == lineNumber) {
			//remove the matched store line
   			storeLines_ids.push(i);
   		}
   	});
   	//remove the store lines
   	for	(index = storeLines_ids.length - 1; index >= 0 ; index--) {
	    dataTableSplit.removeRow(storeLines_ids[index]);
	} 
   	calculeazaTvaPeStoreLines(portletId);
}

function saveStoreIdOnStoreLines(){
	var rows = [];
	var ml = dataTableSplit.data, msg = '', template = '';
	ml.each(function (item, i) {
		var dataS = item.getAttrs(['id_store_line', 'id_store', 'delete_line', 'index', 'article_code', 'description', 'quantity', 'price_no_vat_curr','id_store_1',
		     		                 'id_store_2', 'id_store_3', 'id_store_4', 'id_store_5', 'id_store_6','id_store_7','id_store_8', 'id_store_9', 'id_department', 
		                             'associated_acc', 'id_tip_inreg', 'id_act_mf', 'id_dpi', 'id_tip_inventar', 'unit_price', 'product_code', 'nr',
		                             'currency', 'inv_number', 'line_id', 'price_no_vat_ron', 'vizualizare_dpi', 'cont_imob_in_curs', 'cont_imob_in_fct', 
		                             'cont_imob_avans', 'id_cat_gestiune', 'detalii', 'inventory', 'initiative', 'new_prj']);
		var selectedStore = 0;
		if (selectedCompany == 1) {
			selectedStore = dataS.id_store_1;
		} else if (selectedCompany == 2) {
			selectedStore = dataS.id_store_2;
		} else if (selectedCompany == 3) {
			selectedStore = dataS.id_store_3;
		} else if (selectedCompany == 4) {
			selectedStore = dataS.id_store_4;
		} else if (selectedCompany == 5) {
			selectedStore = dataS.id_store_5;
		} else if (selectedCompany == 6) {
			selectedStore = dataS.id_store_6;
		}else if (selectedCompany == 7) {
			selectedStore = dataS.id_store_7;
		}else if (selectedCompany == 8) {
			selectedStore = dataS.id_store_8;
		}else if (selectedCompany == 9) {
			selectedStore = dataS.id_store_9;
		}
		dataS.id_store = selectedStore;
		
		rows.push(dataS);
	});
	// set value on table
	dataTableSplit.set('recordset', rows);
    //show the stores column 
	$('.stores_' + selectedCompany).removeClass('hiddencol');
}

//onClick function for button "Trimite in Ch Gen" 
function sendToGeneralExpenses(portletId){
	if (confirm("Sunteti sigur/a ca doriti sa trimiteti factura curenta in Cheltuieli Generale?")){
		$('#' + portletId + 'delete-invoice').submit();
	}
}

// for every store line check if the account corresponds with the reg type
function checkAccountRegType(){
	var ml  = dataTableSplit.data, msg = '', template = '';
	var result = 0;
	ml.each(function(item,i){
		if(result != 0){
			return;
		}
		var data = item.getAttrs(['associated_acc','id_tip_inreg']);
		//the account corresponds to mf
		if (data.associated_acc.substring(0, 1) == "2"){
			//the reg type corresponds with obj inv
			 if (data.id_tip_inreg == "1" || data.id_tip_inreg == "1"){
				 result = i + 1;
			 }
		//the account corresponds to obj inv
		} else if (data.associated_acc.substring(0, 1) == "3"){
			 //the reg type corresponds with mf
			 if (data.id_tip_inreg == "3" || data.id_tip_inreg == "4"){
				 result = i + 1;
			 }
		}
	});
	return result;
}

function checkCapexDetails(portletNamespace){
	var isGood = true;
	if ($('#'+portletNamespace+'init_cent').is(':checked')) {
		var initiativa = $("select[name=" + portletNamespace +"lista_initiative]").val();
		if ( !initiativa ) {
			alert (" Nu ati selectat tipul de initiativa! ");
			isGood = false;
		}
	}
	
	if ($('#'+portletNamespace+'proiect_nou').is(':checked')) {
		var proiectNou = $("select[name=" + portletNamespace +"project]").val();
		if ( !proiectNou ) {
			alert (" Nu ati selectat tipul de proiect! ");
			isGood = false;
		}
	}
	
	return isGood;
}

/**
 * Check if the aisle is set on every store line
 * @param portletNamespace
 */
function checkAislesOnStoreLines(portletNamespace){
	var ml  = dataTableSplit.data, msg = '', template = '';
	var result  = false;
	ml.each(function(item,i){
		if(result == true){
			return;
		}
		var data = item.getAttrs(['id_department','id_cat_gestiune', 'id_dpi']);
		//raionul nu a fost setat
		if (data.id_department == 0){
			alert("In sectiunea \"Alocare articole pe linii\", pe linia " + (i+1) + " coloana \"Raion/Dep\" nu a fost setata!");
			result = true;
			return;
		}
	});
	return result;
}

function checkDPI(portletNamespace){
	var result = false;
	var missingDPI = false;
	var ml  = dataTableSplit.data, msg = '', template = '';
	ml.each(function(item,i){
		var data = item.getAttrs(['id_dpi']);
		//daca  e lipsa DPI
		if(typeof $('#displayDPi').html() != 'undefined') {
			if($('#displayDPi').html().indexOf("DPI") != -1) {
				result = false;
				missingDPI = true;
			}
		}
		//numarul DPI nu a fost setat
		if (data.id_dpi == 0 && !missingDPI && !$('#'+portletNamespace+'proiect_nou').is(':checked')){
			alert("In sectiunea \"Alocare articole pe linii\", pe linia " + (i+1) + " coloana \"Nr. DPI\" nu a fost setata!");
			result = true;
			return;
		}
		
	});
	return result;
}

//check if associated account on store lines has 8 characters
function checkAccountLength(){
	var ml  = dataTableSplit.data, msg = '', template = '';
	var result  = false;
	ml.each(function(item,i){
		if(result == true){
			return;
		}
		var data = item.getAttrs(['associated_acc']);
		if (data.associated_acc.length != 8){
			alert("In sectiunea \"Alocare articole pe linii\", pe linia " + (i+1) + " lungimea contului asociat difera de 8 caractere !");
			result = true;
			return;
		}
	});
	return result;
};

function submitCashExpenseForm(portletNamespace) {
	
	var tipRelCom = $('#' + portletNamespace + 'tip-rel-comerciala').val();
	if(tipRelCom == 1 || tipRelCom == 2) {
		if(!$('#' + portletNamespace + 'nr-contract').val() 
					|| $('#' + portletNamespace + 'nr-contract').val() == '') {
			alert("Va rugam completati numarul de contract!");
			$('#' + portletNamespace + 'nr-contract').focus();
		return;
		}
	}
		
	if(tipRelCom == 2 ){
		if(!$('#' + portletNamespace + 'nr-comanda').val() 
					|| $('#' + portletNamespace + 'nr-comanda').val() == '') {
			alert("Va rugam completati numarul de comanda!");
			$('#' + portletNamespace + 'nr-comanda').focus();
			return;
		}
	}
	
	//daca nu setata data scadenta
	if($('#' + portletNamespace + 'datascadenta').val()== null 
			|| $('#' + portletNamespace + 'datascadenta').val() == ""
			||  $('#' + portletNamespace + 'datascadenta').val() == "NaN-NaN-NaN"){
		alert("Va rugam completati data scadenta!");
		$('#' + portletNamespace + 'datascadenta').focus();
		return;
	}
	
	if($('#' + portletNamespace + 'tip-rel-comerciala').val() == null || $('#' + portletNamespace + 'tip-rel-comerciala').val() == ""){
		alert("Va rugam selectati tipul de relatie comerciala!");
		$('#' + portletNamespace + 'tip-rel-comerciala').focus();
		return;
	}
	if (!$('#'+portletNamespace+'capex_diverse').is(':checked') && 
			!$('#'+portletNamespace+'init_cent').is(':checked') &&
			!$('#'+portletNamespace+'proiect_nou').is(':checked')) {
		alert("Va rugam completati sectiunea Detalii CAPEX!");
		return;
		
	}
	if ( !checkCapexDetails(portletNamespace) ) {
		return false;
	}
	
	setStoreIdCashExp(portletNamespace);
	
	var lines  = dataTable.data;
	var storeLines = dataTableSplit.data;
	var jsonLines = lines.toJSON();
	var jsonStoreLines = storeLines.toJSON();
	var stringLineData = JSON.stringify(jsonLines);
	var stringStoreLineData = JSON.stringify(jsonStoreLines);
	
	if(jsonLines == ""){
		alert("Factura nu are nici o linie, va rugam sa adaugati linii la factura!");
		$('.stores_' + $('#' + portletNamespace + 'company').val()).removeClass('hiddencol');
		return;
	}
	if (jsonStoreLines == ""){
		alert("Va rugam sa alocati articolele pe linii!");
		$('.stores_' + $('#' + portletNamespace + 'company').val()).removeClass('hiddencol');
		return;
	}
	
	var lineIndex = -1;
	lineIndex = checkStoresOnStoreLinesCashExp();
	if (lineIndex > 0 ){
		alert("In tabelul din sectiunea \"Alocare articole pe linii\", pe linia " + lineIndex + " magazinul nu a fost setat!");
		$('.stores_' + $('#' + portletNamespace + 'company').val()).removeClass('hiddencol');
		return;
	}
	
	lineIndex = checkDepartmentId();
	if (lineIndex > 0 ){
		alert("In tabelul din sectiunea \"Alocare articole pe linii\", pe linia " + lineIndex + " raionul nu a fost setat!");
		$('.stores_' + $('#' + portletNamespace + 'company').val()).removeClass('hiddencol');
		return;
	}
	
	lineIndex = checkGestiune();
	if (lineIndex > 0 ){
		alert("In tabelul din sectiunea \"Alocare articole pe linii\", pe linia " + lineIndex + " gestiunea nu a fost setata!");
		$('.stores_' + $('#' + portletNamespace + 'company').val()).removeClass('hiddencol');
		return;
	}
	
	if (checkDPI(portletNamespace)){
		$('.stores_' + $('#' + portletNamespace + 'company').val()).removeClass('hiddencol');
		return;
	};
	
	lineIndex = checkPifDateCashExp();
	if (lineIndex > 0 ){
		alert("In tabelul din sectiunea \"Alocare articole pe linii\", pe linia " + lineIndex + " data PIF nu a fost setata!");
		$('.stores_' + $('#' + portletNamespace + 'company').val()).removeClass('hiddencol');
		return;
	}
	
	if($('#' + portletNamespace + 'valoare_totala_alocata').val() != $('#' + portletNamespace + 'val_no_vat_ron').val()){
		$('#' + portletNamespace + 'valoare_totala_alocata').css("border-color", "#FE0000");7
		$('#' + portletNamespace + 'val_no_vat_ron').css("border-color", "#FE0000");
		$('#' + portletNamespace + 'valoare_totala_alocata').focus();
		alert("Valoarea alocata pe linii difera de valoarea facturii!");
		$('.stores_' + $('#' + portletNamespace + 'company').val()).removeClass('hiddencol');
		return;
	}
	$('#' + portletNamespace + 'datatable_data').val(stringLineData);
	$('#' + portletNamespace + 'datatable_lines').val(stringStoreLineData);
	
	$('#' + portletNamespace + 'ADD-cash-exp').append('<input type="submit" name="submit-add-cash" id="submit-add-cash"></input>');
	$('#submit-add-cash').click();
	setTimeout(function() { $('#submit-add-cash').remove(); }, 200);
	
}

function setStoreIdCashExp(portletNamespace){
	var rows = [];
	var ml  = dataTableSplit.data, msg = '', template = '';
	 ml.each(function (item, i) {
	    	var dataS = item.getAttrs(['id_cash_exp', 'id_cash_exp_line','id_store_line','delete_store_line', 'line', 'description','id_store_1','id_store_2','id_store_3','id_store_4','id_store_5',
	    				                 'id_store_6','id_store_7','id_store_8', 'id_store_9','quantity','price_no_vat_curr','id_aisle','associated_acc','id_tip_inreg','id_act_mf','gestiune','id_dpi','vizualizare_dpi',
	    				                 'id_tip_inventar','detalii','cont_imob_in_fct','cont_imob_in_curs','cont_imob_avans','inventory']);
	    	var selectedStore = 0;
	    	var companyId = $('#' + portletNamespace + 'company').val();
			if (companyId == 1) {
				selectedStore = dataS.id_store_1;
			} else if (companyId == 2) {
				selectedStore = dataS.id_store_2;
			} else if (companyId == 3) {
				selectedStore = dataS.id_store_3;
			} else if (companyId == 4) {
				selectedStore = dataS.id_store_4;
			} else if (companyId == 5) {
				selectedStore = dataS.id_store_5;
			} else if (companyId == 6) {
				selectedStore = dataS.id_store_6;
			} else if (companyId == 7) {
				selectedStore = dataS.id_store_7;
			} else if (companyId == 8) {
				selectedStore = dataS.id_store_8;
			} else if (companyId == 9) {
				selectedStore = dataS.id_store_9;
			}
			dataS.id_store = selectedStore;
	    	rows.push(dataS);
	    });
	    
	    dataTableSplit.set('recordset', rows);
}

function submitInvoiceForm(e, portletNamespace, isInvoice) {
	
	//daca nu setata data scadenta
	if($('#' + portletNamespace + 'datascadenta').val()== null 
			|| $('#' + portletNamespace + 'datascadenta').val() == ""
			||  $('#' + portletNamespace + 'datascadenta').val() == "NaN-NaN-NaN"){
		alert("Va rugam completati data scadenta!");
		$('#' + portletNamespace + 'datascadenta').focus();
		return;
	}
	
	if($('#' + portletNamespace + 'val_with_vat_ron').val() < $('#' + portletNamespace + 'val_netaplata_ron').val()) {
		alert("Valoare neta de plata depaseste valoarea facturii.");
		return;
	}
	
	var hasKontan2 = true;
	if($('#' + portletNamespace + 'kontan_acc2').val()== "" || $('#' + portletNamespace + 'kontan_acc2').val()== null){
		hasKontan2 = false;
	}
	
	var hasKontan7 = true;
	if($('#' + portletNamespace + 'kontan_acc7').val()== "" || $('#' + portletNamespace + 'kontan_acc7').val()== null){
		hasKontan7 = false;
	}
	
	var hasKontan8 = true;
	if($('#' + portletNamespace + 'kontan_acc8').val()== "" || $('#' + portletNamespace + 'kontan_acc8').val()== null){
		hasKontan8 = false;
	}
	
	//daca nu e completat contul kontan
	if(!hasKontan2 && !hasKontan7 && !hasKontan8){
		alert("Va rugam completati contul Kontan!");
		return;
	}
	
	if ( !checkCapexDetails(portletNamespace) ) {
		return false;
	}
	var incorrectInvoice = $('input[name="' + portletNamespace + 'marcheaza-fact-eronata"]:checked').val();
	//console.log(incorrectInvoice);
	if (incorrectInvoice == 1){
		var detaliiEroare = $('#' + portletNamespace + 'detalii-eroare').val();
		if(!detaliiEroare || detaliiEroare == '') {
			alert("Va rugam sa completati campul \"Detalii eroare\"");
			$('#' + portletNamespace + 'detalii-eroare').focus();
		} else {
			$('#' + portletNamespace + 'ADD-invoice').submit();
		}
		return;
	}
	var flag = 0;
	var ml  = dataTable.data, msg = '', template = '';
	//cantitatea de articole alocate pe linie > cantitatea alocata unei linii => flag=1
	ml.each(function(item,i){
		var data = item.getAttrs(['quantity','line', 'description']);
		//verifica egalitatea de cantitati pentru fiecare linie in parte
		//console.log("header -> quantity : "+ data.quantity + "; line : "+ data.line);
		if(isInvoice) {
			if(data.description.toLowerCase().indexOf("servicii") < 0 &&
					data.description.toLowerCase().indexOf("montaj") < 0 &&
					data.description.toLowerCase().indexOf("taxe") < 0 &&
					data.description.toLowerCase().indexOf("transport") < 0 ) {
				if(!validQuantity(data.quantity, data.line)){
					flag = 1;
				}
			}
		}
	});
	//for every strore line save the store id into id_store column
	saveStoreIdOnStoreLines();
	var mlA = dataTableSplit.data;
	var json = ml.toJSON();
	var jsonA = mlA.toJSON();
	var stringData = JSON.stringify(json);
	var stringDataA = JSON.stringify(jsonA);
	//console.log(stringDataA);
	//check if all the store line are alocated on stores
	var lineIndex = checkStoresOnStoreLines();
	//console.log(lineIndex);
	if (lineIndex != 0){
		alert("In tabelul din sectiunea \"Alocare articole pe linii\", pe linia " + lineIndex + " magazinul nu a fost setat!");
		return;
	}
	
	//console.log(stringDataA);
	$('#' + portletNamespace + 'datatable_data').val(stringData);
	$('#' + portletNamespace + 'datatable_lines').val(stringDataA);

	var invoice_currency = $('#' + portletNamespace + 'currency');
	//retrive the sum of all store lines 
	var store_line_total_value =  $('#' + portletNamespace + 'val_no_vat_aloc_linii');
	var invoice_value = 0;
	if(invoice_currency.val() == 'RON') {
		invoice_value = $('#' + portletNamespace + 'val_no_vat_ron');
	} else {
		invoice_value = $('#' + portletNamespace + 'val_no_vat_curr');
	}
	
	if(json == ""){
		alert("Factura nu are nici o linie, va rugam sa adaugati linii la factura!");
	} else if (jsonA == ""){
		alert("Va rugam sa alocati articolele pe linii!");
	}else if(parseFloat(invoice_value.val().replace(toReplace,'')) != parseFloat(store_line_total_value.val().replace(toReplace,'')) && isInvoice){
		alert("Valoarea alocata pe linii difera de valoarea facturii!");
	} else if (flag == 0) {
		//check if the associated account corresponds with the reg type
		var acc_regType = checkAccountRegType();
		if(acc_regType != 0){
			alert("In tabelul \"Alocare articole pe linii\", pe linia " + acc_regType +" contul asociat nu corespunde cu tipul de inregistrare!");
			//console.log("incor");
			return;
		}
		if (checkAislesOnStoreLines(portletNamespace)){
			return;
		};
		if(isInvoice){
			if (checkDPI(portletNamespace)){
				return;
			};
		}
		if (checkAccountLength()){
			return;
		}
		
		
		//submit the form
		if (verifyPifDate(portletNamespace)){
			$('#' + portletNamespace + 'ADD-invoice').append('<input type="submit" name="submit-add-invice" id="submit-add-invoice"></input>');
			$('#submit-add-invoice').click();
			setTimeout(function() { $('#submit-add-invoice').remove(); }, 200);
		}

	} else { 
		alert("Valoarea alocata pe linii este prea mare!");
	}
}

//get the exchange rate  - on invoice allocation or creation page
function makeAjaxCallGetExchangeRate(action, date, currency, portletNamespace, ajaxURL) {
	$.ajax({
		type: "POST",
		url: ajaxURL,
		data: 	portletNamespace+"action=" + action +  
		"&" + portletNamespace + "date=" +  date +
		"&" + portletNamespace + "currency_sel=" + currency,
		success: function(msg) {
			$('#' + portletNamespace + 'rata-schimb').val("");
			// get table data
			if (msg != ""){
				var jsonEntries = jQuery.parseJSON(msg);
				//set the exchange rate
				$('#' + portletNamespace + 'rata-schimb').val(jsonEntries.exchange_rate);
				//simulate a change on the exchange rate field to recalculate the invoice values 
				var A = AUI();
				A.one('#' + portletNamespace + 'rata-schimb').simulate('change');
			} else {
				alert("A aparut o eroare in procesul de preluare a ratei de schimb valutar !");
			}
		}
	});
}

// show DPI popup on invoice page
function showDPIPopupInvoice(currentNode, idDPI, portletNamespace, ajaxURL, Y) {
	$.ajax({
		type: 	"POST",
		url: 	ajaxURL,
		data: 	portletNamespace + "dpi=" + idDPI + 
				"&" + portletNamespace + "action=showDpi",
		success: function(msg) {
			// get table data
			if (msg != "" && msg.length > 50){
				// set values on the DPI Form after the AJAX request is over
				var jsonEntries = jQuery.parseJSON(msg);
				var dpiHeader = jsonEntries.header;
				var dpiLines = jsonEntries.lines;

				// set header
				$('#' + portletNamespace + 'nr_dpi').val(idDPI);
				$('#' + portletNamespace + 'data_dpi').val(dpiHeader[0].dpi_date);
				$('#' + portletNamespace + 'aprobator').val(dpiHeader[0].approvers);
				$('#' + portletNamespace + 'solicitant').val(
						capitalize(dpiHeader[0].user_email.replace("@carrefour.com", "").replace("_", " ")));
				$('#' + portletNamespace + 'societate').val(dpiHeader[0].company_name);
				$('#' + portletNamespace + 'categ_capex_dpi').val(dpiHeader[0].tip_capex);

				if (dpiHeader[0].tip_capex == 'Capex Diverse'){
					$('#' + portletNamespace + 'capex_diverse').prop('checked', true);
				} else if (dpiHeader[0].tip_capex == 'Initiativa Centralizata'){
					$('#' + portletNamespace + 'init_cent').prop('checked', true);
				}
				// set lines
				if (dpiLines != null){
					dataTableDPI.set('data', eval(dpiLines));
				}
				
			} else {
				// set empty overlay
				$('#' + portletNamespace + 'nr_dpi').val('');
				$('#' + portletNamespace + 'data_dpi').val('');
				$('#' + portletNamespace + 'aprobator').val('');
				$('#' + portletNamespace + 'solicitant').val('');
				$('#' + portletNamespace + 'societate').val('');
				$('#' + portletNamespace + 'categ_capex_dpi').val('');
				$('#' + portletNamespace + 'capex_diverse').prop('checked', false);
				$('#' + portletNamespace + 'init_cent').prop('checked', false);
				dataTableDPI.set('data', []);
			}
			
		}
	});
	
	var overlayDPI = new Y.Overlay({
	    srcNode:"#vizualizareDPI",
	    width:"900px",
	    align: {
	        node: currentNode,
	        points:["bl", "bl"]
	    }
	});
	overlayDPI.render();
	$('#vizualizareDPI').css('right', '900px');
    $('#vizualizareDPI').show();	
}

/* set all product related values on a line - on invoice allocation screen */
function setProductValuesInvoice(line, prodId, portletNamespace, ajaxURL) {
	$.ajax({
		type: "POST",
		url: ajaxURL,
		data: 	portletNamespace + "action=getProdInfo" +
				"&" + portletNamespace + "prodId=" + prodId,
		success: function(msgjson) {
			// get table data
			if (msgjson != ""){
	   			var rows = [];
			    var ml = dataTable.data, msg = '', template = '';
			    
			    ml.each(function (item, i) {
			    	var data = item.getAttrs(['invoice_line_id', 'delete_line', 'counter', 'line', 'inv_number', 'unit_price_ron', 'price_no_vat_ron', 'price_vat_ron', 'price_vat_curr', 'currency',  'article_code', 'product_code', 
			    	  		                 'description', 'um', 'quantity', 'unit_price_curr', 'price_no_vat_curr', 'vat', 'val_cu_tva', 'warranty',
			    	   		                'id_ias', 'id_ifrs', 'dt_ras', 'dt_ifrs', 'lot', 'clasif', 'lot_warranty', 'cont_imob_in_fct', 
			    	   		                'cont_imob_in_curs', 'cont_imob_avans', 'detalii']);
					console.log(data.line);
					console.log("received = " + line);
			        if (data.product_code == prodId && data.line == line) {
						var jsonEntries = jQuery.parseJSON(msgjson);
						var product = jsonEntries.product;
			        	data.dt_ifrs = product[0].ifrs_dt;
			        	data.dt_ras = product[0].ias_dt;
			        	data.id_ias = product[0].ias_id;
			        	data.id_ifrs = product[0].ifrs_id;
			        	data.clasif = product[0].flag_it;
			        	data.lot = product[0].lot_id;
			        	data.lot_warranty = product[0].lot_warranty;
			        	data.cont_imob_in_fct = product[0].cont_imob_in_fct;
			        	data.cont_imob_in_curs = product[0].cont_imob_in_curs;
			        	data.cont_imob_avans = product[0].cont_imob_avans;
			        	data.warranty = 0;
			        	//daca produsul are garantie
			        	if(data.lot_warranty == "Y") {
			        		if($('#' + portletNamespace + 'procent_garantie').val()!="0"
			        				&& $('#' + portletNamespace + 'procent_garantie').val() !=""
			        				&& $('#' + portletNamespace + 'procent_garantie').val() != null){
			        			 	data.warranty = parseFloat($('#' + portletNamespace + 'procent_garantie').val() / 100 * data.price_no_vat_curr).toFixed(4);
			        		}
			        	}
			        }
		        	rows.push(data);
				});
			    
			    dataTable.set('recordset', rows);
			    setPaymentValues(portletNamespace);
			} else {
				alert('Produsul ales nu exista in baza de date!');
			}
		},
		error: function(msg) {
			alert('A aparut o problema in preluarea datelor de pe server. Va rugam incercati din nou!');
		}
	});
}

//function calculateTotalPayment(garantie, portletNamespace, currency, total){
//	
//	if(currency == "RON") {
//		var totalPayment = $('#' + portletNamespace + 'val-netaplata-ron');
//		if(totalPayment.val() == 0.000 ) {
//			totalPayment.val(total);
//		}
//		totalPayment.val(totalPayment.val() - garantie);
//	} else {
//		var totalPaymentCurr = $('#' + portletNamespace + 'val-netaplata-curr');
//		var totalPaymentRon = $('#' + portletNamespace + 'val-netaplata-ron');
//		if ( totalPayment.val() == 0.000){
//			totalPayment.val(total-garantie);
//		} else {
//			totalPayment.val(totalPayment.val() - garantie);
//		}
//	}
//}

function updateContVouchers(cont, line) {
	var rows = [];
    var ml = dataTableSplit.data, msg = '', template = '';
    
    ml.each(function (item, i) {
    	var data = item.getAttrs(['delete_line', 'line', 'description', 'quantity', 'price_no_vat_ron', 'associated_acc', 'id_tip_inreg', 'id_act_mf', 
    	                          'id_dpi', 'vizualizare_dpi', 'id_tip_inventar', 'detalii', 'cont_imob_in_fct', 'cont_imob_in_curs', 'cont_imob_avans']);
    	if ( data.line == line ) {
    		data.associated_acc = cont;
    	}
    	rows.push(data);
    });
    
    dataTableSplit.set('recordset', rows);
    
    
}

function setIasDtValuesVouchers(iasId, portletNamespace, ajaxURL){
	$.ajax({
		type: "POST",
		url: ajaxURL,
		data: 	portletNamespace + "action=getIasInfo" +
			"&" + portletNamespace + "iasId=" + iasId,
		success: function(msgjson) {
			// get table data
			if (msgjson != ""){
	   			var rows = [];
			    var ml = dataTable.data, msg = '', template = '';
			    
			    ml.each(function (item, i) {
			    	var data = item.getAttrs(['id_voucher', 'delete_line', 'line', 'article_code', 'product_code', 'description', 
			    	 		                 'um', 'quantity', 'unit_price_ron', 'price_no_vat_ron', 'price_vat_ron', 'vat', 
			    			                 'price_with_vat_ron', 'lot', 'ras', 'ifrs', 'dt_ras', 'dt_ifrs', 
			    			                 'is_it', 'detalii', 'cont_imob_in_fct', 
			    			                 'cont_imob_in_curs', 'cont_imob_avans']);
					
			        if (data.ras == iasId) {
						var jsonEntries = jQuery.parseJSON(msgjson);
						var product = jsonEntries.product;
			        	data.dt_ras = product[0].durata_implicita;
			        	data.ras = product[0].id;
			        	data.cont_imob_in_fct = product[0].cont_imob_in_fct;
			        	data.cont_imob_in_curs = product[0].cont_imob_in_curs;
			        	data.cont_imob_avans = product[0].cont_imob_avans;
			        	updateContVouchers(product[0].cont_imob_in_curs, data.line);
			        }
		        	rows.push(data);
				});
			    
			    dataTable.set('recordset', rows);
			} else {
				alert('IAS-ul ales nu exista in baza de date!');
			}
		},
		error: function(msg) {
			alert('A aparut o problema in preluarea datelor de pe server. Va rugam incercati din nou!');
		}
	});
}

function setIasDtValuesCashExp(iasId, portletNamespace, ajaxURL){
	$.ajax({
		type: "POST",
		url: ajaxURL,
		data: 	portletNamespace + "action=getIasInfo" +
			"&" + portletNamespace + "iasId=" + iasId,
		success: function(msgjson) {
			// get table data
			if (msgjson != ""){
	   			var rows = [];
			    var ml = dataTable.data, msg = '', template = '';
			    
			    ml.each(function (item, i) {
			    	var data = item.getAttrs(['id_cash_exp', 'cash_exp_line_id', 'delete_line', 'line', 'counter', 'product_code', 'description', 'um', 'quantity', 'unit_price_curr', 
			    				                 'price_no_vat_curr', 'price_vat_curr', 'vat_code', 'price_with_vat_curr', 'id_ias', 'id_ifrs', 'dt_ias', 'dt_ifrs', 'lot', 'clasif', 
			    				                 'detalii', 'cont_imob_in_fct', 'cont_imob_in_curs', 'cont_imob_in_avans', 'is_new_line']);
					
			        if (data.id_ias == iasId) {
						var jsonEntries = jQuery.parseJSON(msgjson);
						var product = jsonEntries.product;
			        	data.dt_ias = product[0].durata_implicita;
			        	data.id_ias = product[0].id;
			        	data.cont_imob_in_fct = product[0].cont_imob_in_fct;
			        	data.cont_imob_in_curs = product[0].cont_imob_in_curs;
			        	data.cont_imob_avans = product[0].cont_imob_avans;
			        	updateContCashExp(product[0].cont_imob_in_curs, data.cash_exp_line_id);
			        }
		        	rows.push(data);
				});
			    
			    dataTable.set('recordset', rows);
			} else {
				alert('IAS-ul ales nu exista in baza de date!');
			}
		},
		error: function(msg) {
			alert('A aparut o problema in preluarea datelor de pe server. Va rugam incercati din nou!');
		}
	});
}

function updateContCashExp(cont, lineId){
	var rows = [];
    var ml = dataTableSplit.data, msg = '', template = '';
    
    ml.each(function (item, i) {
    	var data = item.getAttrs(['id_cash_exp', 'id_cash_exp_line','id_store_line','delete_store_line', 'line', 'description','id_store_1','id_store_2','id_store_3','id_store_4','id_store_5',
    				                 'id_store_6','id_store_7','id_store_8', 'id_store_9','quantity','price_no_vat_curr','id_aisle','associated_acc','id_tip_inreg','id_act_mf','gestiune','id_dpi','vizualizare_dpi',
    				                 'id_tip_inventar','detalii','cont_imob_in_fct','cont_imob_in_curs','cont_imob_avans','inventory']);
    	if ( data.id_cash_exp_line == lineId ) {
    		data.associated_acc = cont;
    	}
    	rows.push(data);
    });
    
    dataTableSplit.set('recordset', rows);
}

function setIfrsDtValuesVouchers(ifrsId, portletNamespace, ajaxURL){
	$.ajax({
		type: "POST",
		url: ajaxURL,
		data: 	portletNamespace + "action=getIfrsInfo" +
			"&" + portletNamespace + "ifrsId=" + ifrsId,
		success: function(msgjson) {
			// get table data
			if (msgjson != ""){
	   			var rows = [];
			    var ml = dataTable.data, msg = '', template = '';
			    
			    ml.each(function (item, i) {
			    	var data = item.getAttrs(['id_voucher', 'delete_line', 'line', 'article_code', 'product_code', 'description', 
				    	 		                 'um', 'quantity', 'unit_price_ron', 'price_no_vat_ron', 'price_vat_ron', 'vat', 
				    			                 'price_with_vat_ron', 'lot', 'ras', 'ifrs', 'dt_ras', 'dt_ifrs', 
				    			                 'is_it', 'detalii', 'cont_imob_in_fct', 
				    			                 'cont_imob_in_curs', 'cont_imob_avans']);
					
			        if (data.ifrs == ifrsId) {
						var jsonEntries = jQuery.parseJSON(msgjson);
						var product = jsonEntries.product;
			        	data.dt_ifrs = product[0].durata_implicita;
			        	data.ifrs = product[0].id;
			        }
		        	rows.push(data);
				});
			    
			    dataTable.set('recordset', rows);
			} else {
				alert('IFRS-ul ales nu exista in baza de date!');
			}
		},
		error: function(msg) {
			alert('A aparut o problema in preluarea datelor de pe server. Va rugam incercati din nou!');
		}
	});
}

function setIfrsDtValuesCashExp(ifrsId, portletNamespace, ajaxURL){
	$.ajax({
		type: "POST",
		url: ajaxURL,
		data: 	portletNamespace + "action=getIfrsInfo" +
			"&" + portletNamespace + "ifrsId=" + ifrsId,
		success: function(msgjson) {
			// get table data
			if (msgjson != ""){
	   			var rows = [];
			    var ml = dataTable.data, msg = '', template = '';
			    
			    ml.each(function (item, i) {
			    	var data = item.getAttrs(['id_cash_exp', 'cash_exp_line_id', 'delete_line', 'line', 'counter', 'product_code', 'description', 'um', 'quantity', 'unit_price_curr', 
			    				                 'price_no_vat_curr', 'price_vat_curr', 'vat_code', 'price_with_vat_curr', 'id_ias', 'id_ifrs', 'dt_ias', 'dt_ifrs', 'lot', 'clasif', 
			    				                 'detalii', 'cont_imob_in_fct', 'cont_imob_in_curs', 'cont_imob_in_avans', 'is_new_line']);
					
			        if (data.id_ifrs == ifrsId) {
						var jsonEntries = jQuery.parseJSON(msgjson);
						var product = jsonEntries.product;
			        	data.dt_ifrs = product[0].durata_implicita;
			        	data.id_ifrs = product[0].id;
			        }
		        	rows.push(data);
				});
			    
			    dataTable.set('recordset', rows);
			} else {
				alert('IFRS-ul ales nu exista in baza de date!');
			}
		},
		error: function(msg) {
			alert('A aparut o problema in preluarea datelor de pe server. Va rugam incercati din nou!');
		}
	});
}

function updateContInvoices(cont, codProdus){
	var rows = [];
    var ml = dataTableSplit.data, msg = '', template = '';
    
    ml.each(function (item, i) {
    	var data = item.getAttrs(['id_store_line', 'id_store', 'delete_line', 'index', 'article_code', 'description', 'quantity', 'price_no_vat_curr','id_store_1',
 	     		                 'id_store_2', 'id_store_3', 'id_store_4', 'id_store_5', 'id_store_6','id_store_7','id_store_8', 'id_store_9', 'id_department', 
	                             'associated_acc', 'id_tip_inreg', 'id_act_mf', 'id_dpi', 'id_tip_inventar', 'unit_price', 'product_code', 'nr',
	                             'currency', 'inv_number', 'line_id', 'price_no_vat_ron', 'vizualizare_dpi', 'cont_imob_in_curs', 'cont_imob_in_fct', 
	                             'cont_imob_avans', 'id_cat_gestiune', 'detalii', 'inventory']);
    	if ( data.product_code == codProdus ) {
    		data.associated_acc = cont;
    	}
    	rows.push(data);
    });
    
    dataTableSplit.set('recordset', rows);
    $('.stores_' + selectedCompany).removeClass('hiddencol');
}

function setIasDtValuesInvoice(iasId, portletNamespace, ajaxURL){
	$.ajax({
		type: "POST",
		url: ajaxURL,
		data: 	portletNamespace + "action=getIasInfo" +
			"&" + portletNamespace + "iasId=" + iasId,
		success: function(msgjson) {
			// get table data
			if (msgjson != ""){
	   			var rows = [];
			    var ml = dataTable.data, msg = '', template = '';
			    
			    ml.each(function (item, i) {
			    	var data = item.getAttrs(['invoice_line_id', 'delete_line', 'counter', 'line', 'inv_number', 'unit_price_ron', 'price_no_vat_ron', 'price_vat_ron', 'price_vat_curr', 'currency',  'article_code', 'product_code', 
				    	  		              'description', 'um', 'quantity', 'unit_price_curr', 'price_no_vat_curr', 'vat', 'val_cu_tva', 'warranty',
					    	   		          'id_ias', 'id_ifrs', 'dt_ras', 'dt_ifrs', 'lot', 'clasif', 'lot_warranty', 'cont_imob_in_fct', 
					    	   		          'cont_imob_in_curs', 'cont_imob_avans', 'detalii']);
					
			        if (data.id_ias == iasId) {
						var jsonEntries = jQuery.parseJSON(msgjson);
						var product = jsonEntries.product;
			        	data.dt_ras = product[0].durata_implicita;
			        	data.id_ias = product[0].id;
			        	data.cont_imob_in_fct = product[0].cont_imob_in_fct;
			        	data.cont_imob_in_curs = product[0].cont_imob_in_curs;
			        	data.cont_imob_avans = product[0].cont_imob_avans;
			        	updateContInvoices(data.cont_imob_in_curs, data.product_code);
			        }
		        	rows.push(data);
				});
			    
			    dataTable.set('recordset', rows);
			} else {
				alert('IAS-ul ales nu exista in baza de date!');
			}
		},
		error: function(msg) {
			alert('A aparut o problema in preluarea datelor de pe server. Va rugam incercati din nou!');
		}
	});
}

function setIfrsDtValuesInvoice(ifrsId, portletNamespace, ajaxURL){
	$.ajax({
		type: "POST",
		url: ajaxURL,
		data: 	portletNamespace + "action=getIfrsInfo" +
			"&" + portletNamespace + "ifrsId=" + ifrsId,
		success: function(msgjson) {
			// get table data
			if (msgjson != ""){
	   			var rows = [];
			    var ml = dataTable.data, msg = '', template = '';
			    
			    ml.each(function (item, i) {
			    	var data = item.getAttrs(['invoice_line_id', 'delete_line', 'counter', 'line', 'inv_number', 'unit_price_ron', 'price_no_vat_ron', 'price_vat_ron', 'price_vat_curr', 'currency',  'article_code', 'product_code', 
				    	  		              'description', 'um', 'quantity', 'unit_price_curr', 'price_no_vat_curr', 'vat', 'val_cu_tva', 'warranty',
					    	   		          'id_ias', 'id_ifrs', 'dt_ras', 'dt_ifrs', 'lot', 'clasif', 'lot_warranty', 'cont_imob_in_fct', 
					    	   		          'cont_imob_in_curs', 'cont_imob_avans', 'detalii']);
					
			        if (data.id_ifrs == ifrsId) {
						var jsonEntries = jQuery.parseJSON(msgjson);
						var product = jsonEntries.product;
			        	data.dt_ifrs = product[0].durata_implicita;
			        	data.id_ifrs = product[0].id;
			        }
		        	rows.push(data);
				});
			    
			    dataTable.set('recordset', rows);
			} else {
				alert('IFRS-ul ales nu exista in baza de date!');
			}
		},
		error: function(msg) {
			alert('A aparut o problema in preluarea datelor de pe server. Va rugam incercati din nou!');
		}
	});
}

// DELETE store line on invoice{ allocation screen
//id 		: id of the store line in the database
//action 	: what action to make on the ajax call
//index		: the index of the store line in the local array 
function makeAjaxCallDeleteStoreLineInvoice(id, action, index, portletNamespace, ajaxURL){
	$.ajax({
		type: "POST",
		url: ajaxURL,
		data: 	portletNamespace + "action=" + action +  
		"&" + portletNamespace + "id=" +  id +
		"&" + portletNamespace + "index=" + index,
		success: function(msg) {
			// get table data
			if (msg != ""){
				var jsonEntries = jQuery.parseJSON(msg);
				var error		= jsonEntries.error;
				if(error != 0){
					dataTableSplit.removeRow(jsonEntries.index);
				} else {
					alert("A aparut o eroare in procesul de stergere  !");
				}
			}
			}
	});
	$('#' + portletNamespace + 'val_no_vat_art').autoNumeric('update', {aSep: ',', mDec: 4, vMin : -999999999, vMax : 999999999 });
	$('#' + portletNamespace + 'val_no_vat_aloc_linii').autoNumeric('update', {aSep: ',', mDec: 4, vMin : -999999999, vMax : 999999999 });
	$('#' + portletNamespace + 'difference').autoNumeric('update', {aSep: ',', mDec: 4, vMin : -999999999, vMax : 999999999 });
}

// make ajax call to delete line on invoice allocation screen
function makeAjaxCallDeleteLineInvoice(lineIDs, action, portletNamespace, ajaxURL, isAlone){
	$.ajax({
		type: "POST",
		url: ajaxURL,
		data: 	portletNamespace + "action=" + action +  
		"&" + portletNamespace + "line_id=" +  lineIDs,
		success: function(msg) {
			// get table data
			if (msg != ""){
				var jsonEntries = jQuery.parseJSON(msg);
				var error		= jsonEntries.error;
				if(error != 0){
					//delete associated lines from interface
					if(lineIDs.constructor === Array) {
						for (var i = lineIDs.length - 1; i >= 0; i--) {
							deleteLineFromInterfaceByDatabaseId(lineIDs[i], portletNamespace );
						}
					} else {
						deleteLineFromInterfaceByDatabaseId(lineIDs, portletNamespace );
					}
					
				    if(isAlone) {
				    	alert("Linia a fost stearsa cu succes!");
					}
				} else {
					 if(isAlone) {
						 alert("A aparut o eroare in procesul de stergere  !");
					 }
				}
			}
			}
	});
	$('#' + portletNamespace + 'val_no_vat_art').autoNumeric('update', {aSep: ',', mDec: 4, vMin : -999999999, vMax : 999999999 });
	$('#' + portletNamespace + 'val_no_vat_aloc_linii').autoNumeric('update', {aSep: ',', mDec: 4, vMin : -999999999, vMax : 999999999 });
	$('#' + portletNamespace + 'difference').autoNumeric('update', {aSep: ',', mDec: 4, vMin : -999999999, vMax : 999999999 });
}

/**
 * 
 * @param portletId
 * @param supplier_code
 * @param acc2
 */
function setAssociatedKontanAccInvoice(portletId, supplier_code, account_value, supplierName, isAdvanceRequestFromAccounting){
	if (supplier_code.substring(0,1) == "7"){
		$('#' + portletId + 'acc2').hide();
		$('#' + portletId + 'kontan_acc2').prop('required',false);
		$('#' + portletId + 'acc8').hide();
		$('#' + portletId + 'kontan_acc8').prop('required',false);
		
		var companie = $('#' + portletId + 'company').val();
		console.log("companie : " + companie);
		//Compania e Artima si furnizorul e CRF
		if( companie == 3 && supplierName.match('carrefour')>=0 ){
			$('#' + portletId + 'kontan_acc7').append('<option value="45119601"> MF - 45119601</option>');
			$('#' + portletId + 'kontan_acc7').append('<option value="45119601"> OBI - 45119601</option>');
			$('#' + portletId + 'kontan_acc7').append('<option value="45119612"> MF - 45119612</option>');
			$('#' + portletId + 'kontan_acc7').append('<option value="45119612"> OBI - 45119612</option>');
		} 
		//Companie a CRF si furnizorul e Artima
		if( companie == 2 && supplierName.match('artima')>=0 ){
			$('#' + portletId + 'kontan_acc7').append('<option value="45119602"> MF - 45119602</option>');
			$('#' + portletId + 'kontan_acc7').append('<option value="45119602"> OBI - 45119602</option>');
		}
		//Companie a Supeco si furnizorul e Artima
		if( companie == 1 && supplierName.match('artima')>=0 ){
			$('#' + portletId + 'kontan_acc7').append('<option value="45119604"> MF - 45119604</option>');
			$('#' + portletId + 'kontan_acc7').append('<option value="45119604"> OBI - 45119604</option>');
		}
		//Companie a Supeco si furnizorul e CRF
		if( companie == 1 && supplierName.match('carrefour')>=0 ){
			$('#' + portletId + 'kontan_acc7').append('<option value="45119605"> MF - 45119605</option>');
			$('#' + portletId + 'kontan_acc7').append('<option value="45119605"> OBI - 45119605</option>');
		}
		//Companie a MGC si furnizorul e CRF
		if( companie == 4 && supplierName.match('carrefour')>=0 ){
			$('#' + portletId + 'kontan_acc7').append('<option value="45119606"> MF - 45119606</option>');
			$('#' + portletId + 'kontan_acc7').append('<option value="45119606"> OBI - 45119606</option>');
		}
		//Compania e Artima si furnizorul e Supeco
		if( companie == 3 && supplierName.match('supeco')>=0 ){
			$('#' + portletId + 'kontan_acc7').append('<option value="45119607"> MF - 45119607</option>');
			$('#' + portletId + 'kontan_acc7').append('<option value="45119607"> OBI - 45119607</option>');
			$('#' + portletId + 'kontan_acc7').append('<option value="45119612"> MF - 45119612</option>');
			$('#' + portletId + 'kontan_acc7').append('<option value="45119612"> OBI - 45119612</option>');
		} 
		// Columbus operational
		if( companie == 7){
			$('#' + portletId + 'kontan_acc7').append('<option value="45119609"> MF - 45119609</option>');
			$('#' + portletId + 'kontan_acc7').append('<option value="45119609"> OBI - 45119609</option>');
			$('#' + portletId + 'kontan_acc7').append('<option value="45119610"> MF - 45119610</option>');
			$('#' + portletId + 'kontan_acc7').append('<option value="45119610"> OBI - 45119610</option>');
			$('#' + portletId + 'kontan_acc7').append('<option value="45119024"> MF - 45119024</option>');
			$('#' + portletId + 'kontan_acc7').append('<option value="45119613"> MF - 45119613</option>');
			$('#' + portletId + 'kontan_acc7').append('<option value="45119613"> OBI - 45119613</option>');
			$('#' + portletId + 'kontan_acc7').append('<option value="45119513"> MF - 45119513</option>');
			$('#' + portletId + 'kontan_acc7').append('<option value="45119513"> OBI - 45119513</option>');
		}
		// Columbus Active
		if( companie == 8){
			$('#' + portletId + 'kontan_acc8').append('<option value="45119024"> MF - 45119024</option>');
		} 
		
		$('#' + portletId + 'kontan_acc7').append('<option value="40130300"> OBI - 40130300</option>');
		
		$('#' + portletId + 'acc7').show();
		if(!isAdvanceRequestFromAccounting) {
			$('#' + portletId + 'kontan_acc7').prop('required',true);
		}
		$('#' + portletId + 'kontan_acc7').val(account_value);
	} else if (supplier_code.substring(0,1) == "8"){
		$('#' + portletId + 'acc2').hide();
		$('#' + portletId + 'kontan_acc2').prop('required',false);
		$('#' + portletId + 'acc8').show();
		if(!isAdvanceRequestFromAccounting) {
			$('#' + portletId + 'kontan_acc8').prop('required',true);
		}
		$('#' + portletId + 'acc7').hide();
		$('#' + portletId + 'kontan_acc7').prop('required',false);
		
		$('#' + portletId + 'kontan_acc8').val(account_value);
	} else if (supplier_code.substring(0,1) == "2"){
		$('#' + portletId + 'acc2').show();
		if(!isAdvanceRequestFromAccounting) {
			$('#' + portletId + 'kontan_acc2').prop('required',true);
		}
		$('#' + portletId + 'acc8').hide();
		$('#' + portletId + 'kontan_acc8').prop('required',false);
		$('#' + portletId + 'acc7').hide();
		$('#' + portletId + 'kontan_acc7').prop('required',false);
		
		$('#' + portletId + 'kontan_acc2').val(account_value);
	}
}

//invoice => sets the supplier type based on the selected supplier code
function setSupplierType(portletId, supplier_code){
	console.log("Supplier code : js line 3385" + supplier_code);
	//supplier_code begins with "8" => supplier type = Extern
	if (supplier_code.substring(0,1) == "8"){
		$('#' + portletId + 'supplier_type_invoice').val("1");
	} else {
		//supplier_code doesn't begin with "8" => supplier type = Intern
		$('#' + portletId + 'supplier_type_invoice').val("0");
	}
}

//show DPI popup on invoice page
function showDPIPopupVoucher(currentNode, idDPI, portletNamespace, ajaxURL, Y) {
	$.ajax({
		type: 	"POST",
		url: 	ajaxURL,
		data: 	portletNamespace + "dpi=" + idDPI + 
				"&" + portletNamespace + "action=showDpi",
		success: function(msg) {
			// get table data
			if (msg != "" && msg.length > 50){
				// set values on the DPI Form after the AJAX request is over
				var jsonEntries = jQuery.parseJSON(msg);
				var dpiHeader = jsonEntries.header;
				//console.log(dpiHeader);
				var dpiLines = jsonEntries.lines;

				// set header
				$('#' + portletNamespace + 'nr_dpi').val(idDPI);
				$('#' + portletNamespace + 'data_dpi').val(dpiHeader[0].dpi_date);
				$('#' + portletNamespace + 'aprobator').val(dpiHeader[0].approvers);
				$('#' + portletNamespace + 'solicitant').val(capitalize(dpiHeader[0].user_email.replace("@carrefour.com", "").replace("_", " ")));
				$('#' + portletNamespace + 'societate').val(dpiHeader[0].company_name);
				$('#' + portletNamespace + 'categ_capex_dpi').val(dpiHeader[0].tip_capex);

				if (dpiHeader[0].tip_capex == 'Capex Diverse'){
					$('#' + portletNamespace + 'capex_diverse').prop('checked', true);
				} else if (dpiHeader[0].tip_capex == 'Initiativa Centralizata'){
					$('#' + portletNamespace + 'init_cent').prop('checked', true);
				}
				// set lines
				if (dpiLines != null){
					dataTableDPI.set('data', eval(dpiLines));
				}
			} else {
				// set empty data
				$('#' + portletNamespace + 'nr_dpi').val('');
				$('#' + portletNamespace + 'data_dpi').val('');
				$('#' + portletNamespace + 'aprobator').val('');
				$('#' + portletNamespace + 'solicitant').val('');
				$('#' + portletNamespace + 'societate').val('');
				$('#' + portletNamespace + 'categ_capex_dpi').val('');
				$('#' + portletNamespace + 'capex_diverse').prop('checked', false);
				$('#' + portletNamespace + 'init_cent').prop('checked', false);
				dataTableDPI.set('data', []);
			}
			
		}
	});
	
	var overlayDPI = new Y.Overlay({
	    srcNode:"#vizualizareDPI",
	    width:"900px",
	    align: {
	        node: currentNode,
	        points:["bl", "bl"]
	    }
	});
	overlayDPI.render();
	$('#vizualizareDPI').css('right', '900px');
    $('#vizualizareDPI').show();	
}

//show DPI popup on CashExpense
function showDPIPopupCashExp(currentNode, idDPI, portletNamespace, ajaxURL, Y) {
	$.ajax({
		type: 	"POST",
		url: 	ajaxURL,
		data: 	portletNamespace + "dpi=" + idDPI + 
				"&" + portletNamespace + "action=showDpi",
		success: function(msg) {
			// get table data
			if (msg != "" && msg.length > 50){
				// set values on the DPI Form after the AJAX request is over
				var jsonEntries = jQuery.parseJSON(msg);
				var dpiHeader = jsonEntries.header;
				//console.log(dpiHeader);
				var dpiLines = jsonEntries.lines;

				// set header
				$('#' + portletNamespace + 'nr_dpi').val(idDPI);
				$('#' + portletNamespace + 'data_dpi').val(dpiHeader[0].dpi_date);
				$('#' + portletNamespace + 'aprobator').val(dpiHeader[0].approvers);
				$('#' + portletNamespace + 'solicitant').val(capitalize(dpiHeader[0].user_email.replace("@carrefour.com", "").replace("_", " ")));
				$('#' + portletNamespace + 'societate').val(dpiHeader[0].company_name);
				$('#' + portletNamespace + 'categ_capex_dpi').val(dpiHeader[0].tip_capex);

				if (dpiHeader[0].tip_capex == 'Capex Diverse'){
					$('#' + portletNamespace + 'capex_diverse').prop('checked', true);
				} else if (dpiHeader[0].tip_capex == 'Initiativa Centralizata'){
					$('#' + portletNamespace + 'init_cent').prop('checked', true);
				}
				// set lines
				if (dpiLines != null){
					dataTableDPI.set('data', eval(dpiLines));
				}
			} else {
				// set empty data
				$('#' + portletNamespace + 'nr_dpi').val('');
				$('#' + portletNamespace + 'data_dpi').val('');
				$('#' + portletNamespace + 'aprobator').val('');
				$('#' + portletNamespace + 'solicitant').val('');
				$('#' + portletNamespace + 'societate').val('');
				$('#' + portletNamespace + 'categ_capex_dpi').val('');
				$('#' + portletNamespace + 'capex_diverse').prop('checked', false);
				$('#' + portletNamespace + 'init_cent').prop('checked', false);
				dataTableDPI.set('data', []);
			}
			
		}
	});
	
	var overlayDPI = new Y.Overlay({
	    srcNode:"#vizualizareDPI",
	    width:"900px",
	    align: {
	        node: currentNode,
	        points:["bl", "bl"]
	    }
	});
	overlayDPI.render();
	$('#vizualizareDPI').css('right', '900px');
    $('#vizualizareDPI').show();	
}


function updateDpiListAjax(portletNamespace, ajaxURL, storeId){
	$.ajax({
		type: 	"POST",
		url: 	ajaxURL,
		data: 	portletNamespace + "storeId=" + storeId + 
				"&" + portletNamespace + "action=filterDpis",
		success: function(msg) {
			// get table data
			if (msg != ""){
				var jsonEntries = jQuery.parseJSON(msg);
				var dpi = jsonEntries.dpis;
				
				//remove all the existing options
				$('#' + portletNamespace + 'select_dpi').empty();
				$('#' + portletNamespace + 'select_dpi').append('<option value="" selected="selected" >DPI</option>');
				//append the new options
				for (var i = 0; i < dpi.length; i++){
					$('#' + portletNamespace + 'select_dpi').append('<option value="' + dpi[i].id +'">' + dpi[i].id + '(' + dpi[i].dpi_date +
							')' + '</option>');
				}
				
				//trigger an update
				$('#' + portletNamespace + 'select_dpi').trigger("chosen:updated");
			}
		}
	});
}


function makeAjaxCallGetBaseInvoices(portletNamespace, action, supplier, company, ajax){
	$.ajax({
		type: "POST",
		url: ajax,
		data: 	portletNamespace + "action=" + action +
				"&" + portletNamespace + "supplier_name=" + supplier +
				"&" + portletNamespace + "id_company=" + company,
				success: function(msg) {
					//reset the list that contains the resulted invoices
					var baseInvoices = [];
					
  					// get resulted id
  					if (msg != ""){
  						var jsonEntries = jQuery.parseJSON(msg);
  						baseInvoices = jsonEntries.baseInvoices;
  						//get the select object
  						var baseInvoicesSelect = $('#' + portletNamespace + 'nr-fact-baza');
  						//remove all the existing options
  						baseInvoicesSelect.empty();
  						if (baseInvoicesSelect){
  							//append all the options resulted from the ajax call
  							for ( i = 0; i < baseInvoices.length; i++){
  								baseInvoicesSelect.append('<option value=' + baseInvoices[i].id + '>' + baseInvoices[i].inv_number + '</option>');
  							};
  						}
  					}
  				}
	});
}

//set invoice option based on supplier type
function setInvoiceOption(portletId){
	var supplierType = $('#' + portletId + 'supplier_type_invoice').val();
	if (supplierType == "0"){
		//0 inseamna Intern deci afisam optiune factura interna si ascundem optiune factura externa
		$('#' + portletId + 'extern_invoice_label_display').hide();
		$('#' + portletId + 'extern_invoice_display').hide();
		$('#' + portletId + 'intern_invoice_label_display').show();
		$('#' + portletId + 'intern_invoice_display').show();
		$('#' + portletId + 'extern_invoice_option').prop('required',false);
		$('#' + portletId + 'intern_invoice_option').prop('required',true);
	} else if (supplierType == "1"){
		$('#' + portletId + 'extern_invoice_label_display').show();
		$('#' + portletId + 'extern_invoice_display').show();
		$('#' + portletId + 'intern_invoice_label_display').hide();
		$('#' + portletId + 'intern_invoice_display').hide();
		$('#' + portletId + 'extern_invoice_option').prop('required',true);
		$('#' + portletId + 'intern_invoice_option').prop('required',false);
	}
}

function showInventoryOverlay(e, Y) {
	// end of prepare table
    $('#den_art_header').text(denArt);
    $('#cod_art_header').text(codArt);
	
    // prepare overlay
    var currentNode = this;
	var overlay = new Y.Overlay({
	    srcNode:"#overlay-detalii-inv",
	    width:"400px",
	    align: {
	        node: e.target,
	        points:["bl", "tl"]
	    }
	});
	
	overlay.render();

    $('#overlay-detalii-inv').show();
    
	// bind actions on pop-up
}

function showDivisionOverlay(e, Y, servicii){
	var remoteDataDivision = [];
	var ml = dataTable.data, msg = '', template = '';
	ml.each(function (item, i) {
	var data = item.getAttrs(['invoice_line_id', 'delete_line', 'counter', 'line', 'inv_number', 'unit_price_ron', 'price_no_vat_ron', 'price_vat_ron', 'price_vat_curr', 'currency',  'article_code', 'product_code', 
       		                 'description', 'um', 'quantity', 'unit_price_curr', 'price_no_vat_curr', 'vat', 'val_cu_tva', 'warranty',
	           		                'id_ias', 'id_ifrs', 'dt_ras', 'dt_ifrs', 'lot', 'clasif', 'lot_warranty', 'cont_imob_in_fct', 
	           		                'cont_imob_in_curs', 'cont_imob_avans', 'detalii']);
		if(data.description.toLowerCase().indexOf("servicii") < 0 &&
				servicii.indexOf(data.description.toString().toLowerCase()) < 0) {
			remoteDataDivision.push({
				delete_line : data.delete_line,
				product_code : data.description,
				old_price : data.price_no_vat_curr
			});
		}
	 });
	 dataTableDivision.set('recordset', remoteDataDivision);
	var overlay = new Y.Overlay({
	    srcNode:"#overlay-repartizare-servicii",
	    width:"400px",
	    align: {
	        node: e.target,
	        points:["bl", "tl"]
	    }
	});
	overlay.render();
    $('#overlay-repartizare-servicii').show();
}

function saveDivision(portletId, servicii){
	var productCodes = [];
	var oldPrices = [];
	var deleteLines = [];
	var sumOldPrices = 0;
	var ml = dataTableDivision.data, msg = '', template = '';
	ml.each(function (item, i) {
	var data = item.getAttrs(['product_code', 'select', 'old_price', 'delete_line']);
		if(data.select == true ){
			deleteLines.push(data.delete_line);
			productCodes.push(data.product_code);
			oldPrices.push(parseFloat(data.old_price));
			sumOldPrices += parseFloat(data.old_price);
		}
	 });
	
	var rows = [];
	var priceToBeAdded = 0;
	var ml = dataTable.data, msg = '', template = '';
	ml.each(function (item, i) {
	var data = item.getAttrs(['invoice_line_id', 'delete_line', 'counter', 'line', 'inv_number', 'unit_price_ron', 'price_no_vat_ron', 'price_vat_ron', 'price_vat_curr', 'currency',  'article_code', 'product_code', 
       		                 'description', 'um', 'quantity', 'unit_price_curr', 'price_no_vat_curr', 'vat', 'val_cu_tva', 'warranty',
	           		                'id_ias', 'id_ifrs', 'dt_ras', 'dt_ifrs', 'lot', 'clasif', 'lot_warranty', 'cont_imob_in_fct', 
	           		                'cont_imob_in_curs', 'cont_imob_avans', 'detalii']);
		if( data.description.toLowerCase().indexOf("servicii") >= 0||
				servicii.indexOf(data.description.toString().toLowerCase()) >= 0 ) {
			priceToBeAdded = parseFloat(data.price_no_vat_curr).toFixed(4);
		}
	 });
	
	var procents = [];
	var newPrices = [];
	for(var i = 0 ; i < oldPrices.length; i++ ){
		procents[i] = (oldPrices[i]*100)/sumOldPrices;
		newPrices[i] = parseFloat(oldPrices[i] + priceToBeAdded*procents[i]/100).toFixed(4);
	}
	
	console.log('oldPrices ' + oldPrices);
	console.log('productcodes ' + oldPrices);
	
	ml.each(function (item, i) {
		var data = item.getAttrs(['invoice_line_id', 'delete_line', 'counter', 'line', 'inv_number', 'unit_price_ron', 'price_no_vat_ron', 'price_vat_ron', 'price_vat_curr', 'currency',  'article_code', 'product_code', 
	       		                 'description', 'um', 'quantity', 'unit_price_curr', 'price_no_vat_curr', 'vat', 'val_cu_tva', 'warranty',
		           		                'id_ias', 'id_ifrs', 'dt_ras', 'dt_ifrs', 'lot', 'clasif', 'lot_warranty', 'cont_imob_in_fct', 
		           		                'cont_imob_in_curs', 'cont_imob_avans', 'detalii']);

				for(var i = 0 ; i < oldPrices.length; i++ ){
					if(data.delete_line == deleteLines[i]) {
						data.price_no_vat_curr = newPrices[i];
						data.val_cu_tva = parseFloat(parseFloat(data.price_no_vat_curr) + parseFloat(data.price_no_vat_curr) * parseFloat(data.vat) / 100).toFixed(4);
					}
				}
			rows.push(data);
		 });
	 dataTable.set('recordset', rows);
	 $('#overlay-repartizare-servicii').hide();
	 setTotalNoVat(portletId);
	 setTotalNoVatLines(portletId);
	 setPaymentValues(portletId);
}

//suppliers.jsp => show supplier popup
function displaySupplierOverlay(portletId, supplierData){
	var supplierId = $('#' + portletId + 'new_furnizor_id').val();
	//var supplierCode = jQuery.parseJSON($('#<portlet:namespace/>supplier_code').val());
	var supplierCode = $('#' + portletId + 'supplier_code').val();
	
	var companyId = $('#' + portletId + 'company').val();
	//console.log("lala");
	//console.log($('#<portlet:namespace/>supplier_data').val());
	if (companyId == "") {
		alert('Setati societatea!');
		return false;
	}
	if (supplierCode > 0) {
		alert('Furnizorul exista deja in Kontan!');
		return false;
	} else {
		clearSupplierValues(portletId);
    	//set the selected company
    	$('#' + portletId + 'new_company').val(supplierData.id_company);
    	$('#' + portletId + 'new_furnizor').val(supplierData.name);
    	$('#' + portletId + 'new_cui').val(supplierData.cui);
    	$('#' + portletId + 'new_email').val(supplierData.email);
    	if (supplierData.RO == "RO") {
    		$('#' + portletId + 'new_roCheckbox').prop('checked', true);
    		$('#' + portletId + 'new_roCheckbox').val(supplierData.RO);
    	}
    	$('#' + portletId + 'new_telefon').val(supplierData.phone);
    	$('#' + portletId + 'new_J_field').val(supplierData.reg_com);
    	$('#' + portletId + 'new_banca_beneficiar').val(supplierData.payment_code);
    	$('#' + portletId + 'new_banca').val(supplierData.bank);
    	$('#' + portletId + 'new_termen_plata').val(supplierData.payment_term);
    	$('#' + portletId + 'new_cont_bancar').val(supplierData.bank_account);
    	$('#' + portletId + 'new_cod_swift').val(supplierData.swift);
    	$('#overlay').show();
    	return;
	}
}

function setInvoiceSupplierOptionValue(portletId, invoiceSupplierOptionValue){
	setInvoiceOption(portletId);
	
	if (invoiceSupplierOptionValue == -1){
		var supplierType = $('#' + portletId + 'supplier_type_invoice').val();
		if (supplierType == "0"){
			$('#' + portletId + 'intern_invoice_option').val("");
		} else if (supplierType == "1"){
			$('#' + portletId + 'extern_invoice_option').val("");
		}
	} else if (invoiceSupplierOptionValue < 4){
		//options from 0-3 : extern supplier option
		$('#' + portletId + 'extern_invoice_option').val(invoiceSupplierOptionValue);
	} else {
		//options from 4,5 : intern supplier option
		$('#' + portletId + 'intern_invoice_option').val(invoiceSupplierOptionValue);
	}
}

function recalculatePaymentValue(newValue, existingData, portletId){
	var currency = $('#' + portletId + 'currency');
	var exchangeRate = $('#' + portletId + 'rata-schimb');
	var paymentValueRon = $('#' + portletId + 'val-netaplata-ron');
	var paymentValueCurr = $('#' + portletId + 'val-netaplata-eur');
	if (currency.val() == 'RON'){
		console.log(paymentValueRon);
		console.log(existingData.warranty.initValue);
		var newcalculatedvalue = parseFloat(parseFloat(paymentValueRon.val().replace(toReplace,'')) + parseFloat(existingData.warranty.initValue)).toFixed(4);
		console.log(parseFloat(parseFloat(newcalculatedvalue) - parseFloat(newValue).toFixed(4)));
		paymentValueRon.val(parseFloat(parseFloat(newcalculatedvalue) - parseFloat(newValue).toFixed(4)).toFixed(4));
	} else {
		var newcalculatedvaluecurr = parseFloat(parseFloat(paymentValueCurr.val().replace(toReplace,'')) + parseFloat(existingData.warranty.initValue)).toFixed(4);
		paymentValueCurr.val(parseFloat(newcalculatedvaluecurr - parseFloat(newValue).toFixed(4)).toFixed(4));
		var newcalculatedvalue = parseFloat(parseFloat(paymentValueRon.val().replace(toReplace,'')) + parseFloat(existingData.warranty.initValue*parseFloat(exchangeRate.val()))).toFixed(4);
		paymentValueRon.val(parseFloat(newcalculatedvalue - parseFloat(newValue*parseFloat(exchangeRate.val())).toFixed(4)).toFixed(4));
	}
	
	$('#' + portletId + 'val-netaplata-eur').autoNumeric('update', {aSep: ',', mDec: 4, vMin : -999999999, vMax : 999999999 });
	$('#' + portletId + 'val-netaplata-ron').autoNumeric('update', {aSep: ',', mDec: 4, vMin : -999999999, vMax : 999999999 });
}


//ALOCATE NEW STORE LINE
function existsAssociatedStoreLine(lineId){
	var returnValue = "";
	var alreadyDup = false;
	var ml = dataTableSplit.data, msg = '', template = '';
	ml.each(function (item, i) {
		var data = item.getAttrs(['id_store_line', 'id_store', 'delete_line', 'index', 'article_code', 'description', 'quantity', 'price_no_vat_curr','id_store_1',
	    		                 'id_store_2', 'id_store_3', 'id_store_4', 'id_store_5', 'id_store_6','id_store_7','id_store_8', 'id_store_9', 'id_department', 
	                             'associated_acc', 'id_tip_inreg', 'id_act_mf', 'id_dpi', 'id_tip_inventar', 'unit_price', 'product_code', 'nr',
	                             'currency', 'inv_number', 'line_id', 'price_no_vat_ron', 'vizualizare_dpi', 'cont_imob_in_curs', 'cont_imob_in_fct', 
	                             'cont_imob_avans', 'id_cat_gestiune', 'detalii', 'inventory', 'initiative', 'new_prj']);
		if (data.nr == lineId && !alreadyDup){
			alreadyDup = true;
			data.inventory = "";
			data.delete_line = storeLineCounter + 1;
			if(data.product_code == 3562 ) {
				data.detalii = data.detalii;
			} else {
				data.detalii = storeLineCounter + 1;
			}
			
			data.index = remoteDataSplit.length + 1;
			returnValue = data;
		}
	});
	
	return returnValue;
}


function createNewStoreLine(lineId, portletId) {
	if ($('#'+ portletId +'company').val() == "") {
		alert ("Va rugam selectati o societate!");
		return;
	}
	
	if (companyHasChanged) {
		storeid = 0;
	}
	
    var ml  = dataTable.data, msg = '', template = '';
    ml.each(function (item, i) {
        var data = item.getAttrs(['invoice_line_id', 'delete_line', 'counter', 'line', 'inv_number', 'unit_price_ron', 'price_no_vat_ron', 
                                  'price_vat_ron', 'price_vat_curr', 'currency',  'article_code', 'product_code', 
          		                 'description', 'um', 'quantity', 'unit_price_curr', 'price_no_vat_curr', 'vat', 'val_cu_tva', 'warranty',
           		                'id_ias', 'id_ifrs', 'dt_ras', 'dt_ifrs', 'lot', 'clasif', 'lot_warranty', 'cont_imob_in_fct', 
           		                'cont_imob_in_curs', 'cont_imob_avans', 'detalii']);

        if (data.line == lineId) {
        	var dupStoreLine = existsAssociatedStoreLine(lineId);
        	createStoreLine(data, portletId, storeid, dupStoreLine);
        	return;
        }
    });
    
   //	setTotalNoVat('<portlet:namespace />');
    //calculeaza total fara tva alocat pe linii
    setTotalNoVatLines(portletId);
    //recalculate payment values
   // setPaymentValues('<portlet:namespace />');
    $('#'+ portletId +'val_no_vat_art').autoNumeric('update', {aSep: ',', mDec: 4, vMin : -999999999, vMax : 999999999 });
	$('#'+ portletId +'val_no_vat_aloc_linii').autoNumeric('update', {aSep: ',', mDec: 4, vMin : -999999999, vMax : 999999999 });
	$('#'+ portletId +'difference').autoNumeric('update', {aSep: ',', mDec: 4, vMin : -999999999, vMax : 999999999 });
}

function ajaxSetDpiAndCategCapex(portletNamespace, action, ajax, dpi){
	$.ajax({
		type: "POST",
		url: ajax,
		data: 	portletNamespace + "action=" + action +
				"&" + portletNamespace + "id_dpi=" + dpi,
				success: function(msg) {
					var tipCapex = "";
					var project = "";
					var init = "";
  					if (msg != ""){
  						var jsonEntries = jQuery.parseJSON(msg);
  						tipCapex = jsonEntries.tipCapex;
  						project = jsonEntries.project;
  						init = jsonEntries.init;
  						if (tipCapex == "0"){
  							$('#' + portletNamespace + 'capex_diverse').prop('checked', true);
  						} else if (tipCapex == "1"){
  							$('#' + portletNamespace + 'init_cent').prop('checked', true);
  							$('#' + portletNamespace + 'lista_initiative').val(init);
  							$('#' + portletNamespace + 'lista_initiative').trigger("chosen:updated");
  						} else {
  							$('#' + portletNamespace + 'proiect_nou').prop('checked', true);
  							$('#' + portletNamespace + 'project').val(project);
  							$('#' + portletNamespace + 'project').trigger("chosen:updated");
  						}
  						
  						alocateAllDpi(portletNamespace);
  					}
  				}
	});
}

function ajaxFilterDpis(portletNamespace, action, ajax){
	var storeFilter = $('#' + portletNamespace + 'filter_dpi_store').val();
	var prodFilter = $('#' + portletNamespace + 'filter_dpi_prod').val();
	var valueFilter = $('#' + portletNamespace + 'filter_dpi_value').val();
	
	$.ajax({
		type: "POST",
		url: ajax,
		data: 	portletNamespace + "action=" + action +
				"&" + portletNamespace + "store=" + storeFilter +
				"&" + portletNamespace + "prod=" + prodFilter +
				"&" + portletNamespace + "value=" + valueFilter,
				success: function(msg) {
					var jsonEntries = jQuery.parseJSON(msg);
					var dropDown = jsonEntries.dpis;
					$('#' + portletNamespace + 'alocare_dpi').html("");
					$('#' + portletNamespace + 'alocare_dpi').html(dropDown);
					$('#' + portletNamespace + 'alocare_dpi').trigger("chosen:updated");
  				}
	});
}

function removeOldInventory(obj, portletId) {
	var rows = [];
	 var ml  = dataTableSplit.data, msg = '', template = '';
	 ml.each(function (item, i) {
			var data = item.getAttrs(['id_store_line', 'id_store', 'delete_line', 'index', 'article_code', 'description', 'quantity', 'price_no_vat_curr','id_store_1',
	    	     		                 'id_store_2', 'id_store_3', 'id_store_4', 'id_store_5', 'id_store_6','id_store_7','id_store_8', 'id_store_9', 'id_department', 
	    	                             'associated_acc', 'id_tip_inreg', 'id_act_mf', 'id_dpi', 'id_tip_inventar', 'unit_price', 'product_code', 'nr',
	    	                             'currency', 'inv_number', 'line_id', 'price_no_vat_ron', 'vizualizare_dpi', 'cont_imob_in_curs', 'cont_imob_in_fct', 
	    	                             'cont_imob_avans', 'id_cat_gestiune', 'detalii', 'inventory']);
			if(data.id_store_1 == obj.id_store_1) {
				data.inventory = "";
			}
			if(data.id_store_2 == obj.id_store_2) {
				data.inventory = "";
			}
			if(data.id_store_3 == obj.id_store_3) {
				data.inventory = "";
			}
			if(data.id_store_5 == obj.id_store_5) {
				data.inventory = "";
			}
			if(data.id_store_6 == obj.id_store_6) {
				data.inventory = "";
			}
			if(data.id_store_7 == obj.id_store_7) {
				data.inventory = "";
			}
			if(data.id_store_8 == obj.id_store_8) {
				data.inventory = "";
			}
			if(data.id_store_9 == obj.id_store_9) {
				data.inventory = "";
			}
			rows.push(data);
	 });
	 dataTableSplit.set('recordset', rows);
	 $('.stores_' + $('#' + portletId + 'company').val()).show();
}

function alocaMagazinPeLinii(magazin, selectedCompany) {
	var rows = [];
	var hasStore = true;
	var hasStoreLines = false;
	if(!magazin || magazin == "" || magazin == "0" || magazin == 0) {
		hasStore = false;
	}
	
	if(hasStore){
		var ml = dataTableSplit.data, msg = '', template = '';
		ml.each(function (item, i) {
		var data = item.getAttrs(['id_store_line', 'id_store', 'delete_line', 'index', 'article_code', 'description', 'quantity', 'price_no_vat_curr','id_store_1',
 	    		                 'id_store_2', 'id_store_3', 'id_store_4', 'id_store_5', 'id_store_6','id_store_7','id_store_8', 'id_store_9', 'id_department', 
 	                             'associated_acc', 'id_tip_inreg', 'id_act_mf', 'id_dpi', 'id_tip_inventar', 'unit_price', 'product_code', 'nr',
 	                             'currency', 'inv_number', 'line_id', 'price_no_vat_ron', 'vizualizare_dpi', 'cont_imob_in_curs', 'cont_imob_in_fct', 
 	                             'cont_imob_avans', 'id_cat_gestiune', 'detalii', 'inventory', 'initiative', 'new_prj']);
			hasStoreLines = true;
			if (selectedCompany == 1) {
				data.id_store_1 = magazin;
			} else if (selectedCompany == 2) {
				data.id_store_2 = magazin;
			} else if (selectedCompany == 3) {
				data.id_store_3 = magazin;
			} else if (selectedCompany == 4) {
				data.id_store_4 = magazin;
			} else if (selectedCompany == 5) {
				data.id_store_5 = magazin;
			} else if (selectedCompany == 6) {
				data.id_store_6 = magazin;
			}else if (selectedCompany == 7) {
				data.id_store_7 = magazin;
			}else if (selectedCompany == 8) {
				data.id_store_8 = magazin;
			}else if (selectedCompany == 9) {
				data.id_store_9 = magazin;
			}
			rows.push(data);
		});	
		if(!hasStoreLines){
			alert("Nu a fost alocata nici o linie. Va rugam alocati cel putin o linie.");
		} else {
			 dataTableSplit.set('recordset', rows);
		}
		$('.stores_' + selectedCompany).removeClass('hiddencol');
	} else {
		alert("Va rugam selectati magazinul.");
	}
}

function alocaRaionPeLinii(raion) {
	var rows = [];
	var hasRaion = true;
	var hasStoreLines = false;
	if(!raion || raion == "" || raion == "0" || raion == 0) {
		hasRaion = false;
	}
	
	if(hasRaion) {
		var ml = dataTableSplit.data, msg = '', template = '';
		ml.each(function (item, i) {
		var data = item.getAttrs(['id_store_line', 'id_store', 'delete_line', 'index', 'article_code', 'description', 'quantity', 'price_no_vat_curr','id_store_1',
 	    		                 'id_store_2', 'id_store_3', 'id_store_4', 'id_store_5', 'id_store_6','id_store_7','id_store_8', 'id_store_9', 'id_department', 
 	                             'associated_acc', 'id_tip_inreg', 'id_act_mf', 'id_dpi', 'id_tip_inventar', 'unit_price', 'product_code', 'nr',
 	                             'currency', 'inv_number', 'line_id', 'price_no_vat_ron', 'vizualizare_dpi', 'cont_imob_in_curs', 'cont_imob_in_fct', 
 	                             'cont_imob_avans', 'id_cat_gestiune', 'detalii', 'inventory', 'initiative', 'new_prj']);
			hasStoreLines = true;
			data.id_department = raion;
			rows.push(data);
		});	
		if(!hasStoreLines){
			alert("Nu a fost alocata nici o linie. Va rugam alocati cel putin o linie.");
		} else {
			 dataTableSplit.set('recordset', rows);
		}
		$('.stores_' + selectedCompany).removeClass('hiddencol');
	} else {
		alert("Va rugam selectati raionul.");
	}
}

function alocaGestiunePeLinii(gestiune) {
	var rows = [];
	var hasGestiune = true;
	var hasStoreLines = false;
	if(!gestiune || gestiune == "" || gestiune == "0" || gestiune == 0) {
		hasGestiune = false;
	}
	
	if(hasGestiune) {
		var ml = dataTableSplit.data, msg = '', template = '';
		ml.each(function (item, i) {
		var data = item.getAttrs(['id_store_line', 'id_store', 'delete_line', 'index', 'article_code', 'description', 'quantity', 'price_no_vat_curr','id_store_1',
 	    		                 'id_store_2', 'id_store_3', 'id_store_4', 'id_store_5', 'id_store_6','id_store_7','id_store_8', 'id_store_9', 'id_department', 
 	                             'associated_acc', 'id_tip_inreg', 'id_act_mf', 'id_dpi', 'id_tip_inventar', 'unit_price', 'product_code', 'nr',
 	                             'currency', 'inv_number', 'line_id', 'price_no_vat_ron', 'vizualizare_dpi', 'cont_imob_in_curs', 'cont_imob_in_fct', 
 	                             'cont_imob_avans', 'id_cat_gestiune', 'detalii', 'inventory', 'initiative', 'new_prj']);
			hasStoreLines = true;
			data.id_cat_gestiune = gestiune;
			rows.push(data);
		});	
		if(!hasStoreLines){
			alert("Nu a fost alocata nici o linie. Va rugam alocati cel putin o linie.");
		} else {
			 dataTableSplit.set('recordset', rows);
		}
		$('.stores_' + selectedCompany).removeClass('hiddencol');
	} else {
		alert("Va rugam selectati gestiunea.");
	}	
}

function alocaTipInregPeLinii(tipInreg) {
	var rows = [];
	var hasTipInreg = true;
	var hasStoreLines = false;
	if(!tipInreg || tipInreg == "" || tipInreg == "0" || tipInreg == 0) {
		hasTipInreg = false;
	}
	
	if(hasTipInreg) {
		var ml = dataTableSplit.data, msg = '', template = '';
		ml.each(function (item, i) {
		var data = item.getAttrs(['id_store_line', 'id_store', 'delete_line', 'index', 'article_code', 'description', 'quantity', 'price_no_vat_curr','id_store_1',
 	    		                 'id_store_2', 'id_store_3', 'id_store_4', 'id_store_5', 'id_store_6','id_store_7','id_store_8', 'id_store_9', 'id_department', 
 	                             'associated_acc', 'id_tip_inreg', 'id_act_mf', 'id_dpi', 'id_tip_inventar', 'unit_price', 'product_code', 'nr',
 	                             'currency', 'inv_number', 'line_id', 'price_no_vat_ron', 'vizualizare_dpi', 'cont_imob_in_curs', 'cont_imob_in_fct', 
 	                             'cont_imob_avans', 'id_cat_gestiune', 'detalii', 'inventory', 'initiative', 'new_prj']);
			hasStoreLines = true;
			data.id_tip_inreg = tipInreg;
			if (data.id_act_mf == '4' ){
				// tipul de actiune mf este "avans"
				data.associated_acc = data.cont_imob_avans;
			} else if (data.id_tip_inreg == '1' || data.id_tip_inreg == '3'){
				//tipul de inregistrare este "obiect de inventar in curs" sau "mijloc fix in curs"
				data.associated_acc = data.cont_imob_in_curs;
			} else { 
				//tipul de inregistrare este "obiect de inventar in functiune" sau "mijloc fix in functiune"
				data.associated_acc = data.cont_imob_in_fct;
			}
			rows.push(data);
		});	
		if(!hasStoreLines){
			alert("Nu a fost alocata nici o linie. Va rugam alocati cel putin o linie.");
		} else {
			 dataTableSplit.set('recordset', rows);
		}
		$('.stores_' + selectedCompany).removeClass('hiddencol');
	} else {
		alert("Va rugam selectati tipul de inregistrare.");
	}	
}

function alocaTipInventarPeLinii(tipInv) {
	var rows = [];
	var hastipInv = true;
	var hasStoreLines = false;
	if(!tipInv || tipInv == "" || tipInv == "0" || tipInv == 0) {
		hastipInv = false;
	}
	
	if(hastipInv) {
		var ml = dataTableSplit.data, msg = '', template = '';
		ml.each(function (item, i) {
		var data = item.getAttrs(['id_store_line', 'id_store', 'delete_line', 'index', 'article_code', 'description', 'quantity', 'price_no_vat_curr','id_store_1',
 	    		                 'id_store_2', 'id_store_3', 'id_store_4', 'id_store_5', 'id_store_6','id_store_7','id_store_8', 'id_store_9', 'id_department', 
 	                             'associated_acc', 'id_tip_inreg', 'id_act_mf', 'id_dpi', 'id_tip_inventar', 'unit_price', 'product_code', 'nr',
 	                             'currency', 'inv_number', 'line_id', 'price_no_vat_ron', 'vizualizare_dpi', 'cont_imob_in_curs', 'cont_imob_in_fct', 
 	                             'cont_imob_avans', 'id_cat_gestiune', 'detalii', 'inventory', 'initiative', 'new_prj']);
			hasStoreLines = true;
			data.id_tip_inventar = tipInv;
			rows.push(data);
		});	
		if(!hasStoreLines){
			alert("Nu a fost alocata nici o linie. Va rugam alocati cel putin o linie.");
		} else {
			 dataTableSplit.set('recordset', rows);
		}
		$('.stores_' + selectedCompany).removeClass('hiddencol');
	} else {
		alert("Va rugam selectati tipul de inventar.");
	}	
}

function calculeazaTvaPeStoreLines(portletId) {
	var ml = dataTable.data, msg = '', template = '';
	var tvaPeLinii = 0;
	//pentru fiecare linie
	ml.each(function (item, i) {
		var data = item.getAttrs(['invoice_line_id', 'delete_line', 'counter', 'line', 'inv_number', 'unit_price_ron', 'price_no_vat_ron', 'price_vat_ron', 'price_vat_curr', 'currency',  'article_code', 'product_code', 
		  		                 'description', 'um', 'quantity', 'unit_price_curr', 'price_no_vat_curr', 'vat', 'val_cu_tva', 'warranty',
		   		                'id_ias', 'id_ifrs', 'dt_ras', 'dt_ifrs', 'lot', 'clasif', 'lot_warranty', 'cont_imob_in_fct', 
		   		                'cont_imob_in_curs', 'cont_imob_avans', 'detalii']);
		var mls = dataTableSplit.data, msg = '', template = '';
		//pentru fiecare store line
		mls.each(function (item, i) {
			var dataS = item.getAttrs(['id_store_line', 'id_store', 'delete_line', 'index', 'article_code', 'description', 'quantity', 'price_no_vat_curr','id_store_1',
	 	    		                 'id_store_2', 'id_store_3', 'id_store_4', 'id_store_5', 'id_store_6','id_store_7','id_store_8', 'id_store_9', 'id_department', 
	 	                             'associated_acc', 'id_tip_inreg', 'id_act_mf', 'id_dpi', 'id_tip_inventar', 'unit_price', 'product_code', 'nr',
	 	                             'currency', 'inv_number', 'line_id', 'price_no_vat_ron', 'vizualizare_dpi', 'cont_imob_in_curs', 'cont_imob_in_fct', 
	 	                             'cont_imob_avans', 'id_cat_gestiune', 'detalii', 'inventory', 'initiative', 'new_prj']);
				if(data.line == dataS.nr) {
					tvaPeLinii = tvaPeLinii + data.vat * data.quantity * dataS.price_no_vat_curr / 100;
				}
			});	
	});
	$('#' + portletId + 'tvaPeLinii').val(parseFloat(tvaPeLinii).toFixed(4));
	var tvaFact = parseFloat($('#' + portletId + 'val_vat_ron').val().replace(',', '')).toFixed(4);
	$('#' + portletId + 'diferentaTva').val((tvaFact - parseFloat(tvaPeLinii).toFixed(4)).toFixed(4));
}

function downloadPdfFunction(cid) {
//	  $.blockUI({message: '<table cellspacing="0" cellpadding="2" border="0" class="ui-widget"><tbody><tr><td valign="middle"><img src="images/ajax-loader.gif" /></td><td valign="middle" style="padding-left: .3em; font-weight: bold;">Va rugam asteptati...</td></tr></tbody></table>'});

	  fetch("http://10.244.116.55:8181/api/downloadImageByCid?cid=" + cid, { // pass the identification id of the document
		method: "GET",
		responseType: "blob"
		}).then(function (response) {
			apelValid = false;
			if (response.status !== 200) {
				// make the promise be rejected if we didn't get a 200 response
				throw new Error("Documentul cu id-ul: " + model.data.cid + " nu a fost gasit!")
			} else {
				// go the desired response
				return response.blob();
			}
		}).then(function (myBlob) {
		   // It is necessary to create a new blob object with mime-type explicitly set
		   // otherwise only Chrome works like it should
		   var newBlob = new Blob([myBlob], { type: "application/pdf" });

		   // IE doesn't allow using a blob object directly as link href
		   // instead it is necessary to use msSaveOrOpenBlob
		   if (window.navigator && window.navigator.msSaveOrOpenBlob) {
			 window.navigator.msSaveOrOpenBlob(newBlob);
			 return;
		   }

		   // For other browsers: 
		   // Create a link pointing to the ObjectURL containing the blob.
		   var data = window.URL.createObjectURL(newBlob);
		   var link = document.createElement('a');
		   link.href = data;
		   link.download = cid + ".pdf"; 
		   document.body.appendChild(link);
		   link.click();
		   
		   setTimeout(function () {
			 // For Firefox it is necessary to delay revoking the ObjectURL
			 document.body.removeChild(link);
			 window.URL.revokeObjectURL(data);
		   }, 100);
		}).catch(function (err) {
		   // just send the user a feedback if something bad happens
		   console.log("Alfresco Indisponibil");
		   alert("Alfresco Indisponibil");
//		   $.unblockUI();
		});
//		$.unblockUI();
	}