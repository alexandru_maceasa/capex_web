package com.profluo.ec.carrefour.definitions;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;

import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;
import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.theme.ThemeDisplay;
import com.liferay.portal.util.PortalUtil;
import com.liferay.util.bridges.mvc.MVCPortlet;
import com.profluo.ecm.flows.SupplierStages;
import com.profluo.ecm.model.db.DatabaseConnectionManager;
import com.profluo.ecm.model.db.DefSupplier;

/**
 * Portlet implementation class DefSupplierController
 */
public class DefSupplierController extends MVCPortlet {
	
private final Log log = LogFactoryUtil.getLog("PROFLUO-" + DefSupplierController.class.getName());
	
    /**
     * @see MVCPortlet#MVCPortlet()
     */
    public DefSupplierController() {
        super();
        log.info("[PROFLUO] Initialize DefSupplierController portlet.");
    }
	
    @Override
    public void serveResource(ResourceRequest resourceRequest, ResourceResponse resourceResponse) throws IOException, PortletException {
    	log.info("[AJAX] DefSupplierController");
    	
    	String action = ParamUtil.getString(resourceRequest, "action");
    	
    	if (action.equals("filter") || action.equals("next") || action.equals("prev") || action.equals("last") || action.equals("first")) {
	    	System.out.println("Action "+ParamUtil.getString(resourceRequest, "action").toString());
	    	System.out.println("Start "+ParamUtil.getString(resourceRequest, "start").toString());
	    	System.out.println("Total "+ParamUtil.getString(resourceRequest, "total").toString());
	    	int start = Integer.parseInt(ParamUtil.getString(resourceRequest, "start").toString());
	    	int count = Integer.parseInt(ParamUtil.getString(resourceRequest, "count").toString());
	    	int total = Integer.parseInt(ParamUtil.getString(resourceRequest, "total").toString());
	    	String code = ParamUtil.getString(resourceRequest, "code");
	    	
	    	
	    	//resourceResponse.setProperty("test", "test_content");
	    	PrintWriter writer = resourceResponse.getWriter();
	    	
	
	    	log.info("[AJAX] start: " + count);
	    	log.info("[AJAX] count: " + start);
	    	log.info("[AJAX] total: " + total);
	    	List<HashMap<String,Object>> allSuppliers = null;
	    	
    		if (code.equals("")) {
    			log.info("Getting all!");
    			allSuppliers = DefSupplier.getAll(start, count);
    			// if total is 0 we need to refresh the count 
    			if (total == 0) {
    				total = DefSupplier.getCount();
    			}
    		} else {
    			log.info("Getting all filtered!");
    			System.out.println("start "+start);
    			System.out.println("count "+count);
    			allSuppliers = DefSupplier.getAllFiltered(start, count, code, "", 0, SupplierStages.PAGE_NOMENCLATOR);
    			// if total is 0 we need to refresh the count
    			if (total == 0) {
    				total = DefSupplier.getAllFilteredCount(start, count, code);
    			}
    		}
	    	if(allSuppliers == null){
	    		System.out.println("NULL");
	    	}
	    	JSONArray jsonSuppliers = new JSONArray();
	    	if (allSuppliers != null) {
	    		jsonSuppliers = DatabaseConnectionManager.convertListToJson(allSuppliers);
	    	}
	    	JSONObject jsonResponse = new JSONObject();
	    	
			jsonResponse.put("action", action);
			jsonResponse.put("start", start);
			jsonResponse.put("count", count);
			jsonResponse.put("total", total);
			jsonResponse.put("values", jsonSuppliers);
			writer.print(jsonResponse.toJSONString());
    	}
    	//super.serveResource(resourceRequest, resourceResponse);
    } 
 
	/**
	 * 
	 * @param request
	 * @param response
	**/    
    
	public void render(RenderRequest renderRequest, RenderResponse renderResponse) throws PortletException, IOException {
		HttpServletRequest request = PortalUtil.getHttpServletRequest(renderRequest);
    	String p_p_id = PortalUtil.getOriginalServletRequest(request).getParameter("p_p_id");
    	String path = PortalUtil.getOriginalServletRequest(request).getParameter("_" + p_p_id + "_mvcPath");

		log.info("[PROFLUO] DefSupplierController portlet - render()");
		//log.info("[PROFLUO] Context Path: " + renderRequest.getContextPath());
    	//log.info("[PROFLUO] Portlet ID	: " + p_p_id);
    	//log.info("[PROFLUO] Portlet Path: " + path);
		ThemeDisplay td  = (ThemeDisplay) renderRequest.getAttribute(WebKeys.THEME_DISPLAY);
		System.out.println(td.getURLCurrent());
    	if (path == null || path.endsWith("view.jsp")) {
    		log.info("[PROFLUO] LISTING render()");
    	}
    	
    	super.render(renderRequest, renderResponse);
	}
}

 

