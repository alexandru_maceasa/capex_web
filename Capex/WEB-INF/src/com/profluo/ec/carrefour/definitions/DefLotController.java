package com.profluo.ec.carrefour.definitions;

import java.io.IOException;
import java.io.PrintWriter;
//import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletConfig;
import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;
import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.portlet.LiferayPortletConfig;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.servlet.SessionMessages;
import com.liferay.portal.kernel.util.JavaConstants;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.util.PortalUtil;
import com.liferay.util.bridges.mvc.MVCPortlet;
import com.profluo.ecm.controller.ControllerUtils;
import com.profluo.ecm.model.db.DatabaseConnectionManager;
import com.profluo.ecm.model.db.DefLot;
import com.profluo.ecm.model.vo.DefLotVo;
import com.profluo.ecm.model.vo.DefProductVo;
import com.profluo.ecm.utils.ExcelUtil;

/**
 * Portlet implementation class Defias_idGroupeController
 */
public class DefLotController extends MVCPortlet {
	/**
	 * 
	 */
	private final Log log = LogFactoryUtil.getLog("PROFLUO-"
			+ DefLotController.class.getName());

	/**
	 * @see MVCPortlet#MVCPortlet()
	 */
	public DefLotController() {
		super();
		log.info("[PROFLUO] Initialize DefLotController portlet.");
	}

	/**
	 * 
	 * @param request
	 * @param response
	 */
	public void serveResource(ResourceRequest resourceRequest,
			ResourceResponse resourceResponse) throws IOException,
			PortletException {
		log.info("[AJAX] DefLotController");

		String action = ParamUtil.getString(resourceRequest, "action");

		if (action.equals("exportExcel")) {
			List<HashMap<String, Object>> allCategories = DefLotVo
					.getInstance();
			
			String[][] header = new String[8][];
			for (int i = 0; i < 8; i++) {
				header[i] = new String[2];
			}

			header[0][0] = "code";
			header[0][1] = "Cod lot";
			header[1][0] = "name";
			header[1][1] = "Nume lot";
			header[2][0] = "category_name";
			header[2][1] = "Categorie";
			header[3][0] = "flag_it_lot";
			header[3][1] = "Clasificare";
			header[4][0] = "warranty";
			header[4][1] = "Garantie";
			header[5][0] = "ias_dt";
			header[5][1] = "Durata IAS";
			header[6][0] = "ifrs_dt";
			header[6][1] = "Durata IFRS";
			header[7][0] = "status";
			header[7][1] = "Status";
			

			try {
				ExcelUtil.getExcel(allCategories, header, "Raport_Loturi",
						resourceResponse);
			} catch (IOException e) {
				e.printStackTrace();
			}

		} else if (action.equals("filter") || action.equals("next") || action.equals("prev") || action.equals("last") || action.equals("first")) {
			System.out.println("Action " + ParamUtil.getString(resourceRequest, "action").toString());
			System.out.println("Start " + ParamUtil.getString(resourceRequest, "start").toString());
			System.out.println("Total " + ParamUtil.getString(resourceRequest, "total").toString());
			int start = Integer.parseInt(ParamUtil.getString(resourceRequest, "start").toString());
			int count = Integer.parseInt(ParamUtil.getString(resourceRequest, "count").toString());
			int total = Integer.parseInt(ParamUtil.getString(resourceRequest, "total").toString());
			String code = ParamUtil.getString(resourceRequest, "code");
			String name = ParamUtil.getString(resourceRequest, "name");
			String category = ParamUtil.getString(resourceRequest, "category");
			int filter_categ = 0;
			if (!category.equals("")) {
				filter_categ = Integer.parseInt(category);
			}

			// resourceResponse.setProperty("test", "test_content");
			PrintWriter writer = resourceResponse.getWriter();

			log.info("[AJAX] start: " + count);
			log.info("[AJAX] count: " + start);
			log.info("[AJAX] total: " + total);
			List<HashMap<String, Object>> allLots = null;

			if (code.equals("") && name.equals("") && category.equals("")) {
				log.info("Getting all!");
				allLots = DefLot.getAll(start, count);
				// if total is 0 we need to refresh the count
				if (total == 0) {
					total = DefLot.getCount();
				}
			} else {
				log.info("Getting all filtered!");
				System.out.println("start " + start);
				System.out.println("count " + count);
				allLots = DefLot.getAllFiltered(start, count, code, name, filter_categ);
				// if total is 0 we need to refresh the count
				if (total == 0) {
					total = DefLot.getAllFilteredCount(start, count, code, name, filter_categ);
				}
			}

			if (allLots == null) {
				System.out.println("NULL");
			}
			JSONArray jsonLots = new JSONArray();
			if (allLots != null) {
				jsonLots = DatabaseConnectionManager.convertListToJson(allLots);
			}
			JSONObject jsonResponse = new JSONObject();

			jsonResponse.put("action", action);
			jsonResponse.put("start", start);
			jsonResponse.put("count", count);
			jsonResponse.put("total", total);
			jsonResponse.put("values", jsonLots);
			writer.print(jsonResponse.toJSONString());
		}
		// super.serveResource(resourceRequest, resourceResponse);
	}

	public void saveLot(ActionRequest request, ActionResponse response) {
		// remove default error message from Liferay
		PortletConfig portletConfig = (PortletConfig) request.getAttribute(JavaConstants.JAVAX_PORTLET_CONFIG);
		LiferayPortletConfig liferayPortletConfig = (LiferayPortletConfig) portletConfig;
		SessionMessages.add(request, liferayPortletConfig.getPortletId() + SessionMessages.KEY_SUFFIX_HIDE_DEFAULT_ERROR_MESSAGE);

		log.info("[PROFLUO] DefLotGroupeController portlet - save()");

		// keep parameters from request on response
		ControllerUtils.saveRequestParamsOnResponse(request, response, log);

		// get form parameters
		String category_id = request.getParameter("category_id");
		
		String name = request.getParameter("name");
		String warranty = request.getParameter("warranty");
		String flag_it = request.getParameter("flag_it");
		String status = request.getParameter("status");
		String iasDT = request.getParameter("ias_dt");
		String ifrsDT = request.getParameter("ifrs_dt");
		// read request parameters
		String id = request.getParameter("id");
		boolean hasId = false;
		if (!(id == null || id.isEmpty())){
			hasId = true;
		}
		
		String code = "";
		if(hasId) {
			code = request.getParameter("code");
		} else {
			code = DefLot.getCode(Integer.parseInt(category_id));
		}
		
		System.out.println("CODE -> " + code);
		
		try {
			if (hasId) {
				// check if all parameters are ok
				if (code != null && name != null && status != null) {
					// update entry in DB
					int noOfEntries = DefLot.updateEntry(Integer.parseInt(id),
							code, category_id, name, iasDT, ifrsDT, warranty, flag_it, status);
					// check if update was ok
					if (noOfEntries > 0) {
						SessionMessages.add(request, "update_ok");
						DefLotVo.resetAndgetInstance();
						DefProductVo.resetAndgetInstance();
						response.setRenderParameter("mvcPath", "/html/definitions/deflotcontroller/add_lot.jsp");
					} else {
						SessionErrors.add(request, "update_nok");
						response.setRenderParameter("mvcPath", "/html/definitions/deflotcontroller/add_lot.jsp");
					}
				} else {
					SessionErrors.add(request, "params_nok");
					response.setRenderParameter("mvcPath", "/html/definitions/deflotcontroller/add_lot.jsp");
				}
			} else {
				// create Lot
				// check if form parameters are ok
				if (name != null && status != null) {
					// insert entry into db
					int noOfEntries = DefLot.insertEntry(code, category_id,
							name, warranty, flag_it, status, iasDT,ifrsDT);

					// show status to user
					if (noOfEntries > 0) {
						request.setAttribute("newProductCode", code);
						SessionMessages.add(request, "insert_ok");
						SessionMessages.add(request, "insert_ok");
						DefLotVo.resetAndgetInstance();
						DefProductVo.resetAndgetInstance();
					} else {
						SessionErrors.add(request, "insert_nok");
						response.setRenderParameter("mvcPath",
								"/html/definitions/deflotcontroller/add_lot.jsp");
					}
				} else {
					SessionErrors.add(request, "params_nok");
					response.setRenderParameter("mvcPath",
							"/html/definitions/deflotcontroller/add_lot.jsp");
				}
			}
		} catch (Exception e) {
			SessionErrors.add(request, e.getClass().getName());
			// just for show
			SessionErrors.add(request, "error");

			response.setRenderParameter("mvcPath",
					"/html/definitions/deflotcontroller/add_lot.jsp");
		}
	}

	@Override
	public void render(RenderRequest renderRequest,
			RenderResponse renderResponse) throws PortletException, IOException {
		HttpServletRequest request = PortalUtil
				.getHttpServletRequest(renderRequest);
		String p_p_id = PortalUtil.getOriginalServletRequest(request)
				.getParameter("p_p_id");
		String path = PortalUtil.getOriginalServletRequest(request)
				.getParameter("_" + p_p_id + "_mvcPath");

		log.info("[PROFLUO] Defias_idGroupeController portlet - render()");
		log.info("[PROFLUO] Context Path: " + renderRequest.getContextPath());
		log.info("[PROFLUO] Portlet ID	: " + p_p_id);
		log.info("[PROFLUO] Portlet Path: " + path);

		if (path == null || path.endsWith("view.jsp")) {
			log.info("[PROFLUO] LISTING Lot render()");
		} else if (path.endsWith("add_lot.jsp")) {
			log.info("[PROFLUO] Save Lot render()");
		}

		super.render(renderRequest, renderResponse);
	}
}
