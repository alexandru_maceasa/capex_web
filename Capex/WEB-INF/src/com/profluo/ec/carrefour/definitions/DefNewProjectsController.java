package com.profluo.ec.carrefour.definitions;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletConfig;
import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;
import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.portlet.LiferayPortletConfig;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.servlet.SessionMessages;
import com.liferay.portal.kernel.util.JavaConstants;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.model.User;
import com.liferay.portal.service.UserServiceUtil;
import com.liferay.portal.util.PortalUtil;
import com.liferay.util.bridges.mvc.MVCPortlet;
import com.profluo.ecm.controller.ControllerUtils;
import com.profluo.ecm.model.db.DatabaseConnectionManager;
import com.profluo.ecm.model.db.DefNewProj;
import com.profluo.ecm.model.db.GenericDBStatements;
import com.profluo.ecm.model.vo.DefNewProjVo;
import com.profluo.ecm.utils.ExcelUtil;

/**
 * Portlet implementation class DefNewProjectsController
 */
public class DefNewProjectsController extends MVCPortlet {
	/**
	 * 
	 */
	private final Log log = LogFactoryUtil.getLog("PROFLUO-" + DefNewProjectsController.class.getName());
	
    /**
     * @see MVCPortlet#MVCPortlet()
     */
    public DefNewProjectsController() {
        super();
        log.info("[PROFLUO] Initialize DefNewProjectsController portlet.");
    }
 
	/**
	 * 
	 * @param request
	 * @param response
	 */
	public void saveNewProj(ActionRequest request, ActionResponse response) {
    	// remove default error message from Liferay
    	PortletConfig portletConfig = (PortletConfig)request.getAttribute(JavaConstants.JAVAX_PORTLET_CONFIG);
    	LiferayPortletConfig liferayPortletConfig = (LiferayPortletConfig) portletConfig;
    	SessionMessages.add(request, liferayPortletConfig.getPortletId() + SessionMessages.KEY_SUFFIX_HIDE_DEFAULT_ERROR_MESSAGE);
		
		log.info("[PROFLUO] DefNewProjectsController portlet - save()");
		
	    try {
	    	// read request parameters
	    	String id = request.getParameter("id");
    	    // get parameters from form in order to send to database
    	    String name = request.getParameter("name");
    	    String status = request.getParameter("status");
    	    String start_date = request.getParameter("start_date");
    	    String end_date = request.getParameter("end_date");
    	    if(end_date.equals("null")) {
    	    	end_date = null;
    	    }
    	    String estimateDate = request.getParameter("estimate_date");
    	    String openDate = request.getParameter("open_date");
    	    if(openDate.equals("null") || openDate.equals("")) {
    	    	openDate = estimateDate;
    	    }
    	    String store_id = request.getParameter("store_id");
    	    String company_id = request.getParameter("company_id");
    	    String owner = request.getParameter("owner");
    	    int retea = Integer.parseInt(request.getParameter("retea").toString());
    	    int lockOpenDate = Integer.parseInt(request.getParameter("lock_open_date").toString());
    	    
    	    if (store_id == null || store_id.equals("")) {
    	    	store_id = "0";
    	    }
	    	
	    	if (id != null) {
	    		// save currency
	    		// keep parameters from request on response
	    		ControllerUtils.saveRequestParamsOnResponse(request, response, log);

	    	    // check if all parameters are ok
	    	    if ( name != null && status != null && owner != null) {
	    	    	// update entry in DB
	    	    	//System.out.println(store_id+ " fffff "+company_id);
	    	    	int noOfEntries = DefNewProj.updateEntry(Integer.parseInt(id), name, status, start_date, end_date, openDate, estimateDate,
	    	    				Integer.parseInt(store_id), Integer.parseInt(company_id), owner, retea, lockOpenDate);
	    	    	// check if update was ok
	    	    	if (noOfEntries > 0) {
	    	    		SessionMessages.add(request, "update_ok");
	    	    		DefNewProjVo.resetAndgetInstance();
	    	    		response.setRenderParameter("mvcPath", "/html/definitions/defnewprojectscontroller/add_newproject.jsp");
	    	    	} else {
	    	    		SessionErrors.add(request, "update_nok");
	    	    		response.setRenderParameter("mvcPath", "/html/definitions/defnewprojectscontroller/add_newproject.jsp");
	    	    	}
	    	    } else {
	    	    	SessionErrors.add(request, "params_nok");
	    	    	response.setRenderParameter("mvcPath", "/html/definitions/defnewprojectscontroller/add_newproject.jsp");
	    	    }
	    	} else {
	    		// create currency
	    		ControllerUtils.saveRequestParamsOnResponse(request, response, log);
	    	    //check if entry already exists in the database
	    	    if(!DefNewProjVo.exists(name)){
	    	    	// check if form parameters are ok
		    	    if (name != null &&  status != null) {
		    	    	// insert entry into db
		    	    	int noOfEntries = DefNewProj.insertEntry(owner, name, status, start_date, end_date, openDate, estimateDate, Integer.parseInt(store_id), Integer.parseInt(company_id), retea, lockOpenDate);
		    	    	
		    	    	// show status to user
		    	    	if (noOfEntries > 0) {
		    	    		SessionMessages.add(request, "insert_ok");
		    	    		DefNewProjVo.resetAndgetInstance();
		    	    	} else {
		    	    		SessionErrors.add(request, "insert_nok");
		    	    		response.setRenderParameter("mvcPath", "/html/definitions/defnewprojectscontroller/add_newproject.jsp");
		    	    	}
		    	    } else {
		    	    	SessionErrors.add(request, "params_nok");
		    	    	response.setRenderParameter("mvcPath", "/html/definitions/defnewprojectscontroller/add_newproject.jsp");
		    	    }
		    	}else{
		    		SessionErrors.add(request, "entry_exists");
	    	    	response.setRenderParameter("mvcPath", "/html/definitions/defnewprojectscontroller/add_newproject.jsp");
		    	}
	    	}
	    } catch (Exception e) {
	    	e.printStackTrace();
	        SessionErrors.add(request, e.getClass().getName());
	        // just for show
	        SessionErrors.add(request, "error");

	        response.setRenderParameter("mvcPath", "/html/definitions/defnewprojectscontroller/add_newproject.jsp");
	    }
	}
	
	@Override
	public void render(RenderRequest renderRequest, RenderResponse renderResponse) throws PortletException, IOException {
		HttpServletRequest request = PortalUtil.getHttpServletRequest(renderRequest);
    	String p_p_id = PortalUtil.getOriginalServletRequest(request).getParameter("p_p_id");
    	String path = PortalUtil.getOriginalServletRequest(request).getParameter("_" + p_p_id + "_mvcPath");

		log.info("[PROFLUO] DefNewProjectsController portlet - render()");
		log.info("[PROFLUO] Context Path: " + renderRequest.getContextPath());
    	log.info("[PROFLUO] Portlet ID	: " + p_p_id);
    	log.info("[PROFLUO] Portlet Path: " + path);
    	
    	if (path == null || path.endsWith("view.jsp")) {
    		log.info("[PROFLUO] LISTING render()");
    		
    	} else if (path.endsWith("add_newproject.jsp")) {
    		log.info("[PROFLUO] Save New Project render()");
    		
    	}
		
	    super.render(renderRequest, renderResponse);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public void serveResource(ResourceRequest resourceRequest, ResourceResponse resourceResponse) throws IOException, PortletException {
		log.info("[AJAX] DefNewProjectsController");

		String action = ParamUtil.getString(resourceRequest, "action");
		User user = null;
		try {
			user = UserServiceUtil.getUserById(Long.parseLong(resourceRequest.getRemoteUser()));
		} catch (NumberFormatException | PortalException | SystemException e1) {
			e1.printStackTrace();
		}
		
		if (action.equals("exportExcel")) {
			List<HashMap<String, Object>> allCategories = DefNewProjVo.getInstance();
			
			String[][] header = new String[9][];
			for (int i = 0; i < 9; i++) {
				header[i] = new String[2];
			}
			
			header[0][0] = "id";
			header[0][1] = "Cod produs";
			header[1][0] = "name";
			header[1][1] = "Nume produs";
			header[2][0] = "store_id";
			header[2][1] = "Cod magazin";
			header[3][0] = "company_id";
			header[3][1] = "Cod companie";
			header[4][0] = "company_name";
			header[4][1] = "Nume companie";
			header[5][0] = "start_date";
			header[5][1] = "Data inceput";
			header[6][0] = "end_date";
			header[6][1] = "Data sfarsit";
			header[7][0] = "owner";
			header[7][1] = "Responsabil";
			header[8][0] = "status";
			header[8][1] = "Status";
			
			try {
				ExcelUtil.getExcel(allCategories, header, "Raport_Proiecte_Noi",
						resourceResponse);
			} catch (IOException e) {
				e.printStackTrace();
			}
		
		} else if (action.equals("filter") || action.equals("next") || action.equals("prev") || action.equals("last") || action.equals("first")) {
			PrintWriter writer = resourceResponse.getWriter();
			int start = Integer.parseInt(ParamUtil.getString(resourceRequest, "start").toString());
			int count = Integer.parseInt(ParamUtil.getString(resourceRequest, "count").toString());
			int total = Integer.parseInt(ParamUtil.getString(resourceRequest, "total").toString());
			String name = ParamUtil.getString(resourceRequest, "new_prj_name").toString();
			String storeCode = ParamUtil.getString(resourceRequest, "store_code").toString();
			String companyName = ParamUtil.getString(resourceRequest, "company_name").toString();
			String status = ParamUtil.getString(resourceRequest, "status").toString();

			List<HashMap<String, Object>> allNewPrj = DefNewProj.getAllFiltered(start, count, name, storeCode, companyName, status);
			total = DefNewProj.getAllFilteredCount(name, storeCode, companyName, status);

			JSONArray jsonLines = new JSONArray();
			if (allNewPrj != null) {
				jsonLines = DatabaseConnectionManager.convertListToJson(allNewPrj);
			}
			JSONObject jsonResponse = new JSONObject();

			jsonResponse.put("total", total);
			jsonResponse.put("values", jsonLines);
			writer.print(jsonResponse.toJSONString());
		} else if(action.equals("update_estimate_date")) {
			PrintWriter writer = resourceResponse.getWriter();
			int storetype = Integer.parseInt(ParamUtil.getString(resourceRequest, "storetype"));
			int nrLuni = Integer.parseInt(ParamUtil.getString(resourceRequest, "nr_luni"));
			DefNewProj.updateEstimateDate(storetype, nrLuni);
			String [][] arrFields = new String [4][2];
			arrFields[0][0] = "storetype";
			arrFields[0][1] = String.valueOf(storetype);
			arrFields[1][0] = "nr_luni";
			arrFields[1][1] = String.valueOf(nrLuni);
			arrFields[2][0] = "activ";
			arrFields[2][1] = "1";
			arrFields[3][0] = "user_id";
			arrFields[3][1] = String.valueOf(user.getUserId());
			//set inactiv status for old entries
			DefNewProj.setActivStatusForOldEntries(storetype);
			//add new entry
			GenericDBStatements.insertEntry(arrFields, "def_new_prj_periods", new ArrayList<String>());
			writer.print("success");
		} else if(action.equals("get_nr_of_months")) {
			PrintWriter writer = resourceResponse.getWriter();
			int storetype = Integer.parseInt(ParamUtil.getString(resourceRequest, "tip_retea"));
			int nrOfMonths = DefNewProj.getLastPeriod(storetype);
			writer.print(nrOfMonths);
		} else if (action.equals("get_estimate_date")) {
			PrintWriter writer = resourceResponse.getWriter();
			int storetype = Integer.parseInt(ParamUtil.getString(resourceRequest, "tip_retea"));
			String startDate = ParamUtil.getString(resourceRequest, "start_date").toString();
			int nrOfMonths = DefNewProj.getLastPeriod(storetype);
			String estimateDate = DefNewProj.getEstimateDate(nrOfMonths, startDate);
			writer.print(estimateDate);
		}
		
	}


}
