package com.profluo.ec.carrefour.definitions;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.List;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletConfig;
import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;
import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.portlet.LiferayPortletConfig;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.servlet.SessionMessages;
import com.liferay.portal.kernel.util.JavaConstants;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.util.PortalUtil;
import com.liferay.util.bridges.mvc.MVCPortlet;
import com.profluo.ecm.controller.ControllerUtils;
import com.profluo.ecm.model.db.DatabaseConnectionManager;
import com.profluo.ecm.model.db.DefIAS;
import com.profluo.ecm.model.vo.DefIASVo;
import com.profluo.ecm.model.vo.DefLotVo;
import com.profluo.ecm.model.vo.DefProductVo;
import com.profluo.ecm.utils.ExcelUtil;

/**
 * Portlet implementation class DefIASGroupeController
 */
public class DefIASGroupeController extends MVCPortlet {
	/**
	 * 
	 */
	private final Log log = LogFactoryUtil.getLog("PROFLUO-"
			+ DefIASGroupeController.class.getName());

	/**
	 * @see MVCPortlet#MVCPortlet()
	 */
	public DefIASGroupeController() {
		super();
		log.info("[PROFLUO] Initialize DefIASGroupeController portlet.");
	}

	/**
     * 
     */
	public void serveResource(ResourceRequest resourceRequest,
			ResourceResponse resourceResponse) throws IOException,
			PortletException {
		log.info("[AJAX] DefIASGroupeController");

		String action = ParamUtil.getString(resourceRequest, "action");
		log.info("[AJAX] action: " + action);

		if (action.equals("exportExcel")) {
			List<HashMap<String, Object>> allCategories = DefIASVo
					.getInstance();
			String[][] header = new String[10][];
			for (int i = 0; i < 10; i++) {
				header[i] = new String[2];
			}
			
			header[0][0] = "grupa_sintetica";
			header[0][1] = "Grupa sintetica";
			header[1][0] = "grupa_analitica";
			header[1][1] = "Grupa analitica";
			header[2][0] = "name";
			header[2][1] = "Denumire grupa";
			header[3][0] = "durata_minima";
			header[3][1] = "Durata minima";
			header[4][0] = "durata_maxima";
			header[4][1] = "Durata maxima";
			header[5][0] = "durata_implicita";
			header[5][1] = "Durata implicita";
			header[6][0] = "status";
			header[6][1] = "Status";
			header[7][0] = "cont_imob_in_fct";
			header[7][1] = "Cont imob. in functiune";
			header[8][0] = "cont_imob_in_curs";
			header[8][1] = "Cont imob. in curs";
			header[9][0] = "cont_imob_avans";
			header[9][1] = "Cont avans imob";

			try {
				ExcelUtil.getExcel(allCategories, header, "Raport_IAS",
						resourceResponse);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		if (action.equals("filter") || action.equals("next")
				|| action.equals("prev") || action.equals("last")
				|| action.equals("first")) {
			PrintWriter writer = resourceResponse.getWriter();
			int start = Integer.parseInt(ParamUtil.getString(resourceRequest,
					"start").toString());
			int count = Integer.parseInt(ParamUtil.getString(resourceRequest,
					"count").toString());
			int total = Integer.parseInt(ParamUtil.getString(resourceRequest,
					"total").toString());
			String grupa_analitica = ParamUtil.getString(resourceRequest,
					"grupa_analitica");
			String grupa_sintetica = ParamUtil.getString(resourceRequest,
					"grupa_sintetica");

			log.info("[AJAX] start: " + count);
			log.info("[AJAX] count: " + start);
			log.info("[AJAX] total: " + total);

			List<HashMap<String, Object>> allIas = null;

			log.info("Getting all filtered!");
			allIas = DefIAS.getAllFiltered(start, count, grupa_sintetica,
					grupa_analitica);
			// if total is 0 we need to refresh the count
			if (total == 0) {
				total = DefIAS.getAllFilteredCount(grupa_sintetica,
						grupa_analitica);
			}

			JSONArray jsonIAS = new JSONArray();
			if (allIas != null) {
				jsonIAS = DatabaseConnectionManager.convertListToJson(allIas);
			}
			JSONObject jsonResponse = new JSONObject();

			jsonResponse.put("action", action);
			jsonResponse.put("start", start);
			jsonResponse.put("count", count);
			jsonResponse.put("total", total);
			jsonResponse.put("values", jsonIAS);
			writer.print(jsonResponse.toJSONString());
		}
	}

	/**
	 * 
	 * @param request
	 * @param response
	 **/
	public void saveIAS(ActionRequest request, ActionResponse response) {
		// remove default error message from Liferay
		PortletConfig portletConfig = (PortletConfig) request
				.getAttribute(JavaConstants.JAVAX_PORTLET_CONFIG);
		LiferayPortletConfig liferayPortletConfig = (LiferayPortletConfig) portletConfig;
		SessionMessages.add(request, liferayPortletConfig.getPortletId()
				+ SessionMessages.KEY_SUFFIX_HIDE_DEFAULT_ERROR_MESSAGE);

		log.info("[PROFLUO] DefIASGroupeController portlet - save()");

		try {
			// read request parameters
			String id = request.getParameter("id");
			// get parameters from form in order to send to database
			String name = request.getParameter("name");
			String grupa_sintetica = request.getParameter("grupa_sintetica");
			String grupa_analitica = request.getParameter("grupa_analitica");
			String durata_minima = request.getParameter("durata_minima");
			String durata_maxima = request.getParameter("durata_maxima");
			String durata_implicita = request.getParameter("durata_implicita");
			String status = request.getParameter("status");
			// in case the accounts are empty from the request or are not
			// integer values do not save them to the DB.
			int cont_imob_in_fct = 0;
			int cont_imob_in_curs = 0;
			int cont_imob_avans = 0;
			try {
				cont_imob_in_fct = Integer.parseInt(request.getParameter(
						"cont_imob_in_fct").toString());
			} catch (Exception e) {
			}
			try {
				cont_imob_in_curs = Integer.parseInt(request.getParameter(
						"cont_imob_in_curs").toString());
			} catch (Exception e) {
			}
			try {
				cont_imob_avans = Integer.parseInt(request.getParameter(
						"cont_imob_avans").toString());
			} catch (Exception e) {
			}

			if (id != null) {
				// save IAS
				// keep parameters from request on response
				ControllerUtils.saveRequestParamsOnResponse(request, response,
						log);

				// check if all parameters are ok
				if (name != null && status != null) {
					// update entry in DB
					int noOfEntries = DefIAS.updateEntry(Integer.parseInt(id),
							name, grupa_sintetica, grupa_analitica,
							Integer.parseInt(durata_minima),
							Integer.parseInt(durata_maxima),
							Integer.parseInt(durata_implicita), status,
							cont_imob_in_fct, cont_imob_in_curs,
							cont_imob_avans);
					// check if update was ok
					if (noOfEntries > 0) {
						SessionMessages.add(request, "update_ok");
						DefIASVo.resetAndgetInstance();
						DefLotVo.resetAndgetInstance();
						DefProductVo.resetAndgetInstance();
						response.setRenderParameter("mvcPath",
								"/html/definitions/defiasgroupecontroller/add_IAS.jsp");
					} else {
						// keep parameters from request on response
						ControllerUtils.saveRequestParamsOnResponse(request,
								response, log);

						SessionErrors.add(request, "update_nok");
						response.setRenderParameter("mvcPath",
								"/html/definitions/defiasgroupecontroller/add_IAS.jsp");
					}
				} else {
					// keep parameters from request on response
					ControllerUtils.saveRequestParamsOnResponse(request,
							response, log);

					SessionErrors.add(request, "params_nok");
					response.setRenderParameter("mvcPath",
							"/html/definitions/defiasgroupecontroller/add_IAS.jsp");
				}
			} else {
				// check if form parameters are ok
				if (name != null && status != null) {
					// insert entry into db
					System.out.println(name + " -> " + grupa_analitica + " -> " + grupa_sintetica);
					if (!DefIASVo.existsCombo(grupa_analitica, grupa_sintetica)) {
						int noOfEntries = DefIAS.insertEntry(name,
								grupa_sintetica, grupa_analitica,
								Integer.parseInt(durata_minima),
								Integer.parseInt(durata_maxima),
								Integer.parseInt(durata_implicita), status,
								cont_imob_in_fct, cont_imob_in_curs,
								cont_imob_avans);

						// show status to user
						if (noOfEntries > 0) {
							SessionMessages.add(request, "insert_ok");
							DefIASVo.resetAndgetInstance();
							DefLotVo.resetAndgetInstance();
							DefProductVo.resetAndgetInstance();
							response.setRenderParameter("mvcPath",
									"/html/definitions/defiasgroupecontroller/view.jsp");
						} else {
							// keep parameters from request on response
							ControllerUtils.saveRequestParamsOnResponse(
									request, response, log);

							SessionErrors.add(request, "insert_nok");
							response.setRenderParameter("mvcPath",
									"/html/definitions/defiasgroupecontroller/add_IAS.jsp");
						}
					} else {
						System.out.println("INTREA PE ELSEEEEEEEEEEEEEEEE");
						// keep parameters from request on response
						ControllerUtils.saveRequestParamsOnResponse(request,
								response, log);

						SessionErrors.add(request, "entry_exists");
						response.setRenderParameter("mvcPath",
								"/html/definitions/defiasgroupecontroller/add_IAS.jsp");
					}
				} else {
					// keep parameters from request on response
					ControllerUtils.saveRequestParamsOnResponse(request,
							response, log);

					SessionErrors.add(request, "params_nok");
					response.setRenderParameter("mvcPath",
							"/html/definitions/defiasgroupecontroller/add_IAS.jsp");
				}
			}
		} catch (Exception e) {
			SessionErrors.add(request, e.getClass().getName());
			// just for show
			SessionErrors.add(request, "error");

			response.setRenderParameter("mvcPath",
					"/html/definitions/defiasgroupecontroller/add_IAS.jsp");
		}
	}

	@Override
	/**
	 * 
	 */
	public void render(RenderRequest renderRequest,
			RenderResponse renderResponse) throws PortletException, IOException {
		HttpServletRequest request = PortalUtil
				.getHttpServletRequest(renderRequest);
		String p_p_id = PortalUtil.getOriginalServletRequest(request)
				.getParameter("p_p_id");
		String path = PortalUtil.getOriginalServletRequest(request)
				.getParameter("_" + p_p_id + "_mvcPath");

		log.info("[PROFLUO] DefIASGroupeController portlet - render()");
		log.info("[PROFLUO] Context Path: " + renderRequest.getContextPath());
		log.info("[PROFLUO] Portlet ID	: " + p_p_id);
		log.info("[PROFLUO] Portlet Path: " + path);

		if (path == null || path.endsWith("view.jsp")) {
			log.info("[PROFLUO] LISTING render()");

		} else if (path.endsWith("add_IAS.jsp")) {
			log.info("[PROFLUO] Save IAS render()");

		}

		super.render(renderRequest, renderResponse);
	}

}
