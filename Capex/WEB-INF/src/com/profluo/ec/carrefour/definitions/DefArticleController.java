package com.profluo.ec.carrefour.definitions;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.List;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletConfig;
import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;
import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.portlet.LiferayPortletConfig;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.servlet.SessionMessages;
import com.liferay.portal.kernel.util.JavaConstants;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.util.PortalUtil;
import com.liferay.util.bridges.mvc.MVCPortlet;
import com.profluo.ecm.controller.ControllerUtils;
import com.profluo.ecm.model.db.DatabaseConnectionManager;
import com.profluo.ecm.model.db.DefArticle;


/**
 * Portlet implementation class DefIASGroupeController
 */
public class DefArticleController extends MVCPortlet {
	/**
	 * 
	 */
	private final Log log = LogFactoryUtil.getLog("PROFLUO-" + DefArticleController.class.getName());
	
    /**
     * @see MVCPortlet#MVCPortlet()
     */
    public DefArticleController() {
        super();
        log.info("[PROFLUO] Initialize DefArticleController portlet.");
    }
 
	/**
	 * 
	 * @param request
	 * @param response
	 */
    
    public void serveResource(ResourceRequest resourceRequest, ResourceResponse resourceResponse) throws IOException, PortletException {
    	log.info("[AJAX] DefProductController");
    	log.info("[AJAX] action: " + ParamUtil.getString(resourceRequest, "action"));
    	log.info("[AJAX] start: " + ParamUtil.getString(resourceRequest, "start"));
    	log.info("[AJAX] count: " + ParamUtil.getString(resourceRequest, "count"));
    	
    	String action = ParamUtil.getString(resourceRequest, "action");
    	int start = Integer.parseInt(ParamUtil.getString(resourceRequest, "start").toString());
    	int count = Integer.parseInt(ParamUtil.getString(resourceRequest, "count").toString());
    	
    	resourceResponse.setProperty("test", "test_content");
    	PrintWriter writer = resourceResponse.getWriter();
    	
    	List<HashMap<String,Object>> allArticles = null;
    	
    	allArticles = DefArticle.getAll(start, count);
    	
    	JSONArray jsonStores = new JSONArray();
    	if (allArticles != null) {
    		jsonStores = DatabaseConnectionManager.convertListToJson(allArticles);
    	}
    	JSONObject jsonResponse = new JSONObject();
    	
		jsonResponse.put("action", action);
		jsonResponse.put("start", start);
		jsonResponse.put("count", count);
		jsonResponse.put("values", jsonStores);
		writer.print(jsonResponse.toJSONString());

    	super.serveResource(resourceRequest, resourceResponse);
    } 
 
	/**
	 * 
	 * @param request
	 * @param response
	**/    
    
	public void saveArticle(ActionRequest request, ActionResponse response) {
    	// remove default error message from Liferay
    	PortletConfig portletConfig = (PortletConfig)request.getAttribute(JavaConstants.JAVAX_PORTLET_CONFIG);
    	LiferayPortletConfig liferayPortletConfig = (LiferayPortletConfig) portletConfig;
    	SessionMessages.add(request, liferayPortletConfig.getPortletId() + SessionMessages.KEY_SUFFIX_HIDE_DEFAULT_ERROR_MESSAGE);
		
		log.info("[PROFLUO] DefArticleController portlet - save()");
		
	    try {
	    	// read request parameters
	    	String id = request.getParameter("id");
	    	
	    	if (id != null) {
	    		// save article
	    		// keep parameters from request on response
	    		ControllerUtils.saveRequestParamsOnResponse(request, response, log);
	    	    // get parameters from form in order to send to database
	    	  
	    	    String name  =request.getParameter("name");
	    	    String prod_id = request.getParameter("produs_id");
	    	    String lot_id = request.getParameter("lot_id");
	    	    String cat_id = request.getParameter("cat_id");
	    	    String flag_it = request.getParameter("flag_it");
	    	    String status = request.getParameter("status");
	    	    String ias_id = request.getParameter("ias_id");
	    	    String ifrs_id = request.getParameter("ifrs_id");
	    	    
	    	    // check if all parameters are ok
	    	    if ( name != null && prod_id!=null && lot_id!=null && cat_id!=null && flag_it!=null && status != null && ias_id!=null && ifrs_id!=null) {
	    	    	// update entry in DB
	    	    	int noOfEntries = DefArticle.updateEntry(Integer.parseInt(id),name,prod_id, lot_id, cat_id,flag_it,status,Integer.parseInt(ias_id),Integer.parseInt(ifrs_id));
	    	    	// check if update was ok
	    	    	if (noOfEntries > 0) {
	    	    		SessionMessages.add(request, "update_ok");
	    	    		response.setRenderParameter("mvcPath", "/html/definitions/defarticlecontroller/add_article.jsp");
	    	    	} else {
	    	    		SessionErrors.add(request, "update_nok");
	    	    		response.setRenderParameter("mvcPath", "/html/definitions/defarticlecontroller/add_article.jsp");
	    	    	}
	    	    } else {
	    	    	SessionErrors.add(request, "params_nok");
	    	    	response.setRenderParameter("mvcPath", "/html/definitions/defarticlecontroller/add_article.jsp");
	    	    }
	    	} else {
	    		// create article
	    		ControllerUtils.saveRequestParamsOnResponse(request, response, log);
	    		
	    		// get form parameters 
	    		
	    	    String name=request.getParameter("name");
	    	    String prod_id=request.getParameter("produs_id");
	    	    String lot_id=request.getParameter("lot_id");
	    	    String cat_id=request.getParameter("cat_id");
	    	    String flag_it= request.getParameter("flag_it");
	    	    String status= request.getParameter("status");
	    	    String ias_id= request.getParameter("ias_id");
	    	    String ifrs_id = request.getParameter("ifrs_id");
	    	    
	    	    //check if entry already exists in the database
	    	    
	    	    // check if form parameters are ok
	    	    if (name != null && prod_id!=null && lot_id!=null && cat_id!=null && flag_it!=null && status != null && ias_id!=null && ifrs_id!=null) {
	    	    	// insert entry into db
	    	    	int noOfEntries = DefArticle.insertEntry(name,prod_id,lot_id,cat_id,flag_it,status,Integer.parseInt(ias_id),Integer.parseInt(ifrs_id));
	    	    	
	    	    	// show status to user
	    	    	if (noOfEntries > 0) {
	    	    		SessionMessages.add(request, "insert_ok");
	    	    		response.setRenderParameter("mvcPath", "/html/definitions/defarticlecontroller/view.jsp");
	    	    	} else {
	    	    		SessionErrors.add(request, "insert_nok");
	    	    		response.setRenderParameter("mvcPath", "/html/definitions/defarticlecontroller/add_article.jsp");
	    	    	}
	    	    } else {
	    	    	SessionErrors.add(request, "params_nok");
	    	    	response.setRenderParameter("mvcPath", "/html/definitions/defarticlecontroller/add_article.jsp");
	    	    }
	    	    
	    	}
	    } catch (Exception e) {
	        SessionErrors.add(request, e.getClass().getName());
	        // just for show
	        SessionErrors.add(request, "error");

	        response.setRenderParameter("mvcPath", "/html/definitions/defarticlecontroller/add_article.jsp");
	    }
	}
	
	@Override
	public void render(RenderRequest renderRequest, RenderResponse renderResponse) throws PortletException, IOException {
		HttpServletRequest request = PortalUtil.getHttpServletRequest(renderRequest);
    	String p_p_id = PortalUtil.getOriginalServletRequest(request).getParameter("p_p_id");
    	String path = PortalUtil.getOriginalServletRequest(request).getParameter("_" + p_p_id + "_mvcPath");

		log.info("[PROFLUO] DefArticleController portlet - render()");
		log.info("[PROFLUO] Context Path: " + renderRequest.getContextPath());
    	log.info("[PROFLUO] Portlet ID	: " + p_p_id);
    	log.info("[PROFLUO] Portlet Path: " + path);
    	
    	if (path == null || path.endsWith("view.jsp")) {
    		
    	} else if (path.endsWith("add_article.jsp")) {
    		log.info("[PROFLUO] Save Article render()");
    		
    	}
		
	    super.render(renderRequest, renderResponse);
	}


}
