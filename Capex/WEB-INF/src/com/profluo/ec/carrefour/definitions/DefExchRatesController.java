package com.profluo.ec.carrefour.definitions;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.List;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletConfig;
import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;
import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.portlet.LiferayPortletConfig;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.servlet.SessionMessages;
import com.liferay.portal.kernel.util.JavaConstants;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.util.PortalUtil;
import com.liferay.util.bridges.mvc.MVCPortlet;
import com.profluo.ecm.controller.ControllerUtils;
import com.profluo.ecm.model.db.DatabaseConnectionManager;
import com.profluo.ecm.model.db.DefExchRates;

/**
 * Portlet implementation class DefExchRatesController
 */
public class DefExchRatesController extends MVCPortlet {
	
	
private final Log log = LogFactoryUtil.getLog("PROFLUO-" + DefExchRatesController.class.getName());

    /**
     * @see MVCPortlet#MVCPortlet()
     */
    public DefExchRatesController() {
        super();
        log.info("[PROFLUO] Initialize DefExchRatesController portlet.");
    }

    /**
     * 
     * @param request
     * @param response
     */
    public void saveExchRate(ActionRequest request,ActionResponse response){
    	// remove default error message from Liferay
    	PortletConfig portletConfig = (PortletConfig)request.getAttribute(JavaConstants.JAVAX_PORTLET_CONFIG);
    	LiferayPortletConfig liferayPortletConfig = (LiferayPortletConfig) portletConfig;
    	SessionMessages.add(request, liferayPortletConfig.getPortletId() + SessionMessages.KEY_SUFFIX_HIDE_DEFAULT_ERROR_MESSAGE);
    
    	log.info("[PROFLUO] DefExchRatesController Portlet - saveExchRate()");
    	
    	try {
    		String id 			= request.getParameter("id");
    		String ref 			= request.getParameter("ref");
    		String value 		= request.getParameter("value");
    		String date_of_rate = request.getParameter("date_of_rate");
    		String type 		= request.getParameter("type");
    	    
    		// keep parameters from request on response
    		ControllerUtils.saveRequestParamsOnResponse(request, response, log);
    		   
	    	if (id != null) {
	    		// save rate
	    	    // check if all parameters are ok
	    	    if (ref !=null && value != null && date_of_rate != null && type != null) {
	    	    	// update entry in DB
	    	    	int noOfEntries = DefExchRates.updateEntry(Integer.parseInt(id), ref, value, date_of_rate, type);
	    	    	// check if update was ok
	    	    	if (noOfEntries > 0) {
	    	    		SessionMessages.add(request, "update_ok");
	    	    		response.setRenderParameter("mvcPath", "/html/defexchratescontroller/add_exch_rate.jsp");
	    	    	} else {
	    	    		SessionErrors.add(request, "update_nok");
	    	    		response.setRenderParameter("mvcPath", "/html/defexchratescontroller/add_exch_rate.jsp");
	    	    	}
	    	    } else {
	    	    	SessionErrors.add(request, "params_nok");
	    	    	response.setRenderParameter("mvcPath", "/html/defexchratescontroller/add_exch_rate.jsp");
	    	    }
	    	} else {
	    		//check if entry already exists in the database
	    	    // check if form parameters are ok
	    		if (ref !=null && value != null && date_of_rate != null && type != null) {
	    	    	// insert entry into db
	    	    	int noOfEntries = DefExchRates.insertEntry(ref, value, date_of_rate, type);
	    	    	
	    	    	// show status to user
	    	    	if (noOfEntries > 0) {
	    	    		SessionMessages.add(request, "insert_ok");
	    	    	} else {
	    	    		SessionErrors.add(request, "insert_nok");
	    	    		response.setRenderParameter("mvcPath", "/html/defexchratescontroller/add_exch_rate.jsp");
	    	    	}
	    	    } else {
	    	    	SessionErrors.add(request, "params_nok");
	    	    	response.setRenderParameter("mvcPath", "/html/defexchratescontroller/add_exch_rate.jsp");
	    	    }
	    	}
	    } catch (Exception e) {
	        SessionErrors.add(request, e.getClass().getName());
	        // just for show
	        SessionErrors.add(request, "error");

	        response.setRenderParameter("mvcPath", "/html/defexchratescontroller/add_exch_rate.jsp");
	    }
	}

    @Override
    /**
     * 
     * @param request
     * @param response
     */
    public void serveResource(ResourceRequest resourceRequest, ResourceResponse resourceResponse) throws IOException, PortletException {
    	log.info("[AJAX] DefExchRatesController");
    	
    	String action = ParamUtil.getString(resourceRequest, "action");
    	log.info("[AJAX] action: " + action);
    	PrintWriter writer = resourceResponse.getWriter();

    	if (action.equals("filter") || action.equals("next") || action.equals("prev") || action.equals("last") || action.equals("first")) {
    	 	int start = Integer.parseInt(ParamUtil.getString(resourceRequest, "start").toString());
	    	int count = Integer.parseInt(ParamUtil.getString(resourceRequest, "count").toString());
	    	int total = Integer.parseInt(ParamUtil.getString(resourceRequest, "total").toString());
	    	String date = ParamUtil.getString(resourceRequest, "data_rata");
	    	String moneda = ParamUtil.getString(resourceRequest, "moneda");

	    	log.info("[AJAX] start: " + count);
	    	log.info("[AJAX] count: " + start);
	    	log.info("[AJAX] total: " + total);
	    		    	
	    	List<HashMap<String,Object>> allRates = null;
			log.info("Getting all!");
			// allRates = DefExchRates.getAll(start, count);
			allRates = DefExchRates.getAllFiltered(start, count, date, moneda);
			// if total is 0 we need to refresh the count 
			if (total == 0) {
				total = DefExchRates.getAllFilteredCount(start, count, date, moneda);
				//total = DefExchRates.getCount();
			}
	    	JSONArray jsonRates = new JSONArray();
	    	if (allRates != null) {
	    		jsonRates = DatabaseConnectionManager.convertListToJson(allRates);
	    	}
	    	JSONObject jsonResponse = new JSONObject();

	    	jsonResponse.put("action", action);
	    	jsonResponse.put("start", start);
			jsonResponse.put("count", count);
			jsonResponse.put("total", total);
			jsonResponse.put("values", jsonRates);
			writer.print(jsonResponse.toJSONString());
    	}
    } 
	    
    /**
     * 
     * @param request
     * @param response
     */
	public void render(RenderRequest renderRequest, RenderResponse renderResponse) throws PortletException, IOException {
		HttpServletRequest request = PortalUtil.getHttpServletRequest(renderRequest);
    	String p_p_id = PortalUtil.getOriginalServletRequest(request).getParameter("p_p_id");
    	String path = PortalUtil.getOriginalServletRequest(request).getParameter("_" + p_p_id + "_mvcPath");

		log.info("[PROFLUO] DefExchRatesController portlet - render()");
		log.info("[PROFLUO] Context Path: " + renderRequest.getContextPath());
    	log.info("[PROFLUO] Portlet ID	: " + p_p_id);
    	log.info("[PROFLUO] Portlet Path: " + path);
    	
    	if (path == null || path.endsWith("view.jsp")) {
    		log.info("[PROFLUO] LISTING render()");
    	}
    	
    	super.render(renderRequest, renderResponse);
	}
}