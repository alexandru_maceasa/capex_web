package com.profluo.ec.carrefour.definitions;

import java.io.IOException;

import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.servlet.http.HttpServletRequest;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.model.User;
import com.liferay.portal.service.UserServiceUtil;
import com.liferay.portal.util.PortalUtil;
import com.liferay.util.bridges.mvc.MVCPortlet;
import com.profluo.ec.carrefour.portlet.Invoices;

/**
 * Portlet implementation class AprobareFactura
 */
public class AprobareFactura extends MVCPortlet {
	/**
	 * 
	 */
	private final Log log = LogFactoryUtil.getLog("PROFLUO-" + Invoices.class.getName());
	
	/**
	 * 
	 */
    @Override
    public void init() throws PortletException {
    	log.info("[PROFLUO] Initialize Invoices portlet.");
    	super.init();
    }
    
    
    
    @Override
	public void render(RenderRequest renderRequest, RenderResponse renderResponse) throws PortletException, IOException {
		HttpServletRequest request = PortalUtil.getHttpServletRequest(renderRequest);
    	String p_p_id = PortalUtil.getOriginalServletRequest(request).getParameter("p_p_id");
    	String path = PortalUtil.getOriginalServletRequest(request).getParameter("_" + p_p_id + "_mvcPath");
    	
		log.info("[PROFLUO] Aprobare Factura portlet - render()");
		log.info("[PROFLUO] Context Path: " + renderRequest.getContextPath());
    	log.info("[PROFLUO] Portlet ID	: " + p_p_id);
    	log.info("[PROFLUO] Portlet Path: " + path);
    	
    	try {
	    	User user = UserServiceUtil.getUserById(Long.parseLong(renderRequest.getRemoteUser()));
	    	int id_user 			= (int) user.getUserId();
	    	int groupId 			= (int) user.getGroupId();
	    	
	    	log.info("[PROFLUO] User id " + id_user);
	    	log.info("[PROFLUO] Group id " + groupId);
    	} catch (Exception e) {}
    	
    	if (path == null || path.endsWith("view.jsp")) {
    		log.info("[PROFLUO] LISTING render()");
    		
    	}
		
	    super.render(renderRequest, renderResponse);
	}
	
	

}
