package com.profluo.ec.carrefour.definitions;

import java.io.IOException;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletConfig;
import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.servlet.http.HttpServletRequest;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.portlet.LiferayPortletConfig;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.servlet.SessionMessages;
import com.liferay.portal.kernel.util.JavaConstants;
import com.liferay.portal.util.PortalUtil;
import com.liferay.util.bridges.mvc.MVCPortlet;
import com.profluo.ecm.controller.ControllerUtils;
import com.profluo.ecm.model.db.InventoryNoRule;


/**
 * Portlet implementation class InventoryNoRule
 */
	public class InventoryNoRulesController extends MVCPortlet {
	
	private final Log log = LogFactoryUtil.getLog( "PROFLUO " + InventoryNoRulesController.class.getName());
 
	public InventoryNoRulesController() {
	super();
	log.info("[PROFLUO] Initialize InventoryNoRule() Portlet");
	
	
}
	@Override
	public void render(RenderRequest renderRequest, RenderResponse renderResponse) throws PortletException, IOException {
		HttpServletRequest request = PortalUtil.getHttpServletRequest(renderRequest);
		String p_p_id = PortalUtil.getOriginalServletRequest(request).getParameter("p_p_id");
		String path = PortalUtil.getOriginalServletRequest(request).getParameter("_"+ p_p_id +"_mvc_path");
		
		log.info("[PROFLUO] InventoryNoRuleController Portlet - render ()");
		log.info("[PROFLUO] Context Path: " + renderRequest.getContextPath());
		log.info("[PROLFUO] PorletID :" + p_p_id);
		log.info("[PROFLUO] Portlet path :"+ path);
		
		if ( path == null || path.endsWith("view.jsp")){
			log.info("[PROFLUO] Listing render()");
			log.info("[PROFLUO]");
		}
		
	    super.render(renderRequest, renderResponse);
	}
	
	public void saveRule(ActionRequest request, ActionResponse response) {
    	// remove default error message from Liferay
    	PortletConfig portletConfig = (PortletConfig)request.getAttribute(JavaConstants.JAVAX_PORTLET_CONFIG);
    	LiferayPortletConfig liferayPortletConfig = (LiferayPortletConfig) portletConfig;
    	SessionMessages.add(request, liferayPortletConfig.getPortletId() + SessionMessages.KEY_SUFFIX_HIDE_DEFAULT_ERROR_MESSAGE);
		
		log.info("[PROFLUO] InvNoRulesController portlet - save()");
		
	    try {
	    	// read request parameters
	    	String id = request.getParameter("id");
    		// keep parameters from request on response
    		ControllerUtils.saveRequestParamsOnResponse(request, response, log);
    		
    	    // get parameters from form in order to send to database
    		String iasID = request.getParameter("id_ias");
    		String grupaSintetica = request.getParameter("grupa_sintetica");
    		String denGrupaSintetica = request.getParameter("den_grupa_sintetica");
    		System.out.println(denGrupaSintetica);
    		String sqrGroup = request.getParameter("sqr_group");
    		String storeCode = request.getParameter("store_code_no");
    		String storePosition = request.getParameter("store_pos");
    		String lastInvNo = request.getParameter("last_inv_no");
    	    String status = request.getParameter("status");
    	    
	    	if (grupaSintetica != null) {
	    		// save currency
	    	    // check if all parameters are ok
	    
	    	    if (grupaSintetica != null && denGrupaSintetica != null && sqrGroup != null && storeCode != null && lastInvNo != null && status != null) {
	    	    	// update entry in DB
	    	    	System.out.println("update rule");
	    	    	int noOfEntries = InventoryNoRule.updateEntry(Integer.parseInt(id), grupaSintetica, denGrupaSintetica, sqrGroup, storeCode, lastInvNo, status);
	    	    	// check if update was ok
	    	    	if (noOfEntries > 0) {
	    	    		SessionMessages.add(request, "update_ok");
	    	    		
	    	    		response.setRenderParameter("mvcPath", "/html/definitions/inventorynorules/addInvRule.jsp");
	    	    	} else {
	    	    		SessionErrors.add(request, "update_nok");
	    	    		response.setRenderParameter("mvcPath", "/html/definitions/inventorynorules/addInvRule.jsp");
	    	    	}
	    	    } else {
	    	    	SessionErrors.add(request, "params_nok");
	    	    	response.setRenderParameter("mvcPath", "/html/definitions/inventorynorules/addInvRule.jsp");
	    	    }
	    	} else {
	    	    //check if entry already exists in the database
	    	       	// check if form parameters are ok
	    	    	 if ( iasID != null && grupaSintetica != null && denGrupaSintetica != null && sqrGroup != null && storeCode != null && storePosition != null && lastInvNo != null && status != null) {
		    	    	// insert entry into db
		    	    	int noOfEntries = InventoryNoRule.insertEntry(iasID,grupaSintetica,denGrupaSintetica,sqrGroup,storeCode,storePosition,lastInvNo,status);
		    	    	
		    	    	// show status to user
		    	    	if (noOfEntries > 0) {
		    	    		SessionMessages.add(request, "insert_ok");
		    	    		
		    	    	} else {
		    	    		SessionErrors.add(request, "insert_nok");
		    	    		response.setRenderParameter("mvcPath", "/html/definitions/inventorynorules/addInvRule.jsp");
		    	    	}
		    	    } else {
		    	    	SessionErrors.add(request, "params_nok");
		    	    	response.setRenderParameter("mvcPath", "/html/definitions/inventorynorules/addInvRule.jsp");
		    	    }
		    	}	
	    	   } catch (Exception e) {
	        SessionErrors.add(request, e.getClass().getName());
	        // just for show
	        SessionErrors.add(request, "error");

	        response.setRenderParameter("mvcPath", "/html/definitions/inventorynorules/addInvRule.jsp");
	    }
	}

}
