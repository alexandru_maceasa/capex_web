package com.profluo.ec.carrefour.definitions;

import java.io.IOException;
import java.io.PrintWriter;
//import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletConfig;
import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;
import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.portlet.LiferayPortletConfig;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.servlet.SessionMessages;
import com.liferay.portal.kernel.util.JavaConstants;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.util.PortalUtil;
import com.liferay.util.bridges.mvc.MVCPortlet;
import com.profluo.ecm.controller.ControllerUtils;
import com.profluo.ecm.model.db.DatabaseConnectionManager;
import com.profluo.ecm.model.db.DefIFRS;
import com.profluo.ecm.model.vo.DefIFRSVo;
import com.profluo.ecm.model.vo.DefLotVo;
import com.profluo.ecm.model.vo.DefProductVo;
import com.profluo.ecm.utils.ExcelUtil;

/**
 * Portlet implementation class DefGrupeIFRSController
 */
public class DefGrupeIFRSController extends MVCPortlet {
	/**
	 * 
	 */
	private final Log log = LogFactoryUtil.getLog("PROFLUO-"
			+ DefGrupeIFRSController.class.getName());

	/**
	 * @see MVCPortlet#MVCPortlet()
	 */
	public DefGrupeIFRSController() {
		super();
		log.info("[PROFLUO] Initialize DefGrupeIFRSController portlet.");
	}

	public void serveResource(ResourceRequest resourceRequest,
			ResourceResponse resourceResponse) throws IOException,
			PortletException {

		String action = ParamUtil.getString(resourceRequest, "action");
		log.info("[AJAX] action: " + action);

		if (action.equals("exportExcel")) {
			List<HashMap<String, Object>> allCategories = DefIFRSVo
					.getInstance();
			String[][] header = new String[7][];
			for (int i = 0; i < 7; i++) {
				header[i] = new String[2];
			}

			header[0][0] = "grupa_sintetica";
			header[0][1] = "Grupa sintetica";
			header[1][0] = "grupa_analitica";
			header[1][1] = "Grupa analitica";
			header[2][0] = "name";
			header[2][1] = "Denumire grupa";
			header[3][0] = "durata_minima";
			header[3][1] = "Durata minima";
			header[4][0] = "durata_maxima";
			header[4][1] = "Durata maxima";
			header[5][0] = "durata_implicita";
			header[5][1] = "Durata implicita";
			header[6][0] = "status";
			header[6][1] = "Status";

			try {
				ExcelUtil.getExcel(allCategories, header, "Raport_IFRS",
						resourceResponse);
			} catch (IOException e) {
				e.printStackTrace();
			}

		} else {

			PrintWriter writer = resourceResponse.getWriter();

			log.info("[AJAX] DefGrupeIFRSController");
			log.info("[AJAX] action: "
					+ ParamUtil.getString(resourceRequest, "action"));
			log.info("[AJAX] start: "
					+ ParamUtil.getString(resourceRequest, "start"));
			log.info("[AJAX] count: "
					+ ParamUtil.getString(resourceRequest, "count"));

			int start = Integer.parseInt(ParamUtil.getString(resourceRequest,
					"start").toString());
			int count = Integer.parseInt(ParamUtil.getString(resourceRequest,
					"count").toString());
			int total = Integer.parseInt(ParamUtil.getString(resourceRequest,
					"total").toString());
			String grupa_analitica = ParamUtil.getString(resourceRequest,
					"grupa_analitica");
			String grupa_sintetica = ParamUtil.getString(resourceRequest,
					"grupa_sintetica");

			log.info("[AJAX] start: " + count);
			log.info("[AJAX] count: " + start);
			log.info("[AJAX] total: " + total);

			List<HashMap<String, Object>> allIFRS = null;

			log.info("Getting all filtered!");
			allIFRS = DefIFRS.getAllFiltered(start, count, grupa_sintetica,
					grupa_analitica);
			// if total is 0 we need to refresh the count
			if (total == 0) {
				total = DefIFRS.getAllFilteredCount(grupa_sintetica,
						grupa_analitica);
			}
			JSONArray jsonIFRS = new JSONArray();
			if (allIFRS != null) {
				jsonIFRS = DatabaseConnectionManager.convertListToJson(allIFRS);
			}
			JSONObject jsonResponse = new JSONObject();

			jsonResponse.put("action", action);
			jsonResponse.put("start", start);
			jsonResponse.put("count", count);
			jsonResponse.put("total", total);
			jsonResponse.put("values", jsonIFRS);
			writer.print(jsonResponse.toJSONString());
		}
	}

	/**
	 * 
	 * @param request
	 * @param response
	 **/

	public void saveIFRS(ActionRequest request, ActionResponse response) {
		// remove default error message from Liferay
		PortletConfig portletConfig = (PortletConfig) request
				.getAttribute(JavaConstants.JAVAX_PORTLET_CONFIG);
		LiferayPortletConfig liferayPortletConfig = (LiferayPortletConfig) portletConfig;
		SessionMessages.add(request, liferayPortletConfig.getPortletId()
				+ SessionMessages.KEY_SUFFIX_HIDE_DEFAULT_ERROR_MESSAGE);

		log.info("[PROFLUO] DefGrupeIFRSController portlet - save()");

		try {

			// keep parameters from request on response
			ControllerUtils.saveRequestParamsOnResponse(request, response, log);
			// get parameters from form in order to send to database
			String name = request.getParameter("name");
			String grupa_sintetica = request.getParameter("grupa_sintetica");
			String grupa_analitica = request.getParameter("grupa_analitica");
			String durata_minima = request.getParameter("durata_minima");
			String durata_maxima = request.getParameter("durata_maxima");
			String durata_implicita = request.getParameter("durata_implicita");
			String status = request.getParameter("status");
			// read request parameters
			String id = request.getParameter("id");

			if (id != null) {
				// save ifrs group
				// check if all parameters are ok
				if (name != null && status != null) {
					// update entry in DB
					int noOfEntries = DefIFRS.updateEntry(Integer.parseInt(id),
							name, grupa_sintetica, grupa_analitica,
							Integer.parseInt(durata_minima),
							Integer.parseInt(durata_maxima),
							Integer.parseInt(durata_implicita), status);
					// check if update was ok
					if (noOfEntries > 0) {
						SessionMessages.add(request, "update_ok");
						DefIFRSVo.resetAndgetInstance();
						DefLotVo.resetAndgetInstance();
						DefProductVo.resetAndgetInstance();
						response.setRenderParameter("mvcPath",
								"/html/definitions/defgrupeifrscontroller/add_ifrs.jsp");
					} else {
						SessionErrors.add(request, "update_nok");
						response.setRenderParameter("mvcPath",
								"/html/definitions/defgrupeifrscontroller/add_ifrs.jsp");
					}
				} else {
					SessionErrors.add(request, "params_nok");
					response.setRenderParameter("mvcPath",
							"/html/definitions/defgrupeifrscontroller/add_ifrs.jsp");
				}
			} else {
				// check if entry already exists in the database
				if (!DefIFRSVo.exists(name)
						&& !DefIFRSVo.existsCombo(grupa_analitica,
								grupa_sintetica)) {
					// check if form parameters are ok
					if (name != null && status != null) {
						// insert entry into db
						int noOfEntries = DefIFRS.insertEntry(name,
								grupa_sintetica, grupa_analitica,
								Integer.parseInt(durata_minima),
								Integer.parseInt(durata_maxima),
								Integer.parseInt(durata_implicita), status);

						// show status to user
						if (noOfEntries > 0) {
							SessionMessages.add(request, "insert_ok");
							DefIFRSVo.resetAndgetInstance();
							DefLotVo.resetAndgetInstance();
							DefProductVo.resetAndgetInstance();
							response.setRenderParameter("mvcPath",
									"/html/definitions/defgrupeifrscontroller/view.jsp");
						} else {
							SessionErrors.add(request, "insert_nok");
							response.setRenderParameter("mvcPath",
									"/html/definitions/defgrupeifrscontroller/add_ifrs.jsp");
						}
					} else {
						SessionErrors.add(request, "params_nok");
						response.setRenderParameter("mvcPath",
								"/html/definitions/defgrupeifrscontroller/add_ifrs.jsp");
					}
				} else {
					SessionErrors.add(request, "entry_exists");
					response.setRenderParameter("mvcPath",
							"/html/definitions/defgrupeifrscontroller/add_ifrs.jsp");
				}
			}
		} catch (Exception e) {
			SessionErrors.add(request, e.getClass().getName());
			// just for show
			SessionErrors.add(request, "error");

			response.setRenderParameter("mvcPath",
					"/html/definitions/defgrupeifrscontroller/add_ifrs.jsp");
		}
	}

	@Override
	public void render(RenderRequest renderRequest,
			RenderResponse renderResponse) throws PortletException, IOException {
		HttpServletRequest request = PortalUtil
				.getHttpServletRequest(renderRequest);
		String p_p_id = PortalUtil.getOriginalServletRequest(request)
				.getParameter("p_p_id");
		String path = PortalUtil.getOriginalServletRequest(request)
				.getParameter("_" + p_p_id + "_mvcPath");

		log.info("[PROFLUO] DefGrupeIFRSController portlet - render()");
		log.info("[PROFLUO] Context Path: " + renderRequest.getContextPath());
		log.info("[PROFLUO] Portlet ID	: " + p_p_id);
		log.info("[PROFLUO] Portlet Path: " + path);

		if (path == null || path.endsWith("view.jsp")) {
			log.info("[PROFLUO] LISTING IFRS render()");
		} else if (path.endsWith("add_ifrs.jsp")) {
			log.info("[PROFLUO] Save IFRS render()");

		}

		super.render(renderRequest, renderResponse);
	}

}
