package com.profluo.ec.carrefour.definitions;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletConfig;
import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;
import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.portlet.LiferayPortletConfig;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.servlet.SessionMessages;
import com.liferay.portal.kernel.util.JavaConstants;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.util.PortalUtil;
import com.liferay.util.bridges.mvc.MVCPortlet;
import com.profluo.ecm.controller.ControllerUtils;
import com.profluo.ecm.model.db.DatabaseConnectionManager;
//import com.profluo.ecm.model.db.GenericDBStatements;
import com.profluo.ecm.model.db.ListaInit;
import com.profluo.ecm.model.vo.ListaInitVo;
import com.profluo.ecm.utils.ExcelUtil;

/**
 * Portlet implementation class DefCurrencyController
 */
public class ListaInitiativeController extends MVCPortlet {
	/**
	 * 
	 */
	private final Log log = LogFactoryUtil.getLog("PROFLUO-" + ListaInitiativeController.class.getName());
	
    /**
     * @see MVCPortlet#MVCPortlet()
     */
    public ListaInitiativeController() {
        super();
        log.info("[PROFLUO] Initialize ListaInitiativeController portlet.");
    }
    
    private String[][] getSQLFields(ActionRequest request, ActionResponse response) {
    	int arrSize = 7;
        String[][] arrFields = new String [arrSize][2];

        for (int i = 0; i < arrSize; i++) {
        	arrFields[i] = new String[2];
        }
    	arrFields[0][0] = "name";
    	arrFields[0][1] = request.getParameter("name");;
    	arrFields[1][0] = "code";
    	arrFields[1][1] = request.getParameter("code");;
    	arrFields[2][0] = "status";
    	arrFields[2][1] = request.getParameter("status");;
    	arrFields[3][0] = "year";
    	arrFields[3][1] = request.getParameter("year");
    	arrFields[4][0] = "currency";
    	arrFields[4][1] = request.getParameter("currency");
    	arrFields[5][0] = "is_property";
    	arrFields[5][1] = request.getParameter("is_property");
    	arrFields[6][0] = "is_modified";
    	arrFields[6][1] = "0";
    	
    	return arrFields;
    }
 
	/**
	 * 
	 * @param request
	 * @param response
	 */
	public void saveListInit(ActionRequest request, ActionResponse response) {
    	// remove default error message from Liferay
    	PortletConfig portletConfig = (PortletConfig)request.getAttribute(JavaConstants.JAVAX_PORTLET_CONFIG);
    	LiferayPortletConfig liferayPortletConfig = (LiferayPortletConfig) portletConfig;
    	SessionMessages.add(request, liferayPortletConfig.getPortletId() + SessionMessages.KEY_SUFFIX_HIDE_DEFAULT_ERROR_MESSAGE);
		
		log.info("[PROFLUO] ListaInitiativeController portlet - save()");
		
	    try {
	    	// read request parameters
	    	String id = request.getParameter("id");
    	    String name = request.getParameter("name");
    	    String start_date=request.getParameter("start_date");
    	    String end_date=request.getParameter("end_date");
    	    String retea=request.getParameter("retea");
    	    String currency=request.getParameter("currency");
    	  //  String year=request.getParameter("year");
    	    String year= String.valueOf(Calendar.getInstance().get(Calendar.YEAR));
    	    String code=request.getParameter("code");
    	    if(code == null || code.equals("")) {
    	    	code = getNextInitCode();
    	    }
    	    String status=request.getParameter("status");
    	    String is_property=request.getParameter("is_property");
   	    
    	    
    		// keep parameters from request on response
    		ControllerUtils.saveRequestParamsOnResponse(request, response, log);
	    	String[][] arrFields = this.getSQLFields(request, response);
	    	if (id != null) {
	    		// save initiative
    	    	// update entry in DB
    	    	// int noOfEntries = ListaInit.updateEntry(Integer.parseInt(id), name, code, status);
    			//attrs for where statement
            	ArrayList<String> whereStatement = new ArrayList<String>();
            	//attrs values for where statement
            	ArrayList<String> whereStatementValues = new ArrayList<String>();
            	whereStatement.add("id");
            	whereStatementValues.add(id);
            	//ArrayList<String> excludes = new ArrayList<String>();
    	    	arrFields[6][1] = "1";
    	    //	int noOfEntries = GenericDBStatements.updateEntry(arrFields, "def_lista_initiative", excludes, whereStatement, whereStatementValues);
    	    	int noOfEntries = ListaInit.updateEntry(Integer.parseInt(id),name, code ,Integer.parseInt(year),currency,status,is_property,retea,start_date,end_date);
    	    	
    	    	// check if update was ok
    	    	if (noOfEntries > 0) {
    	    		SessionMessages.add(request, "update_ok");
    	    		ListaInitVo.resetAndgetInstance();
    	    		response.setRenderParameter("mvcPath", "/html/definitions/deflistainitiativecontroller/add_lista_ini.jsp");
    	    	} else {
    	    		SessionErrors.add(request, "update_nok");
    	    		response.setRenderParameter("mvcPath", "/html/definitions/deflistainitiativecontroller/add_lista_ini.jsp");
    	    	}
	    	} else {
	    		//check if entry already exists in the database
	    		if(!ListaInitVo.exists(name)){
	    			// check if form parameters are ok
	    	    	// insert entry into db
	    	    //	ArrayList<String> excludes = new ArrayList<String>();
	    	    //	System.out.println(arrFields);
	    	    	//int noOfEntries = GenericDBStatements.insertEntry(arrFields, "def_lista_initiative", excludes);
	    	    int noOfEntries = ListaInit.insertEntry(name, code ,Integer.parseInt(year),currency,status,is_property,retea,start_date,end_date);
	    	    	
	    	    	// show status to user
	    	    	if (noOfEntries > 0) {
	    	    		SessionMessages.add(request, "insert_ok");
	    	    		ListaInitVo.resetAndgetInstance();
	    	    	} else {
	    	    		SessionErrors.add(request, "insert_nok");
	    	    		response.setRenderParameter("mvcPath", "/html/definitions/deflistainitiativecontroller/add_lista_ini.jsp");
	    	    	}
	    	    } else {
	    	    	SessionErrors.add(request, "entry_exists");
	    	    	response.setRenderParameter("mvcPath", "/html/definitions/deflistainitiativecontroller/add_lista_ini.jsp");
	    	    }
	    	}
	    } catch (Exception e) {
	        SessionErrors.add(request, e.getClass().getName());
	        // just for show
	        SessionErrors.add(request, "error");
	        response.setRenderParameter("mvcPath", "/html/definitions/deflistainitiativecontroller/add_lista_ini.jsp");
	    }
	}
	
	@Override
	public void render(RenderRequest renderRequest, RenderResponse renderResponse) throws PortletException, IOException {
		HttpServletRequest request = PortalUtil.getHttpServletRequest(renderRequest);
    	String p_p_id = PortalUtil.getOriginalServletRequest(request).getParameter("p_p_id");
    	String path = PortalUtil.getOriginalServletRequest(request).getParameter("_" + p_p_id + "_mvcPath");

		log.info("[PROFLUO] ListaInit portlet - render()");
		log.info("[PROFLUO] Context Path: " + renderRequest.getContextPath());
    	log.info("[PROFLUO] Portlet ID	: " + p_p_id);
    	log.info("[PROFLUO] Portlet Path: " + path);
    	
    	if (path == null || path.endsWith("view.jsp")) {
    		log.info("[PROFLUO] LISTING render()");
    	} else if (path.endsWith("add_lista_ini.jsp")) {
    		log.info("[PROFLUO] Save Initiatives  render()");
    	}
		
	    super.render(renderRequest, renderResponse);
	}
	
	@SuppressWarnings("unchecked")
	public void serveResource(ResourceRequest resourceRequest, ResourceResponse resourceResponse) throws IOException, PortletException {
    	log.info("[AJAX] ListaInitiativeController");
    	
    	String action = ParamUtil.getString(resourceRequest, "action");
    	log.info("[AJAX] action: " + action);
    	
    	if (action.equals("exportExcel")){	
    		List<HashMap<String, Object>> allCategories = ListaInitVo.getInstance();
    		
    		String[][] header = new String[9][];
			for (int i = 0; i < 9; i++) {
				header[i] = new String[2];
			}
			
			header[0][0] = "code";
			header[0][1] = "Cod produs";
			header[1][0] = "name";
			header[1][1] = "Nume";
			header[2][0] = "year";
			header[2][1] = "An";
			header[3][0] = "currency";
			header[3][1] = "Moneda";
			header[4][0] = "status";
			header[4][1] = "Status";
			header[5][0] = "is_property";
			header[5][1] = "Responsabil";
			header[6][0] = "start_date";
			header[6][1] = "Data inceput";
			header[7][0] = "end_date";
			header[7][1] = "Data sfarsit";
			header[8][0] = "store_type";
			header[8][1] = "Retea";
			
			try {
				ExcelUtil.getExcel(allCategories, header, "Raport_Initiative", resourceResponse);
			} catch (IOException e) {
				e.printStackTrace();
			}
    		
    		
    	} else if (action.equals("filter") || action.equals("next") || action.equals("prev") || action.equals("last") || action.equals("first")) {
    		PrintWriter writer = resourceResponse.getWriter();
    	 	int start = Integer.parseInt(ParamUtil.getString(resourceRequest, "start").toString());
	    	int count = Integer.parseInt(ParamUtil.getString(resourceRequest, "count").toString());
	    	int total = Integer.parseInt(ParamUtil.getString(resourceRequest, "total").toString());
	    	String code = ParamUtil.getString(resourceRequest, "code");
	    	String name = ParamUtil.getString(resourceRequest, "name");

	    	log.info("[AJAX] start: " + count);
	    	log.info("[AJAX] count: " + start);
	    	log.info("[AJAX] total: " + total);
	    		    	
	    	List<HashMap<String,Object>> allInit = null;
	    	
			log.info("Getting all filtered!");
			allInit = ListaInit.getAllFiltered(start, count, code, name);
			// if total is 0 we need to refresh the count
			if (total == 0) {
				total = ListaInit.getAllFilteredCount(code, name);
			}
	    	
	    	JSONArray jsonInit = new JSONArray();
	    	if (allInit != null) {
	    		jsonInit = DatabaseConnectionManager.convertListToJson(allInit);
	    	}
	    	JSONObject jsonResponse = new JSONObject();

	    	jsonResponse.put("action", action);
	    	jsonResponse.put("start", start);
			jsonResponse.put("count", count);
			jsonResponse.put("total", total);
			jsonResponse.put("values", jsonInit);
			writer.print(jsonResponse.toJSONString());
    	}       
    } 
	
	private String getNextInitCode() {
		String result = "Init";
		int maxCode = ListaInit.getMaxInitCode();
		result += (++maxCode);
		return result;
	}
}
