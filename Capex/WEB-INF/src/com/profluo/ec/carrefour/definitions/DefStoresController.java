package com.profluo.ec.carrefour.definitions;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletConfig;
import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;
import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.portlet.LiferayPortletConfig;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.servlet.SessionMessages;
import com.liferay.portal.kernel.util.JavaConstants;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.util.PortalUtil;
import com.liferay.util.bridges.mvc.MVCPortlet;
import com.profluo.ecm.controller.ControllerUtils;
import com.profluo.ecm.model.db.DatabaseConnectionManager;
//import com.profluo.ecm.model.db.DefExchRates;
import com.profluo.ecm.model.db.DefStore;
import com.profluo.ecm.model.vo.DefStoreVo;

/**
 * Portlet implementation class DefStoresController
 */
public class DefStoresController extends MVCPortlet {
	/**
	 * 
	 */
	private final Log log = LogFactoryUtil.getLog("PROFLUO-" + DefStoresController.class.getName());
	
    /**
     * @see MVCPortlet#MVCPortlet()
     */
    public DefStoresController() {
        super();
        log.info("[PROFLUO] Initialize DefStoresController portlet.");
    }
    
    @Override
    public void serveResource(ResourceRequest resourceRequest, ResourceResponse resourceResponse) throws IOException, PortletException {
    	log.info("[AJAX] DefStoresController");
    	
    	String action = ParamUtil.getString(resourceRequest, "action");
    	
    	if (action.equals("filter") || action.equals("next") || action.equals("prev") || action.equals("last") || action.equals("first")) {
	    	//System.out.println("Action "+ParamUtil.getString(resourceRequest, "action").toString());
	    	//System.out.println("Start "+ParamUtil.getString(resourceRequest, "start").toString());
	    	//System.out.println("Total "+ParamUtil.getString(resourceRequest, "total").toString());
	    	int start = Integer.parseInt(ParamUtil.getString(resourceRequest, "start").toString());
	    	int count = Integer.parseInt(ParamUtil.getString(resourceRequest, "count").toString());
	    	int total = Integer.parseInt(ParamUtil.getString(resourceRequest, "total").toString());
	    	String code = ParamUtil.getString(resourceRequest, "code");
	    	String name = ParamUtil.getString(resourceRequest, "name");

	    	PrintWriter writer = resourceResponse.getWriter();
	
	    	log.info("[AJAX] start: " + count);
	    	log.info("[AJAX] count: " + start);
	    	log.info("[AJAX] total: " + total);
	    	List<HashMap<String,Object>> allStores = null;
	    	
			log.info("Getting all filtered!");
			//System.out.println("start "+start);
			//System.out.println("count "+count);
			allStores = DefStore.getAllFiltered(start, count, code, name);
			// if total is 0 we need to refresh the count
			if (total == 0) {
				total = DefStore.getAllFilteredCount(code, name);
			}
			
	    	JSONArray jsonStores = new JSONArray();
	    	if (allStores != null) {
	    		jsonStores = DatabaseConnectionManager.convertListToJson(allStores);
	    	}
	    	JSONObject jsonResponse = new JSONObject();
	    	
			jsonResponse.put("action", action);
			jsonResponse.put("start", start);
			jsonResponse.put("count", count);
			jsonResponse.put("total", total);
			jsonResponse.put("values", jsonStores);
			writer.print(jsonResponse.toJSONString());
    	}
    	//super.serveResource(resourceRequest, resourceResponse);
    } 
 
	/**
	 * 
	 * @param request
	 * @param response
	 */
	public void saveStore(ActionRequest request, ActionResponse response) {
    	// remove default error message from Liferay
    	PortletConfig portletConfig = (PortletConfig)request.getAttribute(JavaConstants.JAVAX_PORTLET_CONFIG);
    	LiferayPortletConfig liferayPortletConfig = (LiferayPortletConfig) portletConfig;
    	SessionMessages.add(request, liferayPortletConfig.getPortletId() + SessionMessages.KEY_SUFFIX_HIDE_DEFAULT_ERROR_MESSAGE);
		
		log.info("[PROFLUO] DefStoresController portlet - save()");
		
	    try {
	    	// read request parameters
	    	String id = request.getParameter("id");
	    	
	    	if (id != null) {
	    		// save currency
	    		// keep parameters from request on response
	    		ControllerUtils.saveRequestParamsOnResponse(request, response, log);
	    	    // get parameters from form in order to send to database
	    	    String gln			= request.getParameter("GLN");
	    	    String storetype	= request.getParameter("storetype");
	    	    String status 		= request.getParameter("status");
	    	    String codBFC 		= request.getParameter("cod_bfc");
	    	    
	    	    // check if all parameters are ok
	    	    if (storetype != null && status != null) {
	    	    	// update entry in DB
	    	    	int noOfEntries = DefStore.updateEntry(Integer.parseInt(id), gln, storetype, status, codBFC);
	    	    	// check if update was ok
	    	    	if (noOfEntries > 0) {
	    	    		SessionMessages.add(request, "update_ok");
	    	    		DefStoreVo.resetAndgetInstance();
	    	    		response.setRenderParameter("mvcPath", "/html/definitions/defstorescontroller/add_store.jsp");
	    	    	} else {
	    	    		SessionErrors.add(request, "update_nok");
	    	    		response.setRenderParameter("mvcPath", "/html/definitions/defstorescontroller/add_store.jsp");
	    	    	}
	    	    } else {
	    	    	SessionErrors.add(request, "params_nok");
	    	    	response.setRenderParameter("mvcPath", "/html/definitions/defstorescontroller/add_store.jsp");
	    	    }
	    	} /*else {
	    		// create currency
	    		
	    		ControllerUtils.saveRequestParamsOnResponse(request, response, log);
	    		
	    		// get form parameters 
	    	    String name 		= request.getParameter("name");
	    	    String store_code	= request.getParameter("store_code");
	    	    String address		= request.getParameter("address");
	    	    String email		= request.getParameter("email");
	    	    String phone		= request.getParameter("phone");
	    	    String company_id	= request.getParameter("company_id");
	    	    String storetype	= request.getParameter("storetype");
	    	    String status 		= request.getParameter("status");
	    	    
	    	   //check if entry already exists in the database
	    	    
	    	    // check if form parameters are ok
		    	    if (name != null &&  status != null) {
		    	    	// insert entry into db
		    	    	int noOfEntries = DefStore.insertEntry(name,store_code,address,email,phone,Integer.parseInt(company_id),Integer.parseInt(storetype),status);
		    	    	
		    	    	// show status to user
		    	    	if (noOfEntries > 0) {
		    	    		SessionMessages.add(request, "insert_ok");
		    	    	} else {
		    	    		SessionErrors.add(request, "insert_nok");
		    	    		response.setRenderParameter("mvcPath", "/html/definitions/defstorescontroller/add_store.jsp");
		    	    	}
		    	    } else {
		    	    	SessionErrors.add(request, "params_nok");
		    	    	response.setRenderParameter("mvcPath", "/html/definitions/defstorescontroller/add_store.jsp");
		    	    }
	    	    
	    	}*/
	    } catch (Exception e) {
	        SessionErrors.add(request, e.getClass().getName());
	        // just for show
	        SessionErrors.add(request, "error");

	        response.setRenderParameter("mvcPath", "/html/definitions/defstorescontroller/add_store.jsp");
	    }
	}
	
	@Override
	public void render(RenderRequest renderRequest, RenderResponse renderResponse) throws PortletException, IOException {
		HttpServletRequest request = PortalUtil.getHttpServletRequest(renderRequest);
    	String p_p_id = PortalUtil.getOriginalServletRequest(request).getParameter("p_p_id");
    	String path = PortalUtil.getOriginalServletRequest(request).getParameter("_" + p_p_id + "_mvcPath");

		log.info("[PROFLUO] DefStoresController portlet - render()");
		log.info("[PROFLUO] Context Path: " + renderRequest.getContextPath());
    	log.info("[PROFLUO] Portlet ID	: " + p_p_id);
    	log.info("[PROFLUO] Portlet Path: " + path);
    	
    	if (path == null || path.endsWith("view.jsp")) {
    		log.info("[PROFLUO] LISTING render()");
    		
    	} else if (path.endsWith("add_store.jsp")) {
    		log.info("[PROFLUO] Save Store render()");
    	}
		
	    super.render(renderRequest, renderResponse);
	}


}
