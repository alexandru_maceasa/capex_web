package com.profluo.ec.carrefour.definitions;

import java.io.IOException;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletConfig;
import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.servlet.http.HttpServletRequest;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.portlet.LiferayPortletConfig;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.servlet.SessionMessages;
import com.liferay.portal.kernel.util.JavaConstants;
import com.liferay.portal.util.PortalUtil;
import com.liferay.util.bridges.mvc.MVCPortlet;
import com.profluo.ecm.controller.ControllerUtils;
import com.profluo.ecm.model.db.DefCatGest;
import com.profluo.ecm.model.vo.DefCatGestVo;


/**
 * Portlet implementation class DefCatGestController
 */
public class DefCatGestController extends MVCPortlet {
	/**
	 * 
	 */
	private final Log log = LogFactoryUtil.getLog("PROFLUO-" + DefCatGestController.class.getName());
	
    /**
     * @see MVCPortlet#MVCPortlet()
     */
    public DefCatGestController() {
        super();
        log.info("[PROFLUO] Initialize DefCatGestController portlet.");
    }
 
	/**
	 * 
	 * @param request
	 * @param response
	 */
	public void saveCatGest(ActionRequest request, ActionResponse response) {
    	// remove default error message from Liferay
    	PortletConfig portletConfig = (PortletConfig)request.getAttribute(JavaConstants.JAVAX_PORTLET_CONFIG);
    	LiferayPortletConfig liferayPortletConfig = (LiferayPortletConfig) portletConfig;
    	SessionMessages.add(request, liferayPortletConfig.getPortletId() + SessionMessages.KEY_SUFFIX_HIDE_DEFAULT_ERROR_MESSAGE);
		
		log.info("[PROFLUO] DefCatGestController portlet - save()");
		
	    try {
	    	// read request parameters
	    	String id = request.getParameter("id");
	    	
	    	if (id != null) {
	    		// save category
	    		// keep parameters from request on response
	    		ControllerUtils.saveRequestParamsOnResponse(request, response, log);
	    	    // get parameters from form in order to send to database
	    	 //   String name = request.getParameter("name");
	    	    String ref = request.getParameter("ref");
	    	    String status = request.getParameter("status");
	    	    
	    	    // check if all parameters are ok
	    	    if ( ref != null && status != null) {
	    	    	// update entry in DB
	    	    	int noOfEntries = DefCatGest.updateEntry(Integer.parseInt(id), ref, status);
	    	    	// check if update was ok
	    	    	if (noOfEntries > 0) {
	    	    		SessionMessages.add(request, "update_ok");
	    	    		DefCatGestVo.resetAndgetInstance();
	    	    		response.setRenderParameter("mvcPath", "/html/definitions/defcatgestcontroller/add_cat_gest.jsp");
	    	    	} else {
	    	    		SessionErrors.add(request, "update_nok");
	    	    		response.setRenderParameter("mvcPath", "/html/definitions/defcatgestcontroller/add_cat_gest.jsp");
	    	    	}
	    	    } else {
	    	    	SessionErrors.add(request, "params_nok");
	    	    	response.setRenderParameter("mvcPath", "/html/definitions/defcatgestcontroller/add_cat_gest.jsp");
	    	    }
	    	} else {
	    		// create currency
	    		ControllerUtils.saveRequestParamsOnResponse(request, response, log);
	    		
	    		// get form parameters 
	    	  //  String name = request.getParameter("name");
	    	    String ref = request.getParameter("ref");
	    	    String status = request.getParameter("status");
	    	    
	    	    if(!DefCatGestVo.exists(ref)){
	    	    // check if form parameters are ok
	    	    if ( ref != null && status != null) {
	    	    	// insert entry into db
	    	    	int noOfEntries = DefCatGest.insertEntry(ref, status);
	    	    	
	    	    	// show status to user
	    	    	if (noOfEntries > 0) {
	    	    		SessionMessages.add(request, "insert_ok");
	    	    		DefCatGestVo.resetAndgetInstance();
	    	    	} else {
	    	    		SessionErrors.add(request, "insert_nok");
	    	    		response.setRenderParameter("mvcPath", "/html/definitions/defcatgestcontroller/add_cat_gest.jsp");
	    	    	}
	    	    	} else {
	    	    	SessionErrors.add(request, "params_nok");
	    	    	response.setRenderParameter("mvcPath", "/html/definitions/defcatgestcontroller/add_cat_gest.jsp");
	    	    	}
	    	    	}
	    	    else{
	    	    	SessionErrors.add(request,"entry_exists");
	    	    	response.setRenderParameter("mvcPath","/html/definitions/defcatgestcontroller/add_cat_gest.jsp");
	    	    	
	    	    }
	    	}
	    } catch (Exception e) {
	        SessionErrors.add(request, e.getClass().getName());
	        // just for show
	        SessionErrors.add(request, "error");

	        response.setRenderParameter("mvcPath", "/html/definitions/defcatgestcontroller/add_cat_gest.jsp");
	    }
	}
	
	@Override
	public void render(RenderRequest renderRequest, RenderResponse renderResponse) throws PortletException, IOException {
		HttpServletRequest request = PortalUtil.getHttpServletRequest(renderRequest);
    	String p_p_id = PortalUtil.getOriginalServletRequest(request).getParameter("p_p_id");
    	String path = PortalUtil.getOriginalServletRequest(request).getParameter("_" + p_p_id + "_mvcPath");

		log.info("[PROFLUO] DefCatGestController portlet - render()");
		log.info("[PROFLUO] Context Path: " + renderRequest.getContextPath());
    	log.info("[PROFLUO] Portlet ID	: " + p_p_id);
    	log.info("[PROFLUO] Portlet Path: " + path);
    	
    	if (path == null || path.endsWith("view.jsp")) {
    		log.info("[PROFLUO] LISTING render()");
    		
    	} else if (path.endsWith("add_cat_gest.jsp")) {
    		log.info("[PROFLUO] Save CATEGORY render()");
    		
    	}
		
	    super.render(renderRequest, renderResponse);
	}


}
