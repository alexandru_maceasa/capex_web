package com.profluo.ec.carrefour.definitions;

import java.util.ArrayList;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletConfig;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.portlet.LiferayPortletConfig;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.servlet.SessionMessages;
import com.liferay.portal.kernel.util.JavaConstants;
import com.liferay.portal.model.User;
import com.liferay.portal.service.UserServiceUtil;
import com.liferay.util.bridges.mvc.MVCPortlet;
import com.profluo.ecm.model.db.DefCursLunarBd;
import com.profluo.ecm.model.db.GenericDBStatements;

/**
 * Portlet implementation class DefCursLunar
 */
public class DefCursLunar extends MVCPortlet {
 
	
	public void saveCurs(ActionRequest request, ActionResponse response) {
		// remove default error message from Liferay
		PortletConfig portletConfig = (PortletConfig) request.getAttribute(JavaConstants.JAVAX_PORTLET_CONFIG);
		LiferayPortletConfig liferayPortletConfig = (LiferayPortletConfig) portletConfig;
		SessionMessages.add(request, liferayPortletConfig.getPortletId() + SessionMessages.KEY_SUFFIX_HIDE_DEFAULT_ERROR_MESSAGE);
		try {
			User user = UserServiceUtil.getUserById(Long.parseLong(request.getRemoteUser()));
			String id =request.getParameter("id").toString();
			String year =request.getParameter("year").toString();
			String month = request.getParameter("month").toString();
			String valoare = request.getParameter("valoare").toString();
			
			if(id.equals("")) {
				String arrFields[][] = new String [4][2];
				arrFields[0][0] = "year";
				arrFields[0][1] = year;
				arrFields[1][0] = "month";
				arrFields[1][1] = month;
				arrFields[2][0] = "valoare_curs";
				arrFields[2][1] = valoare;
				arrFields[3][0] = "user_id";
				arrFields[3][1] = String.valueOf(user.getUserId());
				int success = 0;
				success = GenericDBStatements.insertEntry(arrFields, "def_curs_lunar", new ArrayList<String>());
				if( success == 0){
					SessionErrors.add(request, "insert_nok");
				} else {
					SessionMessages.add(request, "insert_ok");
				}
				
			} else {
				DefCursLunarBd.updateEntry(id, year, month, valoare);
				SessionMessages.add(request, "update_ok");
			}
			
		} catch (NumberFormatException | PortalException | SystemException e) {
			e.printStackTrace();
		}
		
	}

}
