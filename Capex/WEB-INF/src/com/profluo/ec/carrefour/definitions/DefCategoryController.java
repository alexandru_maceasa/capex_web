package com.profluo.ec.carrefour.definitions;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletConfig;
import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;
import javax.servlet.http.HttpServletRequest;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.portlet.LiferayPortletConfig;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.servlet.SessionMessages;
import com.liferay.portal.kernel.util.JavaConstants;
import com.liferay.portal.util.PortalUtil;
import com.liferay.util.bridges.mvc.MVCPortlet;
import com.profluo.ecm.controller.ControllerUtils;
import com.profluo.ecm.model.db.DefCategory;
import com.profluo.ecm.model.vo.DefCategoryVo;
import com.profluo.ecm.model.vo.DefLotVo;
import com.profluo.ecm.model.vo.DefProductVo;
import com.profluo.ecm.utils.ExcelUtil;

/**
 * Portlet implementation class DefIASGroupeController
 */
public class DefCategoryController extends MVCPortlet {
	/**
	 * 
	 */
	private final Log log = LogFactoryUtil.getLog("PROFLUO-"
			+ DefCategoryController.class.getName());

	/**
	 * @see MVCPortlet#MVCPortlet()
	 */
	public DefCategoryController() {
		super();
		log.info("[PROFLUO] Initialize DefCategoryController portlet.");
	}

	/**
	 * 
	 * @param request
	 * @param response
	 */
	public void saveCat(ActionRequest request, ActionResponse response) {
		// remove default error message from Liferay
		PortletConfig portletConfig = (PortletConfig) request
				.getAttribute(JavaConstants.JAVAX_PORTLET_CONFIG);
		LiferayPortletConfig liferayPortletConfig = (LiferayPortletConfig) portletConfig;
		SessionMessages.add(request, liferayPortletConfig.getPortletId()
				+ SessionMessages.KEY_SUFFIX_HIDE_DEFAULT_ERROR_MESSAGE);

		log.info("[PROFLUO] DefCategoryController portlet - saveCat()");

		try {
			// read request parameters
			String id = request.getParameter("id");
			String code = request.getParameter("code");
			String name = request.getParameter("name");
			String flag_it = request.getParameter("flag_it");
			String status = request.getParameter("status");

			// keep parameters from request on response
			ControllerUtils.saveRequestParamsOnResponse(request, response, log);

			if (id != null) {
				// save category
				// check if all parameters are ok
				if (code != null && name != null && status != null) {
					// update entry in DB
					int noOfEntries = DefCategory.updateEntry(
							Integer.parseInt(id), code, name, flag_it, status);
					// check if update was ok
					if (noOfEntries > 0) {
						SessionMessages.add(request, "update_ok");
						DefCategoryVo.resetAndgetInstance();
						DefProductVo.resetAndgetInstance();
						DefLotVo.resetAndgetInstance();
						response.setRenderParameter("mvcPath",
								"/html/definitions/defcategorycontroller/add_cat.jsp");
					} else {
						SessionErrors.add(request, "update_nok");
						response.setRenderParameter("mvcPath",
								"/html/definitions/defcategorycontroller/add_cat.jsp");
					}
				} else {
					SessionErrors.add(request, "params_nok");
					response.setRenderParameter("mvcPath",
							"/html/definitions/defcatcontroller/add_cat.jsp");
				}
			} else {
				// create category
				// check if form parameters are ok
				if (code != null && name != null && status != null) {
					// insert entry into db
					int noOfEntries = DefCategory.insertEntry(code, name,
							flag_it, status);

					// show status to user
					if (noOfEntries > 0) {
						SessionMessages.add(request, "insert_ok");
						DefCategoryVo.resetAndgetInstance();
						DefProductVo.resetAndgetInstance();
						DefLotVo.resetAndgetInstance();
					} else {
						SessionErrors.add(request, "insert_nok");
						response.setRenderParameter("mvcPath",
								"/html/definitions/defcategorycontroller/add_cat.jsp");
					}
				} else {
					SessionErrors.add(request, "params_nok");
					response.setRenderParameter("mvcPath",
							"/html/definitions/defcategorycontroller/add_cat.jsp");
				}
			}
		} catch (Exception e) {
			SessionErrors.add(request, e.getClass().getName());
			// just for show
			SessionErrors.add(request, "error");

			response.setRenderParameter("mvcPath",
					"/html/definitions/defcategorycontroller/add_cat.jsp");
		}
	}

	/**
	 * 
	 */
	public void serveResource(ResourceRequest request, ResourceResponse response) {
		List<HashMap<String, Object>> allCategories = DefCategoryVo
				.getInstance();
		String[][] header = new String[4][];
		for (int i = 0; i < 4; i++) {
			header[i] = new String[2];
		}

		header[0][0] = "code";
		header[0][1] = "Cod";
		header[1][0] = "name";
		header[1][1] = "Nume";
		header[2][0] = "flag_it";
		header[2][1] = "Clasificare";
		header[3][0] = "status";
		header[3][1] = "Status";

		try {
			ExcelUtil.getExcel(allCategories, header, "Raport_Categorii",
					response);
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	@Override
	public void render(RenderRequest renderRequest,
			RenderResponse renderResponse) throws PortletException, IOException {
		HttpServletRequest request = PortalUtil
				.getHttpServletRequest(renderRequest);
		String p_p_id = PortalUtil.getOriginalServletRequest(request)
				.getParameter("p_p_id");
		String path = PortalUtil.getOriginalServletRequest(request)
				.getParameter("_" + p_p_id + "_mvcPath");

		log.info("[PROFLUO] DefCategoryController portlet - render()");
		log.info("[PROFLUO] Context Path: " + renderRequest.getContextPath());
		log.info("[PROFLUO] Portlet ID	: " + p_p_id);
		log.info("[PROFLUO] Portlet Path: " + path);

		if (path == null || path.endsWith("view.jsp")) {
			log.info("[PROFLUO] LISTING render()");
		} else if (path.endsWith("add_cat.jsp")) {
			log.info("[PROFLUO] Save Category render()");
		}

		super.render(renderRequest, renderResponse);
	}
}
