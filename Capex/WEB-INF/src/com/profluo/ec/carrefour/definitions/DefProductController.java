package com.profluo.ec.carrefour.definitions;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.List;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletConfig;
import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;
import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.portlet.LiferayPortletConfig;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.servlet.SessionMessages;
import com.liferay.portal.kernel.util.JavaConstants;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.util.PortalUtil;
import com.liferay.util.bridges.mvc.MVCPortlet;
import com.profluo.ecm.controller.ControllerUtils;
import com.profluo.ecm.model.db.DatabaseConnectionManager;
import com.profluo.ecm.model.db.DefProduct;
import com.profluo.ecm.model.vo.DefProductVo;
import com.profluo.ecm.utils.ExcelUtil;

/**
 * Portlet implementation class Defias_idGroupeController
 */
public class DefProductController extends MVCPortlet {
	/**
	 * 
	 */
	private final Log log = LogFactoryUtil.getLog("PROFLUO-"
			+ DefProductController.class.getName());

	/**
	 * @see MVCPortlet#MVCPortlet()
	 */
	public DefProductController() {
		super();
		log.info("[PROFLUO] Initialize DefProductController portlet.");
	}

	/**
	 * 
	 * @param request
	 * @param response
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void serveResource(ResourceRequest resourceRequest,
			ResourceResponse resourceResponse) throws IOException,
			PortletException {
		log.info("[AJAX] DefProductController");

		String action = ParamUtil.getString(resourceRequest, "action");

		if (action.equals("exportExcel")) {

			List<HashMap<String, Object>> allCategories = DefProductVo
					.getInstance();

			String[][] header = new String[13][];
			for (int i = 0; i < 13; i++) {
				header[i] = new String[2];
			}

			header[0][0] = "code";
			header[0][1] = "Cod produs";
			header[1][0] = "name";
			header[1][1] = "Nume produs";
			header[2][0] = "lot_name";
			header[2][1] = "Lot";
			header[3][0] = "lot_warranty";
			header[3][1] = "Garantie Lot";
			header[4][0] = "cat_name";
			header[4][1] = "Categorie";
			header[5][0] = "flag_it";
			header[5][1] = "Clasificare";
			header[6][0] = "ias_code";
			header[6][1] = "IAS";
			header[7][0] = "ifrs_code";
			header[7][1] = "IFRS";
			header[8][0] = "price_per_unit";
			header[8][1] = "Pret unitar";
			header[9][0] = "status";
			header[9][1] = "Status";
			header[10][0] = "cont_imob_in_curs";
			header[10][1] = "Cont imobiliar in curs";
			header[11][0] = "cont_imob_avans";
			header[11][1] = "Cont imobiliar avans";
			header[12][0] = "cont_imob_in_fct";
			header[12][1] = "Cont imobiliar in functie";
			
			try {
				ExcelUtil.getExcel(allCategories, header, "Raport_Produse",
						resourceResponse);
			} catch (IOException e) {
				e.printStackTrace();
			}

		} else if (action.equals("filter") || action.equals("next") || action.equals("prev") || action.equals("last") || action.equals("first")) {

			int start = Integer.parseInt(ParamUtil.getString(resourceRequest, "start").toString());
			int count = Integer.parseInt(ParamUtil.getString(resourceRequest, "count").toString());
			int total = Integer.parseInt(ParamUtil.getString(resourceRequest, "total").toString());
			String code = ParamUtil.getString(resourceRequest, "code");
			String prod_name = ParamUtil.getString(resourceRequest, "prod_name");
			String lot_id = ParamUtil.getString(resourceRequest, "filter_lot_id").toString();
			int filter_lot_id = 0;
			if (!lot_id.equals("")) {
				filter_lot_id = Integer.parseInt(lot_id);
			}
			// resourceResponse.setProperty("test", "test_content");
			PrintWriter writer = resourceResponse.getWriter();

			log.info("[AJAX] start: " + count);
			log.info("[AJAX] count: " + start);
			log.info("[AJAX] total: " + total);
			List<HashMap<String, Object>> allProducts = null;

			if (code.equals("") && prod_name.equals("") && filter_lot_id == 0) {
				log.info("Getting all!");
				allProducts = DefProduct.getAll(start, count);
				// if total is 0 we need to refresh the count
				if (total == 0) {
					total = DefProduct.getCount();
				}
			} else {
				log.info("Getting all filtered!");
				System.out.println("start " + start);
				System.out.println("count " + count);
				allProducts = DefProduct.getAllFiltered(start, count, code, prod_name, filter_lot_id);
				// if total is 0 we need to refresh the count
				if (total == 0) {
					total = DefProduct.getAllFilteredCount(start, count, code, prod_name, filter_lot_id);
				}
			}
			if (allProducts == null) {
				System.out.println("NULL");
			}
			JSONArray jsonProducts = new JSONArray();
			if (allProducts != null) {
				jsonProducts = DatabaseConnectionManager
						.convertListToJson(allProducts);
			}
			JSONObject jsonResponse = new JSONObject();

			jsonResponse.put("action", action);
			jsonResponse.put("start", start);
			jsonResponse.put("count", count);
			jsonResponse.put("total", total);
			jsonResponse.put("values", jsonProducts);
			writer.print(jsonResponse.toJSONString());
		}
		// super.serveResource(resourceRequest, resourceResponse);
	}

	/**
	 * 
	 * @param request
	 * @param response
	 **/

	public void saveProduct(ActionRequest request, ActionResponse response) {
		// remove default error message from Liferay
		PortletConfig portletConfig = (PortletConfig) request.getAttribute(JavaConstants.JAVAX_PORTLET_CONFIG);
		LiferayPortletConfig liferayPortletConfig = (LiferayPortletConfig) portletConfig;
		SessionMessages.add(request, liferayPortletConfig.getPortletId() + SessionMessages.KEY_SUFFIX_HIDE_DEFAULT_ERROR_MESSAGE);

		log.info("[PROFLUO] Defias_idGroupeController portlet - save()");

		try {
			// read request parameters
			String id = request.getParameter("id");
			boolean hasId = false;

			// keep parameters from request on response
			ControllerUtils.saveRequestParamsOnResponse(request, response, log);
			// get parameters from form in order to send to database
			String code = "";
			String lot_id = request.getParameter("lot_id");
			String cat_id = request.getParameter("cat_id");
			
			if (!(id == null || id.isEmpty())) {
				hasId = true;
			}
			System.out.println(cat_id + " -=> " + lot_id);
			
			if (!hasId){
				code = DefProduct.getLotCode(Integer.parseInt(cat_id), Integer.parseInt(lot_id));
			} else {
				code = request.getParameter("productCode").toString();
			}
			System.out.println(code);
			String name = request.getParameter("name");
			String flag_it = request.getParameter("flag_it");
			String status = request.getParameter("status");
			String ias_id = request.getParameter("ias_id");
			String ifrs_id = request.getParameter("ifrs_id");
			String pricePerUnit = request.getParameter("price_per_unit").replace(",", "");

			if (hasId) {
				// save product
				// check if all parameters are ok
				if (code != null && name != null) {
					// update entry in DB
					int noOfEntries = DefProduct.updateEntry(Integer.parseInt(id), lot_id, cat_id, name, flag_it, status,
							Integer.parseInt(ias_id), Integer.parseInt(ifrs_id), pricePerUnit);
					// check if update was ok
					if (noOfEntries > 0) {
						SessionMessages.add(request, "update_ok");
						response.setRenderParameter("mvcPath", "/html/definitions/defproductcontroller/add_product.jsp");
					} else {
						SessionErrors.add(request, "update_nok");
						response.setRenderParameter("mvcPath", "/html/definitions/defproductcontroller/add_product.jsp");
					}
				} else {
					SessionErrors.add(request, "params_nok");
					response.setRenderParameter("mvcPath", "/html/definitions/defproductcontroller/add_product.jsp");
				}
			} else {
				// check if entry already exists in the database
				// check if form parameters are ok
				if (code != null && name != null) {
					// insert entry into db
					int noOfEntries = DefProduct.insertEntry(code, lot_id, cat_id, name, flag_it, status,
							Integer.parseInt(ias_id), Integer.parseInt(ifrs_id), pricePerUnit);

					// show status to user
					if (noOfEntries > 0) {
						request.setAttribute("newProductCode", code);
						SessionMessages.add(request, "insert_ok");
					} else {
						SessionErrors.add(request, "insert_nok");
						response.setRenderParameter("mvcPath", "/html/definitions/defproductcontroller/add_product.jsp");
					}
				} else {
					SessionErrors.add(request, "params_nok");
					response.setRenderParameter("mvcPath", "/html/definitions/defproductcontroller/add_product.jsp");
				}
			}
			DefProductVo.resetAndgetInstance();
			
		} catch (Exception e) {
			SessionErrors.add(request, e.getClass().getName());
			// just for show
			SessionErrors.add(request, "error");

			response.setRenderParameter("mvcPath", "/html/definitions/defproductcontroller/add_product.jsp");
		}
	}

	@Override
	public void render(RenderRequest renderRequest,
			RenderResponse renderResponse) throws PortletException, IOException {
		HttpServletRequest request = PortalUtil.getHttpServletRequest(renderRequest);
		String p_p_id = PortalUtil.getOriginalServletRequest(request).getParameter("p_p_id");
		String path = PortalUtil.getOriginalServletRequest(request).getParameter("_" + p_p_id + "_mvcPath");

		log.info("[PROFLUO] DefProductController portlet - render()");
		log.info("[PROFLUO] Context Path: " + renderRequest.getContextPath());
		log.info("[PROFLUO] Portlet ID	: " + p_p_id);
		log.info("[PROFLUO] Portlet Path: " + path);

		if (path == null || path.endsWith("view.jsp")) {

		} else if (path.endsWith("add_product.jsp")) {
			log.info("[PROFLUO] Save Product render()");
		}

		super.render(renderRequest, renderResponse);
	}

}
