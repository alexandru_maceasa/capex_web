package com.profluo.ec.carrefour.htmlutils;

import java.util.HashMap;
import java.util.List;

public class TabelUtils {
	
	
	public static String trim(Object obj){
		String value = obj.toString();
		return value.substring(0, value.indexOf(".") + 3);
	}
	
	public static String getTableTdS(List<HashMap<String,Object>> list, int i){
		
		return "<td>" + list.get(i).get("code") + "</td>"
			+ "<td>" +list.get(i).get("denumire") + "</td>"
			+ "<td>" + trim(list.get(i).get("buget_an")) + "</td>"
			+ "<td>" + trim(list.get(i).get("actual_ian")) + "</td>"
			+ "<td>" + trim(list.get(i).get("actual_feb")) + "</td>"
			+ "<td>" + trim(list.get(i).get("actual_mar")) + "</td>"
			+ "<td>" + trim(list.get(i).get("actual_apr")) + "</td>"
			+ "<td>" + trim(list.get(i).get("actual_mai")) + "</td>"
			+ "<td>" + trim(list.get(i).get("actual_iun")) + "</td>"
			+ "<td>" + trim(list.get(i).get("actual_iul")) + "</td>"
			+ "<td>" + trim(list.get(i).get("actual_aug")) + "</td>"
			+ "<td>" + trim(list.get(i).get("actual_sept")) + "</td>"
			+ "<td>" + trim(list.get(i).get("actual_oct")) + "</td>"
			+ "<td>" + trim(list.get(i).get("actual_noi")) + "</td>"
			+ "<td>" + trim(list.get(i).get("actual_dec")) + "</td>"
			+ "<td>" + trim(list.get(i).get("total")) + "</td>"
			+ "<td>" + trim(list.get(i).get("diferenta")) + "</td>";
	}
	
	public static String getTotalTableTdS(List<HashMap<String,Object>> list, int i){
		return  "<td>" +list.get(i).get("denumire") + "</td>"
			+ "<td>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp</td>"
			+ "<td>" + trim(list.get(i).get("buget_an")) + "</td>"
			+ "<td>" + trim(list.get(i).get("actual_ian")) + "</td>"
			+ "<td>" + trim(list.get(i).get("actual_feb")) + "</td>"
			+ "<td>" + trim(list.get(i).get("actual_mar")) + "</td>"
			+ "<td>" + trim(list.get(i).get("actual_apr")) + "</td>"
			+ "<td>" + trim(list.get(i).get("actual_mai")) + "</td>"
			+ "<td>" + trim(list.get(i).get("actual_iun")) + "</td>"
			+ "<td>" + trim(list.get(i).get("actual_iul")) + "</td>"
			+ "<td>" + trim(list.get(i).get("actual_aug")) + "</td>"
			+ "<td>" + trim(list.get(i).get("actual_sept")) + "</td>"
			+ "<td>" + trim(list.get(i).get("actual_oct")) + "</td>"
			+ "<td>" + trim(list.get(i).get("actual_noi")) + "</td>"
			+ "<td>" + trim(list.get(i).get("actual_dec")) + "</td>"
			+ "<td>" + trim(list.get(i).get("total")) + "</td>"
			+ "<td>" + trim(list.get(i).get("diferenta")) + "</td>";
	}
}
