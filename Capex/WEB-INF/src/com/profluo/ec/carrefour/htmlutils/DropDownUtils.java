package com.profluo.ec.carrefour.htmlutils;

import java.util.HashMap;
import java.util.List;

public class DropDownUtils {
	
	/**
	 * 
	 * @param inputList lista de intrare
	 * @param valueCol coloana din care se vor preluat valorile necesare dropdown-ului
	 * @param namesCol coloana din care se vor prelua numele necesare dropdown-ului
	 * @param selectedValue valoare ce va si preselectata
	 * @param emptyText numele ce va fi afisat in cazul in care nu exista valoare preselectata
	 * @return
	 */
	public static String generateSimpleDropdown(List<HashMap<String, Object>> inputList, 
			String valueCol, String namesCol, String selectedValue, String emptyText) {
		boolean found = false;
		String output = "";

		for (int i = 0; i < inputList.size(); i++) {
			if (inputList.get(i).get(valueCol).toString().equals(selectedValue)) {
				output += "<option selected='selected' value='"
						+ inputList.get(i).get(valueCol).toString() + "'>"
						+ inputList.get(i).get(namesCol).toString()
						+ "</option>";
				found = true;
			} else {
				output += "<option value='"
						+ inputList.get(i).get(valueCol).toString() + "'>"
						+ inputList.get(i).get(namesCol).toString()
						+ "</option>";
			}
		}
		if (!found) {
			output = "<option value='0'>" + emptyText + "</option>" + output;
		}

		return output;
	}

	
	/**
	 * @param inputList lista de intrare
	 * @param valueCol coloanda din care se preiau valorile
	 * @param namesCol1 coloana din care se preia prima parte a numelui
	 * @param namesCol2 coloana din care se preia a doua parte a numelui
	 * @return
	 */
	public static String generateDropDownWithTwoNameColumn(List<HashMap<String, Object>> inputList, String defaultValue, 
			String valueCol, String namesCol1, String namesCol2){
		String output = "<option value='0' selected='selected'> " + defaultValue + " </option>";
		for (int i = 0; i < inputList.size(); i++) {
			output += "<option value='"+ inputList.get(i).get(valueCol).toString() + "'>"
					+ inputList.get(i).get(namesCol1).toString() + " - " + inputList.get(i).get(namesCol2).toString() 
					+ "</option>";
		}
		return output;
	}
	
	/**
	 * @param inputList lista de intrare
	 * @param valueCol coloana din care se vor prelua valorile
	 * @param namesCol1 coloana din care se va prelua prima parte a numelui
	 * @param namesCol2 coloana din care se va prelua a doua parte a numelui
	 * @param className numele clasei (prefix)
	 * @param classCol coloana din care se va prelua sufixul necesar la numele clasei
	 * @return
	 */
	public static String generateDropdownWithClassAndTwoNamesColumns(List<HashMap<String, Object>> inputList, 
			String valueCol, String namesCol1, String namesCol2, String className, String classCol, boolean hasStatus) {
		String output = "";
		for (int i = 0; i < inputList.size(); i++) {
			if (hasStatus && inputList.get(i).get("status").toString().equals("A")) {
				output += "<option value='"
						+ inputList.get(i).get(valueCol).toString() + "' class='" + className + inputList.get(i).get(classCol).toString() + "'>"
						+ inputList.get(i).get(namesCol1).toString() + " - " + inputList.get(i).get(namesCol2).toString()
						+ "</option>";
			} else {
				output += "<option value='"
						+ inputList.get(i).get(valueCol).toString() + "' class='" + className + inputList.get(i).get(classCol).toString() + "'>"
						+ inputList.get(i).get(namesCol1).toString() + " - " + inputList.get(i).get(namesCol2).toString()
						+ "</option>";
			}
		}
		return output;
	}
}
