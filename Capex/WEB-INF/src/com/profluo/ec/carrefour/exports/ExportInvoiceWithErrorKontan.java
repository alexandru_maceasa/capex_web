package com.profluo.ec.carrefour.exports;

import java.sql.SQLException;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.messaging.Message;
import com.liferay.portal.kernel.messaging.MessageListener;
import com.liferay.portal.kernel.messaging.MessageListenerException;
import com.liferay.util.bridges.mvc.MVCPortlet;
import com.profluo.ecm.exports.ExportInvoiceWithErrorToKontan;

/**
 * Portlet implementation class ExportInvoiceWithErrorKontan
 */
public class ExportInvoiceWithErrorKontan extends MVCPortlet implements MessageListener {
	
	private final Log log = LogFactoryUtil.getLog("PROFLUO-" + ExportInvoiceWithErrorKontan.class.getName());
 
	public void exportExcel(ActionRequest request, ActionResponse response) {
		try {
			ExportInvoiceWithErrorToKontan.exportInvoicesWithErrors();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public void receive(Message message) throws MessageListenerException {
		try {
			log.info("[PROFLUO] Export Invoices with error To Kontan - AUTOMATIC EVENT");
			ExportInvoiceWithErrorToKontan.exportInvoicesWithErrors();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}
}
