package com.profluo.ec.carrefour.exports;

import java.sql.SQLException;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;

import com.liferay.util.bridges.mvc.MVCPortlet;
import com.profluo.ecm.exports.ExportUnblockSupplierToKontan;

/**
 * Portlet implementation class ExportUnblockSupplier
 */
public class ExportUnblockSupplier extends MVCPortlet {
	
	public void exportExcel(ActionRequest request, ActionResponse response) {
		try {
			ExportUnblockSupplierToKontan.exportUnblockSuppliers();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

}
