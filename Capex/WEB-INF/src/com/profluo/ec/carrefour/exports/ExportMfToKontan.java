package com.profluo.ec.carrefour.exports;

import java.sql.SQLException;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;

import com.liferay.util.bridges.mvc.MVCPortlet;
import com.profluo.ecm.exports.ExportSchimbareMFtoKontan;

/**
 * Portlet implementation class ExportMfToKontan
 */
public class ExportMfToKontan extends MVCPortlet {
	
	public void exportExcel(ActionRequest request, ActionResponse response) {
		try {
			ExportSchimbareMFtoKontan.exportMFtoKontan();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
 

}
