package com.profluo.ec.carrefour.exports;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;

import javax.portlet.PortletException;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;

import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.util.bridges.mvc.MVCPortlet;
import com.profluo.ecm.model.db.InventoryHeader;
import com.profluo.ecm.utils.ExcelUtil;

/**
 * Portlet implementation class ExportOptimal
 */
public class ExportOptimal extends MVCPortlet   {

	public void serveResource(ResourceRequest resourceRequest, ResourceResponse resourceResponse) throws IOException, PortletException {
		String action = ParamUtil.getString(resourceRequest, "action");
		if (action.equals("exportExcel")) {
			System.out.println("Importing invoices into inventory................");
			InventoryHeader.importInvoicesIntoInventory();
			System.out.println("done importing invoices");
			System.out.println("Importing vouchers into inventory................");
			InventoryHeader.importVouchersIntoInventory();
			System.out.println("done importing vouchers");
			System.out.println("Importing working clothes into inventory................");
			InventoryHeader.importWorkingClothesIntoInventory();
			System.out.println("done importing working clothes");
			System.out.println("Importing deconturi");
			InventoryHeader.importCashExp();
			System.out.println("done importing deconturi");
			
			int company =  ParamUtil.getInteger(resourceRequest, "company");
			String exportType = ParamUtil.getString(resourceRequest, "export_type");
			
			if(exportType.equals("exportDocumente")){
				//incarca lista cu toate documentele corespunzatoare societatii introsduse
				List<HashMap<String, Object>> allDocsByCompany = InventoryHeader.getInventoryByCompany(company);
				String[][] header;
//				if(company == 1 || company == 7 || company == 8 || company == 2 ) {
					header = new String[36][];
					for (int i = 0; i < 36; i++) {
						header[i] = new String[2];
					}
//				} else {
//					header = new String[29][];
//					for (int i = 0; i < 29; i++) {
//						header[i] = new String[2];
//					}
//				}
				header[0][0] = "docNo";
				header[0][1] = "Numar Document";
				header[1][0] = "dataDoc";
				header[1][1] = "Data Document";
				header[2][0] = "codSocietate";
				header[2][1] = "Cod societate";
				header[3][0] = "codMagazin";
				header[3][1] = "Cod centru de cost";
				header[4][0] = "raion";
				header[4][1] = "Raion";
				header[5][0] = "codFurnizor";
				header[5][1] = "Cod furnizor";
				header[6][0] = "denumire";
				header[6][1] = "Denumire";
				header[7][0] = "produs";
				header[7][1] = "Produs";
				header[8][0] = "codProdus";
				header[8][1] = "Cod produs";
				header[9][0] = "valAchizite";
				header[9][1] = "Valoare achizitie";
				header[10][0] = "tipInregistrare";
				header[10][1] = "Tip inregistrare";
				header[11][0] = "actiuneMF";
				header[11][1] = "Actiune MF";
				header[12][0] = "gestiune";
				header[12][1] = "Gestiune";
				header[13][0] = "clasificareIT";
				header[13][1] = "Clasificare";
				header[14][0] = "nrInventar";
				header[14][1] = "Numar inventar";
				header[15][0] = "grupaRAS";
				header[15][1] = "Cod grupa clasificare";
				header[16][0] = "grupaIFRS";
				header[16][1] = "Cod grupa clasificare IFRS";
				header[17][0] = "durataRAS";
				header[17][1] = "Durata amortizare";
				header[18][0] = "durataIFRS";
				header[18][1] = "Durata amortizare IFRS";
				header[19][0] = "dataPIF";
				header[19][1] = "Data PIF";
				header[20][0] = "numeInitiativa";
				header[20][1] = "Initiativa";
				header[21][0] = "createdDate";
				header[21][1] = "Data de creeare";
				header[22][0] = "dpi";
				header[22][1] = "DPI";
				header[23][0] = "numarContract";
				header[23][1] = "Numar contract";
				header[24][0] = "contIas";
				header[24][1] = "cont";
				header[25][0] = "codeLot";
				header[25][1] = "Codul lotului";
//				header[26][0] = "lotName";
//				header[26][1] = "Denumirea lotului";
				header[26][0] = "type";
				header[26][1] = "Tip document";
					
//				if(company == 1 || company == 7 || company == 8 || company == 2 ) {
					header[27][0] = "dataScadenta";
					header[27][1] = "Data scadenta";
					header[28][0] = "procentGarantie";
					header[28][1] = "Procent garantie";
					header[29][0] = "cotaTva";
					header[29][1] = "cota TVA";
					header[30][0] = "valCuTva";
					header[30][1] = "Valoare cu TVA";
					header[31][0] = "vendorAccount";
					header[31][1] = "Vendor Reconciliation Account";
					header[32][0] = "moneda";
					header[32][1] = "Moneda";
					header[33][0] = "rataSchimb";
					header[33][1] = "Rata de schimb";
					header[34][0] = "JurnalKontan";
					header[34][1] = "Jurnal Kontan";
					header[35][0] = "VATCodeSAP";
					header[35][1] = "VAT Code SAP";	

//				}
				
				try {
					if(allDocsByCompany.size() > 0 ){
						ExcelUtil.getExcel(allDocsByCompany, header, allDocsByCompany.get(0).get("codSocietate").toString(), resourceResponse);
					} else {
						ExcelUtil.getExcel(allDocsByCompany, header, "Raport_documente", resourceResponse);
					}
				} catch (IOException e) {
					e.printStackTrace();
				}
					
				System.out.println("Updating inventory stage............");
				InventoryHeader.updateInventoryStage(company);
				System.out.println("Done updating");
				
				System.out.println("Updating documents optimal status..........");
				InventoryHeader.updateDocOptimalStatus(company);
				System.out.println("Done updating");
			}
		}
	}

	/*
	private void importDocs() {
		System.out.println("Importing invoices into inventory................");
		InventoryHeader.importInvoicesIntoInventory();
		System.out.println("done importing invoices");
		System.out.println("Importing vouchers into inventory................");
		InventoryHeader.importVouchersIntoInventory();
		System.out.println("done importing vouchers");
		System.out.println("Importing working clothes into inventory................");
		InventoryHeader.importWorkingClothesIntoInventory();
		System.out.println("done importing working clothes");
		System.out.println("Importing deconturi");
		InventoryHeader.importCashExp();
		System.out.println("done importing deconturi");
	}*/
}
