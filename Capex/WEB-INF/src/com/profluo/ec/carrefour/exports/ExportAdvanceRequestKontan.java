package com.profluo.ec.carrefour.exports;

import java.sql.SQLException;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;

import com.liferay.util.bridges.mvc.MVCPortlet;
import com.profluo.ecm.exports.ExportAdvanceRequestToKontan;

/**
 * Portlet implementation class ExportAdvanceRequestKontan
 */
public class ExportAdvanceRequestKontan extends MVCPortlet {
	
	public void exportExcel(ActionRequest request, ActionResponse response) {
		try {
			ExportAdvanceRequestToKontan.exportAdvanceRequests();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

}
