package com.profluo.ec.carrefour.portlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.List;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletException;
import javax.portlet.PortletPreferences;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;
import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.model.User;
import com.liferay.portal.service.UserServiceUtil;
import com.liferay.portal.theme.ThemeDisplay;
import com.liferay.portal.util.PortalUtil;
import com.liferay.portlet.PortletPreferencesFactoryUtil;
import com.liferay.util.bridges.mvc.MVCPortlet;
import com.profluo.ecm.controller.ControllerUtils;
import com.profluo.ecm.flows.NotificationTemplates;
import com.profluo.ecm.model.db.DatabaseConnectionManager;
import com.profluo.ecm.model.db.InvoiceApprovals;
import com.profluo.ecm.model.db.InvoiceHeader;

/**
 * Portlet implementation class AdministrareControlorGestiune
 */
public class AdministrareControlorGestiune extends MVCPortlet {

	/*
	 * Generic log instance.
	 */
	private final Log log = LogFactoryUtil.getLog("PROFLUO-" + MVCPortlet.class.getName());
	
	/**
	 * Getting information about portlet initialization.
	 */
   @Override
   public void init() throws PortletException {
	   	log.info("[PROFLUO] Initialize Furnizori blocati la plata portlet.");
	   	super.init();
   }
   
   
   /**
    * Default AJAX method in the application framework.
    */
   @SuppressWarnings("unchecked")
@Override
   public void serveResource(ResourceRequest resourceRequest, ResourceResponse resourceResponse) throws IOException, PortletException {
	   	log.info("[AJAX] Furnizori blocati la plata Controller");
	
	   	String action = ParamUtil.getString(resourceRequest, "action");
	   	log.info("[AJAX] action: " + action);
	   	PrintWriter writer = resourceResponse.getWriter();
	
	   	if (action.equals("get")) {
	   		String id = ParamUtil.getString(resourceRequest, "id");
	   		// get all associated invoices
	   		List<HashMap<String,Object>> associatedInvoices = null;
			associatedInvoices = InvoiceHeader.getAssociatedInvoices(id);
	    	JSONArray jsonInvoices = new JSONArray();
	    	if (associatedInvoices != null) {
	    		jsonInvoices = DatabaseConnectionManager.convertListToJson(associatedInvoices);
	    	}
	    	
	    	// create JSON response
	    	JSONObject jsonResponse = new JSONObject();
			jsonResponse.put("values", jsonInvoices);
			// send AJAX response
			writer.print(jsonResponse.toJSONString());
	   	}
	   	//super.serveResource(resourceRequest, resourceResponse);
   } 
   
   
   /**
	 * 
	 */
	@Override
	public void render(RenderRequest renderRequest, RenderResponse renderResponse) throws PortletException, IOException {
		HttpServletRequest request = PortalUtil.getHttpServletRequest(renderRequest);
	   	String p_p_id = PortalUtil.getOriginalServletRequest(request).getParameter("p_p_id");
	   	String path = PortalUtil.getOriginalServletRequest(request).getParameter("_" + p_p_id + "_mvcPath");
	   	try {
	    	ThemeDisplay themeDisplay = ((ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY));
	    	String portletId = themeDisplay.getPortletDisplay().getId();
	    	PortletPreferences portletSetup = PortletPreferencesFactoryUtil.getLayoutPortletSetup(themeDisplay.getLayout(), portletId);
	    	String portletCustomTitle = themeDisplay.getPortletDisplay().getTitle();
	    	portletCustomTitle = portletSetup.getValue("portletSetupTitle_" + themeDisplay.getLanguageId(), portletCustomTitle);
	    	User user = UserServiceUtil.getUserById(Long.parseLong(renderRequest.getRemoteUser()));
	    	
	        ThemeDisplay td  = (ThemeDisplay) renderRequest.getAttribute(WebKeys.THEME_DISPLAY);
	        log.info("[PROFLUO] Invoices portlet - render()");
	        log.info("[PROFLUO] User full name: " + td.getUser().getFullName());
	        log.info("[PROFLUO] Portlet Title: " + portletCustomTitle);
	    	
	    	int id_user 			= (int) user.getUserId();
	    	log.info("[PROFLUO] User id " + id_user);
	    	
	    	if (path == null || path.equals("view.jsp")) {
	    		log.info("[PROFLUO] LISTING Furnizori blocati la plata render()");
	    	}
   	} catch (Exception e) {
   		e.printStackTrace();
   	}
   	
	    super.render(renderRequest, renderResponse);
	} 

	
	public void updateStores(ActionRequest request, ActionResponse response) {
		String id =request.getParameter("id");
		System.out.println(id);
		int len = id.indexOf("--");
		String idUser = id.substring(0, len);
		String magazine = id.substring(len+2, id.length() - idUser.length()+5);
		System.out.println("id-ul de utilizator:"+idUser);
		System.out.println("magazinele:"+magazine);
		String[] mag = magazine.split(",");
		if (magazine.equals("")){
			InvoiceHeader.deleteCG(idUser);
		}
		if (!magazine.equals("")){
			InvoiceHeader.deleteCG(idUser);
			for (String m : mag){
				  System.out.println(m.trim());
				  InvoiceHeader.insertCG(idUser, m.trim());
				}
			}
		}

}
