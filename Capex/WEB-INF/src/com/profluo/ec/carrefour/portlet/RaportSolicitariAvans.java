package com.profluo.ec.carrefour.portlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.List;

import javax.portlet.PortletException;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.util.bridges.mvc.MVCPortlet;
import com.profluo.ecm.controller.ControllerUtils;
import com.profluo.ecm.model.db.DatabaseConnectionManager;
import com.profluo.ecm.model.db.RaportSolicitariAvansBd;
import com.profluo.ecm.utils.ExcelUtil;

/**
 * Portlet implementation class RaportSolicitariAvans
 */
public class RaportSolicitariAvans extends MVCPortlet {
 
	@SuppressWarnings("unchecked")
	public void serveResource(ResourceRequest resourceRequest, ResourceResponse resourceResponse) throws IOException, PortletException {
		
		String action = ParamUtil.getString(resourceRequest, "action");
		if (action.equals("filter") || action.equals("next") || action.equals("prev") || action.equals("last") || action.equals("first")) {
			PrintWriter writer = resourceResponse.getWriter();
			int start = Integer.parseInt(ParamUtil.getString(resourceRequest, "start").toString());
			int count = Integer.parseInt(ParamUtil.getString(resourceRequest, "count").toString());
			int total = Integer.parseInt(ParamUtil.getString(resourceRequest, "total").toString());
			String societate = ParamUtil.getString(resourceRequest, "societate").toString();
			String magazin = ParamUtil.getString(resourceRequest, "magazin");
			int supplier = Integer.parseInt(ParamUtil.getString(resourceRequest, "supplier_id").toString());
			int fromYear = Integer.parseInt(ParamUtil.getString(resourceRequest, "from_year").toString());
			int toYear = Integer.parseInt(ParamUtil.getString(resourceRequest, "to_year").toString());
			int fromMonth = Integer.parseInt(ParamUtil.getString(resourceRequest, "from_month").toString());
			int toMonth = Integer.parseInt(ParamUtil.getString(resourceRequest, "to_month").toString());

			List<HashMap<String, Object>> allLines = null;

			allLines = RaportSolicitariAvansBd.getAllInfo(start, count, societate, magazin, supplier, fromYear, toYear, fromMonth, toMonth);
			total = RaportSolicitariAvansBd.getAllInfoCount(societate, magazin, supplier, fromYear, toYear, fromMonth, toMonth);

			JSONArray jsonLines = new JSONArray();
			if (allLines != null) {
				jsonLines = DatabaseConnectionManager.convertListToJson(allLines);
			}
			JSONObject jsonResponse = new JSONObject();

			jsonResponse.put("action", action);
			jsonResponse.put("start", start);
			jsonResponse.put("count", count);
			jsonResponse.put("total", total);
			jsonResponse.put("values", jsonLines);
			writer.print(jsonResponse.toJSONString());
		} else if (action.equals("findSupplier")) {
			PrintWriter writer = resourceResponse.getWriter();
			String query = ParamUtil.getString(resourceRequest, "keywords");
			String idCompany = ParamUtil.getString(resourceRequest, "id_company");

			writer.print(ControllerUtils.filterSupplierByName(query, Integer.parseInt(idCompany)).toJSONString());
		} else if(action.equals("exportRaport")) {
			String societate = ParamUtil.getString(resourceRequest, "company_id").toString();
			String magazin = ParamUtil.getString(resourceRequest, "id_magazin").toString();
			int supplierId = ParamUtil.getInteger(resourceRequest, "supplier_id");
			int fromYear = ParamUtil.getInteger(resourceRequest, "from_year");
			int toYear = ParamUtil.getInteger(resourceRequest, "to_year");
			int fromMonth =  ParamUtil.getInteger(resourceRequest, "from_month");
			int toMonth =  ParamUtil.getInteger(resourceRequest, "to_month");
			
			List<HashMap<String, Object>> allLines = RaportSolicitariAvansBd.getAllInfo(0, 5000000, societate, magazin, supplierId, fromYear, toYear, fromMonth, toMonth);
			String[][] header = new String[17][];
			for (int i = 0; i < 17; i++) {
				header[i] = new String[2];
			}
			header[0][0] = "nr_cerere";
			header[0][1] = "Numar cerere avans";
			header[1][0] = "solicitant";
			header[1][1] = "Solicitant avans";
			header[2][0] = "department";
			header[2][1] = "Magazin/Departament";
			header[3][0] = "societate";
			header[3][1] = "Societate";
			header[4][0] = "magazin";
			header[4][1] = "Magazin";
			header[5][0] = "codFurnizor";
			header[5][1] = "Cod furnizor";
			header[6][0] = "numeFurnizor";
			header[6][1] = "Nume furnizor";
			header[7][0] = "tipSolicitare";
			header[7][1] = "Tip document";
			header[8][0] = "facturi";
			header[8][1] = "Numar document";
			header[9][0] = "valoareRON";
			header[9][1] = "Valoare RON";
			header[10][0] = "valoareCURR";
			header[10][1] = "Valoare EURO";
			header[11][0] = "moneda";
			header[11][1] = "Moneda";
			header[12][0] = "detalii";
			header[12][1] = "Explicatii avans";
			header[13][0] = "validation_date";
			header[13][1] = "Data validare";
			header[14][0] = "stare";
			header[14][1] = "Status avans";
			header[15][0] = "data_inchidere";
			header[15][1] = "Data inchidere";
			header[16][0] = "doc_inchidere";
			header[16][1] = "Numar document inchidere avans";
			
			ExcelUtil.getExcel(allLines, header, "Raport_Solicitari_Avans", resourceResponse);
		}
		
	}
}
