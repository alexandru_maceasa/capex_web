package com.profluo.ec.carrefour.portlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletConfig;
import javax.portlet.PortletException;
import javax.portlet.PortletPreferences;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;
import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.portlet.LiferayPortletConfig;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.servlet.SessionMessages;
import com.liferay.portal.kernel.util.JavaConstants;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.model.User;
import com.liferay.portal.service.UserLocalServiceUtil;
import com.liferay.portal.service.UserServiceUtil;
import com.liferay.portal.theme.ThemeDisplay;
import com.liferay.portal.util.PortalUtil;
import com.liferay.portlet.PortletPreferencesFactoryUtil;
import com.liferay.util.bridges.mvc.MVCPortlet;
import com.profluo.ec.carrefour.htmlutils.DropDownUtils;
import com.profluo.ecm.controller.ControllerUtils;
import com.profluo.ecm.controller.InventoryNumberBO;
import com.profluo.ecm.controller.UserTeamIdUtils;
import com.profluo.ecm.flows.InvoiceStages;
import com.profluo.ecm.flows.Links;
import com.profluo.ecm.flows.NotificationTemplates;
import com.profluo.ecm.flows.NotificationsSubject;
import com.profluo.ecm.flows.SupplierStages;
import com.profluo.ecm.model.db.AdvanceRequest;
import com.profluo.ecm.model.db.DPIHeader;
import com.profluo.ecm.model.db.DatabaseConnectionManager;
import com.profluo.ecm.model.db.DefCompanies;
import com.profluo.ecm.model.db.DefExchRates;
import com.profluo.ecm.model.db.DefLot;
import com.profluo.ecm.model.db.DefStore;
import com.profluo.ecm.model.db.DefSupplier;
import com.profluo.ecm.model.db.GeneralCodes;
import com.profluo.ecm.model.db.GenericDBStatements;
import com.profluo.ecm.model.db.InvoiceHeader;
import com.profluo.ecm.model.db.InvoiceHistory;
import com.profluo.ecm.model.db.InvoiceSequence;
import com.profluo.ecm.model.db.UpdateChGenInvoice;
import com.profluo.ecm.model.db.UsersInformations;
import com.profluo.ecm.model.vo.DefAisles;
import com.profluo.ecm.model.vo.DefIASVo;
import com.profluo.ecm.model.vo.DefIFRSVo;
import com.profluo.ecm.model.vo.DefNewProjVo;
import com.profluo.ecm.model.vo.DefProductVo;
import com.profluo.ecm.notifications.Notifications;
/**
 * Portlet implementation class Facturi
 */
public class Invoices extends MVCPortlet {
	/**
	 * link from the current page
	 */
	private String currentURL;
	/**
	 * Generic log instance.
	 */
	private final Log log = LogFactoryUtil.getLog("PROFLUO-" + Invoices.class.getName());

	/**
	 * True if an invoice line is prorata
	 */
	boolean isProrata = false;
	/**
	 * true if the invoice line has services
	 */
	boolean hasServices = false;
	/**
	 * True if the invoice has new project
	 */
	boolean isNewProject = false;
	
	/**
	 * True if the invoice has initiative
	 */
	boolean isInitiative = false;
	/**
	 * Users that must be notified when an invoice was registered with success
	 */
	List<HashMap<String, Object>> approvers = new ArrayList<HashMap<String,Object>>();
	
	
	/**
	 * List of all columns that in the database are varchar or text
	 */
	ArrayList<String> columnsWithDefaultValueString = new ArrayList<String>();
	
	/**
	 * List of all columns that in the database are numeric
	 */
	ArrayList<String> columnsWithDefaultValueNumeric= new ArrayList<String>();
	
	
	/**
	 * True if the invoice has missing documents
	 */
	public boolean invoiceWithMissingDoc = false;

	/**
	 * List of parameters to be excluded from SQL lines insert.
	 */
	private ArrayList<String> lineExcludes = null;

	/**
	 * List of parameters to be excluded from SQL store lines insert.
	 */
	private ArrayList<String> lineStoresExcludes = null;

	/**
	 * Array to store the line numbers for the store lines processing.
	 */
	private String[] lineNumber = null;
	/**
	 * 
	 */
	private int supplier_code = 0;
	/**
	 * List of parameters to be excluded from SQL suppliers insert.
	 */
	private ArrayList<String> suppliersExcludes = null;

	/**
	 * Sets invoiceWithMissingDocs attribute when on Invoice() object is created from another
	 * class
	 * @param bol
	 */
	public void setMissingDocs(boolean bol){
		invoiceWithMissingDoc = bol;
	}
	
	/**
	 * Sets isNewProject attribute when on Invoice() object is created from another
	 * class
	 * @param bol
	 */
	public void setNewProject(boolean bol){
		isNewProject = bol;
	}
	
	/**
	 * Sets isNewProject attribute when on Invoice() object is created from another
	 * class
	 * @param bol
	 */
	public void setIsInitiative(boolean bol){
		isInitiative = bol;
	}
	
	public void deleteFact(ActionRequest actionRequest, ActionResponse actionResponse) {

		String id = actionRequest.getParameter("fact_id_delete");
		String[][] a = new String[1][2];
		a[0][0] = "status";
		a[0][1] = "I";

		ArrayList<String> columns = new ArrayList<String>();
		columns.add("id");
		ArrayList<String> values = new ArrayList<String>();
		values.add(id);
		
		System.out.println("ID -> " + id);

		GenericDBStatements.updateEntry(a, "invoice_header", new ArrayList<String>(), columns, values);

		//UpdateChGenInvoice.updateInvoiceChGen(Integer.parseInt(id));
		
		SessionMessages.add(actionRequest, "delete_ok");

	}

	/**
	 * Returns the index of column in the array of line columns
	 * 
	 * @param row
	 * @return
	 */
	public int getColumnIndex(String[][] row, String column) {
		for (int i = 0; i < row.length; i++) {
			if (row[i][0].equals(column)) {
				return i;
			}
		}
		return 0;
	}

	/**
	 * 
	 * @param line
	 *            : one invoice line
	 * @param storeLine
	 *            :one invoice store line
	 * @return true if the invoice store line belongs to the invoice line
	 */
	private boolean checkIfStoreLineBelongsToLine(String[][] line, String[][] storeLine) {
		// iterate through all line columns
		boolean result = false;
		for (int i = 0; i < line.length; i++) {
			// find the line number
			if (line[i][0].equals("line")) {
				// iterate through all store lines columns
				for (int j = 0; j < storeLine.length; j++) {
					/*
					 * find the associated line number the column "nr" in every
					 * store line contains the associated line number
					 */
					if (storeLine[j][0].equals("nr")) {
						// if storeLine belongs to line return true
						if (line[i][1].equals(storeLine[j][1])) {
							result = true;
						}
					}
				}
			}
		}
		return result;
	}

	/**
	 * Sets all the columns that are ignored when the invoice line is inserted into the database
	 */
	private void setLinesExcludes() {
		// attributes to be ignored for table invoice_line
		ArrayList<String> excludes = new ArrayList<String>();
		String[] items = {"id", "detalii", "clasif", "lot", "val_cu_tva", "lot_warranty", "counter", "cont_imob_in_fct",
				"cont_imob_in_curs", "cont_imob_avans", "invoice_line_id", "delete_line", "grupa_ras", "grupa_ifrs" };
		for (int i = 0; i < items.length; i++) {
			excludes.add(items[i]);
		}
		this.lineExcludes = excludes;
	}

	/**
	 * 
	 */
	private void setSupplierExcludes() {
		// attributes to be ignored for table invoice_line
		ArrayList<String> excludes = new ArrayList<String>();
		String[] items = {};
		for (int i = 0; i < items.length; i++) {
			excludes.add(items[i]);
		}
		this.suppliersExcludes = excludes;
	}

	/**
	 * Set list of parameters to be excluded from the invoice store lines sql
	 * statement update.
	 */
	private void setStoreLineExcludes() {
		// attributes to be ignored for table invoice_store_line
		ArrayList<String> excludesForStoreLines = new ArrayList<String>();
		String[] itemsStoreLines = { "id_store_line", "nr", "inventory", "delete_line",
				"vizualizare_dpi", "detalii", "index", "unit_price",
				"cont_imob_in_fct", "cont_imob_in_curs", "cont_imob_avans",
				"id_store_1", "id_store_2", "id_store_3", "id_store_4",
				"id_store_5", "id_store_6", "id_store_7", "id_store_8", "id_store_9" };
		for (int i = 0; i < itemsStoreLines.length; i++) {
			excludesForStoreLines.add(itemsStoreLines[i]);
		}

		this.lineStoresExcludes = excludesForStoreLines;
	}


	/**
	 * Prepare the sql array in order to insert the invoice lines in the DB.
	 * 
	 * @param request
	 * @param response
	 * @param lineData
	 * @param arrFields
	 * @param invNo
	 * @return
	 */
	private String[][][] getSQLInvoiceLines(ActionRequest request, ActionResponse response, String[][][] lineData, String[][] arrFields, int invNo) {
		for (int i = 0; i < lineData.length; i++) {
			isProrata = false;
			String unit_price_curr = "";
			String price_no_vat_curr = "";
			String price_with_vat_curr = "";
			String price_vat_curr = "";
			this.lineNumber = new String[lineData.length];
			for (int j = 0; j < lineData[i].length; j++) {
				if (lineData[i][j][0].equals("description")) {
					if (lineData[i][j][1].toLowerCase().contains("prorata")){
						isProrata = true;
					} else {
						isProrata = false;
					}
					if(GeneralCodes.getServices().contains(lineData[i][j][1].toLowerCase())){
						hasServices = true;
					} else {
						hasServices = false;
					}
				}
			}
			System.out.println("Prorata : " + isProrata);
			// get values from table
			for (int j = 0; j < lineData[i].length; j++) {
				if (lineData[i][j][0].equals("unit_price_curr")) {
					unit_price_curr = lineData[i][j][1];
//					if (isProrata){
//						unit_price_curr = "0";
//					}
				} else if (lineData[i][j][0].equals("price_no_vat_curr")) {
					price_no_vat_curr = lineData[i][j][1];
				} else if (lineData[i][j][0].equals("val_cu_tva")) {
					price_with_vat_curr = lineData[i][j][1];
				} else if (lineData[i][j][0].equals("line")) {
					lineNumber[i] = lineData[i][j][1];
					// System.out.println("LineData : "+lineData[i][j][1]);
					// System.out.println("I este :" + i);
				}
			}
			price_vat_curr = ((Float) (Float.parseFloat(price_with_vat_curr) - Float.parseFloat(price_no_vat_curr))).toString();
			// set values for the columns that are in invoice_line but not in
			// the listing
			for (int j = 0; j < lineData[i].length; j++) {
				if (lineData[i][j][0].equals("inv_number")) {
					lineData[i][j][1] = ((Integer) invNo).toString();
				} else if (lineData[i][j][0].equals("currency")) {
					lineData[i][j][1] = arrFields[17][1];
				} else if (lineData[i][j][0].equals("unit_price_ron")) {
					if (arrFields[17][1].equals("RON")) {
						lineData[i][j][1] = unit_price_curr;
					} else {
						if (!isProrata){
							lineData[i][j][1] = ControllerUtils.fromCurrToRon(Float.parseFloat(unit_price_curr), Float.parseFloat(arrFields[18][1]));
						} else {
							lineData[i][j][1] = unit_price_curr;
						}
					}
				} else if (lineData[i][j][0].equals("price_no_vat_ron")) {
					if (arrFields[17][1].equals("RON")) {
						lineData[i][j][1] = price_no_vat_curr;
					} else {
						lineData[i][j][1] = ControllerUtils.fromCurrToRon(Float.parseFloat(price_no_vat_curr), Float.parseFloat(arrFields[18][1]));
					}
				} else if (lineData[i][j][0].equals("price_vat_ron")) {
					if (arrFields[17][1].equals("RON")) {
						lineData[i][j][1] = price_vat_curr;
					} else {
						lineData[i][j][1] = ControllerUtils.fromCurrToRon(Float.parseFloat(price_vat_curr), Float.parseFloat(arrFields[18][1]));
					}
				} else if (lineData[i][j][0].equals("price_vat_curr")) {
					lineData[i][j][1] = price_vat_curr;
				} else if (lineData[i][j][0].equals("unit_price_curr") && isProrata) {
					lineData[i][j][1] = unit_price_curr;
				} 
//			else if (lineData[i][j][0].equals("quantity") && isProrata) {
//				lineData[i][j][1] = "0";
//			} else if (lineData[i][j][0].equals("product_code") && isProrata) {
//				lineData[i][j][1] = "0";
//			}
			}
		}

		return lineData;
	}

	/**
	 * Prepare the sql array to insert the invoice header in the DB.
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	private String[][] getSqlHeaderInvoiceParams(ActionRequest request, ActionResponse response, String action, String currentStage) {
		//String page = request.getParameter("page").toString();
		User user = null;
		try {
			user = UserServiceUtil.getUserById(Long.parseLong(request.getRemoteUser()));
		} catch (Exception e) {
			e.printStackTrace();
		}

		//String currentStage = request.getParameter("currentStage").toString();
		// read request parameters
		int arrSize = 40;
		String[][] arrFields = new String[arrSize][2];

		for (int i = 0; i < arrSize; i++) {
			arrFields[i] = new String[2];
		}

		arrFields[0][0] = "inv_number";
		arrFields[0][1] = request.getParameter("inv_number").toString();
		// String inv_date = request.getParameter("inv_date2").toString();
		arrFields[1][0] = "inv_date";
		arrFields[1][1] = request.getParameter("inv_date2").toString();
		arrFields[2][0] = "due_date";
		arrFields[2][1] = request.getParameter("datascadenta").toString();
		arrFields[3][0] = "tip_fact";
		arrFields[3][1] = request.getParameter("tip-factura").toString();
		arrFields[4][0] = "tip_rel_com";
		arrFields[4][1] = request.getParameter("tip-rel-comerciala").toString();
		arrFields[5][0] = "id_parent";

		// daca tip factura = de corectie
		if (arrFields[3][1].equals("2")) {
			arrFields[5][1] = request.getParameter("nr-fact-baza").toString();
		} else {
			arrFields[5][1] = "0";
		}
		arrFields[6][0] = "id_company";
		arrFields[6][1] = request.getParameter("company").toString();
		arrFields[7][0] = "id_supplier";
		arrFields[7][1] = request.getParameter("id_supplier").toString();
		arrFields[8][0] = "co_number";
		arrFields[8][1] = request.getParameter("nr-contract").toString();
		arrFields[9][0] = "order_no";
		arrFields[9][1] = request.getParameter("nr-comanda").toString();
		
		//Valoare fara TVA
		arrFields[10][0] = "total_no_vat_ron";
		String val_fara_TVA = request.getParameter("val_no_vat_ron").toString();
		if ( val_fara_TVA.contains(",")){
			val_fara_TVA = val_fara_TVA.replace(",", "");
		}
		arrFields[10][1] = val_fara_TVA.toString();
		
		//Valoare TVA
		arrFields[11][0] = "total_vat_ron";
		String val_TVA = request.getParameter("val_vat_ron").toString();
		if ( val_TVA.contains(",")){
			val_TVA = val_TVA.replace(",", "");
		}
		arrFields[11][1] = val_TVA.toString();
		
		//Valoare cu TVA
		arrFields[12][0] = "total_with_vat_ron";
		String val_cu_TVA = request.getParameter("val_with_vat_ron").toString();
		if ( val_cu_TVA.contains(",")){
			val_cu_TVA = val_cu_TVA.replace(",", "");
		}
		arrFields[12][1] = val_cu_TVA.toString();
		
		arrFields[13][0] = "total_no_vat_curr";
		arrFields[13][1] = "0";
		arrFields[14][0] = "total_vat_curr";
		arrFields[14][1] = "0";
		arrFields[15][0] = "total_with_vat_curr";
		arrFields[15][1] = "0";
		arrFields[16][0] = "warranty_proc";
		arrFields[16][1] = (request.getParameter("procent_garantie").toString().equals("") ? "0" : request.getParameter("procent_garantie").toString());
		arrFields[17][0] = "currency";
		arrFields[17][1] = request.getParameter("currency").toString();
		if (arrFields[17][1].equals("RON")) {
			arrFields[13][1] = arrFields[10][1];
			arrFields[14][1] = arrFields[11][1];
			arrFields[15][1] = arrFields[12][1];
		} else {
			String val_no_vat_curr = request.getParameter("val_no_vat_curr").toString();
			if (val_no_vat_curr.contains(",")){
				val_no_vat_curr = val_no_vat_curr.replace(",", "");
			}
			arrFields[13][1] =val_no_vat_curr.toString();
			String val_vat_curr = request.getParameter("val_vat_curr").toString();;
			if (val_vat_curr.contains(",")){
				val_vat_curr = val_vat_curr.replace(",", "");
			}
			arrFields[14][1] = val_vat_curr.toString();
			String val_with_vat_curr = request.getParameter("val_with_vat_curr").toString();
			if (val_with_vat_curr.contains(",")){
				val_with_vat_curr = val_with_vat_curr.replace(",", "");
			}
			arrFields[15][1] = val_with_vat_curr.toString();
		}
		arrFields[18][0] = "exchange_rate";
		arrFields[18][1] = request.getParameter("rata-schimb").toString();
		arrFields[19][0] = "gen_prov";
		arrFields[19][1] = "";
		arrFields[20][0] = "payment_lock";
		String blocat_la_plata = request.getParameter("blocat-la-plata").toString();
		if (blocat_la_plata.equals("1")) {
			arrFields[20][1] = "Y";
		} else
			arrFields[20][1] = "N";

		arrFields[21][0] = "id_fact_avans";
		String fact_avans_asoc = request.getParameter("fact-avans-asoc").toString();
		if (fact_avans_asoc.equals("1")) {
			arrFields[21][1] = request.getParameter("nr-fact-avans-asoc").toString();
		} else
			arrFields[21][1] = "0";

		arrFields[22][0] = "warranty_lock";
		String garantie_blocata = request.getParameter("garantie-blocata").toString();
		if (garantie_blocata.equals("1")) {
			arrFields[22][1] = "Y";
		} else
			arrFields[22][1] = "N";

		arrFields[23][0] = "missing_docs";
		String lipsa_doc = request.getParameter("lipsa-doc").toString();
		if (lipsa_doc.equals("1")) {
			arrFields[23][1] = request.getParameter("missing_docs").toString();
			if (arrFields[23][1].length() > 1 && arrFields[23][1].substring(0, 1).equals(",")) {
				arrFields[23][1] = arrFields[23][1].substring(1, arrFields[23][1].length());
			}
			invoiceWithMissingDoc = true;
		} else {
			arrFields[23][1] = "";
			invoiceWithMissingDoc = false;
		}

		arrFields[24][0] = "error_details";
		String marcheaza_fact_eronata = request.getParameter("marcheaza-fact-eronata").toString();
		if (marcheaza_fact_eronata.equals("1")) {
			arrFields[24][1] = request.getParameter("detalii-eroare").toString();
		} else {
			arrFields[24][1] = "";
		}
		arrFields[25][0] = "payment_value_ron";
		arrFields[25][1] = request.getParameter("val-netaplata-ron").replace(",", "").toString();
		arrFields[26][0] = "payment_value_curr";
		if (arrFields[17][1].equals("RON")) {
			arrFields[26][1] = arrFields[25][1];
		} else {
			arrFields[26][1] = request.getParameter("val-netaplata-eur").replace(",", "").toString();
		}

		arrFields[27][0] = "tip_capex";
		arrFields[27][1] = request.getParameter("categ_capex").toString();
		System.out.println("categ capex : " + arrFields[27][1]);
		arrFields[28][0] = "id_new_prj";
		// categoria capex este proiect nou
		if (arrFields[27][1].equals("2")) {
			String proiect = request.getParameter("project").toString();
			if ( proiect == "" ){
				proiect = "0";
			}
			arrFields[28][1] = proiect;
			isNewProject = true;
		} else {
			arrFields[28][1] = "0";
			isNewProject = false;
		}
		arrFields[29][0] = "id_initiative";
		// categoria capex este lista initiativa
		if (arrFields[27][1].equals("1")) {
			String initiativa = request.getParameter("lista_initiative").toString();
			if ( initiativa == "" ){
				initiativa = "0";
			}
			arrFields[29][1] = initiativa;
			isInitiative = true;
		} else {
			arrFields[29][1] = "0";
			isInitiative = false;
		}
        //calculate next stage
        arrFields[30][0] = "stage";
        arrFields[30][1] = InvoiceStages.calculateNextStage(arrFields);
        
        Calendar calendar = Calendar.getInstance();
	    java.sql.Timestamp currentTS = new java.sql.Timestamp(calendar.getTime().getTime());
	    
        arrFields[31][0] = "modified";
        arrFields[31][1] = currentTS.toString();
        arrFields[32][0] = "id_user";
        String currentUser = InvoiceHeader.getCurrentUser(action);
        if(!currentUser.equals("") && !currentUser.equals("0")) {
        	 arrFields[32][1] = currentUser;
        } else {
        	 arrFields[32][1] = String.valueOf(user.getUserId());
        }
        arrFields[33][0] = "details_aditional";
        arrFields[33][1] = "";
        
        arrFields[34][0] = "reg_date";
        System.out.println("De pe aici current stage = " + currentStage);
        if(currentStage.equals("0") || currentStage.equals("1") || currentStage.equals("")) {
        	arrFields[34][1] = currentTS.toString();
        } else {
        	arrFields[34][1] = InvoiceHeader.getRegDate(action);
        }
        
        arrFields[35][0] = "kontan_acc";
        
        String store_code = request.getParameter("supplier_code").toString();
        
        if (store_code.startsWith("7")){
        	if(request.getParameter("kontan_acc7") != null){
        		arrFields[35][1] = request.getParameter("kontan_acc7").toString();
        	}
        } else if (store_code.startsWith("8")){
        	if(request.getParameter("kontan_acc8") != null){
        		arrFields[35][1] = request.getParameter("kontan_acc8").toString();
        	}
        } else {
        	if(request.getParameter("kontan_acc2") != null){
        		arrFields[35][1] = request.getParameter("kontan_acc2").toString();
        	}
        }
        
        arrFields[36][0] = "invoice_supplier_option";
        String supplier_type = request.getParameter("supplier_type_invoice").toString();
        if (supplier_type.equals("0")){
        	arrFields[36][1] =  request.getParameter("intern_invoice_option").toString();
        } else if (supplier_type.equals("1")){
        	arrFields[36][1] =  request.getParameter("extern_invoice_option").toString();
        }
        
        //populate accounting_month an year
        arrFields[37][0] = "accounting_month";
        arrFields[37][1] = GeneralCodes.getAccountingMonth();
        arrFields[38][0] = "accounting_year";
        arrFields[38][1] = GeneralCodes.getAccountingYear();
        arrFields[39][0] = "vat_code";
        arrFields[39][1] = request.getParameter("cota_tva").toString();
        
        return arrFields;
	}

	/**
	 * Executed at server startup or application deploy.
	 */
	@Override
	public void init() throws PortletException {
		log.info("[PROFLUO] Initialize Invoices portlet.");
		super.init();
	}

	public int simpleSave(String[][] arrFields, String id, ArrayList<String> excludes, ArrayList<String> whereStatement, ArrayList<String> whereStatementValues) {
		int result = 0;
		try {
			result = GenericDBStatements.updateEntry(arrFields, "invoice_header", excludes, whereStatement, whereStatementValues);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	
	/**
	 * Used when registering a new invoice; Notify approvers if the invoice has new prj
	 * @param arrFields
	 * @param user
	 * @param supplier
	 * @param lineDataArticles
	 * @param storeLineIds
	 */
	/*
	public void sendNotificationsforNewProject(String[][] arrFields, User user, String supplier, String[][][] lineDataArticles, HashMap<Integer, Integer> storeLineIds) {
		boolean haslotIT = false;
		boolean hasNonItLines = false;
		//boolean hasDPI = false;
		//String idDPI = "";
		String productCode = "";
		//List<String> allDpis = new ArrayList<String>();
		List<String> allStoreLinesStores = new ArrayList<String>();
		List<String> hqStoreIds = new ArrayList<String>();
		List<String> storeIdsNotHq = new ArrayList<String>();
		List<String> allDpis = new ArrayList<String>();
		String idDPI = "";
		String idStore = "";
		String idStoreNotHq = "";
		String idStoreHQ = ""; 
		List<HashMap<String, Object>> storeLines = ControllerUtils.convertToListFromArray(lineDataArticles);
		
		for (int i = 0; i < lineDataArticles.length; i++){
			String id_st = storeLineIds.get(i).toString();
			for (int j = 0; j < lineDataArticles[i].length; j++ ) {
				if (lineDataArticles[i][j][0].equals("id_store")) {
					if (DefStore.isHQ(lineDataArticles[i][j][1])) {
						idStoreHQ = lineDataArticles[i][j][1];
					} else {
						idStoreNotHq = lineDataArticles[i][j][1];
					}
				}
//				//save all store 
				if(lineDataArticles[i][j][0].equals("id_store")){
					idStore = lineDataArticles[i][j][1];
				}
				//save all store 
				if(lineDataArticles[i][j][0].equals("id_dpi")){
					idDPI = lineDataArticles[i][j][1];
				}
				
				if(lineDataArticles[i][j][0].equals("product_code")) {
					productCode = lineDataArticles[i][j][1];
				}
			}
			//daca sunt linii cu IT
			if(DefLot.hasITLot(productCode)){
				InvoiceHeader.insertInvoiceApprovals(InvoiceHeader.getInvoiceIdByInvNumber(arrFields[0][1]), user.getUserId(), id_st, "3");
				haslotIT = true;
			} else {
				//construire lista dpi-uri
				if (idDPI.equals("") || idDPI.equals("0")){
					if (!allStoreLinesStores.contains(idStore)){
						allStoreLinesStores.add(idStore);
					}
					if(!storeIdsNotHq.contains(idStoreNotHq)){
						storeIdsNotHq.add(idStoreNotHq);
					}
					if(!hqStoreIds.contains(idStoreHQ)){
						hqStoreIds.add(idStoreHQ);
					}
					InvoiceHeader.insertInvoiceApprovals(InvoiceHeader.getInvoiceIdByInvNumber(arrFields[0][1]), user.getUserId(), id_st, "3");
				} else {
					if(!allDpis.contains(idDPI)){
						allDpis.add(idDPI);
					}
				}
				hasNonItLines = true;
			}
		}
		
		String companyName = DefCompanies.getCompanyNameById(arrFields[6][1]);
		String link = Links.individualInvoiceForApprovals + "&id=" + arrFields[0][1];
		String templateId = "";
		String body = "";
		if(invoiceWithMissingDoc){
			body = "<tr><td style='border:1px solid black'>" + arrFields[0][1] + "</td>"
					+ "<td style='border:1px solid black'>" + arrFields[1][1] + "</td><td style='border:1px solid black'>"
					+ supplier + "</td><td style='border:1px solid black'>"
					+ companyName + "</td>" + 
					"<td style='border:1px solid black'>" + arrFields[23][1] + "</td>"
					+ "<td style='border:1px solid black'><a href ='" + link + "'>Link</a></td></tr>";
			templateId = NotificationTemplates.LIPSA_DOCUMENTE;
		} else {
			body = "<tr><td style='border:1px solid black'>" + arrFields[0][1] + "</td>"
					+ "<td style='border:1px solid black'>" + arrFields[1][1] + "</td><td style='border:1px solid black'>"
					+ supplier + "</td><td style='border:1px solid black'>"
					+ companyName + "</td>"
					+ "<td style='border:1px solid black'><a href ='" + link + "'>Link</a></td></tr>";
			templateId = NotificationTemplates.INREGISTRARE_FACTURA;
		}
		
		HashMap<String, Object> bodyDetails = new HashMap<String,Object>();
		bodyDetails = setInsertParameters(templateId, "", "", body, "0", "B", "1", user, currentURL);
		List<HashMap<String,Object>> allDirectors = new ArrayList<HashMap<String,Object>>();

		if(!invoiceWithMissingDoc){
			if(allDpis.size() > 0) {
				String sqlValues = "(";
				//form values for IN clause
				for (int i = 0; i < allDpis.size(); i++){
					sqlValues += allDpis.get(i);
					if (i != allDpis.size() - 1){
						sqlValues += ",";
					}
				}
				sqlValues += ")";
				List<HashMap<String, Object>> dpiUsersToApprove = DPIHeader.getColumn("id", "user_email", sqlValues);
				List<HashMap<String, Object>> list = new ArrayList<HashMap<String,Object>>();
				
				//remove duplicate emails
				List<HashMap<String, Object>> uniqueDpis = new ArrayList<HashMap<String,Object>>();
				for (HashMap<String, Object> hash : dpiUsersToApprove){
					if (!uniqueDpis.contains(hash)){
						uniqueDpis.add(hash);
					}
				}
				for(int i = 0 ; i < uniqueDpis.size(); i++){
					HashMap<String,Object> hash = new HashMap<String,Object>();
					hash.put("email", uniqueDpis.get(i).get("user_email").toString());
					hash.put("first_name", "");
					hash.put("last_name", "");
					list.add(hash);
				}
				Notifications.insertNotification(list, bodyDetails, NotificationsSubject.INREGISTRARE_FACTURA);		
			} else {
				//se trece factura in stage 4 
				InvoiceHeader.setStageToFour(arrFields[0][1]);
				if(haslotIT) {
					List<HashMap<String,Object>> allITDirectors = UsersInformations.getUserDetailsByTeamId(UserTeamIdUtils.IT_DIRECTOR);
					Notifications.insertNotification(allITDirectors, bodyDetails, NotificationsSubject.INREGISTRARE_FACTURA);
				}
				
				//daca sunt linii care NU au lot IT se trimite notificare catre directorii de Active/Property/Ambii
				if(hasNonItLines) {
					String idNewPrj = arrFields[28][1];
					if (checkStoreLinesWithoutDpi(storeLines)){
						String projectOwner = DefNewProj.getOwnerById(idNewPrj);
						//daca e setat Active la administrarea de proiecte noi se trimite notificare catre directorii Active
						if(projectOwner.equals("0")) {
							allDirectors.clear();
							allDirectors.addAll(UsersInformations.getUserDetailsByTeamId(UserTeamIdUtils.DIRECTOR_ACTIVE));
						//daca e setat Property la administrarea de proiecte noi se trimite notificare catre directorii Property
						} else if (projectOwner.equals("1")) {
							allDirectors.clear();
							allDirectors.addAll(UsersInformations.getUserDetailsByTeamId(UserTeamIdUtils.PROPERTY_DIRECTOR));
						//altfel se notifica atat directorii Active cat si cei de Property
						} else {
							allDirectors.clear();
							allDirectors.addAll(UsersInformations.getUserDetailsByTeamId(UserTeamIdUtils.DIRECTOR_ACTIVE));
							allDirectors.addAll(UsersInformations.getUserDetailsByTeamId(UserTeamIdUtils.PROPERTY_DIRECTOR));
						}
					}
					if (storeIdsNotHq.size() > 0){
						List<HashMap<String,Object>> alLStoreDirectorsFromLiferay = UsersInformations.getUserDetailsByTeamId(UserTeamIdUtils.STORE_DIRECTOR);
						for (int i = 0 ; i < alLStoreDirectorsFromLiferay.size(); i++ ){
							HashMap<String,Object> storeDirector = alLStoreDirectorsFromLiferay.get(i);
							User directorMagazin = null;
							try {
								directorMagazin = UserLocalServiceUtil.getUser(Integer.parseInt(storeDirector.get("user_id").toString()));
								if(directorMagazin != null ){
									//se adauga directorul magazinului specific in lista
									if(storeIdsNotHq.contains(directorMagazin.getAddresses().get(0).getStreet2())){
										allDirectors.add(storeDirector);
									}
								}
							} catch (NumberFormatException | PortalException | SystemException e) {
								e.printStackTrace();
							}
						}
					}
					
					//send notification to hq admins if the invoice has nonit lines, is new project and stores are hq
					if (hqStoreIds.size() > 0){
						List<HashMap<String,Object>> allSediuAdmins = UsersInformations.getUserDetailsByTeamId(UserTeamIdUtils.ADMINISTRATOR_SEDIU);
						for (int j = 0; j < hqStoreIds.size(); j++){
							for (int k = 0; k < allSediuAdmins.size(); k++){
								try {
									User adminSediu = UserLocalServiceUtil.getUser(Integer.parseInt(allSediuAdmins.get(k).get("user_id").toString()));
									if (adminSediu.getAddresses().size() > 0 && 
											DefStore.getIdByStoreId(adminSediu.getAddresses().get(0).getStreet2().toString()).equals(hqStoreIds.get(j))){
										allDirectors.add(allSediuAdmins.get(k));
									}
								} catch (NumberFormatException | PortalException | SystemException e) {
									e.printStackTrace();
								}
							}
						}
					}
				}
				Notifications.insertNotification(allDirectors, bodyDetails, NotificationsSubject.INREGISTRARE_FACTURA);
			}
		} else {
			//String store_id = "";
			
			String valuesForSql = "(";
			//form values for IN clause
			for (int i = 0; i < allStoreLinesStores.size(); i++){
				valuesForSql += DefStore.getStoreIdById(allStoreLinesStores.get(i));
				if (i != allStoreLinesStores.size() - 1){
					valuesForSql += ",";
				}
			}
			valuesForSql += ")";
			//obtain all dpis from stores
			List<HashMap<String, Object>> dpisFromStores = DPIHeader.getColumn("id_store", "id", valuesForSql);
			//remove duplicates from dpi list
			for (HashMap<String, Object> hash : dpisFromStores){
				if (!allDpis.contains(hash.get("id").toString())){
					allDpis.add(hash.get("id").toString());
				}
			}
			valuesForSql = "(";
			//form values for IN clause
			for (int i = 0; i < allDpis.size(); i++){
				valuesForSql += allDpis.get(i);
				if (i != allDpis.size() - 1){
					valuesForSql += ",";
				}
			}
			valuesForSql += ")";
			List<HashMap<String, Object>> dpiUsersToApprove = DPIHeader.getColumn("id", "user_email", valuesForSql);
			List<HashMap<String, Object>> list = new ArrayList<HashMap<String,Object>>();
			
			//remove duplicate emails
			List<HashMap<String, Object>> uniqueDpis = new ArrayList<HashMap<String,Object>>();
			for (HashMap<String, Object> hash : dpiUsersToApprove){
				if (!uniqueDpis.contains(hash)){
					uniqueDpis.add(hash);
				}
			}
			for(int i = 0 ; i < uniqueDpis.size(); i++){
				HashMap<String,Object> hash = new HashMap<String,Object>();
				hash.put("email", uniqueDpis.get(i).get("user_email").toString());
				hash.put("first_name", "");
				hash.put("last_name", "");
				list.add(hash);
			}
			Notifications.insertNotification(list, bodyDetails, NotificationsSubject.FACTURA_CU_LIPSA_DOCUMENTE);
		}
//			} else {
//				//daca sunt linii care au lot IT se trimite notificare catre directorul IT
//				if(haslotIT && allDpis.size() < 1) {
//					List<HashMap<String,Object>> allITDirectors = UsersInformations.getUserDetailsByTeamId(UserTeamIdUtils.IT_DIRECTOR);
//					Notifications.insertNotification(allITDirectors, bodyDetails, NotificationsSubject.INREGISTRARE_FACTURA);
//				}
//				//daca sunt aprobatori dpi acestia vor fi notificati
//				System.out.println("################### " + allDpis);
//				String valuesForSql = "(";
//				//form values for IN clause
//				for (int i = 0; i < allDpis.size(); i++){
//					valuesForSql += allDpis.get(i);
//					if (i != allDpis.size() - 1){
//						valuesForSql += ",";
//					}
//				}
//				valuesForSql += ")";
//				List<HashMap<String, Object>> respEmails = new ArrayList<HashMap<String,Object>>();
//				if(allDpis.size() > 0 ){
//					respEmails = DPIHeader.getColumn("id", "user_email", valuesForSql);
//				}
//				allDirectors.clear();
//				for(int i = 0 ; i < respEmails.size(); i++){
//					HashMap<String,Object> hash = new HashMap<String,Object>();
//					hash.put("email", respEmails.get(i).get("user_email").toString());
//					hash.put("first_name", "");
//					hash.put("last_name", "");
//					list.add(hash);
//				}
//				allDirectors.addAll(list);
//				System.out.println("PLMMMMMMMMMM -> " + respEmails);
//				System.out.println("STFFFFFFFFFF -> " + allDirectors);
//				Notifications.insertNotification(allDirectors, bodyDetails, NotificationsSubject.INREGISTRARE_FACTURA);
//			}
		//daca e proiect nou pe sediu se notifica aprobatorii dpi de pe sediu
//		else {
//			List<HashMap<String,Object>> allDPIForSediu = DPIHeader.getAllDpiForSediu("9150");
//			List<HashMap<String,Object>> list = new ArrayList<HashMap<String,Object>>();
//			for(int i = 0 ; i < allDPIForSediu.size(); i ++ ) {
//				HashMap<String,Object> hash = new HashMap<String,Object>();
//				hash.put("email", allDPIForSediu.get(i).get("email").toString());
//				hash.put("first_name", "");
//				hash.put("last_name", "");
//				list.add(hash);
//			}
//			allDirectors.addAll(list);
//			Notifications.insertNotification(allDirectors, bodyDetails, NotificationsSubject.INREGISTRARE_FACTURA);
//		}
	}
	*/
	
	public void deleteInvoice(ActionRequest request, ActionResponse response) {
		User user = null;
		try {
			user = UserServiceUtil.getUserById(Long.parseLong(request.getRemoteUser()));
		} catch (NumberFormatException | PortalException | SystemException e) {
			e.printStackTrace();
		}
		int id = Integer.parseInt(request.getParameter("fact_id").toString());
		
		System.out.println("Id-ul facturii ce va fi trimisa in ChGen: " + id);
		
		int rez = UpdateChGenInvoice.updateInvoiceChGen(id); //modific contract nomber-ul facturii din ChGen in empty string, pentru a fi importat in aceasta
		
		if (rez != 0) {
			List<HashMap<String, Object>> oneInvoice = InvoiceHeader.getHeaderById(String.valueOf(id), "invoice_header");
			String[][] fields = new String[7][2];
			fields[0][0] = "inv_number";
			fields[0][1] = oneInvoice.get(0).get("inv_number").toString();
			fields[1][0] = "id_supplier";
			fields[1][1] = oneInvoice.get(0).get("id_supplier").toString();
			fields[2][0] = "inv_date";
			fields[2][1] = oneInvoice.get(0).get("inv_date").toString();
			fields[3][0] = "total_with_vat_curr";
			fields[3][1] = oneInvoice.get(0).get("total_with_vat_curr").toString();
			fields[4][0] = "id_company";
			fields[4][1] = oneInvoice.get(0).get("id_company").toString();
			fields[5][0] = "user_id";
			fields[5][1] = String.valueOf(user.getUserId());
			fields[6][0] = "cid";
			fields[6][1] = oneInvoice.get(0).get("cid").toString();
			GenericDBStatements.insertEntry(fields, "log_sent_to_chgen_invoices", new ArrayList<String>());
			
			GenericDBStatements.deleteEntry("invoice_header", "id", String.valueOf(id));
			GenericDBStatements.deleteEntry("invoice_line", "inv_number", String.valueOf(id));
			SessionMessages.add(request, "update_ok");
			response.setRenderParameter("mvcPath", "/html/facturi/view.jsp");
		} else {
			SessionErrors.add(request, "chgen_error");
			response.setRenderParameter("mvcPath", "/html/facturi/view.jsp");
		}
	}
	
	private void addNotificationToSupplier(String invoiceId, User user, String invoiceDate,  String  litigiu, String invoiceNumber) {
		String supplierEmail = DefSupplier.getSupplierEmailByInvoiceId(invoiceId);
		//String value = null;
		
		String company = DefCompanies.getCompanyNameByInvoiceId(invoiceId);
		List<HashMap<String,Object>> userDetails = new ArrayList<HashMap<String,Object>>();
		userDetails.add(new HashMap<String,Object>());
		userDetails.get(0).put("email", supplierEmail);
		userDetails.get(0).put("first_name", "Name");
		userDetails.get(0).put("last_name", "Surname");
		
		String body = "Va informam ca pentru factura cu nr " + invoiceNumber + " data " + invoiceDate; 
		body += " emisa catre societatea " + company + " s-au identificat urmatoarele erori: <br/>";
		body +=  "<ul><li>" + litigiu + "</li></ul><br/>";
		body += "Va rugam sa trimiteti factura storno si factura corectata.<br/><br/> ";
		
		HashMap<String, Object> bodyDetails = new HashMap<String,Object>();
		bodyDetails = setInsertParameters(NotificationTemplates.VINA_FURNIZOR, "", "", body, "0", "S", "1", user, currentURL);
		
		Notifications.insertNotification(invoiceId, userDetails, bodyDetails, NotificationsSubject.VINA_FURNIZOR);
		
	}
	
	//get all stages associated with an invoice and an user
	public static ArrayList<String> getAssociatedStages(String [] list){
		System.out.println(list.length);
		ArrayList<String> result = new ArrayList<String>();
		for (int i = 0; i < list.length; i++){
			//23=3,4
			String[] parts = list[i].split("=");
			String[] allStages = parts[1].split(",");
			for (int j = 0; j < allStages.length; j++){
				if (!result.contains(allStages[j])){
					result.add(allStages[j]);
				}
			}
		}
		System.out.println(result);
		return result;
	}
	/**
	 * Approve invoice method mapped in the JSP in order to save / create an
	 * invoice in the DB.
	 * 
	 * @param request
	 * @param response
	 */
	public void approveInvoice(ActionRequest request, ActionResponse response) {
		// remove default error message from Liferay
		PortletConfig portletConfig = (PortletConfig) request.getAttribute(JavaConstants.JAVAX_PORTLET_CONFIG);
		LiferayPortletConfig liferayPortletConfig = (LiferayPortletConfig) portletConfig;
		SessionMessages.add(request, liferayPortletConfig.getPortletId() + SessionMessages.KEY_SUFFIX_HIDE_DEFAULT_ERROR_MESSAGE);

		log.info("[PROFLUO] Adauga Factura portlet - approveInvoice()");

		try {
			String motivInvalidare = "";
			User user = UserServiceUtil.getUserById(Long.parseLong(request.getRemoteUser()));
			int arrSize = 6;
			String[][] arrFields = new String[arrSize][2];
			String id = request.getParameter("idInvoice");
			String blocked = request.getParameter("blockedORnot").toString();
			boolean action = false;
			if (blocked.equals("OK")) {
				action = true;
			} else {
				action = false;
			}

			String ids = request.getParameter("lineIds").toString();
			String invoiceDate = request.getParameter("invoiceDate").toString();
			
			//params for blocked invoice notification
			String invoiceNumber = request.getParameter("invoiceNumber").toString();
			String supplierName = request.getParameter("supplierName").toString();
			//String supplierCui = request.getParameter("supplierCode").toString();
			String companyName = request.getParameter("companyName").toString();
			//String kontanAcc = request.getParameter("kontanAccount").toString();
			
			//String idNewProject = request.getParameter("idNewProject").toString();
			

			String[] lineIds = ids.substring(0, ids.length() - 1).split(",");

			arrFields[0][0] = "blocked_type";
			arrFields[0][1] = "0";
			if (request.getParameter("motiv_invalidare") != null ){
				motivInvalidare = request.getParameter("motiv_invalidare").toString();
				if (motivInvalidare != null || motivInvalidare != "") {
					arrFields[0][1] = motivInvalidare;
				}
			}
			arrFields[1][0] = "stage";
			arrFields[2][0] = "litigii";
			String litigiu = request.getParameter("existenta_litigiu").toString();
			if (litigiu.equals("1")) {
				arrFields[2][1] = "Y";
			} else {
				arrFields[2][1] = "N";
			}
			arrFields[3][0] = "details_litigii";
			String detaliiLitigiu = "";
			if ( request.getParameter("detalii-litigiu") != null ){
				detaliiLitigiu = request.getParameter("detalii-litigiu").toString();
				if (arrFields[2][1].equals("Y")) {
					arrFields[3][1] = detaliiLitigiu;
				} else {
					arrFields[3][1] = "";
				}
			}
			arrFields[4][0] = "supplier_fault";
			String suppFault = "";
			arrFields[4][1] = "N";
			if (request.getParameter("vina_furnizor") != null) {
				suppFault = request.getParameter("vina_furnizor").toString();
				if (suppFault.equals("1")) {
					arrFields[4][1] = "Y";
					addNotificationToSupplier(id, user, invoiceDate, detaliiLitigiu, invoiceNumber);
				}
			}
			String detalii_aditionale = "";
			if (request.getParameter("detalii-aditionale") != null) {
				detalii_aditionale = request.getParameter("detalii-aditionale").toString();
			}
			arrFields[5][0] = "details_aditional";
			arrFields[5][1] = detalii_aditionale;

			// attrs for where statement
			ArrayList<String> whereStatement = new ArrayList<String>();
			// attrs values for where statement
			ArrayList<String> whereStatementValues = new ArrayList<String>();
			whereStatement.add("id");
			whereStatementValues.add(id);
			ArrayList<String> excludes = new ArrayList<String>();
			Calendar calendar = Calendar.getInstance();
		    java.sql.Timestamp currentTS = new java.sql.Timestamp(calendar.getTime().getTime());
			for (int i = 0; i < lineIds.length; i++){
				if (!action){
					InvoiceHeader.updateInvoiceApprovals(currentTS.toString(), String.valueOf(user.getUserId()),
							ids.substring(0, ids.length() - 1), "1", "0");
				} else {
					InvoiceHeader.updateInvoiceApprovals(currentTS.toString(), String.valueOf(user.getUserId()),
							ids.substring(0, ids.length() - 1), "0", "1");
				}
			}
			boolean hasAllApprovals = InvoiceHeader.hasAllApprovals(id);
			if (!action){
				arrFields[1][1] = "-3";
				addNotificationForBlockedInvoice(id, user, invoiceNumber, supplierName, companyName, invoiceDate, detaliiLitigiu);
			} else {
				if(hasAllApprovals) {
					arrFields[1][1] = "10";
				} else {
					arrFields[1][1] = "3";
					//add notification to next level
					addNotificationForFirstUnapprovedLevel(6, invoiceNumber, user, supplierName, id, invoiceDate, companyName);
				}
			}
			SessionMessages.add(request, "update_ok");
			GenericDBStatements.updateEntry(arrFields, "invoice_header", excludes, whereStatement, whereStatementValues);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void addNotificationForBlockedInvoice(String invoiceId, User user, String invNumber, String supplierName, String companyName, String invoiceDate, String motivInvalidare) {
		//list of previous approvals
		List<HashMap<String,Object>> previousApprovals = InvoiceHeader.getAllPreviousApprovals(invoiceId);
		List<HashMap<String,Object>> usersToBeNotified = new ArrayList<HashMap<String,Object>>();
		for(HashMap<String,Object> hash : previousApprovals) {
			String approvalsIds = hash.get("id_user").toString();
			String [] individualIds = approvalsIds.split(",");
			try {
				for(int i = 0; i < individualIds.length; i++ ) {
					HashMap<String,Object> userInfo = new HashMap<String,Object>();
					User approval = UserLocalServiceUtil.getUser(Integer.parseInt(individualIds[i]));
					userInfo.put("email", approval.getEmailAddress());
					userInfo.put("first_name", approval.getFirstName());
					userInfo.put("last_name", approval.getLastName());
					usersToBeNotified.add(userInfo);
				}
			} catch (NumberFormatException | PortalException | SystemException e) {
				e.printStackTrace();
			}
		}
		String link = Links.allInvoicesForApprovals;
		HashMap<String, Object> bodyDetails = new HashMap<String,Object>();
		String body = "<tr><td style='border:1px solid black'>" + invNumber + "</td><td  style='border:1px solid black'>" 
				+ invoiceDate + "</td><td style='border:1px solid black'>" + supplierName + "</td><td style='border:1px solid black'>"
				+ companyName + "</td><td style='border:1px solid black'>"
				+ motivInvalidare + "</td><td style='border:1px solid black'><a href='" + link + "'>Link</a></td></tr>";
		bodyDetails = setInsertParameters(NotificationTemplates.FACTURA_BLOCATA, "", "", body, "0", "B", "1", user, link);
		//insert the notifications
		Notifications.insertNotification(invoiceId, usersToBeNotified, bodyDetails, NotificationsSubject.FACTURA_BLOCATA);
	}
	
	/**
	 * Check if the current invoice has lines with lot it true
	 * If it has, delete them for further use of the list
	 * 
	 * @param lines
	 * @return
	 */
//	private List<HashMap<String, Object>> checkLotIt(List<HashMap<String, Object>> lines){
//		//IT-Hardware = 1; IT-Software = 2
//		String catIt = "1,2";
//		System.out.println("Check lot it");
//		//int initialSize = lines.size();
//		for (int i = lines.size() - 1; i >= 0; i--){
//			if (catIt.contains(lines.get(i).get("clasif").toString())){
//				lines.remove(i);
//				System.out.println("			line IT");
//			}
//		}
//		return lines;
//	}
	
	/**
	 * Returns a list of store line store ids;
	 * The store lines must not be IT, must have dpi and the stores must not be hq
	 * @param list
	 * @param state : false -> must not pe hq
	 * 				: true 	-> must be hq 
	 * @return
	 */
	public static List<String> getNonItStoreLineStoreIds(List<HashMap<String, Object>> list, boolean state){
		List<String> result = new ArrayList<String>();
		for (HashMap<String, Object> hash : list){
			String dpi = hash.get("id_dpi").toString();
			String store = DefStore.getStoreIdById(hash.get("id_store").toString());
			//if it has dpi
			if (!dpi.equals("") && !dpi.equals("0")){
				//if not it
				if (!DefLot.hasITLot(hash.get("product_code").toString())){
					//if not headquarter
					if (DefStore.isHQ(hash.get("id_store").toString()) == state){
						if (!result.contains(store)){
							result.add(store);
						}
					}
				}		
			}
		}
		return result;
	}
	
	/**
	 * If the invoice has at least one store line without dpi then notify acvite/property directors
	 * @param lines
	 * @return
	 */
	public static boolean checkStoreLinesWithoutDpi(List<HashMap<String, Object>> lines){
		for (HashMap<String, Object> hash : lines){
			if (hash.get("id_dpi").toString().equals("") || hash.get("id_dpi").toString().equals("0")){
				if (!DefLot.hasITLot(hash.get("product_code").toString())){
					return true;
				}
			}
		}
		return false;
	}
	
//	/**
//	 * Decide which teams to notify after the invoice was approved entirely by dpi responsables
//	 * 
//	 * @param id
//	 * @param user
//	 * @param invoiceNumber
//	 * @param invoiceDate
//	 * @param supplier
//	 * @param company
//	 * @param account
//	 */
//	private void sendNotifactionFromDpiToApprovers(int id, User user, String invoiceNumber, String invoiceDate, 
//			String supplier, String company, String account, String idNewProject){
//		
//		approvers.clear();
//		List<String> allStores = new ArrayList<String>();
//		List<HashMap<String,Object>> allStoreDirectorsToBeNotified = new ArrayList<HashMap<String,Object>>();
//		List<HashMap<String,Object>> allHqAdminsToBeNotified = new ArrayList<HashMap<String,Object>>();
//		List<HashMap<String,Object>> alLStoreDirectorsFromLiferay = UsersInformations.getUserDetailsByTeamId(UserTeamIdUtils.STORE_DIRECTOR);
//		List<HashMap<String,Object>> allHqAdmins = UsersInformations.getUserDetailsByTeamId(UserTeamIdUtils.ADMINISTRATOR_SEDIU);
//		
//		boolean hasLineForSediuAdmin = false;
//		//all store lines
//		List<HashMap<String, Object>> storeLines = InvoiceHeader.getStoreLinesById(String.valueOf(id), "invoice_store_line");
//		//all lines
//		List<HashMap<String, Object>> lines = InvoiceHeader.getLinesById(String.valueOf(id), "invoice_line");
//		int initialSize = lines.size();
//		//check if it departament must be notified
//		checkLotIt(lines);
//		//notify IT DIRECTOR
//		if (lines == null || lines.size() < initialSize){
//			//the invoice has at least one line with lot it
//			approvers.addAll(UsersInformations.getUserDetailsByTeamId(UserTeamIdUtils.IT_DIRECTOR));
//		}
//		
//		//daca e proiect nou se notifica directorii de active/property/ambii in functie de owner-ul proiectului nou
//		if (Integer.parseInt(idNewProject) != 0) {
//			String projectOwner = DefNewProj.getOwnerById(idNewProject);
//			
//			if (checkStoreLinesWithoutDpi(storeLines)){
//				//daca e 0 atunci se notifica directorii de active
//				if(projectOwner.equals("0")){
//					approvers.addAll(UsersInformations.getUserDetailsByTeamId(UserTeamIdUtils.DIRECTOR_ACTIVE));
//				//daca e 1 se notifica directorii de property
//				} else if (projectOwner.equals("1")){
//					approvers.addAll(UsersInformations.getUserDetailsByTeamId(UserTeamIdUtils.PROPERTY_DIRECTOR));
//				//altfel se notifica ambii
//				} else {
//					approvers.addAll(UsersInformations.getUserDetailsByTeamId(UserTeamIdUtils.DIRECTOR_ACTIVE));
//					approvers.addAll(UsersInformations.getUserDetailsByTeamId(UserTeamIdUtils.PROPERTY_DIRECTOR));
//				}
//			}
//			
//			List<String> storeIdsNonHq = getNonItStoreLineStoreIds(storeLines, false);
//			List<String> storeIdsHQ = getNonItStoreLineStoreIds(storeLines, true);
//			if (storeIdsNonHq.size() > 0){
//				for (int i = 0 ; i < alLStoreDirectorsFromLiferay.size(); i++ ){
//					HashMap<String,Object> storeDirector = alLStoreDirectorsFromLiferay.get(i);
//					User directorMagazin = null;
//					try {
//						directorMagazin = UserLocalServiceUtil.getUser(Integer.parseInt(storeDirector.get("user_id").toString()));
//						if(directorMagazin != null ){
//							//se adauga directorul magazinului specific in lista
//							if(storeIdsNonHq.contains(directorMagazin.getAddresses().get(0).getStreet2())){
//								allStoreDirectorsToBeNotified.add(storeDirector);
//							}
//						}
//					} catch (NumberFormatException | PortalException | SystemException e) {
//						e.printStackTrace();
//					}
//				}
//			}
//			
//			if (storeIdsHQ.size() > 0){
//				for (int i = 0 ; i < allHqAdmins.size(); i++ ){
//					HashMap<String,Object> hqAdmin = allHqAdmins.get(i);
//					User adminSediu = null;
//					try {
//						adminSediu = UserLocalServiceUtil.getUser(Integer.parseInt(hqAdmin.get("user_id").toString()));
//						if(adminSediu != null ){
//							//se adauga directorul magazinului specific in lista
//							if(storeIdsHQ.contains(adminSediu.getAddresses().get(0).getStreet2())){
//								allHqAdminsToBeNotified.add(hqAdmin);
//							}
//						}
//					} catch (NumberFormatException | PortalException | SystemException e) {
//						e.printStackTrace();
//					}
//				}
//			}
//			
//		} else {
//			if (lines != null){
//				//pentru fiecare linie
//				for (HashMap<String, Object> line : lines){
//					//pentru fiecare store line
//					for (HashMap<String, Object> storeLine : storeLines){
//						if(InvoiceHeader.wasApproved(storeLine.get("id").toString(), InvoiceStages.GENERAL_APPROVE)){
//							continue;
//						}
//						if (storeLine.get("line_id").toString().equals(line.get("id").toString())){
//							//daca factura are linii pe sediu
//							if (DefStore.isHQ(storeLine.get("id_store").toString())){
//								hasLineForSediuAdmin = true;
//							} else {
//								//lista de magazine ale caror directori trebuie notificati
//								allStores.add(storeLine.get("id_store").toString());
//								for(int i = 0 ; i < alLStoreDirectorsFromLiferay.size(); i++ ){
//									HashMap<String,Object> storeDirector = alLStoreDirectorsFromLiferay.get(i);
//									User directorMagazin = null;
//									try {
//										directorMagazin = UserLocalServiceUtil.getUser(Integer.parseInt(storeDirector.get("user_id").toString()));
//										if(directorMagazin != null ){
//											//se adauga directorul magazinului specific in lista
//											if(directorMagazin.getAddresses().get(0).getStreet2().equals(DefStore.getStoreIdById(storeLine.get("id_store").toString()))){
//												allStoreDirectorsToBeNotified.add(storeDirector);
//											}
//										}
//									} catch (NumberFormatException | PortalException | SystemException e) {
//										e.printStackTrace();
//									}
//								}
//							}
//						}
//					}
//				}
//			}
//		}
//		//daca sunt linii pe sediu se notifica administratorii de sediu
//		if(hasLineForSediuAdmin){
//			approvers.addAll(UsersInformations.getUserDetailsByTeamId(UserTeamIdUtils.ADMINISTRATOR_SEDIU));
//		}
//		
//		if (allHqAdminsToBeNotified.size() > 0){
//			approvers.addAll(allHqAdminsToBeNotified);
//		}
//		//daca sunt directori de magazine ce trebuie notificati
//		if(allStoreDirectorsToBeNotified.size() > 0 ){
//			approvers.addAll(allStoreDirectorsToBeNotified);
//		}
//		
//		//set insert parameters
//		HashMap<String, Object> notificationFields = new HashMap<String, Object>();
//		
//		String link = Links.individualInvoiceForApprovals + "&id=" + id;
//		String body = "<tr><td style='border:1px solid black'>" + invoiceNumber + "</td><td style='border:1px solid black'>"
//				+ invoiceDate + "</td><td style='border:1px solid black'>" + supplier + "</td><td style='border:1px solid black'>" 
//				+ company + "</td>"
//				+ "<td style='border:1px solid black'><a href='" + link + "'>Link</a></td></tr>";
//		
//		notificationFields = setInsertParameters(NotificationTemplates.INREGISTRARE_FACTURA, "", "", body, "0", "B", "1", user, link);
//		//insert the notifications
//		Notifications.insertNotification(String.valueOf(id), approvers, notificationFields, NotificationsSubject.INREGISTRARE_FACTURA);
//		//resert the list
//		approvers.clear();
//	}
	
	public String extractCUI(String cui){
		if (cui.startsWith("RO"))
			return cui.substring(2);
		else return cui;
	}

	/**
	 * 
	 * @param resourceRequest
	 */
	private int createSupplier(ResourceRequest resourceRequest, User user) {
		String name = ParamUtil.getString(resourceRequest, "name");
		String cui = ParamUtil.getString(resourceRequest, "cui");
		String email = ParamUtil.getString(resourceRequest, "email");
		String phone = ParamUtil.getString(resourceRequest, "phone");
		String reg_com = ParamUtil.getString(resourceRequest, "reg_com");
		String payment_code = ParamUtil.getString(resourceRequest, "payment_code");
		String ro = ParamUtil.getString(resourceRequest, "ro");
		String bank = ParamUtil.getString(resourceRequest, "bank");
		String bank_acc = ParamUtil.getString(resourceRequest, "bank_acc");
		String swift = ParamUtil.getString(resourceRequest, "swift");
		String id_company = ParamUtil.getString(resourceRequest, "id_company");
		String payment_term = ParamUtil.getString(resourceRequest, "payment_term");
		String address = ParamUtil.getString(resourceRequest, "new_address");
    	String supplier_type = ParamUtil.getString(resourceRequest, "supplier_type_db").equals("") ? "0" : ParamUtil.getString(resourceRequest, "supplier_type_db") ;
    	String tert_group = ParamUtil.getString(resourceRequest, "supplier_tert_group_popup");
		// set suppliers excludes
		this.setSupplierExcludes();
		// init insert data
		String[][] arrFields = new String[19][2];
		arrFields[0][0] = "name";
		arrFields[0][1] = name;
		arrFields[1][0] = "cui";
		arrFields[1][1] = cui;
		arrFields[2][0] = "email";
		arrFields[2][1] = email;
		arrFields[3][0] = "phone";
		arrFields[3][1] = phone;
		arrFields[4][0] = "bank";
		arrFields[4][1] = bank;
		arrFields[5][0] = "bank_acc";
		arrFields[5][1] = bank_acc;
		arrFields[6][0] = "swift";
		arrFields[6][1] = swift;
		arrFields[7][0] = "payment_code";
		arrFields[7][1] = payment_code;
		arrFields[8][0] = "RO";
		if (ro.equals("false")) {
			arrFields[8][1] = "";
		} else {
			arrFields[8][1] = "RO";
			
		}
		arrFields[9][0] = "reg_com";
		arrFields[9][1] = reg_com;
		arrFields[10][0] = "supplier_code";
		try {
			supplier_code = InvoiceHeader.getMinSupplierCode();
		} catch (Exception e) {
		}

		arrFields[10][1] = String.valueOf(--supplier_code);
		arrFields[11][0] = "stage";
		arrFields[11][1] = String.valueOf(SupplierStages.APPROVED_BY_ACCOUNTING);
		arrFields[12][0] = "id_company";
		arrFields[12][1] = id_company;
		arrFields[13][0] = "payment_term";
		arrFields[13][1] = payment_term;
		arrFields[14][0] = "supplier_type";
		arrFields[14][1] = supplier_type;
		arrFields[15][0] = "address";
		arrFields[15][1] = address;
		arrFields[16][0] = "tert_group";
		arrFields[16][1] = tert_group;
		arrFields[17][0] = "cuiint";
		arrFields[17][1] = extractCUI(cui);
		arrFields[18][0] = "id_user";
		arrFields[18][1] = String.valueOf(user.getUserId());
		// result
		int resultedID = 0;
		try {
			resultedID = GenericDBStatements.insertEntry(arrFields, "def_suppliers", this.suppliersExcludes);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return resultedID;
	}
	
	/**
	 * Sets insert parameters for notification database
	 * Gets all the users that must be notified
	 * 
	 * @param resultId
	 * @param user
	 */
	public void addIncorrectInvoiceNotification(int id, User user, String[][] arrFields) {
		//set insert parameters
		HashMap<String, Object> notificationFields = new HashMap<String, Object>();
		String companyName = DefCompanies.getCompanyNameById(arrFields[6][1]);
		
		//form the body of the template
		String body = "Va informam ca pentru factura cu nr " +arrFields[0][1] + " data " + arrFields[1][1] + " emisa catre societatea " 
					+ companyName + " in valoare de " + arrFields[15][1] + " " + arrFields[17][1] + " s-au identificat urmatoarele erori:<br/>";
		body += "<ul><li>" + arrFields[24][1] + "</li></ul>";
		body += "<br/>IDENTIFICARE AUTOMATA IN APLICATIE SI PRELUARE DESCRIERE EROARE IN MESAJ CATRE FURNIZOR<br/><br/>Va rugam sa trimiteti factura storno si factura corectata .";
		notificationFields = setInsertParameters(NotificationTemplates.FACTURA_ERONATA, "", "", body, "0", "S", "1", user, currentURL);
		//notify the supplier
		//get supplier email
		String supplier_id = "";
		for (int i = 0; i < arrFields.length; i++){
			if (arrFields[i][0].equals("id_supplier")){
				supplier_id = arrFields[i][1];
			}
		}
		//get supplier email
		String supplier_email = DefSupplier.getOneColumn(supplier_id, "def_suppliers", "email");
		//details of the supplier that is being notified
		List<HashMap<String, Object>> supplierDetails = new ArrayList<HashMap<String,Object>>();
		HashMap<String, Object> supplier = new HashMap<String, Object>();
		supplier.put("email", supplier_email);
		supplier.put("first_name", "");
		supplier.put("last_name", "");
		supplierDetails.add(supplier);
		if (!supplier_id.equals("") && supplier_email.equals("")){
			System.out.println("FURNIZORUL DE PE FACTURA NUMARUL " + arrFields[0][1] + " NU ARE ADRESA DE EMAIL!!!");
		}
		//insert notification
		Notifications.insertNotification(String.valueOf(id), supplierDetails, notificationFields, NotificationsSubject.FACTURA_ERONATA);
	}
	
	/**
	 * Inserts into notification datatable a blocked invoice type notification for every user
	 * that must be notified
	 * 
	 * @param id = invoice id
	 * @param user = current session user
	 * @param invoiceNumber = invoice number
	 * @param invoiceDate = invoice date
	 * @param supplier = supplierCui + " - " supplierName
	 * @param company = company name
	 * @param account = kontan account
	 * @param reason = reason of invalidation
	 */
	public void addBlockedInvoiceNotification(int id, User user, String invoiceNumber, String invoiceDate, 
			String supplier, String company, String account, String reason, String currentStage){
		
		//set insert parameters
		HashMap<String, Object> notificationFields = new HashMap<String, Object>();

		String link = Links.allInvoicesForApprovals;
		
		String body = "<tr><td style='border:1px solid black'>" + invoiceNumber + "</td><td  style='border:1px solid black'>" 
				+ invoiceDate + "</td><td style='border:1px solid black'>" + supplier + "</td><td style='border:1px solid black'>"
				+ company + "</td><td style='border:1px solid black'>"
				+ reason + "</td><td style='border:1px solid black'><a href='" + link + "'>Link</a></td></tr>";
		
		notificationFields = setInsertParameters(NotificationTemplates.FACTURA_BLOCATA, "", "", body, "0", "B", "1", user, link);
		//if an approver rejects an invoice => notify all previous approvers + account
		//get the account who registered this invoice
		String accountingUser = InvoiceHeader.getOneColumn(String.valueOf(id), "invoice_header", "id_user");
		
		//get all previous approvers
		List<HashMap<String, Object>> previousApprovers = InvoiceHeader.getPreviousApprovers(String.valueOf(id), currentStage);
		
		String inClause = "(";
		//form IN CLAUSE statement
		if(previousApprovers.size() > 0){
			for (HashMap<String, Object> approver : previousApprovers){
				//add all approvers ids in the in clause
				inClause += approver.get("approver") + ", ";
			}
		}
		//add account id in IN Clause
		inClause += accountingUser + ")";
		List<HashMap<String, Object>> usersDetails = UsersInformations.getUsersDetails(inClause);
		
		Notifications.insertNotification(String.valueOf(id), usersDetails, notificationFields, NotificationsSubject.FACTURA_BLOCATA);
	}
	
//	/**
//	 * Forms an arraylist of all values for a certain key from multiple hashmaps
//	 * @param list
//	 * @return
//	 */
//	private ArrayList<String> convertFromHashMapListToList(List<HashMap<String, Object>> list, String column){
//		ArrayList<String> result = new ArrayList<String>();
//		for (HashMap<String, Object> hash : list){
//			result.add(hash.get(column).toString());
//		}
//		
//		return result;
//	}
	
	private HashMap<String, Object> setInsertParameters (String id_template, String to_email, String to_name, String body, String status, 
			String type, String doc_type, User user, String link){
		System.out.println("Link : " + link);
		HashMap<String, Object> bodyDetails = new HashMap<String,Object>();
		
		bodyDetails.put("id_template", id_template);
		bodyDetails.put("sender_email", user.getEmailAddress());
		bodyDetails.put("sender_name", user.getFullName());
		bodyDetails.put("body", body);
		String currentTime = new Timestamp(new Date().getTime()).toString();
		bodyDetails.put("time_to_send", currentTime);
		bodyDetails.put("time_of_send", currentTime);
		bodyDetails.put("status", status);
		bodyDetails.put("type", type);
		bodyDetails.put("doc_type", doc_type);
		bodyDetails.put("created", currentTime);
		if(link.contains("url")){
			bodyDetails.put("link", link.substring(0, link.lastIndexOf("?")));
		} else {
			bodyDetails.put("link", link);
		}
		return bodyDetails;
	}
	
	private void addSupplierRequestNotificationToAccountingDirector(int resultId, User user) {
		//int stage = SupplierStages.APPROVED_BY_ACCOUNTING;
		int nextTeamId = UserTeamIdUtils.ACCOUNTING_DIRECTOR;
		List<HashMap<String,Object>> userDetails = UsersInformations.getUserDetailsByTeamId(nextTeamId);
		HashMap<String, Object> bodyDetails = new HashMap<String,Object>();

		String supplierId = resultId + "";
		//supplierId = supplierId.substring(0, supplierId.length()-1);
		List<HashMap<String,Object>> supplierDetails = DefSupplier.getSupplierDetails(supplierId);
		String supplierName = supplierDetails.get(0).get("name").toString();
		String supplierEmail = supplierDetails.get(0).get("email").toString();
		String supplierPhone = supplierDetails.get(0).get("phone").toString();
		String termenDePlata = supplierDetails.get(0).get("payment_term").toString();
		
		String link = Links.allSuppliersRequestsForAccountingDirector;
		
		String body = "<tr><td style='border:1px solid black'>" + user.getFullName() + "</td><td style='border:1px solid black'>"
				+ supplierName + "</td><td style='border:1px solid black'>Adresa</td><td style='border:1px solid black'>" 
				+ supplierEmail + "</td><td style='border:1px solid black'>" + supplierPhone
				+ "</td><td style='border:1px solid black'>" + termenDePlata + "</td>"
				+ "<td style='border:1px solid black'><a href='" + link + "'>Link</a></td></tr>";
		
		bodyDetails = setInsertParameters(NotificationTemplates.CONTA_CREARE_FURNIZOR, "", "", body, "0", "B", "1", user, link);
		Notifications.insertNotification("", userDetails, bodyDetails, NotificationsSubject.CREEARE_FURNIZOR);
	}
	
	private void addSupplierRequestNotificationToSupplierApprovals(int resultId, User user) {
		//int stage = SupplierStages.APPROVED_BY_ACCOUNTING;
		int nextTeamId = UserTeamIdUtils.VALIDATE_SUPPLIER;
		List<HashMap<String,Object>> userDetails = UsersInformations.getUserDetailsByTeamId(nextTeamId);
		HashMap<String, Object> bodyDetails = new HashMap<String,Object>();

		String supplierId = resultId + "";
		//supplierId = supplierId.substring(0, supplierId.length()-1);
		List<HashMap<String,Object>> supplierDetails = DefSupplier.getSupplierDetails(supplierId);
		String supplierName = supplierDetails.get(0).get("name").toString();
		String supplierEmail = supplierDetails.get(0).get("email").toString();
		String supplierPhone = supplierDetails.get(0).get("phone").toString();
		String termenDePlata = supplierDetails.get(0).get("payment_term").toString();
		
		String link = Links.allSuppliersRequestsForAccountingDirector;
		
		String body = "<tr><td style='border:1px solid black'>" + user.getFullName() + "</td><td style='border:1px solid black'>"
				+ supplierName + "</td><td style='border:1px solid black'>Adresa</td><td style='border:1px solid black'>" 
				+ supplierEmail + "</td><td style='border:1px solid black'>" + supplierPhone
				+ "</td><td style='border:1px solid black'>" + termenDePlata + "</td>"
				+ "<td style='border:1px solid black'><a href='" + link + "'>Link</a></td></tr>";
		
		bodyDetails = setInsertParameters(NotificationTemplates.CONTA_CREARE_FURNIZOR, "", "", body, "0", "B", "1", user, link);
		Notifications.insertNotification("", userDetails, bodyDetails, NotificationsSubject.CREEARE_FURNIZOR);
	}

	private void updateSupplier(String id, ResourceRequest resourceRequest, User user) {
		String name = ParamUtil.getString(resourceRequest, "name");
		String cui = ParamUtil.getString(resourceRequest, "cui");
		String email = ParamUtil.getString(resourceRequest, "email");
		String phone = ParamUtil.getString(resourceRequest, "phone");
		String reg_com = ParamUtil.getString(resourceRequest, "reg_com");
		String payment_code = ParamUtil.getString(resourceRequest, "payment_code");
		String ro = ParamUtil.getString(resourceRequest, "ro");
		String bank = ParamUtil.getString(resourceRequest, "bank");
		String bank_acc = ParamUtil.getString(resourceRequest, "bank_acc");
		String swift = ParamUtil.getString(resourceRequest, "swift");
		String id_company = ParamUtil.getString(resourceRequest, "id_company");
		String payment_term = ParamUtil.getString(resourceRequest, "payment_term");
		String address = ParamUtil.getString(resourceRequest, "new_address");
    	String supplier_type = ParamUtil.getString(resourceRequest, "supplier_type_db").equals("") ? "0" : ParamUtil.getString(resourceRequest, "supplier_type_db") ;
    	String tert_group = ParamUtil.getString(resourceRequest, "supplier_tert_group_popup");
		// set suppliers excludes
		this.setSupplierExcludes();
		// init insert data
		String[][] arrFields = new String[17][2];
		arrFields[0][0] = "name";
		arrFields[0][1] = name;
		arrFields[1][0] = "cui";
		arrFields[1][1] = cui;
		arrFields[2][0] = "email";
		arrFields[2][1] = email;
		arrFields[3][0] = "phone";
		arrFields[3][1] = phone;
		arrFields[4][0] = "bank";
		arrFields[4][1] = bank;
		arrFields[5][0] = "bank_acc";
		arrFields[5][1] = bank_acc;
		arrFields[6][0] = "swift";
		arrFields[6][1] = swift;
		arrFields[7][0] = "payment_code";
		arrFields[7][1] = payment_code;
		arrFields[8][0] = "RO";
		if (ro.equals("false")) {
			arrFields[8][1] = "";
		} else
			arrFields[8][1] = "RO";
		arrFields[9][0] = "reg_com";
		arrFields[9][1] = reg_com;
		arrFields[10][0] = "stage";
		arrFields[10][1] = String.valueOf(SupplierStages.APPROVED_BY_ACCOUNTING);
		arrFields[11][0] = "id_company";
		arrFields[11][1] = id_company;
		arrFields[12][0] = "payment_term";
		arrFields[12][1] = payment_term;
		arrFields[13][0] = "supplier_type";
		arrFields[13][1] = supplier_type;
		arrFields[14][0] = "address";
		arrFields[14][1] = address;
		arrFields[15][0] = "tert_group";
		arrFields[15][1] = tert_group;
		arrFields[16][0] = "id_user";
		arrFields[16][1] = String.valueOf(user.getUserId());
		
		// where statement
		ArrayList<String> columns = new ArrayList<String>();
		columns.add("id");
		// where statement values
		ArrayList<String> values = new ArrayList<String>();
		values.add(id);
		try {
			// update supplier
			GenericDBStatements.updateEntry(arrFields, "def_suppliers", this.suppliersExcludes, columns, values);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Default AJAX method in the application framework.
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void serveResource(ResourceRequest resourceRequest, ResourceResponse resourceResponse) throws IOException, PortletException {
		log.info("[AJAX] Invoice Controller");
		
		User user = null;
    	try{
    		user = UserServiceUtil.getUserById(Long.parseLong(resourceRequest.getRemoteUser()));
    	}catch (Exception e) {}
    	
		String action = ParamUtil.getString(resourceRequest, "action");
		log.info("[AJAX] action: " + action);
		PrintWriter writer = resourceResponse.getWriter();
		if (action.equals("filterDpis")){
			String storeFilter = ParamUtil.getString(resourceRequest, "store").toString();
			if (!storeFilter.equals("0")){
				storeFilter = DefStore.getStoreIdById(storeFilter);
			}
			String prodFilter = ParamUtil.getString(resourceRequest, "prod").toString();
			String valuefilter = ParamUtil.getString(resourceRequest, "value").toString();
			
			
			List<HashMap<String, Object>> filteredDpis = DPIHeader.getAllDpiFilteres(storeFilter, prodFilter, valuefilter);
			System.out.println(filteredDpis.size());
			String result = DropDownUtils.generateDropDownWithTwoNameColumn(filteredDpis, "--------------", "id", "id", "dpi_date");
			JSONObject jsonResponse = new JSONObject();
			jsonResponse.put("dpis", result);
			writer.print(jsonResponse.toJSONString());
		} else if (action.equals("getDpiCategCapex")){
			String dpi = ParamUtil.getString(resourceRequest, "id_dpi").toString();
			List<HashMap<String, Object>> dpiInfo = DPIHeader.getColumn("id", "*", "(" + dpi + ")");
			JSONObject jsonResponse = new JSONObject();
			jsonResponse.put("tipCapex", dpiInfo.get(0).get("tip_capex").toString());
			jsonResponse.put("project", dpiInfo.get(0).get("id_new_prj").toString());
			jsonResponse.put("init", dpiInfo.get(0).get("id_initiative").toString());
			
			writer.print(jsonResponse.toJSONString());
		} else if (action.equals("getInventoryNo")) {
			int storeId = Integer.parseInt(ParamUtil.getString(resourceRequest, "store").toString());
			int iasId = Integer.parseInt(ParamUtil.getString(resourceRequest, "iasId").toString());
			int tipInv = Integer.parseInt(ParamUtil.getString(resourceRequest, "tipInv").toString());
			int count = (int) Float.parseFloat(ParamUtil.getString(resourceRequest, "count"));
			String idNewPrj = ParamUtil.getString(resourceRequest, "id_new_prj").toString();

			List<String> listInvNo = null;

			if (iasId != 0 && storeId != 0 && tipInv != 0) {
				if (tipInv == InventoryNumberBO.DE_GRUP) {
					listInvNo = InventoryNumberBO.getInvetoryNumberSequence(storeId, iasId, 1);
				} else if (tipInv == InventoryNumberBO.INDIVIDUAL) {
					listInvNo = InventoryNumberBO.getInvetoryNumberSequence(storeId, iasId, count);
				}
			}

			System.out.println("Secventa de numere de inventar este : " + listInvNo);
			
			String pifDate = InvoiceHeader.getPifDateByNewPrjId(idNewPrj);
			
			JSONArray jsonInvNo = new JSONArray();
			if (listInvNo != null) {
				for (int i = 0; i < listInvNo.size(); i++) {
					JSONObject jsonLine = new JSONObject();
					jsonLine.put("nr_inv", listInvNo.get(i).toString());
					jsonLine.put("data_pif", pifDate);
					jsonLine.put("receptie", "");
					jsonInvNo.add(jsonLine);
				}
				writer.print(jsonInvNo.toJSONString());
			} else {
				writer.print("");
			}
		} else if (action.equals("getinventoryHistory")) {
			String storeId = ParamUtil.getString(resourceRequest, "storeIdInv").toString();
			String invNo = ParamUtil.getString(resourceRequest, "invNo").toString();
			if(InvoiceSequence.getInventoryHistory(Integer.parseInt(storeId), invNo)) {
				writer.print("success");
			} else {
				writer.print("eroare");
			}
			
		} else if (action.equals("getInventoryDetails")){
			String idStoreLine = ParamUtil.getString(resourceRequest, "idStoreLine").toString();
			List<HashMap<String,Object>> inventoryDetails = InvoiceHeader.getInventoryDetailsByIdStoreLine(idStoreLine);
			JSONArray jsonInvNo = new JSONArray();
			if (inventoryDetails != null) {
				for (int i = 0; i < inventoryDetails.size(); i++) {
					JSONObject jsonLine = new JSONObject();
					jsonLine.put("nr_inv", inventoryDetails.get(i).get("inventory_no").toString());
					jsonLine.put("data_pif", inventoryDetails.get(i).get("pif_date").toString());
					jsonLine.put("receptie", inventoryDetails.get(i).get("reception").toString());
					jsonInvNo.add(jsonLine);
				}
				writer.print(jsonInvNo.toJSONString());
			} else {
				writer.print("");
			}
			
		} else if(action.equals("setFlag")) {
			String invoiceId = ParamUtil.getString(resourceRequest, "invoiceId").toString();
			int value = Integer.parseInt(ParamUtil.getString(resourceRequest, "value").toString());
			if(value == 1) {
				InvoiceHeader.setInProcessFlag(invoiceId, value, user.getUserId());
			} else {
				InvoiceHeader.setInProcessFlag(invoiceId, value, 0);
			}
			
			writer.print("success");
		} else if(action.equals("checkAccess")) {
			int invoiceId = Integer.parseInt(ParamUtil.getString(resourceRequest, "invoiceId").toString());
			int userId = Integer.parseInt(ParamUtil.getString(resourceRequest, "userId").toString());
			boolean access = InvoiceHeader.checkApproverAccess(userId, invoiceId);
			writer.print(access);
		} else if (action.equals("getBaseInvoices")) {
			String supplier_name = ParamUtil.getString(resourceRequest, "supplier_name").toString();
			String id_company = ParamUtil.getString(resourceRequest, "id_company").toString();
			List<HashMap<String, Object>> baseInvoices = null;

			baseInvoices = InvoiceHeader.getBaseInvoicesFromSupplier(supplier_name, id_company);

			JSONArray jsonBaseInvoices = new JSONArray();
			if (baseInvoices != null) {
				jsonBaseInvoices = DatabaseConnectionManager.convertListToJson(baseInvoices);
			}
			//System.out.println(jsonBaseInvoices);
			JSONObject jsonResponse = new JSONObject();
			jsonResponse.put("baseInvoices", jsonBaseInvoices);
			// send AJAX response
			writer.print(jsonResponse.toJSONString());
		} else if (action.equals("deleteLine")) {
			// id of the invoice in invoice_line
			System.out.println(action);
			String[] ids = ParamUtil.getString(resourceRequest, "line_id").toString().split(",");
			System.out.println("controller : " + ParamUtil.getString(resourceRequest, "line_id").toString());
			int error = 0;
			for (String lineId : ids){
				try {
					error = GenericDBStatements.deleteEntry("invoice_line", "id", lineId);
					GenericDBStatements.deleteEntry("invoice_store_line", "line_id", lineId);
				} catch (Exception e) {	}
				if(error != 0) {
					//Save lineId in log
					String arrFields [][] = new String [2][2];
					arrFields[0][0] = "line_id";
					arrFields[0][1] = lineId;
					arrFields[1][0] = "user_id";
					arrFields[1][1] = String.valueOf(user.getUserId());
					GenericDBStatements.insertEntry(arrFields, "log_deleted_lines", new ArrayList<String>());
				} else {
					break;
				}
			}
			// create JSON response
			JSONObject jsonResponse = new JSONObject();
			jsonResponse.put("error", error);
			// send AJAX response
			writer.print(jsonResponse.toJSONString());

		} else if (action.equals("deleteStoreLine")) {
			// id of the store line in invoice_store_line
			String storeLineId = ParamUtil.getString(resourceRequest, "id").toString();
			// index of the store line in the local array
			String index = ParamUtil.getString(resourceRequest, "index").toString();
			int error = 0;
			try {
				error = GenericDBStatements.deleteEntry("invoice_store_line", "id", storeLineId);
			} catch (Exception e) {
			}
			if(error != 0) {
				//Save storeLineId in log
				String arrFields [][] = new String [2][2];
				arrFields[0][0] = "store_line_id";
				arrFields[0][1] = storeLineId;
				arrFields[1][0] = "user_id";
				arrFields[1][1] = String.valueOf(user.getUserId());
				GenericDBStatements.insertEntry(arrFields, "log_deleted_lines", new ArrayList<String>());
			}
			// create JSON response
			JSONObject jsonResponse = new JSONObject();
			jsonResponse.put("error", error);
			jsonResponse.put("index", index);
			// send AJAX response
			writer.print(jsonResponse.toJSONString());

		} else if (action.equals("getHistory")) {
			// get the id of the invoice
			String invoiceID = ParamUtil.getString(resourceRequest, "id").toString();
			// get the names and jobTitles from all approvers
			List<HashMap<String, Object>> approverInfo = InvoiceHeader.getApproversHistory(invoiceID);
			List<HashMap<String, Object>> tableData = UserTeamIdUtils.getApproverInfoFromApproverId(approverInfo);
			JSONArray jsonApproverInfo = new JSONArray();
			if (tableData != null) {
				jsonApproverInfo = DatabaseConnectionManager.convertListToJson(tableData);
			}
			// create JSON response
			JSONObject jsonResponse = new JSONObject();
			jsonResponse.put("values", jsonApproverInfo);
			// send AJAX response
			writer.print(jsonResponse.toJSONString());
		} else if (action.equals("getExchangeRate")) {
			String date = ParamUtil.getString(resourceRequest, "date").toString();
			String selected_currency = ParamUtil.getString(resourceRequest, "currency_sel").toString();
			if (!date.equals("")) {
				float exchange_rate = 0;
				try {
					exchange_rate = DefExchRates.getExchangeRate(selected_currency, date, 1);
				} catch (Exception e) {
				}
				// create JSON response
				JSONObject jsonResponse = new JSONObject();
				jsonResponse.put("exchange_rate", exchange_rate);
				// send AJAX response
				writer.print(jsonResponse.toJSONString());
			}
		} else if (action.equals("requestSupplierCreation")) {
			String supplierId = ParamUtil.getString(resourceRequest, "supplierIdCreation").toString();
			if (supplierId.equals("")) {
				// create supplier
				int resultedId = this.createSupplier(resourceRequest, user);
				//adding notification
				this.addSupplierRequestNotificationToAccountingDirector(resultedId, user);
				this.addSupplierRequestNotificationToSupplierApprovals(resultedId, user);
				log.info("[AJAX] created supplier id : " + resultedId);
				// create JSON response
				JSONArray jsonSuppliers = new JSONArray();
				List<HashMap<String, Object>> supplierData = DefSupplier.getSupplierInfo(String.valueOf(resultedId));
				if (supplierData != null) {
		    		jsonSuppliers = DatabaseConnectionManager.convertListToJson(supplierData);
		    	}
				JSONObject jsonResponse = new JSONObject();
				jsonResponse.put("id", resultedId);
				jsonResponse.put("supplier_data", jsonSuppliers);
				jsonResponse.put("message", "Furnizorul a fost modificat cu succes!");
				// send AJAX response
				writer.print(jsonResponse.toJSONString());
			} else {
				log.info("[AJAX] update supplier id : " + supplierId);
				// update supplier
				this.updateSupplier(supplierId, resourceRequest, user);
				// create JSON response
				JSONArray jsonSuppliers = new JSONArray();
				List<HashMap<String, Object>> supplierData = DefSupplier.getSupplierInfo(String.valueOf(supplierId));
				if (supplierData != null) {
		    		jsonSuppliers = DatabaseConnectionManager.convertListToJson(supplierData);
		    	}
				this.addSupplierRequestNotificationToAccountingDirector(Integer.parseInt(supplierId), user);
				this.addSupplierRequestNotificationToSupplierApprovals(Integer.parseInt(supplierId), user);
				JSONObject jsonResponse = new JSONObject();
				jsonResponse.put("supplier_data", jsonSuppliers);
				jsonResponse.put("id", supplierId);
				jsonResponse.put("message", "Furnizorul a fost modificat cu succes!");
				// send AJAX response
				writer.print(jsonResponse.toJSONString());
			}
			
		} else if (action.equals("showDpi")) {
			String idDpi = ParamUtil.getString(resourceRequest, "dpi");
			// get DPI header info based on DPI id
			List<HashMap<String, Object>> oneDpi = null;

			// oneDpi = GenericDBStatements.getAllByFieldName("dpi_header",  "id", idDpi);
			oneDpi = DPIHeader.getAllByFieldName("id", idDpi);

			JSONArray jsonDpiHeader = new JSONArray();
			if (oneDpi != null) {
				jsonDpiHeader = DatabaseConnectionManager.convertListToJson(oneDpi);
			}

			// get DPI lines info based on DPI id
			List<HashMap<String, Object>> dpiLines = null;

			dpiLines = GenericDBStatements.getAllByFieldName("dpi_line", "id_dpi", idDpi);

			JSONArray jsonDpiLines = new JSONArray();
			if (dpiLines != null) {
				jsonDpiLines = DatabaseConnectionManager.convertListToJson(dpiLines);
			}

			// create JSON response
			JSONObject jsonResponse = new JSONObject();
			jsonResponse.put("header", jsonDpiHeader);
			jsonResponse.put("lines", jsonDpiLines);
			// send AJAX response
			writer.print(jsonResponse.toJSONString());
		} else if (action.equals("getProdInfo")) {
			// ajax to get product info for one row based on id of the selected
			// product
			String id = ParamUtil.getString(resourceRequest, "prodId").toString();
			List<HashMap<String, Object>> product = DefProductVo.getById(id);
			JSONArray jsonProduct = new JSONArray();
			if (product != null) {
				jsonProduct = DatabaseConnectionManager.convertListToJson(product);
			}
			JSONObject jsonResponse = new JSONObject();
			jsonResponse.put("product", jsonProduct);
			//System.out.println(jsonProduct);
			writer.print(jsonResponse.toJSONString());
		} else if (action.equals("getIasInfo")){
			String id = ParamUtil.getString(resourceRequest, "iasId").toString();
			List<HashMap<String, Object>> product = DefIASVo.getById(id);
			JSONArray jsonProduct = new JSONArray();
			if (product != null) {
				jsonProduct = DatabaseConnectionManager.convertListToJson(product);
			}
			JSONObject jsonResponse = new JSONObject();
			jsonResponse.put("product", jsonProduct);
			//System.out.println(jsonProduct);
			writer.print(jsonResponse.toJSONString());
		} else if (action.equals("getIfrsInfo")){
			String id = ParamUtil.getString(resourceRequest, "ifrsId").toString();
			List<HashMap<String, Object>> product = DefIFRSVo.getById(id);
			JSONArray jsonProduct = new JSONArray();
			if (product != null) {
				jsonProduct = DatabaseConnectionManager.convertListToJson(product);
			}
			JSONObject jsonResponse = new JSONObject();
			jsonResponse.put("product", jsonProduct);
			//System.out.println(jsonProduct);
			writer.print(jsonResponse.toJSONString());
		} else if (action.equals("findSupplier")) {
			String query = ParamUtil.getString(resourceRequest, "keywords");
			String idCompany = ParamUtil.getString(resourceRequest, "id_company");
			log.info("[AJAX] Supplier query: " + query);

			writer.print(ControllerUtils.filterSupplierByName(query, Integer.parseInt(idCompany)).toJSONString());
		} else if ( action.equals("getStoreAislesByStore")){
			String storeId = ParamUtil.getString(resourceRequest, "storeId").toString();
			System.out.println(storeId);
			writer.print(DefAisles.getJsonStringAisles(storeId));
		} else if ( action.equals("getDpisByAisle")){
			String storeId = ParamUtil.getString(resourceRequest, "storeId").toString();
			String raion = ParamUtil.getString(resourceRequest, "raionOrDept").toString();
			List<HashMap<String,Object>> allDpisByAisle = DPIHeader.getDpiByAisle(storeId, raion);
			JSONArray jsonProduct = new JSONArray();
			if (allDpisByAisle != null) {
				jsonProduct = DatabaseConnectionManager.convertListToJson(allDpisByAisle);
			}
			JSONObject jsonResponse = new JSONObject();
			jsonResponse.put("allDpisByAisle", jsonProduct);
			//System.out.println(jsonProduct);
			writer.print(jsonResponse.toJSONString());
		} else if ( action.equals("getAdvanceRequests") ) {
			log.info("[AJAX] Invoices - get all advance requests ids by company");
			String companyId = ParamUtil.getString(resourceRequest, "company");
			String supplierName = ParamUtil.getString(resourceRequest, "supplierName");
			List<HashMap<String,Object>> storesDropdown = DefStore.getAllByCompanyForDropdown(companyId);
			List<HashMap<String,Object>> list = AdvanceRequest.getAdvanceRequestIdByCompanyIdAndSupplier(companyId, supplierName);
			JSONArray jsonProduct = new JSONArray();
			JSONArray jsonStores = new JSONArray();
			if(list!=null) {
				jsonProduct = DatabaseConnectionManager.convertListToJson(list);
			}
			if(storesDropdown != null) {
				jsonStores = DatabaseConnectionManager.convertListToJson(storesDropdown);
			}
			JSONObject jsonResponse = new JSONObject();
			jsonResponse.put("list", jsonProduct);
			jsonResponse.put("storesDropdown", jsonStores);
			//System.out.println(jsonProduct);
			writer.print(jsonResponse.toJSONString());
		} else if(action.equals("getDPIInfo")) { 
			String storeLineIds = ParamUtil.getString(resourceRequest, "storeLineIds");
			List<HashMap<String,Object>> list = DPIHeader.getDPIRealizat(storeLineIds);
			BigDecimal total = new BigDecimal(0);
			BigDecimal invTotal = new BigDecimal(0);
			
			for(HashMap<String,Object> hash : list) {
				BigDecimal val = new BigDecimal(hash.get("valoare_dpi").toString());
				total = total.add(val);
				invTotal = invTotal.add(new BigDecimal(hash.get("valoare_fact_emise").toString()));
			}
			JSONArray jsonInfo = new JSONArray();
			if (list != null) {
				jsonInfo = DatabaseConnectionManager.convertListToJson(list);
			}
			// create JSON response
			JSONObject jsonResponse = new JSONObject();
			jsonResponse.put("values", jsonInfo);
			jsonResponse.put("valoare_dpi", total);
			jsonResponse.put("valoare_facturi", invTotal);
			jsonResponse.put("diferenta", total.subtract(invTotal));
			// send AJAX response
			writer.print(jsonResponse.toJSONString());
		} else if (action.equals("getBuget")) {
			String invId = ParamUtil.getString(resourceRequest, "id");
			List<HashMap<String,Object>> list = InvoiceHeader.getRealizareBuget(invId);
			JSONArray jsonInfo = new JSONArray();
			if (list != null) {
				jsonInfo = DatabaseConnectionManager.convertListToJson(list);
			}
			// create JSON response
			JSONObject jsonResponse = new JSONObject();
			jsonResponse.put("values", jsonInfo);
			// send AJAX response
			writer.print(jsonResponse.toJSONString());
		} else if (action.equals("filter") || action.equals("next") || action.equals("prev") || action.equals("last") || action.equals("first")) {
			// read AJAX parameters used in filtering and pagination
			int start = Integer.parseInt(ParamUtil.getString(resourceRequest, "start").toString());
			int count = Integer.parseInt(ParamUtil.getString(resourceRequest, "count").toString());
			int total = Integer.parseInt(ParamUtil.getString(resourceRequest, "total").toString());
			int companyId = Integer.parseInt(ParamUtil.getString(resourceRequest, "company_id").toString());
			String storeName = ParamUtil.getString(resourceRequest, "storeName");
			String moneda = ParamUtil.getString(resourceRequest, "moneda");
			String number = "0";
			if (ParamUtil.getString(resourceRequest, "number") != ""){
				number = ParamUtil.getString(resourceRequest, "number").toString();
			}
			String descriere = ParamUtil.getString(resourceRequest, "descriere");
			String datafactura = ParamUtil.getString(resourceRequest, "datafactura");
			String datainreg = ParamUtil.getString(resourceRequest, "datainreg");
			String supplierCode = ParamUtil.getString(resourceRequest, "supplier_code");
			String supplierName = ParamUtil.getString(resourceRequest, "supplier_name");
			String magazinName = ParamUtil.getString(resourceRequest, "magazin_name");
			String categorie = ParamUtil.getString(resourceRequest, "categorie");
			String pageLayout = ParamUtil.getString(resourceRequest, "pageLayout");
			List<HashMap<String, Object>> allInvoices = new ArrayList<HashMap<String,Object>>();
	    	String user_email = "";
	    	try{
	    		user_email = user.getEmailAddress();
	    	}catch (Exception e){}
			log.info("Getting all filtered!");
			
			if(pageLayout.equals(InvoiceStages.PAGE_FACTURI_NEALOCATE)){
				allInvoices = InvoiceHeader.getAllFilteredForAccounting(start, count, user.getUserId(), companyId, moneda, number, storeName,
						datafactura, datainreg, supplierCode, supplierName, magazinName, user_email, categorie, "1", descriere);
				total = InvoiceHeader.getAllFilteredCountForAccounting(user.getUserId(), companyId, moneda, number, storeName,
					datafactura, datainreg, supplierCode, supplierName, magazinName, user_email, categorie, "1", descriere);
			} else if (pageLayout.equals(InvoiceStages.PAGE_CONTABILITATE_ASTEAPTA_VALIDARE)){
				allInvoices = InvoiceHeader.getAllFilteredForAccounting(start, count, user.getUserId(), companyId, moneda, number, storeName,
						datafactura, datainreg, supplierCode, supplierName, magazinName, user_email, categorie, "3", descriere);
				total = InvoiceHeader.getAllFilteredCountForAccounting(user.getUserId(), companyId, moneda, number, storeName,
					datafactura, datainreg, supplierCode, supplierName, magazinName, user_email, categorie, "3", descriere);
			} else if(pageLayout.equals(InvoiceStages.KONTAN_PAGE)) {
				allInvoices = InvoiceHeader.getAllFiltered(start, count, companyId, moneda, number, 10, 12, "", datafactura, datainreg, supplierCode, supplierName, "", categorie, user);
				total = InvoiceHeader.getAllFilteredCount(start, count, companyId, moneda, number, 10, 12, "", datafactura, datainreg, supplierCode, supplierName, "", categorie, user);
			} else {
				allInvoices = InvoiceHeader.getAllFiltered(start, count, user.getUserId(), companyId, moneda, number, storeName,
							datafactura, datainreg, supplierCode, supplierName, magazinName, user_email, categorie, descriere);
				total = InvoiceHeader.getAllFilteredCount(user.getUserId(), companyId, moneda, number, storeName,
						datafactura, datainreg, supplierCode, supplierName, magazinName, user_email, categorie, descriere);
			}
			
			
			
			JSONArray jsonInvoices = new JSONArray();
			if (allInvoices != null) {
				jsonInvoices = DatabaseConnectionManager.convertListToJson(allInvoices);
			}
			JSONObject jsonResponse = new JSONObject();

			jsonResponse.put("action", action);
			jsonResponse.put("start", start);
			jsonResponse.put("count", count);
			jsonResponse.put("total", total);
			jsonResponse.put("values", jsonInvoices);
			writer.print(jsonResponse.toJSONString());
		}

		// super.serveResource(resourceRequest, resourceResponse);
	}

	/**
	 * 
	 * @param invoice
	 * @param store_line
	 * @param inventory_data
	 * @return
	 */
	public int updateInventoryNo(int invoice, int store_line, String[] inventory_data) {
		//System.out.println(inventory_data[1]);
		String[][] arrFields = new String[3][2];
		arrFields[0][0] = "inventory_no";
		arrFields[1][0] = "pif_date";
		arrFields[2][0] = "reception";

		Object obj = JSONValue.parse(inventory_data[1]);
		JSONArray jsonArr = (JSONArray) obj;
		System.out.println("############### " + jsonArr);
		for (int k = 0; k < jsonArr.size(); k++) {
			JSONObject jsonObject = (JSONObject) jsonArr.get(k);
			boolean isPifDate = false;
			String pifDate = "";
			if (jsonObject.get("pif_date") != null) {
				pifDate = ((JSONObject) jsonArr.get(k)).get("pif_date").toString().replace("[{\"", "").replace("\"}]", "");
				isPifDate = true;
			} else if(jsonObject.get("data_pif") != null) {
				pifDate = ((JSONObject) jsonArr.get(k)).get("data_pif").toString().replace("[{\"", "").replace("\"}]", "");
				isPifDate = false;
			}
			if ( pifDate.startsWith("[")) {
				pifDate = pifDate.substring(2, pifDate.length());
			}
			if ( pifDate.endsWith("]")) {
				pifDate = pifDate.substring(0, pifDate.length()-2);
			}
			arrFields[1][1] = pifDate;
			if(isPifDate){
				arrFields[0][1] = jsonObject.get("inventory_no").toString();
			} else {
				arrFields[0][1] = jsonObject.get("nr_inv").toString();
			}
			if(isPifDate) {
				arrFields[2][1] = jsonObject.get("reception").toString();
			} else {
				arrFields[2][1] = jsonObject.get("receptie").toString();
			}
			

			ArrayList<String> excludedFieldsInv = new ArrayList<String>();
			// attrs for where statement
			ArrayList<String> whereStatement = new ArrayList<String>();
			// attrs values for where statement
			ArrayList<String> whereStatementValues = new ArrayList<String>();

			whereStatement.add("id_store_line");
			whereStatement.add("id_invoice");
			whereStatementValues.add(String.valueOf(store_line));
			whereStatementValues.add(String.valueOf(invoice));
			int result = GenericDBStatements.updateEntry(arrFields, "invoice_store_line_inv", excludedFieldsInv, whereStatement, whereStatementValues);
			if (result <= 0) {
				return 0;
			}
		}
		return 1;
	}

	/**
	 * 
	 * @param invoice
	 * @param store_line
	 * @param inventory_data
	 * @return
	 */
	public int insertInventoryNo(int invoice, int store_line, String[] inventory_data) {
		//System.out.println(inventory_data[1]);
		String[][] arrFields = new String[5][2];
		arrFields[0][0] = "id_store_line";
		arrFields[0][1] = String.valueOf(store_line);
		arrFields[1][0] = "id_invoice";
		arrFields[1][1] = String.valueOf(invoice);
		arrFields[2][0] = "inventory_no";
		arrFields[3][0] = "pif_date";
		arrFields[4][0] = "reception";

		Object obj = JSONValue.parse(inventory_data[1]);
		JSONArray jsonArr = (JSONArray) obj;
		for (int k = 0; k < jsonArr.size(); k++) {
			String pifDate = ((JSONObject) jsonArr.get(k)).get("data_pif").toString().replace("[\"", "").replace("\"]", "");
			if(pifDate.startsWith("[")) {
				pifDate = pifDate.substring(1);
			}
			if(pifDate.endsWith("]")){
				pifDate = pifDate.substring(0, pifDate.length()-1);
			}
			arrFields[3][1] = pifDate;
			arrFields[2][1] = ((JSONObject) jsonArr.get(k)).get("nr_inv").toString();
			arrFields[4][1] = ((JSONObject) jsonArr.get(k)).get("receptie").toString();

			ArrayList<String> excludedFieldsInv = new ArrayList<String>();
			GenericDBStatements.insertEntry(arrFields, "invoice_store_line_inv", excludedFieldsInv);
		}
		return 1;
	}
	
	/**
	 * 5.03.2016
	 * Returns a hashmap containing as keys - invoice store line ids and as values - associated store ids
	 * @param storeLines = INVOICE STORE LINES
	 * @param ids = INVOICE STORE LINES IDS
	 * @return
	 */
	public HashMap<String, String> getStoresPerStoreLine(String [][][] storeLines, HashMap<Integer, Integer> ids){
		HashMap<String, String> stores = new HashMap<String, String>();
		for (int i = 0; i < storeLines.length; i++){
			if (ids.containsKey(i)){
				int index = getColumnIndex(storeLines[i], "id_store");
				stores.put(String.valueOf(ids.get(i)), storeLines[i][index][1]);
			}
		}
		return stores;
	}
	
	/**
	 * 5.03.2016
	 * Returns a hashmap containing as keys - invoice store line ids and as values - associated aisles ids
	 * @param storeLines = INVOICE STORE LINES
	 * @param ids = INVOICE STORE LINES IDS
	 * @return
	 */
	public HashMap<String, String> getAislesPerStoreLine(String [][][] storeLines, HashMap<Integer, Integer> ids){
		HashMap<String, String> aisles = new HashMap<String, String>();
		for (int i = 0; i < storeLines.length; i++){
			if (ids.containsKey(i)){
				int index = getColumnIndex(storeLines[i], "id_department");
				aisles.put(String.valueOf(ids.get(i)), storeLines[i][index][1]);
			}
		}
		return aisles;
	}
	
	
	/**
	 * 5.03.2016
	 * Returns a hashmap containing as keys - invoice store line ids and as values - associated dpi ids
	 * @param storeLines = INVOICE STORE LINES
	 * @param ids = INVOICE STORE LINES IDS
	 * @return
	 */
	public HashMap<String, String> getDpisPerStoreLine(String [][][] storeLines, HashMap<Integer, Integer> ids){
		HashMap<String, String> dpis = new HashMap<String, String>();
		for (int i = 0; i < storeLines.length; i++){
			if (ids.containsKey(i)){
				int index = getColumnIndex(storeLines[i], "id_dpi");
				dpis.put(String.valueOf(ids.get(i)), storeLines[i][index][1]);
			}
		}
		return dpis;
	}
	
	/**
	 * 5.03.2016
	 * Returns a list of store line ids that have lot it true
	 * @param storeLines = INVOICE STORE LINES
	 * @param ids = INVOICE STORE LINES IDS
	 * @return
	 */
	public List<String> getItStoreLines(String[][][] storeLines, HashMap<Integer, Integer> ids){
		
		List<String> itStoreLines = new ArrayList<String>();
		for (int i = 0; i < storeLines.length; i++){
			for (int j = 0; j < storeLines[i].length; j++){
				if (storeLines[i][j][0].equals("product_code")){
					if (DefLot.hasITLot(storeLines[i][j][1])){
						itStoreLines.add(String.valueOf(ids.get(i)));
					}
				}
			}
		}
		return itStoreLines;
	}
	
	/**
	 * 
	 * @param storeLines
	 * @param ids
	 * @return
	 */
	public List<String> getAllStoreLines(String[][][] storeLines, HashMap<Integer, Integer> ids){
		List<String> allStoreLines = new ArrayList<String>();
		for(int i = 0; i < storeLines.length; i++) {
			for(int j = 0; j < storeLines[i].length; j++ ){
				if (storeLines[i][j][0].equals("product_code")){
					allStoreLines.add(String.valueOf(ids.get(i)));
				}
			}
		}
		return allStoreLines;
	}
	
	/**
	 * 5.03.2016
	 * Returns true if the invoice value is over 50.000 EUR
	 * @param header = INVOICE HEADER
	 * @return
	 */
	public boolean goToFinancialDirector(String[][] header){
		String valueRON = "";
		String valueCURR = "";
		String currency = "";
		String inv_date = "";
		for (int i = 0; i < header.length; i++){
			if (header[i][0].equals("total_with_vat_ron")){
				valueRON = header[i][1];
			}
			if (header[i][0].equals("total_with_vat_curr")){
				valueCURR = header[i][1];
			}
			if (header[i][0].equals("currency")){
				currency = header[i][1];
			}
			if (header[i][0].equals("inv_date")){
				inv_date = header[i][1];
			}
		}
		float exchangeRateEUR = DefExchRates.getExchangeRate("EUR", inv_date, 1);
		if(currency.equals("EUR")){
			if(Float.parseFloat(valueCURR) >= UserTeamIdUtils.FINANCIAL_DIRECTOR_APROVE_VALUE){
				return true;
			} else {
				return false;
			}
		} else if( Float.parseFloat(valueRON) / exchangeRateEUR >= UserTeamIdUtils.FINANCIAL_DIRECTOR_APROVE_VALUE ) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * 5.03.2016
	 * Levels of approval
	 */
	public HashMap<String, String> initializeApprovalLevels(){
		HashMap<String, String> levels = new HashMap<String, String>();
		levels.put("first", "1");
		levels.put("second", "2");
		levels.put("third", "3");
		return levels;
	}
	
	/**
	 * 5.03.2016
	 * Returns a hashMap of hashmaps implementing the following structure:
	 * storeLineId ---> approval level--->{user ids}
	 * 	(key)		\        (key)          (values)
	 * 				 \
	 * 					other approval level ---> {user ids} 
	 * 							(key)               (values)
	 * 
	 * @param header = INVOICE HEADER
	 * @param lines = INVOICE LINES
	 * @param storeLines = INVOICE STORE LINES
	 * @return
	 */
	public HashMap<String, HashMap<String, List<String>>> computeNextApprovers(String[][] header, String[][][] storeLines,
			HashMap<Integer, Integer> storeLinesIds){
		/**
		 * Facturile cu documente lipsa (indiferent de societate sau categoria capex selectata):
			�	Daca nu exista DPI, liniile facturii vor putea fi vizualizate (si notificare trimisa) de catre responsabilii 
						departamentului selectat la inregistrare factura si de magazinul marcat la nivel de linie factura;
			�	Daca exista DPI, se vor notifica si vor fi afisate liniile facturii la initiatorul de DPI;
		 */
		
		/**
		 * Facturile trimise la validare vor respecta urmatoarele reguli (indiferent de societate sau categoria capex selectata):
			�	Facturile fara DPI (proiectele noi), vor fi validate de  departamentul (raionul) marcat la inregistrare factura (o singura aprobare necesara);
			�	Facturile cu DPI facut de la SEDIU, vor putea fi validate de tot departamentul initiatorului de DPI (aprobarea unui singur user din departament e suficienta). Se va elimina validarea magazinului.
			�	Facturile cu DPI de la magazin, vor fi aprobate de:
					(i)		initiator DPI;
					(ii)	director magazin;
			�	In functie de valoarea facturii, indiferent de cazul de mai sus, ultima validare va fi data de Directorul Financiar.
		 */
		System.out.println("missing docs =" + this.invoiceWithMissingDoc);
		String currentLevel = "first";
		HashMap<String, HashMap<String, List<String>>> nextApprovers = new HashMap<String, HashMap<String, List<String>>>();
		//facturile cu documente lipsa 
		if(invoiceWithMissingDoc ) {
			//pentru fiecare store line
			for(int i = 0 ; i < storeLines.length; i++) {
				String [][] storeLine = storeLines[i];
				String idDpi = "";
				String idMagazin = "";
				String idRaion = "";
				for(int j = 0 ; j < storeLine.length; j++) {
					//se preia id-ul de DPI
					if(storeLine[j][0].equals("id_dpi")) {
						idDpi = storeLine[j][1];
					}
					//se preia id-ul de magazin
					if(storeLine[j][0].equals("id_store")) {
						idMagazin = storeLine[j][1];
					}
					//se preia id-ul de raion
					if(storeLine[j][0].equals("id_department")) {
						idRaion = storeLine[j][1];
					}					
				}
				//daca nu exista dpi pe store_line linia merge la toti userii de pe raionul selectat + directorii de magazin
				if(idDpi.equals("0") || idDpi.isEmpty()) {
					HashMap<String, List<String>> hash = new HashMap<String, List<String>>();
					hash.put(currentLevel, InvoiceHeader.getAllUsersByStoreAndDepartment(idMagazin, idRaion));
					nextApprovers.put(String.valueOf(storeLinesIds.get(i)), hash);
				//daca exista dpi, linia merge la initiatorul de dpi
				} else {
					HashMap<String, List<String>> hash = new HashMap<String, List<String>>();
					hash.put(currentLevel, InvoiceHeader.getUserIdByDpiId(idDpi));
					nextApprovers.put(String.valueOf(storeLinesIds.get(i)), hash);
				}
			}
		//daca nu e factura cu lipsa documente ====> suntem pe aprobare
		} else {
			System.out.println("new project =" + this.isNewProject);
			//se verifica DPI-urile de pe store line
			
			if (isNewProject) {
				currentLevel = "1";
				for(int i = 0 ; i < storeLines.length; i++) {
					String [][] storeLine = storeLines[i];
					String idRaion = "";
					for(int j = 0 ; j < storeLine.length; j++) {
						//se preia id-ul de raion
						if(storeLine[j][0].equals("id_department")) {
							idRaion = storeLine[j][1];
						}					
					}
					//daca exista raion, linia merge la userii din raionul selectat
					if(!idRaion.equals("0") && !idRaion.isEmpty()) {
						HashMap<String, List<String>> hash = new HashMap<String, List<String>>();
						hash.put(currentLevel, InvoiceHeader.getAllUsersByDepartment(idRaion));
						nextApprovers.put(String.valueOf(storeLinesIds.get(i)), hash);
					}
				}
				//daca nu suntem pe proiect nou
			} else {
				currentLevel = "1";
				for(int i = 0 ; i < storeLines.length; i++) {
					String [][] storeLine = storeLines[i];
					String idRaion = "";
					String idDpi = "";
					String idMagazin = "";
					for(int j = 0 ; j < storeLine.length; j++) {
						//se preia id-ul de raion
						if(storeLine[j][0].equals("id_department")) {
							idRaion = storeLine[j][1];
						}		
						//se preia id-ul de DPI
						if(storeLine[j][0].equals("id_dpi")) {
							idDpi = storeLine[j][1];
						}
						//se preia id-ul de magazin
						if(storeLine[j][0].equals("id_store")) {
							idMagazin = storeLine[j][1];
						}
					}
					//daca este linie de IT se valideaza de director IT
					if(idRaion.equals("98")) {
						currentLevel = "1";
						HashMap<String, List<String>> hash = new HashMap<String, List<String>>();
						hash.put(currentLevel, InvoiceHeader.getAllUsersByDepartment(idRaion));
						nextApprovers.put(String.valueOf(storeLinesIds.get(i)), hash);
					//daca raionul de pe linie este 90 atunci linia se duce la dir. Property (update: 22.03.2018)
					} else if (idRaion.equals("90")) {
						currentLevel = "1";
						HashMap<String, List<String>> hash = new HashMap<String, List<String>>();
						hash.put(currentLevel, InvoiceHeader.getAllUsersByDepartment(idRaion));
						nextApprovers.put(String.valueOf(storeLinesIds.get(i)), hash);
					//verificam daca DPI-ul este facut de la sediu, in caz afirmativ se valideaza linia de departamentul creatorului de DPI
					} else 	if(DefStore.isHQ_DPI(idDpi)) {
						currentLevel = "1";
						HashMap<String, List<String>> hash = new HashMap<String, List<String>>();
						hash.put(currentLevel, InvoiceHeader.getAllUsersByDPIDepartment(idDpi));
						nextApprovers.put(String.valueOf(storeLinesIds.get(i)), hash);
						
					//daca este sediu se valideaza in functie de raion	
					} else if(DefStore.isHQ(idMagazin)){
						currentLevel = "1";
						HashMap<String, List<String>> hash = new HashMap<String, List<String>>();
						hash.put(currentLevel, InvoiceHeader.getAllUsersByDepartment(idRaion));
						nextApprovers.put(String.valueOf(storeLinesIds.get(i)), hash);
						
					//se valideaza linia de initiator dpi si director magazin
					} else {
						//initiator DPI, pe nivel first
						currentLevel = "1";
						HashMap<String, List<String>> hash = new HashMap<String, List<String>>();
						hash.put(currentLevel, InvoiceHeader.getUserIdByDpiId(idDpi));
						nextApprovers.put(String.valueOf(storeLinesIds.get(i)), hash);
						
						currentLevel = "2";
						hash.put(currentLevel, InvoiceHeader.getAllUsersByStore(idMagazin));
						nextApprovers.put(String.valueOf(storeLinesIds.get(i)), hash);
					}
				}
			}
			//FINANCIAL DIRECTOR
			if (goToFinancialDirector(header)) {
				currentLevel = "3";
				for(int j = 0 ; j < storeLinesIds.size(); j++) {
					HashMap<String, List<String>> hash = nextApprovers.get(String.valueOf(storeLinesIds.get(j)));
					hash.put(currentLevel, InvoiceHeader.getUserByTeamId(UserTeamIdUtils.FINANCIAL_DIRECTOR));					
					nextApprovers.put(String.valueOf(storeLinesIds.get(j)), hash);
				}
			}
		}
		
		/* OLD CODE
		System.out.println("new project =" + this.isNewProject);
		
		List<HashMap<String, Object>> allDpiUsers = null;
		//INIT APPROVAL LEVELS
		HashMap<String, String> levels = initializeApprovalLevels();
		//String currentLevel = "";
		if (invoiceWithMissingDoc){
			allDpiUsers = UsersInformations.getUserDetailsByTeamId(UserTeamIdUtils.DPI_RESPONSABLE);
		}
		//List of all store lines that have dpi
		List<String> hasDpi = new ArrayList<String>();
		//INITIALIZE NEXT APPROVERS LIST 
		for (int index : storeLinesIds.keySet()){
			HashMap<String, List<String>> hash = new HashMap<String, List<String>>();
			for (String key : levels.keySet()){
				hash.put(levels.get(key), new ArrayList<String>());
			}
			nextApprovers.put(String.valueOf(storeLinesIds.get(index)), hash);
		}
	
		boolean isBillaInvoice = false;
		
		//FACTURILE PE BILLA MERG LA IT SAU DIR ACTIVE + PROPERTY
		int companyId = Integer.parseInt(header[6][1]);
		if(companyId == 7 || companyId == 8 || companyId == 9) {
			isBillaInvoice = true;
			//ADAUGARE DIRECTORI IT PENTRU LINIILE IT
			List<String> itStoreLines = getItStoreLines(storeLines, storeLinesIds);
			if (itStoreLines.size() > 0){
				currentLevel = "first";
				// GET ALL IT USERS
				List<HashMap<String, Object>> itApprovers = UsersInformations.getUserDetailsByTeamId(UserTeamIdUtils.IT_DIRECTOR);
				// ASSOCIATE IT USERS TO ALL INVOICE STORE LINES
				//for each store line ids
				for (String storeLineId : itStoreLines){
					//get values for current store line id
					List<String> existingList = nextApprovers.get(storeLineId).get(levels.get(currentLevel));
					//for each director
					for (HashMap<String, Object> director : itApprovers){
						//append the new user id
						existingList.add(director.get("user_id").toString());
					}
					//set back the values
					nextApprovers.get(storeLineId).put(levels.get(currentLevel), existingList);
				}
			}
			//REMOVE IT STORE LINE FOR FURTHER USE IN FUNCTION
			HashMap<Integer, Integer> clone = new HashMap<Integer, Integer>();
			for (int id : storeLinesIds.keySet()){
				if (!itStoreLines.contains(storeLinesIds.get(id).toString())){
					clone.put(id, storeLinesIds.get(id));
				}
			}
			storeLinesIds.clear();
			for (int id : clone.keySet()){
				storeLinesIds.put(id, clone.get(id));
			}
			//daca sunt si linii non-IT
			if(storeLinesIds.size() > 0) {
				//adaugare directori regionali
//				List<HashMap<String,Object>> regionalDirectors 	= UsersInformations.getUserDetailsByTeamId(UserTeamIdUtils.DIRECTOR_REGIONAL);
//				HashMap<String, String> storesPerStoreLines = getStoresPerStoreLine(storeLines, storeLinesIds);
				currentLevel = "first";
				List<String> allStoreLines = getAllStoreLines(storeLines, storeLinesIds);
				for(String storeLineId : allStoreLines) {
					//get values for current store line id
					if(!storeLineId.equals("null")) {
						//preluare aprobatori pe primul nivel
						List<String> existingList = nextApprovers.get(storeLineId).get(levels.get("first"));
						List<HashMap<String,Object>> regionalDirectors 	= UsersInformations.getUserByStoreId(UsersInformations.getStoreIdByStoreLineId(storeLineId));
						//for each regional director
						if (regionalDirectors.isEmpty()){
							existingList.add("fictive_regional");
						} else
						{
							for (HashMap<String, Object> director : regionalDirectors){
								//append the new user id
	//							if(UsersInformations.isRegional(UsersInformations.getUserIdByLiferayId(director.get("user_id")), storesPerStoreLines.get(storeLineId))){
									//APPEND USER ID TO ASSOCIATED STORE LINE
									existingList.add(director.get("idUserLiferay").toString());
								} 
//							else {
								
//							}
						}
				
						//set back the values for first level
						nextApprovers.get(storeLineId).put(levels.get("first"), existingList);
						
						//preluare aprobatori pe al doilea nivel
						existingList = nextApprovers.get(storeLineId).get(levels.get("second"));
						int billaOwner = UsersInformations.getBillaOwner();
						if(billaOwner != 0) {
							existingList.add(String.valueOf(billaOwner));
						} else {
							existingList.add("fictive_billa");
						}
						//set back the values for second level
						nextApprovers.get(storeLineId).put(levels.get("second"), existingList);
					}
				}
			}
			
		}
		//daca nu e factura pe billa ramane neschimbat
		if(!isBillaInvoice) {
			//INITIATIVA PE PROPERTY
			if (isInitiative){
				int initiativeIndex = getColumnIndex(header, "id_initiative");
				System.out.println(" A reusit sa intre pe primul if -> " + header[initiativeIndex][1]);
				boolean isForProperty = ListaInit.isProperty(Integer.parseInt(header[initiativeIndex][1]));
				if(isForProperty) {
					currentLevel = "first";
					List<HashMap<String,Object>> allPropertyDirectors = UsersInformations.getUserDetailsByTeamId(UserTeamIdUtils.PROPERTY_DIRECTOR);
					List<String> allStoreLines = getAllStoreLines(storeLines, storeLinesIds);
					for(String storeLineId : allStoreLines) {
						//get values for current store line id
						List<String> existingList = nextApprovers.get(storeLineId).get(levels.get(currentLevel));
						//for each director
						for (HashMap<String, Object> director : allPropertyDirectors){
							//append the new user id
							existingList.add(director.get("user_id").toString());
						}
						//set back the values
						nextApprovers.get(storeLineId).put(levels.get(currentLevel), existingList);
					}
					return nextApprovers;
				}
			}
			//IT
			List<String> itStoreLines = getItStoreLines(storeLines, storeLinesIds);
			
			if (itStoreLines.size() > 0){
				currentLevel = "first";
				// GET ALL IT USERS
				List<HashMap<String, Object>> itApprovers = UsersInformations.getUserDetailsByTeamId(UserTeamIdUtils.IT_DIRECTOR);
				// ASSOCIATE IT USERS TO ALL INVOICE STORE LINES
				//for each store line ids
				for (String storeLineId : itStoreLines){
					//get values for current store line id
					List<String> existingList = nextApprovers.get(storeLineId).get(levels.get(currentLevel));
					//for each director
					for (HashMap<String, Object> director : itApprovers){
						//append the new user id
						existingList.add(director.get("user_id").toString());
					}
					//set back the values
					nextApprovers.get(storeLineId).put(levels.get(currentLevel), existingList);
				}
			}
			//REMOVE IT STORE LINE FOR FURTHER USE IN FUNCTION
			HashMap<Integer, Integer> clone = new HashMap<Integer, Integer>();
			for (int id : storeLinesIds.keySet()){
				if (!itStoreLines.contains(storeLinesIds.get(id).toString())){
					clone.put(id, storeLinesIds.get(id));
				}
			}
			storeLinesIds.clear();
			for (int id : clone.keySet()){
				storeLinesIds.put(id, clone.get(id));
			}
			//DPIS
			HashMap<String, String> dpisPerStoreLines = getDpisPerStoreLine(storeLines, storeLinesIds);
			HashMap<String, String> storesPerStoreLines = getStoresPerStoreLine(storeLines, storeLinesIds);
			HashMap<String, String> aislesPerStoreLines = getAislesPerStoreLine(storeLines, storeLinesIds);
			String idDpi = "";
			currentLevel = "first";
			for (String id : dpisPerStoreLines.keySet()){
				idDpi = dpisPerStoreLines.get(id);
				if (!idDpi.equals("") && !idDpi.equals("0")){
					hasDpi.add(id);
					//ASSOCIATE DPI USER IDS TO STORE LINE IDS
					List<HashMap<String, Object>> emails = DPIHeader.getColumn("id", "user_email", "(" + idDpi + ")");
					List<HashMap<String, Object>> dpiUserIds = UsersInformations.getColumn("user_id", "email", emails.get(0).get("user_email").toString());
					//APPEND USER ID TO ASSOCIATED STORE LINE
					List<String> existingList = nextApprovers.get(id).get(levels.get(currentLevel));
					if(dpiUserIds.size() > 0 ){
						existingList.add(dpiUserIds.get(0).get("user_id").toString());
					} else {
						existingList.add("fictive_user_dpi");
					}
					nextApprovers.get(id).put(levels.get(currentLevel), existingList);
				} else {
					if (invoiceWithMissingDoc && !isNewProject){
						//GET DPI RESPONSABLE FROM STORE
						String storeId = DefStore.getStoreIdById(storesPerStoreLines.get(id));
						boolean isHQ = DefStore.isHQ(storesPerStoreLines.get(id));
						List<String> existingList = nextApprovers.get(id).get(levels.get(currentLevel));
						for (HashMap<String, Object> user : allDpiUsers){
							User dpiUser = null;
							try {
								dpiUser = UserLocalServiceUtil.getUser(Integer.parseInt(user.get("user_id").toString()));
								if (dpiUser != null){
									if(storeId.equals(dpiUser.getAddresses().get(0).getStreet2().toString())){
										//IF IS HQ CHECK THE AISLE
										if (isHQ){
											if (aislesPerStoreLines.get(id).equals(dpiUser.getAddresses().get(0).getStreet3().toString())){
												if (!hasDpi.contains(id)){
													hasDpi.add(id);
												}
												// APPEND USER ID TO ASSOCIATED STORE LINE
												existingList.add(user.get("user_id").toString());
											}
										} else {
											if (!hasDpi.contains(id)){
												hasDpi.add(id);
											}
											// APPEND USER ID TO ASSOCIATED STORE LINE
											existingList.add(user.get("user_id").toString());
										}
									}
								}
							} catch (NumberFormatException | PortalException
									| SystemException e) {
								e.printStackTrace();
							}
						}
						nextApprovers.get(id).put(levels.get(currentLevel), existingList);
					}
				}
			}
			//ACTIVE/PROPERTY
			if (isNewProject){
				//has all documents and has store lines without dpi
				if (hasDpi.size() < storeLinesIds.size()){
					List<HashMap<String, Object>> property = UsersInformations.getUserDetailsByTeamId(UserTeamIdUtils.PROPERTY_DIRECTOR);
					List<HashMap<String, Object>> active = UsersInformations.getUserDetailsByTeamId(UserTeamIdUtils.DIRECTOR_ACTIVE);
					String storeId = "";
					String owner = "";
					for (String storeLineId : storesPerStoreLines.keySet()){
						if (!hasDpi.contains(storeLineId)){
							currentLevel = "first";
							//means that the store line hasn't got a dpi 
							storeId = DefStore.getStoreIdById(storesPerStoreLines.get(storeLineId).toString());
							owner = DefNewProj.getOwnerByStoreId(storeId);
							if (owner.equals("0")){
								//add ACTIVE
								List<String> existingList = nextApprovers.get(storeLineId).get(levels.get(currentLevel));
								for (HashMap<String, Object> hash : active){
									existingList.add(hash.get("user_id").toString());
								}
								nextApprovers.get(storeLineId).put(levels.get(currentLevel), existingList);
							} else if (owner.equals("1")){
								//add property
								List<String> existingList = nextApprovers.get(storeLineId).get(levels.get(currentLevel));
								for (HashMap<String, Object> hash : property){
									existingList.add(hash.get("user_id").toString());
								}
								nextApprovers.get(storeLineId).put(levels.get(currentLevel), existingList);
							} else {
								//add both
								List<String> existingList = nextApprovers.get(storeLineId).get(levels.get(currentLevel));
								for (HashMap<String, Object> hash : active){
									existingList.add(hash.get("user_id").toString());
								}
								for (HashMap<String, Object> hash : property){
									existingList.add(hash.get("user_id").toString());
								}
								nextApprovers.get(storeLineId).put(levels.get(currentLevel), existingList);
							}
						}
					}
				}
			}
			//STORE DIRECTOR
			if (invoiceWithMissingDoc){
				//if (hasDpi.size() < storeLinesIds.size()){
					List<HashMap<String, Object>> storeDirectors = UsersInformations.getUserDetailsByTeamId(UserTeamIdUtils.STORE_DIRECTOR);
					String storeId = "";
					for (String id : storesPerStoreLines.keySet()){
						if (!hasDpi.contains(id)){
							currentLevel="first";
						} else {
							currentLevel="second";
						}
						storeId = storesPerStoreLines.get(id);
						if (currentLevel.equals("second") && DefStore.isHQ(storeId)){
							continue;
						}
						if (DefStore.isHQ(storeId)){
							continue;
						}
						List<String> existingList = nextApprovers.get(id).get(levels.get(currentLevel));
						int existingUsersSize = existingList.size();
						for (HashMap<String, Object> user : storeDirectors){
							User storeUser = null;
							try {
								storeUser = UserLocalServiceUtil.getUser(Integer.parseInt(user.get("user_id").toString()));
								if (storeUser != null){
									if(DefStore.getStoreIdById(storeId).equals(storeUser.getAddresses().get(0).getStreet2().toString())){
										//APPEND USER ID TO ASSOCIATED STORE LINE
										existingList.add(user.get("user_id").toString());
									}
								}
							} catch (NumberFormatException | PortalException | SystemException e) {
								e.printStackTrace();
							}
						}
						//if the store doesn't have a store director add a 
						if (existingUsersSize == existingList.size()){
							existingList.add("fictive_user_magazin");
						}
						nextApprovers.get(id).put(levels.get(currentLevel), existingList);
					}
				//}
			} else {
					currentLevel="second";
					List<HashMap<String, Object>> storeDirectors = UsersInformations.getUserDetailsByTeamId(UserTeamIdUtils.STORE_DIRECTOR);
					String storeId = "";
					for (String id : storesPerStoreLines.keySet()){
						if (!hasDpi.contains(id)){
							continue;
						}
						storeId = storesPerStoreLines.get(id);
						if (!DefStore.isHQ(storeId)){
							List<String> existingList = nextApprovers.get(id).get(levels.get(currentLevel));
							int existingUsersSize = existingList.size();
							for (HashMap<String, Object> user : storeDirectors){
								User storeUser = null;
								try {
									storeUser = UserLocalServiceUtil.getUser(Integer.parseInt(user.get("user_id").toString()));
									if (storeUser != null){
										if(DefStore.getStoreIdById(storeId).equals(storeUser.getAddresses().get(0).getStreet2().toString())){
											//APPEND USER ID TO ASSOCIATED STORE LINE
											existingList.add(user.get("user_id").toString());
										}
									}
								} catch (NumberFormatException | PortalException
										| SystemException e) {
									e.printStackTrace();
								}
							}
							//if the store doesn't have a store director add a 
							if (existingUsersSize == existingList.size()){
								existingList.add("fictive_user_magazin");
							}
							nextApprovers.get(id).put(levels.get(currentLevel), existingList);
						}
					}
			}
			//HQ - FIRST WE NEED TO CHECK WITH C4
			currentLevel="second";
			List<HashMap<String, Object>> admins = UsersInformations.getUserDetailsByTeamId(UserTeamIdUtils.ADMINISTRATOR_SEDIU);
			String storeId = "";
			for (String id : storesPerStoreLines.keySet()){
				if (!hasDpi.contains(id)){
					currentLevel="first";;
				} else {
					currentLevel="second";
				}
				if (!hasDpi.contains(id) && isNewProject){
					continue;
				}
				storeId = storesPerStoreLines.get(id);
				if (DefStore.isHQ(storeId)){
					int foundHqAdmin = 0;
					List<String> existingList = nextApprovers.get(id).get(levels.get(currentLevel));
					for (HashMap<String, Object> user : admins){
						User storeUser = null;
						try {
							storeUser = UserLocalServiceUtil.getUser(Integer.parseInt(user.get("user_id").toString()));
							if (storeUser != null){
								if(DefStore.getStoreIdById(storeId).equals(storeUser.getAddresses().get(0).getStreet2().toString())){
									if (aislesPerStoreLines.get(id).equals(storeUser.getAddresses().get(0).getStreet3().toString())){
										//APPEND USER ID TO ASSOCIATED STORE LINE
										existingList.add(user.get("user_id").toString());
										foundHqAdmin = 1;
									}
								}
							}
						} catch (NumberFormatException | PortalException | SystemException e) {
							e.printStackTrace();
						}
					}
					if (foundHqAdmin == 0){
						existingList.add("fictive_user_hq");
					}
					nextApprovers.get(id).put(levels.get(currentLevel), existingList);
				}
			}	
			//FINANCIAL DIRECTOR
			currentLevel = "third";
			if (goToFinancialDirector(header)) {
				List<HashMap<String, Object>> financialDirectors = UsersInformations.getUserDetailsByTeamId(UserTeamIdUtils.FINANCIAL_DIRECTOR);
				for (String id : storesPerStoreLines.keySet()){
					List<String> existingList = new ArrayList<String>();
					existingList = nextApprovers.get(id).get(levels.get(currentLevel));
					for (HashMap<String, Object> director : financialDirectors){
						//APPEND USER ID TO ASSOCIATED STORE LINE
						existingList.add(director.get("user_id").toString());
					}
					nextApprovers.get(id).put(levels.get(currentLevel), existingList);
				}
			}
		}
		*/
		return nextApprovers;
	}
	
	/**
	 * 05.03.2016
	 * Returns the approval level based on approval role
	 * @param level
	 * @return
	 */
	public String getLevel(String level){
		HashMap<String, String> levels = initializeApprovalLevels();
		String approvalLevel = "";
		for (String key : levels.keySet()){
			if (levels.get(key).equals(level)){
				approvalLevel = key;
			}
		}
		return approvalLevel;
	}
	
	/**
	 * 05.03.2016
	 * Insert entries into invoice_set_approvals table
	 * @param header = INVOICE HEADER
	 * @param storeLines = INVOICE STORE LINES WITH APPROVER IDS ON EACH LEVEL
	 */
	public void insertEntriesApproversTable(String invNumber, HashMap<String, HashMap<String, List<String>>> storeLines){
		String[][] fields = new String[4][2];
		fields[0][0] = "id_inv_store_line";
		fields[1][0] = "id_user";
		fields[2][0] = "level_approval";
		fields[3][0] = "inv_no";
		fields[3][1] = invNumber;
		for (String id : storeLines.keySet()){
			for (String level : storeLines.get(id).keySet()){
				List<String> users = storeLines.get(id).get(level);
				if (users.size() > 0){
					String userIds = "";
					for (String userId : users){
						userIds+= userId + ",";
					}
					userIds = userIds.substring(0, userIds.length()-1);
					fields[0][1] = id;
					fields[1][1] = userIds;
					fields[2][1] = level;
					GenericDBStatements.insertEntry(fields, "invoice_set_approvals", new ArrayList<String>());
				}
			}
		}
	}
	
	
	/**
	 * 05.03.2016
	 * Insert entries into invoice_set_missing_doc table
	 * @param header = INVOICE HEADER
	 * @param storeLines = INVOICE STORE LINES WITH APPROVER IDS ON EACH LEVEL
	 */
	public void insertEntriesIntoMissingDocsTable(String invId, String[][] header, HashMap<String, HashMap<String, List<String>>> storeLines){
		String[][] fields = new String[4][2];
		String missing_doc = header[getColumnIndex(header, "missing_docs")][1];
		
		for (String id : storeLines.keySet()){
			String userIds = "";
			for (String level : storeLines.get(id).keySet()){
				List<String> users = storeLines.get(id).get(level);
				if (users.size() > 0){
					for (String userId : users){
						userIds+= userId + ",";
					}
				} else {
					userIds+= "fictive_user2,";
				}
			}
			//userIds = userIds.substring(0, userIds.length());
			fields[0][0] = "id_inv_store_line";
			fields[0][1] = id;
			fields[1][0] = "id_user";
			fields[1][1] = userIds;
			fields[2][0] = "missing_doc";
			fields[2][1] = missing_doc;
			fields[3][0] = "invoice_id";
			fields[3][1] = invId;
			GenericDBStatements.insertEntry(fields, "invoice_set_missing_doc", new ArrayList<String>());
		}
		
	}
	
	/**
	 * 
	 * @param request
	 * @param response
	 */
	public void addInvoice(ActionRequest request, ActionResponse response) {
		boolean isNewLine = false;
		// remove default error message from Liferay
		PortletConfig portletConfig = (PortletConfig) request.getAttribute(JavaConstants.JAVAX_PORTLET_CONFIG);
		LiferayPortletConfig liferayPortletConfig = (LiferayPortletConfig) portletConfig;
		SessionMessages.add(request, liferayPortletConfig.getPortletId() + SessionMessages.KEY_SUFFIX_HIDE_DEFAULT_ERROR_MESSAGE);

		log.info("[PROFLUO] Invoices portlet - addInvoice()");
		try {
			User user =  UserServiceUtil.getUserById(Long.parseLong(request.getRemoteUser()));
			String action = request.getParameter("insertORupdate").toString();
			String cid = null;
			// from what page was the form submited
			String currentStage = request.getParameter("currentStage").toString();
			//user for notifications 
			String supplier = "";
			String supplierName = request.getParameter("vendor").toString();
			String supplierCode = request.getParameter("supplier_code").toString();
			supplier = supplierCode + " - " + supplierName;
			String dpiIds = "";
			
			// remains 0 if the insert or update of a store line was not
			// successful
			int storeLineSuccess = 0;

			// the method requires an exclude list, which is null for
			// invoice_header
			ArrayList<String> nullArrayList = new ArrayList<String>();
			int noInserted = 0;
			this.setLinesExcludes();
			this.setStoreLineExcludes();
			
			HashMap<Integer, Integer> storeLineIds = new HashMap<Integer, Integer>();
			
			String[][] arrFields = this.getSqlHeaderInvoiceParams(request, response, action, currentStage);
			//INCORRECT INVOICE 
			if (!arrFields[24][1].equals("")){
				//update invoice header stage
				String[][] incorrectInvoice = new String[7][2];
				//column values that are updated
				incorrectInvoice[0][0] = "stage";
				incorrectInvoice[0][1] = InvoiceStages.INCORRECT_INVOICE;
				incorrectInvoice[1][0] = "error_details";
				incorrectInvoice[1][1] = arrFields[24][1];
				incorrectInvoice[2][0] = "kontan_acc";
				incorrectInvoice[2][1] = arrFields[35][1];
				incorrectInvoice[2][0] = "kontan_acc";
				incorrectInvoice[2][1] = arrFields[35][1];
				incorrectInvoice[3][0] = "accounting_month";
				incorrectInvoice[3][1] = arrFields[37][1];
				incorrectInvoice[4][0] = "accounting_year";
				incorrectInvoice[4][1] = arrFields[38][1];
				Calendar calendar = Calendar.getInstance();
			    java.sql.Timestamp currentTS = new java.sql.Timestamp(calendar.getTime().getTime());
				incorrectInvoice[5][0] = "reg_date";
				incorrectInvoice[5][1] = currentTS.toString();
				incorrectInvoice[6][0] = "id_user";
				incorrectInvoice[6][1] = String.valueOf(user.getUserId());
				//columns from the where clause
				ArrayList<String> columns = new ArrayList<String>();
				columns.add("id");
				//values for columns in the where clause
				ArrayList<String> values = new ArrayList<String>();
				values.add(action);
				//update invoice header
				int updated = GenericDBStatements.updateEntry(incorrectInvoice, "invoice_header", new ArrayList<String>(), columns, values);
				if (updated > 0){
					SessionMessages.add(request, "invoiceUpdated");
				} else {
					SessionErrors.add(request, "update_nok");
				}
				//INCORRECT INVOICE => SEND NOTIFICATION TO SUPPLIER
				addIncorrectInvoiceNotification(Integer.parseInt(action), user, arrFields);
				return;
			}
			
			String[][][] lineDataArticles = null;
			String[][][] lineData = null;
			String dataTableData = request.getParameter("datatable_data");
			String dataTableArticles = request.getParameter("datatable_lines");
			//liniile facturii
			lineData = ControllerUtils.convertToArrayMatrixFromJSON(dataTableData);
			//liniile alocate pe factura
			lineDataArticles = ControllerUtils.convertToArrayMatrixFromJSON(dataTableArticles);
			//set values for all columns in case of prorata so that the insertion of the line in the databes will be successful
			//setAllFieldsForPRORATALines(lineData);
			
			boolean update = false;
			// save invoice header
			
			if (action.equals("")) {
				// insert
				noInserted = GenericDBStatements.insertEntry(arrFields,"invoice_header", nullArrayList);
				
				String idSocietate = arrFields[6][1];
				String entityCode = "";
				if(idSocietate.equals("1")) {
					entityCode = "SP";
				} else if(idSocietate.equals("2")) {
					entityCode = "CF";
				}  else if(idSocietate.equals("3")) {
					entityCode = "AR";
				}  else if(idSocietate.equals("7")) {
					entityCode = "CU";
				}
				
				if(!entityCode.isEmpty()) {
					//generare CID
					cid = GenericDBStatements.executeProcedure("spGetCID", noInserted, "CPX", entityCode);
					InvoiceHeader.setInvoiceCid(noInserted, cid);
				}
				
				
				String companyId = arrFields[6][1];
				//daca furnizorul e blocat se solicita deblocarea lui
				boolean isBLocked = DefSupplier.isBlocked(supplierCode, companyId);
				if (isBLocked){
					DefSupplier.setBlockedStatusBySupplierCode(supplierCode, companyId, "1");
				}
				
				if (noInserted <= 0) {
					SessionErrors.add(request, "insert_nok");
					ControllerUtils.saveRequestParamsOnResponse(request, response, log);
					response.setRenderParameter("mvcPath", "/html/facturi/adauga_factura.jsp");
				}
				action = String.valueOf(noInserted);
				// if the invoice doesn't exist then it doesn't have a current stage
				currentStage = "0";
			} else {
				System.out.println("action = " + action);
				if(!currentStage.equals("1")) {
					//save old invoice header into history
					InvoiceHistory.saveInvoiceHeader(action,user.getUserId());
					//save old invoice lines into history
					InvoiceHistory.saveInvoiceLines(action);
					//save old invoice store lines into history
					InvoiceHistory.saveInvoiceStoreLines(action);
					//save old invoice store lines inventory into history
					InvoiceHistory.saveInvoiceStoreLinesInv(action);
				}
				// update attrs for where statement
				ArrayList<String> whereStatement = new ArrayList<String>();
				// attrs values for where statement
				ArrayList<String> whereStatementValues = new ArrayList<String>();

				update = true;
				whereStatement.add("id");
				whereStatementValues.add(action);
				noInserted = GenericDBStatements.updateEntry(arrFields, "invoice_header", nullArrayList, whereStatement, whereStatementValues);
				if (noInserted <= 0) {
					SessionErrors.add(request, "update_nok");
					ControllerUtils.saveRequestParamsOnResponse(request, response, log);
					response.setRenderParameter("mvcPath", "/html/facturi/adauga_factura.jsp");
				}
			} 
			// to match store lines with invoice lines
			// String[] lineNumber = new String[lineData.length];
			// calculate values for all columns in invoice_line
			if (noInserted > 0) {
				lineData = this.getSQLInvoiceLines(request, response, lineData, arrFields, Integer.parseInt(action));
				if(hasServices && currentStage.equals("1")) {
					InvoiceHistory.saveOldLines(action);
				}
				// insert lines in invoice_line
				for (int i = 0; i < lineData.length; i++) {
					// id of inserted/updated row in table_line
					int resultedID = 0;
					if (!update) {
						// insert
						int indexOfDescription = getColumnIndex(lineData[i], "description");
						if(!GeneralCodes.getServices().contains(lineData[i][indexOfDescription][1].toLowerCase())){
							resultedID = GenericDBStatements.insertEntry(lineData[i], "invoice_line", this.lineExcludes);
						} else {
							resultedID = 1;
						}
						if (resultedID <= 0) {
							SessionErrors.add(request, "insert_nok");
							ControllerUtils.saveRequestParamsOnResponse(request, response, log);
							response.setRenderParameter("mvcPath", "/html/facturi/adauga_factura.jsp");
						} 
					} else {
						// update
						// pe pozitia lineData[i][7][1] se gaseste numarul pe  care il are linia in cadrul facturii
						// attributes where statement
						ArrayList<String> whereStatement = new ArrayList<String>();
						// values for attributes for where clause
						ArrayList<String> whereStatementValues = new ArrayList<String>();
						whereStatement.add("inv_number");
						whereStatement.add("line");
						whereStatementValues.add(action);

						int indexOfLine = getColumnIndex(lineData[i], "line");
						whereStatementValues.add(lineData[i][indexOfLine][1]);
						
						// pentru prorata
						int indexOfLineID = getColumnIndex(lineData[i], "invoice_line_id");
						if (lineData[i][indexOfLineID][1].equals("")){
							int indexOfDescription = getColumnIndex(lineData[i], "description");
							//System.out.println("PRORATA");
							//daca linia vine fara id si totusi factura vine prin EDI inseamna ca linia curenta a fost adaugata in interfata
							if(!GeneralCodes.getServices().contains(lineData[i][indexOfDescription][1].toLowerCase())){
								resultedID = GenericDBStatements.insertEntry(lineData[i], "invoice_line", this.lineExcludes);
							} else {
								resultedID = 1;
							}
							isNewLine = true;
						} else {
							int indexOfDescription = getColumnIndex(lineData[i], "description");
							if(!GeneralCodes.getServices().contains(lineData[i][indexOfDescription][1].toLowerCase())){
								resultedID = GenericDBStatements.updateEntry( lineData[i], "invoice_line", this.lineExcludes, whereStatement, whereStatementValues);
							} else {
								resultedID = 1;
							}
							isNewLine = false;
						}
						if (resultedID <= 0) {
							SessionErrors.add(request, "update_nok");
							ControllerUtils.saveRequestParamsOnResponse(request, response, log);
							response.setRenderParameter("mvcPath", "/html/facturi/adauga_factura.jsp");
						}
					}

					if (resultedID > 0) {
						 // prima dimensiune = nr de linii, a doua dimensiune = campurile din tabel, a treia dimensiune = valorile asociate campurilor
						for (int j = 0; j < lineDataArticles.length; j++) {
							// if the invoice store line belongs to the invoice line
							if (checkIfStoreLineBelongsToLine(lineData[i], lineDataArticles[j])) {
								String val_no_vat_curr = "";
								String invoice_store_line_id = "";
								// we need to find out what the currency value is in order to make calculation with echange rate
								for (int k = 0; k < lineDataArticles[j].length; k++) {
									// retrieve the value without vat from the  invoice store line
									if (lineDataArticles[j][k][0].equals("price_no_vat_curr")) {
										val_no_vat_curr = lineDataArticles[j][k][1];
									}
									// retrieve the invoice store line id
									if (lineDataArticles[j][k][0].equals("id_store_line")) {
										System.out.println("lala");
										invoice_store_line_id = lineDataArticles[j][k][1];
									}
									if (lineDataArticles[j][k][0].equals("id_dpi")) {
										dpiIds += lineDataArticles[j][k][1] + ",";
									}
								}
								// make all other calculations on store lines  array
								for (int k = 0; k < lineDataArticles[j].length; k++) {
									if (lineDataArticles[j][k][0].equals("inv_number")) {
										// set invoice id on invoice store line
										lineDataArticles[j][k][1] = action;
									} else if (lineDataArticles[j][k][0].equals("line_id")) {
										// set invoice line on invoice store line
										if (!update || isNewLine) {
											// if the line was inserted then set the resulted id
											lineDataArticles[j][k][1] = ((Integer) resultedID).toString();
										} else {
											// if the line was updated then set the existing id
											lineDataArticles[j][k][1] = lineData[i][this.getColumnIndex(lineData[i], "invoice_line_id")][1];
										}
									} else if (lineDataArticles[j][k][0].equals("currency")) {
										// set currency on invoice store line
										lineDataArticles[j][k][1] = arrFields[17][1];
									} else if (lineDataArticles[j][k][0].equals("price_no_vat_ron")) {
										//val no vat curr will always contain a RON value
										lineDataArticles[j][k][1] = ControllerUtils.fromCurrToRon(Float.parseFloat(val_no_vat_curr), Float.parseFloat(arrFields[18][1]));
									} else if (lineDataArticles[j][k][0].equals("price_no_vat_curr")) {
											// calculate price without vat on invoice store line
											lineDataArticles[j][k][1] = val_no_vat_curr; 
									}

								}
								if (currentStage.equals("1") || currentStage.equals("0")) {
									storeLineSuccess = GenericDBStatements.insertEntry(lineDataArticles[j], "invoice_store_line", this.lineStoresExcludes);
									
									storeLineIds.put(j, storeLineSuccess);
									
									if (storeLineSuccess <= 0) {
										SessionErrors.add(request, "insert_nok");
										ControllerUtils.saveRequestParamsOnResponse(request, response, log);
										response.setRenderParameter("mvcPath", "/html/facturi/adauga_factura.jsp");
									}
									
									int columnPosition = this.getColumnIndex(lineDataArticles[j], "inventory");
									int productPosition = this.getColumnIndex(lineDataArticles[j], "product_code");
									int inventory = 0;
									if (!update) {
										if(!lineDataArticles[j][productPosition][1].equals("3562")) {
											inventory = this.insertInventoryNo(noInserted, storeLineSuccess, lineDataArticles[j][columnPosition]);
										} else {
											inventory = 1;
										}
									} else {
										if(!lineDataArticles[j][productPosition][1].equals("3562")) {
											inventory = this.insertInventoryNo(Integer.parseInt(action),storeLineSuccess, lineDataArticles[j][columnPosition]);
										} else {
											inventory = 1;
										}
									}
									if (inventory == 0) {
										SessionErrors.add(request, "update_nok");
										ControllerUtils.saveRequestParamsOnResponse(request, response, log);
										response.setRenderParameter("mvcPath", "/html/facturi/adauga_factura.jsp");
									}
								} else {
									storeLineIds.put(j, Integer.parseInt(invoice_store_line_id));
									// attributes where statement
									ArrayList<String> whereStatement = new ArrayList<String>();
									// values for attributes for where clause
									ArrayList<String> whereStatementValues = new ArrayList<String>();
									whereStatement.add("id");
									whereStatementValues.add(invoice_store_line_id);
									// do not modify the line_id on update
									this.lineStoresExcludes.add("line_id");
									// System.out.println(this.lineStoresExcludes);
									storeLineSuccess = GenericDBStatements.updateEntry(lineDataArticles[j], "invoice_store_line", 
											this.lineStoresExcludes, whereStatement, whereStatementValues);
											this.lineStoresExcludes.remove(this.lineStoresExcludes.size() - 1);
									if (storeLineSuccess <= 0) {
										SessionErrors.add(request, "update_nok");
										ControllerUtils.saveRequestParamsOnResponse( request, response, log);
										response.setRenderParameter("mvcPath", "/html/facturi/adauga_factura.jsp");
									}
									int columnPosition = this.getColumnIndex(lineDataArticles[j], "inventory");
									int inventory = this.updateInventoryNo(Integer.parseInt(action),Integer.parseInt(invoice_store_line_id), lineDataArticles[j][columnPosition]);
									if (inventory == 0) {
										SessionErrors.add(request, "update_nok");
										ControllerUtils.saveRequestParamsOnResponse(request, response, log);
										response.setRenderParameter("mvcPath", "/html/facturi/adauga_factura.jsp");
									}
								}
							}
						}
					}
				}
			} 

			//dezactivate DPIS after invoice is saved
			if(dpiIds.endsWith(",")) {
				dpiIds = dpiIds.substring(0, dpiIds.length()-1);
			}
			if(!dpiIds.isEmpty() && dpiIds.length() > 1) {
				DPIHeader.dezactivateDpi(dpiIds);
			}
			
			System.out.println("Currentstage = " + currentStage);
			
			//INVOICE IS SAVED
			// major changes reset invoice flow
			//currentStage == 0 => Factura manuala
			//currentStage == 1 => Factura din EDI
			//currentStage == 2 => Factura lipsa documente
			if(InvoiceHistory.hasMajorChanges(action) || currentStage.equals("1") || currentStage.equals("2") || currentStage.equals("0")) {
				//COMPUTE NEXT APPROVERS
				HashMap<String, HashMap<String, List<String>>> nextApprovers = computeNextApprovers(arrFields, lineDataArticles, storeLineIds);
				//for (String i : nextApprovers.keySet()){
					//for (String level : nextApprovers.get(i).keySet()){
						//System.out.println(i + " ----> " + level + " ----> " + nextApprovers.get(i).get(level));
					//}
				//}
				//POPULATE HISTORY TABLE
				if (invoiceWithMissingDoc){
					if(currentStage.equals("1") || currentStage.equals("3") || currentStage.equals("0")) {
						//INSERT INTO MISSING DOCS TABLE
						insertEntriesIntoMissingDocsTable(action, arrFields, nextApprovers);
						//Add notifications for missing docs
						addNotificationsForMissingDocs(action, arrFields[0][1], user, supplier, DefCompanies.getCompanyNameById(arrFields[6][1]), arrFields[1][1], arrFields[23][1]);
					}
				} else {
					//delete old approvals
					InvoiceHistory.deleteOldApprovals(action);
					//insert new approvals
					insertEntriesApproversTable(action, nextApprovers);
					//SEND FIRST STEP NOTIFICATIONS
					addNotificationForFirstUnapprovedLevel(6, arrFields[0][1], user, supplier, action, arrFields[1][1], DefCompanies.getCompanyNameById(arrFields[6][1]));
				}
				//update line colomn in order to start from 1
				List<HashMap<String, Object>> invoiceLines = InvoiceHeader.getLineIds(action);
				for(int j = 0 ; j < invoiceLines.size(); j++ ) {
					InvoiceHeader.updateLine(j+1, invoiceLines.get(j).get("id"));
				}
				SessionMessages.add(request, "update_ok");
			}
			request.setAttribute("invoice_id", action);
			request.setAttribute("invoice_cid", cid);
		} catch (Exception e) {
			e.printStackTrace();
			SessionErrors.add(request, "error");
			response.setRenderParameter("mvcPath", "/html/facturi/adauga_factura.jsp");
		}
	}
	
	
	public void addNotificationsForMissingDocs(String invoiceId, String invNumber, User user, String supplier, String company, String invDate, String missingDocs){
		//list of users to be notified
		List<HashMap<String,Object>> usersToBeNotified = new ArrayList<HashMap<String,Object>>();
		//continutul notificarii
		HashMap<String, Object> bodyDetails = new HashMap<String,Object>();
		List<HashMap<String,Object>> firstApprovals = InvoiceHeader.getAllApprovalsForMissingDocs(invoiceId);
		for(HashMap<String,Object> hash : firstApprovals) {
			String approvalsIds = hash.get("id_user").toString();
			String [] individualIds = approvalsIds.split(",");
			try {
				for(int i = 0; i < individualIds.length; i++ ) {
					User approval = null;
					try{
						approval = UserLocalServiceUtil.getUser(Integer.parseInt(individualIds[i]));
					} catch (NumberFormatException e){
						continue;
					}
					if(!Notifications.notificationAllreadyInserted(12,invoiceId, approval.getEmailAddress())) {
						HashMap<String,Object> userInfo = new HashMap<String,Object>();
						userInfo.put("email", approval.getEmailAddress());
						userInfo.put("first_name", approval.getFirstName());
						userInfo.put("last_name", approval.getLastName());
						if(!usersToBeNotified.contains(userInfo)) {
							usersToBeNotified.add(userInfo);
						}
					}
				}
			} catch (NumberFormatException | PortalException | SystemException e) {
				e.printStackTrace();
			}
		}
		String link = Links.allInvoicesWithMissingDocs;
		String body = "<tr><td style='border:1px solid black'>" + invNumber + "</td>"
				+ "<td style='border:1px solid black'>" + invDate + "</td><td style='border:1px solid black'>"
				+ supplier + "</td><td style='border:1px solid black'>"
				+ company + "</td>" + 
				"<td style='border:1px solid black'>" + missingDocs + "</td>"
				+ "<td style='border:1px solid black'><a href ='" + link + "'>Link</a></td></tr>";
		bodyDetails = setInsertParameters(NotificationTemplates.LIPSA_DOCUMENTE, "", "", body, "0", "B", "1", user, link);
		//insert the notifications
		Notifications.insertNotification(invoiceId, usersToBeNotified, bodyDetails, NotificationsSubject.FACTURA_CU_LIPSA_DOCUMENTE);
	}
	
	public void addNotificationForFirstUnapprovedLevel(int templateId, String invNumber, User user, String supplier, String invId, String invDate, String company) {
		//list of users to be notified
		List<HashMap<String,Object>> usersToBeNotified = new ArrayList<HashMap<String,Object>>();
		//continutul notificarii
		HashMap<String, Object> bodyDetails = new HashMap<String,Object>();
		List<HashMap<String,Object>> firstApprovals = InvoiceHeader.getFirstLevelApprovals(invId);
		for(HashMap<String,Object> hash : firstApprovals) {
			String approvalsIds = hash.get("id_user").toString();
			String [] individualIds = approvalsIds.split(",");
			try {
				for(int i = 0; i < individualIds.length; i++ ) {
					User approval = null;
					try{
						approval = UserLocalServiceUtil.getUser(Integer.parseInt(individualIds[i]));
					} catch (NumberFormatException e){
						continue;
					}
					if(!Notifications.notificationAllreadyInserted(templateId, invId, approval.getEmailAddress())) {
						HashMap<String,Object> userInfo = new HashMap<String,Object>();
						userInfo.put("email", approval.getEmailAddress());
						userInfo.put("first_name", approval.getFirstName());
						userInfo.put("last_name", approval.getLastName());
						if(!usersToBeNotified.contains(userInfo)) {
							usersToBeNotified.add(userInfo);
						}
					}
				}
			} catch (NumberFormatException | PortalException | SystemException e) {
				e.printStackTrace();
			}
		}
		String link = Links.individualInvoiceForApprovals + "&id=" + invId;
		String body = "<tr><td style='border:1px solid black'>" + invNumber + "</td><td style='border:1px solid black'>"
				+ invDate + "</td><td style='border:1px solid black'>" + supplier + "</td><td style='border:1px solid black'>"
				+ company + "</td>"
				+ "<td style='border:1px solid black'><a href='" + link + "'>Link</a></td></tr>";
		bodyDetails = setInsertParameters(NotificationTemplates.INREGISTRARE_FACTURA, "", "", body, "0", "B", "1", user, link);
		//insert the notifications
		Notifications.insertNotification(invId, usersToBeNotified, bodyDetails, NotificationsSubject.INREGISTRARE_FACTURA);
	}

	/**
	 * 
	 */
	@Override
	public void render(RenderRequest renderRequest, RenderResponse renderResponse) throws PortletException, IOException {
		HttpServletRequest request = PortalUtil.getHttpServletRequest(renderRequest);
		String p_p_id = PortalUtil.getOriginalServletRequest(request).getParameter("p_p_id");
		String path = PortalUtil.getOriginalServletRequest(request).getParameter("_" + p_p_id + "_mvcPath");
		
		renderRequest.setAttribute("pagina", "facturi-contabilitate");
		
		try {
			ThemeDisplay themeDisplay = ((ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY));
			String portletId = themeDisplay.getPortletDisplay().getId();
			PortletPreferences portletSetup = PortletPreferencesFactoryUtil.getLayoutPortletSetup(themeDisplay.getLayout(), portletId);
			String portletCustomTitle = themeDisplay.getPortletDisplay().getTitle();
			portletCustomTitle = portletSetup.getValue("portletSetupTitle_" + themeDisplay.getLanguageId(), portletCustomTitle);
			User user = UserServiceUtil.getUserById(Long.parseLong(renderRequest.getRemoteUser()));
			ThemeDisplay td = (ThemeDisplay) renderRequest.getAttribute(WebKeys.THEME_DISPLAY);
			log.info("[PROFLUO] Invoices portlet - render()");
			log.info("[PROFLUO] User full name: " + td.getUser().getFullName());
			log.info("[PROFLUO] Portlet Title: " + portletCustomTitle);
			
			currentURL = Links.unifiedLoginPrefix +  PortalUtil.getCurrentCompleteURL(request);
			
			int id_user = (int) user.getUserId();
			log.info("[PROFLUO] User id " + id_user);
			log.info("[PROFLUO] Group id " + user.getGroupId());
			log.info("[PROFLUO] Group id " + user.getRoles());
			// set on request the sql statement type, stage and page
			
			DefNewProjVo.resetAndgetInstance();

			List<Integer> types = InvoiceStages.getSqlStatementType(user, td.getURLCurrent());
			String stage 	= InvoiceStages.getStage(user, td.getURLCurrent(), InvoiceStages.APPROVED);
			String page 	= InvoiceStages.getPageLayout(td.getURLCurrent());
			
			String allTypes = "";
			for(Integer type : types){
				allTypes += type + ",";
			}
			
			if(allTypes.endsWith(",")){
				allTypes = allTypes.substring(0, allTypes.length()-1);
			}
			renderRequest.setAttribute("page-layout", page);
			renderRequest.setAttribute("page-status", stage);
			renderRequest.setAttribute("type", allTypes);
			renderRequest.setAttribute("pagina", "invoice");
			
			log.info("[PROFLUO] SQL TYPE : " + allTypes);
			log.info("[PROFLUO] STAGE : " + stage);
			log.info("[PROFLUO] PAGE LAYOUT : " + page);

			if (path == null || path.equals("view.jsp")) {
				log.info("[PROFLUO] LISTING render()");

			} else if (path.endsWith("adauga_factura.jsp")) {
				log.info("[PROFLUO] ADD INVOICE render()");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		super.render(renderRequest, renderResponse);
	}
}