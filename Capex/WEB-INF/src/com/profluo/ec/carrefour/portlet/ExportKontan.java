package com.profluo.ec.carrefour.portlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletException;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.util.bridges.mvc.MVCPortlet;
import com.profluo.ecm.export.ExportInvoiceToKontan;
import com.profluo.ecm.model.db.DatabaseConnectionManager;


/**
 * Portlet implementation class ExportKontan
 */
public class ExportKontan extends MVCPortlet {
	
	@SuppressWarnings("unchecked")
	public void serveResource(ResourceRequest resourceRequest, ResourceResponse resourceResponse) throws IOException,PortletException {
		
		String action = ParamUtil.getString(resourceRequest, "action");
		
		if (action.equals("filter") || action.equals("next")|| action.equals("prev") || action.equals("last")|| action.equals("first")) {
			PrintWriter writer = resourceResponse.getWriter();
			int start = Integer.parseInt(ParamUtil.getString(resourceRequest, "start").toString());
			int count = Integer.parseInt(ParamUtil.getString(resourceRequest, "count").toString());
			int total = Integer.parseInt(ParamUtil.getString(resourceRequest, "total").toString());
			String invoiceNo = ParamUtil.getString(resourceRequest,"InvoiceNo");
			String furnizor = ParamUtil.getString(resourceRequest, "supplier");

			List<HashMap<String, Object>> allLines = null;

			allLines = ExportInvoiceToKontan.getAllInfoWithLimit(start, count, invoiceNo, furnizor);
			if (total == 0) {
				total = ExportInvoiceToKontan.getInvoiceTotal();
			}

			JSONArray jsonLines = new JSONArray();
			if (allLines != null) {
				jsonLines = DatabaseConnectionManager.convertListToJson(allLines);
			}
			JSONObject jsonResponse = new JSONObject();

			jsonResponse.put("action", action);
			jsonResponse.put("start", start);
			jsonResponse.put("count", count);
			jsonResponse.put("total", total);
			jsonResponse.put("values", jsonLines);
			writer.print(jsonResponse.toJSONString());
		
		}
	}
	
	public void generateKim(ActionRequest actionRequest,	ActionResponse actionResponse) {

		
			System.out.println("Am apelat exportul");
			try {
				com.profluo.ecm.exports.ExportInvoiceToKontan.generateKim();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			
			actionResponse.setRenderParameter("mvcPath", "/html/exportkontan/view.jsp");
			
	
	}
	
	
}