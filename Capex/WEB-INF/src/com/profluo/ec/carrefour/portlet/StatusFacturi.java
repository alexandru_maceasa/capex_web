package com.profluo.ec.carrefour.portlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletException;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.model.User;
import com.liferay.portal.service.UserLocalServiceUtil;
import com.liferay.util.bridges.mvc.MVCPortlet;
import com.profluo.ecm.controller.ControllerUtils;
import com.profluo.ecm.model.db.DatabaseConnectionManager;
import com.profluo.ecm.model.db.DefStore;
import com.profluo.ecm.model.db.GenericDBStatements;
import com.profluo.ecm.model.db.InvoiceHeader;
import com.profluo.ecm.model.db.UsersInformations;
import com.profluo.ecm.model.vo.StatusFacturiBD;
import com.profluo.ecm.utils.ExcelUtil;

/**
 * Portlet implementation class StatusFacturi
 */
public class StatusFacturi extends MVCPortlet {
	
	private static final Log log = LogFactoryUtil.getLog("PROFLUO-" + StatusFacturi.class.getName());
	
	public void importAllInvoices(ActionRequest request, ActionResponse response) {
		List<HashMap<String,Object>> allInvoicesHeaders = InvoiceHeader.getAllInvoicesHeaders();
		boolean isBlocked = false;
		for(int i = 0 ; i < allInvoicesHeaders.size(); i++) {
			System.out.println(" COUNT --------------------------->>>>>>>>>>>> " + i);
			System.out.println("######## " + allInvoicesHeaders.get(i).get("inv_number") + " ############");
			if(Integer.parseInt(allInvoicesHeaders.get(i).get("stage").toString()) < 0){
				isBlocked = true;
			} else {
				isBlocked = false;
			}
			String invoiceHeader [][] = ControllerUtils.convertHashMapToStringMatrix(allInvoicesHeaders.get(i));
			String invId = allInvoicesHeaders.get(i).get("inv_id").toString();
			List<HashMap<String,Object>> allInvoiceStoreLines = InvoiceHeader.getAllStoreLinesByInvoiceId(invId);
			String invoiceStoreLines [][][] = ControllerUtils.convertListOfHashMapToStringMatrix(allInvoiceStoreLines);
			Invoices inv = new Invoices();
			if(allInvoicesHeaders.get(i).get("id_new_prj") != null){
				inv.isNewProject = true;
			}
			if(!allInvoicesHeaders.get(i).get("missing_docs").toString().equals("")){
				inv.invoiceWithMissingDoc = true;
			}
			HashMap<Integer, Integer> storeLineIds = new HashMap<Integer, Integer>();
			for(int j = 0 ; j < allInvoiceStoreLines.size(); j++){
				storeLineIds.put(j, Integer.parseInt(allInvoiceStoreLines.get(j).get("id").toString()));
			}
			HashMap<String, HashMap<String, List<String>>> nextApprovers = inv.computeNextApprovers(invoiceHeader, invoiceStoreLines, storeLineIds);	
			for (String idStoreLine : nextApprovers.keySet()){
				for (String level : nextApprovers.get(idStoreLine).keySet()){
					boolean storeLineIsApproved = false;
					Timestamp approveDate = null;
					String userIds = "";
					String userThatApprovedStoreline = "";
					System.out.println(idStoreLine + " ----> " + level + " ----> " + nextApprovers.get(idStoreLine).get(level));
					//lista aprobatorilor intorsi de functie
					List<String> currentApprovers = nextApprovers.get(idStoreLine).get(level);
					for(String userId : currentApprovers) {
						userIds += userId + ",";
						if(InvoiceHeader.wasApprovedByCurrentUser(userId, idStoreLine) != null){
							approveDate =InvoiceHeader.wasApprovedByCurrentUser(userId, idStoreLine);
							userThatApprovedStoreline = userId;
						}
						if(approveDate != null){
							storeLineIsApproved = true;
						}
					}
					System.out.println(storeLineIsApproved + " " + approveDate + " -> " + currentApprovers);
					//arrFields for insert into invoice_set_approvals
					if(currentApprovers.size() > 0){
						String [][] arrFields;
						if(storeLineIsApproved) {
							arrFields = new String[8][2];
							arrFields[0][0] = "id_inv_store_line";
							arrFields[1][0] = "id_user";
							arrFields[2][0] = "level_approval";
							arrFields[3][0] = "validation";
							arrFields[4][0] = "validation_date";
							arrFields[5][0] = "blocked";
							arrFields[6][0] = "id_approver";
							arrFields[7][0] = "inv_no";
							//values to be inserted
							arrFields[0][1] = idStoreLine;
							arrFields[1][1] = userIds;
							arrFields[2][1] = level;
							arrFields[3][1] = "1";
							arrFields[4][1] = approveDate.toString();
							arrFields[7][1] = allInvoicesHeaders.get(i).get("id").toString();
							if(isBlocked){
								arrFields[5][1] = "1";
							} else {
								arrFields[5][1] = "0";
							}
							arrFields[6][1] = userThatApprovedStoreline;
						} else {
							arrFields = new String[6][2];
							arrFields[0][0] = "id_inv_store_line";
							arrFields[1][0] = "id_user";
							arrFields[2][0] = "level_approval";
							arrFields[3][0] = "validation";
							arrFields[4][0] = "blocked";
							arrFields[5][0] = "inv_no";
							//values to be inserted
							arrFields[0][1] = idStoreLine;
							arrFields[1][1] = userIds;
							arrFields[2][1] = level;
							arrFields[3][1] = "0";
							arrFields[5][1] = allInvoicesHeaders.get(i).get("id").toString();
							if(isBlocked){
								arrFields[4][1] = "1";
							} else {
								arrFields[4][1] = "0";
							}
						}
						//INSERT
						GenericDBStatements.insertEntry(arrFields, "invoice_set_approvals", new ArrayList<String>());
					}
				}
			}
			System.out.println("#########-> END <-###########");
		}
	}
	
	public void importMissingDocs(ActionRequest request, ActionResponse response) {
		//Getting the list with all store lines
		List<HashMap<String,Object>> allInvoices = InvoiceHeader.getAllInvoicesWithMissingDocs();
		//iterating throw each store line
		for(int i = 0; i < allInvoices.size(); i++){
			String invoiceHeader [][] = ControllerUtils.convertHashMapToStringMatrix(allInvoices.get(i));
			String invId = allInvoices.get(i).get("id").toString();
			List<HashMap<String,Object>> allInvoiceStoreLines = InvoiceHeader.getAllStoreLinesByInvoiceId(invId);
			String invoiceStoreLines [][][] = ControllerUtils.convertListOfHashMapToStringMatrix(allInvoiceStoreLines);
			Invoices inv = new Invoices();
			if(allInvoices.get(i).get("id_new_prj") != null){
				inv.isNewProject = true;
			}
			if(!allInvoices.get(i).get("missing_docs").toString().equals("")){
				inv.invoiceWithMissingDoc = true;
			}
			HashMap<Integer, Integer> storeLineIds = new HashMap<Integer, Integer>();
			for(int j = 0 ; j < allInvoiceStoreLines.size(); j++){
				storeLineIds.put(j, Integer.parseInt(allInvoiceStoreLines.get(j).get("id").toString()));
			}
			HashMap<String, HashMap<String, List<String>>> nextApprovers = inv.computeNextApprovers(invoiceHeader, invoiceStoreLines, storeLineIds);
			inv.insertEntriesIntoMissingDocsTable(invId, invoiceHeader, nextApprovers);
		}
	}
	
	public static HashMap<String, Object> getAllApprovals(List<HashMap<String,Object>> allInvoices) {
		
		log.info("Status Facturi -> getAllApprovals()");
		
		HashMap<String, Object> allApprovals = new HashMap<String,Object>();
		
		for(int i = 0; i < allInvoices.size(); i++) {
			//daca factura e in stage 10 inseamna ca urmeaza sa fie exportata
			if(allInvoices.get(i).get("stage").toString().equals("10")) {
				String approvedInvoice = "Factura a fost aprobata si ";
				if(allInvoices.get(i).get("kontan_status").toString().equals("0") && allInvoices.get(i).get("optimal_status").toString().equals("0")) {
					approvedInvoice += "urmeaza sa fie exportata in Kontan si Optimal";
				} else if((allInvoices.get(i).get("kontan_status").toString().equals("1") )&& allInvoices.get(i).get("optimal_status").toString().equals("0")) {
					approvedInvoice += " exportata doar in Kontan";
				} else if(allInvoices.get(i).get("kontan_status").toString().equals("0") && allInvoices.get(i).get("optimal_status").toString().equals("1")) {
					approvedInvoice += " exportata doar in Optimal";
				} else if((allInvoices.get(i).get("kontan_status").toString().equals("1") ) && allInvoices.get(i).get("optimal_status").toString().equals("1")) {
					approvedInvoice += " exportata in Kontan si Optimal";
				} else if (allInvoices.get(i).get("kontan_status").toString().equals("2")  && allInvoices.get(i).get("optimal_status").toString().equals("1")) {
					approvedInvoice += " deblocata din Kontan si exportata in Optimal";
				} else if (allInvoices.get(i).get("kontan_status").toString().equals("2")  && allInvoices.get(i).get("optimal_status").toString().equals("0")) {
					approvedInvoice += " deblocata din Kontan";
				}
			allApprovals.put(String.valueOf(i), approvedInvoice);
			} else if (allInvoices.get(i).get("stage").toString().equals("-1")) {
				allApprovals.put(String.valueOf(i), "Factura eronata");
			} else if (allInvoices.get(i).get("stage").toString().equals("2")) {
				allApprovals.put(String.valueOf(i), "Factura se afla in listingul de facturi cu lipsa documente: " + allInvoices.get(i).get("missing_docs").toString());
			} else if (Integer.parseInt(allInvoices.get(i).get("stage").toString()) < 0) {
				allApprovals.put(String.valueOf(i), "Factura se afla in listingul de facturi blocate la plata.");
			} else {
				String nextApproval = "";
				int idStoreLine = Integer.parseInt(allInvoices.get(i).get("idStoreLine").toString());
				String approvalsIds = StatusFacturiBD.getStoreLineApprovals(idStoreLine);
				if(approvalsIds.equals("") || approvalsIds.contains("fictive_user_dpi") || approvalsIds.contains("fictive_user_magazin") || approvalsIds.contains("fictive_user_hq")) {
					if(StatusFacturiBD.isApproved(idStoreLine)) {
						nextApproval = "Linie aprobata";
					} else {
						nextApproval = "Linia nu are aprobator setat: ";
						if(approvalsIds.contains("fictive_user_dpi")){
							nextApproval += "Lipsa aprobator DPI";
						} else if(approvalsIds.contains("fictive_user_magazin")) {
							nextApproval += "Lipsa Director magazin";
						} else if (approvalsIds.contains("fictive_user_hq")) {
							nextApproval += "Lipsa Admin Sediu";
						} else if (approvalsIds.contains("fictive_regional")) {
							nextApproval += "Lipsa Director Regional";
						} else if (approvalsIds.contains("fictive_billa")) {
							nextApproval += "Lipsa Administrator Billa";
						}
						else {
							nextApproval += "Lipsa altele";
						}
					}
				} else {
					String [] approvalsSplit = approvalsIds.split(",");
					for(String approvalId : approvalsSplit) {
						try {
							if(!approvalId.equals("fictive_user_dpi") && !approvalId.equals("fictive_user_magazin") && !approvalId.equals("fictive_regional") && !approvalId.equals("fictive_user")) {
								User user = UserLocalServiceUtil.getUser(Integer.parseInt(approvalId));
								nextApproval += user.getFullName() + ",";
							}
						} catch (NumberFormatException | PortalException | SystemException e) {
							e.printStackTrace();
						}
					}
				}
				if(nextApproval.endsWith(",")) {
					nextApproval = nextApproval.substring(0, nextApproval.length()-1);
				}
				allApprovals.put(String.valueOf(i), nextApproval);	
			}
		}
		return allApprovals;
	}
	
	@SuppressWarnings("unchecked")
	public void serveResource(ResourceRequest resourceRequest, ResourceResponse resourceResponse) throws IOException, PortletException {
		
		log.info("Status Facturi - serveResource()");
		
		String action = ParamUtil.getString(resourceRequest, "action");
		
		String denumireMagazin = resourceRequest.getParameter("DenumireMagazin");
		String denumireFurnizor = resourceRequest.getParameter("DenumireFurnizor");
//		String Aprobator = resourceRequest.getParameter("Approbator");
		String DataInregistrareDeLa = resourceRequest.getParameter("dataDeLa");
		String DataInregistrarePanaLa = resourceRequest.getParameter("dataPanaLa");
		String statusFact = resourceRequest.getParameter("statusFactura");
		
		
		
		if (action.equals("exportExcel")) {
			List<HashMap<String,Object>> allInvoicesWithoutApprovals = StatusFacturiBD.getAllInvoices(0, 80000, "", "", denumireMagazin, denumireFurnizor, DataInregistrareDeLa, DataInregistrarePanaLa, statusFact);
			HashMap<String,Object> allApprovals = StatusFacturi.getAllApprovals(allInvoicesWithoutApprovals);
			List<HashMap<String,Object>> allInvoices = new ArrayList<HashMap<String,Object>>();
			//construire lista de facturi cu aprobatori
			for(int i = 0 ; i < allInvoicesWithoutApprovals.size(); i++){
				HashMap<String,Object> hash = allInvoicesWithoutApprovals.get(i);
				hash.put("aprobator", allApprovals.get(""+i));
				allInvoices.add(hash);
			}
			
			String[][] header = new String[8][];
			for (int i = 0; i < 8; i++) {
				header[i] = new String[2];
			}

			header[0][0] = "nrFactura";
			header[0][1] = "Nr factura";
			header[1][0] = "dataFactura";
			header[1][1] = "Data factura";
			header[2][0] = "dataInregistrare";
			header[2][1] = "Data inregistrare";
			header[3][0] = "magazin_name";
			header[3][1] = "Magazin";
			header[4][0] = "furnizor";
			header[4][1] = "Furnizor";
			header[5][0] = "aprobator";
			header[5][1] = "Aprobator";
			header[6][0] = "totalFaraTva";
			header[6][1] = "Total Fara TVA";
			header[7][0] = "totalCuTva";
			header[7][1] = "Total Cu TVA";

			try {
				ExcelUtil.getExcel(allInvoices, header, "Raport_Facturi", resourceResponse);
			} catch (IOException e) {
				e.printStackTrace();
			} 
		} else if (action.equals("getInventoryDetails")){
			PrintWriter writer = resourceResponse.getWriter();
			String idStoreLine = ParamUtil.getString(resourceRequest, "idStoreLine").toString();
			List<HashMap<String,Object>> inventoryDetails = InvoiceHeader.getInventoryDetailsByIdStoreLine(idStoreLine);
			JSONArray jsonInvNo = new JSONArray();
			if (inventoryDetails != null) {
				for (int i = 0; i < inventoryDetails.size(); i++) {
					JSONObject jsonLine = new JSONObject();
					jsonLine.put("nr_inv", inventoryDetails.get(i).get("inventory_no").toString());
					jsonLine.put("data_pif", inventoryDetails.get(i).get("pif_date").toString());
					jsonLine.put("receptie", inventoryDetails.get(i).get("reception").toString());
					jsonInvNo.add(jsonLine);
				}
				writer.print(jsonInvNo.toJSONString());
			} else {
				writer.print("");
			}
			
		} else if (action.equals("exportUsersExcel")) {
			List<HashMap<String,Object>> allusersGroups = UsersInformations.getALlUsersGroups();
			System.out.println(allusersGroups);
			
			for(int i = 0; i < allusersGroups.size()-1 ;i++) {
				HashMap<String,Object> oldHash = allusersGroups.get(i);
				try {
					if(allusersGroups.get(i).get("user_id") != null ){
						User user = UserLocalServiceUtil.getUser(Integer.parseInt(allusersGroups.get(i).get("user_id").toString()));
						if(user.getAddresses().size() > 0 ){
							if(user.getAddresses().get(0).getStreet2() != null){
								oldHash.put("magazin", DefStore.getStoreNameById(DefStore.getIdByStoreId(user.getAddresses().get(0).getStreet2().toString())));
							} else {
								oldHash.put("magazin", "");
							}
						} else {
							oldHash.put("magazin", "");
						}
					} else {
						oldHash.put("magazin", "");
					}
					allusersGroups.remove(i);
					allusersGroups.add(i, oldHash);
				} catch (NumberFormatException | PortalException | SystemException e) {
					System.out.println("EROAREE");
				}
			}
			
			String[][] header = new String[4][];
			for (int i = 0; i < 4; i++) {
				header[i] = new String[2];
			}

			header[0][0] = "first_name";
			header[0][1] = "First Name";
			header[1][0] = "last_name";
			header[1][1] = "Last Name";
			header[2][0] = "user_groups";
			header[2][1] = "Groups";
			header[3][0] = "magazin";
			header[3][1] = "Store";

			try {
				ExcelUtil.getExcel(allusersGroups, header, "Users_groups", resourceResponse);
			} catch (IOException e) {
				e.printStackTrace();
			} 
		} else if (action.equals("next") || action.equals("prev") || action.equals("last") || action.equals("first") || action.equals("filter")) {
			
			PrintWriter writer = resourceResponse.getWriter();
			// read AJAX parameters used in filtering and pagination
			int start = Integer.parseInt(ParamUtil.getString(resourceRequest, "start").toString());
			int count = Integer.parseInt(ParamUtil.getString(resourceRequest, "count").toString());
			int total = Integer.parseInt(ParamUtil.getString(resourceRequest, "total").toString());
			String factNo = ParamUtil.getString(resourceRequest, "factNo").toString();
			String supplier = ParamUtil.getString(resourceRequest, "supplier").toString();
			
			List<HashMap<String, Object>> allInvoicesWithoutApprovals = com.profluo.ecm.model.vo.StatusFacturiBD.getAllInvoices(start, count, factNo, supplier, null, null, null, null, null);
			total = com.profluo.ecm.model.vo.StatusFacturiBD.getAllInvoicesCount(factNo, supplier);
			
			HashMap<String,Object> allApprovals = StatusFacturi.getAllApprovals(allInvoicesWithoutApprovals);

			List<HashMap<String,Object>> allInvoices = new ArrayList<HashMap<String,Object>>();
			//construire lista de facturi cu aprobatori
			for(int i = 0 ; i < allInvoicesWithoutApprovals.size(); i++){
				HashMap<String,Object> hash = allInvoicesWithoutApprovals.get(i);
				hash.put("aprobator", allApprovals.get(""+i));
				allInvoices.add(hash);
			}

			JSONArray jsonInvoices = new JSONArray();
			if (allInvoices != null) {
				jsonInvoices = DatabaseConnectionManager.convertListToJson(allInvoices);
			}
			JSONObject jsonResponse = new JSONObject();

			jsonResponse.put("action", action);
			jsonResponse.put("start", start);
			jsonResponse.put("count", count);
			jsonResponse.put("total", total);
			jsonResponse.put("values", jsonInvoices);
			writer.print(jsonResponse.toJSONString());
		}
		
	}
 

}
