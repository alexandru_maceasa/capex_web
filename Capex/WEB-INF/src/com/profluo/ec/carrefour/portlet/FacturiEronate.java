package com.profluo.ec.carrefour.portlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.List;

import javax.portlet.PortletException;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.util.bridges.mvc.MVCPortlet;
import com.profluo.ecm.model.db.DatabaseConnectionManager;
import com.profluo.ecm.model.db.InvoiceHeader;

/**
 * Portlet implementation class FacturiEronate
 */
public class FacturiEronate extends MVCPortlet {

	@SuppressWarnings("unchecked")
	public void serveResource(ResourceRequest resourceRequest, ResourceResponse resourceResponse) throws IOException, PortletException {
		String action = ParamUtil.getString(resourceRequest, "action");
		if (action.equals("filter") || action.equals("next") || action.equals("prev") || action.equals("last") || action.equals("first")) {
			PrintWriter writer = resourceResponse.getWriter();
			int start = Integer.parseInt(ParamUtil.getString(resourceRequest, "start").toString());
			int count = Integer.parseInt(ParamUtil.getString(resourceRequest, "count").toString());
			int total = Integer.parseInt(ParamUtil.getString(resourceRequest, "total").toString());
			String invoiceNo = ParamUtil.getString(resourceRequest, "invoiceNo");
			String supplierName = ParamUtil.getString(resourceRequest, "supplierName");
			int companyId = Integer.parseInt(ParamUtil.getString(resourceRequest, "companyId"));

			List<HashMap<String, Object>> allRegVouchers = InvoiceHeader.getAllWrongInvoices(start, count, invoiceNo, supplierName, companyId);
			total = 1;
			JSONArray jsonLines = new JSONArray();
			if (allRegVouchers != null) {
				jsonLines = DatabaseConnectionManager.convertListToJson(allRegVouchers);
			}
			JSONObject jsonResponse = new JSONObject();

			jsonResponse.put("action", action);
			jsonResponse.put("start", start);
			jsonResponse.put("count", count);
			jsonResponse.put("total", total);
			jsonResponse.put("values", jsonLines);
			writer.print(jsonResponse.toJSONString());
		}
	}
}
