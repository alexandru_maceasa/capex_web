package com.profluo.ec.carrefour.portlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletConfig;
import javax.portlet.PortletException;
import javax.portlet.PortletPreferences;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;
import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.portlet.LiferayPortletConfig;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.servlet.SessionMessages;
import com.liferay.portal.kernel.util.JavaConstants;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.model.User;
import com.liferay.portal.service.UserServiceUtil;
import com.liferay.portal.theme.ThemeDisplay;
import com.liferay.portal.util.PortalUtil;
import com.liferay.portlet.PortletPreferencesFactoryUtil;
import com.liferay.util.bridges.mvc.MVCPortlet;
import com.profluo.ecm.controller.ControllerUtils;
import com.profluo.ecm.controller.InventoryNumberBO;
import com.profluo.ecm.model.db.DPIHeader;
import com.profluo.ecm.model.db.DatabaseConnectionManager;
import com.profluo.ecm.model.db.GeneralCodes;
import com.profluo.ecm.model.db.GenericDBStatements;
import com.profluo.ecm.model.db.VoucherHeader;
import com.profluo.ecm.model.vo.DefIASVo;
import com.profluo.ecm.model.vo.DefIFRSVo;
import com.profluo.ecm.model.vo.DefProductVo;

/**
 * Portlet implementation class Vouchers
 */
public class Vouchers extends MVCPortlet {
	/**
	 * 
	 */
	private final Log log = LogFactoryUtil.getLog("PROFLUO-" + MVCPortlet.class.getName());
	
	/**
	 * Getting information about portlet initialization.
	 */
    @Override
    public void init() throws PortletException {
    	log.info("[PROFLUO] Initialize Vouchers portlet.");
    	super.init();
    }
    
    /**
     * All ajax requests come in here
     */
    @SuppressWarnings("unchecked")
	@Override
    public void serveResource(ResourceRequest resourceRequest, ResourceResponse resourceResponse) throws IOException, PortletException {
    	log.info("[AJAX] VouchersController");
    	
    	String action = ParamUtil.getString(resourceRequest, "action");
    	log.info("[AJAX] action: " + action);
    	// used to write the output answer
    	PrintWriter writer = resourceResponse.getWriter();
    	
    	if (action.equals("getInventoryNo")) {
    		int storeId = Integer.parseInt(ParamUtil.getString(resourceRequest, "storeId").toString());
    		int iasId 	= Integer.parseInt(ParamUtil.getString(resourceRequest, "iasId").toString());
    		int tipInv 	= Integer.parseInt(ParamUtil.getString(resourceRequest, "tipInv").toString());
    		int count 	= Integer.parseInt(ParamUtil.getString(resourceRequest, "count").toString());
    		
    		List<String> listInvNo = null;
    		
    		if (iasId != 0 && storeId != 0 && tipInv != 0) {
    			if (tipInv == InventoryNumberBO.DE_GRUP) {
    				listInvNo = InventoryNumberBO.getInvetoryNumberSequence(storeId, iasId, 1);
    			} else if (tipInv == InventoryNumberBO.INDIVIDUAL) {
    				listInvNo = InventoryNumberBO.getInvetoryNumberSequence(storeId, iasId, count);
    			}
    		}
    		
    		JSONArray jsonInvNo = new JSONArray();
    		if (listInvNo != null) {
    			for (int i = 0; i < listInvNo.size(); i++) {
    				JSONObject jsonLine = new JSONObject();
    				jsonLine.put("nr_inv", listInvNo.get(i).toString());
    				jsonLine.put("data_pif", "");
    				jsonLine.put("receptie", "");
    				jsonInvNo.add(jsonLine);
    			}
    			writer.print(jsonInvNo.toJSONString());
    		} else {
    			writer.print("");
    		}
    	} else if (action.equals("getIasInfo")){ 
    		String id = ParamUtil.getString(resourceRequest, "iasId").toString();
    		List<HashMap<String, Object>> product = DefIASVo.getById(id);
			JSONArray jsonProduct = new JSONArray();
			if (product != null) {
				jsonProduct = DatabaseConnectionManager
						.convertListToJson(product);
			}
			JSONObject jsonResponse = new JSONObject();
			jsonResponse.put("product", jsonProduct);
			//System.out.println(jsonProduct);
			writer.print(jsonResponse.toJSONString());
    	} else if (action.equals("getIfrsInfo")){
			String id = ParamUtil.getString(resourceRequest, "ifrsId").toString();
			List<HashMap<String, Object>> product = DefIFRSVo.getById(id);
			JSONArray jsonProduct = new JSONArray();
			if (product != null) {
				jsonProduct = DatabaseConnectionManager
						.convertListToJson(product);
			}
			JSONObject jsonResponse = new JSONObject();
			jsonResponse.put("product", jsonProduct);
			//System.out.println(jsonProduct);
			writer.print(jsonResponse.toJSONString());
		} else if (action.equals("showDpi")) {
			String idDpi = ParamUtil.getString(resourceRequest, "dpi");
			System.out.println(idDpi);
			// get DPI header info based on DPI id
			List<HashMap<String, Object>> oneDpi = null;

			// oneDpi = GenericDBStatements.getAllByFieldName("dpi_header",
			// "id", idDpi);
			oneDpi = DPIHeader.getAllByFieldName("id", idDpi);

			JSONArray jsonDpiHeader = new JSONArray();
			if (oneDpi != null) {
				jsonDpiHeader = DatabaseConnectionManager
						.convertListToJson(oneDpi);
			}
			System.out.println(jsonDpiHeader);
			// get DPI lines info based on DPI id
			List<HashMap<String, Object>> dpiLines = null;

			dpiLines = GenericDBStatements.getAllByFieldName("dpi_line",
					"id_dpi", idDpi);

			JSONArray jsonDpiLines = new JSONArray();
			if (dpiLines != null) {
				jsonDpiLines = DatabaseConnectionManager
						.convertListToJson(dpiLines);
			}

			// create JSON response
			JSONObject jsonResponse = new JSONObject();
			jsonResponse.put("header", jsonDpiHeader);
			jsonResponse.put("lines", jsonDpiLines);
			// send AJAX response
			writer.print(jsonResponse.toJSONString());
    	}else if (action.equals("getProdInfo")) {
    		// ajax to get product info for one row based on id of the selected product
    		String id = ParamUtil.getString(resourceRequest, "prodId").toString();
    		List<HashMap<String,Object>> product = DefProductVo.getById(id);
	    	JSONArray jsonProduct = new JSONArray();
	    	if (product != null) {
	    		jsonProduct = DatabaseConnectionManager.convertListToJson(product);
	    	}
	    	JSONObject jsonResponse = new JSONObject();
			jsonResponse.put("product", jsonProduct);
			writer.print(jsonResponse.toJSONString());
    	} else if (action.equals("findSupplier")) {
    		// ajax to find supplier by name
    		String query = ParamUtil.getString(resourceRequest, "keywords");
    		String idCompany = ParamUtil.getString(resourceRequest, "id_company");
    		log.info("[AJAX] query: " + query);

    		writer.print(ControllerUtils.filterSupplierByName(query, Integer.parseInt(idCompany)).toJSONString());
    	} else if (action.equals("filter") || action.equals("next") || action.equals("prev") 
    			|| action.equals("last") || action.equals("first")) {
    		// ajax for pagination and filtering in listing
    		int stage = Integer.parseInt(ParamUtil.getString(resourceRequest, "stage").toString());
	    	int start = Integer.parseInt(ParamUtil.getString(resourceRequest, "start").toString());
	    	int count = Integer.parseInt(ParamUtil.getString(resourceRequest, "count").toString());
	    	int total = Integer.parseInt(ParamUtil.getString(resourceRequest, "total").toString());
	    	int companyId = Integer.parseInt(ParamUtil.getString(resourceRequest, "company_id").toString());
	    	String voucherNo = ParamUtil.getString(resourceRequest, "voucher_no");
	    	String voucherDate = ParamUtil.getString(resourceRequest, "voucher_date");
	    	
	    	log.info("[AJAX] start: " + count);
	    	log.info("[AJAX] count: " + start);
	    	log.info("[AJAX] total: " + total);
	    	
	    	List<HashMap<String,Object>> allInvoices = null;
	    	
			allInvoices = VoucherHeader.getAllFiltered(start, count, companyId, voucherNo, voucherDate, stage);
			// if total is 0 we need to refresh the count
			if (total == 0) {
				total = VoucherHeader.getAllFilteredCount(start, count, companyId, voucherNo, voucherDate, stage);
			}
	    	
	    	JSONArray jsonInvoices = new JSONArray();
	    	if (allInvoices != null) {
	    		jsonInvoices = DatabaseConnectionManager.convertListToJson(allInvoices);
	    	}
	    	JSONObject jsonResponse = new JSONObject();
	    	
			jsonResponse.put("action", action);
			jsonResponse.put("start", start);
			jsonResponse.put("count", count);
			jsonResponse.put("total", total);
			jsonResponse.put("values", jsonInvoices);
			writer.print(jsonResponse.toJSONString());
    	}
    }
    
    /**
     * 
     * @param request
     * @param response
     */
    public void saveVoucher(ActionRequest request, ActionResponse response) {
    	// remove default error message from Liferay
    	PortletConfig portletConfig = (PortletConfig)request.getAttribute(JavaConstants.JAVAX_PORTLET_CONFIG);
    	LiferayPortletConfig liferayPortletConfig = (LiferayPortletConfig) portletConfig;
    	SessionMessages.add(request, liferayPortletConfig.getPortletId() + SessionMessages.KEY_SUFFIX_HIDE_DEFAULT_ERROR_MESSAGE);
		
		log.info("[PROFLUO] Vouchers portlet - saveVoucher()");
		//System.out.println("Invoice_header excludes: " + excludesForStoreLines);
		ControllerUtils.saveRequestParamsOnResponse(request, response, log);
		
		User user = null;
		// get user info
		try {
			user = UserServiceUtil.getUserById(Long.parseLong(request.getRemoteUser()));
		} catch (Exception pe) {
			pe.printStackTrace();
		}
		
		// retrieve params
		String categCapex = "";
		if (request.getParameter("categ_capex") != null) { 
			categCapex = request.getParameter("categ_capex").toString(); 
			if ( categCapex == "" ){
				categCapex = "0";
			}
		}
		String idBon = "";
		if (request.getParameter("id") != null) { idBon = request.getParameter("id").toString(); }
		String raion = "";
		if (request.getParameter("raion") != null) { raion = request.getParameter("raion").toString(); }
		String project = "";
		if (request.getParameter("project") != null) { 
			project = request.getParameter("project").toString();
			if ( project == "" ){
				project = "0";
			}
		}
		String listaInitiative = "";
		if (request.getParameter("lista_initiative") != null) {
			listaInitiative = request.getParameter("lista_initiative").toString();;
			if ( listaInitiative == "" ){
				listaInitiative = "0";
			}
			
		}
		// get article lines
        String[][][] lineData = null;
        String[][][] lineDataSplit = null;
		if (request.getParameter("article_table") != null) { 
			lineData = ControllerUtils.convertToArrayMatrixFromJSON(request.getParameter("article_table")); 
		}
		if (request.getParameter("article_table_lines") != null) { 
			lineDataSplit = ControllerUtils.convertToArrayMatrixFromJSON(request.getParameter("article_table_lines")); 
		}
		String[][][] voucherLines = ControllerUtils.mergeArrays(lineData, lineDataSplit);
		
		// handle errors
		if (idBon.equals("") || raion.equals("") || categCapex.equals("")) {
			SessionErrors.add(request, "missingParameters");
			response.setRenderParameter("mvcPath", "/html/vouchers/inreg_voucher.jsp");
			return;
		} else {
			// initiative with missing initiative selected
			if (categCapex.endsWith("1") && listaInitiative.equals("")) {
				SessionErrors.add(request, "missingInitiative");
				response.setRenderParameter("mvcPath", "/html/vouchers/inreg_voucher.jsp");
				return;
			}
			// new project but without new project selected
			if (categCapex.endsWith("2") && project.equals("")) {
				SessionErrors.add(request, "missingProject");
				response.setRenderParameter("mvcPath", "/html/vouchers/inreg_voucher.jsp");
				return;
			}
			
			//attributes to be ignored for table invoice_store_line
			ArrayList<String> excludedFields = new ArrayList<String>();
			String [] excludedFieldsItems = { "dt_ifrs", "dt_ras", "lot", "vizualizare_dpi", "detalii", "cont_imob_in_fct", "cont_imob_in_avans", "cont_imob_avans", "cont_imob_in_curs", "delete_line", "inventory"};
			for (int i = 0; i < excludedFieldsItems.length; i++){
				excludedFields.add(excludedFieldsItems[i]);
			}
			
			// insert inventory lines 1st
			for (int i = 0; i < voucherLines.length; i++) {
	        	// TODO: remove comment below after test ends
				int voucherLineId = GenericDBStatements.insertEntry(voucherLines[i], "voucher_line", excludedFields);
				/**** INSERT VOUCHER INVENTORY ****/ 
				// loop all fieds on one row
				for (int j = 0; j < voucherLines[i].length; j++) {
					// if the field is inventory
					if (voucherLines[i][j][0].equals("inventory")) {
						System.out.println("Inventory on line[" + i + "] : " + voucherLines[i][j][1]);
						System.out.println("Inv for voucher: " + idBon + " and line: " + voucherLineId);
						Object obj = JSONValue.parse(voucherLines[i][j][1]);
						JSONArray jsonArr = (JSONArray) obj;
						// prepare voucher inventory_line_inv values
						String voucherInvInfo[][] = new String[5][2];
						voucherInvInfo[0][0] = "id_voucher_line";
						voucherInvInfo[0][1] = String.valueOf(voucherLineId);
						voucherInvInfo[1][0] = "id_voucher";
						voucherInvInfo[1][1] = idBon;
						
						//String[][][] voucherInventoryLine = ControllerUtils.convertToArrayMatrixFromJSON(voucherLines[i][j][1]);
						for (int k = 0; k < jsonArr.size(); k++) {
							voucherInvInfo[2][0] = "pif_date";
							String pifDate = ((JSONObject)jsonArr.get(k)).get("data_pif").toString().replace("[\"", "").replace("\"]","");
							System.out.println("DATE: " + pifDate);
							voucherInvInfo[2][1] = pifDate;
							
							voucherInvInfo[3][0] = "inventory_no";
							voucherInvInfo[3][1] = ((JSONObject)jsonArr.get(k)).get("nr_inv").toString();
							
							voucherInvInfo[4][0] = "reception";
							voucherInvInfo[4][1] = ((JSONObject)jsonArr.get(k)).get("receptie").toString();
							// insert into voucher_line_inv
							ArrayList<String> excludedFieldsInv = new ArrayList<String>();
							GenericDBStatements.insertEntry(voucherInvInfo, "voucher_line_inv", excludedFieldsInv);
						}
						break;
					}
				}
			}
			
			// set values for voucher_header table update
			int arrSize = 9;
			String[][] arrFields = new String [arrSize][2];
	        for (int i = 0; i < arrSize; i++) {
	        	arrFields[i] = new String[2];
	        }
	        
	        arrFields[0][0] = "id_aisle";
	        arrFields[0][1] = raion;
	        arrFields[1][0] = "tip_capex";
	        arrFields[1][1] = categCapex;
	        arrFields[2][0] = "id_new_prj";
	        if (categCapex.equals("2")) {
	        	arrFields[2][1] = project;
	        } else {
	        	arrFields[2][1] = "0";
	        }
	        arrFields[3][0] = "id_initiative";
	        if (categCapex.equals("1")) {
	        	arrFields[3][1] = listaInitiative;
	        } else {
	        	arrFields[3][1] = "0";
	        }
	        // set user that made the changes on the voucher
	        arrFields[4][0] = "id_user";
	        arrFields[4][1] = String.valueOf((int)user.getUserId());
	        // set the stage on the voucher to be exported in Optimal
	        arrFields[5][0] = "stage";
	        arrFields[5][1] = String.valueOf(2);
	        //set accounting month
	        arrFields[6][0] = "accounting_month";
	        arrFields[6][1] = GeneralCodes.getAccountingMonth();
	        arrFields[7][0] = "accounting_year";
	        arrFields[7][1] = GeneralCodes.getAccountingYear();
	        
	        Calendar calendar = Calendar.getInstance();
		    java.sql.Timestamp currentTS = new java.sql.Timestamp(calendar.getTime().getTime());
		    
	        arrFields[8][0] = "updated";
	        arrFields[8][1] = currentTS.toString();
	        
			ArrayList<String> excludeList = new ArrayList<String>();
        	//attrs for where statement
        	ArrayList<String> whereStatement = new ArrayList<String>();
        	//attrs values for where statement
        	ArrayList<String> whereStatementValues = new ArrayList<String>();
        	whereStatement.add("id");
        	whereStatementValues.add(idBon);
        	// update voucher_header
			GenericDBStatements.updateEntry(arrFields, "voucher_header", excludeList, whereStatement, whereStatementValues);
		}
		
		try {			
	        SessionMessages.add(request, "voucherSaved");
	        response.setRenderParameter("mvcPath", "/html/vouchers/view.jsp");
	    } catch (Exception e) {
	    	e.printStackTrace();
	        SessionErrors.add(request, "error");
	        response.setRenderParameter("mvcPath", "/html/vouchers/inreg_voucher.jsp");
	    }
    }

	@Override
	public void render(RenderRequest renderRequest, RenderResponse renderResponse) throws PortletException, IOException {
		HttpServletRequest request = PortalUtil.getHttpServletRequest(renderRequest);
    	String p_p_id = PortalUtil.getOriginalServletRequest(request).getParameter("p_p_id");
    	String path = PortalUtil.getOriginalServletRequest(request).getParameter("_" + p_p_id + "_mvcPath");
    	
    	try {
	    	ThemeDisplay themeDisplay = ((ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY));
	    	String portletId = themeDisplay.getPortletDisplay().getId();
	    	PortletPreferences portletSetup = PortletPreferencesFactoryUtil.getLayoutPortletSetup(themeDisplay.getLayout(), portletId);
	    	String portletCustomTitle = themeDisplay.getPortletDisplay().getTitle();
	    	portletCustomTitle = portletSetup.getValue("portletSetupTitle_" + themeDisplay.getLanguageId(), portletCustomTitle);
	    	
	        ThemeDisplay td  = (ThemeDisplay) renderRequest.getAttribute(WebKeys.THEME_DISPLAY);
	        log.info("[PROFLUO][VOUCHER] User full name: " + td.getUser().getFullName());
	        log.info("[PROFLUO][VOUCHER] current url: " + td.getURLCurrent());
	        log.info("[PROFLUO][VOUCHER] portal url: " + td.getURLPortal());
	        log.info("[PROFLUO][VOUCHER] home url: " + td.getURLHome());
			log.info("[PROFLUO][VOUCHER] portlet - render()");
			log.info("[PROFLUO][VOUCHER] Context Path: " + renderRequest.getContextPath());
	    	log.info("[PROFLUO][VOUCHER] Portlet ID	: " + p_p_id);
	    	log.info("[PROFLUO][VOUCHER] Portlet Path: " + path);
	    	// BASED on portlet title make diffrent queries for data
	    	log.info("[PROFLUO][VOUCHER] Portlet Title: " + portletCustomTitle);
	    	
	    	// maybe we need to know in which of the portlets views we are
		    if (path == null || path.equals("view.jsp")) {
	    		log.info("[PROFLUO] LISTING VOUCHER render()");
	
	    	} else if (path.endsWith("inreg_voucher.jsp")) {
	    		log.info("[PROFLUO] Inreg VOUCHER render()");
	    		
	    	}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	    super.render(renderRequest, renderResponse);
	}
}
