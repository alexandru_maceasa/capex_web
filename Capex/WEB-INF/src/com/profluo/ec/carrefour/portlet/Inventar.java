package com.profluo.ec.carrefour.portlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.List;

import javax.portlet.PortletException;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.util.bridges.mvc.MVCPortlet;
import com.profluo.ecm.model.db.DatabaseConnectionManager;
import com.profluo.ecm.model.db.InventoryHeader;
import com.profluo.ecm.utils.ExcelUtil;

/**
 * Portlet implementation class Inventar
 */
public class Inventar extends MVCPortlet {
	
	@SuppressWarnings("unchecked")
	public void serveResource(ResourceRequest resourceRequest, ResourceResponse resourceResponse) throws IOException, PortletException {
		
		String action = ParamUtil.getString(resourceRequest, "action");
		if (action.equals("exportExcel")) {
			System.out.println("Importing invoices into inventory................");
			InventoryHeader.importInvoicesIntoInventory();
			System.out.println("done importing invoices");
			System.out.println("Importing vouchers into inventory................");
			InventoryHeader.importVouchersIntoInventory();
			System.out.println("done importing vouchers");
			System.out.println("Importing working clothes into inventory................");
			InventoryHeader.importWorkingClothesIntoInventory();
			System.out.println("done importing working clothes");
			System.out.println("Importing deconturi");
			InventoryHeader.importCashExp();
			System.out.println("done importing deconturi");
			
			int company =  ParamUtil.getInteger(resourceRequest, "company");
			String exportType = ParamUtil.getString(resourceRequest, "export_type");
			
			if(exportType.equals("exportDocumente")){
				//incarca lista cu toate documentele corespunzatoare societatii introduse
				List<HashMap<String, Object>> allDocsByCompany = InventoryHeader.getInventoryByCompany(company);
				String[][] header;
				if(company == 1  || company == 7 || company == 8 || company == 2 ) {
					header = new String[38][];
					for (int i = 0; i < 38; i++) {
						header[i] = new String[2];
					}
				} else {
					header = new String[31][];
					for (int i = 0; i < 31; i++) {
						header[i] = new String[2];
					}
				}
				
				header[0][0] = "docNo";
				header[0][1] = "Numar Document";
				header[1][0] = "dataDoc";
				header[1][1] = "Data Document";
				header[2][0] = "codSocietate";
				header[2][1] = "Cod societate";
				header[3][0] = "codMagazin";
				header[3][1] = "Cod centru de cost";
				header[4][0] = "raion";
				header[4][1] = "Raion";
				header[5][0] = "codFurnizor";
				header[5][1] = "Cod furnizor";
				header[6][0] = "denumire";
				header[6][1] = "Denumire";
				header[7][0] = "produs";
				header[7][1] = "Produs";
				header[8][0] = "codProdus";
				header[8][1] = "Cod produs";
				header[9][0] = "valAchizite";
				header[9][1] = "Valoare achizitie";
				header[10][0] = "tipInregistrare";
				header[10][1] = "Tip inregistrare";
				header[11][0] = "actiuneMF";
				header[11][1] = "Actiune MF";
				header[12][0] = "gestiune";
				header[12][1] = "Gestiune";
				header[13][0] = "clasificareIT";
				header[13][1] = "Clasificare";
				header[14][0] = "nrInventar";
				header[14][1] = "Numar inventar";
				header[15][0] = "grupaRAS";
				header[15][1] = "Cod grupa clasificare";
				header[16][0] = "grupaIFRS";
				header[16][1] = "Cod grupa clasificare IFRS";
				header[17][0] = "durataRAS";
				header[17][1] = "Durata amortizare";
				header[18][0] = "durataIFRS";
				header[18][1] = "Durata amortizare IFRS";
				header[19][0] = "dataPIF";
				header[19][1] = "Data PIF";
				header[20][0] = "numeInitiativa";
				header[20][1] = "Initiativa";
				header[21][0] = "createdDate";
				header[21][1] = "Data de creeare";
				header[22][0] = "dpi";
				header[22][1] = "DPI";
				header[23][0] = "numarContract";
				header[23][1] = "Numar contract";
				header[24][0] = "contIas";
				header[24][1] = "cont";
				header[25][0] = "codeLot";
				header[25][1] = "Codul lotului";
				header[26][0] = "lotName";
				header[26][1] = "Denumirea lotului";
				header[26][0] = "type";
				header[26][1] = "Tip document";
				header[27][0] = "moneda";
				header[27][1] = "Moneda";
				header[28][0] = "rataSchimb";
				header[28][1] = "Rata de schimb";
				header[29][0] = "JurnalKontan";
				header[29][1] = "Jurnal Kontan";
				header[30][0] = "VATCodeSAP";
				header[30][1] = "VAT Code SAP";
				
				if(company == 1 || company == 7 || company == 8 || company == 2 ) {
					header[31][0] = "dataScadenta";
					header[31][1] = "Data scadenta";
					header[32][0] = "procentGarantie";
					header[32][1] = "Procent garantie";
					header[33][0] = "cotaTva";
					header[33][1] = "Cota TVA";
					header[34][0] = "valCuTva";
					header[34][1] = "Valoare cu TVA";
					header[35][0] = "vendorAccount";
					header[35][1] = "Vendor Reconciliation Account";
					header[36][0] = "moneda";
					header[36][1] = "Moneda";
					header[37][0] = "rataSchimb";
					header[37][1] = "Rata de schimb";
					
				}
				
				try {
					if(allDocsByCompany.size() > 0 ){
						ExcelUtil.getExcel(allDocsByCompany, header, allDocsByCompany.get(0).get("codSocietate").toString(), resourceResponse);
					} else {
						ExcelUtil.getExcel(allDocsByCompany, header, "Raport_documente", resourceResponse);
					}
				} catch (IOException e) {
					e.printStackTrace();
				}
					
				System.out.println("Updating inventory stage............");
				InventoryHeader.updateInventoryStage(company);
				System.out.println("Done updating");
				
				System.out.println("Updating documents optimal status..........");
				InventoryHeader.updateDocOptimalStatus(company);
				System.out.println("Done updating");
			}
		} else if (action.equals("filter") || action.equals("next") || action.equals("prev") || action.equals("last") || action.equals("first")) {
			PrintWriter writer = resourceResponse.getWriter();
			int start = Integer.parseInt(ParamUtil.getString(resourceRequest, "start").toString());
			int tipCapex = ParamUtil.getInteger(resourceRequest, "tip_capex");
			int count = Integer.parseInt(ParamUtil.getString(resourceRequest, "count").toString());
			int total = Integer.parseInt(ParamUtil.getString(resourceRequest, "total").toString());
			String societate = ParamUtil.getString(resourceRequest, "societate").toString();
			String initiativa = ParamUtil.getString(resourceRequest, "initiativa").toString();
			String proiectNou = ParamUtil.getString(resourceRequest, "proiectNou").toString();
			String magazin = ParamUtil.getString(resourceRequest, "magazin");
			int fromYear = Integer.parseInt(ParamUtil.getString(resourceRequest, "from_year").toString());
			int toYear = Integer.parseInt(ParamUtil.getString(resourceRequest, "to_year").toString());
			int fromMonth = Integer.parseInt(ParamUtil.getString(resourceRequest, "from_month").toString());
			int toMonth = Integer.parseInt(ParamUtil.getString(resourceRequest, "to_month").toString());

			List<HashMap<String, Object>> allLines = null;

			allLines = InventoryHeader.getAllInfoWithLimit(start, count, societate, initiativa, proiectNou, magazin,
					fromYear, toYear, fromMonth, toMonth, tipCapex);
			total = InventoryHeader.getInventoryTotal(societate, initiativa, proiectNou, magazin);

			JSONArray jsonLines = new JSONArray();
			if (allLines != null) {
				jsonLines = DatabaseConnectionManager.convertListToJson(allLines);
			}
			JSONObject jsonResponse = new JSONObject();

			jsonResponse.put("action", action);
			jsonResponse.put("start", start);
			jsonResponse.put("count", count);
			jsonResponse.put("total", total);
			jsonResponse.put("values", jsonLines);
			writer.print(jsonResponse.toJSONString());
		} else if(action.equals("exportRaport")) {
			String societate = ParamUtil.getString(resourceRequest, "company_id").toString();
			int tipCapex = ParamUtil.getInteger(resourceRequest, "tip_capex");
			String initiativa = ParamUtil.getString(resourceRequest, "initiativa").toString();
			String proiectNou = ParamUtil.getString(resourceRequest, "proiectNou").toString();
			String magazin = ParamUtil.getString(resourceRequest, "id_magazin").toString();
			int fromYear = ParamUtil.getInteger(resourceRequest, "from_year");
			int toYear = ParamUtil.getInteger(resourceRequest, "to_year");
			int fromMonth =  ParamUtil.getInteger(resourceRequest, "from_month");
			int toMonth =  ParamUtil.getInteger(resourceRequest, "to_month");
			
			System.out.println("magazinul este -> " + magazin);
			System.out.println("ProietcNoi -> " + proiectNou);
			System.out.println("societate = " + societate);
			System.out.println("initiativa = " + initiativa);
			
			List<HashMap<String, Object>> allLines = InventoryHeader.getAllInfoWithLimit(0, 5000000, societate, initiativa, proiectNou,
					magazin, fromYear, toYear, fromMonth, toMonth, tipCapex);
			String[][] header = new String[31][];
			for (int i = 0; i < 31; i++) {
				header[i] = new String[2];
			}
			header[0][0] = "societate";
			header[0][1] = "Societate";
			header[1][0] = "codInitiativa";
			header[1][1] = "Cod initiativa";
			header[2][0] = "initiativa";
			header[2][1] = "Initiativa";
			header[3][0] = "codMagazin";
			header[3][1] = "Cod magazin";
			header[4][0] = "magazin";
			header[4][1] = "Denumire magazin";
			header[5][0] = "codLot";
			header[5][1] = "Cod loturi";
			header[6][0] = "lot";
			header[6][1] = "Loturile";
			header[7][0] = "codProdus";
			header[7][1] = "Cod produs";
			header[8][0] = "denumireProdus";
			header[8][1] = "Denumire produs";
			header[9][0] = "tipInreg";
			header[9][1] = "Tip inregistrare";
			header[10][0] = "gestiune";
			header[10][1] = "Clasificare";
			header[11][0] = "nrInventar";
			header[11][1] = "Numar inventar";
			header[12][0] = "valoareRON";
			header[12][1] = "Valoare operatie - RON";
			header[13][0] = "valoareCURR";
			header[13][1] = "Valoare operatie - EURO";
			header[14][0] = "grupaIFRS";
			header[14][1] = "Cod grupa IFRS";
			header[15][0] = "durataIFRS";
			header[15][1] = "Durata amortizare IFRS";
			header[16][0] = "grupaIAS";
			header[16][1] = "Cod grupa IAS";
			header[17][0] = "durataIAS";
			header[17][1] = "Durata amortizare IAS";
			header[18][0] = "codFurnizor";
			header[18][1] = "Cod furnizor";
			header[19][0] = "furnizor";
			header[19][1] = "Furnizor";
			header[20][0] = "luna";
			header[20][1] = "Luna";
			header[21][0] = "docNo";
			header[21][1] = "Numar document";
			header[22][0] = "dataDoc";
			header[22][1] = "Data";
			header[23][0] = "dataPIF";
			header[23][1] = "Data PIF";
			header[24][0] = "actMF";
			header[24][1] = "Tip operatie";
			header[25][0] = "detinator";
			header[25][1] = "Property/Active";
			header[26][0] = "dpi";
			header[26][1] = "DPI";
			header[27][0] = "nrContract";
			header[27][1] = "Numar contract";
			header[28][0] = "receptie";
			header[28][1] = "Receptie";
			header[29][0] = "cont";
			header[29][1] = "Cont contabil";
			header[30][0] = "docType";
			header[30][1] = "Tip document";
			
			ExcelUtil.getExcel(allLines, header, "Raport_Optimal", resourceResponse);
		}
	}

}
