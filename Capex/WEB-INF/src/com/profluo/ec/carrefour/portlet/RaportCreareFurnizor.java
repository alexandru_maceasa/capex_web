package com.profluo.ec.carrefour.portlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.List;

import javax.portlet.PortletException;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.model.User;
import com.liferay.portal.service.UserServiceUtil;
import com.liferay.util.bridges.mvc.MVCPortlet;
import com.profluo.ecm.model.db.DatabaseConnectionManager;
import com.profluo.ecm.model.db.DefStore;
import com.profluo.ecm.model.db.DefSupplier;
import com.profluo.ecm.utils.ExcelUtil;

/**
 * Portlet implementation class RaportCreareFurnizor
 */
public class RaportCreareFurnizor extends MVCPortlet {
	
	public void serveResource(ResourceRequest resourceRequest, ResourceResponse resourceResponse) throws IOException, PortletException {
		String action = ParamUtil.getString(resourceRequest, "action");
		if (action.equals("filter") || action.equals("next") || action.equals("prev") || action.equals("last") || action.equals("first")) {
			PrintWriter writer = resourceResponse.getWriter();
			int start = Integer.parseInt(ParamUtil.getString(resourceRequest, "start").toString());
			int count = Integer.parseInt(ParamUtil.getString(resourceRequest, "count").toString());
			int total = Integer.parseInt(ParamUtil.getString(resourceRequest, "total").toString());
			String societate = ParamUtil.getString(resourceRequest, "societate").toString();
			
			List<HashMap<String, Object>> allSuppliers = null;

			allSuppliers = DefSupplier.getAllFilteredForRaport(start, count, societate);
			total = DefSupplier.getAllFilteredForRaportCount(societate);
			String supplierCreatorId = "";
			User creator = null;
			String store = "";
			
			//set store for every supplier
			for (HashMap<String, Object> supplier : allSuppliers){
//				supplierCreatorId = supplier.get("user").toString();
				try {
//					creator = UserServiceUtil.getUserById(Long.parseLong(supplier.get("user").toString()));
//					store = DefStore.getIdByStoreId(creator.getAddresses().get(0).getStreet2().toString());
					supplier.put("creator_store", "Sediu");
				} catch (NumberFormatException e) {
					e.printStackTrace();
				}
			}
			JSONArray jsonLines = new JSONArray();
			if (allSuppliers != null) {
				jsonLines = DatabaseConnectionManager.convertListToJson(allSuppliers);
			}
			JSONObject jsonResponse = new JSONObject();

			jsonResponse.put("total", total);
			jsonResponse.put("values", jsonLines);
			writer.print(jsonResponse.toJSONString());
		} else if(action.equals("exportRaport")) {
			String societate = ParamUtil.getString(resourceRequest, "company_id").toString();
			
			List<HashMap<String, Object>> allSuppliers = DefSupplier.getAllFilteredForRaport(0, 50000000, societate);
			
			String supplierCreatorId = "";
			User creator = null;
			String store = "";
			//set store for every supplier
			for (HashMap<String, Object> supplier : allSuppliers){
//				supplierCreatorId = supplier.get("user").toString();
				try {
//					creator = UserServiceUtil.getUserById(Long.parseLong(supplier.get("user").toString()));
//					store = DefStore.getIdByStoreId(creator.getAddresses().get(0).getStreet2().toString());
					supplier.put("creator_store", "Sediu");
				} catch (NumberFormatException e) {
					e.printStackTrace();
				}
			}
			
			String[][] header = new String[18][];
			for (int i = 0; i < 18; i++) {
				header[i] = new String[2];
			}
			header[0][0] = "creator_name";
			header[0][1] = "Nume solicitant";
			header[1][0] = "creator_store";
			header[1][1] = "Magazin";
			header[2][0] = "societate";
			header[2][1] = "Societate";
			header[3][0] = "supplier_code";
			header[3][1] = "Cod furnizor";
			header[4][0] = "supplier_name";
			header[4][1] = "Nume furnizor";
			header[5][0] = "supplier_cui";
			header[5][1] = "CUI";
			header[6][0] = "reg_com";
			header[6][1] = "Numar Reg. Comertului";
			header[7][0] = "address";
			header[7][1] = "Adresa";
			header[8][0] = "phone";
			header[8][1] = "Telefon";
			header[9][0] = "email";
			header[9][1] = "Adresa email";
			header[10][0] = "scadenta";
			header[10][1] = "Scadenta";
			header[11][0] = "bank_acc";
			header[11][1] = "Cont bancar furnizor";
			header[12][0] = "bank";
			header[12][1] = "Banca furnizor";
			header[13][0] = "supplier_status";
			header[13][1] = "Furnizor Activ/Inactiv";
			header[14][0] = "supplier_type";
			header[14][1] = "Tip furnizor";
			header[15][0] = "supplier_request_date";
			header[15][1] = "Data solicitare";
			header[16][0] = "status";
			header[16][1] = "Status";
			header[17][0] = "kontan_creation_date";
			header[17][1] = "Data creare Kontan";

			ExcelUtil.getExcel(allSuppliers, header, "Raport_Furnizori_Creati", resourceResponse);
		}
	}
}
