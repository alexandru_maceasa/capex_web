package com.profluo.ec.carrefour.portlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.List;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletException;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.model.User;
import com.liferay.portal.service.UserServiceUtil;
import com.liferay.util.bridges.mvc.MVCPortlet;
import com.profluo.ecm.controller.ControllerUtils;
import com.profluo.ecm.flows.NotificationTemplates;
import com.profluo.ecm.model.db.DatabaseConnectionManager;
import com.profluo.ecm.model.db.InvoiceApprovals;
import com.profluo.ecm.model.db.InvoiceHeader;

/**
 * Portlet implementation class ModificareAprobatori
 */
public class ModificareAprobatori extends MVCPortlet {
	
	public void updateApproval(ActionRequest request, ActionResponse response) {
		String dataTableLines = request.getParameter("datatable_lines");
		String[][][] lineData = null;
		lineData = ControllerUtils.convertToArrayMatrixFromJSON(dataTableLines);
		
		String storeLineId = "";
		String nextApprovals = "";
		String oldApprovals = "";
		int isFinal = 1;
		
		// prima dimensiune = nr de linii, a doua dimensiune = campurile din tabel, a treia dimensiune = valorile asociate campurilor
		for(int i = 0; i < lineData.length; i++ ) {
			for(int j = 0 ; j < lineData[i].length; j++ ) {
				if(lineData[i][j][0].equals("line_id")) {
					storeLineId = lineData[i][j][1];
				}
				if(lineData[i][j][0].equals("next_approvals")) {
					nextApprovals = lineData[i][j][1];
				}
				if(lineData[i][j][0].equals("old_approvals")) {
					oldApprovals = lineData[i][j][1];
				}
				if(lineData[i][j][0].equals("aprobator_final")) {
					isFinal = Integer.parseInt(lineData[i][j][1]);
				}
			}
			if(oldApprovals.endsWith(",")) {
				oldApprovals = oldApprovals.substring(0, oldApprovals.length()-1);
			}
			if(nextApprovals.endsWith(",")) {
				nextApprovals = nextApprovals.substring(0, nextApprovals.length()-1);
			}
			System.out.println("Store line id : " + storeLineId + " OLD : " + oldApprovals + " -> NEW : " + nextApprovals + " IS FInal : " + isFinal);
			String [] oldApprovalsParts = oldApprovals.split(",");
			for(String approval : oldApprovalsParts) {
				InvoiceApprovals.updateApproval(approval, nextApprovals, storeLineId, isFinal);
			}
			Invoices inv = new Invoices();
			User user = null;
			try {
				user = UserServiceUtil.getUserById(Long.parseLong(request.getRemoteUser()));
			} catch (Exception e) {
				e.printStackTrace();
			}
			List<HashMap<String,Object>> invDetails = InvoiceHeader.getInvoiceInfoByStoreLineId(Integer.parseInt(storeLineId));
			inv.addNotificationForFirstUnapprovedLevel(Integer.parseInt(NotificationTemplates.INREGISTRARE_FACTURA), invDetails.get(0).get("inv_number").toString(), user, 
					invDetails.get(0).get("supplier").toString(), invDetails.get(0).get("id").toString(), invDetails.get(0).get("inv_date").toString(), invDetails.get(0).get("companie").toString());
		}
	}
 
	@SuppressWarnings("unchecked")
	public void serveResource(ResourceRequest resourceRequest, ResourceResponse resourceResponse) throws IOException, PortletException {
		String action = ParamUtil.getString(resourceRequest, "action");
		if (action.equals("filter") || action.equals("next") || action.equals("prev") || action.equals("last") || action.equals("first")) {
			PrintWriter writer = resourceResponse.getWriter();
			int start = Integer.parseInt(ParamUtil.getString(resourceRequest, "start").toString());
			int count = Integer.parseInt(ParamUtil.getString(resourceRequest, "count").toString());
			int total = Integer.parseInt(ParamUtil.getString(resourceRequest, "total").toString());
			String invNo = ParamUtil.getString(resourceRequest, "factNo").toString();
			String furnizor = ParamUtil.getString(resourceRequest, "furnizor").toString();


			List<HashMap<String, Object>> allLines = InvoiceHeader.getAllRegisteredInvoices(start, count, invNo, furnizor);
			total = InvoiceHeader.getAllRegisteredInvoicesCount(invNo);

			JSONArray jsonLines = new JSONArray();
			if (allLines != null) {
				jsonLines = DatabaseConnectionManager.convertListToJson(allLines);
			}
			JSONObject jsonResponse = new JSONObject();

			jsonResponse.put("action", action);
			jsonResponse.put("start", start);
			jsonResponse.put("count", count);
			jsonResponse.put("total", total);
			jsonResponse.put("values", jsonLines);
			writer.print(jsonResponse.toJSONString());
		}
	}

}
