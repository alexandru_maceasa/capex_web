package com.profluo.ec.carrefour.portlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletException;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.util.bridges.mvc.MVCPortlet;
import com.profluo.ecm.controller.ControllerUtils;
import com.profluo.ecm.model.db.DatabaseConnectionManager;
import com.profluo.ecm.model.db.GenericDBStatements;
import com.profluo.ecm.model.db.UsersInformations;

/**
 * Portlet implementation class RepartizareUserMagazin
 */
public class RepartizareUserMagazin extends MVCPortlet {
	
	 @SuppressWarnings("unchecked")
	public void serveResource(ResourceRequest resourceRequest, ResourceResponse resourceResponse) throws IOException, PortletException {
		 String action = ParamUtil.getString(resourceRequest, "action");
		 if(action.equals("reseteazaLista")) {
			 PrintWriter writer = resourceResponse.getWriter();
			 int userId = ParamUtil.getInteger(resourceRequest, "userId");
			 List<HashMap<String,Object>> lista = UsersInformations.getUserStores(UsersInformations.getUserIdByLiferayId(userId));
			 JSONArray jsonInvoices = new JSONArray();
	    	if (lista != null) {
	    		jsonInvoices = DatabaseConnectionManager.convertListToJson(lista);
	    	}
	    	JSONObject jsonResponse = new JSONObject();
			jsonResponse.put("values", jsonInvoices);
			// send AJAX response
			writer.print(jsonResponse.toJSONString());
		 }
	 }
 
	public void salveazaRepartizare(ActionRequest request, ActionResponse response) {
		String[][][] lineData = null;
		String dataTableData = request.getParameter("liniiMagazine");
		lineData = ControllerUtils.convertToArrayMatrixFromJSON(dataTableData);
		String userId = request.getParameter("user_id");
		
		//stergere alocarea veche
		UsersInformations.removeOldAlocations(UsersInformations.getUserIdByLiferayId(userId));
		//informatii ce vor fi adaugate in BD
		String[][] fields = new String[2][2];
		fields[0][0] = "idUser";
		fields[0][1] = String.valueOf(UsersInformations.getUserIdByLiferayId(userId));
		fields[1][0] = "idMagazin";
		//pentru fiecare linie
		for(int i = 0 ; i < lineData.length; i++ ) {
			boolean isChecked = false;
			//pentru fiecare informatie din linie (camp din dataTable)
			for(int j = 0; j < lineData[i].length; j++) {
				if(lineData[i][j][0].equals("checkBox")) {
					if(lineData[i][j][1].equals("true") || lineData[i][j][1].equals("1")) {
						isChecked = true;
					}
				}
				if(lineData[i][j][0].equals("idMagazin")) {
					fields[1][1] = lineData[i][j][1];
				}
			}
			if(isChecked) {
				GenericDBStatements.insertEntry(fields, "user_magazin", new ArrayList<String>());
			}
		}
	}
	
}
