package com.profluo.ec.carrefour.portlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.List;

import javax.portlet.PortletException;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.util.bridges.mvc.MVCPortlet;
import com.profluo.ecm.model.db.DatabaseConnectionManager;
import com.profluo.ecm.model.db.SupplierNotifications;

/**
 * Portlet implementation class NotificariFurnizori
 */
public class NotificariFurnizori extends MVCPortlet {
 
	private final Log log = LogFactoryUtil.getLog("PROFLUO-" + NotificariFurnizori.class.getName());
	
	@SuppressWarnings("unchecked")
	public void serveResource(ResourceRequest resourceRequest,ResourceResponse resourceResponse) 
			throws IOException,PortletException {
		log.info("[AJAX] NotificariFurnizori Controller");
		String action = ParamUtil.getString(resourceRequest, "action");
		log.info("[AJAX] action: " + action);
		PrintWriter writer = resourceResponse.getWriter();
		
		if (action.equals("filter")) {
			String supplierName = ParamUtil.getString(resourceRequest, "supplierName").toString();
			String notificationType = ParamUtil.getString(resourceRequest, "notificationType").toString();
			String keyWord = ParamUtil.getString(resourceRequest, "keyWord").toString();
			List<HashMap<String,Object>> allNotifiedSuppliers = SupplierNotifications.getAllNotifiedSuppliers(supplierName, notificationType, keyWord);
			
			JSONArray jsonSuppliers = new JSONArray();
			if (allNotifiedSuppliers != null) {
				jsonSuppliers = DatabaseConnectionManager.convertListToJson(allNotifiedSuppliers);
			}
			JSONObject jsonResponse = new JSONObject();

			jsonResponse.put("action", action);
			jsonResponse.put("values", jsonSuppliers);
			writer.print(jsonResponse.toJSONString());
		} else if(action.equals("resendNotification")) {
			String notificationId = ParamUtil.getString(resourceRequest, "notificationId").toString();
			int success = SupplierNotifications.resendNotification(notificationId);
			JSONObject jsonResponse = new JSONObject();
			if(success != 0){
				jsonResponse.put("succes", "succes");
			}
			writer.print(jsonResponse.toJSONString());
		}
	}
}
