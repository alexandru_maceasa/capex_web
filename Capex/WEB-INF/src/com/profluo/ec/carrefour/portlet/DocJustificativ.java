package com.profluo.ec.carrefour.portlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletConfig;
import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.portlet.LiferayPortletConfig;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.servlet.SessionMessages;
import com.liferay.portal.kernel.util.JavaConstants;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.model.User;
import com.liferay.portal.service.UserServiceUtil;
import com.liferay.portal.theme.ThemeDisplay;
import com.liferay.util.bridges.mvc.MVCPortlet;
import com.profluo.ecm.controller.ControllerUtils;
import com.profluo.ecm.flows.InvoiceStages;
import com.profluo.ecm.model.db.DPIHeader;
import com.profluo.ecm.model.db.DatabaseConnectionManager;
import com.profluo.ecm.model.db.DefCompanies;
import com.profluo.ecm.model.db.DefSupplier;
import com.profluo.ecm.model.db.GenericDBStatements;
import com.profluo.ecm.model.db.InvoiceHeader;
import com.profluo.ecm.model.vo.DefIASVo;
import com.profluo.ecm.model.vo.DefIFRSVo;
import com.profluo.ecm.model.vo.DefProductVo;

/**
 * Portlet implementation class DocJustificativ
 */
public class DocJustificativ extends MVCPortlet {
	/**
	 * Generic log instance.
	 */
	private final Log log = LogFactoryUtil.getLog("PROFLUO-" + MVCPortlet.class.getName());
	
	
	public static List<String> dpis = new ArrayList<String>();
	/**
	 * Getting information about portlet initialization.
	 */
    @Override
    public void init() throws PortletException {
    	log.info("[PROFLUO] Initialize Invoice missing docs portlet.");
    	super.init();
    }
    
    /**
	 * Returns the index of column in the array of line columns
	 * 
	 * @param row
	 * @return
	 */
	public int getColumnIndex(String[][] row, String column) {
		for (int i = 0; i < row.length; i++) {
			if (row[i][0].equals(column)) {
				return i;
			}
		}
		return 0;
	}
	
	public void addInvoice(ActionRequest request, ActionResponse response) {
		Invoices inv = new Invoices();
		inv.addInvoice(request, response);
	}
    
    /**
     * Action that allows to make changes to missing docs field 
     * and also move to next stage in case all missing docs are found.
     * @param request
     * @param response
     */
    public void saveMissingDocs(ActionRequest request, ActionResponse response) {
    	// remove default error message from Liferay
    	PortletConfig portletConfig = (PortletConfig)request.getAttribute(JavaConstants.JAVAX_PORTLET_CONFIG);
    	LiferayPortletConfig liferayPortletConfig = (LiferayPortletConfig) portletConfig;
    	SessionMessages.add(request, liferayPortletConfig.getPortletId() + SessionMessages.KEY_SUFFIX_HIDE_DEFAULT_ERROR_MESSAGE);
		
		log.info("[PROFLUO] Documente justificative lipsa portlet - saveMissingDocs()");
		String missingDocs = "";
		if (request.getParameter("missing_docs") != null) { missingDocs = request.getParameter("missing_docs").toString(); }
		String invNo = "";
		if (request.getParameter("inv_number") != null) { invNo = request.getParameter("inv_number").toString(); }
		String invId = "";
		if (request.getParameter("inv_id") != null) { invId = request.getParameter("inv_id").toString(); }
		
		String userId = "";
		if (request.getParameter("userId") != null) { userId = request.getParameter("userId").toString(); }
		
		System.out.println("Missing Docs -> " + missingDocs + " -> " + invNo + " -> " + invId);
		
		// set values for DB
		int arrSize = 2;
		if (missingDocs.equals("")) {
			arrSize = 3;
		}
		String[][] arrFields = new String [arrSize][2];
        for (int i = 0; i < arrSize; i++) {
        	arrFields[i] = new String[2];
        }
        
        
        arrFields[0][0] = "missing_docs";
        arrFields[0][1] = missingDocs;
        
        Calendar calendar = Calendar.getInstance();
	    java.sql.Timestamp currentTS = new java.sql.Timestamp(calendar.getTime().getTime());  
        arrFields[1][0] = "modified";
        arrFields[1][1] = currentTS.toString();
        
        if (arrSize == 3) {
        	List<HashMap<String,Object>> invoiceHeader = InvoiceHeader.getOneInvoiceHeaderById(invId);
        	List<HashMap<String,Object>> allStoreLines = InvoiceHeader.getStoreLinesByIdWithoutProrata(invId, "invoice_store_line");
        	Invoices inv = new Invoices();
        	String [][] header = ControllerUtils.convertHashMapToStringMatrix(invoiceHeader.get(0));
        	String [][][] lineDataArticles = ControllerUtils.convertListOfHashMapToStringMatrix(allStoreLines);
        	
        	int index = getColumnIndex(header, "id_new_prj"); 
        	if (!header[index][1].equals("0")){
        		inv.setNewProject(true);
        	}
        	index = getColumnIndex(header, "id_initiative");
        	if (!header[index][1].equals("0")){
        		inv.setIsInitiative(true);
        	}
        	inv.setMissingDocs(false);
        	
        	HashMap<Integer, Integer> storeLineIds = new HashMap<Integer, Integer>();
			for(int j = 0 ; j < allStoreLines.size(); j++){
				storeLineIds.put(j, Integer.parseInt(allStoreLines.get(j).get("id").toString()));
			}
        	HashMap<String, HashMap<String, List<String>>> nextApprovers = inv.computeNextApprovers(header, lineDataArticles, storeLineIds);

        	inv.insertEntriesApproversTable(invoiceHeader.get(0).get("id").toString(), nextApprovers);
        	User user = null;
			try {
				user = UserServiceUtil.getUserById(Long.parseLong(request.getRemoteUser()));
			} catch (NumberFormatException | PortalException | SystemException e) {
				e.printStackTrace();
			}
        	inv.addNotificationForFirstUnapprovedLevel(6, invNo, user, DefSupplier.getSupplierNameById(invoiceHeader.get(0).get("id_supplier").toString()), invId, invoiceHeader.get(0).get("inv_date").toString(), DefCompanies.getCompanyNameById(invoiceHeader.get(0).get("id_company").toString()));
	        arrFields[2][0] = "stage";
	        arrFields[2][1] = "3";
        }
		
		//attrs for where statement
    	ArrayList<String> whereStatement = new ArrayList<String>();
    	//attrs values for where statement
    	ArrayList<String> whereStatementValues = new ArrayList<String>();
    	whereStatement.add("id");
    	whereStatementValues.add(invId);
    	// no excludes
    	ArrayList<String> excludeList = new ArrayList<String>();
		
		int affectedRows = GenericDBStatements.updateEntry(arrFields, "invoice_header", excludeList, whereStatement, whereStatementValues);
		if (affectedRows > 0) {
			if (arrSize == 1) {
				SessionMessages.add(request, "invoiceSaved");
			} else {
				SessionMessages.add(request, "invoiceSaved");
			}
			InvoiceHeader.setMissingDocUser(invId.toString(), userId);
		} else {
			SessionErrors.add(request, "invoiceNotSaved");
		}
    }
    
    /**
     * All ajax requests come in here
     */
    @SuppressWarnings("unchecked")
	@Override
    public void serveResource(ResourceRequest resourceRequest, ResourceResponse resourceResponse) throws IOException, PortletException {
    	log.info("[AJAX] Doc Justif");
    	
    	String action = ParamUtil.getString(resourceRequest, "action");
    	log.info("[AJAX] action: " + action);
    	// used to write the output answer
    	PrintWriter writer = resourceResponse.getWriter();
    	
    	if (action.equals("filter") || action.equals("next") || action.equals("prev")  || action.equals("last") || action.equals("first")) {
    		// ajax for pagination and filtering in listing
	    	int start = Integer.parseInt(ParamUtil.getString(resourceRequest, "start").toString());
	    	int count = Integer.parseInt(ParamUtil.getString(resourceRequest, "count").toString());
	    	int total = Integer.parseInt(ParamUtil.getString(resourceRequest, "total").toString());
	    	int companyId = Integer.parseInt(ParamUtil.getString(resourceRequest, "company_id").toString());
	    	String type = ParamUtil.getString(resourceRequest, "type").toString();
	    	String moneda = ParamUtil.getString(resourceRequest, "moneda");
	    	String number = ParamUtil.getString(resourceRequest, "number").replace(",", "").toString();
	    	String datafactura = ParamUtil.getString(resourceRequest, "datafactura");
	    	String datainreg = ParamUtil.getString(resourceRequest, "datainreg");
	    	String supplierName = ParamUtil.getString(resourceRequest, "supplier_name");
	    	String numeMagazin = ParamUtil.getString(resourceRequest, "nume_magazin");
	    	
	    	log.info("[AJAX] start: " + count);
	    	log.info("[AJAX] count: " + start);
	    	log.info("[AJAX] total: " + total);
	    	
	    	//resourceResponse.setProperty("test", "test_content");
	    	User user = null;
	    	try{
	    		user = UserServiceUtil.getUserById(Long.parseLong(resourceRequest.getRemoteUser()));
	    	}catch (Exception e){}
	    	
	    	
	    	List<HashMap<String,Object>> allInvoices = InvoiceHeader.getInvociesWithMissingDocs(start, count, companyId, moneda, number, datafactura, datainreg, supplierName, numeMagazin, (int)user.getUserId(), type);
	    	total = InvoiceHeader.getInvociesWithMissingDocsCount( companyId, moneda, number, datafactura, datainreg, supplierName, numeMagazin, (int)user.getUserId(), type);

	    	
	    	JSONArray jsonInvoices = new JSONArray();
	    	if (allInvoices != null) {
	    		jsonInvoices = DatabaseConnectionManager.convertListToJson(allInvoices);
	    	}
	    	JSONObject jsonResponse = new JSONObject();
			jsonResponse.put("action", action);
			jsonResponse.put("start", start);
			jsonResponse.put("count", count);
			jsonResponse.put("total", total);
			jsonResponse.put("values", jsonInvoices);
			writer.print(jsonResponse.toJSONString());
    	} else if (action.equals("getIasInfo")){
			String id = ParamUtil.getString(resourceRequest, "iasId").toString();
			List<HashMap<String, Object>> product = DefIASVo.getById(id);
			JSONArray jsonProduct = new JSONArray();
			if (product != null) {
				jsonProduct = DatabaseConnectionManager.convertListToJson(product);
			}
			JSONObject jsonResponse = new JSONObject();
			jsonResponse.put("product", jsonProduct);
			//System.out.println(jsonProduct);
			writer.print(jsonResponse.toJSONString());
		} else if (action.equals("getIfrsInfo")){
			String id = ParamUtil.getString(resourceRequest, "ifrsId").toString();
			List<HashMap<String, Object>> product = DefIFRSVo.getById(id);
			JSONArray jsonProduct = new JSONArray();
			if (product != null) {
				jsonProduct = DatabaseConnectionManager.convertListToJson(product);
			}
			JSONObject jsonResponse = new JSONObject();
			jsonResponse.put("product", jsonProduct);
			//System.out.println(jsonProduct);
			writer.print(jsonResponse.toJSONString());
		} else if (action.equals("showDpi")) {
			String idDpi = ParamUtil.getString(resourceRequest, "dpi");
			// get DPI header info based on DPI id
			List<HashMap<String, Object>> oneDpi = null;

			// oneDpi = GenericDBStatements.getAllByFieldName("dpi_header",  "id", idDpi);
			oneDpi = DPIHeader.getAllByFieldName("id", idDpi);

			JSONArray jsonDpiHeader = new JSONArray();
			if (oneDpi != null) {
				jsonDpiHeader = DatabaseConnectionManager.convertListToJson(oneDpi);
			}

			// get DPI lines info based on DPI id
			List<HashMap<String, Object>> dpiLines = null;

			dpiLines = GenericDBStatements.getAllByFieldName("dpi_line", "id_dpi", idDpi);

			JSONArray jsonDpiLines = new JSONArray();
			if (dpiLines != null) {
				jsonDpiLines = DatabaseConnectionManager.convertListToJson(dpiLines);
			}

			// create JSON response
			JSONObject jsonResponse = new JSONObject();
			jsonResponse.put("header", jsonDpiHeader);
			jsonResponse.put("lines", jsonDpiLines);
			// send AJAX response
			writer.print(jsonResponse.toJSONString());
		} else if (action.equals("getProdInfo")) {
			// ajax to get product info for one row based on id of the selected
			// product
			String id = ParamUtil.getString(resourceRequest, "prodId").toString();
			List<HashMap<String, Object>> product = DefProductVo.getById(id);
			JSONArray jsonProduct = new JSONArray();
			if (product != null) {
				jsonProduct = DatabaseConnectionManager.convertListToJson(product);
			}
			JSONObject jsonResponse = new JSONObject();
			jsonResponse.put("product", jsonProduct);
			//System.out.println(jsonProduct);
			writer.print(jsonResponse.toJSONString());
		} else if (action.equals("getStoreLinesByInvoiceId")){
    		int invoice_id = Integer.parseInt(ParamUtil.getString(resourceRequest, "invoice_id").toString());
    		List<HashMap<String,Object>> allStoreLines = null;
    		allStoreLines = InvoiceHeader.getMissingDocsAssociatedStoreLines(invoice_id);
    		JSONArray storeLines = new JSONArray();
	    	if (allStoreLines != null) {
	    		storeLines = DatabaseConnectionManager.convertListToJson(allStoreLines);
	    	}
	    	JSONObject jsonResponse = new JSONObject();
	    	jsonResponse.put("values", storeLines);
	    	writer.print(jsonResponse.toJSONString());
    	} else if (action.equals("updateDpiOnStoreLine")){
    		String store_line_id = ParamUtil.getString(resourceRequest, "store_line_id").toString();
    		String id_dpi = ParamUtil.getString(resourceRequest, "id_dpi").toString();
    		String[][] arrFields = new String[1][2];
    		arrFields[0][0] = "id_dpi";
    		arrFields[0][1] = id_dpi;
    		ArrayList<String> columns = new ArrayList<String>();
    		ArrayList<String> values = new ArrayList<String>();
    		columns.add("id");
    		values.add(store_line_id);
    		int result = -1;
    		try{
    			result = GenericDBStatements.updateEntry(arrFields, "invoice_store_line", new ArrayList<String>(), columns, values);
    		}catch (Exception e){e.printStackTrace();}
    		writer.print(String.valueOf(result));
    	} else if (action.equals("getInventoryDetails")){
			String idStoreLine = ParamUtil.getString(resourceRequest, "idStoreLine").toString();
			List<HashMap<String,Object>> inventoryDetails = InvoiceHeader.getInventoryDetailsByIdStoreLine(idStoreLine);
			JSONArray jsonInvNo = new JSONArray();
			if (inventoryDetails != null) {
				for (int i = 0; i < inventoryDetails.size(); i++) {
					JSONObject jsonLine = new JSONObject();
					jsonLine.put("nr_inv", inventoryDetails.get(i).get("inventory_no").toString());
					jsonLine.put("data_pif", inventoryDetails.get(i).get("pif_date").toString());
					jsonLine.put("receptie", inventoryDetails.get(i).get("reception").toString());
					jsonInvNo.add(jsonLine);
				}
				writer.print(jsonInvNo.toJSONString());
			} else {
				writer.print("");
			}
			
		}
    }
    
    /**
     * 
     */
	@Override
	public void render(RenderRequest renderRequest, RenderResponse renderResponse) throws PortletException, IOException {
		//HttpServletRequest request = PortalUtil.getHttpServletRequest(renderRequest);
    	//String p_p_id = PortalUtil.getOriginalServletRequest(request).getParameter("p_p_id");
    	//String path = PortalUtil.getOriginalServletRequest(request).getParameter("_" + p_p_id + "_mvcPath");
    	
    	try {
	    	User user = UserServiceUtil.getUserById(Long.parseLong(renderRequest.getRemoteUser()));
	    	
	        ThemeDisplay td  = (ThemeDisplay) renderRequest.getAttribute(WebKeys.THEME_DISPLAY);
	        log.info("[PROFLUO] Invoices doc justif - render()");
	        log.info("[PROFLUO] User full name: " + td.getUser().getFullName());
	    	log.info("[PROFLUO] User id " + user.getUserId());
	    	
	    	List<Integer> types 		= InvoiceStages.getSqlStatementType(user, td.getURLCurrent());
			String stage 	= InvoiceStages.CREATED_WITH_MISSING_DOCS;
			
			String allTypes = "";
			for(Integer type : types ){
				allTypes = allTypes + type + ",";
			}
			if(allTypes.endsWith(",")){
				allTypes = allTypes.substring(0, allTypes.length()-1);
			}
			
			String page 	= InvoiceStages.getPageLayout(td.getURLCurrent());
			renderRequest.setAttribute("page-layout", page);
			renderRequest.setAttribute("page-status", stage);
			renderRequest.setAttribute("type", allTypes);
	    	
    	} catch (Exception e) {
    		e.printStackTrace();
    	}
	
    	super.render(renderRequest, renderResponse);
	}

}
