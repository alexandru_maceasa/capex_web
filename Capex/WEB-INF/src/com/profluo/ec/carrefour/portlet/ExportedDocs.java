package com.profluo.ec.carrefour.portlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletException;
import javax.portlet.PortletPreferences;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;
import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.model.User;
import com.liferay.portal.service.UserServiceUtil;
import com.liferay.portal.theme.ThemeDisplay;
import com.liferay.portal.util.PortalUtil;
import com.liferay.portlet.PortletPreferencesFactoryUtil;
import com.liferay.util.bridges.mvc.MVCPortlet;
import com.profluo.ecm.model.db.CashExpense;
import com.profluo.ecm.model.db.DPIHeader;
import com.profluo.ecm.model.db.DatabaseConnectionManager;
import com.profluo.ecm.model.db.GenericDBStatements;
import com.profluo.ecm.model.db.InvoiceHeader;
import com.profluo.ecm.model.vo.DefIASVo;
import com.profluo.ecm.model.vo.DefIFRSVo;
import com.profluo.ecm.model.vo.DefProductVo;

public class ExportedDocs extends MVCPortlet {
	/**
	 * Generic log instance.
	 */
	private final Log log = LogFactoryUtil.getLog("PROFLUO-" + Invoices.class.getName());
	
	/**
	 * Default AJAX method in the application framework.
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void serveResource(ResourceRequest resourceRequest, ResourceResponse resourceResponse) throws IOException, PortletException {
		log.info("[AJAX] Exported Docs Controller");
		
//		User user = null;
//    	try{
//    		user = UserServiceUtil.getUserById(Long.parseLong(resourceRequest.getRemoteUser()));
//    	}catch (Exception e) {}
    	
		String action = ParamUtil.getString(resourceRequest, "action");
		log.info("[AJAX] action: " + action);
		PrintWriter writer = resourceResponse.getWriter();
		if (action.equals("filter") || action.equals("next") || action.equals("prev") || action.equals("last") || action.equals("first")) {
			// read AJAX parameters used in filtering and pagination
			int start = Integer.parseInt(ParamUtil.getString(resourceRequest, "start").toString());
			int count = Integer.parseInt(ParamUtil.getString(resourceRequest, "count").toString());
			int total = Integer.parseInt(ParamUtil.getString(resourceRequest, "total").toString());
			int companyId = Integer.parseInt(ParamUtil.getString(resourceRequest, "company_id").toString());
			String moneda = ParamUtil.getString(resourceRequest, "moneda");
			String number = ParamUtil.getString(resourceRequest, "number").toString();
			String datafactura = ParamUtil.getString(resourceRequest, "datafactura");
			String datainreg = ParamUtil.getString(resourceRequest, "datainreg");
			String supplierCode = ParamUtil.getString(resourceRequest, "supplier_code");
			String supplierName = ParamUtil.getString(resourceRequest, "supplier_name");
			String docType = ParamUtil.getString(resourceRequest, "doc_type");
			boolean isControlorGestiune =  ParamUtil.getBoolean(resourceRequest, "isControlorGestiune");
			String idUser = ParamUtil.getString(resourceRequest, "idUser");
			
			System.out.println("controlor gestiune in controler:" +isControlorGestiune);
			
			List<HashMap<String, Object>> allInvoices = new ArrayList<HashMap<String,Object>>();
			
			allInvoices = InvoiceHeader.getAllFilteredExportedDocs(start, count, companyId, moneda, number, datafactura, datainreg, supplierCode, supplierName, docType, isControlorGestiune,idUser);
			//TODO : COUNT
			total = InvoiceHeader.getAllFilteredCountExportedDocs(companyId, moneda, number, datafactura, datainreg, supplierCode, supplierName, docType, isControlorGestiune,idUser);
			
			
			
			JSONArray jsonInvoices = new JSONArray();
			if (allInvoices != null) {
				jsonInvoices = DatabaseConnectionManager.convertListToJson(allInvoices);
			}
			JSONObject jsonResponse = new JSONObject();

			jsonResponse.put("action", action);
			jsonResponse.put("start", start);
			jsonResponse.put("count", count);
			jsonResponse.put("total", total);
			jsonResponse.put("values", jsonInvoices);
			writer.print(jsonResponse.toJSONString());
		} else if (action.equals("getProdInfo")) {
			String id = ParamUtil.getString(resourceRequest, "prodId").toString();
			List<HashMap<String, Object>> product = DefProductVo.getById(id);
			JSONArray jsonProduct = new JSONArray();
			if (product != null) {
				jsonProduct = DatabaseConnectionManager.convertListToJson(product);
			}
			JSONObject jsonResponse = new JSONObject();
			jsonResponse.put("product", jsonProduct);
			writer.print(jsonResponse.toJSONString());
		}  else if (action.equals("showDpi")) {
			String idDpi = ParamUtil.getString(resourceRequest, "dpi");
			// get DPI header info based on DPI id
			List<HashMap<String, Object>> oneDpi = null;

			// oneDpi = GenericDBStatements.getAllByFieldName("dpi_header",  "id", idDpi);
			oneDpi = DPIHeader.getAllByFieldName("id", idDpi);

			JSONArray jsonDpiHeader = new JSONArray();
			if (oneDpi != null) {
				jsonDpiHeader = DatabaseConnectionManager.convertListToJson(oneDpi);
			}

			// get DPI lines info based on DPI id
			List<HashMap<String, Object>> dpiLines = null;

			dpiLines = GenericDBStatements.getAllByFieldName("dpi_line", "id_dpi", idDpi);

			JSONArray jsonDpiLines = new JSONArray();
			if (dpiLines != null) {
				jsonDpiLines = DatabaseConnectionManager.convertListToJson(dpiLines);
			}

			// create JSON response
			JSONObject jsonResponse = new JSONObject();
			jsonResponse.put("header", jsonDpiHeader);
			jsonResponse.put("lines", jsonDpiLines);
			// send AJAX response
			writer.print(jsonResponse.toJSONString());
			
		} else if (action.equals("getIasInfo")){
			String id = ParamUtil.getString(resourceRequest, "iasId").toString();
			List<HashMap<String, Object>> product = DefIASVo.getById(id);
			JSONArray jsonProduct = new JSONArray();
			if (product != null) {
				jsonProduct = DatabaseConnectionManager.convertListToJson(product);
			}
			JSONObject jsonResponse = new JSONObject();
			jsonResponse.put("product", jsonProduct);
			//System.out.println(jsonProduct);
			writer.print(jsonResponse.toJSONString());
		} else if (action.equals("getIfrsInfo")){
			String id = ParamUtil.getString(resourceRequest, "ifrsId").toString();
			List<HashMap<String, Object>> product = DefIFRSVo.getById(id);
			JSONArray jsonProduct = new JSONArray();
			if (product != null) {
				jsonProduct = DatabaseConnectionManager.convertListToJson(product);
			}
			JSONObject jsonResponse = new JSONObject();
			jsonResponse.put("product", jsonProduct);
			//System.out.println(jsonProduct);
			writer.print(jsonResponse.toJSONString());
		} else if (action.equals("getInventoryDetails")){
			String idStoreLine = ParamUtil.getString(resourceRequest, "idStoreLine").toString();
			List<HashMap<String,Object>> inventoryDetails = InvoiceHeader.getInventoryDetailsByIdStoreLine(idStoreLine);
			JSONArray jsonInvNo = new JSONArray();
			if (inventoryDetails != null) {
				for (int i = 0; i < inventoryDetails.size(); i++) {
					JSONObject jsonLine = new JSONObject();
					jsonLine.put("nr_inv", inventoryDetails.get(i).get("inventory_no").toString());
					jsonLine.put("data_pif", inventoryDetails.get(i).get("pif_date").toString());
					jsonLine.put("receptie", inventoryDetails.get(i).get("reception").toString());
					jsonInvNo.add(jsonLine);
				}
				writer.print(jsonInvNo.toJSONString());
			} else {
				writer.print("");
			}
		} else if (action.equals("getInventoryDetailsCash")){
			String idStoreLine = ParamUtil.getString(resourceRequest, "idCashexp").toString();
			System.out.println(idStoreLine);
			List<HashMap<String,Object>> inventoryDetails = CashExpense.getInventoryDetailsByIdStoreLine(idStoreLine);
			JSONArray jsonInvNo = new JSONArray();
			if (inventoryDetails != null) {
				for (int i = 0; i < inventoryDetails.size(); i++) {
					JSONObject jsonLine = new JSONObject();
					jsonLine.put("nr_inv", inventoryDetails.get(i).get("inventory_no").toString());
					jsonLine.put("data_pif", inventoryDetails.get(i).get("pif_date").toString());
					jsonLine.put("receptie", inventoryDetails.get(i).get("reception").toString());
					jsonInvNo.add(jsonLine);
				}
				writer.print(jsonInvNo.toJSONString());
			} else {
				writer.print("");
			}
			
		}
	}
	
	public void addInvoice(ActionRequest request, ActionResponse response) {
		Invoices inv = new Invoices();
		inv.addInvoice(request, response);
	}
	
	/**
	 * 
	 */
	@Override
	public void render(RenderRequest renderRequest, RenderResponse renderResponse) throws PortletException, IOException {
		HttpServletRequest request = PortalUtil.getHttpServletRequest(renderRequest);
		String p_p_id = PortalUtil.getOriginalServletRequest(request).getParameter("p_p_id");
		String path = PortalUtil.getOriginalServletRequest(request).getParameter("_" + p_p_id + "_mvcPath");
		try {
			ThemeDisplay themeDisplay = ((ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY));
			String portletId = themeDisplay.getPortletDisplay().getId();
			PortletPreferences portletSetup = PortletPreferencesFactoryUtil.getLayoutPortletSetup(themeDisplay.getLayout(), portletId);
			String portletCustomTitle = themeDisplay.getPortletDisplay().getTitle();
			portletCustomTitle = portletSetup.getValue("portletSetupTitle_" + themeDisplay.getLanguageId(), portletCustomTitle);
			User user = UserServiceUtil.getUserById(Long.parseLong(renderRequest.getRemoteUser()));

			ThemeDisplay td = (ThemeDisplay) renderRequest.getAttribute(WebKeys.THEME_DISPLAY);
			log.info("[PROFLUO] Invoices portlet - render()");
			log.info("[PROFLUO] User full name: " + td.getUser().getFullName());
			log.info("[PROFLUO] Portlet Title: " + portletCustomTitle);
			
			int id_user = (int) user.getUserId();
			log.info("[PROFLUO] User id " + id_user);
			log.info("[PROFLUO] Group id " + user.getGroupId());
			log.info("[PROFLUO] Group id " + user.getRoles());


			if (path == null || path.equals("view.jsp")) {
				log.info("[PROFLUO] EXPORTED DOCS render()");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		super.render(renderRequest, renderResponse);
	}
}
