package com.profluo.ec.carrefour.portlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.List;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.messaging.Message;
import com.liferay.portal.kernel.messaging.MessageListener;
import com.liferay.portal.kernel.messaging.MessageListenerException;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.util.bridges.mvc.MVCPortlet;
import com.profluo.ecm.model.db.DatabaseConnectionManager;
import com.profluo.ecm.model.db.DefCompanies;
import com.profluo.ecm.model.db.Raports;
import com.profluo.ecm.model.vo.DefNewProjVo;
import com.profluo.ecm.model.vo.DefStoreVo;
import com.profluo.ecm.model.vo.ListaInitVo;
import com.profluo.ecm.utils.ExcelUtil;

/**
 * Portlet implementation class RealizatLunarVsBugetat
 */
public class RealizatLunarVsBugetat extends MVCPortlet implements MessageListener  {
	
	private final Log log = LogFactoryUtil.getLog("PROFLUO-" + RealizatLunarVsBugetat.class.getName());
	
	@Override
	public void receive(Message message) throws MessageListenerException {
		log.info("[PROFLUO] RealizatLunarVsBugetat portlet - AUTOMATIC UPDATE TABLE");
		insertAllInformations();
		insertTotalInfo();
	}
	
	@SuppressWarnings("unchecked")
	public void serveResource(ResourceRequest resourceRequest, ResourceResponse resourceResponse) throws IOException, PortletException {
		
		String action = ParamUtil.getString(resourceRequest, "action");
		if (action.equals("filter") || action.equals("next") || action.equals("prev") || action.equals("last") || action.equals("first")) {
			PrintWriter writer = resourceResponse.getWriter();
			int start = Integer.parseInt(ParamUtil.getString(resourceRequest, "start").toString());
			int count = Integer.parseInt(ParamUtil.getString(resourceRequest, "count").toString());
			int total = Integer.parseInt(ParamUtil.getString(resourceRequest, "total").toString());
			List<HashMap<String, Object>> allLines = null;
			allLines = Raports.getRaport(start, count, 1);
			allLines.addAll(Raports.getTableFooter(true, 1));
			total = Raports.getRaportTotal();
			JSONArray jsonLines = new JSONArray();
			if (allLines != null) {
				jsonLines = DatabaseConnectionManager.convertListToJson(allLines);
			}
			JSONObject jsonResponse = new JSONObject();
			jsonResponse.put("action", action);
			jsonResponse.put("start", start);
			jsonResponse.put("count", count);
			jsonResponse.put("total", total);
			jsonResponse.put("values", jsonLines);
			writer.print(jsonResponse.toJSONString());
		} else if(action.equals("exportExcel")) {
			
			List<HashMap<String,Object>> list = Raports.getRaport(0, 50000000, 1);
			list.addAll(Raports.getTableFooter(false, 1));
			
			String firstHeader [][] = new String [15][2];
			firstHeader[0][0] = "code";
			firstHeader[0][1] = "Cod";
			firstHeader[1][0] = "denumire";
			firstHeader[1][1] = "Denumire";
			firstHeader[2][0] = "buget_an";
			firstHeader[2][1] = "Buget An";
			firstHeader[3][0] = "actual_ian";
			firstHeader[3][1] = "Actual Ianuarie";
			firstHeader[4][0] = "actual_feb";
			firstHeader[4][1] = "Actual Februarie";
			firstHeader[5][0] = "actual_mar";
			firstHeader[5][1] = "Actual Martie";
			firstHeader[5][0] = "actual_apr";
			firstHeader[5][1] = "Actual Aprilie";
			firstHeader[6][0] = "actual_mai";
			firstHeader[6][1] = "Actual Mai";
			firstHeader[7][0] = "actual_iun";
			firstHeader[7][1] = "Actual Iunie";
			firstHeader[8][0] = "actual_iul";
			firstHeader[8][1] = "Actual Iulie";
			firstHeader[9][0] = "actual_aug";
			firstHeader[9][1] = "Actual August";
			firstHeader[10][0] = "actual_sept";
			firstHeader[10][1] = "Actual Septembrie";
			firstHeader[11][0] = "actual_oct";
			firstHeader[11][1] = "Actual Octombrie";
			firstHeader[12][0] = "actual_noi";
			firstHeader[12][1] = "Actual Noiembrie";
			firstHeader[13][0] = "actual_dec";
			firstHeader[13][1] = "Actual Decembrie";
			firstHeader[14][0] = "total";
			firstHeader[14][1] = "Total";
			firstHeader[14][0] = "diferenta";
			firstHeader[14][1] = "Actual vs Bugetat";
			
			ExcelUtil.getTwoSheetExcel(list, list, firstHeader, firstHeader, "Raport realizatLunar vs Bugetat", resourceResponse);
		}
		
	}
	
	public void updateInfo(ActionRequest actionRequest,	ActionResponse actionResponse) {
		log.info("[PROFLUO] RealizatLunarVsBugetat portlet - updateInfo()");
		insertAllInformations();
		insertTotalInfo();
	}
	
	protected static void insertAllInformations(){
		List<HashMap<String,Object>> allCompanies = DefCompanies.getAll();
		List<HashMap<String,Object>> allStores = DefStoreVo.getInstance();
		List<HashMap<String,Object>> newProjects = DefNewProjVo.getInstance();
		List<HashMap<String,Object>> initiatives = ListaInitVo.getInstance();
		
		//Remove old informations
		Raports.truncateTable("template_rapoarte");
			
		List<HashMap<String,Object>> initiativeTotal = Raports.getMonthSums(2016, 0, 1, 0, 0, 0, 0);
		if(initiativeTotal.size() > 0 ){
			//Adding initiatives header
			Raports.insertRaport("1", "Initiative centralizate", 1, 0, 1, initiativeTotal, Raports.getBugetForInitiatives(2016, 0, 0, 0, 0, 0));
			for(HashMap<String,Object> company : allCompanies) {
				List<HashMap<String,Object>> companyTotal = Raports.getMonthSums(2016, Integer.parseInt(company.get("id").toString()), 1, 0, 0, 0, 0);
				if(companyTotal.size() > 0 ) {
					//Adding companies header
					Raports.insertRaport(company.get("entity_id").toString(), company.get("name").toString(), 1, 0, 1, companyTotal, Raports.getBugetForInitiatives(2016, Integer.parseInt(company.get("id").toString()), 0, 0, 0, 0));
					for(HashMap<String,Object> initiativa : initiatives) {
						List<HashMap<String,Object>> oneInitTotal = Raports.getMonthSums(2016, Integer.parseInt(company.get("id").toString()), 1, Integer.parseInt(initiativa.get("id").toString()), 0, 0, 0);
						if(oneInitTotal.size() > 0 ) {
							//Adding individual initiative
							Raports.insertRaport(initiativa.get("code").toString(), initiativa.get("name").toString(), 1, Integer.parseInt(initiativa.get("id").toString()), 1, oneInitTotal, Raports.getBugetForInitiatives(2016, Integer.parseInt(company.get("id").toString()), Integer.parseInt(initiativa.get("id").toString()), 0, 0, 0));
							for(HashMap<String, Object> store : allStores) {
								List<HashMap<String,Object>> storeTotal = Raports.getMonthSums(2016, Integer.parseInt(company.get("id").toString()), 1, Integer.parseInt(initiativa.get("id").toString()), Integer.parseInt(store.get("id").toString()), 0, 0);
								if(storeTotal.size() > 0 ) {
									//adding individual store
									Raports.insertRaport(store.get("store_id").toString(), store.get("store_name").toString(), 1, Integer.parseInt(store.get("id").toString()), 1, storeTotal, Raports.getBugetForInitiatives(2016, Integer.parseInt(company.get("id").toString()), Integer.parseInt(initiativa.get("id").toString()), Integer.parseInt(store.get("id").toString()), 0, 0));
								}
							}
						}
						
					}
				}
			}
		}
		
		List<HashMap<String,Object>> newPrjTotal = Raports.getMonthSums(2016, 0, 2, 0, 0, 0, 0);
		if(newPrjTotal.size() > 0 ) {
			//Adding new projects header
			Raports.insertRaport("2", "Proiecte noi", 2, 0, 1, newPrjTotal, Raports.getBugetForNewPrj(2016, 0, 0, 0, 0));
			for(HashMap<String, Object> company : allCompanies) {
				List<HashMap<String,Object>> companyTotal = Raports.getMonthSums(2016, Integer.parseInt(company.get("id").toString()), 2, 0, 0, 0, 0);
				if(companyTotal.size() > 0 ) {
					//Adding companies header
					Raports.insertRaport(company.get("entity_id").toString(), company.get("name").toString(), 2, 0, 1, companyTotal, Raports.getBugetForNewPrj(2016, Integer.parseInt(company.get("id").toString()), 0, 0, 0));
					for(HashMap<String,Object> newPrj : newProjects) {
						List<HashMap<String,Object>> oneProject = Raports.getMonthSums(2016, Integer.parseInt(company.get("id").toString()), 2, Integer.parseInt(newPrj.get("id").toString()), 0, 0, 0);
						if(oneProject.size() > 0 ) {
							//Adding individual new project
							Raports.insertRaport(newPrj.get("store_id").toString(), newPrj.get("name").toString(), 2, Integer.parseInt(newPrj.get("id").toString()), 1, oneProject, Raports.getBugetForNewPrj(2016, Integer.parseInt(company.get("id").toString()),  Integer.parseInt(newPrj.get("id").toString()), 0, 0));
						}
					}
				}
			}
		}
		
		List<HashMap<String,Object>> storeTotal = Raports.getMonthSums(2016, 0, 0, 0, 0, 0, 0);
		if(storeTotal.size() > 0 ) {
			//Adding capex diverse header
			Raports.insertRaport("3", "Capex diverse", 0, 0, 1, storeTotal, Raports.getBugetForCapexDiverse(2016, 0, 0, 0, 0));
			for(HashMap<String, Object> company : allCompanies) {
				List<HashMap<String,Object>> companyTotal = Raports.getMonthSums(2016, Integer.parseInt(company.get("id").toString()), 0, 0, 0, 0, 0);
				if(companyTotal.size() > 0 ) {
					//Adding companies header
					Raports.insertRaport(company.get("entity_id").toString(), company.get("name").toString(), 0, 0, 1, companyTotal, Raports.getBugetForCapexDiverse(2016, Integer.parseInt(company.get("id").toString()), 0, 0, 0));
					for(HashMap<String,Object> store : allStores) {
						List<HashMap<String,Object>> oneStoreTotal = Raports.getMonthSums(2016, Integer.parseInt(company.get("id").toString()), 0, Integer.parseInt(store.get("id").toString()), 0, 0, 0);
						if(oneStoreTotal.size() > 0 ) {
							//Adding individual store
							Raports.insertRaport(store.get("store_id").toString(), store.get("store_name").toString(), 0, Integer.parseInt(store.get("id").toString()), 1, oneStoreTotal, Raports.getBugetForCapexDiverse(2016, Integer.parseInt(company.get("id").toString()), Integer.parseInt(store.get("id").toString()), 0, 0));
						}
					}
				}
			}
		}
	}
	
	protected static void insertTotalInfo(){
		//Remove old informations
		Raports.truncateTable("realizat_bugetat_total");
		
		List<HashMap<String,Object>> totalIt = Raports.getMonthSumsForTotal(2016, true, false);
		if(totalIt.size() > 0) {
			Raports.insertRaportTotal("IT", totalIt, Raports.getItTotal(2016));
		}
		List<HashMap<String,Object>> totalProperty = Raports.getMonthSumsForTotal(2016, false, true);
		if(totalProperty.size() > 0) {
			Raports.insertRaportTotal("PROPERTY", totalProperty, Raports.getPropertyTotal(2016));
		}
		Raports.insertActiveTotal("template_rapoarte");
	}
	
	@Override
	public void render(RenderRequest renderRequest, RenderResponse renderResponse) throws PortletException, IOException {
		log.info("[PROFLUO] RealizatLunarVsBugetat portlet - render()");
		super.render(renderRequest, renderResponse);
	}
}
