package com.profluo.ec.carrefour.portlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletException;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.model.User;
import com.liferay.portal.service.UserServiceUtil;
import com.liferay.util.bridges.mvc.MVCPortlet;
import com.profluo.ecm.controller.ControllerUtils;
import com.profluo.ecm.model.db.DPIHeader;
import com.profluo.ecm.model.db.DatabaseConnectionManager;
import com.profluo.ecm.model.db.GenericDBStatements;
import com.profluo.ecm.model.db.InvoiceHeader;
import com.profluo.ecm.utils.ExcelUtil;

/**
 * Portlet implementation class RaportDpi
 */
public class RaportDpi extends MVCPortlet {
 
	@SuppressWarnings("unchecked")
	public void serveResource(ResourceRequest resourceRequest, ResourceResponse resourceResponse) throws IOException, PortletException {
		
		String action = ParamUtil.getString(resourceRequest, "action");
		if(action.equals("deleteLine")) {
			PrintWriter writer = resourceResponse.getWriter();
			int lineId = Integer.parseInt(ParamUtil.getString(resourceRequest, "lineId").toString());
			DPIHeader.deleteLine(lineId);
			JSONObject jsonResponse = new JSONObject();
			jsonResponse.put("succes", true);
			writer.print(jsonResponse.toJSONString());
		}
		if (action.equals("next") || action.equals("prev") || action.equals("last") || action.equals("first") || action.equals("filter")) {
			PrintWriter writer = resourceResponse.getWriter();
			// read AJAX parameters used in filtering and pagination
			int start = Integer.parseInt(ParamUtil.getString(resourceRequest, "start").toString());
			int count = Integer.parseInt(ParamUtil.getString(resourceRequest, "count").toString());
			int total = Integer.parseInt(ParamUtil.getString(resourceRequest, "total").toString());
			String idDpi = ParamUtil.getString(resourceRequest, "idDpi").toString();
			String colName = ParamUtil.getString(resourceRequest, "col_name").toString();
			
			List<HashMap<String, Object>> allDpis = DPIHeader.getAllActiveDpis(start, count, idDpi, colName);
			total = DPIHeader.getAllActiveDpisCount(idDpi);
			
			JSONArray jsonInvoices = new JSONArray();
			if (allDpis != null) {
				jsonInvoices = DatabaseConnectionManager.convertListToJson(allDpis);
			}
			JSONObject jsonResponse = new JSONObject();

			jsonResponse.put("action", action);
			jsonResponse.put("start", start);
			jsonResponse.put("count", count);
			jsonResponse.put("total", total);
			jsonResponse.put("values", jsonInvoices);
			writer.print(jsonResponse.toJSONString());
		} else if (action.endsWith("exportExcel")) {
			
			List<HashMap<String,Object>> allDpis = DPIHeader.getAllActiveDpis(0, 500000, "", "");
			
			String[][] header = new String[10][2];
			
			header[0][0] = "societate";
			header[0][1] = "Societate";
			header[1][0] = "magazin";
			header[1][1] = "Magazin";
			header[2][0] = "id_dpi";
			header[2][1] = "ID Dpi";
			header[3][0] = "data_dpi";
			header[3][1] = "Data DPI";
			header[4][0] = "valoare_dpi";
			header[4][1] = "Valoare DPI";
			header[5][0] = "suma_facturi";
			header[5][1] = "Valoare fara TVA facturi";
			header[6][0] = "diferenta";
			header[6][1] = "Diferenta";
			header[7][0] = "facturi";
			header[7][1] = "Facturi asociate";
			header[8][0] = "furnizori";
			header[8][1] = "Furnizori";
			header[9][0] = "status";
			header[9][1] = "Status";
			ExcelUtil.getExcel(allDpis, header, "Raport_DPI", resourceResponse);
		}
		if(action.equals("getInvoiceTotal")) {
			PrintWriter writer = resourceResponse.getWriter();
			String invoiceId = ParamUtil.getString(resourceRequest, "invoiceId").toString();
			List<HashMap<String,Object>> invDetail = InvoiceHeader.getHeaderById(invoiceId, "invoice_header");
			Object invoiceTotal = null;
			if(invDetail.size() > 0) {
				invoiceTotal = invDetail.get(0).get("total_no_vat_curr");
			}
			JSONObject jsonResponse = new JSONObject();
			jsonResponse.put("valoareFactura", invoiceTotal);
			writer.print(jsonResponse.toJSONString());
		}
	}
	
	public void saveDpi(ActionRequest request, ActionResponse response) {
		String[][][] lineData = null;
		String dataTableData = request.getParameter("liniiFacturi");
		lineData = ControllerUtils.convertToArrayMatrixFromJSON(dataTableData);
		String id = request.getParameter("id");
		String status = request.getParameter("status");
		if(status.equals("1")) {
			status = "A";
		} else {
			status = "I";
		}
		
		User user = null;
    	try{
    		user = UserServiceUtil.getUserById(Long.parseLong(request.getRemoteUser()));
    	}catch (Exception e) {}
		
    	DPIHeader.updateDPI(id, user.getUserId(), status);
    	
		//pentru fiecare linie
		for(int i = 0 ; i < lineData.length; i++) {
			int lineId = 0;
			String[][] fields = new String[5][2];
			//pentru fiecare camp din table
			for(int j = 0 ; j < lineData[i].length; j++ ) {
				fields[0][0] = "idFactura";
				fields[1][0] = "nrFactura";
				fields[2][0] = "valoareFactura";
				fields[3][0] = "idUser";
				fields[3][1] = String.valueOf(user.getUserId());
				fields[4][0] = "idDpi";
				fields[4][1] = id;
				//salvare id
				if(lineData[i][j][0].equals("id")) {
					if(!lineData[i][j][1].equals("")) {
						lineId = Integer.parseInt(lineData[i][j][1]);
					}
				}
				//salvare factura din sistem
				if(lineData[i][j][0].equals("idFactura")) {
					fields[0][1] = lineData[i][j][1];
				}
				//salvare factura noua
				if(lineData[i][j][0].equals("nrFactura")) {
					fields[1][1] = lineData[i][j][1];
				}
				//salvare valoare factura noua
				if(lineData[i][j][0].equals("valoareFactura")) {
					fields[2][1] = lineData[i][j][1];
				}
			}
			//daca e linie existenta se face insert
			if(lineId == 0 ) {
				GenericDBStatements.insertEntry(fields, "dpi_header_factura", new ArrayList<String>());
			//altfel se face update
			} else {
				DPIHeader.updateDpiInvoice(lineId, fields[0][1], fields[1][1], fields[2][1]);
			}
		}
		
	}

}
