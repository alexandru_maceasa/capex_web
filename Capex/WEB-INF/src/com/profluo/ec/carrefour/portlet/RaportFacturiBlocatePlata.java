package com.profluo.ec.carrefour.portlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.List;

import javax.portlet.PortletException;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.util.bridges.mvc.MVCPortlet;
import com.profluo.ecm.controller.ControllerUtils;
import com.profluo.ecm.model.db.DatabaseConnectionManager;
import com.profluo.ecm.model.db.DefSupplier;
import com.profluo.ecm.model.db.FacturiBlocatePlata;
import com.profluo.ecm.utils.ExcelUtil;

/**
 * Portlet implementation class RaportFacturiBlocatePlata
 */
public class RaportFacturiBlocatePlata extends MVCPortlet {
	
	@SuppressWarnings("unchecked")
	public void serveResource(ResourceRequest resourceRequest, ResourceResponse resourceResponse) throws IOException, PortletException {
		
		String action = ParamUtil.getString(resourceRequest, "action");
		if (action.equals("exportRaport")) {
		
			int societate = ParamUtil.getInteger(resourceRequest, "societate");
			int furnizor = ParamUtil.getInteger(resourceRequest, "furnizor");
			int motiv = ParamUtil.getInteger(resourceRequest, "motiv");
			int start_date_an = ParamUtil.getInteger(resourceRequest, "start_date_an");
			int start_date_luna = ParamUtil.getInteger(resourceRequest, "start_date_luna");
			int end_date_an = ParamUtil.getInteger(resourceRequest, "end_date_an");
			int end_date_luna = ParamUtil.getInteger(resourceRequest, "end_date_luna");
			//String exportType = ParamUtil.getString(resourceRequest, "export_type");
			String finalDate = "";
			if (end_date_an != 0 && end_date_luna != 0){
				finalDate = String.valueOf(end_date_an) + "-"+ String.valueOf(end_date_luna) + "-01";
				finalDate = FacturiBlocatePlata.getLastDayOfMonth(finalDate);
				//System.out.println("data dupa conversie:"+finalDate);
			}
			//incarca lista cu toate documentele corespunzatoare societatii introduse
			List<HashMap<String, Object>> allInvoices = null;
			allInvoices = FacturiBlocatePlata.getRejectedInvoices(0,10000,societate, furnizor, motiv, start_date_luna, start_date_an, end_date_luna, end_date_an, finalDate);
				
			String[][] header = new String[18][];
				for (int i = 0; i < 18; i++) {
					header[i] = new String[2];
				}
				header[0][0] = "societate";
				header[0][1] = "Societate";
				header[1][0] = "data_import";
				header[1][1] = "Data import";
				header[2][0] = "inv_number";
				header[2][1] = "Factura nr.";
				header[3][0] = "inv_date";
				header[3][1] = "Data factura";
				header[4][0] = "supplier_code";
				header[4][1] = "Cod furnizor";
				header[5][0] = "supplier_name";
				header[5][1] = "Nume furnizor";
				header[6][0] = "magazin";
				header[6][1] = "Magazin";
				header[7][0] = "data_scadenta";
				header[7][1] = "Data scadenta";
				header[8][0] = "total_with_vat_ron";
				header[8][1] = "Val. cu TVA (RON)";
				header[9][0] = "currency";
				header[9][1] = "Moneda";
				header[10][0] = "total_with_vat_curr";
				header[10][1] = "Val. cu TVA (EUR)";
				header[11][0] = "motiv_blocat";
				header[11][1] = "Motiv blocare la plata";
				header[12][0] = "detalii_motiv";
				header[12][1] = "Detalii motiv";
				header[13][0] = "neajuns_scadenta";
				header[13][1] = "Neajuns la scadenta";
				header[14][0] = "mai_mic_30_zile";
				header[14][1] = "Intarziere mai mica de 30 zile";
				header[15][0] = "intre_30_90";
				header[15][1] = "Intarziere intre 30 si 90 zile";
				header[16][0] = "intre_90_365";
				header[16][1] = "Intarziere intre 90 si 365 zile";
				header[17][0] = "mai_mare_365";
				header[17][1] = "Intarziere mai mare de 1 an";
				try {
					
					ExcelUtil.getExcel(allInvoices, header, "Raport facturi blocate la plata", resourceResponse);
				
				} catch (IOException e) {
					e.printStackTrace();
				}
		} else if (action.equals("findSupplier")) {
			PrintWriter writer = resourceResponse.getWriter();
			String query = ParamUtil.getString(resourceRequest, "keywords");
			String idCompany = ParamUtil.getString(resourceRequest, "id_company");
			writer.print(ControllerUtils.filterSupplierByName(query, Integer.parseInt(idCompany)).toJSONString());
		} else if (action.equals("filter") || action.equals("next") || action.equals("prev") || action.equals("last") || action.equals("first")) {
			PrintWriter writer = resourceResponse.getWriter();
			int start = Integer.parseInt(ParamUtil.getString(resourceRequest, "start").toString());
			int count = Integer.parseInt(ParamUtil.getString(resourceRequest, "count").toString());
			int total = Integer.parseInt(ParamUtil.getString(resourceRequest, "total").toString());
			int societate = ParamUtil.getInteger(resourceRequest, "societate");
			int furnizor = ParamUtil.getInteger(resourceRequest, "furnizor");
			int motiv = ParamUtil.getInteger(resourceRequest, "motiv");
			int start_date_an = ParamUtil.getInteger(resourceRequest, "start_date_an");
			int start_date_luna = ParamUtil.getInteger(resourceRequest, "start_date_luna");
			int end_date_an = ParamUtil.getInteger(resourceRequest, "end_date_an");
			int end_date_luna = ParamUtil.getInteger(resourceRequest, "end_date_luna");
			String finalDate = "";
			if (end_date_an != 0 && end_date_luna != 0){
				finalDate = String.valueOf(end_date_an) + "-"+ String.valueOf(end_date_luna) + "-01";
				finalDate = FacturiBlocatePlata.getLastDayOfMonth(finalDate);
				System.out.println("data dupa conversie:"+finalDate);
			}
			System.out.println("societate: "+societate + " -> " +", furnizor:"+ furnizor + " -> " + ", motiv: " +motiv+ " -> " + 
					", start_date_an:"+start_date_an + ", start_date_luna: "+start_date_luna + ", end_date_an: " +end_date_an + "end_date_luna: "+end_date_luna );

			List<HashMap<String, Object>> allInvoices = null;

			allInvoices = FacturiBlocatePlata.getRejectedInvoices(start, count, societate, furnizor, motiv, start_date_luna, start_date_an, end_date_luna, end_date_an, finalDate);
			allInvoices.addAll(FacturiBlocatePlata.getRejectedInvoicesTotal(start, count, societate, furnizor, motiv, start_date_luna, start_date_an, end_date_luna, end_date_an, finalDate));

			JSONArray jsonLines = new JSONArray();
			if (allInvoices != null) {
				jsonLines = DatabaseConnectionManager.convertListToJson(allInvoices);
			}
			JSONObject jsonResponse = new JSONObject();

			jsonResponse.put("action", action);
			jsonResponse.put("start", start);
			jsonResponse.put("count", count);
			jsonResponse.put("total", total);
			jsonResponse.put("values", jsonLines);
			writer.print(jsonResponse.toJSONString());
		} else if (action.equals("setareFurnizor") ){
			
			List<HashMap<String, Object>> getSupplierNameByCompany = null;
			PrintWriter writer = resourceResponse.getWriter();
			int societate = ParamUtil.getInteger(resourceRequest, "societate");
			getSupplierNameByCompany = DefSupplier.getSupplierNameByCompany(societate);
			
			for (int i = 0 ; i < getSupplierNameByCompany.size(); i++) { 
				writer.print("<option value="+getSupplierNameByCompany.get(i).get("id")+">"+ getSupplierNameByCompany.get(i).get("supplier_code")+" - "+ getSupplierNameByCompany.get(i).get("name")+"</option>");
				//System.out.println("<option value="+getSupplierNameByCompany.get(i).get("id")+">"+ getSupplierNameByCompany.get(i).get("supplier_code")+" - "+ getSupplierNameByCompany.get(i).get("name")+"</option>");
		}
			
			
		}
	}


 

}
