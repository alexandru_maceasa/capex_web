package com.profluo.ec.carrefour.portlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.List;

import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.util.bridges.mvc.MVCPortlet;
import com.profluo.ecm.model.db.DatabaseConnectionManager;
import com.profluo.ecm.model.db.WorkEquipmentHeader;

/**
 * Portlet implementation class RaportHaineLucru
 */
public class RaportHaineLucru extends MVCPortlet {
 
	
	@SuppressWarnings("unchecked")
	public void serveResource(ResourceRequest resourceRequest, ResourceResponse resourceResponse) throws IOException, PortletException {
		String action = ParamUtil.getString(resourceRequest, "action");
		if (action.equals("filter") || action.equals("next") || action.equals("prev") || action.equals("last") || action.equals("first")) {
			PrintWriter writer = resourceResponse.getWriter();
			int start = Integer.parseInt(ParamUtil.getString(resourceRequest, "start").toString());
			int count = Integer.parseInt(ParamUtil.getString(resourceRequest, "count").toString());
			int total = Integer.parseInt(ParamUtil.getString(resourceRequest, "total").toString());
			String invNumber = ParamUtil.getString(resourceRequest, "invoiceNo").toString();
			String supplierCode = ParamUtil.getString(resourceRequest, "supplier_code").toString();
			String supplierName = ParamUtil.getString(resourceRequest, "supplier_name").toString();
			String moneda = ParamUtil.getString(resourceRequest, "moneda").toString();
			String companyId = ParamUtil.getString(resourceRequest, "company_id").toString();
			
			System.out.println(invNumber + " -> " + supplierCode + " -> " + supplierName + " -> " + moneda + " -> " + companyId);
			
			List<HashMap<String, Object>> allLines = null;

			allLines = WorkEquipmentHeader.getAllFilteredFromOptimal(start, count, invNumber, supplierCode, supplierName, moneda, companyId);
			total = WorkEquipmentHeader.getAllFilteredFromOptimalCount(invNumber, supplierCode, supplierName, moneda, companyId);
			
			JSONArray jsonLines = new JSONArray();
			if (allLines != null) {
				jsonLines = DatabaseConnectionManager.convertListToJson(allLines);
			}
			JSONObject jsonResponse = new JSONObject();

			jsonResponse.put("action", action);
			jsonResponse.put("start", start);
			jsonResponse.put("count", count);
			jsonResponse.put("total", total);
			jsonResponse.put("values", jsonLines);
			writer.print(jsonResponse.toJSONString());
		} else if (action.equals("getInventoryDetails")){
			PrintWriter writer = resourceResponse.getWriter();
			String idStoreLine = ParamUtil.getString(resourceRequest, "idStoreLine").toString();
			List<HashMap<String,Object>> inventoryDetails = WorkEquipmentHeader.getInventoryDetailsByIdStoreLine(idStoreLine);
			JSONArray jsonInvNo = new JSONArray();
			
			System.out.println("idStoreLine -> " + idStoreLine);
			
			if (inventoryDetails != null) {
				for (int i = 0; i < inventoryDetails.size(); i++) {
					JSONObject jsonLine = new JSONObject();
					jsonLine.put("nr_inv", inventoryDetails.get(i).get("inventory_no").toString());
					jsonLine.put("data_pif", inventoryDetails.get(i).get("pif_date").toString());
					jsonLine.put("receptie", inventoryDetails.get(i).get("reception").toString());
					jsonInvNo.add(jsonLine);
				}
				writer.print(jsonInvNo.toJSONString());
			} else {
				writer.print("");
			}
			
		}
	}
	
	@Override
	public void render(RenderRequest renderRequest, RenderResponse renderResponse) throws PortletException, IOException {
		renderRequest.setAttribute("pagina", "raport");
		super.render(renderRequest, renderResponse);
	}

}
