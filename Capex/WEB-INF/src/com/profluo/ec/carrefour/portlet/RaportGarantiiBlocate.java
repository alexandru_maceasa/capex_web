package com.profluo.ec.carrefour.portlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.portlet.PortletException;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.model.User;
import com.liferay.portal.service.UserServiceUtil;
import com.liferay.util.bridges.mvc.MVCPortlet;
import com.profluo.ecm.controller.UserTeamIdUtils;
import com.profluo.ecm.flows.NotificationTemplates;
import com.profluo.ecm.flows.NotificationsSubject;
import com.profluo.ecm.model.db.DatabaseConnectionManager;
import com.profluo.ecm.model.db.GenericDBStatements;
import com.profluo.ecm.model.db.InvoiceApprovals;
import com.profluo.ecm.model.db.InvoiceHistory;
import com.profluo.ecm.model.db.RaportSituatieInregistrari;
import com.profluo.ecm.model.db.UsersInformations;
import com.profluo.ecm.notifications.Notifications;
import com.profluo.ecm.utils.ExcelUtil;

/**
 * Portlet implementation class RaportGarantiiBlocate
 */
public class RaportGarantiiBlocate extends MVCPortlet {
 
	@SuppressWarnings("unchecked")
	public void serveResource(ResourceRequest resourceRequest, ResourceResponse resourceResponse) throws IOException, PortletException {
		String action = ParamUtil.getString(resourceRequest, "action");
		if (action.equals("filter") || action.equals("next") || action.equals("prev") || action.equals("last") || action.equals("first")) {
			PrintWriter writer = resourceResponse.getWriter();
			int start = Integer.parseInt(ParamUtil.getString(resourceRequest, "start").toString());
			int count = Integer.parseInt(ParamUtil.getString(resourceRequest, "count").toString());
			int total = Integer.parseInt(ParamUtil.getString(resourceRequest, "total").toString());
			String invNumber = ParamUtil.getString(resourceRequest, "invoiceNo");
			String supplier = ParamUtil.getString(resourceRequest, "supplier");
			String magazin = ParamUtil.getString(resourceRequest, "magazin");
			String co_number = ParamUtil.getString(resourceRequest, "co_number");
			User user = null;
			try {
				user = UserServiceUtil.getUserById(Long.parseLong(resourceRequest.getRemoteUser()));
			} catch (NumberFormatException | PortalException | SystemException e) {
				e.printStackTrace();
			}
			List<HashMap<String,Object>> allLines = RaportSituatieInregistrari.getAllWarrantyLines(start, count, invNumber, supplier, magazin, co_number, user);
			allLines.addAll(RaportSituatieInregistrari.getAllWarrantyLinesTotal(start, count, invNumber, supplier, magazin, co_number, user));
			total = RaportSituatieInregistrari.getAllWarrantyLinesCount(invNumber, supplier, magazin, co_number, user);
			
			JSONArray jsonLines = new JSONArray();
			if (allLines != null) {
				jsonLines = DatabaseConnectionManager.convertListToJson(allLines);
			}
			JSONObject jsonResponse = new JSONObject();
			jsonResponse.put("total", total);
			jsonResponse.put("values", jsonLines);
			writer.print(jsonResponse.toJSONString());
		} else if (action.equals("setWarrantyFlag")) {
			PrintWriter writer = resourceResponse.getWriter();
			String lineIds = ParamUtil.getString(resourceRequest, "ids_to_update");
			InvoiceApprovals.updateWarrantyFlag(lineIds);
			User user = null;
			try {
				user = UserServiceUtil.getUserById(Long.parseLong(resourceRequest.getRemoteUser()));
			} catch (NumberFormatException | PortalException | SystemException e) {
				e.printStackTrace();
			}
			//insert notifications
			List<HashMap<String,Object>> notificationsInfo = InvoiceHistory.getWarrantyLinesInfo(lineIds);
			String body = "";
			for(int i = 0 ; i < notificationsInfo.size(); i++ ){
				body += "<tr><td style='border:1px solid black'>" + notificationsInfo.get(i).get("inv_number").toString() + "</td><td  style='border:1px solid black'>"
						+ notificationsInfo.get(i).get("inv_date").toString() +  "</td><td  style='border:1px solid black'>"
						+ notificationsInfo.get(i).get("furnizor").toString() + "</td><td  style='border:1px solid black'>"
						+ notificationsInfo.get(i).get("produs").toString() + "</td><td  style='border:1px solid black'>"
						+ notificationsInfo.get(i).get("warranty").toString() + "</td></tr>";
			}
			HashMap<String, Object> bodyDetails = new HashMap<String,Object>();
			bodyDetails.put("id_template", NotificationTemplates.DEBLOCARE_GARANTII);
			bodyDetails.put("sender_email", user.getEmailAddress());
			bodyDetails.put("sender_name", user.getFullName());
			bodyDetails.put("body", body);
			String currentTime = new Timestamp(new Date().getTime()).toString();
			bodyDetails.put("time_to_send", currentTime);
			bodyDetails.put("time_of_send", currentTime);
			bodyDetails.put("status", "0");
			bodyDetails.put("type", "B");
			bodyDetails.put("doc_type", "1");
			bodyDetails.put("created", currentTime);
			bodyDetails.put("link", "");

			List<HashMap<String,Object>> userDetails = UsersInformations.getUserDetailsByTeamId(UserTeamIdUtils.ACCOUNTING);
			Notifications.insertNotification("", userDetails, bodyDetails, NotificationsSubject.DEBLOCARE_GARANTII);
			
			//save in log
			String arrFields [][] = new String [2][2];
			arrFields[0][0] = "line_id";
			arrFields[1][0] = "user_id";
			arrFields[1][1] =  String.valueOf(user.getUserId());
			String parts [] = lineIds.split(",");
			for(int i = 0 ; i < parts.length; i++ ) {
				arrFields[0][1] = parts[i];
				GenericDBStatements.insertEntry(arrFields, "log_deblocare_garantie", new ArrayList<String>());
			}
			writer.print("success");
		} else if (action.equals("exportExcel")) {
			User user = null;
			try {
				user = UserServiceUtil.getUserById(Long.parseLong(resourceRequest.getRemoteUser()));
			} catch (NumberFormatException | PortalException | SystemException e) {
				e.printStackTrace();
			}
			List<HashMap<String,Object>> allLines = RaportSituatieInregistrari.getAllWarrantyLines(0, 500000, "", "" , "", "", user);
			allLines.addAll(RaportSituatieInregistrari.getAllWarrantyLinesTotal(0, 500000, "", "" , "", "", user));
			
			String[][] header = new String[7][2];
			
			header[0][0] = "inv_number";
			header[0][1] = "Numar Document";
			header[1][0] = "inv_date";
			header[1][1] = "Data Document";
			header[2][0] = "supplier_name";
			header[2][1] = "Furnizor";
			header[3][0] = "product_name";
			header[3][1] = "Produs";
			header[4][0] = "garantie";
			header[4][1] = "Valoare garantie";
			header[5][0] = "contract";
			header[5][1] = "Nr. contract";
			header[6][0] = "magazin";
			header[6][1] = "Magazin";
			
			ExcelUtil.getExcel(allLines, header, "Raport_Garantii", resourceResponse);
		}
	}
}
