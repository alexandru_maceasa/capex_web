package com.profluo.ec.carrefour.portlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletConfig;
import javax.portlet.PortletException;
import javax.portlet.PortletPreferences;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;
import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.portlet.LiferayPortletConfig;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.servlet.SessionMessages;
import com.liferay.portal.kernel.util.JavaConstants;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.model.User;
import com.liferay.portal.service.UserLocalServiceUtil;
import com.liferay.portal.service.UserServiceUtil;
import com.liferay.portal.theme.ThemeDisplay;
import com.liferay.portal.util.PortalUtil;
import com.liferay.portlet.PortletPreferencesFactoryUtil;
import com.liferay.util.bridges.mvc.MVCPortlet;
import com.profluo.ecm.controller.ControllerUtils;
import com.profluo.ecm.controller.InventoryNumberBO;
import com.profluo.ecm.controller.UserTeamIdUtils;
import com.profluo.ecm.flows.Links;
import com.profluo.ecm.flows.NotificationTemplates;
import com.profluo.ecm.flows.NotificationsSubject;
import com.profluo.ecm.model.db.DPIHeader;
import com.profluo.ecm.model.db.DatabaseConnectionManager;
import com.profluo.ecm.model.db.DefCompanies;
import com.profluo.ecm.model.db.DefExchRates;
import com.profluo.ecm.model.db.DefStore;
import com.profluo.ecm.model.db.GeneralCodes;
import com.profluo.ecm.model.db.GenericDBStatements;
import com.profluo.ecm.model.db.UsersInformations;
import com.profluo.ecm.model.db.WorkEquipmentHeader;
import com.profluo.ecm.model.vo.DefIASVo;
import com.profluo.ecm.model.vo.DefIFRSVo;
import com.profluo.ecm.model.vo.DefProductVo;
import com.profluo.ecm.notifications.Notifications;

/**
 * Portlet implementation class HaineDeLucru
 */
public class WorkEquipment extends MVCPortlet {
	/**
	 * link from the current page
	 */
	private String currentURL;
	
	/**
	 * Generic log instance.
	 */
	private final Log log = LogFactoryUtil.getLog("PROFLUO-" + Invoices.class.getName());
	
	/**
	 * True if an invoice line is prorata
	 */
	boolean isProrata;
	
	/**
	 * Array to store the line numbers for the store lines processing.
	 */
	//private String[] lineNumber = null;
	/**
	 * True if the invoice has new project
	 */
	boolean isNewProject = false;
	/**
	 * List of parameters to be excluded from SQL lines insert.
	 */
	private ArrayList<String> lineExcludes = null;

	/**
	 * List of parameters to be excluded from SQL store lines insert.
	 */
	private ArrayList<String> lineStoresExcludes = null;
	/**
	 * Default AJAX method in the application framework.
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void serveResource(ResourceRequest resourceRequest, ResourceResponse resourceResponse) throws IOException, PortletException {
		log.info("[AJAX] Work Equipment Controller");
		
		User user = null;
    	try{
    		user = UserServiceUtil.getUserById(Long.parseLong(resourceRequest.getRemoteUser()));
    	}catch (Exception e) {}
    	
		String action = ParamUtil.getString(resourceRequest, "action");
		log.info("[AJAX] action: " + action);
		PrintWriter writer = resourceResponse.getWriter();
		 if (action.equals("filter") || action.equals("next") || action.equals("prev") || action.equals("last") || action.equals("first")) {
			// read AJAX parameters used in filtering and pagination
			int start = Integer.parseInt(ParamUtil.getString(resourceRequest, "start").toString());
			int count = Integer.parseInt(ParamUtil.getString(resourceRequest, "count").toString());
			int total = Integer.parseInt(ParamUtil.getString(resourceRequest, "total").toString());
			String moneda = ParamUtil.getString(resourceRequest, "moneda");
			String number = "0";
			if (ParamUtil.getString(resourceRequest, "number") != ""){
				number = ParamUtil.getString(resourceRequest, "number").toString();
			}
			int companyId = Integer.parseInt(ParamUtil.getString(resourceRequest, "company_id").toString());
			String datafactura = ParamUtil.getString(resourceRequest, "datafactura");
			String datainreg = ParamUtil.getString(resourceRequest, "datainreg");
			int stage = Integer.parseInt(ParamUtil.getString(resourceRequest, "stage").toString());
			String supplierCode = ParamUtil.getString(resourceRequest, "supplier_code");
			String supplierName = ParamUtil.getString(resourceRequest, "supplier_name");

			List<HashMap<String, Object>> allWorkEquipments = new ArrayList<HashMap<String,Object>>();
			log.info("Getting all filtered!");
			total = 0;
			allWorkEquipments = WorkEquipmentHeader.getAllFiltered(start, count, companyId, moneda, number, stage, datafactura, datainreg, supplierCode, supplierName, user);
			// if total is 0 we need to refresh the count
			total = WorkEquipmentHeader.getAllFilteredCount(start, count, companyId, moneda, number, stage, datafactura, datainreg, supplierCode, supplierName, user);
			
			JSONArray jsonInvoices = new JSONArray();
			if (allWorkEquipments != null) {
				jsonInvoices = DatabaseConnectionManager.convertListToJson(allWorkEquipments);
			}
			JSONObject jsonResponse = new JSONObject();

			jsonResponse.put("action", action);
			jsonResponse.put("start", start);
			jsonResponse.put("count", count);
			jsonResponse.put("total", total);
			jsonResponse.put("values", jsonInvoices);
			writer.print(jsonResponse.toJSONString());
		} else if (action.equals("getInventoryNo")) {
			int storeId = Integer.parseInt(ParamUtil.getString(resourceRequest, "store").toString());
			int iasId = Integer.parseInt(ParamUtil.getString(resourceRequest, "iasId").toString());
			int tipInv = Integer.parseInt(ParamUtil.getString(resourceRequest, "tipInv").toString());
			int count = (int) Float.parseFloat(ParamUtil.getString(resourceRequest, "count"));
			String numarFactura = ParamUtil.getString(resourceRequest, "numarFactura").toString();

			System.out.println(numarFactura);
			
			//String dataPif = WorkEquipmentHeader.getInvDateById(numarFactura);
			String receptie = WorkEquipmentHeader.getReceptionDateById(numarFactura);
			List<String> listInvNo = null;

			if (iasId != 0 && storeId != 0 && tipInv != 0) {
				if (tipInv == InventoryNumberBO.DE_GRUP) {
					listInvNo = InventoryNumberBO.getInvetoryNumberSequence(storeId, iasId, 1);
				} else if (tipInv == InventoryNumberBO.INDIVIDUAL) {
					listInvNo = InventoryNumberBO.getInvetoryNumberSequence(storeId, iasId, count);
				}
			}

			System.out.println("Secventa de numere de inventar este : " + listInvNo);
			
			JSONArray jsonInvNo = new JSONArray();
			if (listInvNo != null) {
				for (int i = 0; i < listInvNo.size(); i++) {
					JSONObject jsonLine = new JSONObject();
					jsonLine.put("nr_inv", listInvNo.get(i).toString());
					jsonLine.put("data_pif", receptie);
					jsonLine.put("receptie", receptie);
					jsonInvNo.add(jsonLine);
				}
				writer.print(jsonInvNo.toJSONString());
			} else {
				writer.print("");
			}
		} else if (action.equals("getInventoryDetails")){
			String idStoreLine = ParamUtil.getString(resourceRequest, "idStoreLine").toString();
			List<HashMap<String,Object>> inventoryDetails = WorkEquipmentHeader.getInventoryDetailsByIdStoreLine(idStoreLine);
			JSONArray jsonInvNo = new JSONArray();
			
			System.out.println("idStoreLine -> " + idStoreLine);
			
			if (inventoryDetails != null) {
				for (int i = 0; i < inventoryDetails.size(); i++) {
					JSONObject jsonLine = new JSONObject();
					jsonLine.put("nr_inv", inventoryDetails.get(i).get("inventory_no").toString());
					jsonLine.put("data_pif", inventoryDetails.get(i).get("pif_date").toString());
					jsonLine.put("receptie", inventoryDetails.get(i).get("reception").toString());
					jsonInvNo.add(jsonLine);
				}
				writer.print(jsonInvNo.toJSONString());
			} else {
				writer.print("");
			}
			
		}else if (action.equals("getIasInfo")){
			String id = ParamUtil.getString(resourceRequest, "iasId").toString();
			List<HashMap<String, Object>> product = DefIASVo.getById(id);
			JSONArray jsonProduct = new JSONArray();
			if (product != null) {
				jsonProduct = DatabaseConnectionManager.convertListToJson(product);
			}
			JSONObject jsonResponse = new JSONObject();
			jsonResponse.put("product", jsonProduct);
			//System.out.println(jsonProduct);
			writer.print(jsonResponse.toJSONString());
		}else if (action.equals("getIfrsInfo")){
			String id = ParamUtil.getString(resourceRequest, "ifrsId").toString();
			List<HashMap<String, Object>> product = DefIFRSVo.getById(id);
			JSONArray jsonProduct = new JSONArray();
			if (product != null) {
				jsonProduct = DatabaseConnectionManager.convertListToJson(product);
			}
			JSONObject jsonResponse = new JSONObject();
			jsonResponse.put("product", jsonProduct);
			//System.out.println(jsonProduct);
			writer.print(jsonResponse.toJSONString());
		} else if (action.equals("getProdInfo")) {
			// ajax to get product info for one row based on id of the selected
			// product
			String id = ParamUtil.getString(resourceRequest, "prodId").toString();
			List<HashMap<String, Object>> product = DefProductVo.getById(id);
			JSONArray jsonProduct = new JSONArray();
			if (product != null) {
				jsonProduct = DatabaseConnectionManager.convertListToJson(product);
			}
			JSONObject jsonResponse = new JSONObject();
			jsonResponse.put("product", jsonProduct);
			//System.out.println(jsonProduct);
			writer.print(jsonResponse.toJSONString());
		} else if (action.equals("findSupplier")) {
			String query = ParamUtil.getString(resourceRequest, "keywords");
			String idCompany = ParamUtil.getString(resourceRequest, "id_company");
			log.info("[AJAX] Supplier query: " + query);

			writer.print(ControllerUtils.filterSupplierByName(query, Integer.parseInt(idCompany)).toJSONString());
		} else if (action.equals("getBaseInvoices")) {
			String supplier_name = ParamUtil.getString(resourceRequest, "supplier_name").toString();
			String id_company = ParamUtil.getString(resourceRequest, "id_company").toString();
			List<HashMap<String, Object>> baseInvoices = null;

			baseInvoices = WorkEquipmentHeader.getBaseInvoicesFromSupplier(supplier_name, id_company);

			JSONArray jsonBaseInvoices = new JSONArray();
			if (baseInvoices != null) {
				jsonBaseInvoices = DatabaseConnectionManager.convertListToJson(baseInvoices);
			}
			//System.out.println(jsonBaseInvoices);
			JSONObject jsonResponse = new JSONObject();
			jsonResponse.put("baseInvoices", jsonBaseInvoices);
			// send AJAX response
			writer.print(jsonResponse.toJSONString());
		} else if (action.equals("deleteLine")) {
			// id of the invoice in invoice_line
			System.out.println(action);
			String lineId = ParamUtil.getString(resourceRequest, "line_id").toString();
			// System.out.println("controller : " + lineId);
			int error = 0;
			try {
				error = GenericDBStatements.deleteEntry("working_equipment_line", "id", lineId);
				error = GenericDBStatements.deleteEntry("working_equipment_store_line", "line_id", lineId);
			} catch (Exception e) {	}
			// create JSON response
			JSONObject jsonResponse = new JSONObject();
			jsonResponse.put("error", error);
			// send AJAX response
			writer.print(jsonResponse.toJSONString());

		} else if (action.equals("deleteStoreLine")) {
			// id of the store line in invoice_store_line
			String storeLineId = ParamUtil.getString(resourceRequest, "id").toString();
			// index of the store line in the local array
			String index = ParamUtil.getString(resourceRequest, "index").toString();
			int error = 0;
			try {
				error = GenericDBStatements.deleteEntry("working_equipment_store_line", "id", storeLineId);
			} catch (Exception e) {
			}
			// create JSON response
			JSONObject jsonResponse = new JSONObject();
			jsonResponse.put("error", error);
			jsonResponse.put("index", index);
			// send AJAX response
			writer.print(jsonResponse.toJSONString());

		} else if (action.equals("getExchangeRate")) {
			String date = ParamUtil.getString(resourceRequest, "date").toString();
			String selected_currency = ParamUtil.getString(resourceRequest, "currency_sel").toString();
			if (!date.equals("")) {
				float exchange_rate = 0;
				try {
					exchange_rate = DefExchRates.getExchangeRate(
							selected_currency, date, 1);
				} catch (Exception e) {
				}
				// create JSON response
				JSONObject jsonResponse = new JSONObject();
				jsonResponse.put("exchange_rate", exchange_rate);
				// send AJAX response
				writer.print(jsonResponse.toJSONString());
			}
		} else if (action.equals("showDpi")) {
			String idDpi = ParamUtil.getString(resourceRequest, "dpi");

			// get DPI header info based on DPI id
			List<HashMap<String, Object>> oneDpi = null;

			// oneDpi = GenericDBStatements.getAllByFieldName("dpi_header",  "id", idDpi);
			oneDpi = DPIHeader.getAllByFieldName("id", idDpi);

			JSONArray jsonDpiHeader = new JSONArray();
			if (oneDpi != null) {
				jsonDpiHeader = DatabaseConnectionManager.convertListToJson(oneDpi);
			}

			// get DPI lines info based on DPI id
			List<HashMap<String, Object>> dpiLines = null;

			dpiLines = GenericDBStatements.getAllByFieldName("dpi_line", "id_dpi", idDpi);

			JSONArray jsonDpiLines = new JSONArray();
			if (dpiLines != null) {
				jsonDpiLines = DatabaseConnectionManager.convertListToJson(dpiLines);
			}

			// create JSON response
			JSONObject jsonResponse = new JSONObject();
			jsonResponse.put("header", jsonDpiHeader);
			jsonResponse.put("lines", jsonDpiLines);
			// send AJAX response
			writer.print(jsonResponse.toJSONString());
		}
		// super.serveResource(resourceRequest, resourceResponse);
	}
	
	/**
	 * Set list of parameters to be excluded from the invoice store lines sql
	 * statement update.
	 */
	private void setStoreLineExcludes() {
		// attributes to be ignored for table invoice_store_line
		ArrayList<String> excludesForStoreLines = new ArrayList<String>();
		String[] itemsStoreLines = { "id_ifrs", "dt_ifrs", "vizualizare_dpi", "cont_imob_avans",
				"id", "cont_imob_in_curs", "detalii", "clasif", "price_vat_curr", "store_name",
				"id_store_line", "cont_imob_in_fct", "id_ias", "unit_price_ron", "um",
				"inventory", "dt_ras", "lot", "warranty", "vat", "unit_price", "val_cu_tva",
				"invoice_line_id", "product_code", "unit_price_curr", "price_vat_ron", "delete_line", "id_store_line",
				"inventory_no", "pif_date", "reception"};
		for (int i = 0; i < itemsStoreLines.length; i++) {
			excludesForStoreLines.add(itemsStoreLines[i]);
		}

		this.lineStoresExcludes = excludesForStoreLines;
	}
	
	/**
	 * Sets all the columns that are ignored when the invoice line is inserted into the database
	 */
	private void setLinesExcludes() {
		// attributes to be ignored for table invoice_line
		ArrayList<String> excludes = new ArrayList<String>();
		String[] items = { "id_tip_inventar", "id_dpi", "vizualizare_dpi", "cont_imob_avans", "new_prj", "id_store", "initiative", 
				"cont_imob_in_curs", "detalii", "clasif", "id_tip_inreg", "id_store_line", "cont_imob_in_fct", "associated_acc",
				"inventory", "product_code", "lot", "id_act_mf", "id_department", "warranty", "invoice_line_id", "id_cat_gestiune",
				"id_tip_inventar", "id_dpi", "cont_imob_avans", "unit_price", "val_cu_tva", "store_name", "delete_line", "id_store_line",
				"inventory_no", "pif_date", "reception", "id"};
		for (int i = 0; i < items.length; i++) {
			excludes.add(items[i]);
		}
		this.lineExcludes = excludes;
	}
	
	/**
	 * Prepare the sql array to insert the invoice header in the DB.
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	private String[][] getSqlHeaderInvoiceParams(ActionRequest request, ActionResponse response) {
		User user = null;
		try {
			user = UserServiceUtil.getUserById(Long.parseLong(request.getRemoteUser()));
		} catch (Exception e) {
			e.printStackTrace();
		}
		// read request parameters
		int arrSize = 27;
		String[][] arrFields = new String[arrSize][2];

		for (int i = 0; i < arrSize; i++) {
			arrFields[i] = new String[2];
		}

		arrFields[0][0] = "inv_number";
		arrFields[0][1] = request.getParameter("inv_number").toString();
		// String inv_date = request.getParameter("inv_date2").toString();
		arrFields[1][0] = "inv_date";
		arrFields[1][1] = request.getParameter("inv_date2").toString();
		arrFields[2][0] = "due_date";
		arrFields[2][1] = request.getParameter("datascadenta").toString();
		arrFields[3][0] = "tip_fact";
		arrFields[3][1] = request.getParameter("tip-factura").toString();
		arrFields[4][0] = "tip_rel_com";
		arrFields[4][1] = request.getParameter("tip-rel-comerciala").toString();
		arrFields[5][0] = "id_parent";

		// daca tip factura = de corectie
		if (arrFields[3][1].equals("2")) {
			arrFields[5][1] = request.getParameter("nr-fact-baza").toString();
		} else {
			arrFields[5][1] = "0";
		}
		arrFields[6][0] = "id_company";
		arrFields[6][1] = request.getParameter("company").toString();
		arrFields[7][0] = "id_supplier";
		arrFields[7][1] = request.getParameter("id_supplier").toString();
		arrFields[8][0] = "co_number";
		arrFields[8][1] = request.getParameter("nr-contract").toString();
		arrFields[9][0] = "order_no";
		arrFields[9][1] = request.getParameter("nr-comanda").toString();
		
		//Valoare fara TVA
		arrFields[10][0] = "total_no_vat_ron";
		String val_fara_TVA = request.getParameter("val_no_vat_ron").toString();
		if ( val_fara_TVA.contains(",")){
			val_fara_TVA = val_fara_TVA.replace(",", "");
		}
		arrFields[10][1] = val_fara_TVA.toString();
		
		//Valoare TVA
		arrFields[11][0] = "total_vat_ron";
		String val_TVA = request.getParameter("val_vat_ron").toString();
		if ( val_TVA.contains(",")){
			val_TVA = val_TVA.replace(",", "");
		}
		arrFields[11][1] = val_TVA.toString();
		
		//Valoare cu TVA
		arrFields[12][0] = "total_with_vat_ron";
		String val_cu_TVA = request.getParameter("val_with_vat_ron").toString();
		if ( val_cu_TVA.contains(",")){
			val_cu_TVA = val_cu_TVA.replace(",", "");
		}
		arrFields[12][1] = val_cu_TVA.toString();
		
		arrFields[13][0] = "total_no_vat_curr";
		arrFields[13][1] = "0";
		arrFields[14][0] = "total_vat_curr";
		arrFields[14][1] = "0";
		arrFields[15][0] = "total_with_vat_curr";
		arrFields[15][1] = "0";
		arrFields[16][0] = "currency";
		arrFields[16][1] = request.getParameter("currency").toString();
		if (arrFields[16][1].equals("RON")) {
			arrFields[13][1] = arrFields[10][1];
			arrFields[14][1] = arrFields[11][1];
			arrFields[15][1] = arrFields[12][1];
		} else {
			String val_no_vat_curr = request.getParameter("val_no_vat_curr").toString();
			if (val_no_vat_curr.contains(",")){
				val_no_vat_curr = val_no_vat_curr.replace(",", "");
			}
			arrFields[13][1] =val_no_vat_curr.toString();
			String val_vat_curr = request.getParameter("val_vat_curr").toString();;
			if (val_vat_curr.contains(",")){
				val_vat_curr = val_vat_curr.replace(",", "");
			}
			arrFields[14][1] = val_vat_curr.toString();
			String val_with_vat_curr = request.getParameter("val_with_vat_curr").toString();
			if (val_with_vat_curr.contains(",")){
				val_with_vat_curr = val_with_vat_curr.replace(",", "");
			}
			arrFields[15][1] = val_with_vat_curr.toString();
		}
		arrFields[17][0] = "exchange_rate";
		arrFields[17][1] = request.getParameter("rata-schimb").toString();
		
		arrFields[18][0] = "payment_value_ron";
		arrFields[18][1] = request.getParameter("val-netaplata-ron").replace(",", "").toString();
		arrFields[19][0] = "payment_value_curr";
		if (arrFields[16][1].equals("RON")) {
			arrFields[19][1] = arrFields[18][1];
		} else {
			arrFields[19][1] = request.getParameter("val-netaplata-eur").replace(",", "").toString();
		}

//		arrFields[20][0] = "tip_capex";
//		arrFields[20][1] = request.getParameter("categ_capex").toString();
	//	System.out.println("categ capex : " + arrFields[20][1]);
//		arrFields[21][0] = "id_new_prj";
		// categoria capex este proiect nou
//		if (arrFields[20][1].equals("2")) {
//			String proiect = request.getParameter("project").toString();
//			if ( proiect == "" ){
//				proiect = "0";
//			}
//			arrFields[21][1] = proiect;
//			isNewProject = true;
//		} else {
//			arrFields[21][1] = "0";
//		}
//		arrFields[22][0] = "id_initiative";
		// categoria capex este lista initiativa
//		if (arrFields[20][1].equals("1")) {
//			String initiativa = request.getParameter("lista_initiative").toString();
//			if ( initiativa == "" ){
//				initiativa = "0";
//			}
//			arrFields[22][1] = initiativa;
//		} else {
//			arrFields[22][1] = "0";
//		}
        
        
        Calendar calendar = Calendar.getInstance();
	    java.sql.Timestamp currentTS = new java.sql.Timestamp(calendar.getTime().getTime());
	    
        arrFields[20][0] = "modified";
        arrFields[20][1] = currentTS.toString();
        arrFields[21][0] = "id_user";
        arrFields[21][1] = String.valueOf(user.getUserId());
        arrFields[22][0] = "reg_date";
        arrFields[22][1] = request.getParameter("created").toString();
        arrFields[23][0] = "kontan_acc";
        
        String store_code = request.getParameter("supplier_code").toString();
        
        if (store_code.startsWith("7")){
        	if(request.getParameter("kontan_acc7") != null){
        		arrFields[23][1] = request.getParameter("kontan_acc7").toString();
        	}
        } else if (store_code.startsWith("8")){
        	if(request.getParameter("kontan_acc8") != null){
        		arrFields[23][1] = request.getParameter("kontan_acc8").toString();
        	}
        } else {
        	if(request.getParameter("kontan_acc2") != null){
        		arrFields[23][1] = request.getParameter("kontan_acc2").toString();
        	}
        }
        arrFields[24][0] = "stage";
        arrFields[24][1] = "2";
        //set accounting month
        arrFields[25][0] = "accounting_month";
        arrFields[25][1] = GeneralCodes.getAccountingMonth();
        arrFields[26][0] = "accounting_year";
        arrFields[26][1] = GeneralCodes.getAccountingYear();
        
        return arrFields;
	}
	
	
	/**
	 * Returns the index of column in the array of line columns
	 * 
	 * @param row
	 * @return
	 */
	public int getColumnIndex(String[][] row, String column) {
		for (int i = 0; i < row.length; i++) {
			if (row[i][0].equals(column)) {
				return i;
			}
		}
		return 0;
	}
	
	private HashMap<String, Object> setInsertParameters (String id_template, String to_email, String to_name, String body, String status, 
			String type, String doc_type, User user, String link){
		System.out.println("Link : " + link);
		HashMap<String, Object> bodyDetails = new HashMap<String,Object>();
		
		bodyDetails.put("id_template", id_template);
		bodyDetails.put("sender_email", user.getEmailAddress());
		bodyDetails.put("sender_name", user.getFullName());
		bodyDetails.put("body", body);
		String currentTime = new Timestamp(new Date().getTime()).toString();
		bodyDetails.put("time_to_send", currentTime);
		bodyDetails.put("time_of_send", currentTime);
		bodyDetails.put("status", status);
		bodyDetails.put("type", type);
		bodyDetails.put("doc_type", doc_type);
		bodyDetails.put("created", currentTime);
		if(link.contains("url")){
			bodyDetails.put("link", link.substring(0, link.lastIndexOf("?")));
		} else {
			bodyDetails.put("link", link);
		}
		return bodyDetails;
	}
	
	/**
	 * Return a list of stores that have no associated dpi 
	 * @param storeIdsWithDpi
	 * @return
	 */
	private List<String> getAllStoreIdsToNotify (String storeIdsWithDpi){
		List<String> storeIds = new ArrayList<String>();
		String[] splitByComma = storeIdsWithDpi.split(",");
		for (int i = 0; i < splitByComma.length; i++){
			if (splitByComma[i].contains("-0")){
				storeIds.add(splitByComma[i].substring(0, splitByComma[i].indexOf("-")));
			}
		}
		System.out.println("store ids = " + storeIds);
		return storeIds;
	}
	
	public void sendNotificationsforMissingDPI(String[][] arrFields, User user, String supplier, String[][][] lineDataArticles) {
		
		boolean mustNotifiStoreDirector = false;
		String storesWithDpi = "";
		
		for (int i = 0; i < lineDataArticles.length; i++){
			String store = "", dpi = "";
			for (int j = 0; j < lineDataArticles[i].length; j++ ){
				if (lineDataArticles[i][j][0].equals("id_store")){
					store = lineDataArticles[i][j][1];
				}
				if(lineDataArticles[i][j][0].equals("id_dpi")){
					dpi = lineDataArticles[i][j][1];
					//daca factura pe proiect nou dar are si DPI
					if(dpi.equals("0")){
						mustNotifiStoreDirector = true;
					}
				}
			}
			if (i == lineDataArticles.length - 1){
				storesWithDpi += store + "-" + dpi;
			} else {
				storesWithDpi += store + "-" + dpi + ",";
			}
		}
		
		String companyName = DefCompanies.getCompanyNameById(arrFields[6][1]);
		String link = Links.individualInvoiceForApprovals + "&id=" + arrFields[0][1];
		String templateId = "";
		String body = "";
		body = "<tr><td style='border:1px solid black'>" + arrFields[0][1] + "</td>"
				+ "<td style='border:1px solid black'>" + arrFields[1][1] + "</td><td style='border:1px solid black'>"
				+ supplier + "</td><td style='border:1px solid black'>"
				+ companyName + "</td>"
				+ "<td style='border:1px solid black'><a href ='" + link + "'>Link</a></td></tr>";
		templateId = NotificationTemplates.INREGISTRARE_FACTURA;
		
		HashMap<String, Object> bodyDetails = new HashMap<String,Object>();
		bodyDetails = setInsertParameters(templateId, "", "", body, "0", "B", "1", user, currentURL);
		System.out.println("must notif store directors = " + mustNotifiStoreDirector);
		if (mustNotifiStoreDirector){
			List<String> storeIdsToNotify = getAllStoreIdsToNotify(storesWithDpi);
			List<HashMap<String,Object>> allStoreDirectors = UsersInformations.getUserDetailsByTeamId(UserTeamIdUtils.STORE_DIRECTOR);
			for (int i = allStoreDirectors.size() - 1; i >= 0; i-- ){
				try {
					User storeDirector = UserLocalServiceUtil.getUser(Integer.parseInt(allStoreDirectors.get(i).get("user_id").toString()));
					if (storeDirector.getAddresses().size() > 0){
						if (!storeIdsToNotify.contains(DefStore.getIdByStoreId(storeDirector.getAddresses().get(0).getStreet2().toString()))){
							allStoreDirectors.remove(i);
						}
					}
				} catch (NumberFormatException | PortalException | SystemException e) {
					e.printStackTrace();
				}	
			}
			Notifications.insertNotification(arrFields[0][1], allStoreDirectors, bodyDetails, NotificationsSubject.FACTURA_HAINE_DE_LUCRU_LIPSA_DPI);
		}
	}
	
	public void addInvoice(ActionRequest request, ActionResponse response) {
		// remove default error message from Liferay
		PortletConfig portletConfig = (PortletConfig) request.getAttribute(JavaConstants.JAVAX_PORTLET_CONFIG);
		LiferayPortletConfig liferayPortletConfig = (LiferayPortletConfig) portletConfig;
		SessionMessages.add(request, liferayPortletConfig.getPortletId() + SessionMessages.KEY_SUFFIX_HIDE_DEFAULT_ERROR_MESSAGE);

		log.info("[PROFLUO] WorkEquipment portlet - addInvoice()");
		try {
			
			String action = request.getParameter("insertORupdate").toString();
			int storeLineSuccess = 0;
			ArrayList<String> nullArrayList = new ArrayList<String>();
			int noInserted = 0;
			this.setLinesExcludes();
			this.setStoreLineExcludes();
			
			String[][] arrFields = this.getSqlHeaderInvoiceParams(request, response);
			String[][][] lineDataArticles = null;
			String dataTableArticles = request.getParameter("datatable_lines");
			//liniile alocate pe factura
			lineDataArticles = ControllerUtils.convertToArrayMatrixFromJSON(dataTableArticles);
			
			// update attrs for where statement
			ArrayList<String> whereStatement = new ArrayList<String>();
			// attrs values for where statement
			ArrayList<String> whereStatementValues = new ArrayList<String>();
			whereStatement.add("id");
			whereStatementValues.add(action);
			//update header
			noInserted = GenericDBStatements.updateEntry(arrFields, "working_equipment_header", nullArrayList, whereStatement, whereStatementValues);
			if (noInserted <= 0) {
				SessionErrors.add(request, "update_nok");
				ControllerUtils.saveRequestParamsOnResponse(request, response, log);
				response.setRenderParameter("mvcPath", "/html/workequipment/registerWorkEquipment.jsp");
			}
			if (noInserted > 0) {
				for(int i = 0; i < lineDataArticles.length; i++ ){
					this.setLinesExcludes();
					ArrayList<String> columns = new ArrayList<String>();
					columns.add("id");
					columns.add("inv_number");
					ArrayList<String> values = new ArrayList<String>();
					int indexOfLine = getColumnIndex(lineDataArticles[i], "invoice_line_id");
					values.add(lineDataArticles[i][indexOfLine][1]);
					indexOfLine = getColumnIndex(lineDataArticles[i], "inv_number");
					values.add(lineDataArticles[i][indexOfLine][1]);
					//update lines
					GenericDBStatements.updateEntry(lineDataArticles[i], "working_equipment_line", lineExcludes, columns, values);
					
					this.setStoreLineExcludes();
					columns.clear();
					columns.add("line_id");
					columns.add("inv_number");
					values.clear();
					indexOfLine = getColumnIndex(lineDataArticles[i], "id_store_line");
					values.add(lineDataArticles[i][indexOfLine][1]);
					indexOfLine = getColumnIndex(lineDataArticles[i], "inv_number");
					values.add(lineDataArticles[i][indexOfLine][1]);
					//update store lines
					storeLineSuccess = GenericDBStatements.updateEntry(lineDataArticles[i], "working_equipment_store_line",lineStoresExcludes , columns, values);
					
					//update store_line_inv
					indexOfLine = getColumnIndex(lineDataArticles[i], "inventory_no");
					String inventoryNo = lineDataArticles[i][indexOfLine][1];
					indexOfLine = getColumnIndex(lineDataArticles[i], "pif_date");
					String pifDate = lineDataArticles[i][indexOfLine][1];
					indexOfLine = getColumnIndex(lineDataArticles[i], "reception");
					String reception = lineDataArticles[i][indexOfLine][1];
					indexOfLine = getColumnIndex(lineDataArticles[i], "id_store_line");
					String id_store_line = lineDataArticles[i][indexOfLine][1];
					indexOfLine = getColumnIndex(lineDataArticles[i], "inv_number");
					String inv_number = lineDataArticles[i][indexOfLine][1];
					if(pifDate.startsWith("[\"")) {
						pifDate = pifDate.replace("[\"", "");
					}
					if(pifDate.endsWith("\"]")) {
						pifDate = pifDate.replace("\"]", "");
					}
					if(reception.startsWith("[\"")) {
						reception = reception.replace("[\"", "");
					}
					if(reception.endsWith("\"]")) {
						reception = reception.replace("\"]", "");
					}
					WorkEquipmentHeader.updateInventory(inventoryNo, pifDate, reception, id_store_line,inv_number);
				}
			} 
			
			if (storeLineSuccess > 0) {
				SessionMessages.add(request, "invoiceUpdated");
				response.setRenderParameter("mvcPath", "/html/workequipment/view.jsp");
			}
		} catch (Exception e) {
			e.printStackTrace();
			SessionErrors.add(request, "error");
			response.setRenderParameter("mvcPath", "/html/workequipment/registerWorkEquipment.jsp");
		}
	}

	
	
	/**
	 * 
	 */
	@Override
	public void render(RenderRequest renderRequest, RenderResponse renderResponse) throws PortletException, IOException {
		HttpServletRequest request = PortalUtil.getHttpServletRequest(renderRequest);
		String p_p_id = PortalUtil.getOriginalServletRequest(request).getParameter("p_p_id");
		String path = PortalUtil.getOriginalServletRequest(request).getParameter("_" + p_p_id + "_mvcPath");
		
		//renderRequest.setAttribute("pagina", "facturi-contabilitate");
		
		try {
			ThemeDisplay themeDisplay = ((ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY));
			String portletId = themeDisplay.getPortletDisplay().getId();
			PortletPreferences portletSetup = PortletPreferencesFactoryUtil.getLayoutPortletSetup(themeDisplay.getLayout(), portletId);
			String portletCustomTitle = themeDisplay.getPortletDisplay().getTitle();
			portletCustomTitle = portletSetup.getValue("portletSetupTitle_" + themeDisplay.getLanguageId(), portletCustomTitle);
			User user = UserServiceUtil.getUserById(Long.parseLong(renderRequest.getRemoteUser()));

			ThemeDisplay td = (ThemeDisplay) renderRequest.getAttribute(WebKeys.THEME_DISPLAY);
			log.info("[PROFLUO] Work Equipment portlet - render()");
			log.info("[PROFLUO] User full name: " + td.getUser().getFullName());
			log.info("[PROFLUO] Portlet Title: " + portletCustomTitle);
			
			currentURL = Links.unifiedLoginPrefix +  PortalUtil.getCurrentCompleteURL(request);
			
			int id_user = (int) user.getUserId();
			log.info("[PROFLUO] User id " + id_user);
			log.info("[PROFLUO] Group id " + user.getGroupId());
			log.info("[PROFLUO] Group id " + user.getRoles());
			log.info("[PROFLUO] currentUrl " + currentURL);

			renderRequest.setAttribute("pagina", "workEquipment");

			if (path == null || path.equals("view.jsp")) {
				log.info("[PROFLUO] HAINE DE LUCRU LISTING render()");
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}

		super.render(renderRequest, renderResponse);
	}

}
