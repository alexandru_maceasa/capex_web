package com.profluo.ec.carrefour.portlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.List;

import javax.portlet.PortletException;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.util.bridges.mvc.MVCPortlet;
import com.profluo.ecm.model.db.DatabaseConnectionManager;
import com.profluo.ecm.model.db.VoucherHeader;
import com.profluo.ecm.utils.ExcelUtil;

/**
 * Portlet implementation class RegisteredVouchers
 */
public class RegisteredVouchers extends MVCPortlet {
	
	public String calibrateDate(String date){
		String[] parts = date.split("-");
		if (Integer.parseInt(parts[1]) < 10){
			String aux = parts[1].substring(1, parts[1].length());
			parts[1] = aux;
		}
		return parts[0] + "-" + parts[1] + "-01";
	}
	
	@SuppressWarnings("unchecked")
	public void serveResource(ResourceRequest resourceRequest, ResourceResponse resourceResponse) throws IOException, PortletException {
		String action = ParamUtil.getString(resourceRequest, "action");
		if (action.equals("exportExcel")) {
			String fromDate = ParamUtil.getString(resourceRequest, "start_date_an") + "-" + ParamUtil.getString(resourceRequest, "start_date_luna") + "-01";
			String toDate =  ParamUtil.getString(resourceRequest, "end_date_an") + "-" + ParamUtil.getString(resourceRequest, "end_date_luna") + "-01";
			System.out.println(fromDate + " si data de sfarsit:" + toDate+ "si action:"+action);
			
			List<HashMap<String, Object>> allVouchers = VoucherHeader.getAllRegisteredVouchersRaport(fromDate, toDate);
			String[][] header = new String[9][];
			for (int i = 0; i < 9; i++) {
				header[i] = new String[2];
			}
			
			header[0][0] = "nrInventar";
			header[0][1] = "Numar inventar";
			header[1][0] = "dataPIF";
			header[1][1] = "Data PIF";
			header[2][0] = "denumire";
			header[2][1] = "Denumire";
			header[3][0] = "nrVoucher";
			header[3][1] = "Numar bon";
			header[4][0] = "dataVoucher";
			header[4][1] = "Data bon";
			header[5][0] = "codSocietate";
			header[5][1] = "Cod societate";
			header[6][0] = "codMagazin";
			header[6][1] = "Cod magazin";
			header[7][0] = "valoare";
			header[7][1] = "Valoare fara TVA";
			header[8][0] = "reg_date";
			header[8][1] = "Data inregistrare";
					
			try {
				ExcelUtil.getExcel(allVouchers, header, "Raport_bonuri_inregistrate",
						resourceResponse);
			} catch (IOException e) {
				e.printStackTrace();
			}
		} else if (action.equals("filter") || action.equals("next") || action.equals("prev") || action.equals("last") || action.equals("first")) {
			PrintWriter writer = resourceResponse.getWriter();
			int start = Integer.parseInt(ParamUtil.getString(resourceRequest, "start").toString());
			int count = Integer.parseInt(ParamUtil.getString(resourceRequest, "count").toString());
			int total = Integer.parseInt(ParamUtil.getString(resourceRequest, "total").toString());
			String inventoryNo = ParamUtil.getString(resourceRequest, "inventoryNo");
			String voucherNo = ParamUtil.getString(resourceRequest, "voucherNo");
			String codMagazin = ParamUtil.getString(resourceRequest, "codMagazin");

			List<HashMap<String, Object>> allRegVouchers = VoucherHeader.getAllRegisteredVouchers(start, count, inventoryNo, voucherNo, codMagazin);
			total =  VoucherHeader.getAllRegisteredVouchersCount(inventoryNo, voucherNo, codMagazin);

			JSONArray jsonLines = new JSONArray();
			if (allRegVouchers != null) {
				jsonLines = DatabaseConnectionManager.convertListToJson(allRegVouchers);
			}
			JSONObject jsonResponse = new JSONObject();

			jsonResponse.put("action", action);
			jsonResponse.put("start", start);
			jsonResponse.put("count", count);
			jsonResponse.put("total", total);
			jsonResponse.put("values", jsonLines);
			writer.print(jsonResponse.toJSONString());
		}
	}
	
	public HashMap<String, Integer> computeDate(String fromDate, String toDate){
		HashMap<String, Integer> components = new HashMap<String, Integer>();
		//start date
		String[] comps = fromDate.split("-");
		components.put("startMonth", Integer.parseInt(comps[1]));
		components.put("startYear", Integer.parseInt(comps[0]));
		//end date
		comps = toDate.split("-");
		components.put("endMonth", Integer.parseInt(comps[1]));
		components.put("endYear", Integer.parseInt(comps[0]));
		
		return components;
	}
}
