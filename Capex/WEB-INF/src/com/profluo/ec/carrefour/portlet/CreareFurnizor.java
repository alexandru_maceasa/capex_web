package com.profluo.ec.carrefour.portlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletConfig;
import javax.portlet.PortletException;
import javax.portlet.PortletPreferences;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;
import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.portlet.LiferayPortletConfig;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.servlet.SessionMessages;
import com.liferay.portal.kernel.util.JavaConstants;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.model.User;
import com.liferay.portal.service.UserServiceUtil;
import com.liferay.portal.theme.ThemeDisplay;
import com.liferay.portal.util.PortalUtil;
import com.liferay.portlet.PortletPreferencesFactoryUtil;
import com.liferay.util.bridges.mvc.MVCPortlet;
import com.profluo.ecm.model.db.DatabaseConnectionManager;
import com.profluo.ecm.model.db.GenericDBStatements;
import com.profluo.ecm.model.db.InvoiceHeader;

/**
 * Portlet implementation class CreareFurnizor
 */
public class CreareFurnizor extends MVCPortlet {
	/*
	 * Generic log instance.
	 */
	private final Log log = LogFactoryUtil.getLog("PROFLUO-" + MVCPortlet.class.getName());
	
	/**
	 * Getting information about portlet initialization.
	 */
   @Override
   public void init() throws PortletException {
	   	log.info("[PROFLUO] Initialize Creare furnizor portlet.");
	   	super.init();
   }
   
   /**
    * 
    * @param request
    * @param response
    */
   public void updateEntry( ActionRequest request, ActionResponse response){
	   // remove default error message from Liferay
	   PortletConfig portletConfig = (PortletConfig)request.getAttribute(JavaConstants.JAVAX_PORTLET_CONFIG);
	   LiferayPortletConfig liferayPortletConfig = (LiferayPortletConfig) portletConfig;
	   SessionMessages.add(request, liferayPortletConfig.getPortletId() + SessionMessages.KEY_SUFFIX_HIDE_DEFAULT_ERROR_MESSAGE);
		
	   log.info("[PROFLUO] Facturi Blocate portlet - approveInvoice()");
	   try{
		   	String[][] arrFields = new String[1][2];
		   	
		   	String id = request.getParameter("id").toString();
		   	String supplier_code = request.getParameter("supplier_code").toString();
		   	
		   	arrFields[0][0] = "supplier_code";
		   	arrFields[0][1] = supplier_code;
		   	//attrs for where statement
       		ArrayList<String> whereStatement = new ArrayList<String>();
       		//attrs values for where statement
	       	ArrayList<String> whereStatementValues = new ArrayList<String>();
	       	whereStatement.add("id");
	       	whereStatementValues.add(id);
	       	ArrayList<String> excludes = new ArrayList<String>();
	       	int update = GenericDBStatements.updateEntry(arrFields, "def_suppliers", excludes, whereStatement, whereStatementValues);
	       	if(update <= 0){
				SessionErrors.add(request, "update_nok");
	    		response.setRenderParameter("mvcPath", "/html/inv_create_supplier/view.jsp");
	    		return;
	       	} else {
	       		SessionErrors.add(request, "update_ok");
	       	}
	   }catch (Exception e){
		   
	   }
   }
   /**
    * Default AJAX method in the application framework.
    */
   @SuppressWarnings("unchecked")
@Override
   public void serveResource(ResourceRequest resourceRequest, ResourceResponse resourceResponse) throws IOException, PortletException {
	   	log.info("[AJAX] Creare furnizor Controller");
	
	   	String action = ParamUtil.getString(resourceRequest, "action");
	   	log.info("[AJAX] action: " + action);
	   	PrintWriter writer = resourceResponse.getWriter();
	
	   	if (action.equals("get")) {
	   		String id = ParamUtil.getString(resourceRequest, "id");
	   		// get all associated invoices
	   		List<HashMap<String,Object>> associatedInvoices = null;
			associatedInvoices = InvoiceHeader.getAssociatedInvoices(id);
	    	JSONArray jsonInvoices = new JSONArray();
	    	if (associatedInvoices != null) {
	    		jsonInvoices = DatabaseConnectionManager.convertListToJson(associatedInvoices);
	    	}
	    	
	    	// create JSON response
	    	JSONObject jsonResponse = new JSONObject();
			jsonResponse.put("values", jsonInvoices);
			// send AJAX response
			writer.print(jsonResponse.toJSONString());
   	}
   	//super.serveResource(resourceRequest, resourceResponse);
   } 
   
   
   /**
	 * 
	 */
	@Override
	public void render(RenderRequest renderRequest, RenderResponse renderResponse) throws PortletException, IOException {
		HttpServletRequest request = PortalUtil.getHttpServletRequest(renderRequest);
	   	String p_p_id = PortalUtil.getOriginalServletRequest(request).getParameter("p_p_id");
	   	String path = PortalUtil.getOriginalServletRequest(request).getParameter("_" + p_p_id + "_mvcPath");
	   	try {
	    	ThemeDisplay themeDisplay = ((ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY));
	    	String portletId = themeDisplay.getPortletDisplay().getId();
	    	PortletPreferences portletSetup = PortletPreferencesFactoryUtil.getLayoutPortletSetup(themeDisplay.getLayout(), portletId);
	    	String portletCustomTitle = themeDisplay.getPortletDisplay().getTitle();
	    	portletCustomTitle = portletSetup.getValue("portletSetupTitle_" + themeDisplay.getLanguageId(), portletCustomTitle);
	    	User user = UserServiceUtil.getUserById(Long.parseLong(renderRequest.getRemoteUser()));
	    	
	        ThemeDisplay td  = (ThemeDisplay) renderRequest.getAttribute(WebKeys.THEME_DISPLAY);
	        log.info("[PROFLUO] Invoices portlet - render()");
	        log.info("[PROFLUO] User full name: " + td.getUser().getFullName());
	        log.info("[PROFLUO] Portlet Title: " + portletCustomTitle);
	    	
	    	int id_user 			= (int) user.getUserId();
	    	log.info("[PROFLUO] User id " + id_user);
	    	
	    	if (path == null || path.equals("view.jsp")) {
	    		log.info("[PROFLUO] LISTING Creare furnizor render()");
	    	}
   	} catch (Exception e) {
   		e.printStackTrace();
   	}
   	
	    super.render(renderRequest, renderResponse);
	}
}
