package com.profluo.ec.carrefour.portlet;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;

import javax.portlet.PortletException;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.util.bridges.mvc.MVCPortlet;
import com.profluo.ecm.model.db.RaportSituatieInregistrari;
import com.profluo.ecm.utils.ExcelUtil;

/**
 * Portlet implementation class RaportChGenInvoices
 */
public class RaportChGenInvoices extends MVCPortlet {
	
	private final Log log = LogFactoryUtil.getLog("PROFLUO-" + RaportChGenInvoices.class.getName());
 
	@Override
	public void serveResource(ResourceRequest resourceRequest, ResourceResponse resourceResponse) throws IOException, PortletException {
		log.info("[AJAX] Raport_ch_gen_invoices");

		String action = ParamUtil.getString(resourceRequest, "action");
		if (action.equals("exportExcel")) {
			String fromDate = ParamUtil.getString(resourceRequest, "from_date");
			String toDate = ParamUtil.getString(resourceRequest, "to_date");
			
			List<HashMap<String, Object>> allCategories = RaportSituatieInregistrari.getAllInvoicesSentToChGen(fromDate, toDate);
						
			String[][] header = new String[6][];
			for (int i = 0; i < 6; i++) {
				header[i] = new String[2];
			}
			
			header[0][0] = "inv_number";
			header[0][1] = "Numar factura";
			header[1][0] = "furnizor";
			header[1][1] = "Furnizor";
			header[2][0] = "companie";
			header[2][1] = "Societate";
			header[3][0] = "valoare";
			header[3][1] = "Valoare factura";
			header[4][0] = "contabil";
			header[4][1] = "Contabil";
			header[5][0] = "time_of_sent";
			header[5][1] = "Data trimiterii in Ch Gen";
					
			try {
				ExcelUtil.getExcel(allCategories, header, "Raport_facturi_Ch_Gen", resourceResponse);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

}
