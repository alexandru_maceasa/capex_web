package com.profluo.ec.carrefour.portlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.List;

import javax.portlet.PortletException;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.util.bridges.mvc.MVCPortlet;
import com.profluo.ecm.controller.ControllerUtils;
import com.profluo.ecm.model.db.DatabaseConnectionManager;
import com.profluo.ecm.model.db.DefStore;
import com.profluo.ecm.model.db.DefSupplier;
import com.profluo.ecm.model.db.GenericDBStatements;
import com.profluo.ecm.model.db.InventoryHeader;
import com.profluo.ecm.model.db.InvoiceHeader;
import com.profluo.ecm.model.vo.DefSupplierVo;
import com.profluo.ecm.utils.ExcelUtil;

/**
 * Portlet implementation class RaporFacturiLitigii
 */
public class RaporFacturiLitigii extends MVCPortlet {
	
	/**
	 * Generic log instance.
	 */
	private final Log log = LogFactoryUtil.getLog("PROFLUO-" + RaporFacturiLitigii.class.getName());
	
	public void serveResource(ResourceRequest resourceRequest, ResourceResponse resourceResponse) throws IOException, PortletException {
		String action = ParamUtil.getString(resourceRequest, "action");
		if (action.equals("filter") || action.equals("next") || action.equals("prev") || action.equals("last") || action.equals("first")) {
			PrintWriter writer = resourceResponse.getWriter();
			int start = Integer.parseInt(ParamUtil.getString(resourceRequest, "start").toString());
			int count = Integer.parseInt(ParamUtil.getString(resourceRequest, "count").toString());
			int total = Integer.parseInt(ParamUtil.getString(resourceRequest, "total").toString());
			String societate = ParamUtil.getString(resourceRequest, "societate").toString();
			String magazin = ParamUtil.getString(resourceRequest, "magazin");
			String supplier = ParamUtil.getString(resourceRequest, "supplier");
			int fromYear = Integer.parseInt(ParamUtil.getString(resourceRequest, "from_year").toString());
			int toYear = Integer.parseInt(ParamUtil.getString(resourceRequest, "to_year").toString());
			int fromMonth = Integer.parseInt(ParamUtil.getString(resourceRequest, "from_month").toString());
			int toMonth = Integer.parseInt(ParamUtil.getString(resourceRequest, "to_month").toString());
			String storeName = "";
			if (!magazin.equals("") && !magazin.equals("0") && !magazin.equals("-1")){
				storeName = DefStore.getStoreNameById(magazin);
			}
			List<HashMap<String, Object>> allLines = null;

			allLines = InvoiceHeader.getInvoicesLinesLitigii(start, count, societate, storeName, supplier, fromYear, toYear, fromMonth, toMonth);
			total = InvoiceHeader.getInvoicesLinesLitigiiCount(societate, storeName, supplier, fromYear, toYear, fromMonth, toMonth);

			JSONArray jsonLines = new JSONArray();
			if (allLines != null) {
				jsonLines = DatabaseConnectionManager.convertListToJson(allLines);
			}
			JSONObject jsonResponse = new JSONObject();

			jsonResponse.put("total", total);
			jsonResponse.put("values", jsonLines);
			writer.print(jsonResponse.toJSONString());
		} else if(action.equals("exportRaport")) {
			String societate = ParamUtil.getString(resourceRequest, "company_id").toString();
			String magazin = ParamUtil.getString(resourceRequest, "id_magazin").toString();
			String supplier = ParamUtil.getString(resourceRequest, "id_furnizor").toString();
			int fromYear = ParamUtil.getInteger(resourceRequest, "from_year");
			int toYear = ParamUtil.getInteger(resourceRequest, "to_year");
			int fromMonth =  ParamUtil.getInteger(resourceRequest, "from_month");
			int toMonth =  ParamUtil.getInteger(resourceRequest, "to_month");
			String storeName = "";
			if (!magazin.equals("") && !magazin.equals("0") && !magazin.equals("-1")){
				storeName = DefStore.getStoreNameById(magazin);
			}
			
			List<HashMap<String, Object>> allLines = InvoiceHeader.getInvoicesLinesLitigii(0, 5000000, societate, storeName, supplier, fromYear, toYear, fromMonth, toMonth);
			String[][] header = new String[15][];
			for (int i = 0; i < 15; i++) {
				header[i] = new String[2];
			}
			header[0][0] = "societate";
			header[0][1] = "Societate";
			header[1][0] = "data_import";
			header[1][1] = "Data import";
			header[2][0] = "nr";
			header[2][1] = "Factura nr.";
			header[3][0] = "inv_date";
			header[3][1] = "Data factura";
			header[4][0] = "codFurnizor";
			header[4][1] = "Cod furnizor";
			header[5][0] = "furnizor";
			header[5][1] = "Nume furnizor";
			header[6][0] = "magazin";
			header[6][1] = "Magazin";
			header[7][0] = "cod_prod";
			header[7][1] = "Cod produs";
			header[8][0] = "description";
			header[8][1] = "Denumire produs";
			header[9][0] = "quantity";
			header[9][1] = "Cantitate";
			header[10][0] = "unit_price_ron";
			header[10][1] = "Pret unitar factura RON";
			header[11][0] = "val_no_vat_ron";
			header[11][1] = "Val fara TVA factura RON";
			header[12][0] = "unit_price_curr";
			header[12][1] = "Pret unitar factura EUR";
			header[13][0] = "val_no_vat_curr";
			header[13][1] = "Val fara TVA factura EUR";
			header[14][0] = "detalii_litigii";
			header[14][1] = "Detalii litigii";

			ExcelUtil.getExcel(allLines, header, "Raport_Facturi_litigii", resourceResponse);
		} else if (action.equals("findSupplier")){
			PrintWriter writer = resourceResponse.getWriter();
			String query = ParamUtil.getString(resourceRequest, "keywords");
			String idCompany = ParamUtil.getString(resourceRequest, "id_company");
			log.info("[AJAX] Supplier query: " + query);

			writer.print(ControllerUtils.filterSupplierByName(query, Integer.parseInt(idCompany)).toJSONString());
		}
	}
}
