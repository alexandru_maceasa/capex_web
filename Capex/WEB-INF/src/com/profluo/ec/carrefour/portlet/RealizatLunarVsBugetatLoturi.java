package com.profluo.ec.carrefour.portlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.List;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.messaging.Message;
import com.liferay.portal.kernel.messaging.MessageListener;
import com.liferay.portal.kernel.messaging.MessageListenerException;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.util.bridges.mvc.MVCPortlet;
import com.profluo.ecm.model.db.DatabaseConnectionManager;
import com.profluo.ecm.model.db.DefCategory;
import com.profluo.ecm.model.db.DefCompanies;
import com.profluo.ecm.model.db.DefLot;
import com.profluo.ecm.model.db.DefStore;
import com.profluo.ecm.model.db.ListaInit;
import com.profluo.ecm.model.db.Raports;
import com.profluo.ecm.model.vo.DefNewProjVo;
import com.profluo.ecm.model.vo.DefStoreVo;
import com.profluo.ecm.model.vo.ListaInitVo;

/**
 * Portlet implementation class RealizatLunarVsBugetat
 */
public class RealizatLunarVsBugetatLoturi extends MVCPortlet implements MessageListener  {
	
	private final Log log = LogFactoryUtil.getLog("PROFLUO-" + RealizatLunarVsBugetatLoturi.class.getName());
	
	@Override
	public void receive(Message message) throws MessageListenerException {
		log.info("[PROFLUO] RealizatLunarVsBugetatLunarLoturi portlet - AUTOMATIC UPDATE TABLE");
		insertAllInformations("", "", "", "");
		insertTotalInfo("", "", "", "");
	}
	
	public void serveResource(ResourceRequest resourceRequest, ResourceResponse resourceResponse) throws IOException, PortletException {
		
		String action = ParamUtil.getString(resourceRequest, "action");
		if(action.equals("exportExcel")) {
			//TODO
			System.out.println("Da excel");
		}
		
	}
	
	public void filter(ActionRequest actionRequest,	ActionResponse actionResponse) {
		log.info("[PROFLUO] RealizatLunarVsBugetatLoturi portlet - filter()");
		String company = actionRequest.getParameter("societate").toString();
		String store = actionRequest.getParameter("store").toString();
		String year = actionRequest.getParameter("year").toString();
		String tipCapex = actionRequest.getParameter("initiativa").toString();
		insertAllInformations(company, store, year, tipCapex);
		//TODO insertTotalInfo
		insertTotalInfo(company, store, year, tipCapex);
	}
	
	public void updateInfo(ActionRequest actionRequest,	ActionResponse actionResponse) {
		log.info("[PROFLUO] RealizatLunarVsBugetatLoturi portlet - updateInfo()");
		insertAllInformations("", "", "", "");
		insertTotalInfo("", "", "", "");
	}
	
	protected static void insertAllInformations(String societate, String magazin, String year, String tipCapex){
		List<HashMap<String, Object>> allCompanies = DefCompanies.getCompanyById(societate);
		List<HashMap<String, Object>> allStores = DefStore.getStoreByCompanyAndInit(societate, magazin);
		//TODO get initiatives based on company and/or store
		List<HashMap<String, Object>> initiatives = ListaInit.getAll();
		
		List<HashMap<String, Object>> newProjects = DefNewProjVo.getInstance();
		List<HashMap<String, Object>> categories = DefCategory.getAll();
		List<HashMap<String, Object>> lots = DefLot.getAll();
		
		//Remove old informations
		Raports.truncateTable("raport_loturi");
		
		if (tipCapex.equals("") || tipCapex.equals("1")){
			List<HashMap<String,Object>> initiativeTotal = Raports.getMonthSums(2016, 0, 1, 0, 0, 0, 0);
			if(initiativeTotal.size() > 0 ){
				//Adding initiatives header
				Raports.insertRaportLoturi("1", "Initiative centralizate", 1, 0, 3, initiativeTotal, Raports.getBugetForInitiatives(2016, 0, 0, 0, 0, 0), 0, 0);
				for(HashMap<String,Object> company : allCompanies) {
					List<HashMap<String,Object>> companyTotal = Raports.getMonthSums(2016, Integer.parseInt(company.get("id").toString()), 1, 0, 0, 0, 0);
					if(companyTotal.size() > 0 ) {
						//Adding companies header
						Raports.insertRaportLoturi(company.get("entity_id").toString(), company.get("name").toString(), 1, 0, 3, companyTotal, Raports.getBugetForInitiatives(2016, Integer.parseInt(company.get("id").toString()), 0, 0, 0, 0), 0, 0);
						for(HashMap<String,Object> initiativa : initiatives) {
							List<HashMap<String,Object>> oneInitTotal = Raports.getMonthSums(2016, Integer.parseInt(company.get("id").toString()), 1, Integer.parseInt(initiativa.get("id").toString()), 0, 0, 0);
							if(oneInitTotal.size() > 0 ) {
								//Adding individual initiative
								Raports.insertRaportLoturi(initiativa.get("code").toString(), initiativa.get("name").toString(), 1, Integer.parseInt(initiativa.get("id").toString()), 3, oneInitTotal, Raports.getBugetForInitiatives(2016, Integer.parseInt(company.get("id").toString()), Integer.parseInt(initiativa.get("id").toString()), 0, 0, 0), 0, 0);
								for(HashMap<String, Object> store : allStores) {
									List<HashMap<String,Object>> storeTotal = Raports.getMonthSums(2016, Integer.parseInt(company.get("id").toString()), 1, Integer.parseInt(initiativa.get("id").toString()), Integer.parseInt(store.get("id").toString()), 0, 0);
									if(storeTotal.size() > 0 ) {
										//adding individual store
										Raports.insertRaportLoturi(store.get("store_id").toString(), store.get("store_name").toString(), 1, Integer.parseInt(store.get("id").toString()), 3, storeTotal, Raports.getBugetForInitiatives(2016, Integer.parseInt(company.get("id").toString()), Integer.parseInt(initiativa.get("id").toString()), Integer.parseInt(store.get("id").toString()), 0, 0), 0, 0);
										for(HashMap<String, Object> cat :categories){
											List<HashMap<String,Object>> catTotal = Raports.getMonthSums(2016, Integer.parseInt(company.get("id").toString()), 1, Integer.parseInt(initiativa.get("id").toString()), Integer.parseInt(store.get("id").toString()), Integer.parseInt(cat.get("id").toString()), 0);
											if (catTotal.size() > 0){
												//adding individual lot
												Raports.insertRaportLoturi(cat.get("code").toString(), cat.get("name").toString(), 1, Integer.parseInt(store.get("id").toString()), 3, catTotal, Raports.getBugetForInitiatives(2016, Integer.parseInt(company.get("id").toString()), Integer.parseInt(initiativa.get("id").toString()), Integer.parseInt(store.get("id").toString()), Integer.parseInt(cat.get("id").toString()), 0), Integer.parseInt(cat.get("id").toString()), 0);
												for (HashMap<String, Object> lot : lots){
													List<HashMap<String,Object>> lotTotal = Raports.getMonthSums(2016, Integer.parseInt(company.get("id").toString()), 1, Integer.parseInt(initiativa.get("id").toString()), Integer.parseInt(store.get("id").toString()), Integer.parseInt(cat.get("id").toString()), Integer.parseInt(lot.get("id").toString()));
													if (lotTotal.size() > 0){
														Raports.insertRaportLoturi(lot.get("code").toString(), lot.get("name").toString(), 1, Integer.parseInt(store.get("id").toString()), 3, lotTotal, 
																Raports.getBugetForInitiatives(2016, Integer.parseInt(company.get("id").toString()), Integer.parseInt(initiativa.get("id").toString()), 
																		Integer.parseInt(store.get("id").toString()), Integer.parseInt(cat.get("id").toString()), Integer.parseInt(lot.get("id").toString())),
																		Integer.parseInt(cat.get("id").toString()), Integer.parseInt(lot.get("id").toString()));
													}
												}
											}
										}
									}
								}
							}
							
						}
					}
				}
			}
		}
		if (tipCapex.equals("") || tipCapex.equals("2")){
			List<HashMap<String,Object>> newPrjTotal = Raports.getMonthSums(2016, 0, 2, 0, 0, 0, 0);
			if(newPrjTotal.size() > 0 ) {
				//Adding new projects header
				Raports.insertRaportLoturi("2", "Proiecte noi", 2, 0, 3, newPrjTotal, Raports.getBugetForNewPrj(2016, 0, 0, 0, 0), 0, 0);
				for(HashMap<String, Object> company : allCompanies) {
					List<HashMap<String,Object>> companyTotal = Raports.getMonthSums(2016, Integer.parseInt(company.get("id").toString()), 2, 0, 0, 0, 0);
					if(companyTotal.size() > 0 ) {
						//Adding companies header
						Raports.insertRaportLoturi(company.get("entity_id").toString(), company.get("name").toString(), 2, 0, 3, companyTotal, Raports.getBugetForNewPrj(2016, Integer.parseInt(company.get("id").toString()), 0, 0, 0), 0, 0);
						for(HashMap<String,Object> newPrj : newProjects) {
							List<HashMap<String,Object>> oneProject = Raports.getMonthSums(2016, Integer.parseInt(company.get("id").toString()), 2, Integer.parseInt(newPrj.get("id").toString()), 0, 0, 0);
							if(oneProject.size() > 0 ) {
								//Adding individual new project
								Raports.insertRaportLoturi(newPrj.get("store_id").toString(), newPrj.get("name").toString(), 2, Integer.parseInt(newPrj.get("id").toString()), 3, oneProject, Raports.getBugetForNewPrj(2016, Integer.parseInt(company.get("id").toString()),  Integer.parseInt(newPrj.get("id").toString()), 0, 0), 0, 0);
								for(HashMap<String, Object> cat :categories){
									List<HashMap<String,Object>> catTotal = Raports.getMonthSums(2016, Integer.parseInt(company.get("id").toString()), 2, Integer.parseInt(newPrj.get("id").toString()), 0, Integer.parseInt(cat.get("id").toString()), 0);
									if (catTotal.size() > 0){
										//adding individual lot
										Raports.insertRaportLoturi(cat.get("code").toString(), cat.get("name").toString(), 1, Integer.parseInt(newPrj.get("id").toString()), 3, catTotal, Raports.getBugetForNewPrj(2016, Integer.parseInt(company.get("id").toString()), Integer.parseInt(newPrj.get("id").toString()), Integer.parseInt(cat.get("id").toString()), 0), Integer.parseInt(cat.get("id").toString()), 0);
										for (HashMap<String, Object> lot : lots){
											List<HashMap<String,Object>> lotTotal = Raports.getMonthSums(2016, Integer.parseInt(company.get("id").toString()), 2, Integer.parseInt(newPrj.get("id").toString()), 0, Integer.parseInt(cat.get("id").toString()), Integer.parseInt(lot.get("id").toString()));
											if (lotTotal.size() > 0){
												Raports.insertRaportLoturi(lot.get("code").toString(), lot.get("name").toString(), 1, Integer.parseInt(newPrj.get("id").toString()), 3, lotTotal, Raports.getBugetForNewPrj(2016, Integer.parseInt(company.get("id").toString()), Integer.parseInt(newPrj.get("id").toString()), Integer.parseInt(cat.get("id").toString()), Integer.parseInt(lot.get("id").toString())),
														Integer.parseInt(cat.get("id").toString()), Integer.parseInt(lot.get("id").toString()));
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
		if (tipCapex.equals("") || tipCapex.equals("0")){
			List<HashMap<String,Object>> storeTotal = Raports.getMonthSums(2016, 0, 0, 0, 0, 0, 0);
			if(storeTotal.size() > 0 ) {
				//Adding capex diverse header
				Raports.insertRaportLoturi("3", "Capex diverse", 0, 0, 1, storeTotal, Raports.getBugetForCapexDiverse(2016, 0, 0, 0, 0), 0, 0);
				for(HashMap<String, Object> company : allCompanies) {
					List<HashMap<String,Object>> companyTotal = Raports.getMonthSums(2016, Integer.parseInt(company.get("id").toString()), 0, 0, 0, 0, 0);
					if(companyTotal.size() > 0 ) {
						//Adding companies header
						Raports.insertRaportLoturi(company.get("entity_id").toString(), company.get("name").toString(), 0, 0, 1, companyTotal, Raports.getBugetForCapexDiverse(2016, Integer.parseInt(company.get("id").toString()), 0, 0, 0), 0, 0);
						for(HashMap<String,Object> store : allStores) {
							List<HashMap<String,Object>> oneStoreTotal = Raports.getMonthSums(2016, Integer.parseInt(company.get("id").toString()), 0, Integer.parseInt(store.get("id").toString()), 0, 0, 0);
							if(oneStoreTotal.size() > 0 ) {
								//Adding individual store
								Raports.insertRaportLoturi(store.get("store_id").toString(), store.get("store_name").toString(), 0, Integer.parseInt(store.get("id").toString()), 3, oneStoreTotal, Raports.getBugetForCapexDiverse(2016, Integer.parseInt(company.get("id").toString()), Integer.parseInt(store.get("id").toString()), 0, 0), 0, 0);
								for(HashMap<String, Object> cat : categories){
									List<HashMap<String,Object>> catTotal = Raports.getMonthSums(2016, Integer.parseInt(company.get("id").toString()), 0, Integer.parseInt(store.get("id").toString()), 0, Integer.parseInt(cat.get("id").toString()), 0);
									if (catTotal.size() > 0){
										//adding individual lot
										Raports.insertRaportLoturi(cat.get("code").toString(), cat.get("name").toString(), 1, Integer.parseInt(store.get("id").toString()), 3, catTotal, Raports.getBugetForCapexDiverse(2016, Integer.parseInt(company.get("id").toString()), Integer.parseInt(store.get("id").toString()), Integer.parseInt(cat.get("id").toString()), 0), 
												Integer.parseInt(cat.get("id").toString()), 0);
										for (HashMap<String, Object> lot : lots){
											List<HashMap<String,Object>> lotTotal = Raports.getMonthSums(2016, Integer.parseInt(company.get("id").toString()), 0, Integer.parseInt(store.get("id").toString()), 0, Integer.parseInt(cat.get("id").toString()), Integer.parseInt(lot.get("id").toString()));
											if (lotTotal.size() > 0){
												Raports.insertRaportLoturi(lot.get("code").toString(), lot.get("name").toString(), 1, Integer.parseInt(store.get("id").toString()), 3, lotTotal, Raports.getBugetForCapexDiverse(2016, Integer.parseInt(company.get("id").toString()), Integer.parseInt(store.get("id").toString()), Integer.parseInt(cat.get("id").toString()), Integer.parseInt(lot.get("id").toString())), 
														Integer.parseInt(cat.get("id").toString()), Integer.parseInt(lot.get("id").toString()));
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
	}
	
	protected static void insertTotalInfo(String company, String store, String year, String init){
		//TODO tine cont de filtre
		//Remove old informations
		Raports.truncateTable("realizat_bugetat_total");
		
		List<HashMap<String,Object>> totalIt = Raports.getMonthSumsForTotal(2016, true, false);
		if(totalIt.size() > 0) {
			Raports.insertRaportTotal("IT", totalIt, Raports.getItTotal(2016));
		}
		List<HashMap<String,Object>> totalProperty = Raports.getMonthSumsForTotal(2016, false, true);
		if(totalProperty.size() > 0) {
			Raports.insertRaportTotal("PROPERTY", totalProperty, Raports.getPropertyTotal(2016));
		}
		Raports.insertActiveTotal("raport_loturi");
	}
	
	@Override
	public void render(RenderRequest renderRequest, RenderResponse renderResponse) throws PortletException, IOException {
		log.info("[PROFLUO] RealizatLunarVsBugetatLoturi portlet - render()");
		super.render(renderRequest, renderResponse);
	}
}
