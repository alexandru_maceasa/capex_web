package com.profluo.ec.carrefour.portlet;

import java.io.IOException;

import javax.portlet.PortletException;
import javax.portlet.PortletPreferences;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.servlet.http.HttpServletRequest;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.model.User;
import com.liferay.portal.service.UserServiceUtil;
import com.liferay.portal.theme.ThemeDisplay;
import com.liferay.portal.util.PortalUtil;
import com.liferay.portlet.PortletPreferencesFactoryUtil;
import com.liferay.util.bridges.mvc.MVCPortlet;
import com.profluo.ecm.flows.InvoiceStages;

/**
 * Portlet implementation class AsteaptaValidare
 */
public class AsteaptaValidare extends MVCPortlet {
	/*
	 * Generic log instance.
	 */
	private final Log log = LogFactoryUtil.getLog("PROFLUO-" + MVCPortlet.class.getName());
	
	/**
	 * Getting information about portlet initialization.
	 */
   @Override
   public void init() throws PortletException {
   	log.info("[PROFLUO] Initialize Asteapta validare aprobatori portlet.");
   	super.init();
   }
	/**
	 * 
	 */
	@Override
	public void render(RenderRequest renderRequest, RenderResponse renderResponse) throws PortletException, IOException {
		HttpServletRequest request = PortalUtil.getHttpServletRequest(renderRequest);
		String p_p_id = PortalUtil.getOriginalServletRequest(request).getParameter("p_p_id");
		String path = PortalUtil.getOriginalServletRequest(request).getParameter("_" + p_p_id + "_mvcPath");
   	
		try {
	    	ThemeDisplay themeDisplay = ((ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY));
	    	String portletId = themeDisplay.getPortletDisplay().getId();
	    	PortletPreferences portletSetup = PortletPreferencesFactoryUtil.getLayoutPortletSetup(themeDisplay.getLayout(), portletId);
	    	String portletCustomTitle = themeDisplay.getPortletDisplay().getTitle();
	    	portletCustomTitle = portletSetup.getValue("portletSetupTitle_" + themeDisplay.getLanguageId(), portletCustomTitle);
	    	
	    	User user = UserServiceUtil.getUserById(Long.parseLong(renderRequest.getRemoteUser()));
	    	
	        ThemeDisplay td  = (ThemeDisplay) renderRequest.getAttribute(WebKeys.THEME_DISPLAY);
	        log.info("[PROFLUO] Asteapta validare aprobatori portlet - render()");
	        log.info("[PROFLUO] User full name: " + td.getUser().getFullName());
	        log.info("[PROFLUO] Portlet Title: " + portletCustomTitle);
	        
	    	int id_user 			= (int) user.getUserId();
	    	log.info("[PROFLUO] User id " + id_user);
	    	//set on request the sql statement type, stage and page
	    	
	    	String page = InvoiceStages.getPageLayout(td.getURLCurrent());
	    	
	    	renderRequest.setAttribute("page-layout", page);
			
			log.info("[PROFLUO] PAGE LAYOUT : " + page);
			log.info("[PROFLUO] PAGE  : " + td.getURLCurrent());
	    	if (path == null || path.equals("view.jsp")) {
	    		log.info("[PROFLUO] Asteapta validare : LISTING render()");

	    	}
	   	} catch (Exception e) {
	   		e.printStackTrace();
	   	}
	   	super.render(renderRequest, renderResponse);
	} 

}
