package com.profluo.ec.carrefour.portlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.List;

import javax.portlet.PortletException;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.util.bridges.mvc.MVCPortlet;
import com.profluo.ecm.model.db.DatabaseConnectionManager;
import com.profluo.ecm.model.db.InvoiceHeader;
import com.profluo.ecm.utils.ExcelUtil;

/**
 * Portlet implementation class RaportFacturiInregistrate
 */
public class RaportFacturiInregistrate extends MVCPortlet {
	
	@SuppressWarnings("unchecked")
	public void serveResource(ResourceRequest resourceRequest, ResourceResponse resourceResponse) throws IOException, PortletException {
		
		String action = ParamUtil.getString(resourceRequest, "action");
	
	 if (action.equals("next") || action.equals("prev") || action.equals("last") || action.equals("first") || action.equals("filter")) {
		
		PrintWriter writer = resourceResponse.getWriter();
		// read AJAX parameters used in filtering and pagination
		int start = Integer.parseInt(ParamUtil.getString(resourceRequest, "start").toString());
		int count = Integer.parseInt(ParamUtil.getString(resourceRequest, "count").toString());
		int total = Integer.parseInt(ParamUtil.getString(resourceRequest, "total").toString());
		String factNo = ParamUtil.getString(resourceRequest, "factNo").toString();
		String supplier = ParamUtil.getString(resourceRequest, "supplier").toString();
		String societate = ParamUtil.getString(resourceRequest, "societate").toString();
		String Magazin = ParamUtil.getString(resourceRequest, "Magazin").toString();
		String dataFactura = ParamUtil.getString(resourceRequest, "dataFactura").toString();
		String codInitiativa = ParamUtil.getString(resourceRequest, "codInitiativa").toString();
		String Initiativa = ParamUtil.getString(resourceRequest, "Initiativa").toString();
		String valoareCuTVA = ParamUtil.getString(resourceRequest, "valoareCuTVA").toString();
		String anInceput = ParamUtil.getString(resourceRequest, "start_date_an").toString();
		String lunaInceput = ParamUtil.getString(resourceRequest, "start_date_luna").toString();
		String anSfarsit = ParamUtil.getString(resourceRequest, "end_date_an").toString();
		String lunaSfarsit = ParamUtil.getString(resourceRequest, "end_date_luna").toString();
		

		
		List<HashMap<String, Object>> facturiInregistrate = null;
		try {
			facturiInregistrate = InvoiceHeader.getFacturiInregistrate(start, count, factNo, supplier, societate, dataFactura, codInitiativa, Initiativa, valoareCuTVA, Magazin, anInceput, lunaInceput, anSfarsit, lunaSfarsit);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		total = InvoiceHeader.getFacturiInregistrateCount(factNo, supplier, societate, dataFactura, codInitiativa, Initiativa, valoareCuTVA, Magazin, anInceput, lunaInceput, anSfarsit, lunaSfarsit);

		JSONArray jsonInvoices = new JSONArray();
		if (facturiInregistrate != null) {
			jsonInvoices = DatabaseConnectionManager.convertListToJson(facturiInregistrate);
		}
		JSONObject jsonResponse = new JSONObject();

		jsonResponse.put("action", action);
		jsonResponse.put("start", start);
		jsonResponse.put("count", count);
		jsonResponse.put("total", total);
		jsonResponse.put("values", jsonInvoices);
		writer.print(jsonResponse.toJSONString());
	 	}
	 	else if (action.equals("exportExcel")) {
	 		
	 		int start = 0;
			int count = 0;
			String factNo = ParamUtil.getString(resourceRequest, "factNo").toString();
			String supplier = ParamUtil.getString(resourceRequest, "furnizor").toString();
			String societate = ParamUtil.getString(resourceRequest, "societate").toString();
			String magazin = ParamUtil.getString(resourceRequest, "Magazin").toString();
			String dataFactura = ParamUtil.getString(resourceRequest, "dataFactura").toString();
			String codInitiativa = ParamUtil.getString(resourceRequest, "codInitiativa").toString();
			String Initiativa = ParamUtil.getString(resourceRequest, "Initiativa").toString();
			String valoareCuTVA = ParamUtil.getString(resourceRequest, "valoareCuTVA").toString();
			String anInceput = ParamUtil.getString(resourceRequest, "start_date_an_param").toString();
			String lunaInceput = ParamUtil.getString(resourceRequest, "start_date_luna_param").toString();
			String anSfarsit = ParamUtil.getString(resourceRequest, "end_date_an_param").toString();
			String lunaSfarsit = ParamUtil.getString(resourceRequest, "end_date_luna_param").toString();
			
	 		
	 		List<HashMap<String, Object>> facturiInregistrate = null;
			try {
				facturiInregistrate = InvoiceHeader.getFacturiInregistrate(start, count, factNo, supplier, societate, dataFactura, codInitiativa, Initiativa, valoareCuTVA, magazin, anInceput, lunaInceput, anSfarsit, lunaSfarsit);
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			String[][] header = new String[23][];
			for (int i = 0; i < 23; i++) {
				header[i] = new String[2];
			}

			header[0][0] = "name";
			header[0][1] = "Societate";
			header[1][0] = "Magazin";
			header[1][1] = "Magazin";
			header[2][0] = "reg_date";
			header[2][1] = "Data Inregistrare";
			header[3][0] = "Furnizor";
			header[3][1] = "Furnizor";
			header[4][0] = "inv_number";
			header[4][1] = "Numar Factura";			
			header[5][0] = "inv_date";
			header[5][1] = "Data factura";
			header[6][0] = "Numar_Inventar";
			header[6][1] = "Numar_Inventar";
			header[7][0] = "Denumire_Articol";
			header[7][1] = "Denumire Articol";
			header[8][0] = "total_no_vat_ron";
			header[8][1] = "Valoare fara TVA";
			header[9][0] = "total_with_vat_ron";
			header[9][1] = "Valoare cu TVA";			
			header[10][0] = "warranty";
			header[10][1] = "Garantie";			
			header[11][0] = "Id_IAS";
			header[11][1] = "Id IAS";
			header[12][0] = "Cod_IFRS";
			header[12][1] = "Cod IFRS";			
			header[13][0] = "Durata_Amortizare_IAS";
			header[13][1] = "Durata Amortizare IAS";
			header[14][0] = "Durata_Amortizare_IFRS";
			header[14][1] = "Durata Amortizare IFRS";
			header[15][0] = "Tip_Inregistrare";
			header[15][1] = "Tip Inregistrare";
			header[16][0] = "Actiune_MF";
			header[16][1] = "Actiune MF";
			header[17][0] = "Gestiune";
			header[17][1] = "Gestiune";
			header[18][0] = "Cont_Imob_Generale";
			header[18][1] = "Cont Imob Generale";
			header[19][0] = "Cod_Initiativa";
			header[19][1] = "Cod Initiativa";
			header[20][0] = "Initiativa";
			header[20][1] = "Initiativa";
			header[21][0] = "lunaContabila";
			header[21][1] = "Luna Contabila";
			header[22][0] = "anContabil";
			header[22][1] = "An Contabil";

			try {
				ExcelUtil.getExcel(facturiInregistrate, header, "Raport_Facturi_Inregistrate", resourceResponse);
			} catch (IOException e) {
				e.printStackTrace();
			} 
		
	 
	 	}
	}
}
