package com.profluo.ec.carrefour.portlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletException;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.servlet.SessionMessages;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.model.User;
import com.liferay.portal.service.UserServiceUtil;
import com.liferay.util.bridges.mvc.MVCPortlet;
import com.profluo.ecm.model.db.DatabaseConnectionManager;
import com.profluo.ecm.model.db.GenericDBStatements;
import com.profluo.ecm.model.db.ModificareTipInregistrareDB;
import com.profluo.ecm.utils.ExcelUtil;

/**
 * Portlet implementation class ModificareTipInregistrare
 */
public class ModificareTipInregistrare extends MVCPortlet {
	
	/**
	 * Generic log instance.
	 */
	private final Log log = LogFactoryUtil.getLog("PROFLUO-" + Invoices.class.getName());

	
	public void modifyTipInreg(ActionRequest request, ActionResponse response){
		String supplierName = ParamUtil.getString(request, "filterSupplierName");
		String supplierCode = ParamUtil.getString(request, "filterSupplierCode");
		String company= ParamUtil.getString(request, "filterCompany");
		String documentType = ParamUtil.getString(request, "filterDocumentType");
		
		System.out.println(supplierName + " - > "  + supplierCode + " - > " + company + " - > " + documentType);
		
		List<HashMap<String,Object>> allDocs = ModificareTipInregistrareDB.getAllInfoWithLimit(0, 500000000, company,
				supplierCode, supplierName, documentType);
		boolean allAreGood = true;
		for(int i = 0; i < allDocs.size(); i++ ) {
			if (allDocs.get(i).get("document") == null || allDocs.get(i).get("document").toString().equals("")) {
				allAreGood = false;
			}
		}
		
		if(allAreGood) {
			User user = null;
			try {
				user =  UserServiceUtil.getUserById(Long.parseLong(request.getRemoteUser()));
			} catch (NumberFormatException | PortalException | SystemException e) {
				e.printStackTrace();
			}
			//separate record types
			String[][] fields = new String[1][2];
			fields[0][0] = "id_tip_inreg_final";
			
			String[][] fieldsHistory = new String[3][2];
			fieldsHistory[0][0] = "id_store_line";
			fieldsHistory[1][0] = "type";
			fieldsHistory[2][0] = "id_user";
			fieldsHistory[2][1] = String.valueOf(user.getUserId());
			String table = "";
			ArrayList<String> columns = new ArrayList<String>();
			columns.add("id");
			ArrayList<String> values = new ArrayList<String>();
			int result = 0;
			int resultHistory = 0; 
			int counter = 0;
			List<String> invoices = new ArrayList<String>();
			List<String> vouchers = new ArrayList<String>();
			List<String> work = new ArrayList<String>();
			List<String> cashexp = new ArrayList<String>();
			for (HashMap<String, Object> doc : allDocs){
				if (doc.get("id_tip_inreg").toString().equals("1")){
					fields[0][1] = "2";
				} else {
					fields[0][1] = "4";
				}
	
				fieldsHistory[0][1] = doc.get("id_record").toString();
				values.add(doc.get("id_record").toString());
				if (doc.get("docType").toString().equals("1")){
					invoices.add(doc.get("id_record").toString());
					table = "voucher_line";
					//tip inregistrare Obiect de inventar in functiune
					result = GenericDBStatements.updateEntry(fields, table, new ArrayList<String>(), columns, values);
					fieldsHistory[1][1] = "1";
					resultHistory = GenericDBStatements.insertEntry(fieldsHistory, "export_treceri_functiune", new ArrayList<String>());
					counter++;
				} else if (doc.get("docType").toString().equals("2")){
					table = "working_equipment_store_line";
					work.add(doc.get("id_record").toString());
					//tip inregistrare Obiect de inventar in functiune
					result = GenericDBStatements.updateEntry(fields, table, new ArrayList<String>(), columns, values);
					fieldsHistory[1][1] = "2";
					resultHistory = GenericDBStatements.insertEntry(fieldsHistory, "export_treceri_functiune", new ArrayList<String>());
					counter++;
				} else if (doc.get("docType").toString().equals("0")){
					table = "invoice_store_line";
					vouchers.add(doc.get("id_record").toString());
					//tip inregistrare Obiect de inventar in functiune
					result = GenericDBStatements.updateEntry(fields, table, new ArrayList<String>(), columns, values);
					fieldsHistory[1][1] = "0";
					resultHistory = GenericDBStatements.insertEntry(fieldsHistory, "export_treceri_functiune", new ArrayList<String>());
					counter++;
				} else if(doc.get("docType").toString().equals("3")){
					table = "cashexp_item_store_lines";
					cashexp.add(doc.get("id_record").toString());
					//tip inregistrare Obiect de inventar in functiune
					result = GenericDBStatements.updateEntry(fields, table, new ArrayList<String>(), columns, values);
					fieldsHistory[1][1] = "3";
					resultHistory = GenericDBStatements.insertEntry(fieldsHistory, "export_treceri_functiune", new ArrayList<String>());
					counter++;
				}
				values.clear();
				if(result <= 0){
					SessionErrors.add(request, "update_nok");
					log.info("[PROFLUO] Modificare tip inregistrare - update failed");
				}
				if(resultHistory <= 0){
					SessionErrors.add(request, "update_nok");
					log.info("[PROFLUO] Modificare tip inregistrare - update history failed (table export_treceri functiune)");
				}
			}
			System.out.println("invoices = " + invoices);
			System.out.println("vouchers = " + vouchers);
			System.out.println("wotk = " + work);
			System.out.println("wotk = " + cashexp);
			System.out.println("Controller result = " + counter);
			if (result > 0){
				SessionMessages.add(request, "update_ok");
			}
	} else {
		SessionErrors.add(request, "missing_doc");
	}
}
	@SuppressWarnings("unchecked")
	public void serveResource(ResourceRequest resourceRequest, ResourceResponse resourceResponse) throws IOException, PortletException {
		
		String action = ParamUtil.getString(resourceRequest, "action");
		if (action.equals("filter") || action.equals("next") || action.equals("prev") || action.equals("last") || action.equals("first")) {
			PrintWriter writer = resourceResponse.getWriter();
			int start = Integer.parseInt(ParamUtil.getString(resourceRequest, "start").toString());
			int count = Integer.parseInt(ParamUtil.getString(resourceRequest, "count").toString());
			int total = Integer.parseInt(ParamUtil.getString(resourceRequest, "total").toString());
			String company = ParamUtil.getString(resourceRequest, "company_id").toString();
			String supplierCode = ParamUtil.getString(resourceRequest, "supplier_code").toString();
			String supplierName = ParamUtil.getString(resourceRequest, "supplier_name").toString();
			String documentType = ParamUtil.getString(resourceRequest, "documentType").toString();
			List<HashMap<String, Object>> allLines = null;
			
			System.out.println(company + " -> " + supplierCode + " -> " + supplierName + " -> " + documentType);

			allLines = ModificareTipInregistrareDB.getAllInfoWithLimit(start, count, company, supplierCode, supplierName, documentType);
			total = ModificareTipInregistrareDB.getInventoryTotal(company, supplierCode, supplierName, documentType);
			JSONArray jsonLines = new JSONArray();
			if (allLines != null) {
				jsonLines = DatabaseConnectionManager.convertListToJson(allLines);
			}
			JSONObject jsonResponse = new JSONObject();
			jsonResponse.put("total", total);
			jsonResponse.put("values", jsonLines);
			writer.print(jsonResponse.toJSONString());
		} else if (action.equals("setDocument")){
			PrintWriter writer = resourceResponse.getWriter();
			String company = ParamUtil.getString(resourceRequest, "company_id").toString();
			String supplierCode = ParamUtil.getString(resourceRequest, "supplier_code").toString();
			String supplierName = ParamUtil.getString(resourceRequest, "supplier_name").toString();
			String documentType = ParamUtil.getString(resourceRequest, "documentType").toString();
			String documentToSet = ParamUtil.getString(resourceRequest, "doc").toString();
			List<HashMap<String, Object>> allDocs = null;
			//separate record types
			String[][] fields = new String[1][2];
			fields[0][0] = "document";
			fields[0][1] = documentToSet;
			ArrayList<String> columns = new ArrayList<String>();
			columns.add("id_store_line");
			ArrayList<String> values = new ArrayList<String>();
			String table = "";
			
			allDocs = ModificareTipInregistrareDB.getAllInfoWithLimit(0, 50000000, company, supplierCode, supplierName, documentType);
			for (HashMap<String, Object> doc : allDocs){
				values.add(doc.get("id_record").toString());
				if (doc.get("docType").toString().equals("1")){
					table = "voucher_line_inv";
					//tip inregistrare Obiect de inventar in functiune
					GenericDBStatements.updateEntry(fields, table, new ArrayList<String>(), columns, values);
				} else if (doc.get("docType").toString().equals("2")){
					table = "working_equipment_store_line_inv";
					//tip inregistrare Obiect de inventar in functiune
					GenericDBStatements.updateEntry(fields, table, new ArrayList<String>(), columns, values);
				} else if (doc.get("docType").toString().equals("0")){
					table = "invoice_store_line_inv";
					//tip inregistrare Obiect de inventar in functiune
					GenericDBStatements.updateEntry(fields, table, new ArrayList<String>(), columns, values);
				} else if(doc.get("docType").toString().equals("3")){
					table = "cashexp_item_store_lines_inv";
					//tip inregistrare Obiect de inventar in functiune
					GenericDBStatements.updateEntry(fields, table, new ArrayList<String>(), columns, values);
				}
				values.clear();
			}
			

			allDocs = ModificareTipInregistrareDB.getAllInfoWithLimit(0, 50000000, company, supplierCode, supplierName, documentType);
			int total = ModificareTipInregistrareDB.getInventoryTotal(company, supplierCode, supplierName, documentType);
			JSONArray jsonLines = new JSONArray();
			if (allDocs != null) {
				jsonLines = DatabaseConnectionManager.convertListToJson(allDocs);
			}
			JSONObject jsonResponse = new JSONObject();
			jsonResponse.put("total", total);
			jsonResponse.put("values", jsonLines);
			writer.print(jsonResponse.toJSONString());
		} else if (action.equals("exportExcel")) {
			int company =  ParamUtil.getInteger(resourceRequest, "company");
			
			List<HashMap<String,Object>> list = ModificareTipInregistrareDB.getAllForExcel(company);
			
			String [][] header = new String [14][2];
			header[0][0] = "entity_id";
			header[0][1] = "Cod societate";
			header[1][0] = "inventory_no";
			header[1][1] = "Numar inventar";
			header[2][0] = "pif_date";
			header[2][1] = "Data punerii in functiune";
			header[3][0] = "document";
			header[3][1] = "Documentul";
			header[4][0] = "explicatie";
			header[4][1] = "Explicatie Operatiuni";
			header[5][0] = "data_operatie";
			header[5][1] = "Data Operatie";
			header[6][0] = "val_operatie";
			header[6][1] = "Valoare Operatie";
			header[7][0] = "cod_grupa_ias";
			header[7][1] = "Cod Grupa IAS";
			header[8][0] = "cod_grupa_ifrs";
			header[8][1] = "Cod Grupa IFRS";
			header[9][0] = "cod_sucursala";
			header[9][1] = "Cod sucursala";
			header[10][0] = "tip_obiect";
			header[10][1] = "Tip Obiect";
			header[11][0] = "stare";
			header[11][1] = "Stare";
			header[12][0] = "description";
			header[12][1] = "Denumire nr. inventar";
			header[13][0] = "stare_finala";
			header[13][1] = "Stare finala";
			
			ExcelUtil.getExcel(list, header, "Raport_Tip_Inregistrare", resourceResponse);
			
			ModificareTipInregistrareDB.updateOptimalStatus(company);
		}
	}


}
