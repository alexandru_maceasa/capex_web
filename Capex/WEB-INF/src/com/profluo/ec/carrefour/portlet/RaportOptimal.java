package com.profluo.ec.carrefour.portlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.List;

import javax.portlet.PortletException;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.util.bridges.mvc.MVCPortlet;
import com.profluo.ecm.model.db.DatabaseConnectionManager;
import com.profluo.ecm.model.vo.StatusFacturiBD;
import com.profluo.ecm.utils.ExcelUtil;

/**
 * Portlet implementation class RaportOptimal
 */
public class RaportOptimal extends MVCPortlet {
	
	@SuppressWarnings("unchecked")
	public void serveResource(ResourceRequest resourceRequest, ResourceResponse resourceResponse) throws IOException, PortletException {
		
		String action = ParamUtil.getString(resourceRequest, "action");
		if (action.equals("exportExcel")) {
			String fromDate = ParamUtil.getString(resourceRequest, "from_date");
			String toDate = ParamUtil.getString(resourceRequest, "to_date");
			String exportType = ParamUtil.getString(resourceRequest, "export_type");
			
			if(exportType.equals("exportFacturi")){
				List<HashMap<String,Object>> allInvoicesFromOptimal = StatusFacturiBD.getAllInvoicesFromOptimalByDate(fromDate, toDate);
				allInvoicesFromOptimal.addAll(StatusFacturiBD.getAllVouchersFromOptimalByDate(fromDate, toDate));
				String[][] header = new String[28][];
				for (int i = 0; i < 28; i++) {
					header[i] = new String[2];
				}
				header[0][0] = "nrFactura";
				header[0][1] = "Numar factura";
				header[1][0] = "dataFactura";
				header[1][1] = "Data factura";
				header[2][0] = "codSocietate";
				header[2][1] = "Cod societate";
				header[3][0] = "codMagazin";
				header[3][1] = "Cod centru de cost";
				header[4][0] = "raion";
				header[4][1] = "Raion";
				header[5][0] = "codFurnizor";
				header[5][1] = "Cod furnizor";
				header[6][0] = "denumire";
				header[6][1] = "Denumire";
				header[7][0] = "produs";
				header[7][1] = "Produs";
				header[8][0] = "codProdus";
				header[8][1] = "Cod produs";
				header[9][0] = "valAchizite";
				header[9][1] = "Valoare achizitie";
				header[10][0] = "tipInregistrare";
				header[10][1] = "Tip inregistrare";
				header[11][0] = "actiuneMF";
				header[11][1] = "Actiune MF";
				header[12][0] = "gestiune";
				header[12][1] = "Gestiune";
				header[13][0] = "clasificareIT";
				header[13][1] = "Clasificare";
				header[14][0] = "nrInventar";
				header[14][1] = "Numar inventar";
				header[15][0] = "grupaRAS";
				header[15][1] = "Cod grupa clasificare";
				header[16][0] = "grupaIFRS";
				header[16][1] = "Cod grupa clasificare IFRS";
				header[17][0] = "durataRAS";
				header[17][1] = "Durata amortizare";
				header[18][0] = "durataIFRS";
				header[18][1] = "Durata amortizare IFRS";
				header[19][0] = "dataPIF";
				header[19][1] = "Data PIF";
				header[20][0] = "numeInitiativa";
				header[20][1] = "Initiativa";
				header[21][0] = "createdDate";
				header[21][1] = "Data de creeare";
				header[22][0] = "dpi";
				header[22][1] = "DPI";
				header[23][0] = "numarContract";
				header[23][1] = "Numar contract";
				header[24][0] = "contIas";
				header[24][1] = "cont";
				header[25][0] = "codeLot";
				header[25][1] = "Codul lotului";
				header[26][0] = "lotName";
				header[26][1] = "Denumirea lotului";
				header[27][0] = "tipDocument";
				header[27][1] = "Tip Document";
				
				try {
					ExcelUtil.getExcel(allInvoicesFromOptimal, header, "Raport_Optimal", resourceResponse);
				} catch (IOException e) {
					e.printStackTrace();
				}
			} else {
				List<HashMap<String,Object>> allVouchersFromKontan = StatusFacturiBD.getAllVouchersFromOptimalByDate(fromDate, toDate);
				String[][] header = new String[28][];
				for (int i = 0; i < 28; i++) {
					header[i] = new String[2];
				}
				header[0][0] = "nrVoucher";
				header[0][1] = "Numar Voucher";
				header[1][0] = "dataVoucher";
				header[1][1] = "Data Voucher";
				header[2][0] = "codSocietate";
				header[2][1] = "Cod societate";
				header[3][0] = "codMagazin";
				header[3][1] = "Cod centru de cost";
				header[4][0] = "raion";
				header[4][1] = "Raion";
				header[5][0] = "codFurnizor";
				header[5][1] = "Cod furnizor";
				header[6][0] = "denumire";
				header[6][1] = "Denumire";
				header[7][0] = "produs";
				header[7][1] = "Produs";
				header[8][0] = "codProdus";
				header[8][1] = "Cod produs";
				header[9][0] = "valAchizite";
				header[9][1] = "Valoare achizitie";
				header[10][0] = "tipInregistrare";
				header[10][1] = "Tip inregistrare";
				header[11][0] = "actiuneMF";
				header[11][1] = "Actiune MF";
				header[12][0] = "gestiune";
				header[12][1] = "Gestiune";
				header[13][0] = "clasificareIT";
				header[13][1] = "Clasificare";
				header[14][0] = "nrInventar";
				header[14][1] = "Numar inventar";
				header[15][0] = "grupaRAS";
				header[15][1] = "Cod grupa clasificare";
				header[16][0] = "grupaIFRS";
				header[16][1] = "Cod grupa clasificare IFRS";
				header[17][0] = "durataRAS";
				header[17][1] = "Durata amortizare";
				header[18][0] = "durataIFRS";
				header[18][1] = "Durata amortizare IFRS";
				header[19][0] = "dataPIF";
				header[19][1] = "Data PIF";
				header[20][0] = "numeInitiativa";
				header[20][1] = "Initiativa";
				header[21][0] = "createdDate";
				header[21][1] = "Data de creeare";
				header[22][0] = "dpi";
				header[22][1] = "DPI";
				header[23][0] = "numarContract";
				header[23][1] = "Numar contract";
				header[24][0] = "contIas";
				header[24][1] = "cont";
				header[25][0] = "codeLot";
				header[25][1] = "Codul lotului";
				header[26][0] = "lotName";
				header[26][1] = "Denumirea lotului";
				header[27][0] = "codSocietate";
				header[27][1] = "Compania";
			
				try {
					ExcelUtil.getExcel(allVouchersFromKontan, header, "Raport_Bonuri_Cesiune_Optimal", resourceResponse);
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		} else if (action.equals("next") || action.equals("prev") || action.equals("last") || action.equals("first") || action.equals("filter")) {
			PrintWriter writer = resourceResponse.getWriter();
			// read AJAX parameters used in filtering and pagination
			int start = Integer.parseInt(ParamUtil.getString(resourceRequest, "start").toString());
			int count = Integer.parseInt(ParamUtil.getString(resourceRequest, "count").toString());
			int total = Integer.parseInt(ParamUtil.getString(resourceRequest, "total").toString());
			String factNo = ParamUtil.getString(resourceRequest, "factNo").toString();
			String invNo = ParamUtil.getString(resourceRequest, "invNo").toString();
			String storeId = ParamUtil.getString(resourceRequest, "storeId").toString();
			
			System.out.println("invNo: " + invNo + " factNo: " + factNo + " storeId: " + storeId);
			
			List<HashMap<String, Object>> allInvoices = StatusFacturiBD.getAllInvoicesFromOptimal(start, count, factNo, invNo, storeId);
			allInvoices.addAll(StatusFacturiBD.getAllVouchersFromOptimal(start, count, factNo, invNo, storeId));
			allInvoices.addAll(StatusFacturiBD.getAllWorkingEquipmentFromOptimal(start, count, "", "", ""));
			total = StatusFacturiBD.getAllInvoicesFromOptimalCount(factNo, invNo, storeId);
			total += StatusFacturiBD.getAllVouchersFromOptimalCount(factNo, invNo, storeId);
			total += StatusFacturiBD.getAllWorkingEquipmentFromOptimalCount("", "", "");

			JSONArray jsonInvoices = new JSONArray();
			if (allInvoices != null) {
				jsonInvoices = DatabaseConnectionManager.convertListToJson(allInvoices);
			}
			JSONObject jsonResponse = new JSONObject();

			jsonResponse.put("action", action);
			jsonResponse.put("start", start);
			jsonResponse.put("count", count);
			jsonResponse.put("total", total);
			jsonResponse.put("values", jsonInvoices);
			writer.print(jsonResponse.toJSONString());
		}
	}
 

}
