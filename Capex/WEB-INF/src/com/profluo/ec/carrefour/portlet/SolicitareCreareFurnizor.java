package com.profluo.ec.carrefour.portlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.portlet.PortletException;
import javax.portlet.PortletPreferences;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;
import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.model.User;
import com.liferay.portal.service.UserServiceUtil;
import com.liferay.portal.theme.ThemeDisplay;
import com.liferay.portal.util.PortalUtil;
import com.liferay.portlet.PortletPreferencesFactoryUtil;
import com.liferay.util.bridges.mvc.MVCPortlet;
import com.profluo.ecm.controller.UserTeamIdUtils;
import com.profluo.ecm.flows.Links;
import com.profluo.ecm.flows.NotificationTemplates;
import com.profluo.ecm.flows.NotificationsSubject;
import com.profluo.ecm.flows.SupplierStages;
import com.profluo.ecm.model.db.DatabaseConnectionManager;
import com.profluo.ecm.model.db.DefSupplier;
import com.profluo.ecm.model.db.GenericDBStatements;
import com.profluo.ecm.model.db.UsersInformations;
import com.profluo.ecm.notifications.Notifications;

/**
 * Portlet implementation class SolicitareCreareFurnizor
 */
public class SolicitareCreareFurnizor extends MVCPortlet {
	/**
	 * List of column that are excluded on update
	 */
	private ArrayList<String> suppliersExcludes = null;
	/**
	 * The URL identifier of the current page
	 */
	private String currentPageURL = "";
	/**
	 * Generic log instance.
	 */
	private final Log log = LogFactoryUtil.getLog("PROFLUO-" + Invoices.class.getName());
	
	
	
	/**
	 * extrage valorile numerice dintr-un string
	 * @param src
	 * @return
	 */
	public static String extractDigits(String src) {
	    StringBuilder builder = new StringBuilder();
	    for (int i = 0; i < src.length(); i++) {
	        char c = src.charAt(i);
	        if (Character.isDigit(c)) {
	            builder.append(c);
	        }
	    }
	    return builder.toString();
	}
	/**
	 * Set data for insertion into suppliers_approvals
	 * 
	 * @param id
	 * @param user
	 * @return
	 */
	private String[][] setSuppliersApprovalsData(String id, User user){
		String[][] supplierApprovalsData = new String[3][2];
		supplierApprovalsData[0][0] = "id_supplier";
		supplierApprovalsData[0][1] = id;
		supplierApprovalsData[1][0] = "id_approver";
		supplierApprovalsData[1][1] = String.valueOf(user.getUserId());
		supplierApprovalsData[2][0] = "stage";
		supplierApprovalsData[2][1] = String.valueOf(SupplierStages.getStage(currentPageURL));
		
		return supplierApprovalsData;
	}
	
	/**
	 * 
	 */
	private void setSupplierExcludes() {
		//attributes to be ignored for table invoice_line
		ArrayList<String> excludes = new ArrayList<String>();
		String [] items = {};
		for(int i = 0; i < items.length; i++) {
			excludes.add(items[i]);
		}
		
		this.suppliersExcludes = excludes;
	}
	
	/**
	 * Update the stage for all selected suppliers
	 * @param ids
	 */
	private void updateGroupOfSuppliers(String ids, User user){
		System.out.println("ids : "+ids);
		String[][] arrFields = new String[1][2];
		//update where statement
		ArrayList<String> columns = new ArrayList<String>();
		ArrayList<String> values = new ArrayList<String>();
		//separate the ids
		String[] parts = ids.split(",");
		arrFields[0][0] = "stage";
		//get next stage
		arrFields[0][1] = String.valueOf(SupplierStages.getNextStage(this.currentPageURL));
		
		//data to insert into supplier_approvals
		String[][] supplierApprovalsData = new String[3][2];
		ArrayList<String> excludes = new ArrayList<String>();
		this.setSupplierExcludes();
		for (String s : parts){
			try{
				//where statement
				columns.add("id");
				//where statement values
				values.add(s);
	    		//update supplier
				GenericDBStatements.updateEntry(arrFields, "def_suppliers", this.suppliersExcludes, columns, values);
				//set data to insert into suppliers_approvals
				supplierApprovalsData = this.setSuppliersApprovalsData(s, user);
				//insert into suppliers_approvals
				GenericDBStatements.insertEntry(supplierApprovalsData, "suppliers_approvals", excludes);
	    	} catch (Exception e){
	    		e.printStackTrace();
	    	}
			//empty arrays that contain values for where statement
			columns.clear();
			values.clear();
		}
	}
	
	/**
	 * Update information for selected supplier
	 * 
	 * @param id
	 * @param resourceRequest
	 */
	private void updateSupplier(String id , ResourceRequest resourceRequest){
			//get update params
			String name = ParamUtil.getString(resourceRequest, "name");
	    	String cui = ParamUtil.getString(resourceRequest, "cui");
	    	String cuiint = SolicitareCreareFurnizor.extractDigits(cui);
	    	String email = ParamUtil.getString(resourceRequest, "email");
	    	String phone = ParamUtil.getString(resourceRequest, "phone");
	    	String reg_com = ParamUtil.getString(resourceRequest, "reg_com");
	    	String payment_code = ParamUtil.getString(resourceRequest, "payment_code");
	    	String ro = ParamUtil.getString(resourceRequest, "ro");
	    	String bank = ParamUtil.getString(resourceRequest, "bank");
	    	String bank_acc = ParamUtil.getString(resourceRequest, "bank_acc");
	    	String swift = ParamUtil.getString(resourceRequest, "swift");
	    	String id_company = ParamUtil.getString(resourceRequest, "id_company");
	    	String payment_term = ParamUtil.getString(resourceRequest, "payment_term");
	    	String supplier_type = ParamUtil.getString(resourceRequest, "supplier_type_db").equals("") ? "0" : ParamUtil.getString(resourceRequest, "supplier_type_db") ;
	    	String address = ParamUtil.getString(resourceRequest, "new_address");
	    	String tert_group = ParamUtil.getString(resourceRequest, "supplier_tert_group_popup");
	    	 	//set suppliers excludes
	    	this.setSupplierExcludes();
	    	
	    	// where statement
	    	ArrayList<String> columns = new ArrayList<String>();
	    	columns.add("id");
	    	//where statement values
	    	ArrayList<String> values = new ArrayList<String>();
	    	values.add(id);
	    	//set update data
	    	String[][] arrFields = new String[16][2];
	    	arrFields[0][0] = "name";
	    	arrFields[0][1] = name;
	    	arrFields[1][0] = "cui";
	    	arrFields[1][1] = cui;
	    	arrFields[2][0] = "email";
	    	arrFields[2][1] = email;
	    	arrFields[3][0] = "phone";
	    	arrFields[3][1] = phone;
	    	arrFields[4][0] = "bank";
	    	arrFields[4][1] = bank;
	    	arrFields[5][0] = "bank_acc";
	    	arrFields[5][1] = bank_acc;
	    	arrFields[6][0] = "swift";
	    	arrFields[6][1] = swift;
	    	arrFields[7][0] = "payment_code";
	    	arrFields[7][1] = payment_code;
	    	arrFields[8][0] = "RO";
	    	if (ro.equals("false")){
	    		System.out.println("bla");
	    		arrFields[8][1] = "";
	    	} else arrFields[8][1] = "RO";
	    	arrFields[9][0] = "reg_com";
	    	arrFields[9][1] = reg_com;
	    	arrFields[10][0] = "id_company";
	    	arrFields[10][1] = id_company;
	    	arrFields[11][0] = "payment_term";
	    	arrFields[11][1] = payment_term;
	    	arrFields[12][0] = "supplier_type";
	    	arrFields[12][1] = supplier_type;
	    	arrFields[13][0] = "address";
	    	arrFields[13][1] = address;
	    	arrFields[14][0] = "tert_group";
	    	arrFields[14][1] = tert_group;
	    	arrFields[15][0] = "cuiint";
	    	arrFields[15][1] = cuiint;
	    	
	    	try{
	    		//update supplier
				GenericDBStatements.updateEntry(arrFields, "def_suppliers", this.suppliersExcludes, columns, values);
	    	} catch (Exception e){
	    		e.printStackTrace();
	    	}
	    }
	
	 private void addNotification(List<String> supplierIds, User user) {
			int nextTeamId = UserTeamIdUtils.ACCOUNTING_DIRECTOR;
			List<HashMap<String,Object>> userDetails = UsersInformations.getUserDetailsByTeamId(nextTeamId);
			HashMap<String, Object> bodyDetails = new HashMap<String,Object>();
			
			String link = Links.allSuppliersRequestsForAccountingDirector;
			
			String notifBody = "";
			for(int i = 0; i < supplierIds.size(); i++){
				List<HashMap<String,Object>> supplierDetails = DefSupplier.getSupplierDetails(supplierIds.get(i));
				String supplierName = supplierDetails.get(0).get("name").toString();
				String supplierEmail = supplierDetails.get(0).get("email").toString();
				String supplierPhone = supplierDetails.get(0).get("phone").toString();
				String termenDePlata = supplierDetails.get(0).get("payment_term").toString();
				notifBody += "<tr><td style='border:1px solid black'>" + user.getFullName() + "</td><td style='border:1px solid black'>"
						+ supplierName + "</td><td style='border:1px solid black'>Adresa</td><td style='border:1px solid black'>" + supplierEmail
						+ "</td><td style='border:1px solid black'>" + supplierPhone + "</td><td style='border:1px solid black'>" + termenDePlata
						+ "</td><td style='border:1px solid black'><a href='" + link + "'>Link</a></td></tr>";
			}
			
				bodyDetails = setInsertParameters(NotificationTemplates.CONTA_CREARE_FURNIZOR, "", "", notifBody, "0", "S", "1", user, link);
				Notifications.insertNotification("", userDetails, bodyDetails, NotificationsSubject.CREEARE_FURNIZOR);
		}
	 
	 private HashMap<String, Object> setInsertParameters (String id_template, String to_email, String to_name, String body, String status, 
				String type, String doc_type, User user, String link){
			
			HashMap<String, Object> bodyDetails = new HashMap<String,Object>();
			
			bodyDetails.put("id_template", id_template);
			bodyDetails.put("sender_email", user.getEmailAddress());
			bodyDetails.put("sender_name", user.getFullName());
			bodyDetails.put("body", body);
			String currentTime = new Timestamp(new Date().getTime()).toString();
			bodyDetails.put("time_to_send", currentTime);
			bodyDetails.put("time_of_send", currentTime);
			bodyDetails.put("status", status);
			bodyDetails.put("type", type);
			bodyDetails.put("doc_type", doc_type);
			bodyDetails.put("created", currentTime);
			if(link.contains("url")){
				bodyDetails.put("link", link.substring(0, link.lastIndexOf("?")));
			} else {
				bodyDetails.put("link", link);
			}
			return bodyDetails;
		}
	 
	/**
     * Default AJAX method in the application framework.
     */
    @SuppressWarnings("unchecked")
	@Override
    public void serveResource(ResourceRequest resourceRequest, ResourceResponse resourceResponse) throws IOException, PortletException {
    	log.info("[AJAX] Solicitare creare furnizor");

    	String action = ParamUtil.getString(resourceRequest, "action");
    	log.info("[AJAX] action: " + action);
    	PrintWriter writer = resourceResponse.getWriter();
    	User user = null;
    	try{
    		user = UserServiceUtil.getUserById(Long.parseLong(resourceRequest.getRemoteUser()));
    	}catch (Exception e) {}
    	if (action.equals("editSupplier")) {
    		//get the ids of the suppliers that are to be updated
    		String id = ParamUtil.getString(resourceRequest, "id");
    		//get filter name
    		String filter_name = ParamUtil.getString(resourceRequest, "filter_name");
    		//update the stage for the selected supplier
    		this.updateSupplier(id, resourceRequest);
    		// repopulate listing
	    	List<HashMap<String,Object>> allSuppliers = null;
	    	//get all suppliers by stage
	    	try{
	    		allSuppliers = DefSupplier.getAllFiltered(0, 0, "", filter_name, SupplierStages.getStage(this.currentPageURL), SupplierStages.getPageLayout(this.currentPageURL));
	    	}catch (Exception e){}
	    	JSONArray jsonSuppliers = new JSONArray();
	    	if (allSuppliers != null) {
	    		jsonSuppliers = DatabaseConnectionManager.convertListToJson(allSuppliers);
	    	}

	    	ThemeDisplay td = ((ThemeDisplay) resourceRequest.getAttribute(WebKeys.THEME_DISPLAY));
			String page 	= td.getURLCurrent();
			
			//Daca nu e pe pagina aprobatorului final (directorul de contabilitate)
			if ( !page.contains("solicitare-aprobatori-creare-furnizor") ) {
		    	//add Notification
				List<String> supplierIds = new ArrayList<String>();
				supplierIds.add(id);
				addNotification(supplierIds, user);
				
			}
	    	// create JSON response
	    	JSONObject jsonResponse = new JSONObject();
			jsonResponse.put("values", jsonSuppliers);
			// send AJAX response
			writer.print(jsonResponse.toJSONString());
			
    	} else if (action.equals("updateSuppliers")){
    		//get the ids of the suppliers that are to be updated
    		String ids = ParamUtil.getString(resourceRequest, "ids");
    		//get filter name
    		String filter_name = ParamUtil.getString(resourceRequest, "filter_name");
    		//update the stage for every supplier in the selected group 
    		this.updateGroupOfSuppliers(ids, user);
    		// repopulate listing
	    	List<HashMap<String,Object>> allSuppliers = null;
    		//get all suppliers by stage
	    	try{
	    		allSuppliers = DefSupplier.getAllFiltered(0, 0, "", filter_name, SupplierStages.getStage(this.currentPageURL), SupplierStages.getPageLayout(this.currentPageURL));
	    	}catch (Exception e){}
	    	JSONArray jsonSuppliers = new JSONArray();
	    	if (allSuppliers != null) {
	    		jsonSuppliers = DatabaseConnectionManager.convertListToJson(allSuppliers);
	    	}
	    	
	    	ThemeDisplay td = ((ThemeDisplay) resourceRequest.getAttribute(WebKeys.THEME_DISPLAY));
			String page 	= td.getURLCurrent();
			
			if ( !page.contains("solicitare-aprobatori-creare-furnizor") ) {
		    	//add Notification
				if( ids.contains(",")) {
					String [] allSupplierIds = ids.split(",");
					List<String> supplierIds = new ArrayList<String>();
					for(int i = 0; i < allSupplierIds.length; i++){
						supplierIds.add(allSupplierIds[i]);
					}
					addNotification(supplierIds, user);
				} else {
					List<String> supplierIds = new ArrayList<String>();
					supplierIds.add(ids);
					addNotification(supplierIds, user);
				}
			}
	    	
	    	// create JSON response
	    	JSONObject jsonResponse = new JSONObject();
			jsonResponse.put("values", jsonSuppliers);
			// send AJAX response
			writer.print(jsonResponse.toJSONString());
			
			String body = "Buna ziua, <br/> <br/> Va informam ca a fost aprobata solicitarea de creare furnizor in Kontan. <br/> Va rugam sa "
					+ " integrati fisierul generat. <br/> <br/> O zi buna! ";
			
			HashMap<String, Object> bodyDetails = new HashMap<String,Object>();
			bodyDetails.put("id_template", NotificationTemplates.CREARE_FURNIZORI);
			bodyDetails.put("sender_email", user.getEmailAddress());
			bodyDetails.put("sender_name", user.getFullName());
			bodyDetails.put("body", body);
			String currentTime = new Timestamp(new Date().getTime()).toString();
			bodyDetails.put("time_to_send", currentTime);
			bodyDetails.put("time_of_send", currentTime);
			bodyDetails.put("status", "0");
			bodyDetails.put("type", "S");
			bodyDetails.put("doc_type", "1");
			bodyDetails.put("created", currentTime);
			bodyDetails.put("link", "");

			List<HashMap<String,Object>> userDetails = new ArrayList<HashMap<String,Object>> ();
			HashMap<String, Object> hash = new HashMap<String, Object>();
			hash.put("email", "magdalena_draghici@carrefour.com");
			hash.put("first_name", "Magda");
			hash.put("last_name", "Draghici");
			userDetails.add(hash);
			Notifications.insertNotification("", userDetails, bodyDetails, NotificationsSubject.CREEARE_FURNIZOR);
			
    	}
        
    	//super.serveResource(resourceRequest, resourceResponse);
    } 
    
    /**
	 * 
	 */
	@Override
	public void render(RenderRequest renderRequest, RenderResponse renderResponse) throws PortletException, IOException {
		HttpServletRequest request = PortalUtil.getHttpServletRequest(renderRequest);
    	String p_p_id = PortalUtil.getOriginalServletRequest(request).getParameter("p_p_id");
    	String path = PortalUtil.getOriginalServletRequest(request).getParameter("_" + p_p_id + "_mvcPath");
    	
    	
    	
    	try {
	    	ThemeDisplay themeDisplay = ((ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY));
	    	String portletId = themeDisplay.getPortletDisplay().getId();
	    	PortletPreferences portletSetup = PortletPreferencesFactoryUtil.getLayoutPortletSetup(themeDisplay.getLayout(), portletId);
	    	String portletCustomTitle = themeDisplay.getPortletDisplay().getTitle();
	    	portletCustomTitle = portletSetup.getValue("portletSetupTitle_" + themeDisplay.getLanguageId(), portletCustomTitle);
	    	User user = UserServiceUtil.getUserById(Long.parseLong(renderRequest.getRemoteUser()));
	    	
	        ThemeDisplay td  = (ThemeDisplay) renderRequest.getAttribute(WebKeys.THEME_DISPLAY);
	        //retrieve the URL identifier of the current page
	        this.currentPageURL = SupplierStages.getPageLayout(td.getURLCurrent());
	        //set on request the URL identifier of the current page
	        renderRequest.setAttribute("page", this.currentPageURL);
	        //set the stage on the request
	        renderRequest.setAttribute("stage", SupplierStages.getStage(this.currentPageURL));
	        
	        log.info("[PROFLUO] Solicitare Creare Furnizor portlet - render()");
	        log.info("[PROFLUO] User full name: " + td.getUser().getFullName());
	    	
	    	int id_user 			= (int) user.getUserId();
	    	log.info("[PROFLUO] User id " + id_user);
	    	
	    	if(UserTeamIdUtils.belongsToTeam(id_user, UserTeamIdUtils.ACCOUNTING_DIRECTOR )) {
	    		renderRequest.setAttribute("pagina", "validare-furnizori");
	    	}
	    	if (path == null || path.equals("view.jsp")) {
	    		log.info("[PROFLUO] LISTING SolicitareCreareFurnizor render()");

	    	}
    	} catch (Exception e) {
    		e.printStackTrace();
    	}
    	
	    super.render(renderRequest, renderResponse);
	} 
}
