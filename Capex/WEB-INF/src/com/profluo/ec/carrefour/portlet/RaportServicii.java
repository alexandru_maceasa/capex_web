package com.profluo.ec.carrefour.portlet;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;

import javax.portlet.PortletException;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;

import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.util.bridges.mvc.MVCPortlet;
import com.profluo.ecm.model.db.InvoiceHistory;
import com.profluo.ecm.utils.ExcelUtil;

/**
 * Portlet implementation class RaportServicii
 */
public class RaportServicii extends MVCPortlet {

	public void serveResource(ResourceRequest resourceRequest, ResourceResponse resourceResponse) throws IOException, PortletException {
		
		String action = ParamUtil.getString(resourceRequest, "action");
		if (action.equals("exportExcel")) {
			String fromDate = ParamUtil.getString(resourceRequest, "from_date");
			String toDate = ParamUtil.getString(resourceRequest, "to_date");
			List<HashMap<String,Object>> allServicesLines = InvoiceHistory.getAllServicesLines(fromDate, toDate);
			String[][] header = new String[9][2];
			header[0][0] = "nrFactura";
			header[0][1] = "Numar factura";
			header[1][0] = "dataFactura";
			header[1][1] = "Data factura";
			header[2][0] = "societate";
			header[2][1] = "Societate";
			header[3][0] = "furnizor";
			header[3][1] = "Furnizor";
			header[4][0] = "produs";
			header[4][1] = "Produs";
			header[5][0] = "cantitate";
			header[5][1] = "Cantitate";
			header[6][0] = "oldValue";
			header[6][1] = "Valoare Neta veche";
			header[7][0] = "newValue";
			header[7][1] = "Valoare Neta noua";
			header[8][0] = "magazin";
			header[8][1] = "Magazin";
			try {
				ExcelUtil.getExcel(allServicesLines, header, "Raport_Servicii", resourceResponse);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

}
