package com.profluo.ec.carrefour.portlet;

import java.util.HashMap;
import java.util.List;

import com.liferay.util.bridges.mvc.MVCPortlet;
import com.profluo.ecm.model.db.DefCompanies;
import com.profluo.ecm.model.vo.DefLotVo;
import com.profluo.ecm.model.vo.DefNewProjVo;
import com.profluo.ecm.model.vo.DefStoreVo;
import com.profluo.ecm.model.vo.ListaInitVo;

/**
 * Portlet implementation class RaportPeLoturi
 */
public class RaportPeLoturi extends MVCPortlet {
	
	protected static void insertAllInformations(){
		List<HashMap<String,Object>> allCompanies = DefCompanies.getAll();
		List<HashMap<String,Object>> allStores = DefStoreVo.getInstance();
		List<HashMap<String,Object>> newProjects = DefNewProjVo.getInstance();
		List<HashMap<String,Object>> initiatives = ListaInitVo.getInstance();
		List<HashMap<String,Object>> allLots = DefLotVo.getInstance();
		
	}
}
