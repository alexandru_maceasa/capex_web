package com.profluo.ec.carrefour.portlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.List;

import javax.portlet.PortletException;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.util.bridges.mvc.MVCPortlet;
import com.profluo.ecm.model.db.CashExpense;
import com.profluo.ecm.model.db.DPIHeader;
import com.profluo.ecm.model.db.DatabaseConnectionManager;
import com.profluo.ecm.model.db.GenericDBStatements;

/**
 * Portlet implementation class DeconturiInregistrate
 */
public class DeconturiInregistrate extends MVCPortlet {
	
	/**
	 * Default AJAX method in the application framework.
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void serveResource(ResourceRequest resourceRequest, ResourceResponse resourceResponse) throws IOException, PortletException {
		
		String action = ParamUtil.getString(resourceRequest, "action");
		PrintWriter writer = resourceResponse.getWriter();
		if (action.equals("filter") || action.equals("next") || action.equals("prev") || action.equals("last") || action.equals("first")) {
			// read AJAX parameters used in filtering and pagination
			int start = Integer.parseInt(ParamUtil.getString(resourceRequest, "start").toString());
			int count = Integer.parseInt(ParamUtil.getString(resourceRequest, "count").toString());
			int total = Integer.parseInt(ParamUtil.getString(resourceRequest, "total").toString());
			String number  = ParamUtil.getString(resourceRequest, "number").toString();
			String supplier = ParamUtil.getString(resourceRequest, "supplier").toString();
			
			List<HashMap<String,Object>> allcahsExp = CashExpense.getAllRegistered(start, count, number, supplier);
			total = CashExpense.getAllRegisteredCount("", "");
			
			JSONArray jsonInvoices = new JSONArray();
			if (allcahsExp != null) {
				jsonInvoices = DatabaseConnectionManager.convertListToJson(allcahsExp);
			}
			JSONObject jsonResponse = new JSONObject();

			jsonResponse.put("action", action);
			jsonResponse.put("start", start);
			jsonResponse.put("count", count);
			jsonResponse.put("total", total);
			jsonResponse.put("values", jsonInvoices);
			writer.print(jsonResponse.toJSONString());
		} else if (action.equals("showDpi")) {
			String idDpi = ParamUtil.getString(resourceRequest, "dpi");
			// get DPI header info based on DPI id
			List<HashMap<String, Object>> oneDpi = null;
			
			System.out.println(idDpi);

			// oneDpi = GenericDBStatements.getAllByFieldName("dpi_header",  "id", idDpi);
			oneDpi = DPIHeader.getAllByFieldName("id", idDpi);

			JSONArray jsonDpiHeader = new JSONArray();
			if (oneDpi != null) {
				jsonDpiHeader = DatabaseConnectionManager.convertListToJson(oneDpi);
			}

			// get DPI lines info based on DPI id
			List<HashMap<String, Object>> dpiLines = null;

			dpiLines = GenericDBStatements.getAllByFieldName("dpi_line", "id_dpi", idDpi);

			JSONArray jsonDpiLines = new JSONArray();
			if (dpiLines != null) {
				jsonDpiLines = DatabaseConnectionManager.convertListToJson(dpiLines);
			}

			// create JSON response
			JSONObject jsonResponse = new JSONObject();
			jsonResponse.put("header", jsonDpiHeader);
			jsonResponse.put("lines", jsonDpiLines);
			// send AJAX response
			writer.print(jsonResponse.toJSONString());
		} else if (action.equals("getInventoryDetailsCash")){
			String idStoreLine = ParamUtil.getString(resourceRequest, "idCashexp").toString();
			System.out.println(idStoreLine);
			List<HashMap<String,Object>> inventoryDetails = CashExpense.getInventoryDetailsByIdStoreLine(idStoreLine);
			JSONArray jsonInvNo = new JSONArray();
			if (inventoryDetails != null) {
				for (int i = 0; i < inventoryDetails.size(); i++) {
					JSONObject jsonLine = new JSONObject();
					jsonLine.put("nr_inv", inventoryDetails.get(i).get("inventory_no").toString());
					jsonLine.put("data_pif", inventoryDetails.get(i).get("pif_date").toString());
					jsonLine.put("receptie", inventoryDetails.get(i).get("reception").toString());
					jsonInvNo.add(jsonLine);
				}
				writer.print(jsonInvNo.toJSONString());
			} else {
				writer.print("");
			}
			
		}
	}
 

}
