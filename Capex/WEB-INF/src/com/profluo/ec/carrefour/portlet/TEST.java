package com.profluo.ec.carrefour.portlet;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;

import javax.portlet.PortletException;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;

import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.util.bridges.mvc.MVCPortlet;
import com.profluo.ecm.model.db.Raports;
import com.profluo.ecm.utils.ExcelUtil;

/**
 * Portlet implementation class TEST
 */
public class TEST extends MVCPortlet {
	
	public void serveResource(ResourceRequest resourceRequest, ResourceResponse resourceResponse) throws IOException, PortletException {	
		String action = ParamUtil.getString(resourceRequest, "action");
		 if(action.equals("exportExcel")) {
				List<HashMap<String,Object>> list = Raports.getRaport(0, 50000000, 1);
				list.addAll(Raports.getTableFooter(true, 1));
				list.addAll(Raports.getSecondRaport());
				
				String firstHeader [][] = new String [17][2];
				firstHeader[0][0] = "code";
				firstHeader[0][1] = "Cod";
				firstHeader[1][0] = "denumire";
				firstHeader[1][1] = "Denumire";
				firstHeader[2][0] = "buget_an";
				firstHeader[2][1] = "Buget An";
				firstHeader[3][0] = "actual_ian";
				firstHeader[3][1] = "Actual Ianuarie";
				firstHeader[4][0] = "actual_feb";
				firstHeader[4][1] = "Actual Februarie";
				firstHeader[5][0] = "actual_mar";
				firstHeader[5][1] = "Actual Martie";
				firstHeader[6][0] = "actual_apr";
				firstHeader[6][1] = "Actual Aprilie";
				firstHeader[7][0] = "actual_mai";
				firstHeader[7][1] = "Actual Mai";
				firstHeader[8][0] = "actual_iun";
				firstHeader[8][1] = "Actual Iunie";
				firstHeader[9][0] = "actual_iul";
				firstHeader[9][1] = "Actual Iulie";
				firstHeader[10][0] = "actual_aug";
				firstHeader[10][1] = "Actual August";
				firstHeader[11][0] = "actual_sept";
				firstHeader[11][1] = "Actual Septembrie";
				firstHeader[12][0] = "actual_oct";
				firstHeader[12][1] = "Actual Octombrie";
				firstHeader[13][0] = "actual_noi";
				firstHeader[13][1] = "Actual Noiembrie";
				firstHeader[14][0] = "actual_dec";
				firstHeader[14][1] = "Actual Decembrie";
				firstHeader[15][0] = "total";
				firstHeader[15][1] = "Total";
				firstHeader[16][0] = "diferenta";
				firstHeader[16][1] = "Actual vs Bugetat";
				
				ExcelUtil.getTwoSheetExcel(list, list, firstHeader, firstHeader, "Raport realizatLunar vs Bugetat", resourceResponse);
			}
		
	}
 

}
