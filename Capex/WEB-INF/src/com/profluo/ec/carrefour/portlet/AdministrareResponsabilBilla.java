package com.profluo.ec.carrefour.portlet;

import java.io.IOException;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import com.liferay.util.bridges.mvc.MVCPortlet;
import com.profluo.ecm.model.db.Administrare;
import com.profluo.ecm.model.db.UsersInformations;

/**
 * Portlet implementation class AdministrareResponsabilBilla
 */
public class AdministrareResponsabilBilla extends MVCPortlet {
 
	
	public void salveazaResponsabil(ActionRequest request, ActionResponse response) {
		int userId = Integer.parseInt(request.getParameter("id_user"));
		Administrare.updateRule("BILLA_ADMINISTRATOR", String.valueOf(userId));
	}
	
	@Override
	public void render(RenderRequest renderRequest, RenderResponse renderResponse) throws PortletException, IOException {
		renderRequest.setAttribute("userId", UsersInformations.getBillaOwner());
		super.render(renderRequest, renderResponse);
	}
}
