package com.profluo.ec.carrefour.portlet;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;

import javax.portlet.PortletException;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;

import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.util.bridges.mvc.MVCPortlet;
import com.profluo.ecm.utils.ExcelUtil;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.profluo.ecm.model.db.RaportSituatieInregistrari;

/**
 * Portlet implementation class raport_situatie_inreg_facturi
 */
public class Raport_situatie_inreg_facturi extends MVCPortlet {
	

	private final Log log = LogFactoryUtil.getLog("PROFLUO-" + Raport_situatie_inreg_facturi.class.getName());

	@Override
	public void serveResource(ResourceRequest resourceRequest, ResourceResponse resourceResponse) throws IOException, PortletException {
		log.info("[AJAX] raport_situatie_inreg_facturi");

		String action = ParamUtil.getString(resourceRequest, "action");
		
				
		if (action.equals("exportExcel")) {
			String lunaStart = ParamUtil.getString(resourceRequest, "start_date_luna");
			if(Integer.parseInt(lunaStart) < 10) {
				lunaStart = "0" + lunaStart;
			}
			
			String lunaEnd = ParamUtil.getString(resourceRequest, "end_date_luna");
			if(Integer.parseInt(lunaEnd) < 10) {
				lunaEnd = "0" + lunaEnd;
			}
			
			String fromDate = ParamUtil.getString(resourceRequest, "start_date_an") + "-" + lunaStart + "-01";
			String toDate =  ParamUtil.getString(resourceRequest, "end_date_an") + "-" + lunaEnd + "-01";
			System.out.println(fromDate + " si data de sfarsit:" + toDate+ "si action:"+action);
			List<HashMap<String, Object>> allCategories = RaportSituatieInregistrari.getAllInvoices(fromDate, toDate);
						
			String[][] header = new String[9][];
			for (int i = 0; i < 9; i++) {
				header[i] = new String[2];
			}
			
			header[0][0] = "societate";
			header[0][1] = "Societate";
			header[1][0] = "magazin";
			header[1][1] = "Magazin";
			header[2][0] = "contabil";
			header[2][1] = "Asistent contabil";
			header[3][0] = "numar_factura";
			header[3][1] = "Numar factura";
			header[4][0] = "data_factura";
			header[4][1] = "Data factura";
			header[5][0] = "furnizor";
			header[5][1] = "Furnizor";
			header[6][0] = "nr_linii_factura";
			header[6][1] = "Numar linii";
			header[7][0] = "reg_date";
			header[7][1] = "Data inregistrare";
			header[8][0] = "tip";
			header[8][1] = "Tip factura";
					
			try {
				ExcelUtil.getExcel(allCategories, header, "Raport_situatie_facturi",
						resourceResponse);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
}
