package com.profluo.ec.carrefour.portlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.List;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletException;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.util.bridges.mvc.MVCPortlet;
import com.profluo.ecm.model.db.DatabaseConnectionManager;
import com.profluo.ecm.model.db.Raports;

/**
 * Portlet implementation class RaportYTDvsBugetat
 */
public class RaportYTDvsBugetat extends MVCPortlet {
	
	private final Log log = LogFactoryUtil.getLog("PROFLUO-" + RaportYTDvsBugetat.class.getName());
 
	@SuppressWarnings("unchecked")
	public void serveResource(ResourceRequest resourceRequest, ResourceResponse resourceResponse) throws IOException, PortletException {
		
		String action = ParamUtil.getString(resourceRequest, "action");
		if (action.equals("filter") || action.equals("next") || action.equals("prev") || action.equals("last") || action.equals("first")) {
			PrintWriter writer = resourceResponse.getWriter();
			int start = Integer.parseInt(ParamUtil.getString(resourceRequest, "start").toString());
			int count = Integer.parseInt(ParamUtil.getString(resourceRequest, "count").toString());
			int total = Integer.parseInt(ParamUtil.getString(resourceRequest, "total").toString());
			List<HashMap<String, Object>> allLines = null;
			allLines = Raports.getRaport(start, count, 1);
			allLines.addAll(Raports.getTableFooter(true, 1));
			total = Raports.getRaportTotal();
			JSONArray jsonLines = new JSONArray();
			if (allLines != null) {
				jsonLines = DatabaseConnectionManager.convertListToJson(allLines);
			}
			JSONObject jsonResponse = new JSONObject();
			jsonResponse.put("action", action);
			jsonResponse.put("start", start);
			jsonResponse.put("count", count);
			jsonResponse.put("total", total);
			jsonResponse.put("values", jsonLines);
			writer.print(jsonResponse.toJSONString());
		} else if(action.equals("exportExcel")) {
			System.out.println("Da excel");
		}
	}
	
	public void updateInfo(ActionRequest actionRequest,	ActionResponse actionResponse) {
		log.info("[PROFLUO] RaportYTDvsBugetat portlet - updateInfo()");
		RealizatLunarVsBugetat.insertAllInformations();
		RealizatLunarVsBugetat.insertTotalInfo();
	}
}
