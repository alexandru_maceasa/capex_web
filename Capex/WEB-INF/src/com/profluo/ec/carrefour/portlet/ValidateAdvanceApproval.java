package com.profluo.ec.carrefour.portlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.List;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletConfig;
import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;
import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.portlet.LiferayPortletConfig;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.servlet.SessionMessages;
import com.liferay.portal.kernel.util.JavaConstants;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.model.User;
import com.liferay.portal.service.UserLocalServiceUtil;
import com.liferay.portal.service.UserServiceUtil;
import com.liferay.portal.util.PortalUtil;
import com.liferay.util.bridges.mvc.MVCPortlet;
import com.profluo.ecm.model.db.AdvanceRequest;
import com.profluo.ecm.model.db.DatabaseConnectionManager;
import com.profluo.ecm.model.db.GenericDBStatements;

/**
 * Portlet implementation class ValidateAdvanceApproval
 */
public class ValidateAdvanceApproval extends MVCPortlet {
	/**
	 * 
	 */
	private final Log log = LogFactoryUtil.getLog("PROFLUO-" + ValidateAdvanceApproval.class.getName());
	
    /**
     * @see MVCPortlet#MVCPortlet()
     */
    public ValidateAdvanceApproval() {
        super();
        log.info("[PROFLUO] Initialize ValidateAdvanceApproval portlet.");
    }
    
    /**
     * 
     */
    @SuppressWarnings("unchecked")
	@Override
    public void serveResource(ResourceRequest resourceRequest, ResourceResponse resourceResponse) throws IOException, PortletException {
    	log.info("[AJAX] Validate RequestAdvanceApproval");
    	String action = ParamUtil.getString(resourceRequest, "action");
    	log.info("[AJAX] action: " + action);
    	PrintWriter writer = resourceResponse.getWriter();
    	
    	if (action.equals("showDpi")) {
    		String idDpi = ParamUtil.getString(resourceRequest, "dpi");
    		// get DPI header info based on DPI id
    		List<HashMap<String,Object>> oneDpi = null;

    		oneDpi = GenericDBStatements.getAllByFieldName("dpi_header", "id", idDpi);

	    	JSONArray jsonDpiHeader = new JSONArray();
	    	if (oneDpi != null) {
	    		jsonDpiHeader = DatabaseConnectionManager.convertListToJson(oneDpi);
	    	}
	    	
	    	// get DPI lines info based on DPI id
	    	List<HashMap<String,Object>> dpiLines = null;
	    	
    		dpiLines = GenericDBStatements.getAllByFieldName("dpi_line", "id_dpi", idDpi);

	    	JSONArray jsonDpiLines = new JSONArray();
	    	if (dpiLines != null) {
	    		jsonDpiLines = DatabaseConnectionManager.convertListToJson(dpiLines);
	    	}
	    	
	    	// create JSON response
	    	JSONObject jsonResponse = new JSONObject();
			jsonResponse.put("header", jsonDpiHeader);
			jsonResponse.put("lines", jsonDpiLines);
			// send AJAX response
			writer.print(jsonResponse.toJSONString());
    	} else if (action.equals("filter") || action.equals("next") || action.equals("prev") || action.equals("last") || action.equals("first")) {
        	int stage = Integer.parseInt(ParamUtil.getString(resourceRequest, "stage"));
	    	int start = Integer.parseInt(ParamUtil.getString(resourceRequest, "start").toString());
	    	int count = Integer.parseInt(ParamUtil.getString(resourceRequest, "count").toString());
	    	int total = Integer.parseInt(ParamUtil.getString(resourceRequest, "total").toString());
	    	int companyId = Integer.parseInt(ParamUtil.getString(resourceRequest, "company_id").toString());
	    	int storeId = Integer.parseInt(ParamUtil.getString(resourceRequest, "store_id").toString());
	    	String voucherNo = ParamUtil.getString(resourceRequest, "voucher_no").toString();
	    	String voucherDate = ParamUtil.getString(resourceRequest, "voucher_date").toString();
	    	String supplierCode = ParamUtil.getString(resourceRequest, "supplier_code").toString();
	    	String supplierName = ParamUtil.getString(resourceRequest, "supplier_name").toString();
	    	int userId = 0;
	    	int id_store_aisle = 0;
	    	User user = null;
			try {
				user = UserServiceUtil.getUserById(Long.parseLong(resourceRequest.getRemoteUser()));
				userId = (int) user.getUserId();
				id_store_aisle = Integer.parseInt(user.getAddresses().get(0).getStreet3().toString());
			} catch (NumberFormatException | PortalException | SystemException e1) {
				e1.printStackTrace();
			}
	    	
	    	log.info("[AJAX] start: " + count);
	    	log.info("[AJAX] count: " + start);
	    	log.info("[AJAX] total: " + total);
	    	log.info("[AJAX] user: " + userId);
	    	
	    	resourceResponse.setProperty("test", "test_content");
	    	
	    	List<HashMap<String,Object>> allInvoices = null;
	    	
			log.info("Getting all!");
			allInvoices = AdvanceRequest.getAdvanceRequestAccounting(start, count, companyId, storeId, voucherNo,
					voucherDate, Integer.parseInt(supplierCode), supplierName);
			
			// add initiator to answer
			try {
    			for (int i = 0; i < allInvoices.size(); i++) {
    				/*int userA = 0;
    				User approver;
    				if(!allInvoices.get(i).get("approver").toString().equals("")){
    					userA = Integer.parseInt(allInvoices.get(i).get("approver").toString()); 
    		 			approver = UserLocalServiceUtil.getUserById(userA);
    				}*/
    				int solicitantId = Integer.parseInt(allInvoices.get(i).get("id_user").toString()); 
    	 			User solicitant = UserLocalServiceUtil.getUserById(solicitantId);
    	 			//solicitant.getJobTitle() - functie
    	 			//solicitant.getFullName() - solicitant
    	 			allInvoices.get(i).put("solicitant", solicitant.getFullName());
    	 			allInvoices.get(i).put("functie", solicitant.getJobTitle());
    			}
			} catch(Exception e) { }
			// if total is 0 we need to refresh the count
			if (total == 0) {
				total = AdvanceRequest.getAdvanceRequestAccountingCount(start, count, companyId, storeId, voucherNo,
						voucherDate, Integer.parseInt(supplierCode), supplierName);
			}
	    	
	    	
	    	JSONArray jsonInvoices = new JSONArray();
	    	if (allInvoices != null) {
	    		jsonInvoices = DatabaseConnectionManager.convertListToJson(allInvoices);
	    	}
	    	JSONObject jsonResponse = new JSONObject();
	    	
			jsonResponse.put("action", action);
			jsonResponse.put("start", start);
			jsonResponse.put("count", count);
			jsonResponse.put("total", total);
			jsonResponse.put("values", jsonInvoices);
			writer.print(jsonResponse.toJSONString());		
    	}
    }
    
    /**
     * 
     * @param request
     * @param response
     */
	public void updateStage(ActionRequest request, ActionResponse response) {
		PortletConfig portletConfig = (PortletConfig)request.getAttribute(JavaConstants.JAVAX_PORTLET_CONFIG);
	   	LiferayPortletConfig liferayPortletConfig = (LiferayPortletConfig) portletConfig;
	   	SessionMessages.add(request, liferayPortletConfig.getPortletId() + SessionMessages.KEY_SUFFIX_HIDE_DEFAULT_ERROR_MESSAGE);
	   	int acc=0;
		log.info("[PROFLUO] ValidateAdvanceApproval portlet - update stage()");
		try{
			String id = request.getParameter("idToUpdate");
			
			acc = Integer.parseInt(request.getParameter("cont_asociat_furn").toString());
			System.out.println("Cont furnizor: "+ acc);
			System.out.println("ID: "+id);
			//contabilitate-> finalizare
			int updated = AdvanceRequest.validateRequest(id , 6, acc);
			if (updated > 0) {
				System.out.println("DA");
	    		SessionMessages.add(request, "update_ok");
	    	} else {
	    		System.out.println("NU");
	    		SessionErrors.add(request, "update_nok");
	    		response.setRenderParameter("mvcPath", "/html/validateadvanceapproval/validate.jsp");
	    	}
		}catch (Exception e){
			System.out.println("Acoount : "+ acc);
			
		}
		
   }

	@Override
	/**
	 * 
	 */
	public void render(RenderRequest renderRequest, RenderResponse renderResponse) throws PortletException, IOException {
		HttpServletRequest request = PortalUtil.getHttpServletRequest(renderRequest);
    	String p_p_id = PortalUtil.getOriginalServletRequest(request).getParameter("p_p_id");
    	String path = PortalUtil.getOriginalServletRequest(request).getParameter("_" + p_p_id + "_mvcPath");
    	
		log.info("[PROFLUO] ValidateAdvanceApproval portlet - render()");
		log.info("[PROFLUO] Context Path: " + renderRequest.getContextPath());
    	log.info("[PROFLUO] Portlet ID	: " + p_p_id);
    	log.info("[PROFLUO] Portlet Path: " + path);
    	
    	if (path == null || path.endsWith("view.jsp")) {
    		log.info("[PROFLUO] LISTING render()");
    		
    	} else if (path.endsWith("validate.jsp")) {
    		log.info("[PROFLUO] Validare solicitare creare avans render()");
    		
    	}
		
	    super.render(renderRequest, renderResponse);
	}
}
