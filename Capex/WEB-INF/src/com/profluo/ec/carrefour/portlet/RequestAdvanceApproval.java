package com.profluo.ec.carrefour.portlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletConfig;
import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;
import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.portlet.LiferayPortletConfig;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.servlet.SessionMessages;
import com.liferay.portal.kernel.util.JavaConstants;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.model.Role;
import com.liferay.portal.model.Team;
import com.liferay.portal.model.User;
import com.liferay.portal.service.UserLocalServiceUtil;
import com.liferay.portal.service.UserServiceUtil;
import com.liferay.portal.util.PortalUtil;
import com.liferay.util.bridges.mvc.MVCPortlet;
import com.profluo.ecm.controller.ControllerUtils;
import com.profluo.ecm.controller.UserTeamIdUtils;
import com.profluo.ecm.flows.Links;
import com.profluo.ecm.flows.NotificationTemplates;
import com.profluo.ecm.flows.NotificationsSubject;
import com.profluo.ecm.model.db.AdvanceRequest;
import com.profluo.ecm.model.db.DPIHeader;
import com.profluo.ecm.model.db.DatabaseConnectionManager;
import com.profluo.ecm.model.db.DefCompanies;
import com.profluo.ecm.model.db.DefExchRates;
import com.profluo.ecm.model.db.DefSupplier;
import com.profluo.ecm.model.db.GeneralCodes;
import com.profluo.ecm.model.db.GenericDBStatements;
import com.profluo.ecm.model.db.InvoiceHeader;
import com.profluo.ecm.model.db.UsersInformations;
import com.profluo.ecm.notifications.Notifications;

/**
 * Portlet implementation class RequestAdvanceApproval
 */
public class RequestAdvanceApproval extends MVCPortlet {
	/**
	 * 
	 */
	private final Log log = LogFactoryUtil.getLog("PROFLUO-" + RequestAdvanceApproval.class.getName());
	private int supplier_code = 0;
	 /**
		 * List of parameters to be excluded from SQL suppliers insert.
	 */
	private ArrayList<String> suppliersExcludes = null;
	
	boolean[] alsoApprover = new boolean[3];
	boolean noNotifications = false;
	/**
     * @see MVCPortlet#MVCPortlet()
     */
    public RequestAdvanceApproval() {
        super();
        log.info("[PROFLUO] Initialize RequestAdvanceApproval portlet.");
    }
   
    private void setSupplierExcludes() {
		// attributes to be ignored for table invoice_line
		ArrayList<String> excludes = new ArrayList<String>();
		String[] items = {};
		for (int i = 0; i < items.length; i++) {
			excludes.add(items[i]);
		}

		this.suppliersExcludes = excludes;
	}
    
    private int createSupplier(ResourceRequest resourceRequest, User user) {
		String name = ParamUtil.getString(resourceRequest, "name");
		String cui = ParamUtil.getString(resourceRequest, "cui");
		String email = ParamUtil.getString(resourceRequest, "email");
		String phone = ParamUtil.getString(resourceRequest, "phone");
		String reg_com = ParamUtil.getString(resourceRequest, "reg_com");
		String payment_code = ParamUtil.getString(resourceRequest,
				"payment_code");
		String ro = ParamUtil.getString(resourceRequest, "ro");
		String bank = ParamUtil.getString(resourceRequest, "bank");
		String bank_acc = ParamUtil.getString(resourceRequest, "bank_acc");
		String swift = ParamUtil.getString(resourceRequest, "swift");
		String id_company = ParamUtil.getString(resourceRequest, "id_company");
		String payment_term = ParamUtil.getString(resourceRequest,
				"payment_term");
    	String supplier_type = ParamUtil.getString(resourceRequest, "supplier_type_db").equals("") ? "0" : ParamUtil.getString(resourceRequest, "supplier_type_db") ;
		// set suppliers excludes
		this.setSupplierExcludes();
		// init insert data
		String[][] arrFields = new String[16][2];
		arrFields[0][0] = "name";
		arrFields[0][1] = name;
		arrFields[1][0] = "cui";
		arrFields[1][1] = cui;
		arrFields[2][0] = "email";
		arrFields[2][1] = email;
		arrFields[3][0] = "phone";
		arrFields[3][1] = phone;
		arrFields[4][0] = "bank";
		arrFields[4][1] = bank;
		arrFields[5][0] = "bank_acc";
		arrFields[5][1] = bank_acc;
		arrFields[6][0] = "swift";
		arrFields[6][1] = swift;
		arrFields[7][0] = "payment_code";
		arrFields[7][1] = payment_code;
		arrFields[8][0] = "RO";
		if (ro.equals("false")) {
			arrFields[8][1] = "";
		} else
			arrFields[8][1] = "RO";
		arrFields[9][0] = "reg_com";
		arrFields[9][1] = reg_com;
		arrFields[10][0] = "supplier_code";
		try {
			supplier_code = InvoiceHeader.getMinSupplierCode();
		} catch (Exception e) {
		}

		arrFields[10][1] = String.valueOf(--supplier_code);
		arrFields[11][0] = "stage";
		arrFields[11][1] = "2";
		arrFields[12][0] = "id_company";
		arrFields[12][1] = id_company;
		arrFields[13][0] = "payment_term";
		arrFields[13][1] = payment_term;
		arrFields[14][0] = "supplier_type";
		arrFields[14][1] = supplier_type;
		arrFields[15][0] = "id_user";
		arrFields[15][1] = String.valueOf(user.getUserId());
		// result
		int resultedID = 0;
		try {
			resultedID = GenericDBStatements.insertEntry(arrFields,
					"def_suppliers", this.suppliersExcludes);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return resultedID;
	}
    
    @SuppressWarnings("unchecked")
	@Override
    public void serveResource(ResourceRequest resourceRequest, ResourceResponse resourceResponse) throws IOException, PortletException {
    	log.info("[AJAX] RequestAdvanceApproval");
    	
    	String action = ParamUtil.getString(resourceRequest, "action");
    	log.info("[AJAX] action : " + action);
    	PrintWriter writer = resourceResponse.getWriter();
    	
		User user = null;
    	try{
    		user = UserServiceUtil.getUserById(Long.parseLong(resourceRequest.getRemoteUser()));
    	}catch (Exception e) {}
    	//get all unpaid invoices from the selected supplier that have the due date > current date
    	if(action.equals("getUnpaidInvoices")){
    		String current_date = ParamUtil.getString(resourceRequest, "current_date").toString();
    		String supplier_name = ParamUtil.getString(resourceRequest, "supplier_name").toString();
    		String company_id = ParamUtil.getString(resourceRequest, "company_id").toString();
    		List<HashMap<String,Object>> unpaidInvoices = null;
    		
    		unpaidInvoices = AdvanceRequest.getUnpaidInvoicesFromSupplier(current_date, supplier_name, company_id);
    		
    		JSONArray jsonUnpaidInvoices = new JSONArray();
	    	if (unpaidInvoices != null) {
	    		jsonUnpaidInvoices = DatabaseConnectionManager.convertListToJson(unpaidInvoices);
	    	}
	    	JSONObject jsonResponse = new JSONObject();
			jsonResponse.put("unpaidInvoices", jsonUnpaidInvoices);
	    	// send AJAX response
			writer.print(jsonResponse.toJSONString());
    	} else if (action.equals("getExchangeRate")) {
    		String date = ParamUtil.getString(resourceRequest, "date").toString();
    		String selected_currency = ParamUtil.getString(resourceRequest, "currency_sel").toString();
    		if (!date.equals("")){
    			float exchange_rate = 0; 
    			try { 
    				exchange_rate = DefExchRates.getExchangeRate(selected_currency, date, 1);
    			} catch (Exception e) {}
    			// create JSON response
    	    	JSONObject jsonResponse = new JSONObject();
    			jsonResponse.put("exchange_rate", exchange_rate);
    	    	// send AJAX response
    			writer.print(jsonResponse.toJSONString());
    		}
    	} else if (action.equals("findSupplier")) {
    		String query = ParamUtil.getString(resourceRequest, "keywords");
    		String idCompany = ParamUtil.getString(resourceRequest, "id_company");
    		log.info("[AJAX] query: " + query);

    		writer.print(ControllerUtils.filterSupplierByName(query, Integer.parseInt(idCompany)).toJSONString());
    	} else if (action.equals("showDpi")) {
    		String idDpi = ParamUtil.getString(resourceRequest, "dpi");
    		// get DPI header info based on DPI id
    		List<HashMap<String,Object>> oneDpi = null;
    		
			//oneDpi = GenericDBStatements.getAllByFieldName("dpi_header", "id", idDpi);
			oneDpi = DPIHeader.getAllByFieldName("id", idDpi);
    		
	    	JSONArray jsonDpiHeader = new JSONArray();
	    	if (oneDpi != null) {
	    		jsonDpiHeader = DatabaseConnectionManager.convertListToJson(oneDpi);
	    	}
	    	
	    	// get DPI lines info based on DPI id
	    	List<HashMap<String,Object>> dpiLines = null;
	    	
    		dpiLines = GenericDBStatements.getAllByFieldName("dpi_line", "id_dpi", idDpi);

	    	JSONArray jsonDpiLines = new JSONArray();
	    	if (dpiLines != null) {
	    		jsonDpiLines = DatabaseConnectionManager.convertListToJson(dpiLines);
	    	}
	    	
	    	// create JSON response
	    	JSONObject jsonResponse = new JSONObject();
			jsonResponse.put("header", jsonDpiHeader);
			jsonResponse.put("lines", jsonDpiLines);
			// send AJAX response
			writer.print(jsonResponse.toJSONString());
    	} else if (action.equals("filterByUser") || action.equals("next") || action.equals("prev") || action.equals("last") || action.equals("first")) {
	    	int start = Integer.parseInt(ParamUtil.getString(resourceRequest, "start").toString());
	    	int count = Integer.parseInt(ParamUtil.getString(resourceRequest, "count").toString());
	    	int total = Integer.parseInt(ParamUtil.getString(resourceRequest, "total").toString());
	    	int userId = 0;
			userId = (int) user.getUserId();

	    	log.info("[AJAX] start: " + count);
	    	log.info("[AJAX] count: " + start);
	    	log.info("[AJAX] total: " + total);
	    	log.info("[AJAX] user: " + userId);
	    	
	    	resourceResponse.setProperty("test", "test_content");
	    	
	    	List<HashMap<String,Object>> allInvoices = null;
	    	
			log.info("Getting all!");
			allInvoices = AdvanceRequest.getAdvanceRequestByUserId(userId, start, count);
			// if total is 0 we need to refresh the count
			if (total == 0) {
				total = AdvanceRequest.getCountByUserId(userId);
			}
	    	
	    	
	    	// fill in missing fields
			int lastApproverId = 0;
			User lastApprover = null;
			for (int i = 0; i < allInvoices.size(); i++) {
				//System.out.println(i + " - " + allInvoices.get(i).get("categorie"));
				// set approval history
				lastApprover = null;
				if(!allInvoices.get(i).get("approver").toString().equals("")){
					lastApproverId = Integer.parseInt(allInvoices.get(i).get("approver").toString());
					try {
						lastApprover = UserLocalServiceUtil.getUserById(lastApproverId);
					} catch (Exception e) {}
				}
				if (lastApprover != null) {
					allInvoices.get(i).put("istoric", lastApprover.getFullName());
				} else {
					allInvoices.get(i).put("istoric", "");
				}
				
				// set capex details
				if ((Integer)allInvoices.get(i).get("categorie") == 1) {
					allInvoices.get(i).put("detalii_categ", allInvoices.get(i).get("initiativa"));
				} else if ((Integer)allInvoices.get(i).get("categorie") == 2) {
					allInvoices.get(i).put("detalii_categ", allInvoices.get(i).get("project"));
				} else {
					allInvoices.get(i).put("detalii_categ", "Capex Diverse");
				}
			}
	    	
	    	JSONArray jsonInvoices = new JSONArray();
	    	if (allInvoices != null) {
	    		jsonInvoices = DatabaseConnectionManager.convertListToJson(allInvoices);
	    	}
	    	JSONObject jsonResponse = new JSONObject();
	    	
			jsonResponse.put("action", action);
			jsonResponse.put("start", start);
			jsonResponse.put("count", count);
			jsonResponse.put("total", total);
			jsonResponse.put("values", jsonInvoices);
			writer.print(jsonResponse.toJSONString());		
    	} else if (action.equals("filterDpis")){
    		String storeId = ParamUtil.getString(resourceRequest, "storeId").toString();
    		// get DPI header info based on DPI id
    		List<HashMap<String,Object>> dpis = null;
    		
			//oneDpi = GenericDBStatements.getAllByFieldName("dpi_header", "id", idDpi);
			dpis = DPIHeader.getAllByStoreId(storeId);
    		
	    	JSONArray jsonDpis = new JSONArray();
	    	if (dpis != null) {
	    		jsonDpis = DatabaseConnectionManager.convertListToJson(dpis);
	    	}
	    	// create JSON response
	    	JSONObject jsonResponse = new JSONObject();
			jsonResponse.put("dpis", jsonDpis);
			// send AJAX response
			writer.print(jsonResponse.toJSONString());
    	} else if (action.equals("requestSupplierCreation")) {
			String supplierId = ParamUtil.getString(resourceRequest, "supplierIdCreation")
					.toString();
			if (supplierId.equals("")) {
				// create supplier
				int resultedId = this.createSupplier(resourceRequest, user);
				//adding notification
		    	String createdSupplier = resultedId + "";
				addNotification(resultedId, user, createdSupplier);
				log.info("[AJAX] created supplier id : " + resultedId);
				// create JSON response
				JSONArray jsonSuppliers = new JSONArray();
				List<HashMap<String, Object>> supplierData = DefSupplier.getSupplierInfo(String.valueOf(resultedId));
				if (supplierData != null) {
		    		jsonSuppliers = DatabaseConnectionManager.convertListToJson(supplierData);
		    	}
				JSONObject jsonResponse = new JSONObject();
				jsonResponse.put("id", resultedId);
				jsonResponse.put("supplier_data", jsonSuppliers);
				jsonResponse.put("message", "Furnizorul a fost creat cu succes!");
				// send AJAX response
				writer.print(jsonResponse.toJSONString());
			}
    	}
        
    	//super.serveResource(resourceRequest, resourceResponse);
    }
    
    private void addNotification(int resultId, User user, String supplierId) {
		int nextTeamId = UserTeamIdUtils.ACCOUNTING;
		List<HashMap<String,Object>> userDetails = UsersInformations.getUserDetailsByTeamId(nextTeamId);
		HashMap<String, Object> bodyDetails = new HashMap<String,Object>();
		
		
		
		List<HashMap<String,Object>> supplierDetails = DefSupplier.getSupplierDetails(supplierId);
		String supplierName = supplierDetails.get(0).get("name").toString();
		String supplierEmail = supplierDetails.get(0).get("email").toString();
		String supplierPhone = supplierDetails.get(0).get("phone").toString();
		String termenDePlata = supplierDetails.get(0).get("payment_term").toString();
		
		String link = Links.allSuppliersRequestsForAccounting;
		
		String body = "<tr><td style='border:1px solid black'>" + user.getFullName() + "</td>"
						+ "<td style='border:1px solid black'>" + supplierName
						+ "</td><td style='border:1px solid black'>Adresa</td><td style='border:1px solid black'>" + supplierEmail + "</td>"
						+ "<td style='border:1px solid black'>" + supplierPhone + "</td><td style='border:1px solid black'>"
						+ termenDePlata + "</td><td style='border:1px solid black'><a href='" + link + "'>Link</a></td></tr>";
		bodyDetails = setInsertParameters(NotificationTemplates.CONTA_CREARE_FURNIZOR, "", "", body, "0", "B", "1", user, link);
		Notifications.insertNotification("", userDetails, bodyDetails, NotificationsSubject.CREEARE_FURNIZOR);
	}

    private HashMap<String, Object> setInsertParameters (String id_template, String to_email, String to_name, String body, String status, 
			String type, String doc_type, User user, String link){
		
		HashMap<String, Object> bodyDetails = new HashMap<String,Object>();
		
		bodyDetails.put("id_template", id_template);
		bodyDetails.put("sender_email", user.getEmailAddress());
		bodyDetails.put("sender_name", user.getFullName());
		bodyDetails.put("body", body);
		String currentTime = new Timestamp(new Date().getTime()).toString();
		bodyDetails.put("time_to_send", currentTime);
		bodyDetails.put("time_of_send", currentTime);
		bodyDetails.put("status", status);
		bodyDetails.put("type", type);
		bodyDetails.put("doc_type", doc_type);
		bodyDetails.put("created", currentTime);
		if(link.contains("url")){
			bodyDetails.put("link", link.substring(0, link.lastIndexOf("?")));
		} else {
			bodyDetails.put("link", link);
		}
		return bodyDetails;
	}
 
    /**
     * String array containing all the information that mult
     * be inserted into the database
     * @param request
     * @return
     */
    public String[][] getSQLParams(ActionRequest request, String userId, String aisle){
    	
    	
    	String[] items = { "id_supplier", "id_store", "id_company", "id_user", "id_rel_com", 
    				"id_adv_req_type", "id_invoice", "id_product", "currency", "exchange_rate", "val_no_vat_ron", "val_vat_ron",
    				"val_with_vat_ron", "val_no_vat_curr", "val_vat_curr", "val_with_vat_curr",
    				"adv_req_desc", "tip_capex", "id_new_prj", "id_initiative", "issue_date",
    				"end_date", "stage", "id_category", "id_lot", "id_store_aisle", "accounting_year", "accounting_month"};
    	String[][] fields = new String[items.length][2];
    	String id_supplier 		= request.getParameter("id_supplier");
    	String mag_dep 			= request.getParameter("mag_dep");
    	String company 			= request.getParameter("company");
    	String tip_rel_com 		= request.getParameter("tip-rel-comerciala");
    	String tip_solicitare = request.getParameter("tip_solicitare");
    	String id_invoice 		= "0";
    	if (tip_solicitare.equals("1")) {
   		 	id_invoice =request.getParameter("nr_document_asociat");
	   	} else if(tip_solicitare.equals("3")) {
	   		id_invoice = request.getParameter("fact_neplatite");
	   	}
    	String produs = "0";
    	if (request.getParameter("produs") != null && request.getParameter("produs") != "") {
    		produs = request.getParameter("produs").toString();
    	}
    	String currency 		= request.getParameter("currency");
    	String rata_schimb 		= "";
    	String val_no_vat_ron 	= request.getParameter("val_no_vat_ron").replace(",","");
    	String val_vat_ron 		= request.getParameter("val_vat_ron").replace(",","");
    	String val_with_vat_ron = request.getParameter("val_with_vat_ron").replace(",","");
    	String val_no_vat_curr 	= "";
    	String val_vat_curr 	= "";
    	String val_with_vat_curr = "";
    	if (currency.equals("RON")) {
    		rata_schimb = "1";
	    	val_no_vat_curr = val_no_vat_ron;
	    	val_vat_curr = val_vat_ron;
	    	val_with_vat_curr = val_with_vat_ron;
    	} else {
			rata_schimb = request.getParameter("rata-schimb");
			val_no_vat_curr = request.getParameter("val_no_vat_curr").replace(",","");
			val_vat_curr = request.getParameter("val_vat_curr").replace(",","");
			val_with_vat_curr = request.getParameter("val_with_vat_curr").replace(",","");
    	}
    	String explicatii_plata = request.getParameter("explicatii_plata");
    	String categ_capex 		= request.getParameter("categ_capex");
    	if ( categ_capex == "" ){
    		categ_capex = "0";
    	}
    	String lista_initiative = "0";
    	String project 			= "0";
    	if (categ_capex.equals("2")) {
    		lista_initiative = request.getParameter("lista_initiative");
    		if ( lista_initiative == "" ){
    			lista_initiative = "0";
    		}
    	} else if(categ_capex.equals("3")) {
    		project = request.getParameter("project");
    		if ( project == ""){
    			project = "0";
    		}
    	}
    	String inv_date 		= request.getParameter("inv_date");
    	String datascadenta 	= request.getParameter("datascadenta");
    	String stage = "1";
    	String category_id 		= "0";
    	String lot_id 				= "0";
    	category_id 			= request.getParameter("categ_loturi").toString();
    	if (request.getParameter("loturi") != null && request.getParameter("loturi") != "") {
    		lot_id = request.getParameter("loturi").toString();
    	}
    	
    	String[] values = { id_supplier, mag_dep, company, userId, tip_rel_com, tip_solicitare, id_invoice,
    						produs, currency, rata_schimb, val_no_vat_ron, val_vat_ron, val_with_vat_ron, 
    						val_no_vat_curr, val_vat_curr, val_with_vat_curr, explicatii_plata, categ_capex,
    						project, lista_initiative, inv_date, datascadenta, stage, category_id, lot_id,
    						aisle, GeneralCodes.getAccountingYear(), GeneralCodes.getAccountingMonth()};
    	
    	for (int i = 0; i < items.length; i++){
    		fields[i] = new String[2];
    		fields[i][0] = items[i];
    		fields[i][1] = values[i];
    	}
    	
    	for (int i = 0; i < items.length; i++){
    		System.out.println(i + " --> [" + fields[i][0] + "] = [" + fields[i][1]);
    	}
    	
    	return fields;
    }
    

	public boolean containsItem (List<HashMap<String, Object>> list, String wantedKey, String wantedValue){
		for (HashMap<String, Object> entry : list){
			for (String key : entry.keySet()){
				if (wantedKey.equals(key)){
					if (entry.get(key).toString().equals(wantedValue)){
						return true;
					}
				}
			}
		}
		return false;
	}
    
	public List<String> setApprovers(int level, List<HashMap<String, Object>> approvers,
			HashMap<Integer, List<String>> hash, String userId){
		System.out.println(approvers.size());
		List<String> lastApprovers = new ArrayList<String>();
		if (!containsItem(approvers, "user_id", userId)){
			if (hash.get(level) != null)
				lastApprovers = hash.get(level);
			for (HashMap<String, Object> director : approvers){
				String accDirectorId = director.get("user_id").toString();
				if (lastApprovers.size() == 0){
					lastApprovers.add(accDirectorId);
				} else if (!lastApprovers.contains(accDirectorId)){
					lastApprovers.add(accDirectorId);
				}
			}
    	} else {
			alsoApprover[level - 1] = true;
		}
		return lastApprovers;
	}
	
    /**
     * Get approvers for current advance approval
     * @param user
     * @param aisle
     * @return
     */
    public HashMap<Integer, List<String>> computeApprovers (String userId, String aisle, String storeId, boolean goToFinancial){
    	HashMap<Integer, List<String>> approvers = new HashMap<Integer, List<String>>();
    	List<HashMap<String, Object>> directors = UsersInformations.getUserDetailsByTeamId(UserTeamIdUtils.DEPARTAMENT_DIRECTOR);
    	List<HashMap<String, Object>> accountingDirectors = UsersInformations.getUserDetailsByTeamId(UserTeamIdUtils.ACCOUNTING_DIRECTOR);
    	List<HashMap<String, Object>> financialDirectors = UsersInformations.getUserDetailsByTeamId(UserTeamIdUtils.FINANCIAL_DIRECTOR);
    	List<HashMap<String, Object>> storeDirectors = UsersInformations.getUserDetailsByTeamId(UserTeamIdUtils.STORE_DIRECTOR);
    	int level = 1;
    	
    	for (int i = 0; i < 3; i++){
    		alsoApprover[i] = false;
    	}
    	noNotifications = false;
    	
    	if (aisle.equals("93") && !containsItem(storeDirectors, "user_id", userId)){
    		System.out.println("directori = " + directors.toString());
			if (!containsItem(directors, "user_id", userId)){
				List<String> lastApprovers = new ArrayList<String>();
				int size = 0;
				for (HashMap<String, Object> director : directors){
					String directorId = director.get("user_id").toString();
					if (!lastApprovers.contains(directorId)){
    					User dir = null;
    					try {
    						System.out.println(directorId);
							dir = UserLocalServiceUtil.getUser(Integer.parseInt(directorId));
							if (dir.getAddresses().size()>0) {
								if (aisle.equals(dir.getAddresses().get(0).getStreet3().toString()) &&
										storeId.equals(dir.getAddresses().get(0).getStreet2().toString())){
									lastApprovers.add(directorId);
								}
							}
						} catch (NumberFormatException | PortalException
								| SystemException e) {
							e.printStackTrace();
						}
					}
				}
				if (size == lastApprovers.size()){
					lastApprovers.add("fictive_department_director");
				}
				approvers.put(level, lastApprovers);
			} else {
				alsoApprover[level - 1] = true;
			}
    	} else {
			alsoApprover[level - 1] = true;
		}
    	level = 2;
    	//accounting director
    	approvers.put(level, setApprovers(level, accountingDirectors, approvers, userId));
    	level = 3;
    	
    	if (goToFinancial){
    		//financial director
        	approvers.put(level, setApprovers(level, financialDirectors, approvers, userId));
    	}
    	
    	System.out.println("APPROVERS = " + approvers);
    	return approvers;
    }
    
    /**
     * Insert approval levels into advance_request_set_approvals
     * @param approvers
     * @param id
     */
    public void insertApprovals (HashMap<Integer, List<String>> approvers, int id){
    	String[][] fields = new String[3][2];
    	fields[0][0] = "id_adv_req";
    	fields[0][1] = String.valueOf(id);
    	fields[1][0] = "id_user";
    	fields[2][0] = "level_approval";
    	for (int key : approvers.keySet()){
    		if (!alsoApprover[key - 1]){
	    		fields[1][1] = approvers.get(key).toString();
	    		fields[2][1] = String.valueOf(key);
	    		GenericDBStatements.insertEntry(fields, "advance_request_set_approvals", new ArrayList<String>());
    		}
    	}
    }
    
    
    /**
	 * Returns true if the advance request value is over 50.000 EUR
	 * @param header = ADVANCE REQUEST HEADER
	 * @return
	 */
	public boolean goToFinancialDirector(String[][] header){
		String valueRON = "";
		String valueCURR = "";
		String currency = "";
		String inv_date = "";
		for (int i = 0; i < header.length; i++){
			if (header[i][0].equals("val_with_vat_ron")){
				valueRON = header[i][1];
			}
			if (header[i][0].equals("val_with_vat_curr")){
				valueCURR = header[i][1];
			}
			if (header[i][0].equals("currency")){
				currency = header[i][1];
			}
			if (header[i][0].equals("issue_date")){
				inv_date = header[i][1];
			}
		}
		float exchangeRateEUR = DefExchRates.getExchangeRate("EUR", inv_date, 1);
		if(currency.equals("EUR")){
			if(Float.parseFloat(valueCURR) >= UserTeamIdUtils.FINANCIAL_DIRECTOR_APROVE_VALUE){
				return true;
			} else {
				return false;
			}
		} else if( Float.parseFloat(valueRON) / exchangeRateEUR >= UserTeamIdUtils.FINANCIAL_DIRECTOR_APROVE_VALUE ) {
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * If the user that requested the advance is the one that must approve it
	 * @param fields
	 * @return
	 */
	public String[][] setStage(String[][] fields){
		boolean finalStage = true;
		for (int i = 0; i < fields.length; i++){
			if (fields[i][0].equals("stage")){
				for (int j = 0; j < alsoApprover.length; j++){
					if (!alsoApprover[j])
						finalStage = false;
				}
				if (finalStage){
					fields[i][1] = "5";
					noNotifications = true;
				} else {
					fields[i][1] = "1";
				}
			}
		}
		return fields;
	}

	/**
	 * Save a new advance request, send notifications and enquire approvers
	 * @param request
	 * @param response
	 */
	public void addRequest(ActionRequest request, ActionResponse response) {
    	// remove default error message from Liferay
    	PortletConfig portletConfig = (PortletConfig)request.getAttribute(JavaConstants.JAVAX_PORTLET_CONFIG);
    	LiferayPortletConfig liferayPortletConfig = (LiferayPortletConfig) portletConfig;
    	SessionMessages.add(request, liferayPortletConfig.getPortletId() + SessionMessages.KEY_SUFFIX_HIDE_DEFAULT_ERROR_MESSAGE);
		
		log.info("[PROFLUO] RequestAdvanceApproval portlet - save()");
		//set parameters from request on response in case of error
		ControllerUtils.saveRequestParamsOnResponse(request, response, log);
	    try {
	    	//get info from user	
	    	User user 				= UserServiceUtil.getUserById(Long.parseLong(request.getRemoteUser()));
	    	int id_user 			= (int) user.getUserId();
	    	String id_store_aisles 	= "0";
	    	String storeId			= "0";
	    	if (user.getAddresses() != null && user.getAddresses().get(0) != null){
	    		id_store_aisles		= user.getAddresses().get(0).getStreet3().toString();
	    		storeId				= user.getAddresses().get(0).getStreet2().toString();
	    	}
	    	//get useful information from request
	    	String id_supplier 		= request.getParameter("id_supplier");
	    	String company 			= request.getParameter("company");
	    	String inv_date 		= request.getParameter("inv_date");
	    	int stage 				= 1;
	    	//get SQL parameters
	    	String[][] fields = getSQLParams(request, String.valueOf(id_user),id_store_aisles);
	    	
	    	// insert entry into database
	    	if(id_supplier != null) {
	    		HashMap<Integer, List<String>> approvers = computeApprovers(String.valueOf(id_user), 
	    									id_store_aisles, storeId, goToFinancialDirector(fields));
	    		fields = setStage(fields);
	    		
	    		int requestId = GenericDBStatements.insertEntry(fields, "advance_request", new ArrayList<String>());
	    		
	    		insertApprovals(approvers, requestId);
	    		Calendar calendar = Calendar.getInstance();
			    java.sql.Timestamp currentTS = new java.sql.Timestamp(calendar.getTime().getTime());
	    		AdvanceRequest.approveAdvanceRequest(currentTS.toString(), String.valueOf(id_user), String.valueOf(requestId), "0", "1");
	    		
	    		addNotificationToNextTeam(requestId, user, id_supplier, inv_date, company);
	    		String supplierCode = DefSupplier.getSupplierCodeById(id_supplier);	
	    		boolean isBlocked = DefSupplier.isBlocked(supplierCode, company);
	    		if(isBlocked){
	    			DefSupplier.setBlockedStatusBySupplierCode(supplierCode, company, "2");
	    			addNotificationToAccounting(user, supplierCode);
	    		}
    	    		
    	    	log.info("[PROFLUO] Request for Advance created with id: " + requestId);
    	    	// show status to user
    	    	// list of added DPIs to the request
    	    	String dpi_list			= request.getParameter("dpi_list");
    	    	String[] dpis			= null;	
    	    	// check if list has any DPI added
    	    	if (dpi_list.toString().contains(",")) {
    	    		dpis = dpi_list.toString().split(",");
    	    	}
    	    	if (requestId > 0) {
    	    		if (dpis != null) {
    	    			// for all dpis in the list
    	    			for (int j = 0; j < dpis.length; j++) {
    	    				// except the ones that came empty from the request
    	    				if(!dpis[j].trim().equals("")) {
	    	    				// no excludes
			    	    		ArrayList<String> excludes = new ArrayList<String>();
			    	    		// set parameters
			    	    		String arrFields[][] =  new String[2][2];
			    	    		// set the id of the dpis
			    	    		arrFields[0][0] = "id_dpi";
			    	    		arrFields[0][1] = dpis[j].trim();
			    	    		// set the id of the above created request
			    	    		arrFields[1][0] = "id_adv_req";
			    	    		arrFields[1][1] = new String(String.valueOf(requestId));
			    	    		// fire insert statement
								GenericDBStatements.insertEntry(arrFields, "advance_request_dpi", excludes);
    	    				}
    	    			}
    	    		}
    	    		SessionMessages.add(request, "insert_ok");
    	    	} else {
    	    		SessionErrors.add(request, "insert_nok");
    	    		response.setRenderParameter("mvcPath", "/html/requestadvanceapproval/add_request.jsp");
    	    	}
    	    } else {
    	    	SessionErrors.add(request, "params_nok");
    	    	response.setRenderParameter("mvcPath", "/html/requestadvanceapproval/add_request.jsp");
    	    }
	    } catch (Exception e) {
	        SessionErrors.add(request, e.getClass().getName());
	        // just for show
	        SessionErrors.add(request, "error");
	        e.printStackTrace();

	        response.setRenderParameter("mvcPath", "/html/requestadvanceapproval/add_request.jsp");
	    }
	}
	
	private void addNotificationToAccounting(User user, String supplierCode){
		List<HashMap<String,Object>> list = DefSupplier.getSupplierDetailsBySupplierCode(supplierCode);
		String supplierName = list.get(0).get("name").toString();
		String email = list.get(0).get("email").toString();
		String phone = list.get(0).get("phone").toString();
		String paymentTerm = list.get(0).get("payment_term").toString();
		HashMap<String, Object> bodyDetails = new HashMap<String,Object>();
		String body = "<tr><td style='border:1px solid black'>" + user.getFullName() + "</td>"
				+ "<td style='border:1px solid black'>" + supplierName + "</td><td style='border:1px solid black'>Adresa</td>"
				+ "<td style='border:1px solid black'>" + email + "</td>"
				+ "<td style='border:1px solid black'>" + phone + "</td>"
				+ "<td style='border:1px solid black'>" + paymentTerm + "</td>"
				+ "<td style='border:1px solid black'><a href='" + Links.allBlockedSuppliers + "'>Link</a></td></tr>";
		bodyDetails = setInsertParameters(NotificationTemplates.CONTA_DEBLOCARE_FURNIZOR, "", "", body, "0", "B", "1", user, Links.allBlockedSuppliers);
		
		List<HashMap<String, Object>> userDetails = new ArrayList<HashMap<String,Object>>();
		HashMap<String,Object> hashMap = new HashMap<String,Object>();
		hashMap.put("email", user.getEmailAddress());
		hashMap.put("first_name", user.getFirstName());
		hashMap.put("last_name", user.getLastName());
		userDetails.add(hashMap);
		Notifications.insertNotification("", userDetails, bodyDetails, NotificationsSubject.DEBLOCARE_FURNIZOR);
	}
	
	
	private void addNotificationToNextTeam(int advanceRequestId, User user, 
			 		String supplierId, String invDate, String company){
		 
	    	String templateId = "";
	    	boolean isNew = false;
	    	String link = "";
	    	//list of users to be notified
			List<HashMap<String,Object>> usersToBeNotified = new ArrayList<HashMap<String,Object>>();
			//continutul notificarii
			HashMap<String, Object> bodyDetails = new HashMap<String,Object>();
	    	List<HashMap<String,Object>> firstApprovals = AdvanceRequest.getFirstLevelApprovals(String.valueOf(advanceRequestId));
			for(HashMap<String,Object> hash : firstApprovals) {
				String approvalsIds = hash.get("id_user").toString();
				approvalsIds = approvalsIds.substring(1, approvalsIds.length() - 1);
				String [] individualIds = approvalsIds.split(", ");
				try {
					for(int i = 0; i < individualIds.length; i++ ) {
						User approval = null;
						try{
							approval = UserLocalServiceUtil.getUser(Integer.parseInt(individualIds[i]));
						} catch (NumberFormatException e){
							continue;
						}
						HashMap<String,Object> userInfo = new HashMap<String,Object>();
						userInfo.put("email", approval.getEmailAddress());
						userInfo.put("first_name", approval.getFirstName());
						userInfo.put("last_name", approval.getLastName());
						if(!usersToBeNotified.contains(userInfo)) {
							usersToBeNotified.add(userInfo);
						}
					}
				} catch (NumberFormatException | PortalException | SystemException e) {
					e.printStackTrace();
				}
			}
			for (int i = 0; i < alsoApprover.length; i++){
				if (!alsoApprover[i]){
					switch (i + 1){
					case 1 :
						templateId = NotificationTemplates.DIR_DEPT_PLATA_AVANS;
			    		isNew = true;
			    		link = Links.individualAdvanceRequestForApprovals + "&id=" + advanceRequestId;
			    		break;
					case 2 :
						templateId = NotificationTemplates.CONTA_CERERE_PLATA_AVANS;
			    		isNew = true;
			    		link = Links.individualAdvanceRequestForApprovals + "&id=" + advanceRequestId;
			    		break;
					case 3 :
						templateId = NotificationTemplates.DIR_FINANCIAR_PLATA_AVANS;
			    		link = Links.individualAdvanceRequestForApprovals + "&id=" + advanceRequestId;
			    		break;
			    	default :
			    		templateId = "";
			    		link = "#";
			    		break;
					}
					break;
				}
			}
			if (noNotifications){
				//TODO : ALL ACCOUNTING UNIT?
				System.out.println("DIRECT CATRE CONTABILITATE");
				usersToBeNotified = UsersInformations.getUserDetailsByTeamId(UserTeamIdUtils.ACCOUNTING);			
				templateId = NotificationTemplates.CONTA_CERERE_PLATA_AVANS;
	    		link = Links.individualAdvanceRequestForAccounting + "&id=" + advanceRequestId;
			}
			
			
			String supplierName = DefSupplier.getSupplierNameById(supplierId);
			String companyName = DefCompanies.getCompanyNameById(company);
			String body = "<tr><td style='border:1px solid black'>" + advanceRequestId + "</td>"
							+ "<td style='border:1px solid black'>" + invDate + "</td><td style='border:1px solid black'>" + supplierName + "</td>"
							+ "<td style='border:1px solid black'>" + companyName + "</td>"
							+ "<td style='border:1px solid black'><a href='" + link + "'>Link</a></td></tr>";
	    	if (isNew) {
	    		bodyDetails = setInsertParameters(templateId, "", "", body, "0", "S", "1", user, link);
	    	} else {
	    		bodyDetails = setInsertParameters(templateId, "", "", body, "0", "B", "1", user, link);
	    	}
			Notifications.insertNotification(String.valueOf(advanceRequestId), usersToBeNotified, bodyDetails, NotificationsSubject.SOLICITARE_PLATA_AVANS);
	    }
	
	@Override
	public void render(RenderRequest renderRequest, RenderResponse renderResponse) throws PortletException, IOException {
		HttpServletRequest request = PortalUtil.getHttpServletRequest(renderRequest);
    	String p_p_id = PortalUtil.getOriginalServletRequest(request).getParameter("p_p_id");
    	String path = PortalUtil.getOriginalServletRequest(request).getParameter("_" + p_p_id + "_mvcPath");
    	
    	renderRequest.setAttribute("pagina", "solicitari-avans");
    	
		log.info("[PROFLUO] RequestAdvanceApproval portlet - render()");
		log.info("[PROFLUO] Context Path: " + renderRequest.getContextPath());
    	log.info("[PROFLUO] Portlet ID	: " + p_p_id);
    	log.info("[PROFLUO] Portlet Path: " + path);
    	try {
	    	User user = UserServiceUtil.getUserById(Long.parseLong(renderRequest.getRemoteUser()));
	    	int id_user 			= (int) user.getUserId();
	    	
	    	List<Team> userTeams = user.getTeams();
	    	for (int i = 0; i < userTeams.size(); i++) {
	    		log.info("[PROFLUO] User TEAM[" + i + "]: " + userTeams.get(i).getName() + " - " + userTeams.get(i).getTeamId() );
	    	}
	    	
	    	List<Role> userRoles = user.getRoles();
	    	for (int i = 0; i < userRoles.size(); i++) {
	    		log.info("[PROFLUO] User ROLE[" + i + "]: " + userRoles.get(i).getName());
	    	}
	    	
	    	log.info("[PROFLUO] User id " + id_user);
    	} catch (Exception e) {}
    	
    	if (path == null || path.endsWith("view.jsp")) {
    		log.info("[PROFLUO] LISTING render()");
    		
    	} else if (path.endsWith("add_request.jsp")) {
    		log.info("[PROFLUO] Salveaza solicitare creare avans render()");
    		
    	}
		
	    super.render(renderRequest, renderResponse);
	}
}