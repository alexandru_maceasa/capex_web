package com.profluo.ec.carrefour.portlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletConfig;
import javax.portlet.PortletException;
import javax.portlet.PortletPreferences;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;
import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.portlet.LiferayPortletConfig;
import com.liferay.portal.kernel.servlet.SessionMessages;
import com.liferay.portal.kernel.util.JavaConstants;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.model.User;
import com.liferay.portal.service.UserServiceUtil;
import com.liferay.portal.theme.ThemeDisplay;
import com.liferay.portal.util.PortalUtil;
import com.liferay.portlet.PortletPreferencesFactoryUtil;
import com.liferay.util.bridges.mvc.MVCPortlet;
import com.profluo.ecm.controller.UserTeamIdUtils;
import com.profluo.ecm.flows.InvoiceStages;
import com.profluo.ecm.model.db.DatabaseConnectionManager;
import com.profluo.ecm.model.db.GenericDBStatements;
import com.profluo.ecm.model.db.InvoiceHeader;
import com.profluo.ecm.utils.ExcelUtil;

/**
 * Portlet implementation class FacturiBlocateLaPlata
 */
public class FacturiBlocateLaPlata extends MVCPortlet {
	/**
	 * Generic log instance.
	 */
	private final Log log = LogFactoryUtil.getLog("PROFLUO-" + MVCPortlet.class.getName());
	
	/**
	 * Users that must be notified when an invoice was registered with success
	 */
	List<HashMap<String, Object>> approvers = new ArrayList<HashMap<String,Object>>();
	int approversSize = 0;
	
	/**
	 * List of line ids that must also be approved by the current user for the next stages 
	 */
	List<String> linesToBeApprovedInNextStage = new ArrayList<String>();
	
	/*
	 * True if the current user has store lines to approve in multiple stages
	 * For stage 3 => invoice_approvals count fails so this flag becomes true
	 * Used in the next case : for stage 4 invoice_approvals count succeeds so
	 * the invoice_header stage must be updated to 5/10. If the flag is true
	 * this update cannot be executed
	 */
	//boolean previousStageCountFailure = false;
	
	/*
	 * True if the current approver is associated with only one stage
	 */
	boolean approverInOnlyOneStage = false;
	/**
	 * List of all store lines associated with the current invoice
	 */
	List<HashMap<String, Object>> storeLines = new ArrayList<HashMap<String,Object>>();;
	/**
	 * List of all store line ids 
	 */
	HashMap<String, List<String>> storeLinesToBeApprovedInNextStage = new HashMap<String, List<String>>();
	
	/**
	 * List of store lines id that the current user must also approve in the next stage
	 */
	List<String> storeLinesThatBelongToTheCurrentUser = new ArrayList<String>();
	
	/**
	 * List of all stores whose directors must be notified
	 */
	ArrayList<String> storeIds = new ArrayList<String>();
	/**
	 * Getting information about portlet initialization.
	 */
   @Override
   public void init() throws PortletException {
   	log.info("[PROFLUO] Initialize Facturi blocate portlet.");
   	super.init();
   }
  
   @SuppressWarnings("unchecked")
@Override
   public void serveResource(ResourceRequest resourceRequest, ResourceResponse resourceResponse) throws IOException, PortletException {
   	log.info("[AJAX] Invoice Controller");

   	String action = ParamUtil.getString(resourceRequest, "action");
   	log.info("[AJAX] action: " + action);
   	if(action.equals("exportExcel")) {
   		List<HashMap<String, Object>> allInvoices = InvoiceHeader.getRejectedInvoicesForAccounting(0, 50000, 0, "", 0, "", "", "");
   		String[][] header = new String[11][];
		for (int i = 0; i < 11; i++) {
			header[i] = new String[2];
		}
		
		header[0][0] = "nr";
		header[0][1] = "Numar factura";
		header[1][0] = "categ_capex";
		header[1][1] = "Tip capex";
		header[2][0] = "code";
		header[2][1] = "Cod furnizor";
		header[3][0] = "furnizor";
		header[3][1] = "Nume furnizor";
		header[4][0] = "inv_date";
		header[4][1] = "Data factura";
		header[5][0] = "datascadenta";
		header[5][1] = "Data scadenta";
		header[6][0] = "val_cu_tva";
		header[6][1] = "Valoare cu TVA";
		header[7][0] = "currency";
		header[7][1] = "Moneda";
		header[8][0] = "company";
		header[8][1] = "Societate";
		header[9][0] = "motiv_blocat";
		header[9][1] = "Motiv blocare";
		header[10][0] = "supplier_fault";
		header[10][1] = "Vina furnizor";
		
		try {
			ExcelUtil.getExcel(allInvoices, header, "Facturi_blocate_la_plata", resourceResponse);
		} catch (IOException e) {
			e.printStackTrace();
		}
   	} else if (action.equals("getHistory")){
   		PrintWriter writer = resourceResponse.getWriter();
   		//get the id of the invoice
   		String invoiceID = ParamUtil.getString(resourceRequest, "id").toString();
   		//get the names and jobTitles from all approvers
   		List<HashMap<String, Object>> approverInfo = InvoiceHeader.getApproversHistory(invoiceID);
   		List<HashMap<String, Object>> tableData = UserTeamIdUtils.getApproverInfoFromApproverId(approverInfo);
   		JSONArray jsonApproverInfo = new JSONArray();
	    	if (tableData != null) {
	    		jsonApproverInfo = DatabaseConnectionManager.convertListToJson(tableData);
	    	}
	    	// create JSON response
	    	JSONObject jsonResponse = new JSONObject();
			jsonResponse.put("values", jsonApproverInfo);
			// send AJAX response
			writer.print(jsonResponse.toJSONString());
   	} else if (action.equals("filter") || action.equals("next") || action.equals("prev") || action.equals("last") || action.equals("first")) {
   		PrintWriter writer = resourceResponse.getWriter();
		// read AJAX parameters used in filtering and pagination
    	int start = Integer.parseInt(ParamUtil.getString(resourceRequest, "start").toString());
    	int count = Integer.parseInt(ParamUtil.getString(resourceRequest, "count").toString());
    	int total = Integer.parseInt(ParamUtil.getString(resourceRequest, "total").toString());
    	int uid	  = Integer.parseInt(ParamUtil.getString(resourceRequest, "uid").toString());
    	int companyId = Integer.parseInt(ParamUtil.getString(resourceRequest, "company_id").toString());
    	String moneda = ParamUtil.getString(resourceRequest, "moneda");
    	int number = 0;
    	if (ParamUtil.getString(resourceRequest, "number") != null && !ParamUtil.getString(resourceRequest, "number").equals("")) {
    		number = Integer.parseInt(ParamUtil.getString(resourceRequest, "number").replace(",","").toString());
    	}
    	String datafactura = ParamUtil.getString(resourceRequest, "datafactura");
    	log.info("data factura : " + datafactura);
    	String supplierCode = ParamUtil.getString(resourceRequest, "supplier_code");
    	String supplierName = ParamUtil.getString(resourceRequest, "supplier_name");
    	String pageLayout   = ParamUtil.getString(resourceRequest, "page_layout");
    	
    	log.info("number : " + number);
    	List<HashMap<String,Object>> allInvoices = null;
		log.info("Getting all rejected invoices filtered!");
		System.out.println("pageLayout -> " + pageLayout);
		if(pageLayout.equals(InvoiceStages.PAGE_FACTURI_BLOCATE_CONTABILITATE)) {
			allInvoices = InvoiceHeader.getRejectedInvoicesForAccounting(start, count, companyId, "", number, datafactura, supplierCode, supplierName);
			total = InvoiceHeader.getRejectedInvoicesForAccountingCount(start, count, companyId, "", number, datafactura, supplierCode, supplierName);
		} else {
			allInvoices = InvoiceHeader.getRejectedInvoices(start, count, companyId, moneda, number, datafactura, supplierCode, supplierName, uid);
			total = InvoiceHeader.getRejectedInvoicesCount(start, count, companyId, moneda, number, datafactura, supplierCode, supplierName, uid);
		}
    	
    	JSONArray jsonInvoices = new JSONArray();
    	if (allInvoices != null) {
    		jsonInvoices = DatabaseConnectionManager.convertListToJson(allInvoices);
    	}
    	JSONObject jsonResponse = new JSONObject();
    	
		jsonResponse.put("action", action);
		jsonResponse.put("start", start);
		jsonResponse.put("count", count);
		jsonResponse.put("total", total);
		jsonResponse.put("values", jsonInvoices);
		writer.print(jsonResponse.toJSONString());
	} else if (action.equals("getInventoryDetails")){
		PrintWriter writer = resourceResponse.getWriter();
		String idStoreLine = ParamUtil.getString(resourceRequest, "idStoreLine").toString();
		List<HashMap<String,Object>> inventoryDetails = InvoiceHeader.getInventoryDetailsByIdStoreLine(idStoreLine);
		JSONArray jsonInvNo = new JSONArray();
		if (inventoryDetails != null) {
			for (int i = 0; i < inventoryDetails.size(); i++) {
				JSONObject jsonLine = new JSONObject();
				jsonLine.put("nr_inv", inventoryDetails.get(i).get("inventory_no").toString());
				jsonLine.put("data_pif", inventoryDetails.get(i).get("pif_date").toString());
				jsonLine.put("receptie", inventoryDetails.get(i).get("reception").toString());
				jsonInvNo.add(jsonLine);
			}
			writer.print(jsonInvNo.toJSONString());
		}
	}
   	//super.serveResource(resourceRequest, resourceResponse);
   } 
   
   
   
   public int simpleSave(String[][] arrFields, String id, ArrayList<String> excludes, ArrayList<String> whereStatement, ArrayList<String> whereStatementValues){
   	int result = 0;
   	try{
   		result = GenericDBStatements.updateEntry(arrFields, "invoice_header", excludes, whereStatement, whereStatementValues);
   	} catch (Exception e){}
   		return result;
   }
   
   /**
    * 
    * @param request
    * @param response
    * @param user
    * @return
    */
   private String[][] getSqlParams(ActionRequest request, ActionResponse response, User user){
	   // true if the user is dpi responsable
	   String [][] arrFields = null;
	   try{
			int arrSize = 6;
			arrFields = new String[arrSize][2];
			String blocked = request.getParameter("blockedORnot").toString();
			boolean action = false;
			if (blocked.equals("OK")){
				action = true;
			}else action = false;
			arrFields[0][0] = "blocked_type";
			if(!action && request.getParameter("motiv_invalidare") != null){
				//REFUZ LA PLATA
				arrFields[0][1] = request.getParameter("motiv_invalidare").toString();
			} else arrFields[0][1] = "0";
			arrFields[1][0] = "stage";
			arrFields[2][0] = "litigii";
			String litigiu = request.getParameter("existenta_litigiu").toString();
			if(litigiu.equals("1")){
				arrFields[2][1] = "Y";
			}else arrFields[2][1] = "N";
			arrFields[3][0] = "details_litigii";
			if(arrFields[2][1].equals("Y")){
				arrFields[3][1] = request.getParameter("detalii-litigiu").toString();
			}else arrFields[3][1] = "";
			arrFields[4][0] = "supplier_fault";
			String suppFault = "";
			arrFields[4][1] = "N";
			if(request.getParameter("vina_furnizor") != null){
				suppFault = request.getParameter("vina_furnizor").toString();
				if(suppFault.equals("1")){
					arrFields[4][1] = "Y";
				};
			}
			String detalii_aditionale = "";
			if (request.getParameter("detalii-aditionale") != null){
				detalii_aditionale =  request.getParameter("detalii-aditionale").toString();
			}
			arrFields[5][0] = "details_aditional";
			arrFields[5][1] = detalii_aditionale;
	       	if (action){
	       		arrFields[1][1] = "3";
	       	} else {
	       		arrFields[1][1] = "-3";
	       	}
		   } catch (Exception e) {
			   e.printStackTrace();
		   }
	   return arrFields;
   }
   
   
   /**
    * 
    * @param request
    * @param response
    */
   public void approveInvoice( ActionRequest request, ActionResponse response ) {
		// remove default error message from Liferay
	   	PortletConfig portletConfig = (PortletConfig)request.getAttribute(JavaConstants.JAVAX_PORTLET_CONFIG);
	   	LiferayPortletConfig liferayPortletConfig = (LiferayPortletConfig) portletConfig;
	   	SessionMessages.add(request, liferayPortletConfig.getPortletId() + SessionMessages.KEY_SUFFIX_HIDE_DEFAULT_ERROR_MESSAGE);
		
		log.info("[PROFLUO] Facturi Blocate portlet - approveInvoice()");
		try{
			User user = UserServiceUtil.getUserById(Long.parseLong(request.getRemoteUser()));
			String id = request.getParameter("idInvoice");
			String blocked = request.getParameter("blockedORnot").toString();
			String ids = request.getParameter("lineIds").toString();
			//String valueCurr = request.getParameter("valueCurr");
			//String valueRon = request.getParameter("valueRon");
			String invoiceDate = request.getParameter("invoiceDate").toString();
			String invoiceNumber = request.getParameter("invoiceNumber").toString();
			String supplierName = request.getParameter("supplierName").toString();
//			String supplierCui = request.getParameter("supplierCode").toString();
			String companyName = request.getParameter("companyName").toString();
//			String kontanAcc = request.getParameter("kontanAccount").toString();

			String[] lineIds = ids.substring(0, ids.length() - 1).split(",");
        	
        	Calendar calendar = Calendar.getInstance();
		    java.sql.Timestamp currentTS = new java.sql.Timestamp(calendar.getTime().getTime());
        	for (int i = 0; i < lineIds.length; i++){
				if (!blocked.equals("OK")){
					InvoiceHeader.updateInvoiceApprovals(currentTS.toString(), String.valueOf(user.getUserId()),
							ids.substring(0, ids.length() - 1), "1", "0");
				} else {
					InvoiceHeader.updateInvoiceApprovals(currentTS.toString(), String.valueOf(user.getUserId()),
							ids.substring(0, ids.length() - 1), "0", "1");
				}
			}
			
			String[][] arrFields = null;
			arrFields = this.getSqlParams(request, response, user);
			boolean hasAllApprovals = InvoiceHeader.hasAllApprovals(id);
			if(hasAllApprovals){
				arrFields[1][1] = "10";
			} else {
				Invoices inv = new Invoices();
				inv.addNotificationForFirstUnapprovedLevel(6, invoiceNumber, user, supplierName, id, invoiceDate, companyName);
			}
        	ArrayList<String> whereStatement = new ArrayList<String>();
        	//attrs values for where statement
        	ArrayList<String> whereStatementValues = new ArrayList<String>();
        	whereStatement.add("id");
        	whereStatementValues.add(id);
        	GenericDBStatements.updateEntry(arrFields, "invoice_header", new ArrayList<String>(), whereStatement, whereStatementValues); 	
        	SessionMessages.add(request, "update_ok");
		} catch (Exception e){}
   }
   
	/**
	 * Returns true if the current user exists in the group 
	 * @param user = current user
	 * @param list = group of users that must be tested
	 * @return
	*/
//	private boolean currentUserBelongsToGroup(User user){
//		for (int i = approversSize; i < approvers.size(); i++){
//			if (approvers.get(i).get("user_id").toString().equals(String.valueOf(user.getUserId()))){
//				return true;
//			}
//		}
//		return false;
//	}
	/*
	private  void sendNotifactionFromDpi(int id, User user, String invoiceNumber, String invoiceDate, 
			String supplier, String company, String account, String idNewProject){
		
		//set insert parameters
		HashMap<String, Object> notificationFields = new HashMap<String, Object>();
		
		String body = "<tr><td style='border:1px solid black'>" + invoiceNumber + "</td><td style='border:1px solid black'>" + 
						invoiceDate + "</td><td style='border:1px solid black'>" + 
					supplier + "</td><td style='border:1px solid black'>" + company + "</td></tr>";
		notificationFields = setInsertParameters(NotificationTemplates.INREGISTRARE_FACTURA, "", "", body, "0", "B", "1", user, "#");
		
		List<HashMap<String,Object>> aprobatorDetail = new ArrayList<HashMap<String,Object>>();
		List<HashMap<String,Object>> allDirectors = new ArrayList<HashMap<String,Object>>();

		String productCode = "";
		boolean isItLot = false;
		int itLines = 0;
		List<String> allStoresIds = new ArrayList<String>();
		List<String> allStoreIdsWithDpi = new ArrayList<String>();
		List<String> allStoreIdsWithoutDpi = new ArrayList<String>();
		//all store lines
	   	storeLines = InvoiceHeader.getStoreLinesById(String.valueOf(id), "invoice_store_line");
	    //Get all dpis from invoice store lines to know which dpi responsables to notify
		String currentUserStoreId = "";
		try {
			currentUserStoreId = user.getAddresses().get(0).getStreet2().toString();
		} catch (SystemException e1) {
			e1.printStackTrace();
		}
	   	for (HashMap<String, Object> hash : storeLines){
			//salvare id-ul produsului
			productCode = hash.get("product_code").toString();
			//salvare id-ul DPI
			isItLot = DefLot.hasITLot(productCode);
			if (isItLot){
				itLines++;
			} else {
				String store_id = DefStore.getStoreIdById(hash.get("id_store").toString());
				if (!allStoresIds.contains(store_id)){
					//salvare id-ul magazinului
					allStoresIds.add(store_id);
				}
				if (hash.get("id_dpi").toString().equals("") || hash.get("id_dpi").toString().equals("0")){
					if (!allStoreIdsWithoutDpi.contains(store_id)){
						//salvare id-ul magazinului
						allStoreIdsWithoutDpi.add(store_id);
					}
				} else {
					if (!allStoreIdsWithDpi.contains(store_id)){
						//salvare id-ul magazinului
						allStoreIdsWithDpi.add(store_id);
					}
				}
			}
	   	}
	   	if (itLines > 0){
			//insert the notifications
	   		List<HashMap<String, Object>> it = UsersInformations.getUserDetailsByTeamId(UserTeamIdUtils.IT_DIRECTOR);
			Notifications.insertNotification(String.valueOf(id), it, notificationFields, NotificationsSubject.INREGISTRARE_FACTURA);
	   	}
	   	
	   	//remove current user store if the current user is hq admin
	   	if(UsersInformations.belongsToTeam((int)user.getUserId(), UserTeamIdUtils.ADMINISTRATOR_SEDIU)){
	   		if(allStoresIds.contains(currentUserStoreId)){
	   			allStoresIds.remove(currentUserStoreId);
	   		}
	   		if(allStoreIdsWithoutDpi.contains(currentUserStoreId)){
	   			allStoreIdsWithoutDpi.remove(currentUserStoreId);
	   		}
	   		if(allStoreIdsWithDpi.contains(currentUserStoreId)){
	   			allStoreIdsWithDpi.remove(currentUserStoreId);
	   		}
	   	}
	   	List<HashMap<String,Object>> allSediuAdmins = UsersInformations.getUserDetailsByTeamId(UserTeamIdUtils.ADMINISTRATOR_SEDIU);
		int allHQAdmins = allSediuAdmins.size();
		for(int i = allHQAdmins - 1 ; i >= 0; i--) {
			User adminSediu;
			String adminAddress = "";
			try {
				adminSediu = UserLocalServiceUtil.getUser(Integer.parseInt(allSediuAdmins.get(i).get("user_id").toString()));
				adminAddress = adminSediu.getAddresses().get(0).getStreet2().toString();
			} catch (NumberFormatException | PortalException | SystemException e) {
				e.printStackTrace();
			}
			
			if(!allStoresIds.contains(adminAddress)){
				allSediuAdmins.remove(i);
			}
		}
		if (allSediuAdmins.size() != 0){
			Notifications.insertNotification(String.valueOf(id), allSediuAdmins, notificationFields, NotificationsSubject.INREGISTRARE_FACTURA);
		}
		if (Integer.parseInt(idNewProject) != 0){
			//notifica directori active property
			String projectOwner = DefNewProj.getOwnerById(idNewProject);
			//daca e setat Active la administrarea de proiecte noi se trimite notificare catre directorii Active
			if(projectOwner.equals("0")) {
				allDirectors.clear();
				allDirectors.addAll(UsersInformations.getUserDetailsByTeamId(UserTeamIdUtils.DIRECTOR_ACTIVE));
			//daca e setat Property la administrarea de proiecte noi se trimite notificare catre directorii Property
			} else if (projectOwner.equals("1")) {
				allDirectors.clear();
				allDirectors.addAll(UsersInformations.getUserDetailsByTeamId(UserTeamIdUtils.PROPERTY_DIRECTOR));
			//altfel se notifica atat directorii Active cat si cei de Property
			} else {
				allDirectors.clear();
				allDirectors.addAll(UsersInformations.getUserDetailsByTeamId(UserTeamIdUtils.DIRECTOR_ACTIVE));
				allDirectors.addAll(UsersInformations.getUserDetailsByTeamId(UserTeamIdUtils.PROPERTY_DIRECTOR));
			}
			
			Notifications.insertNotification(String.valueOf(id), allDirectors, notificationFields, NotificationsSubject.INREGISTRARE_FACTURA);
	   	} else {
			//elimina sediile
			for (int i = allStoresIds.size() - 1; i >= 0; i--){
				if(DefStore.isHQ(DefStore.getIdByStoreId(allStoresIds.get(i)))){
					allStoresIds.remove(i);
				}
			}
			
			//remove current user store if the current user is hq admin
		   	if(UsersInformations.belongsToTeam((int)user.getUserId(), UserTeamIdUtils.STORE_DIRECTOR)){
		   		if(allStoresIds.contains(currentUserStoreId)){
		   			allStoresIds.remove(currentUserStoreId);
		   		}
		   	}
			aprobatorDetail.clear();
			//notifica directorii de magazin
			if (allStoresIds.size() != 0){
				List<HashMap<String,Object>> allStoreDirectors = UsersInformations.getUserDetailsByTeamId(UserTeamIdUtils.STORE_DIRECTOR);
				for(int i = 0 ; i < allStoresIds.size(); i++){
					String idStore = allStoresIds.get(i);
					for(int j = 0 ; j < allStoreDirectors.size(); j++){
						User storeDirector;
						try {
							storeDirector = UserLocalServiceUtil.getUser(Integer.parseInt(allStoreDirectors.get(j).get("user_id").toString()));
							if(storeDirector.getAddresses().size() > 0 ){
								if(storeDirector.getAddresses().get(0).getStreet2().equals(idStore)){
									HashMap<String,Object> detail = new HashMap<String,Object>();
									detail.put("email", storeDirector.getEmailAddress());
									detail.put("first_name", storeDirector.getFirstName());
									detail.put("last_name", storeDirector.getLastName());
									aprobatorDetail.add(detail);
								}
							}
						} catch (NumberFormatException | PortalException
								| SystemException e) {
							e.printStackTrace();
						}
					}
				}
				Notifications.insertNotification(String.valueOf(id), aprobatorDetail, notificationFields, NotificationsSubject.INREGISTRARE_FACTURA);
			}
	   	}
	}
	
	
   private HashMap<String, Object> setInsertParameters (String id_template, String to_email, String to_name, String body, String status, 
			String type, String doc_type, User user, String link){
		
		HashMap<String, Object> bodyDetails = new HashMap<String,Object>();
		
		bodyDetails.put("id_template", id_template);
		bodyDetails.put("sender_email", user.getEmailAddress());
		bodyDetails.put("sender_name", user.getFullName());
		bodyDetails.put("body", body);
		String currentTime = new Timestamp(new Date().getTime()).toString();
		bodyDetails.put("time_to_send", currentTime);
		bodyDetails.put("time_of_send", currentTime);
		bodyDetails.put("status", status);
		bodyDetails.put("type", type);
		bodyDetails.put("doc_type", doc_type);
		bodyDetails.put("created", currentTime);
		if ( link.contains("?") ){
			bodyDetails.put("link", link.substring(0, link.indexOf("?")));
		} else {
			bodyDetails.put("link", link);
		}
		return bodyDetails;
	}
	*/
   /*
   private void sendNotificationToFinancialDirector(int id, User user, String invoiceNumber, String invoiceDate, 
			String supplier, String company, String account){
		
		approvers.addAll(UsersInformations.getUserDetailsByTeamId(UserTeamIdUtils.FINANCIAL_DIRECTOR));
		if (currentUserBelongsToGroup(user)){
			approvers.clear();
			approversSize = 0;
			return;
		}
		//set insert parameters
		HashMap<String, Object> notificationFields = new HashMap<String, Object>();
		
		String body = "<tr><td style='border:1px solid black'>" + invoiceNumber + "</td><td style='border:1px solid black'>" 
					+ invoiceDate + "</td><td style='border:1px solid black'>" + 
				supplier + "</td><td style='border:1px solid black'>" + company + "</td></tr>";
		
		notificationFields = setInsertParameters(NotificationTemplates.INREGISTRARE_FACTURA, "", "", body, "0", "B", "1", user, "#");
		
		//insert the notifications
		Notifications.insertNotification(String.valueOf(id), approvers, notificationFields, NotificationsSubject.INREGISTRARE_FACTURA);
		//resert the list
		approvers.clear();
		approversSize = 0;
	}
   */
   /**
	 * 
	 */
	@Override
	public void render(RenderRequest renderRequest, RenderResponse renderResponse) throws PortletException, IOException {
		HttpServletRequest request = PortalUtil.getHttpServletRequest(renderRequest);
		String p_p_id = PortalUtil.getOriginalServletRequest(request).getParameter("p_p_id");
		String path = PortalUtil.getOriginalServletRequest(request).getParameter("_" + p_p_id + "_mvcPath");
   	
		try {
	    	ThemeDisplay themeDisplay = ((ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY));
	    	String portletId = themeDisplay.getPortletDisplay().getId();
	    	PortletPreferences portletSetup = PortletPreferencesFactoryUtil.getLayoutPortletSetup(themeDisplay.getLayout(), portletId);
	    	String portletCustomTitle = themeDisplay.getPortletDisplay().getTitle();
	    	portletCustomTitle = portletSetup.getValue("portletSetupTitle_" + themeDisplay.getLanguageId(), portletCustomTitle);
	    	
	    	User user = UserServiceUtil.getUserById(Long.parseLong(renderRequest.getRemoteUser()));
	    	
	        ThemeDisplay td  = (ThemeDisplay) renderRequest.getAttribute(WebKeys.THEME_DISPLAY);
	        log.info("[PROFLUO] Facturi blocate portlet - render()");
	        log.info("[PROFLUO] User full name: " + td.getUser().getFullName());
	        log.info("[PROFLUO] Portlet Title: " + portletCustomTitle);
	        
	    	int id_user 			= (int) user.getUserId();
	    	log.info("[PROFLUO] User id " + id_user);
	    	//set on request the sql statement type, stage and page
	    	
	    	List<Integer> types = InvoiceStages.getSqlStatementType(user, td.getURLCurrent());
	    	String stage = InvoiceStages.getStage(user, td.getURLCurrent(), InvoiceStages.REJECTED);
	    	String page = InvoiceStages.getPageLayout(td.getURLCurrent());
	    	
	    	
	    	String allTypes = "";
	    	for(Integer type : types ){
	    		allTypes += type + ",";
	    	}
	    	
	    	if(allTypes.endsWith(",")){
				allTypes = allTypes.substring(0, allTypes.length()-1);
			}
	    	
	    	renderRequest.setAttribute("page-layout", page);
			renderRequest.setAttribute("page-status", stage);
			renderRequest.setAttribute("type", allTypes);
			
			log.info("[PROFLUO] SQL TYPE : " + allTypes);
			log.info("[PROFLUO] STAGE : " + stage);
			log.info("[PROFLUO] PAGE LAYOUT : " + page);
			log.info("[PROFLUO] PAGE  : " + td.getURLCurrent());
	    	if (path == null || path.equals("view.jsp")) {
	    		log.info("[PROFLUO] Facturi blocate : LISTING render()");

	    	}
	   	} catch (Exception e) {
	   		e.printStackTrace();
	   	}
	   	super.render(renderRequest, renderResponse);
	} 
}
