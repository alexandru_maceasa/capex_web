package com.profluo.ec.carrefour.portlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.List;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletConfig;
import javax.portlet.PortletException;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.portlet.LiferayPortletConfig;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.servlet.SessionMessages;
import com.liferay.portal.kernel.util.JavaConstants;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.util.bridges.mvc.MVCPortlet;
import com.profluo.ecm.controller.ControllerUtils;
import com.profluo.ecm.model.db.DatabaseConnectionManager;
import com.profluo.ecm.utils.ExcelUtil;

/**
 * Portlet implementation class ArticoleHaineLucru
 */
public class ArticoleHaineLucru extends MVCPortlet {
	
	/**
	 * Generic log instance.
	 */
	private final Log log = LogFactoryUtil.getLog("PROFLUO-" + ArticoleHaineLucru.class.getName());
 
	@SuppressWarnings("unchecked")
	public void serveResource(ResourceRequest resourceRequest, ResourceResponse resourceResponse) throws IOException, PortletException {
		String action = ParamUtil.getString(resourceRequest, "action");
		
		if (action.equals("filter") || action.equals("next") || action.equals("prev") || action.equals("last") || action.equals("first")) {
			PrintWriter writer = resourceResponse.getWriter();
			int start = Integer.parseInt(ParamUtil.getString(resourceRequest, "start").toString());
			int count = Integer.parseInt(ParamUtil.getString(resourceRequest, "count").toString());
			int total = Integer.parseInt(ParamUtil.getString(resourceRequest, "total").toString());
			String denumireArticol = ParamUtil.getString(resourceRequest, "denumireArticol").toString();

			List<HashMap<String, Object>> allLines = null;
			
			allLines = com.profluo.ecm.model.db.ArticoleHaineLucru.getAllWorkingArticles(start, count, denumireArticol);
			total = com.profluo.ecm.model.db.ArticoleHaineLucru.getAllWorkingArticlesCount(denumireArticol);

			JSONArray jsonLines = new JSONArray();
			if (allLines != null) {
				jsonLines = DatabaseConnectionManager.convertListToJson(allLines);
			}
			JSONObject jsonResponse = new JSONObject();

			jsonResponse.put("action", action);
			jsonResponse.put("start", start);
			jsonResponse.put("count", count);
			jsonResponse.put("total", total);
			jsonResponse.put("values", jsonLines);
			writer.print(jsonResponse.toJSONString());
		} else if (action.equals("exportExcel")) {
			List<HashMap<String,Object>> allWorkingArticles = com.profluo.ecm.model.db.ArticoleHaineLucru.getAllWorkingArticles(0, 50000000, "");
			
			String[][] header = new String[6][];
			for (int i = 0; i < 6; i++) {
				header[i] = new String[2];
			}
			
			header[0][0] = "codArticol";
			header[0][1] = "Cod Articol";
			header[1][0] = "denumire";
			header[1][1] = "Denumire";
			header[2][0] = "codProdus";
			header[2][1] = "Cod Produs";
			header[3][0] = "denumireProdus";
			header[3][1] = "Denumire Produs";
			header[4][0] = "pret";
			header[4][1] = "Pret";
			header[5][0] = "furnizor";
			header[5][1] = "Furnizor";
			
			try {
				ExcelUtil.getExcel(allWorkingArticles, header, "Raport_Articole_haine_lucru", resourceResponse);
			} catch (IOException e) {
				e.printStackTrace();
			}
		
		} 
	}
	
	public void editArticle(ActionRequest request, ActionResponse response) {
		// remove default error message from Liferay
		PortletConfig portletConfig = (PortletConfig) request.getAttribute(JavaConstants.JAVAX_PORTLET_CONFIG);
		LiferayPortletConfig liferayPortletConfig = (LiferayPortletConfig) portletConfig;
		SessionMessages.add(request, liferayPortletConfig.getPortletId() + SessionMessages.KEY_SUFFIX_HIDE_DEFAULT_ERROR_MESSAGE);
				
		String articleCode = request.getParameter("product_code");
		String productId = request.getParameter("product_id");

		System.out.println(articleCode + " -> " + productId);
		
		int succesUpdate = com.profluo.ecm.model.db.ArticoleHaineLucru.updateWorkingArticle(productId, articleCode);
		if (succesUpdate > 0 ){
			SessionMessages.add(request, "updateOK");
			response.setRenderParameter("mvcPath", "/html/articolehainelucru/view.jsp");
		} else {
			SessionErrors.add(request, "updateNOK");
			ControllerUtils.saveRequestParamsOnResponse(request, response, log);
			response.setRenderParameter("mvcPath", "/html/articolehainelucru/inregistrare_articol.jsp");
		}
		
	}

}
