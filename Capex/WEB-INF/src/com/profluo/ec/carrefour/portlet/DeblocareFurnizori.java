package com.profluo.ec.carrefour.portlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletException;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.model.User;
import com.liferay.portal.service.UserServiceUtil;
import com.liferay.util.bridges.mvc.MVCPortlet;
import com.profluo.ecm.flows.NotificationTemplates;
import com.profluo.ecm.flows.NotificationsSubject;
import com.profluo.ecm.model.db.DatabaseConnectionManager;
import com.profluo.ecm.model.db.DefSupplier;
import com.profluo.ecm.notifications.Notifications;

/**
 * Portlet implementation class DeblocareFurnizori
 */
public class DeblocareFurnizori extends MVCPortlet {
	
	private final Log log = LogFactoryUtil.getLog("PROFLUO-" + DeblocareFurnizori.class.getName());
	
 
	public void updateStage(ActionRequest request, ActionResponse response) {
		
		log.info("[PROFLUO] DeblocareFurnizori portlet - update stage()");
		
		//getting ids to update
		String id = request.getParameter("idToUpdate");
		String [] allIdsToUpdate = id.split(",");
		
		for(String eachId : allIdsToUpdate){
			//set blocked to 0 (unlocked) and blocked status to 0(unlock)
			DefSupplier.updateBlockedStatusById(eachId, "0", "0");
		}
		
		User user = null;
		try {
			user = UserServiceUtil.getUserById(Long.parseLong(request.getRemoteUser()));
		} catch (NumberFormatException | PortalException | SystemException e) {
			e.printStackTrace();
		}
		String body = "Buna ziua, <br/> <br/> Va informam ca a fost aprobata solicitarea de deblocare furnizor in Kontan. <br/> Va rugam sa "
				+ " integrati fisierul generat. <br/> <br/> O zi buna! ";
		
		HashMap<String, Object> bodyDetails = new HashMap<String,Object>();
		bodyDetails.put("id_template", NotificationTemplates.DEBLOCARE_FURNIZORI);
		bodyDetails.put("sender_email", user.getEmailAddress());
		bodyDetails.put("sender_name", user.getFullName());
		bodyDetails.put("body", body);
		String currentTime = new Timestamp(new Date().getTime()).toString();
		bodyDetails.put("time_to_send", currentTime);
		bodyDetails.put("time_of_send", currentTime);
		bodyDetails.put("status", "0");
		bodyDetails.put("type", "S");
		bodyDetails.put("doc_type", "1");
		bodyDetails.put("created", currentTime);
		bodyDetails.put("link", "");

		List<HashMap<String,Object>> userDetails = new ArrayList<HashMap<String,Object>> ();
		HashMap<String, Object> hash = new HashMap<String, Object>();
		hash.put("email", "magdalena_draghici@carrefour.com");
		hash.put("first_name", "Magda");
		hash.put("last_name", "Draghici");
		userDetails.add(hash);
		Notifications.insertNotification("", userDetails, bodyDetails, NotificationsSubject.DEBLOCARE_FURNIZORI);
		
	}
	
	@SuppressWarnings("unchecked")
	public void serveResource(ResourceRequest resourceRequest,ResourceResponse resourceResponse) 
			throws IOException,PortletException {
		log.info("[AJAX] DeblocareFurnizori Controller");
		String action = ParamUtil.getString(resourceRequest, "action");
		log.info("[AJAX] action: " + action);
		PrintWriter writer = resourceResponse.getWriter();
		
		if (action.equals("filter")) {
			String supplierName = ParamUtil.getString(resourceRequest, "supplierName");
			List<HashMap<String,Object>> allBlockedSuppliers = DefSupplier.getBlockedSuppliersFiltered(supplierName);
			
			JSONArray jsonSuppliers = new JSONArray();
			if (allBlockedSuppliers != null) {
				jsonSuppliers = DatabaseConnectionManager.convertListToJson(allBlockedSuppliers);
			}
			JSONObject jsonResponse = new JSONObject();

			jsonResponse.put("action", action);
			jsonResponse.put("values", jsonSuppliers);
			writer.print(jsonResponse.toJSONString());
		}
	}

}
