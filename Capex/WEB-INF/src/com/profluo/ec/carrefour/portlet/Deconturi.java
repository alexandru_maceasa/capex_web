package com.profluo.ec.carrefour.portlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletConfig;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.portlet.LiferayPortletConfig;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.servlet.SessionMessages;
import com.liferay.portal.kernel.util.JavaConstants;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.model.User;
import com.liferay.portal.service.UserServiceUtil;
import com.liferay.util.bridges.mvc.MVCPortlet;
import com.profluo.ecm.controller.ControllerUtils;
import com.profluo.ecm.controller.InventoryNumberBO;
import com.profluo.ecm.model.db.CashExpense;
import com.profluo.ecm.model.db.DPIHeader;
import com.profluo.ecm.model.db.DatabaseConnectionManager;
import com.profluo.ecm.model.db.GeneralCodes;
import com.profluo.ecm.model.db.GenericDBStatements;
import com.profluo.ecm.model.vo.DefIASVo;
import com.profluo.ecm.model.vo.DefIFRSVo;
import com.profluo.ecm.model.vo.DefProductVo;

/**
 * Portlet implementation class Deconturi
 */
public class Deconturi extends MVCPortlet {
	
	private final Log log = LogFactoryUtil.getLog("PROFLUO-" + Deconturi.class.getName());
	
	@SuppressWarnings("unchecked")
	public void serveResource(ResourceRequest resourceRequest, ResourceResponse resourceResponse) {	
		String action = ParamUtil.getString(resourceRequest, "action");
		PrintWriter writer = null;
		try {
			writer = resourceResponse.getWriter();
		} catch (IOException e) {
			e.printStackTrace();
		}
		if (action.equals("filter") || action.equals("next") || action.equals("prev") || action.equals("last") || action.equals("first")) {
			log.info("Deconturi Controller -> Filtrare");
			int start = Integer.parseInt(ParamUtil.getString(resourceRequest, "start").toString());
			int count = Integer.parseInt(ParamUtil.getString(resourceRequest, "count").toString());
			int total = Integer.parseInt(ParamUtil.getString(resourceRequest, "total").toString());
			String docNo = ParamUtil.getString(resourceRequest, "docNo");
			String cuiFurnizor = ParamUtil.getString(resourceRequest, "cuiFurnizor");
			String numeFurnizor = ParamUtil.getString(resourceRequest, "numeFurnizor");
			String moneda = ParamUtil.getString(resourceRequest, "moneda");
			String compania = ParamUtil.getString(resourceRequest, "compania");

			List<HashMap<String, Object>> allLines = null;

			//gettin all filtered
			allLines =  CashExpense.getAllFiltered(start, count, docNo, cuiFurnizor, numeFurnizor, moneda, compania);
			//recalculate total
			total = CashExpense.getAllFilteredCount(docNo, cuiFurnizor, numeFurnizor, moneda, compania);
			
			JSONArray jsonLines = new JSONArray();
			if (allLines != null) {
				jsonLines = DatabaseConnectionManager.convertListToJson(allLines);
			}
			JSONObject jsonResponse = new JSONObject();

			jsonResponse.put("action", action);
			jsonResponse.put("start", start);
			jsonResponse.put("count", count);
			jsonResponse.put("total", total);
			jsonResponse.put("values", jsonLines);
			writer.print(jsonResponse.toJSONString());
		} else if (action.equals("getProdInfo")) {
    		// ajax to get product info for one row based on id of the selected product
    		String id = ParamUtil.getString(resourceRequest, "prodId").toString();
    		List<HashMap<String,Object>> product = DefProductVo.getById(id);
	    	JSONArray jsonProduct = new JSONArray();
	    	if (product != null) {
	    		jsonProduct = DatabaseConnectionManager.convertListToJson(product);
	    	}
	    	JSONObject jsonResponse = new JSONObject();
			jsonResponse.put("product", jsonProduct);
			writer.print(jsonResponse.toJSONString());
    	} else if (action.equals("getIasInfo")){ 
    		String id = ParamUtil.getString(resourceRequest, "iasId").toString();
    		List<HashMap<String, Object>> product = DefIASVo.getById(id);
			JSONArray jsonProduct = new JSONArray();
			if (product != null) {
				jsonProduct = DatabaseConnectionManager.convertListToJson(product);
			}
			JSONObject jsonResponse = new JSONObject();
			jsonResponse.put("product", jsonProduct);
			//System.out.println(jsonProduct);
			writer.print(jsonResponse.toJSONString());
    	} else if (action.equals("getIfrsInfo")){
			String id = ParamUtil.getString(resourceRequest, "ifrsId").toString();
			List<HashMap<String, Object>> product = DefIFRSVo.getById(id);
			JSONArray jsonProduct = new JSONArray();
			if (product != null) {
				jsonProduct = DatabaseConnectionManager.convertListToJson(product);
			}
			JSONObject jsonResponse = new JSONObject();
			jsonResponse.put("product", jsonProduct);
			//System.out.println(jsonProduct);
			writer.print(jsonResponse.toJSONString());
		} else if (action.equals("showDpi")) {
			String idDpi = ParamUtil.getString(resourceRequest, "dpi");
			System.out.println("ID Dpi -> " + idDpi);
			// get DPI header info based on DPI id
			List<HashMap<String, Object>> oneDpi = null;

			// oneDpi = GenericDBStatements.getAllByFieldName("dpi_header",  "id", idDpi);
			oneDpi = DPIHeader.getAllByFieldName("id", idDpi);

			JSONArray jsonDpiHeader = new JSONArray();
			if (oneDpi != null) {
				jsonDpiHeader = DatabaseConnectionManager.convertListToJson(oneDpi);
			}
			System.out.println(jsonDpiHeader);
			// get DPI lines info based on DPI id
			List<HashMap<String, Object>> dpiLines = null;

			dpiLines = GenericDBStatements.getAllByFieldName("dpi_line", "id_dpi", idDpi);

			JSONArray jsonDpiLines = new JSONArray();
			if (dpiLines != null) {
				jsonDpiLines = DatabaseConnectionManager.convertListToJson(dpiLines);
			}

			// create JSON response
			JSONObject jsonResponse = new JSONObject();
			jsonResponse.put("header", jsonDpiHeader);
			jsonResponse.put("lines", jsonDpiLines);
			// send AJAX response
			writer.print(jsonResponse.toJSONString());
    	} else if (action.equals("getInventoryNo")) {
    		String stores = ParamUtil.getString(resourceRequest, "storeId").toString();
    		int storeId = 0;
    		if(stores.contains(",")) {
    			storeId = Integer.parseInt(stores.split(",")[0]);
    		} else {
    			storeId = Integer.parseInt(stores);
    		}
    		int iasId 	= Integer.parseInt(ParamUtil.getString(resourceRequest, "iasId").toString());
    		int tipInv 	= Integer.parseInt(ParamUtil.getString(resourceRequest, "tipInv").toString());
    		int count 	= Integer.parseInt(ParamUtil.getString(resourceRequest, "count").toString());
    		
    		//System.out.println("Controler -> " + storeId + " -> " + iasId + " -> " + tipInv + " -> " + count);
    		
    		List<String> listInvNo = null;
    		
    		if (iasId != 0 && storeId != 0 && tipInv != 0) {
    			if (tipInv == InventoryNumberBO.DE_GRUP) {
    				listInvNo = InventoryNumberBO.getInvetoryNumberSequence(storeId, iasId, 1);
    			} else if (tipInv == InventoryNumberBO.INDIVIDUAL) {
    				listInvNo = InventoryNumberBO.getInvetoryNumberSequence(storeId, iasId, count);
    			}
    		}
    		
    		JSONArray jsonInvNo = new JSONArray();
    		if (listInvNo != null) {
    			for (int i = 0; i < listInvNo.size(); i++) {
    				JSONObject jsonLine = new JSONObject();
    				jsonLine.put("nr_inv", listInvNo.get(i).toString());
    				jsonLine.put("data_pif", "");
    				jsonLine.put("receptie", "");
    				jsonInvNo.add(jsonLine);
    			}
    			writer.print(jsonInvNo.toJSONString());
    		} else {
    			writer.print("");
    		}
    	} else if (action.equals("delete_line")) {
    		String lineId = ParamUtil.getString(resourceRequest, "line_id").toString();
    		try {
				User user = UserServiceUtil.getUserById(Long.parseLong(resourceRequest.getRemoteUser()));
				String arrFields [][] = new String[3][2];
	    		arrFields[0][0] = "cash_exp_line_id";
	    		arrFields[0][1] = lineId;
	    		arrFields[1][0] = "cash_exp_store_line_id";
	    		arrFields[1][1] = "0";
	    		arrFields[2][0] = "user_id";
	    		arrFields[2][1] = String.valueOf(user.getUserId());
	    		GenericDBStatements.insertEntry(arrFields, "log_deleted_lines_cash_exp", new ArrayList<String>());
			} catch (NumberFormatException | PortalException | SystemException e) {
				e.printStackTrace();
			}
    		GenericDBStatements.deleteEntry("cashexp_item_lines", "id", lineId);
    		writer.print("success");
    	}
    	else if (action.equals("delete_store_line")) {
    		String storeLineId = ParamUtil.getString(resourceRequest, "store_line_id").toString();
    		try {
				User user = UserServiceUtil.getUserById(Long.parseLong(resourceRequest.getRemoteUser()));
				String arrFields [][] = new String[3][2];
	    		arrFields[0][0] = "cash_exp_line_id";
	    		arrFields[0][1] = "0";
	    		arrFields[1][0] = "cash_exp_store_line_id";
	    		arrFields[1][1] = storeLineId;
	    		arrFields[2][0] = "user_id";
	    		arrFields[2][1] = String.valueOf(user.getUserId());
	    		GenericDBStatements.insertEntry(arrFields, "log_deleted_lines_cash_exp", new ArrayList<String>());
			} catch (NumberFormatException | PortalException | SystemException e) {
				e.printStackTrace();
			}
    		GenericDBStatements.deleteEntry("cashexp_item_store_lines", "id", storeLineId);
    		writer.print("success");
    	} else if (action.equals("getInventoryDetails")){
			String idCashexp = ParamUtil.getString(resourceRequest, "idCashexp").toString();
			List<HashMap<String,Object>> inventoryDetails = CashExpense.getInventoryDetailsByIdCashexpStoreLine(idCashexp);
			JSONArray jsonInvNo = new JSONArray();
			if (inventoryDetails != null) {
				for (int i = 0; i < inventoryDetails.size(); i++) {
					JSONObject jsonLine = new JSONObject();
					jsonLine.put("nr_inv", inventoryDetails.get(i).get("inventory_no").toString());
					jsonLine.put("data_pif", inventoryDetails.get(i).get("pif_date").toString());
					jsonLine.put("receptie", inventoryDetails.get(i).get("reception").toString());
					jsonInvNo.add(jsonLine);
				}
				writer.print(jsonInvNo.toJSONString());
			} else {
				writer.print("");
			}
			
		}
	}
	
	public void addCashExpense(ActionRequest request, ActionResponse response) {
		// remove default error message from Liferay
		PortletConfig portletConfig = (PortletConfig) request.getAttribute(JavaConstants.JAVAX_PORTLET_CONFIG);
		LiferayPortletConfig liferayPortletConfig = (LiferayPortletConfig) portletConfig;
		SessionMessages.add(request, liferayPortletConfig.getPortletId() + SessionMessages.KEY_SUFFIX_HIDE_DEFAULT_ERROR_MESSAGE);
		
		log.info("[PROFLUO] Deconturi portlet - addCashExpense()");
		try {
			User user 		=  UserServiceUtil.getUserById(Long.parseLong(request.getRemoteUser()));
			String cashExpId	= request.getParameter("id_cash_exp").toString();
			String[][] arrFieldsHeader = this.getSqlHeaderCashExpenseParams(user, request, response);
//			for(int i = 0 ; i < 19; i++) {
//				System.out.println(arrFieldsHeader[i][0] + " -> " + arrFieldsHeader[i][1]);
//			}
			// update attrs for where statement
			ArrayList<String> whereStatement = new ArrayList<String>();
			// attrs values for where statement
			ArrayList<String> whereStatementValues = new ArrayList<String>();
			whereStatement.add("id");
			whereStatementValues.add(cashExpId);
			//update cash expense header
			int noInserted = GenericDBStatements.updateEntry(arrFieldsHeader, "cashexp_header", new ArrayList<String>(), whereStatement, whereStatementValues);
			if (noInserted <= 0) {
				SessionErrors.add(request, "update_nok");
				ControllerUtils.saveRequestParamsOnResponse(request, response, log);
				response.setRenderParameter("mvcPath", "/html/deconturi/view.jsp");
			} else {
				String[][][] lineDataArticles = null;
				String[][][] lineData = null;
				String dataTableData = request.getParameter("datatable_data");
				String dataTableArticles = request.getParameter("datatable_lines");
				//liniile documentului
				lineData = ControllerUtils.convertToArrayMatrixFromJSON(dataTableData);
				//store line-urile documentului
				lineDataArticles = ControllerUtils.convertToArrayMatrixFromJSON(dataTableArticles);
				
				ArrayList<String> lineExcludes = this.setLinesExcludes();
				boolean isNewLine = false;
				String lineId = "";
				for(int i = 0 ; i < lineData.length; i++ ){
					for(int j = 0 ; j < lineData[i].length; j++ ) {
						if(lineData[i][j][0].equals("is_new_line")) {
							if(lineData[i][j][1].equals("1")) {
								isNewLine = true;
							}
						}
						if(lineData[i][j][0].equals("cash_exp_line_id")) {
							lineId = lineData[i][j][1];
						}
					}
					int resultedID = 0;
					if(isNewLine) {
						//insert new line
						resultedID = GenericDBStatements.insertEntry(lineData[i], "cashexp_item_lines", lineExcludes);
						//insert store lines
						for (int j = 0; j < lineDataArticles.length; j++) {
							this.insertProperStoreLines(lineData[i], lineDataArticles[j], resultedID, cashExpId);
						}
					} else {
						whereStatement.clear();
						whereStatementValues.clear();
						whereStatement.add("id");
						whereStatement.add("id_cash_exp");
						whereStatementValues.add(lineId);
						whereStatementValues.add(cashExpId);
						//update existing line
						resultedID = GenericDBStatements.updateEntry( lineData[i], "cashexp_item_lines", lineExcludes, whereStatement, whereStatementValues);
						//insert store lines
						for (int j = 0; j < lineDataArticles.length; j++) {
							this.insertProperStoreLines(lineData[i], lineDataArticles[j], resultedID, cashExpId);
						}
					}
					if(resultedID == 0 ) {
						SessionErrors.add(request, "update_nok");
						ControllerUtils.saveRequestParamsOnResponse(request, response, log);
						response.setRenderParameter("mvcPath", "/html/deconturi/view.jsp");
					}
				}
			}
		} catch (NumberFormatException | PortalException | SystemException e) {
			e.printStackTrace();
			SessionErrors.add(request, "error");
			response.setRenderParameter("mvcPath", "/html/deconturi/view.jsp");
		}
	}
	
	private void insertProperStoreLines(String [][] lines, String [][] storeLines, int resultedLineId, String cashExpId)  {
		boolean isGoodStoreLine = false;
		boolean isNew = false;
		String lineId = "";
		String storeLineId = "";
		for(int i = 0 ; i < lines.length; i++ ) {
			if(lines[i][0].equals("cash_exp_line_id")) {
				lineId = lines[i][1];
				for(int j = 0; j < storeLines.length; j++ ){
					if(storeLines[j][0].equals("line")) {
						if(storeLines[j][1].equals(lineId)) {
							isGoodStoreLine = true;
						}
					}
				}
			}
		}
		if(isGoodStoreLine) {
			for(int j = 0; j < storeLines.length; j++ ){
				if(storeLines[j][0].equals("id_cash_exp_line")) {
					if(storeLines[j][1].equals("")) {
						storeLines[j][1] = String.valueOf(resultedLineId);
					}
				}
				if(storeLines[j][0].equals("id_store_line")) {
					storeLineId = storeLines[j][1];
				}
			}
			
			
			if(storeLineId.equals("")) {
				isNew = true;
			}
			
			ArrayList<String> storeLineExcludes = this.setStoreLinesExcludes();
			int storeLineSuccess = 0;
			if(isNew) {
				storeLineSuccess = GenericDBStatements.insertEntry(storeLines, "cashexp_item_store_lines", storeLineExcludes);
			} else {
				ArrayList<String> whereStatement = new ArrayList<String>();
				ArrayList<String> whereStatementValues = new ArrayList<String>();
				whereStatement.add("id");
				whereStatementValues.add(storeLineId);
				GenericDBStatements.updateEntry(storeLines, "cashexp_item_store_lines", storeLineExcludes, whereStatement, whereStatementValues);
				storeLineSuccess = Integer.parseInt(storeLineId);
			}
			if(storeLineSuccess != 0) {
				this.insertInventoryNo(storeLineSuccess, cashExpId, storeLines);
			}
		}
	}
	
	private void insertInventoryNo(int idStoreLine, String idCashExp, String [][] info){
		String[][] arrFields = new String[5][2];
		arrFields[0][0] = "id_cashexp_store_line";
		arrFields[0][1] = String.valueOf(idStoreLine);
		arrFields[1][0] = "id_cashexp";
		arrFields[1][1] = String.valueOf(idCashExp);
		arrFields[2][0] = "inventory_no";
		arrFields[3][0] = "pif_date";
		arrFields[4][0] = "reception";
		String inventory = "";
		for(int i = 0 ; i < info.length; i++ ){
			if(info[i][0].equals("inventory")) {
				inventory = info[i][1];
			}
		}
		Object obj = JSONValue.parse(inventory);
		JSONArray jsonArr = (JSONArray) obj;
		for (int k = 0; k < jsonArr.size(); k++) {
			String pifDate = ((JSONObject) jsonArr.get(k)).get("data_pif").toString().replace("[\"", "").replace("\"]", "");
			if(pifDate.startsWith("[")) {
				pifDate = pifDate.substring(1);
			}
			if(pifDate.endsWith("]")){
				pifDate = pifDate.substring(0, pifDate.length()-1);
			}
			arrFields[3][1] = pifDate;
			arrFields[2][1] = ((JSONObject) jsonArr.get(k)).get("nr_inv").toString();
			arrFields[4][1] = ((JSONObject) jsonArr.get(k)).get("receptie").toString();

			ArrayList<String> excludedFieldsInv = new ArrayList<String>();
			GenericDBStatements.insertEntry(arrFields, "cashexp_item_lines_inv", excludedFieldsInv);
		}
		
	}
	
	private ArrayList<String> setLinesExcludes() {
		// attributes to be ignored for table invoice_line
		ArrayList<String> excludes = new ArrayList<String>();
		String[] items = { "cont_imob_in_curs", "detalii", "clasif", "counter", "delete_line", "cont_imob_avans", "cont_imob_in_fct", "lot", "is_new_line", "cash_exp_line_id"};
		for (int i = 0; i < items.length; i++) {
			excludes.add(items[i]);
		}
		return excludes;
	}
	
	private ArrayList<String> setStoreLinesExcludes() {
		// attributes to be ignored for table invoice_line
		ArrayList<String> excludes = new ArrayList<String>();
		String[] items = { "vizualizare_dpi", "delete_store_line", "line", "cont_imob_in_curs", "detalii", "id_store_1", "id_store_2",
				"id_store_3", "id_store_4", "id_store_5", "id_store_6", "id_store_7", "id_store_8", "id_store_9", "id_store_line", "cont_imob_in_fct", "inventory","cont_imob_avans" };
		for (int i = 0; i < items.length; i++) {
			excludes.add(items[i]);
		}
		return excludes;
	}
	
	private String [][] getSqlHeaderCashExpenseParams(User user, ActionRequest request, ActionResponse response){
		int arrSize = 26;
		String arrFields [][] = new String [arrSize][2];
		for (int i = 0; i < arrSize; i++) {
			arrFields[i] = new String[2];
		}
		arrFields[0][0] = "cash_exp_no";
		arrFields[0][1] = request.getParameter("cash_exp_no").toString();
		arrFields[1][0] = "cash_exp_date";
		arrFields[1][1] = request.getParameter("cash_exp_date").toString();
		arrFields[2][0] = "due_date";
		arrFields[2][1] = request.getParameter("datascadenta").toString();
		arrFields[3][0] = "id_company";
		arrFields[3][1] = request.getParameter("company").toString();
		arrFields[4][0] = "warranty";
		arrFields[4][1] = request.getParameter("procent_garantie").toString();
		arrFields[5][0] = "currency";
		String currency = request.getParameter("currency").toString();
		arrFields[5][1] = currency;
		arrFields[6][0] = "val_no_vat_ron";
		String valNoVatRon = request.getParameter("val_no_vat_ron").toString();
		if(valNoVatRon.contains(",")) {
			valNoVatRon = valNoVatRon.replace(",", "");
		}
		arrFields[6][1] = valNoVatRon;
		arrFields[7][0] = "val_vat_ron";
		String valVatRon = request.getParameter("val_vat_ron").toString();
		if(valVatRon.contains(",")) {
			valVatRon = valNoVatRon.replace(",", "");
		}
		arrFields[7][1] = valVatRon;
		arrFields[8][0] = "total_with_vat_ron";
		String totalRon = request.getParameter("val_with_vat_ron").toString();
		if(totalRon.contains(",")) {
			totalRon = valNoVatRon.replace(",", "");
		}
		arrFields[8][1] = totalRon;
		if(currency.equals("RON")) {
			arrFields[9][0] = "val_no_vat_curr";
			arrFields[9][1] = arrFields[6][1];
			arrFields[10][0] = "val_vat_curr";
			arrFields[10][1] = arrFields[7][1];
			arrFields[11][0] = "total_with_vat_curr";
			arrFields[11][1] = arrFields[8][1];
		} else {
			arrFields[9][0] = "val_no_vat_curr";
			arrFields[9][1] = request.getParameter("val_no_vat_curr").toString().replace(",", "");
			arrFields[10][0] = "val_vat_curr";
			arrFields[10][1] = request.getParameter("val_vat_curr").toString().replace(",", "");
			arrFields[11][0] = "total_with_vat_curr";
			arrFields[11][1] = request.getParameter("val_with_vat_curr").toString().replace(",", "");
		}
		arrFields[12][0] = "exchange_rate";
		arrFields[12][1] = request.getParameter("rata-schimb").toString();
		arrFields[13][0] = "vat_code";
		arrFields[13][1] = request.getParameter("cota_tva").toString();
		
		arrFields[14][0] = "kontan_acc";
		//TODO ???!?!?!??
		arrFields[14][1] = "0";
		arrFields[15][0] = "stage";
		arrFields[15][1] = "2";
		arrFields[16][0] = "id_user";
		arrFields[16][1] = String.valueOf(user.getUserId());
		arrFields[17][0] = "accounting_month";
		arrFields[17][1] = GeneralCodes.getAccountingMonth();
		arrFields[18][0] = "accounting_year";
		arrFields[18][1] = GeneralCodes.getAccountingYear();
		arrFields[19][0] = "tip_capex";
		arrFields[19][1] = request.getParameter("categ_capex").toString();
		
		arrFields[20][0] = "id_new_prj";
		// categoria capex este proiect nou
		if (arrFields[19][1].equals("2")) {
			String proiect = request.getParameter("project").toString();
			if ( proiect == "" ){
				proiect = "0";
			}
			arrFields[20][1] = proiect;
		} else {
			arrFields[20][1] = "0";
		}
		arrFields[21][0] = "id_initiative";
		// categoria capex este lista initiativa
		if (arrFields[19][1].equals("1")) {
			String initiativa = request.getParameter("lista_initiative").toString();
			if ( initiativa == "" ){
				initiativa = "0";
			}
			arrFields[21][1] = initiativa;
		} else {
			arrFields[21][1] = "0";
		}
		arrFields[22][0] = "reg_date";
		Calendar calendar = Calendar.getInstance();
		java.sql.Timestamp currentTS = new java.sql.Timestamp(calendar.getTime().getTime());
		arrFields[22][1] = currentTS.toString();
		arrFields[23][0] = "tip_rel_com";
		arrFields[23][1] = request.getParameter("tip-rel-comerciala").toString();
		arrFields[24][0] = "nr_contract";
		if(request.getParameter("nr-contract") != null && !request.getParameter("nr-contract").toString().equals("")) {
			arrFields[24][1] = request.getParameter("nr-contract").toString();
		} else {
			arrFields[24][1] = "0";
		}
		arrFields[25][0] = "nr_comanda";
		if(request.getParameter("nr-comanda") != null && !request.getParameter("nr-comanda").toString().equals("")) {
			arrFields[25][1] = request.getParameter("nr-comanda").toString();
		} else {
			arrFields[25][1] = "0";
		}
		return arrFields;
	}
}