package com.profluo.ec.carrefour.portlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.List;

import javax.portlet.PortletException;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.util.bridges.mvc.MVCPortlet;
import com.profluo.ecm.export.ExportInvoiceToKontan;
import com.profluo.ecm.model.db.DatabaseConnectionManager;
import com.profluo.ecm.model.db.RaportSituatieInregistrari;
import com.profluo.ecm.utils.ExcelUtil;

/**
 * Portlet implementation class FacturiLetrateKontan
 */
public class FacturiLetrateKontan extends MVCPortlet {
	

	

	private final Log log = LogFactoryUtil.getLog("PROFLUO-" + FacturiLetrateKontan.class.getName());

	@Override
	public void serveResource(ResourceRequest resourceRequest, ResourceResponse resourceResponse) throws IOException, PortletException {
		log.info("[AJAX] FacturiLetrateKontan");

		String action = ParamUtil.getString(resourceRequest, "action");
		if (action.equals("filter") || action.equals("next")|| action.equals("prev") || action.equals("last")|| action.equals("first")) {
			PrintWriter writer = resourceResponse.getWriter();
			int start = Integer.parseInt(ParamUtil.getString(resourceRequest, "start").toString());
			int count = Integer.parseInt(ParamUtil.getString(resourceRequest, "count").toString());
			int total = Integer.parseInt(ParamUtil.getString(resourceRequest, "total").toString());
			String invoiceNo = ParamUtil.getString(resourceRequest,"InvoiceNo");
			String furnizor = ParamUtil.getString(resourceRequest, "supplier");

			List<HashMap<String, Object>> allLines = null;

			allLines = RaportSituatieInregistrari.getFacturiLetrateWithLimit(start, count, invoiceNo, furnizor);
			if (total == 0) {
				total = RaportSituatieInregistrari.getAllFacturiLetrateTotal();
			}

			JSONArray jsonLines = new JSONArray();
			if (allLines != null) {
				jsonLines = DatabaseConnectionManager.convertListToJson(allLines);
			}
			JSONObject jsonResponse = new JSONObject();

			jsonResponse.put("action", action);
			jsonResponse.put("start", start);
			jsonResponse.put("count", count);
			jsonResponse.put("total", total);
			jsonResponse.put("values", jsonLines);
			writer.print(jsonResponse.toJSONString());
		
		}
				
		if (action.equals("exportExcel")) {
			String lunaStart = ParamUtil.getString(resourceRequest, "start_date_luna");
			if(Integer.parseInt(lunaStart) < 10) {
				lunaStart = "0" + lunaStart;
			}
			
			String lunaEnd = ParamUtil.getString(resourceRequest, "end_date_luna");
			if(Integer.parseInt(lunaEnd) < 10) {
				lunaEnd = "0" + lunaEnd;
			}
			
			String fromDate = ParamUtil.getString(resourceRequest, "start_date_an") + "-" + lunaStart + "-01";
			String toDate =  ParamUtil.getString(resourceRequest, "end_date_an") + "-" + lunaEnd + "-01";
			System.out.println(fromDate + " si data de sfarsit:" + toDate+ "si action:"+action);
			List<HashMap<String, Object>> allCategories = RaportSituatieInregistrari.getAllFacturiLetrate(fromDate, toDate);
						
			String[][] header = new String[10][];
			for (int i = 0; i < 10; i++) {
				header[i] = new String[2];
			}
			
			header[0][0] = "societate";
			header[0][1] = "Societate";
			header[1][0] = "magazin";
			header[1][1] = "Magazin";
			header[2][0] = "numar_factura";
			header[2][1] = "Numar factura";
			header[3][0] = "data_factura";
			header[3][1] = "Data factura";
			header[4][0] = "furnizor";
			header[4][1] = "Furnizor";
			header[5][0] = "total_with_vat_curr";
			header[5][1] = "Valoare factura";
			header[6][0] = "reg_date";
			header[6][1] = "Data inregistrare";
			header[7][0] = "tip_letraj";
			header[7][1] = "Tip plata";
			header[8][0] = "valoare_platita";
			header[8][1] = "Valoare platita";
			header[9][0] = "data_plata";
			header[9][1] = "Data plata";
//			header[10][0] = "tip";
//			header[10][1] = "Tip factura";
					
			try {
				ExcelUtil.getExcel(allCategories, header, "Raport_facturi_letrate",
						resourceResponse);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

 

}
