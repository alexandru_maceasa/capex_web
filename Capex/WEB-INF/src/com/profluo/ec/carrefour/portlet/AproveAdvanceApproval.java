package com.profluo.ec.carrefour.portlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletConfig;
import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;
import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.portlet.LiferayPortletConfig;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.servlet.SessionMessages;
import com.liferay.portal.kernel.util.JavaConstants;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.model.Role;
import com.liferay.portal.model.Team;
import com.liferay.portal.model.User;
import com.liferay.portal.service.UserLocalServiceUtil;
import com.liferay.portal.service.UserServiceUtil;
import com.liferay.portal.util.PortalUtil;
import com.liferay.util.bridges.mvc.MVCPortlet;
import com.profluo.ecm.controller.UserTeamIdUtils;
import com.profluo.ecm.flows.Links;
import com.profluo.ecm.flows.NotificationTemplates;
import com.profluo.ecm.flows.NotificationsSubject;
import com.profluo.ecm.model.db.AdvanceRequest;
import com.profluo.ecm.model.db.AdvanceRequestApprovals;
import com.profluo.ecm.model.db.DatabaseConnectionManager;
//import com.profluo.ecm.model.db.AdvanceRequestApprovals;
import com.profluo.ecm.model.db.DefExchRates;
import com.profluo.ecm.model.db.GenericDBStatements;
import com.profluo.ecm.model.db.InvoiceHeader;
import com.profluo.ecm.model.db.UsersInformations;
import com.profluo.ecm.notifications.Notifications;

/**
 * Portlet implementation class AproveAdvanceApproval
 */
public class AproveAdvanceApproval extends MVCPortlet {
	/**
	 * 
	 */
	private final Log log = LogFactoryUtil.getLog("PROFLUO-" + AproveAdvanceApproval.class.getName());
	
    /**
     * @see MVCPortlet#MVCPortlet()
     */
    public AproveAdvanceApproval() {
        super();
        log.info("[PROFLUO] Initialize AproveAdvanceApproval portlet.");
    }

    @SuppressWarnings("unchecked")
	@Override
    public void serveResource(ResourceRequest resourceRequest, ResourceResponse resourceResponse) throws IOException, PortletException {
    	log.info("[AJAX] Validate RequestAdvanceApproval");
    	
    	String action = ParamUtil.getString(resourceRequest, "action");

    	log.info("[AJAX] action: " + action);
    	PrintWriter writer = resourceResponse.getWriter();
    	if (action.equals("showDpi")) {
    		String idDpi = ParamUtil.getString(resourceRequest, "dpi");
    		// get DPI header info based on DPI id
    		List<HashMap<String,Object>> oneDpi = null;

    		oneDpi = GenericDBStatements.getAllByFieldName("dpi_header", "id", idDpi);

	    	JSONArray jsonDpiHeader = new JSONArray();
	    	if (oneDpi != null) {
	    		jsonDpiHeader = DatabaseConnectionManager.convertListToJson(oneDpi);
	    	}
	    	
	    	// get DPI lines info based on DPI id
	    	List<HashMap<String,Object>> dpiLines = null;
    		
    		dpiLines = GenericDBStatements.getAllByFieldName("dpi_line", "id_dpi", idDpi);

	    	JSONArray jsonDpiLines = new JSONArray();
	    	if (dpiLines != null) {
	    		jsonDpiLines = DatabaseConnectionManager.convertListToJson(dpiLines);
	    	}
	    	
	    	// create JSON response
	    	JSONObject jsonResponse = new JSONObject();
			jsonResponse.put("header", jsonDpiHeader);
			jsonResponse.put("lines", jsonDpiLines);
			// send AJAX response
			writer.print(jsonResponse.toJSONString());
    	} else if (action.equals("filter") || action.equals("next") || action.equals("prev") || action.equals("last") || action.equals("first")) {
        	int stage = Integer.parseInt(ParamUtil.getString(resourceRequest, "stage"));
	    	int start = Integer.parseInt(ParamUtil.getString(resourceRequest, "start").toString());
	    	int count = Integer.parseInt(ParamUtil.getString(resourceRequest, "count").toString());
	    	int total = Integer.parseInt(ParamUtil.getString(resourceRequest, "total").toString());
	    	int companyId = Integer.parseInt(ParamUtil.getString(resourceRequest, "company_id").toString());
	    	int storeId = Integer.parseInt(ParamUtil.getString(resourceRequest, "store_id").toString());
	    	String voucherNo = ParamUtil.getString(resourceRequest, "voucher_no").toString();
	    	String voucherDate = ParamUtil.getString(resourceRequest, "voucher_date").toString();
	    	int userId = 0;
	    	int id_store_aisle = 0;
	    	User user = null;
			try {
				user = UserServiceUtil.getUserById(Long.parseLong(resourceRequest.getRemoteUser()));
				userId = (int) user.getUserId();
				id_store_aisle = Integer.parseInt(user.getAddresses().get(0).getStreet3().toString());
			} catch (NumberFormatException | PortalException | SystemException e1) {
				e1.printStackTrace();
			}
	    	
	    	log.info("[AJAX] start: " + count);
	    	log.info("[AJAX] count: " + start);
	    	log.info("[AJAX] total: " + total);
	    	log.info("[AJAX] user: " + userId);
	    	
	    	resourceResponse.setProperty("test", "test_content");
	    	
	    	List<HashMap<String,Object>> allInvoices = null;
	    	try {
    			log.info("Getting all!");
    			allInvoices = AdvanceRequest.getAdvanceRequestApprovers(start, count, companyId, voucherNo,
    					voucherDate, 0, "", (int)user.getUserId());
    			// set initiator
    			for (int i = 0; i < allInvoices.size(); i++) {
    				int initiatorId = Integer.parseInt(allInvoices.get(i).get("id_user").toString()); 
    	 			User initiator = UserLocalServiceUtil.getUserById(initiatorId);
    	 			//solicitant.getJobTitle() - functie
    	 			//solicitant.getFullName() - solicitant
    	 			allInvoices.get(i).put("solicitant", initiator.getFullName());
    	 			allInvoices.get(i).put("functie", initiator.getJobTitle());
    			}
			} catch(Exception e) { }
			
			// if total is 0 we need to refresh the count
			if (total == 0) {
				total = AdvanceRequest.getAdvanceRequestApproversCount(start, count, companyId,
						voucherNo, voucherDate, 0, "", (int)user.getUserId());
			}
	    	
	    	JSONArray jsonInvoices = new JSONArray();
	    	if (allInvoices != null) {
	    		jsonInvoices = DatabaseConnectionManager.convertListToJson(allInvoices);
	    	}
	    	JSONObject jsonResponse = new JSONObject();
	    	
			jsonResponse.put("action", action);
			jsonResponse.put("start", start);
			jsonResponse.put("count", count);
			jsonResponse.put("total", total);
			jsonResponse.put("values", jsonInvoices);
			writer.print(jsonResponse.toJSONString());		
    	}
    }
    
    private void addNotificationToNextTeam(String advanceRequestId, User user){
    	String templateId = "";
    	String link = "";
    	boolean goToAccounting = true;
    	//list of users to be notified
		List<HashMap<String,Object>> usersToBeNotified = new ArrayList<HashMap<String,Object>>();
		//continutul notificarii
		HashMap<String, Object> bodyDetails = new HashMap<String,Object>();
		
    	List<HashMap<String,Object>> firstApprovals = AdvanceRequest.getFirstLevelApprovals(String.valueOf(advanceRequestId));
    	
		for(HashMap<String,Object> hash : firstApprovals) {
			goToAccounting = false; 
			String approvalsIds = hash.get("id_user").toString().substring(1, hash.get("id_user").toString().length() - 1);
			String [] individualIds = approvalsIds.split(", ");
			try {
				for(int i = 0; i < individualIds.length; i++ ) {
					User approval = null;
					try{
						approval = UserLocalServiceUtil.getUser(Integer.parseInt(individualIds[i]));
					} catch (NumberFormatException e){
						continue;
					}
					HashMap<String,Object> userInfo = new HashMap<String,Object>();
					userInfo.put("email", approval.getEmailAddress());
					userInfo.put("first_name", approval.getFirstName());
					userInfo.put("last_name", approval.getLastName());
					if(!usersToBeNotified.contains(userInfo)) {
						usersToBeNotified.add(userInfo);
					}
				}
			} catch (NumberFormatException | PortalException | SystemException e) {
				e.printStackTrace();
			}
			switch (hash.get("level_approval").toString()){
			case "2" :
				templateId = NotificationTemplates.CONTA_CERERE_PLATA_AVANS;
	    		link = Links.individualAdvanceRequestForApprovals + "&id=" + advanceRequestId;
	    		break;
			case "3" :
				templateId = NotificationTemplates.DIR_FINANCIAR_PLATA_AVANS;
	    		link = Links.individualAdvanceRequestForApprovals + "&id=" + advanceRequestId;
	    		break;
	    	default :
	    		templateId = "";
	    		link = "#";
	    		break;
			}
		}
		
		if (goToAccounting){
			String [][] arrFields = new String[1][2];
			arrFields[0][0] = "stage";
			arrFields[0][1] = "5";
			ArrayList<String> columns = new ArrayList<String>();
			columns.add("id");
			ArrayList<String> values = new ArrayList<String>();
			values.add(advanceRequestId);
			GenericDBStatements.updateEntry(arrFields, "advance_request", new ArrayList<String>(), columns, values);
			//TODO : ALL ACCOUNTING UNIT?
			usersToBeNotified = UsersInformations.getUserDetailsByTeamId(UserTeamIdUtils.ACCOUNTING);			
			templateId = NotificationTemplates.CONTA_CERERE_PLATA_AVANS;
    		link = Links.individualAdvanceRequestForAccounting + "&id=" + advanceRequestId;
		}
		List<HashMap<String,Object>> advanceRequestDetails = AdvanceRequest.getAdvanceRequestDetails(advanceRequestId);
		String supplierName = advanceRequestDetails.get(0).get("numeFurnizor").toString();
		String companyName = advanceRequestDetails.get(0).get("numeCompanie").toString();
		String invDate = advanceRequestDetails.get(0).get("issue_date").toString();
		
		String body =  "<tr><td style='border:1px solid black'>" + advanceRequestId + "</td><td style='border:1px solid black'>" + invDate + "</td>"
					+ "<td style='border:1px solid black'>" + supplierName + "</td><td style='border:1px solid black'>" + companyName
					+ "</td><td style='border:1px solid black'><a href='" + link + "'>Link</a></td></tr>";
		
		bodyDetails = setInsertParameters(templateId, "", "", body, "0", "B", "1", user, link);
		Notifications.insertNotification(advanceRequestId, usersToBeNotified, bodyDetails, NotificationsSubject.SOLICITARE_PLATA_AVANS);
    }

    private HashMap<String, Object> setInsertParameters (String id_template, String to_email, String to_name, String body, String status, 
			String type, String doc_type, User user, String link){
		
		HashMap<String, Object> bodyDetails = new HashMap<String,Object>();
		
		bodyDetails.put("id_template", id_template);
		bodyDetails.put("sender_email", user.getEmailAddress());
		bodyDetails.put("sender_name", user.getFullName());
		bodyDetails.put("body", body);
		String currentTime = new Timestamp(new Date().getTime()).toString();
		bodyDetails.put("time_to_send", currentTime);
		bodyDetails.put("time_of_send", currentTime);
		bodyDetails.put("status", status);
		bodyDetails.put("type", type);
		bodyDetails.put("doc_type", doc_type);
		bodyDetails.put("created", currentTime);
		if(link.contains("url")){
			bodyDetails.put("link", link.substring(0, link.lastIndexOf("?")));
		} else {
			bodyDetails.put("link", link);
		}
		return bodyDetails;
	}
    
    private void addRefuzNotificationToAllApprovals(String advanceRequestId, User user) {
    	
    	String initiator = AdvanceRequest.getUserIdByAdvanceRequest(advanceRequestId);
    	List<String> allAprovals = AdvanceRequestApprovals.getApprovalsOfTheAdvanceRequest(advanceRequestId);
    	
    	//removing the current approval and adding the initiator
    	for (int i = allAprovals.size() - 1; i >= 0; i--){
    		if(allAprovals.get(i).equals(String.valueOf(user.getUserId()))){
    			allAprovals.remove(i);
    		}
    	}
    	allAprovals.add(initiator);
		List<HashMap<String,Object>> advanceRequestDetails = AdvanceRequest.getAdvanceRequestDetails(advanceRequestId);
		String supplierName = advanceRequestDetails.get(0).get("numeFurnizor").toString();
		String companyName = advanceRequestDetails.get(0).get("numeCompanie").toString();
		String invDate = advanceRequestDetails.get(0).get("issue_date").toString();
    	for(int i = 0; i < allAprovals.size(); i++) {
    		List<HashMap<String,Object>> userDetails = UsersInformations.getUserDetailsByUserId(Integer.parseInt(allAprovals.get(i)));
    		HashMap<String, Object> bodyDetails = new HashMap<String,Object>();
    		//obtinere team in functie de userId
    		String approvalLevel = AdvanceRequest.getApprovalLevel(advanceRequestId, allAprovals.get(i));
    		String teamId = "";
    		switch (approvalLevel){
    		case "1" :
    			teamId = String.valueOf(UserTeamIdUtils.DEPARTAMENT_DIRECTOR);
    			break;
    		case "2" :
    			teamId = String.valueOf(UserTeamIdUtils.ACCOUNTING_DIRECTOR);
    			break;
    		case "3" :
    			teamId = String.valueOf(UserTeamIdUtils.FINANCIAL_DIRECTOR);
    			break;
    		default :
    			teamId = "";
    			break;
    		}
    		//obtinere link in functie de team
    		if (i == allAprovals.size() - 1 && teamId.equals("")){
    			//initiatorul
    			boolean teams = UsersInformations.belongsToTeam(Integer.parseInt(allAprovals.get(i)), UserTeamIdUtils.STORE_DIRECTOR);
    			if (teams){
    				teamId = String.valueOf(UserTeamIdUtils.STORE_DIRECTOR);
    			} else {
    				teamId = String.valueOf(UserTeamIdUtils.DEPARTAMENT_ASSISTENT);
    			}
    		}
    		//TODO get link
    		String link = getLink(advanceRequestId, teamId);
    		
    		String body = "<tr><td style='border:1px solid black'>" + advanceRequestId + "</td><td style='border:1px solid black'>" + invDate + "</td>"
					+ "<td style='border:1px solid black'>" + supplierName + "</td><td style='border:1px solid black'>" + companyName +
					"</td>" + "<td style='border:1px solid black'><a href='" + link + "'>Link</a></td></tr>";
    		bodyDetails = setInsertParameters(NotificationTemplates.REFUZ_PLATA_AVANS, "", "", body, "0", "B", "1", user, link);
    		Notifications.insertNotification(advanceRequestId, userDetails, bodyDetails, NotificationsSubject.REFUZ_PLATA_AVANS);
    	}
	}
    
    private String getLink(String advanceRequestId, String teamId){
    	String link = null;
		int team = Integer.parseInt(teamId);
		
		switch (team) {
		case UserTeamIdUtils.DEPARTAMENT_ASSISTENT:
		case UserTeamIdUtils.STORE_DIRECTOR:
			link = Links.individualRequestForInitiators + "&id=" + advanceRequestId;
			break;
		case UserTeamIdUtils.DEPARTAMENT_DIRECTOR:
		case UserTeamIdUtils.ACCOUNTING_DIRECTOR:
		case UserTeamIdUtils.FINANCIAL_DIRECTOR:
			link = Links.allAdvanceRequestForApprovals + "&id=" + advanceRequestId;
			break;
		default:
			link = "#";
			break;
		}
		return link;
    }
    
	public void updateStage(ActionRequest request, ActionResponse response) {
		PortletConfig portletConfig = (PortletConfig)request.getAttribute(JavaConstants.JAVAX_PORTLET_CONFIG);
	   	LiferayPortletConfig liferayPortletConfig = (LiferayPortletConfig) portletConfig;
	   	SessionMessages.add(request, liferayPortletConfig.getPortletId() + SessionMessages.KEY_SUFFIX_HIDE_DEFAULT_ERROR_MESSAGE);
		
		log.info("[PROFLUO] AproveAdvanceApproval portlet - update stage()");
		try{
			User user = UserServiceUtil.getUserById(Long.parseLong(request.getRemoteUser()));
	    	int id_user = (int) user.getUserId();
	    	
	    	String blocked = "0";
	    	String approved = "1";
			//you can call this method : with multiples ids to update or with just only one
			//multiple ids are received from parameter idToUpdate
			//single id is received from parameter idUpd
			String state=request.getParameter("refuzSauAproba");
			String id = request.getParameter("idToUpdate");
			if(id == null) {
				id=request.getParameter("idUpd");
			}
			if(state == null) {
				state = request.getParameter("refuzlaplata");
			}
			System.out.println("state = " + state);
			
			int updated = 0 ;
			String errorOnId="";
			String [] parts = id.split(",");
			if (state.equals("refuz")) {
				blocked = "1";
				approved = "0";
			} else {
				approved = "1";
				blocked = "0";
			}
			// for each selected advance request
			Calendar calendar = Calendar.getInstance();
		    java.sql.Timestamp currentTS = new java.sql.Timestamp(calendar.getTime().getTime());
			for(String eachId : parts) {
				updated = AdvanceRequest.approveAdvanceRequest(currentTS.toString(), String.valueOf(id_user), eachId, blocked, approved);
				if(updated < 0){
					errorOnId = eachId;
					log.info("[UPD] error on id : "+ errorOnId);
					break;
				} 
				//TODO: redo notifications
				if (state.equals("refuz")) {
					//TODO update header stage
					String [][] arrFields = new String[1][2];
					arrFields[0][0] = "stage";
					arrFields[0][1] = "-3";
					ArrayList<String> columns = new ArrayList<String>();
					columns.add("id");
					ArrayList<String> values = new ArrayList<String>();
					values.add(eachId);
					GenericDBStatements.updateEntry(arrFields, "advance_request", new ArrayList<String>(), columns, values);
					addRefuzNotificationToAllApprovals(eachId, user);
				} else {
					addNotificationToNextTeam(eachId, user);
				}
				
			}
			String source = request.getParameter("sourcePage");
			
			if (updated > 0) {
	    		SessionMessages.add(request, "update_ok");
	    	} else {
	    		SessionErrors.add(request, "update_nok");
	    		log.info("[UPD] error on id : "+ errorOnId);
	    		//redirect to the page where the approval failed
	    		if(source.equals("viewAprove")){
	    			response.setRenderParameter("mvcPath", "/html/aproveadvanceapproval/view.jsp");
	    		} else  response.setRenderParameter("mvcPath", "/html/aproveadvanceapproval/aprove.jsp");
	    	}
		}catch (Exception e){}
		
   }

	@Override
	public void render(RenderRequest renderRequest, RenderResponse renderResponse) throws PortletException, IOException {
		HttpServletRequest request = PortalUtil.getHttpServletRequest(renderRequest);
    	String p_p_id = PortalUtil.getOriginalServletRequest(request).getParameter("p_p_id");
    	String path = PortalUtil.getOriginalServletRequest(request).getParameter("_" + p_p_id + "_mvcPath");
    	
		log.info("[PROFLUO] AproveAdvanceApproval portlet - render()");
		log.info("[PROFLUO] Context Path: " + renderRequest.getContextPath());
    	log.info("[PROFLUO] Portlet ID	: " + p_p_id);
    	log.info("[PROFLUO] Portlet Path: " + path);
    	
    	try {
	    	User user = UserServiceUtil.getUserById(Long.parseLong(renderRequest.getRemoteUser()));
	    	int id_user 			= (int) user.getUserId();
	    	
	    	List<Team> userTeams = user.getTeams();
	    	for (int i = 0; i < userTeams.size(); i++) {
	    		log.info("[PROFLUO] User TEAM[" + i + "]: " + userTeams.get(i).getName() + " - " + userTeams.get(i).getTeamId() );
	    	}
	    	
	    	List<Role> userRoles = user.getRoles();
	    	for (int i = 0; i < userRoles.size(); i++) {
	    		log.info("[PROFLUO] User ROLE[" + i + "]: " + userRoles.get(i).getName());
	    	}
	    	
	    	log.info("[PROFLUO] User id " + id_user);
	    	//log.info("[PROFLUO] Group id " + groupId);
    	} catch (Exception e) {}
    	
    	if (path == null || path.endsWith("view.jsp")) {
    		log.info("[PROFLUO] LISTING render()");
    		
    	} else if (path.endsWith("aprove.jsp")) {
    		log.info("[PROFLUO] Aprobare solicitare creare avans render()");
    		
    	}
		
	    super.render(renderRequest, renderResponse);
	}
	
	private List<HashMap<String,Object>> getOneAdvanceRequest(String id) {
		List<HashMap<String,Object>> resultByid = AdvanceRequest.getOne(id);
		return resultByid;
	}

	public int getNextStage(String ids, String state, List<HashMap<String,Object>> resultByid){

		String value = resultByid.get(0).get("val_with_vat_curr").toString();
		String currency = resultByid.get(0).get("moneda").toString();
		//String exchange_rate= resultByid.get(0).get("rata_schimb").toString();
		String stage = resultByid.get(0).get("stage").toString();
		
		int type= 1 ;//for exchange rate 
		String date = resultByid.get(0).get("inv_date").toString();//for exchange rate
		float exchange_rate;//exchange_rate din currency in RON
		float valueInRON;//valoare currency transformata in RON
		float exchangeRateEUR = DefExchRates.getExchangeRate("EUR", date, type);
		
		if(stage.equals("2")){
			if(state.equals("refuz")){
				return -2;
			}
			return 3;//catre director contabilitate
		}
		if(stage.equals("3")){
			if(state.equals("refuz")){
				return -3;
			}
			if(currency.equals("EUR")){
//				System.out.println("Moneda este : "+ currency + " cu valoarea " + value);
				if(Float.parseFloat(value) >= UserTeamIdUtils.FINANCIAL_DIRECTOR_APROVE_VALUE){
					//catre director financiar
					return 4;
				} else return 5; //catre contabilitate
			}else if (currency.equals("RON")){
//				System.out.println("-------------------------------------");
//				System.out.println("Moneda este : "+ currency);
//				System.out.println("Valoarea in RON este: "+  Float.parseFloat(value) );
//				System.out.println("Rata catre EUR este : "+ exchangeRateEUR);
//				System.out.println("Valoarea in EUR este : " + Float.parseFloat(value) / exchangeRateEUR);
//				System.out.println("-------------------------------------");
				if ( Float.parseFloat(value) / exchangeRateEUR >= UserTeamIdUtils.FINANCIAL_DIRECTOR_APROVE_VALUE){
					return 4;
				}else return 5;
			}else{
//				System.out.println("-------------------------------------");
//				System.out.println("Moneda este : "+ currency);
//				System.out.println("Valoarea in "+currency+" este: "+  Float.parseFloat(value) );
//				System.out.println("Rata EUR este : "+ exchangeRateEUR);
				
				exchange_rate = Float.parseFloat(resultByid.get(0).get("rata_schimb").toString());
				valueInRON = exchange_rate * Float.parseFloat(value);
//				System.out.println("Exchange rate-ul pentru moneda "+ currency + " din data de "+ date+ " este : "+ exchange_rate);
//				System.out.println("Valoarea in RON este : "+ valueInRON);
//				System.out.println("-------------------------------------");
				if( valueInRON / exchangeRateEUR >= UserTeamIdUtils.FINANCIAL_DIRECTOR_APROVE_VALUE){
					return 4;
				} else return 5;
			}
		}
		if(stage.equals("4")){
			if(state.equals("refuz")){
				return -4;
			}
			return 5;//catre contabilitate
		}
		return 0;
	}
}
