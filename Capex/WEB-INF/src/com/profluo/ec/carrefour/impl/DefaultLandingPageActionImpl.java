package com.profluo.ec.carrefour.impl;

import java.text.NumberFormat;
import java.text.ParsePosition;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.liferay.portal.kernel.events.Action;
import com.liferay.portal.kernel.events.ActionException;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.struts.LastPath;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.model.Group;
import com.liferay.portal.model.User;
import com.liferay.portal.security.permission.PermissionChecker;
import com.liferay.portal.security.permission.PermissionCheckerFactoryUtil;
import com.liferay.portal.security.permission.PermissionThreadLocal;
import com.liferay.portal.util.PortalUtil;

public class DefaultLandingPageActionImpl extends Action{

	private String capex = "capex";
	private String galcom2 = "galerii-comerciale-ii";
	private String homeCapex = "home";
	private String homeGalcom2 = "contracte";
	
	public void run(HttpServletRequest request, HttpServletResponse response)
			throws ActionException {

			try {
				doRun(request, response);
			}
			catch (Exception e) {
				throw new ActionException(e);
			}
		}
	/**
	 * True if the string received as param is a numeric value
	 * @param str
	 * @return
	 */
	public static boolean isNumeric(String str)
	{
	  NumberFormat formatter = NumberFormat.getInstance();
	  ParsePosition pos = new ParsePosition(0);
	  formatter.parse(str, pos);
	  return str.length() == pos.getIndex();
	}
	
	/**
	 * Form new site path based on logged user
	 * @param user
	 * @param includeLanguage
	 * @param companyId
	 * @return
	 * @throws PortalException
	 * @throws SystemException
	 */
	private String getSitePath(User user, boolean includeLanguage, long companyId) throws PortalException, SystemException {
		
		String sitePath = StringPool.BLANK;
		//get rid of administrator permission
		PermissionChecker permissionChecker = null;
		try {
			permissionChecker = PermissionCheckerFactoryUtil.create(user);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		PermissionThreadLocal.setPermissionChecker(permissionChecker); 
		List<Group> userSites = user.getMySiteGroups();
		List<String> sites = new ArrayList<String>();
		for (Group g : userSites){
			String siteName = g.getName();
			if (!isNumeric(siteName) && !sites.contains(siteName)){
				sites.add(siteName.toString().toLowerCase().replace(" ", "-"));
			}
		}
		if ((userSites != null) && !userSites.isEmpty()) {
			if (sites.contains(this.capex)){
				sitePath = "/web/" + this.capex + "/" + this.homeCapex;
			} else if (sites.contains(this.galcom2)){
				sitePath = "/web/" + this.galcom2 + "/" + this.homeGalcom2;
			}
		}
		return sitePath;
	}
	
	/**
	 * Set the new path
	 * @param request
	 * @param response
	 */
	protected void doRun(HttpServletRequest request, HttpServletResponse response){
		try{
			User user = PortalUtil.getUser(request);
			long companyId = PortalUtil.getCompanyId(request);
			String path = getSitePath(user, false, companyId);
			
			HttpSession session = request.getSession();
			LastPath lastPath = new LastPath(StringPool.BLANK, path);
			session.setAttribute(WebKeys.LAST_PATH, lastPath);

		} catch (Exception e){ e.printStackTrace();}
	}

}
